#include "calendarrecord.h"



QList<QDate> CalendarRecord::getListDatesMax() const
{
    return listDatesMax;
}

void CalendarRecord::setListDatesMax(const QList<QDate> &value)
{
    listDatesMax = value;
}

QDate CalendarRecord::getDateSelected() const
{
    return dateSelected;
}

void CalendarRecord::setDateSelected(const QDate &value)
{
    dateSelected = value;
}
