#include "downloadmanager.h"
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
DownloadManager::DownloadManager(QObject *parent) {
    connect(this, &DownloadManager::doDownloadVideoSuccess , this , &DownloadManager::onDownloadVideoSuccess);
    connect(this, &DownloadManager::doDownloadVideoError , this , &DownloadManager::onDownloadVideoError);
}


void DownloadManager::doDownload(const QUrl &url, QString filePath) {
    //    //    qDebug()<<"doDownLoad";
    //    mStarted = false;
    //    QNetworkAccessManager manager;
    //    qDebug() << "DODOWNLOAD" << url;
    //    QNetworkRequest request(url);
    //    reply = manager.get(request);
    //    connect(reply, &QNetworkReply::downloadProgress, this,
    //            &DownloadManager::processbarDownload);
    //    connect(reply, &QNetworkReply::finished, this,
    //            &DownloadManager::downloadFinished);
    //    connect(reply, &QNetworkReply::readyRead, this,
    //            &DownloadManager::downloadPackage);
    //    currentDownloads.append(reply);
    //    qDebug() << "DO DOWNLOAD";

    mFilePath = filePath;

    QEventLoop eventLoop;
    QNetworkAccessManager manager;
    QObject::connect(&manager, SIGNAL(finished(QNetworkReply *)), &eventLoop,
                     SLOT(quit()));
    reply  = manager.get(QNetworkRequest(QUrl(url)));
    connect(reply, &QNetworkReply::downloadProgress, this,
            &DownloadManager::processbarDownload);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        //        qDebug() << "b" << reply->pos() << reply->bytesAvailable();
        //        qDebug() << "\tdata" << data.size();
        //        qDebug() << "a" << reply->pos() << reply->bytesAvailable();
        //        qDebug() << "Open File" << mFilePath;
        QByteArray data = reply->readAll();
        QFile file(mFilePath);
        file.open(QIODevice::ReadOnly | QIODevice::Append);
        file.seek(file.size());
        file.write(data);
        file.close();
        Q_EMIT doDownloadVideoSuccess();
        reply->deleteLater();
    } else {
        Q_EMIT doDownloadVideoError();
        fprintf(stderr, "Download of %s failed: %s\n", url.toEncoded().constData(),
                qPrintable(reply->errorString()));
        qDebug() << "Lỗi yêu cầu:" << reply->errorString();
        reply->deleteLater();
    }
}

void DownloadManager::cancelDownloadVideoMP4(){
    if(reply && reply->isRunning()){
        reply->abort();
        reply->deleteLater();
    }
}

void DownloadManager::onDownloadVideoSuccess(){
    qDebug() << "Download video success --> doDownload";
    Q_EMIT downloadVideoSuccess();

}
void DownloadManager::onDownloadVideoError(){
    qDebug() << "Download video error --> doDownload";

    Q_EMIT linkRecordNotFound();
}


void DownloadManager::errorDownloadVideo(QNetworkReply::NetworkError error){
    qDebug() << "QNetworkReply::NetworkError" << error;

}
bool DownloadManager::saveToDisk(QUrl url, const QString &filename) {
    QString homePath = QDir::homePath();
    QString rootPath = homePath + "/MultimediaOfVideowall/Videos";
    //    QString rootPath ="/mnt/hdd/videowallstores/Videos";
    if(!QDir(rootPath).exists()){
        QDir().mkpath(rootPath);
    }

    QString filePath = rootPath + "/" + filename;

    QString filePathSave = changeFileName(filePath);
    qDebug() << "change filename save " << filePathSave;

    if (filePathSave.isEmpty()) {
        qDebug() << "cancel save";
        Q_EMIT cancelSaveVideoRecord();
        return false;
    } else {
        doDownload(url, filePathSave);
        return true;
    }
}

QString DownloadManager::changeFileName(QString filename) {
    QString basename = QFile(filename).fileName();
    if (basename.isEmpty()) basename = "download";
    if (QFile::exists(basename)) {
        // already exists, don't overwrite
        int i = 0;
        basename += '.';
        while (QFile::exists(basename + QString::number(i))) ++i;
        basename += QString::number(i);


        qDebug() << "Base name save" << basename;
    }
    return basename;
}


void DownloadManager::startDownloading(QUrl url,QString fileName) {
    saveToDisk(url, fileName);
}

void DownloadManager::processbarDownload(qint64 bytesReceived,
                                         qint64 bytesTotal) {
    if (bytesTotal != 0) {
        double percent = (bytesReceived * 1.0) / bytesTotal;
        int percentInt = percent * 100;
        Q_EMIT percentProcessbar(percentInt);
    }
}
