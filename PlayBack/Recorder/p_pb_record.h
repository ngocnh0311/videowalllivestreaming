#ifndef P_PBRecord_H
#define P_PBRecord_H

#include <Common/generaldefine.h>
#include <PacModel/presentation.h>
#include <QCalendarWidget>
#include <QComboBox>
#include <QDateEdit>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QObject>
#include <QProgressBar>
#include <QPushButton>
#include <QSpinBox>
#include <QTabWidget>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/resources.h"
#include "c_pb_record.h"
#include "calendarrecord.h"
#include "PlayBack/Calendar/DefDateTimeEdit.h"
class C_PBRecord;
class P_PBRecord : public Presentation {
    // init ui control
private:

    int mWidthRight = 550;
    int mHeigthRight = 350; //total

    int mHeigthDateBot = 290; //top panel
    int mHeightBotWidget = 60; //bottom panel

    long mControlTime = 60*60; //10 minutes for clipper

    QWidget *zone;

    //Main layout and widget
    QVBoxLayout *mainVLayout;
    QTabWidget *mMainTabWidget;
    QWidget *pPanelWidget;
    QVBoxLayout *pPanelLayout;


    QWidget *clipperInfoBotWidget;
    QVBoxLayout *clipperInfoVBoxLayout;
    QGridLayout *clipperInfoGridLayout;

    //Clipper information
    QWidget *statusWidget;
    QWidget *commentWidget;
    DefDateTimeEdit* startCalendar;
    DefDateTimeEdit* endCalendar;

    QComboBox *qualityVideoBox;
    QComboBox *sourceVideoBox;
    QLineEdit* fileNameBox;


    //Widget and layout for button
    QWidget *botWidget;
    QVBoxLayout *botVBoxLayout;
    QWidget *processerDownloadWidget;
    QProgressBar *processBarDownload;
    QWidget *botControlWidget;
    QHBoxLayout *botControlHBoxLayout;

    //process socket
    QWidget *processerSocketWidget;
    QProgressBar *processBarSocket;
    QPushButton *cancelCreateVideoButton;
    QPushButton *cancelDownloadVideoButton;


    QPushButton *pCloseButton;
    QPushButton *startRecordButton;

    long timeStampStart = 0;
    long timeStampEnd = 0;
    long currentPlayBackTime;

    QLabel* statusLabel;
    QLabel* durationLabel;

protected:

    bool eventFilter(QObject *obj, QEvent *event);

public:

    P_PBRecord(Control *ctrl, QWidget *zone);
    C_PBRecord *control() { return (C_PBRecord *)this->ctrl; }

    QWidget *getZone(int zoneId);
    void initUI();

    TimeStampRecord getTimeStampRecord();

    //Init widget for clipper information
    void initClipperInfoWidget();
    void initDateStartWidget();
    void initDateEndWidget();
    void initSourceWidget();
    void initStatusWidget();
    void initCommentWidget();

    //Init widget for button
    void initBotWidget();

    void generateLinkMp4Success();
    void totalSecondRecordIsTooSmall();

    void setCurrentPlaybackTime(long time);
    void openRecordDialog(RecordInformation recordInfo);
    void updateLayoutMode(int message);

Q_SIGNALS:

public Q_SLOTS:

    void startRecordDetail();
    void closeMenuRecord();

    void downloadVideoRecordSuccess();
    void cancelSaveVideoRecord();
    void updateProcessBarDownload(int percent);
    void linkRecordNotFound();
    void getClipError();
    void onChangeTabIndex(int index);
    void cancelCreateVideoMP4();
    void cancelCreateVideoMP4Success();
    void cancelDownloadVideoMP4();
    void cancelDownloadVideoMP4Success();

    void startCalendarUpdateTime(QDateTime);
    void endCalendarUpdateTime(QDateTime);

    //process socket
    void updateProcessBarSocketGenerate(int percent);


};

#endif  // PRESENTATION_H
