#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <stdio.h>
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QStringList>
#include <QTimer>
#include <QUrl>
#include <QWidget>
#include  <QThread>

class DownloadManager : public QObject {
  Q_OBJECT


 public:
  DownloadManager(QObject *parent = 0);

  QList<QNetworkReply *> currentDownloads;
  bool mStarted = false;
  QString mFilePath;
  QNetworkReply *reply;
  void doDownload(const QUrl &url, QString filePath);
  QString changeFileName(QString filename);
  bool saveToDisk(QUrl url, const QString &filename);
  void startDownloading(QUrl url, QString fileName);
  void cancelDownloadVideoMP4();
 public Q_SLOTS:

  void processbarDownload(qint64 bytesReceived, qint64 bytesTotal);
  void errorDownloadVideo(QNetworkReply::NetworkError error);
  void onDownloadVideoSuccess();
  void onDownloadVideoError();
 Q_SIGNALS:
  void doDownloadVideoError();
  void doDownloadVideoSuccess();
  void saveFileVideoVODSuccess(QString);
  void downloadVideoSuccess();
  void cancelSaveVideoRecord();
  void percentProcessbar(qint64 percent);
  void linkRecordNotFound();
};

#endif  // DOWNLOADMANAGER_H
