#include "p_pb_record.h"

#include "PlayBack/Calendar/DefDateTimeEdit.h"
/**
     * Generic method to override for updating the presention.
     **/

P_PBRecord::P_PBRecord(Control *ctrl, QWidget *zone) : Presentation(ctrl) {
    // init gui objeact
    this->zone = zone;
    //this->zone->setFixedSize(600, 490);
    this->zone->setFixedSize(mWidthRight, mHeigthRight);

    this->zone->installEventFilter(this);

    initUI();
}

bool P_PBRecord::eventFilter(QObject *obj, QEvent *event)
{
    //This event only jump into this function when widget is visible

    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

        if(keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return) {

            startRecordDetail();

        } else if ( keyEvent->key() == Qt::Key_Escape) {

            closeMenuRecord();
        }

    }

    return QObject::eventFilter(obj, event);
}

QWidget *P_PBRecord::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}

void P_PBRecord::initUI() {
    mainVLayout = new QVBoxLayout();
    mainVLayout->setMargin(0);
    mainVLayout->setSpacing(0);
    this->zone->setLayout(mainVLayout);

    pPanelWidget = new QWidget(this->zone);
    pPanelLayout = new QVBoxLayout();
    pPanelLayout->setSpacing(0);
    pPanelLayout->setMargin(0);
    pPanelWidget->setLayout(pPanelLayout);
    pPanelWidget->setStyleSheet("background:#222");
    mainVLayout->addWidget(pPanelWidget);

    initClipperInfoWidget();
    initBotWidget();

}

void P_PBRecord::initBotWidget() {
    botWidget = new QWidget(pPanelWidget);
    botWidget->setStyleSheet("background:#222");
    botVBoxLayout = new QVBoxLayout();
    botVBoxLayout->setAlignment(Qt::AlignCenter);
    botVBoxLayout->setSpacing(20);
    botVBoxLayout->setMargin(0);
    botWidget->setLayout(botVBoxLayout);

    botWidget->setFixedSize(pPanelWidget->maximumWidth(), mHeightBotWidget);
    pPanelLayout->addWidget(botWidget);

    // add controll
    botControlWidget = new QWidget(botWidget);
    botControlWidget->setStyleSheet("background-color:#222; color #ffffff");
    pCloseButton = new QPushButton(botControlWidget);

    pCloseButton->setText("Đóng");
    pCloseButton->setFixedSize(130, 30);
    pCloseButton->setFont(Resources::instance().getExtraLargeBoldButtonFont());
    connect(pCloseButton, &QPushButton::clicked, this,
            &P_PBRecord::closeMenuRecord);

    startRecordButton = new QPushButton(botControlWidget);
    startRecordButton->setText(tr("Trích xuất"));
    startRecordButton->setFixedSize(130, 30);

    startRecordButton->setFont(Resources::instance().getExtraLargeBoldButtonFont());

    connect(startRecordButton, &QPushButton::clicked, this,
            &P_PBRecord::startRecordDetail);


    cancelCreateVideoButton = new QPushButton(botControlWidget);
    cancelCreateVideoButton->setText(tr("Hủy"));
    cancelCreateVideoButton->setFixedSize(130, 30);

    cancelCreateVideoButton->setFont(Resources::instance().getExtraLargeBoldButtonFont());

    connect(cancelCreateVideoButton, &QPushButton::clicked, this,
            &P_PBRecord::cancelCreateVideoMP4);
    cancelCreateVideoButton->hide();
    cancelCreateVideoButton->setFixedSize(0,0);

    botControlWidget->setFixedHeight(50);
    botControlHBoxLayout = new QHBoxLayout();
    botControlWidget->setLayout(botControlHBoxLayout);

    botControlHBoxLayout->setAlignment(Qt::AlignHCenter);
    botControlHBoxLayout->addWidget(startRecordButton);
    botControlHBoxLayout->setSpacing(20);
    botControlHBoxLayout->addWidget(pCloseButton);
    botControlHBoxLayout->addWidget(cancelCreateVideoButton);
    botVBoxLayout->addWidget(botControlWidget);

}

void P_PBRecord::cancelCreateVideoMP4(){

    //Still not support at this moment
    return;

    startRecordButton->setText("Trích xuất");
    startRecordButton->setEnabled(true);
    processBarDownload->setValue(0);  // With a current value of
    processBarSocket->setValue(0);
    processerDownloadWidget->hide();
    processerSocketWidget->hide();
    cancelCreateVideoButton->setFixedSize(0,0);
    cancelCreateVideoButton->hide();
    //control()->newAction(Message.CANCEL_CREATE_VIDEO_RECORD , Q_NULLPTR);

}

void P_PBRecord::cancelDownloadVideoMP4(){
    control()->newAction(Message.CANCEL_DOWNLOAD_VIDEO_RECORD , Q_NULLPTR);
}

void P_PBRecord::onChangeTabIndex(int index) {
    mMainTabWidget->setCurrentIndex(index);
}

void P_PBRecord::initClipperInfoWidget() {
    clipperInfoBotWidget = new QWidget(pPanelWidget);
    clipperInfoBotWidget->setFixedSize(mWidthRight, mHeigthDateBot);

    clipperInfoGridLayout = new QGridLayout(clipperInfoBotWidget);
    clipperInfoGridLayout->setAlignment(Qt::AlignCenter);
    clipperInfoGridLayout->setSpacing(20);

    //Add top label
    QLabel* label = new QLabel("Clipper", clipperInfoBotWidget);
    label->setFont(Resources::instance().getExtraLargeBoldButtonFont());

    clipperInfoGridLayout->addWidget(label, 0, 0, Qt::AlignCenter);

    //Init all element clipper box
    initDateStartWidget();
    initDateEndWidget();
    initStatusWidget();
    initSourceWidget();
    initCommentWidget();

    clipperInfoBotWidget->setLayout(clipperInfoGridLayout);
    pPanelLayout->addWidget(clipperInfoBotWidget);

    //    //Add warning status
    //    statusLabel = new QLabel( clipperInfoBotWidget);
    //    statusLabel->setStyleSheet("QLabel { color : #009f95; }");
    //    clipperInfoGridLayout->addWidget(statusLabel, 7, 1, Qt::AlignLeft);

}

void P_PBRecord::initDateStartWidget() {

    QLabel* label = new QLabel("Bắt đầu");
    label->setFont(Resources::instance().getSmallRegularButtonFont());
    clipperInfoGridLayout->addWidget(label, 1 , 0, Qt::AlignRight);

    startCalendar = new DefDateTimeEdit(clipperInfoBotWidget);
    startCalendar->setFont(Resources::instance().getSmallRegularButtonFont());
    startCalendar->setFixedWidth(300);
    connect(startCalendar, &DefDateTimeEdit::updateDateTime, this, &P_PBRecord::startCalendarUpdateTime);
    clipperInfoGridLayout->addWidget(startCalendar, 1, 1);

}

void P_PBRecord::initDateEndWidget() {

    QLabel* label = new QLabel("Kết thúc");
    label->setFont(Resources::instance().getSmallRegularButtonFont());
    clipperInfoGridLayout->addWidget(label, 2, 0, Qt::AlignRight);

    endCalendar = new DefDateTimeEdit(clipperInfoBotWidget);
    endCalendar->setFont(Resources::instance().getSmallRegularButtonFont());

    endCalendar->setFixedWidth(300);
    connect(endCalendar, &DefDateTimeEdit::updateDateTime, this, &P_PBRecord::endCalendarUpdateTime);
    clipperInfoGridLayout->addWidget(endCalendar, 2, 1);

}

void P_PBRecord::initStatusWidget() {

    //Duration
    QLabel* timeLabel = new QLabel("Độ dài");
    timeLabel->setFont(Resources::instance().getSmallRegularButtonFont());
    clipperInfoGridLayout->addWidget(timeLabel, 3, 0, Qt::AlignRight);

    durationLabel = new QLabel(clipperInfoBotWidget);
    durationLabel->setFont(Resources::instance().getSmallRegularButtonFont());

    durationLabel->setStyleSheet("QLabel { color : #009f95; }");
    durationLabel->setText("");
    clipperInfoGridLayout->addWidget(durationLabel, 3, 1);

    //Video quality
    QLabel* label = new QLabel("Chất lượng");
    label->setFont(Resources::instance().getSmallRegularButtonFont());
    clipperInfoGridLayout->addWidget(label, 4, 0, Qt::AlignRight);

    qualityVideoBox = new QComboBox(clipperInfoBotWidget);
    qualityVideoBox->setFont(Resources::instance().getSmallRegularButtonFont());
    qualityVideoBox->addItem("HD");
    qualityVideoBox->addItem("SD");
    qualityVideoBox->setFixedWidth(100);
    qualityVideoBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    clipperInfoGridLayout->addWidget(qualityVideoBox, 4, 1);
}

void P_PBRecord::initSourceWidget() {

    //Video source
    QLabel* label = new QLabel("Luồng Video");
    label->setFont(Resources::instance().getSmallRegularButtonFont());

    clipperInfoGridLayout->addWidget(label, 5, 0, Qt::AlignRight);

    sourceVideoBox = new QComboBox(clipperInfoBotWidget);
    sourceVideoBox->setFont(Resources::instance().getSmallRegularButtonFont());

    sourceVideoBox->addItem("NVR");
    sourceVideoBox->addItem("CDN");
    sourceVideoBox->setFixedWidth(100);
    sourceVideoBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    clipperInfoGridLayout->addWidget(sourceVideoBox, 5, 1);
}

void P_PBRecord::initCommentWidget() {

    //File name
    QLabel* label = new QLabel("Tiêu đề/Tên File");
    label->setFont(Resources::instance().getSmallRegularButtonFont());

    clipperInfoGridLayout->addWidget(label, 6, 0, Qt::AlignRight);

    fileNameBox = new QLineEdit(clipperInfoBotWidget);
    fileNameBox->setFont(Resources::instance().getSmallRegularButtonFont());

    fileNameBox->setPlaceholderText("Lý do trích xuất");
    fileNameBox->setFixedWidth(300);
    fileNameBox->setMaxLength(35);

    clipperInfoGridLayout->addWidget(fileNameBox, 6, 1);

}

TimeStampRecord P_PBRecord::getTimeStampRecord() {

    TimeStampRecord timeStampRecord;

    // start
    QDate dateStart = startCalendar->date();
    QTime timeStart = startCalendar->time();

    QString dateStringStart = dateStart.toString("dd-MM-yyyy");
    QString hourStart = QString::number(timeStart.hour());
    QString minuteStart = QString::number(timeStart.minute());
    QString secondStart = QString::number(timeStart.second());
    qDebug() << dateStringStart << hourStart << minuteStart << secondStart;
    long timeStampStart = GeneralDefine::instance().convertDateToTimeStamp(
                dateStringStart, hourStart, minuteStart, secondStart);


    // end
    QDate dateEnd = endCalendar->date();
    QTime timeEnd = endCalendar->time();

    QString dateStringEnd = dateEnd.toString("dd-MM-yyyy");
    QString hourEnd = QString::number(timeEnd.hour());
    QString minuteEnd = QString::number(timeEnd.minute());
    QString secondEnd = QString::number(timeEnd.second());
    qDebug() << dateStringEnd << hourEnd << minuteEnd << secondEnd;
    long timeStampEnd = GeneralDefine::instance().convertDateToTimeStamp(
                dateStringEnd, hourEnd, minuteEnd, secondEnd);

    timeStampRecord.startTimeStampRecord = timeStampStart;
    timeStampRecord.endTimeStampRecord = timeStampEnd;

    timeStampRecord.fileName = fileNameBox->text().simplified();
    timeStampRecord.quality = qualityVideoBox->currentText();

    timeStampRecord.network = (sourceVideoBox->currentText() == "NVR" ? "LAN" :  sourceVideoBox->currentText() );

    qDebug() << "Thoi gian timestamp bat dau" << timeStampStart << "ket thuc"
             << timeStampEnd;
    return timeStampRecord;
}

void P_PBRecord::startRecordDetail() {

    TimeStampRecord timeStampRecord;
    timeStampRecord = getTimeStampRecord();

    if(timeStampRecord.fileName.isEmpty()) {

        //statusLabel->setText("Nhập tiêu đề trước khi Trích xuất");
        //fileNameBox->setFocus();

        timeStampRecord.fileName = fileNameBox->placeholderText();

        //return;
    }

    if(timeStampRecord.endTimeStampRecord - timeStampRecord.startTimeStampRecord > 2*86400){
        QMessageBox *messageBoxError = new QMessageBox;
        messageBoxError->setIcon(QMessageBox::Warning);

        messageBoxError->setWindowTitle(tr("Thông báo trích xuất Video"));
        messageBoxError->setText("Thời gian trích xuất tối đa 2 ngày");
        messageBoxError->show();
        QTimer::singleShot(5000, messageBoxError, [messageBoxError]{
            messageBoxError->hide();
            messageBoxError->deleteLater();
        });
    }else{

        if (timeStampRecord.startTimeStampRecord <
                timeStampRecord.endTimeStampRecord) {

            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<TimeStampRecord>(timeStampRecord);
            control()->newUserAction(Message.APP_PLAY_BACK_START_RECORD_QUICK,
                                     dataStruct);
            closeMenuRecord();

        } else {
            QMessageBox *messageBoxError = new QMessageBox;
            messageBoxError->setIcon(QMessageBox::Warning);

            messageBoxError->setWindowTitle(tr("Thông báo trích xuất Video"));
            messageBoxError->setText("Thời gian sau phải lớn hơn thời gian trước");
            messageBoxError->show();
            QTimer::singleShot(5000, messageBoxError, [messageBoxError]{
                messageBoxError->hide();
                messageBoxError->deleteLater();
            });
        }
    }
}


void P_PBRecord::generateLinkMp4Success(){
    startRecordButton->setText("Đang tải xuống");
    processerSocketWidget->hide();
    processerDownloadWidget->show();
}

void P_PBRecord::downloadVideoRecordSuccess() {
    qDebug() << "downloadVideoRecordSuccess => P_PBRecord";

    QString homePath = QDir::homePath();
    QString rootPath = homePath + "/MultimediaOfVideowall/Videos";
    //    QString rootPath ="/mnt/hdd/videowallstores/Videos";

    QMessageBox *messageBoxSuccess = new QMessageBox;
    messageBoxSuccess->setIcon(QMessageBox::Information);
    messageBoxSuccess->setWindowTitle(tr("Thông báo trích xuất Video"));
    messageBoxSuccess->setText("Trích xuất clip thành công ! Lưu tại : " + rootPath);

    cancelCreateVideoButton->setFixedSize(0,0);
    cancelCreateVideoButton->hide();

    int ret = messageBoxSuccess->exec();
    if(ret == QMessageBox::Ok || ret == QMessageBox::Close){
        messageBoxSuccess->hide();
        messageBoxSuccess->deleteLater();
    }
}

void P_PBRecord::cancelSaveVideoRecord() {

    QMessageBox *messageBoxError = new QMessageBox;
    messageBoxError->setIcon(QMessageBox::Warning);
    messageBoxError->setFixedSize(400,150);
    messageBoxError->setWindowTitle(tr("Thông báo trích xuất Video"));
    messageBoxError->setText("Trích xuất clip thất bại");
    messageBoxError->show();
    QTimer::singleShot(5000, messageBoxError, [messageBoxError]{
        messageBoxError->hide();
        messageBoxError->deleteLater();
    });
}

void P_PBRecord::updateProcessBarDownload(int percent) {
    QString danger =
            "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, "
            "y2: 0,stop: 0 #F7F772,stop: 0.4999 #FF0020,stop: 0.5 #FF0019,stop: 1 "
            "#FF0000 );border-bottom-right-radius: 5px;border-bottom-left-radius: "
            "5px;border: .px solid black;}";
    QString safe =
            "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, "
            "y2: 0,stop: 0 #78d,stop: 0.4999 #46a,stop: 0.5 #45a,stop: 1 #238 "
            ");border-bottom-right-radius: 7px;border-bottom-left-radius: "
            "7px;border: 1px solid black;}";
    processBarDownload->setValue(percent);
    if (processBarDownload->value() < 80)
        processBarDownload->setStyleSheet(danger);
    else
        processBarDownload->setStyleSheet(safe);
}


void P_PBRecord::updateProcessBarSocketGenerate(int percent) {
    QString danger =
            "QProgressBar::chunk {background: #8cd85e;border-bottom-right-radius: 5px;border-bottom-left-radius: "
            "5px;border: .px solid black;}";

    processBarSocket->setValue(percent);
    processBarSocket->setStyleSheet(danger);
}

void P_PBRecord::totalSecondRecordIsTooSmall(){
    QMessageBox *messageBoxError = new QMessageBox;
    messageBoxError->setIcon(QMessageBox::Warning);
    messageBoxError->setFixedSize(400,150);
    messageBoxError->setWindowTitle(tr("Thông báo trích xuất Video"));
    messageBoxError->setText("Thời gian trích xuất phải lớn hơn 5s");
    messageBoxError->show();
    QTimer::singleShot(5000, messageBoxError, [messageBoxError]{
        messageBoxError->hide();
        messageBoxError->deleteLater();
    });
}

void P_PBRecord::getClipError() {

    QMessageBox *messageBoxError = new QMessageBox;
    messageBoxError->setFixedSize(400,150);
    messageBoxError->setWindowTitle(tr("Thông báo trích xuất Video"));
    messageBoxError->setText("Không thể trích xuất được");
    messageBoxError->show();
    QTimer::singleShot(5000, messageBoxError, [messageBoxError]{
        messageBoxError->hide();
        messageBoxError->deleteLater();
    });
}

void P_PBRecord::linkRecordNotFound() {
    qDebug() << "linkRecordNotFound";

    QMessageBox *messageBoxError = new QMessageBox;
    messageBoxError->setFixedSize(400,150);
    messageBoxError->setWindowTitle(tr("Thông báo trích xuất Video"));
    messageBoxError->setText("Không thể trích xuất được");
    messageBoxError->show();
    QTimer::singleShot(5000, messageBoxError, [messageBoxError]{
        messageBoxError->hide();
        messageBoxError->deleteLater();
    });
}

void P_PBRecord::setCurrentPlaybackTime(long time){

    currentPlayBackTime = time;
}

void P_PBRecord::openRecordDialog(RecordInformation recordInfo) {

    sourceVideoBox->clear();
    for (int index = 0; index < recordInfo.listNetworkTypeAvailble.size(); ++index) {
        QString networkType = recordInfo.listNetworkTypeAvailble.at(index) == "LAN" ? "NVR" : recordInfo.listNetworkTypeAvailble.at(index);
        sourceVideoBox->addItem(networkType);
    }

    this->zone->setFocusPolicy(Qt::StrongFocus);
    QDateTime startTime, endTime;

    startTime.setTime_t(currentPlayBackTime - mControlTime/2);
    endTime.setTime_t(currentPlayBackTime + mControlTime/2);

    //startCalendar->setCurrentDateTime(startTime);
    startCalendar->setDateTime(startTime);

    //endCalendar->setCurrentDateTime(endTime);
    endCalendar->setDateTime(endTime);

    //Duration label
    durationLabel->setText(QString::number((int)mControlTime/60) + " phút " + QString::number(mControlTime%60) + " giây");

    //Label
    if(!recordInfo.cameraName.isEmpty()) {
        fileNameBox->setPlaceholderText(recordInfo.cameraName);
    }
    fileNameBox->setText("");
    fileNameBox->setFocus();

    //set video quality
    int index = qualityVideoBox->findText(recordInfo.videoQuality);
    qualityVideoBox->setCurrentIndex(index);

    if(sourceVideoBox->count() == 2){
        //set source Video
        if(recordInfo.networkType == "CDN") {
            index = sourceVideoBox->findText(recordInfo.networkType);

        } else {
            //LAN network type
            index = sourceVideoBox->findText("NVR");
        }
        sourceVideoBox->setCurrentIndex(index);
    }else{
        if(sourceVideoBox->count() == 1){
            sourceVideoBox->setCurrentIndex(0); //set default
        }
    }

    //Erase status text
    //statusLabel->setText("");
}

void P_PBRecord::closeMenuRecord() {

    control()->newUserAction(Message.APP_PLAY_BACK_CLOSE_MENU_RECORD_QUICK,
                             Q_NULLPTR);
}

void P_PBRecord::startCalendarUpdateTime(QDateTime time) {

    long currentStartTime = time.toTime_t();
    long currentEndTime = endCalendar->dateTime().toTime_t();

    QDateTime startTime, endTime;

    startTime.setTime_t(currentStartTime);
    endTime.setTime_t(currentEndTime);

    if(currentStartTime > currentEndTime - 60) {

        qDebug()<<"Start time greater than End time";
        //Start time greater than End time
        endTime.setTime_t(currentStartTime + 60);

        //endCalendar->setCurrentDateTime(endTime);//Set for inside calendar
        endCalendar->setDateTime(endTime); //Set for outside calendar

    } else if (currentEndTime - currentStartTime > mControlTime) {

        qDebug()<<"Time duration is greater than 10 minutes";
        //Time duration is greater than 10 minutes
        endTime.setTime_t(currentStartTime + mControlTime);

        //endCalendar->setCurrentDateTime(endTime);//Set for inside calendar
        endCalendar->setDateTime(endTime); //Set for outside calendar

    } else {

        //Time duration is valid, do nothing
    }

    durationLabel->setText(QString::number((endTime.toTime_t() - startTime.toTime_t())/60 ) + " phút "
                           + QString::number((endTime.toTime_t() - startTime.toTime_t())%60) + " giây");
}

void P_PBRecord::endCalendarUpdateTime(QDateTime time) {

    long currentStartTime = startCalendar->dateTime().toTime_t();
    long currentEndTime = time.toTime_t();

    QDateTime startTime, endTime;

    startTime.setTime_t(currentStartTime);
    endTime.setTime_t(currentEndTime);

    if( currentEndTime < currentStartTime + 60 ) {

        //End time is smaller than start time
        startTime.setTime_t(currentEndTime - 60);

        //startCalendar->setCurrentDateTime(startTime); //Set for inside calendar
        startCalendar->setDateTime(startTime); //Set for outside calendar

    } else if ( currentEndTime - currentStartTime > mControlTime ) {

        //Time duration is greater than 10 minutes
        startTime.setTime_t(currentEndTime - mControlTime);

        //startCalendar->setCurrentDateTime(startTime);//Set for inside calendar
        startCalendar->setDateTime(startTime); //Set for outside calendar

    } else {

        //Time duration is valid, do nothing

    }

    durationLabel->setText(QString::number((endTime.toTime_t() - startTime.toTime_t())/60 ) + " phút "
                           + QString::number((endTime.toTime_t() - startTime.toTime_t())%60) + " giây");
}

void P_PBRecord::updateLayoutMode(int message) {

    if(message == Message.ENTER_FULLSCREEN_MODE) {
        this->zone->move(this->zone->pos().x()+ 330, this->zone->pos().y() + 25);
        this->zone->updateGeometry();
    } else {
        this->zone->move(this->zone->pos().x()- 330, this->zone->pos().y() - 25);
        this->zone->updateGeometry();
    }

}
