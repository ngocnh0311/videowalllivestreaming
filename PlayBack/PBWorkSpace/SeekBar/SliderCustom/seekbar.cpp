#include "seekbar.h"

#include <QTime>
#include <QToolTip>
#include <QPainter>
#include <QRect>
#include <QStyle>

SeekBar::SeekBar(QWidget *parent):
    CustomSlider(parent),
    tickReady(false),
    totalTime(0)
{
}

void SeekBar::setTracking(int _totalTime)
{
    if(_totalTime != 0)
    {
        totalTime = _totalTime;
        // now that we've got totalTime, calculate the tick locations
        // we need to do this because totalTime is obtained after the LOADED event is fired--we need totalTime for calculations
        for(auto &tick : ticks)
            tick = ((double)tick/totalTime)*maximum();
        if(ticks.length() > 0)
        {
            tickReady = true; // ticks are ready to be displayed
            repaint(rect());
        }
        setMouseTracking(true);
    }
    else
        setMouseTracking(false);
}

void SeekBar::setTicks(QList<int> values)
{
    ticks = values; // just set the values
    tickReady = false; // ticks need to be converted when totalTime is obtained
}

void SeekBar::mouseMoveEvent(QMouseEvent* event)
{
//    qDebug() << Q_FUNC_INFO << "mouseMoveEvent" << this->value();
    if(totalTime != 0)
    {
        QToolTip::showText(QPoint(event->globalX()-25, mapToGlobal(rect().topLeft()).y()-40),
                           FormatTime(QStyle::sliderValueFromPosition(minimum(), maximum(), event->x(), width()), totalTime),
                           this, rect());
        QPalette palette = QToolTip::palette();
        palette.setColor(QPalette::ToolTipBase,QColor("#ffffdc")); // light grey
        palette.setColor(QPalette::ToolTipText,QColor("#706F6F"));//dark grey for text
        QToolTip::setPalette(palette);
        QToolTip::setFont(Resources::instance().getSmallRegularButtonFont());
    }
    QSlider::mouseMoveEvent(event);
}

void SeekBar::paintEvent(QPaintEvent *event)
{
    CustomSlider::paintEvent(event);
    if(isEnabled() && tickReady)
    {
        QRect region = event->rect();
        QPainter painter(this);
        painter.setPen(QColor(190,190,190));
        for(auto &tick : ticks)
        {
            int x = QStyle::sliderPositionFromValue(minimum(), maximum(), tick, width());
            painter.drawLine(x, region.top(), x, region.bottom());
        }
    }
}


QString SeekBar::FormatTime(int _time, int _totalTime)
{
    QTime time = QTime::fromMSecsSinceStartOfDay(_time * 1000);
    if(_time >= 3600) // hours
        return time.toString("hh:mm:ss");
    if(_totalTime >= 60)   // minutes
        return time.toString("mm:ss");
    return time.toString("0:ss");   // seconds
}
