#include "p_pb_seekbar.h"

/**
     * Generic method to override for updating the presention.
     **/
P_PBSeekBar::P_PBSeekBar(Control *ctrl, QWidget *zone) : Presentation(ctrl) {
    // init gui object
    this->zone = zone;
    this->zone->setStyleSheet("background-color: #ecf0f1");

    waitingTimer = new QTimer;
    // init gui object
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->setAlignment(Qt::AlignCenter);
    this->zone->setLayout(layout);
    mBottomLeftWidget = new QWidget(this->zone);
    mBottomRightWidget = new QWidget(this->zone);
    mCenterWidget = new QWidget(this->zone);
    mCenterWidget->setFixedWidth(150);

    layout->addWidget(mBottomLeftWidget);
    layout->addWidget(mCenterWidget);
    layout->addWidget(mBottomRightWidget);


    initLeft();
    initTimeCurrentCenter();
    initRight();
    LayoutSet *layoutSet = new LayoutSet(Q_NULLPTR);

    int defaultLayoutIndex = 1;
    int defaultPageIndex = 1;
    int selectedLayoutSaved = -1;
    int selectedPageSaved = -1;
    int numberLayout = 4;

    QSettings settings;
    settings.beginGroup(QString::number(this->control()->getAppContext()->getWorkingUser()->getUserId()));
    //    qDebug() << "SEEK BAR" << QString::number(this->control()->getAppContext()->getWorkingUser()->getUserId());
    settings.beginGroup("videowall");
    selectedLayoutSaved = settings.value("selected_layout").toInt();
    selectedPageSaved = settings.value("selected_page").toInt();

    //    qDebug() << "SEEK BARS SETTINGS USER READ" << selectedLayoutSaved << selectedPageSaved;

    //select layout
    if(selectedLayoutSaved >= 0 && defaultLayoutIndex <= 4){
        defaultLayoutIndex = selectedLayoutSaved;
        layoutCurrent = layoutSet->layoutList.at(defaultLayoutIndex).numberOfCameras;
    }

    mVersion = AppProfile::getAppProfile()->getAppConfig()->getApiVodVersion();
}

void P_PBSeekBar::update() {}

QObject *P_PBSeekBar::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}


void P_PBSeekBar::initTimeCurrentCenter(){
    mCenterHBoxLayout = new QHBoxLayout();
    mCenterHBoxLayout->setSpacing(5);
    mCenterWidget->setLayout(mCenterHBoxLayout);
    //    currentTimeString =
    //            QDateTime::fromSecsSinceEpoch(currentTimestamp)
    //            .toString("hh:mm:ss");
    timeCurrentLablel = new QLabel(mCenterWidget);
    timeCurrentLablel->setFont(Resources::instance().getSmallRegularButtonFont());

    timeCurrentLablel->setStyleSheet("color:#111");
    timeCurrentLablel->setText(QDateTime::fromTime_t(currentValueSlider).toUTC().toString("hh:mm:ss"));
    QLabel *flagLable = new QLabel(mCenterWidget);
    flagLable->setFont(Resources::instance().getSmallRegularButtonFont());
    flagLable->setText("/");
    flagLable->setStyleSheet("color:#111");

    QLabel *timeDefaultLablel = new QLabel(mCenterWidget);
    timeDefaultLablel->setFont(Resources::instance().getSmallRegularButtonFont());
    timeDefaultLablel->setStyleSheet("color:#111");
    timeDefaultLablel->setText("01:00:00");
    mCenterHBoxLayout->addWidget(timeCurrentLablel);
    mCenterHBoxLayout->addWidget(flagLable);
    mCenterHBoxLayout->addWidget(timeDefaultLablel);
}
void P_PBSeekBar::initRight() {
    mBottomRightLayout = new QHBoxLayout();
    mBottomRightLayout->setContentsMargins(10, 0, 10, 0);
    mBottomRightLayout->setSpacing(0);
    mBottomRightLayout->setAlignment(Qt::AlignCenter);
    mBottomRightWidget->setLayout(mBottomRightLayout);
    initBotBackWard();
}

// khi click vao
void P_PBSeekBar::onPressSlider() {
    mIsSliderPressed = 1;  // neu click thi danh dau no da duoc click
}

void P_PBSeekBar::initLeft() {
    // init bottom left layout
    mBottomLeftLayout = new QVBoxLayout();
    mBottomLeftLayout->setContentsMargins(10, 0, 10, 0);
    mBottomLeftLayout->setSpacing(10);
    mBottomLeftLayout->setAlignment(Qt::AlignCenter);
    mBottomLeftWidget->setLayout(mBottomLeftLayout);

    // init slider
    mVideoSlider = new SeekBar(mBottomLeftWidget);
    mVideoSlider->setFont(Resources::instance().getSmallRegularButtonFont());
    mVideoSlider->setCursor(Qt::PointingHandCursor);
    mVideoSlider->setTracking(mLengthDefaultOfVideo);
    QString styleSheetSeekBar = "QSlider {height: 9px;}QSlider::handle:horizontal:disabled,QSlider::sub-page:horizontal:disabled {	background: #6d6d6c;}QSlider::handle:horizontal {	/* handle color */	background: #cbcaca;	width: 9px;	border-radius: 0px;}QSlider::groove:horizontal {	/* the non-progressed color */	background: #6d6d6c;}QSlider::sub-page:horizontal {	/* the progress color */	background: #297fce;}";
    mVideoSlider->setStyleSheet(styleSheetSeekBar);
    mVideoSlider->installEventFilter(this);
    mVideoSlider->setRange(0, mLengthDefaultOfVideo);
    mVideoSlider->setOrientation(Qt::Horizontal);
    mBottomLeftLayout->addWidget(mVideoSlider);
    connect(mVideoSlider, &QSlider::valueChanged, this,
            &P_PBSeekBar::sliderChangeValue);
    connect(mVideoSlider, &QSlider::sliderReleased, this,
            &P_PBSeekBar::onReleaseSlider);
    connect(mVideoSlider, &QSlider::sliderMoved, this,&P_PBSeekBar::onMoveSlider);

    connect(mVideoSlider, &QSlider::sliderPressed, this,
            &P_PBSeekBar::onPressSlider);
    mUpdateSliderTimer = new QTimer(this);
    connect(mUpdateSliderTimer, &QTimer::timeout, this,
            &P_PBSeekBar::updateValueSlider);
}
void P_PBSeekBar::initBotBackWard() {
    QWidget *mWidgetBackWard = new QWidget(mBottomRightWidget);
    QHBoxLayout *mLayoutBackWard = new QHBoxLayout();
    mLayoutBackWard->setSpacing(1);
    mLayoutBackWard->setContentsMargins(0, 0, 10, 0);
    mWidgetBackWard->setLayout(mLayoutBackWard);

    mBackWardButton = new QPushButton(mWidgetBackWard);
    mBackWardButton->setFont(Resources::instance().getSmallRegularButtonFont());
    mBackWardButton->setText(iconButtonTops.backward);
    mBackWardButton->setStyleSheet(
                "background-color: #6d6d6c; color: #BEBEBE; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 5px; "
                "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                "border-bottom-left-radius: 5px;");
    mBackWardButton->setFixedSize(40, 30);
    mBackWardButton->setEnabled(false);
    connect(mBackWardButton, &QPushButton::clicked, this,
            &P_PBSeekBar::selectedBackWard);

    mForWardButton = new QPushButton(mWidgetBackWard);
    mForWardButton->setFont(Resources::instance().getSmallRegularButtonFont());

    mForWardButton->setText(iconButtonTops.forward);
    mForWardButton->setStyleSheet(
                "background-color: #6d6d6c; color: #f7f7f7; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 0px; "
                "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                "border-bottom-left-radius: 0px;");
    mForWardButton->setFixedSize(40, 30);
    connect(mForWardButton, &QPushButton::clicked, this,
            &P_PBSeekBar::selectedForWard);

    mPauseButton = new QPushButton(mWidgetBackWard);
    mPauseButton->setFont(Resources::instance().getSmallRegularButtonFont());
    mPauseButton->setStyleSheet(
                "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 0px; "
                "border-top-right-radius: 0x; border-bottom-right-radius: 0px; "
                "border-bottom-left-radius:0px;");
    mPauseButton->setText(iconButtonTops.pause);
    mPauseButton->setFixedSize(40, 30);
    connect(mPauseButton, &QPushButton::clicked, this,
            &P_PBSeekBar::pauseVideoVOD);

    mLayoutBackWard->addWidget(mBackWardButton);
    mLayoutBackWard->addWidget(mPauseButton);
    mLayoutBackWard->addWidget(mForWardButton);

    QWidget *mWidgetBackWardButton = new QWidget(mBottomRightWidget);
    QHBoxLayout *mLayoutBackWardButton = new QHBoxLayout();
    mWidgetBackWardButton->setLayout(mLayoutBackWardButton);
    mLayoutBackWardButton->setAlignment(Qt::AlignHCenter);
    mLayoutBackWardButton->setSpacing(1);
    mLayoutBackWardButton->setMargin(0);

    mForwardByFrameButton = new QPushButton(mBottomRightWidget);
    mForwardByFrameButton->setFont(Resources::instance().getSmallRegularButtonFont());

    QPixmap pixmapbyframe(":/images/res/iconbyframe.png");
    QIcon iconbyframe(pixmapbyframe);
    mForwardByFrameButton->setIcon(iconbyframe);

    mForwardByFrameButton->setStyleSheet(
                "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 5px; "
                "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                "border-bottom-left-radius: 5px;");

    mForwardByFrameButton->setFixedSize(40, 30);
    connect(mForwardByFrameButton, &QPushButton::clicked, this,
            &P_PBSeekBar::forwardByFrame);

    mForWard025xButton = new QPushButton(mWidgetBackWardButton);
    mForWard025xButton->setFont(Resources::instance().getSmallRegularButtonFont());

    QPixmap pixmap025x(":/images/res/icon025x.png");
    QIcon icon025x(pixmap025x);
    mForWard025xButton->setIcon(icon025x);

    mForWard025xButton->setStyleSheet(
                "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 0px; "
                "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                "border-bottom-left-radius: 0px;");
    mForWard025xButton->setFixedSize(40, 30);
    connect(mForWard025xButton, &QPushButton::clicked, this,
            &P_PBSeekBar::forWardSpeed025x);

    mForWard05xButton = new QPushButton(mWidgetBackWardButton);
    mForWard05xButton->setFont(Resources::instance().getSmallRegularButtonFont());

    QPixmap pixmap05x(":/images/res/icon05x.png");
    QIcon icon05x(pixmap05x);
    mForWard05xButton->setIcon(icon05x);
    connect(mForWard05xButton, &QPushButton::clicked, this,
            &P_PBSeekBar::forWardSpeed05x);

    //    mForWard05xButton->setText("0.5x");

    mForWard05xButton->setStyleSheet(
                "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 0px; "
                "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                "border-bottom-left-radius: 0px;");
    mForWard05xButton->setFixedSize(40, 30);

    mForWard1xButton = new QPushButton(mWidgetBackWardButton);
    mForWard1xButton->setFont(Resources::instance().getSmallRegularButtonFont());
    connect(mForWard1xButton, &QPushButton::clicked, this,
            &P_PBSeekBar::forWardSpeed1x);
    mForWard1xButton->setText("1x");
    // selected
    mForWard1xButton->setStyleSheet(
                "background-color: #6d6d6c; color: #f7f7f7; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 0px; "
                "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                "border-bottom-left-radius: 0px;");
    mForWard1xButton->setFixedSize(40, 30);

    mForWard2xButton = new QPushButton(mWidgetBackWardButton);
    mForWard2xButton->setFont(Resources::instance().getSmallRegularButtonFont());

    connect(mForWard2xButton, &QPushButton::clicked, this,
            &P_PBSeekBar::forWardSpeed2x);

    mForWard2xButton->setText("2x");

    mForWard2xButton->setStyleSheet(
                "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 0px; "
                "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                "border-bottom-left-radius: 0px;");
    mForWard2xButton->setFixedSize(40, 30);

    mForWard4xButton = new QPushButton(mWidgetBackWardButton);
    mForWard4xButton->setFont(Resources::instance().getSmallRegularButtonFont());

    connect(mForWard4xButton, &QPushButton::clicked, this,
            &P_PBSeekBar::forWardSpeed4x);

    mForWard4xButton->setText("4x");
    mForWard4xButton->setStyleSheet(
                "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 0px; "
                "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                "border-bottom-left-radius: 0px;");
    mForWard4xButton->setFixedSize(40, 30);

    mForWard8xButton = new QPushButton(mWidgetBackWardButton);
    mForWard8xButton->setFont(Resources::instance().getSmallRegularButtonFont());

    connect(mForWard8xButton, &QPushButton::clicked, this,
            &P_PBSeekBar::forWardSpeed8x);
    mForWard8xButton->setText("8x");
    mForWard8xButton->setStyleSheet(
                "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 0px; "
                "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                "border-bottom-left-radius: 0px;");
    mForWard8xButton->setFixedSize(40, 30);

    mLayoutBackWardButton->addWidget(mForwardByFrameButton);
    mLayoutBackWardButton->addWidget(mForWard025xButton);
    mLayoutBackWardButton->addWidget(mForWard05xButton);
    mLayoutBackWardButton->addWidget(mForWard1xButton);
    mLayoutBackWardButton->addWidget(mForWard2xButton);
    mLayoutBackWardButton->addWidget(mForWard4xButton);
    mLayoutBackWardButton->addWidget(mForWard8xButton);
    //    mLayoutBackWardButton->addWidget(mForWard16xButton);

    mBottomRightLayout->addWidget(mWidgetBackWard);
    mBottomRightLayout->addWidget(mWidgetBackWardButton);
}

void P_PBSeekBar::sliderChangeValue(int value) {
    currentValueSlider = value;
}

void P_PBSeekBar::onMoveSlider(int value){
    qDebug() << "Value onMoveSlider" << value;

}


void P_PBSeekBar::onReleaseSlider() {
    if(currentValueSlider > 3600){
        currentValueSlider = 3600;
    }
    TimeRange tmpTimeRangeRequest;
    tmpTimeRangeRequest.starttime = mTimeStampSeekNewPositon + currentValueSlider;
    tmpTimeRangeRequest.length = mLengthDefaultOfVideo - currentValueSlider;
    qDebug() << "USER RELEASE" << "start time" <<  tmpTimeRangeRequest.starttime << "length" <<  tmpTimeRangeRequest.length;
    setTimeRangeRequestVOD(tmpTimeRangeRequest);
    playListVideoVOD(tmpTimeRangeRequest);
}

void P_PBSeekBar::setTimeRangeRequestVOD(TimeRange timeRange) {
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<TimeRange>(timeRange);
    control()->newUserAction(Message.APP_PLAY_BACK_UPDATE_TIME_STAMP_CURRENT,
                             dataStruct);
}

// moi giay cap nhat lai gia tri cho slider
void P_PBSeekBar::updateValueSlider() {
    if(control()->appContext->getSiteCameras()->getCamItems().size() == 0) return;
    if(isVideoPaused) return;
    currentValueSlider += mSpeedAllVideo;
    if(currentValueSlider > 3600){
        currentValueSlider = 3600;
    }
    mVideoSlider->setValue(currentValueSlider);
    timeCurrentLablel->setText(QDateTime::fromTime_t(currentValueSlider).toUTC().toString("hh:mm:ss"));
    TimeRange tmpTimeRange;
    tmpTimeRange.starttime = mTimeStampSeekNewPositon + currentValueSlider;
    tmpTimeRange.length = mLengthDefaultOfVideo - currentValueSlider;
    //    qDebug() << "updateValueSlider" << "start time" <<  tmpTimeRange.starttime << "length" <<  tmpTimeRange.length;
    setTimeRangeRequestVOD(tmpTimeRange);
}
void P_PBSeekBar::playListVideoVOD(TimeRange timeRange) {
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<TimeRange>(timeRange);
    control()->newUserAction(Message.APP_PLAY_BACK_PLAY_LIST_VIDEO_VOD,
                             dataStruct);
}
void P_PBSeekBar::playOneVideoVOD(TimeRange timeRange) {
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<TimeRange>(timeRange);
    control()->newUserAction(Message.APP_PLAY_BACK_PLAY_ONE_VIDEO_VOD,
                             dataStruct);
}

void P_PBSeekBar::seekToNewPostion(long newPostion) {
    unpausePlayer();
    mVideoSlider->setValue(0);
    mTimeStampSeekNewPositon = newPostion;
    qDebug() << "seekToNewPostion" << mTimeStampSeekNewPositon;
    mLastTimeStampSelect = newPostion;
}

void P_PBSeekBar::forwardByFrame() {
    setBackgroundPlayBack(true, false, false, false, false, false, false, false);
    mSpeedAllVideo = 0.1f;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<double>(mSpeedAllVideo);
    changedSpeedAllVideo(dataStruct);
}
void P_PBSeekBar::forWardSpeed025x() {
    setBackgroundPlayBack(false, true, false, false, false, false, false, false);
    mSpeedAllVideo = 0.25f;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<double>(mSpeedAllVideo);
    changedSpeedAllVideo(dataStruct);
}
void P_PBSeekBar::forWardSpeed05x() {
    setBackgroundPlayBack(false, false, true, false, false, false, false, false);
    mSpeedAllVideo = 0.5f;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<double>(mSpeedAllVideo);
    changedSpeedAllVideo(dataStruct);
}
void P_PBSeekBar::forWardSpeed1x() {
    setBackgroundPlayBack(false, false, false, true, false, false, false, false);
    mSpeedAllVideo = 1.0f;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<double>(mSpeedAllVideo);
    changedSpeedAllVideo(dataStruct);
}
void P_PBSeekBar::forWardSpeed2x() {
    setBackgroundPlayBack(false, false, false, false, true, false, false, false);
    mSpeedAllVideo = 2.0f;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<double>(mSpeedAllVideo);
    changedSpeedAllVideo(dataStruct);
}
void P_PBSeekBar::forWardSpeed4x() {
    setBackgroundPlayBack(false, false, false, false, false, true, false, false);
    mSpeedAllVideo = 4.0f;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<double>(mSpeedAllVideo);
    changedSpeedAllVideo(dataStruct);
}
void P_PBSeekBar::forWardSpeed8x() {
    setBackgroundPlayBack(false, false, false, false, false, false, true, false);
    mSpeedAllVideo = 8.0f;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<double>(mSpeedAllVideo);
    changedSpeedAllVideo(dataStruct);
}
void P_PBSeekBar::selectedBackWard() {
    mBackWardButton->setStyleSheet(
                "background-color: #6d6d6c; color: #BEBEBE; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 5px; "
                "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                "border-bottom-left-radius: 5px;");
}
void P_PBSeekBar::selectedForWard() {
    mForWardButton->setStyleSheet(
                "background-color: #6d6d6c; color: #f7f7f7; border-width: 0px;  "
                "border-style: solid; border-top-left-radius: 0px; "
                "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                "border-bottom-left-radius: 0px;");
}

void P_PBSeekBar::setBackgroundPlayBack(bool isByFrame, bool is025x, bool is05x,
                                        bool is1x, bool is2x, bool is4x,
                                        bool is8x, bool is16x) {
    if (isByFrame) {
        //        mForwardByFrameButton->setStyleSheet("background-color: #6d6d6c;
        //        color: #f7f7f7;");
        QPixmap pixmapbyframe(":/images/res/iconbyframeselected.png");
        QIcon iconbyframe(pixmapbyframe);
        mForwardByFrameButton->setIcon(iconbyframe);
    } else {
        //        mForwardByFrameButton->setStyleSheet("background-color: #f7f7f7;
        //        color: #6d6d6c;");
        QPixmap pixmapbyframe(":/images/res/iconbyframe.png");
        QIcon iconbyframe(pixmapbyframe);
        mForwardByFrameButton->setIcon(iconbyframe);
    }

    if (is025x) {
        QPixmap pixmap025x(":/images/res/icon025xselected.png");
        QIcon icon025x(pixmap025x);
        mForWard025xButton->setIcon(icon025x);
        //        mForWard025xButton->setStyleSheet("background-color: #6d6d6c;
        //        color: #f7f7f7; border-width: 0px;  border-style: solid;
        //        border-top-left-radius: 0px; border-top-right-radius: 0px;
        //        border-bottom-right-radius: 0px; border-bottom-left-radius:
        //        0px;");
    } else {
        QPixmap pixmap025x(":/images/res/icon025x.png");
        QIcon icon025x(pixmap025x);
        mForWard025xButton->setIcon(icon025x);
        //        mForWard025xButton->setStyleSheet("background-color: #f7f7f7;
        //        color: #6d6d6c; border-width: 0px;  border-style: solid;
        //        border-top-left-radius: 0px; border-top-right-radius: 0px;
        //        border-bottom-right-radius: 0px; border-bottom-left-radius:
        //        0px;");
    }
    if (is05x) {
        QPixmap pixmap05x(":/images/res/icon05xselected.png");
        QIcon icon05x(pixmap05x);
        mForWard05xButton->setIcon(icon05x);
        //        mForWard05xButton->setStyleSheet("background-color: #6d6d6c;
        //        color: #f7f7f7; border-width: 0px;  border-style: solid;
        //        border-top-left-radius: 0px; border-top-right-radius: 0px;
        //        border-bottom-right-radius: 0px; border-bottom-left-radius:
        //        0px;");
    } else {
        QPixmap pixmap05x(":/images/res/icon05x.png");
        QIcon icon05x(pixmap05x);
        mForWard05xButton->setIcon(icon05x);
        // mForWard05xButton->setStyleSheet("background-color: #f7f7f7; color:
        // #6d6d6c; border-width: 0px;  border-style: solid; border-top-left-radius:
        // 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px;
        // border-bottom-left-radius: 0px;");
    }

    if (is1x) {
        mForWard1xButton->setStyleSheet(
                    "background-color: #6d6d6c; color: #f7f7f7; border-width: 0px;  "
                    "border-style: solid; border-top-left-radius: 0px; "
                    "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                    "border-bottom-left-radius: 0px;");
    } else {
        mForWard1xButton->setStyleSheet(
                    "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                    "border-style: solid; border-top-left-radius: 0px; "
                    "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                    "border-bottom-left-radius: 0px;");
    }
    if (is2x) {
        mForWard2xButton->setStyleSheet(
                    "background-color: #6d6d6c; color: #f7f7f7; border-width: 0px;  "
                    "border-style: solid; border-top-left-radius: 0px; "
                    "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                    "border-bottom-left-radius: 0px;");
    } else {
        mForWard2xButton->setStyleSheet(
                    "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                    "border-style: solid; border-top-left-radius: 0px; "
                    "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                    "border-bottom-left-radius: 0px;");
    }
    if (is4x) {
        mForWard4xButton->setStyleSheet(
                    "background-color: #6d6d6c; color: #f7f7f7; border-width: 0px;  "
                    "border-style: solid; border-top-left-radius: 0px; "
                    "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                    "border-bottom-left-radius: 0px;");
    } else {
        mForWard4xButton->setStyleSheet(
                    "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                    "border-style: solid; border-top-left-radius: 0px; "
                    "border-top-right-radius: 0px; border-bottom-right-radius: 0px; "
                    "border-bottom-left-radius: 0px;");
    }

    if (mIsShowOneVideo == 0) {

        if (layoutCurrent < 16) {
            mForWard8xButton->setDisabled(false);
            if (is8x) {
                mForWard8xButton->setStyleSheet(
                            "background-color: #6d6d6c; color: #f7f7f7; border-width: 0px;  "
                            "border-style: solid; border-top-left-radius: 0px; "
                            "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                            "border-bottom-left-radius: 0px;");
            } else {
                mForWard8xButton->setStyleSheet(
                            "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                            "border-style: solid; border-top-left-radius: 0px; "
                            "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                            "border-bottom-left-radius: 0px;");
            }
        } else {
            mForWard8xButton->setDisabled(true);
            mForWard8xButton->setStyleSheet(
                        "background-color: #a1a1a1; color: #8e8e8e; border-width: 0px;  "
                        "border-style: solid; border-top-left-radius: 0px; "
                        "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                        "border-bottom-left-radius: 0px;");
        }

    } else {
        if (is8x) {
            //enable and seleceted
            mForWard8xButton->setStyleSheet(
                        "background-color: #6d6d6c; color: #f7f7f7; border-width: 0px;  "
                        "border-style: solid; border-top-left-radius: 0px; "
                        "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                        "border-bottom-left-radius: 0px;");
        } else {
            //enable not selected
            mForWard8xButton->setStyleSheet(
                        "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                        "border-style: solid; border-top-left-radius: 0px; "
                        "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                        "border-bottom-left-radius: 0px;");
        }
    }
}

void P_PBSeekBar::pauseVideoVOD() {
    if(!isVideoPaused){//if playing
        isVideoPaused = true;
        mPauseButton->setText(iconButtonTops.play);
        control()->newUserAction(Message.PLAYER_PLAYBACK_PAUSED, Q_NULLPTR);
    }else{
        //if paused
        isVideoPaused = false;
        mPauseButton->setText(iconButtonTops.pause);
        control()->newUserAction(Message.PLAYER_PLAYBACK_UNPAUSED, Q_NULLPTR);
    }
}

void P_PBSeekBar::changedSpeedAllVideo(QVariant *dataStruct) {
    control()->newUserAction(Message.APP_PLAY_BACK_CHANGE_SPEED_ALL_VIDEO,
                             dataStruct);
    isVideoPaused = false;
    mPauseButton->setText(iconButtonTops.pause);
}

void P_PBSeekBar::resetPlayerSpeed() {
    unpausePlayer();
    mSpeedAllVideo = 1.0f;
    setBackgroundPlayBack(false, false, false, true, false, false, false, false);
}

void P_PBSeekBar::resetValueSeekBar() {
    mVideoSlider->setValue(0);
}
void P_PBSeekBar::resetValueWhenAppShow(){
    if(!mUpdateSliderTimer->isActive()){
        mUpdateSliderTimer->start(1000);
        mVideoSlider->setValue(0);
    }
}
void P_PBSeekBar::stopWhenChangeApp(){
    isVideoPaused = false;
    mPauseButton->setText(iconButtonTops.pause);
    if(mUpdateSliderTimer->isActive()){
        mUpdateSliderTimer->stop();
        mVideoSlider->setValue(0);
        TimeRange timeRangeLast;
        timeRangeLast.starttime = mLastTimeStampSelect;
        timeRangeLast.length = 3600;
        setTimeRangeRequestVOD(timeRangeLast);
    }
}

void P_PBSeekBar::disableSpeedButton(LayoutStruct layout) {
    mIsShowOneVideo = 0;
    layoutCurrent = layout.numberOfCameras;
}

void P_PBSeekBar::enableSpeedButton() {
    unpausePlayer();
    mIsShowOneVideo = 1;
    mForWard8xButton->setEnabled(true);
    if(layoutCurrent >= 16){
        mForWard8xButton->setStyleSheet(
                    "background-color: #f7f7f7; color: #6d6d6c; border-width: 0px;  "
                    "border-style: solid; border-top-left-radius: 0px; "
                    "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                    "border-bottom-left-radius: 0px;");
    }
}

void P_PBSeekBar::disableSpeedButton() {
    mIsShowOneVideo = 0;
    mForWard8xButton->setDisabled(true);
    if(layoutCurrent >= 16){
        mForWard8xButton->setStyleSheet(
                    "background-color: gray; color: #fff; border-width: 0px;  "
                    "border-style: solid; border-top-left-radius: 0px; "
                    "border-top-right-radius: 5px; border-bottom-right-radius: 5px; "
                    "border-bottom-left-radius: 0px;");
        resetPlayerSpeed();
    }
}

void P_PBSeekBar::unpausePlayer(){
    isVideoPaused = false;
    mPauseButton->setText(iconButtonTops.pause);
}

void P_PBSeekBar::startWaiting() {
    //isWaiting = true
    disconnect(waitingTimer, &QTimer::timeout, this,
              &P_PBSeekBar::onEndWaiting);
    waitingTimer->stop();
    //start timer
    connect(waitingTimer, &QTimer::timeout, this,
            &P_PBSeekBar::onEndWaiting);
    waitingTimer->setSingleShot(true);
    waitingTimer->start(500);
}

void P_PBSeekBar::onEndWaiting(){
    qDebug() << Q_FUNC_INFO;
    this->onReleaseSlider();
}

bool P_PBSeekBar::eventFilter(QObject *watched, QEvent *event){
    QSlider *slider = qobject_cast<QSlider *>(watched);
    if(slider == Q_NULLPTR){
        return P_PBSeekBar::eventFilter(watched, event);
    }
    if(slider == mVideoSlider && slider->isEnabled()){
        if(event->type() == QEvent::Wheel){
            this->startWaiting();
            return false;
        }
        if(event->type() == QEvent::MouseButtonPress){
            this->startWaiting();
            return false;
        }
    }
    return false;
}

