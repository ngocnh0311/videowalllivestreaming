#include "easyhandle.h"

extern "C" {
int progress_callback(void *clientp,   double dltotal,   double dlnow,   double ultotal,   double ulnow) {
    EasyHandlePri *privHandle = (EasyHandlePri *) clientp;
    if (privHandle->getIsAbortRequested()) {
        return -1;
    }
}

size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata) {
    EasyHandlePri *privHandle = (EasyHandlePri *) userdata;
    if (privHandle->getIsAbortRequested()) {
        return -1;
    }

    long realsize = size * nmemb;
    privHandle->appendReceivedData(QByteArray(ptr, realsize), realsize);

    return realsize;
}
}

EasyHandlePri::EasyHandlePri() {
    doDebug = false;

    handle = curl_easy_init();
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_callback);
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, this);
    curl_easy_setopt(handle, CURLOPT_PROGRESSFUNCTION, progress_callback);
    curl_easy_setopt(handle, CURLOPT_PROGRESSDATA, this);
}

EasyHandlePri::~EasyHandlePri() {
    curl_easy_cleanup(handle);
}

void EasyHandlePri::onGet(QString url, long timeoutMsec, long retry) {
    receivedData.clear();

    curl_easy_setopt(handle, CURLOPT_URL, (const char *)url.toLatin1().data());
    if (timeoutMsec != -1)
        curl_easy_setopt(handle, CURLOPT_TIMEOUT_MS, timeoutMsec);

    CURLcode curlRet = curl_easy_perform(handle);

    if (isAbortRequested) {
        Q_EMIT aborted();
        return;
    }

    long httpCode;
    curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &httpCode);

    if (CURLE_OK == curlRet && receivedData.size() > 0) {
        if (doDebug)
            qDebug() << "+++okok" << url << "httpCode" << httpCode << receivedData.size();

        curl_easy_setopt(handle, CURLOPT_FRESH_CONNECT, 0);

        Q_EMIT finished(httpCode, receivedData);
    } else {
        if (retry > 0) {
            if (doDebug)
                qDebug() << "---retry"  << url << "httpCode" << httpCode << "curlRet" << curlRet << curl_easy_strerror(curlRet) << "retryTime" << retry;

            curl_easy_setopt(handle, CURLOPT_FRESH_CONNECT, 1L);
            onGet(url, timeoutMsec, --retry);
        } else {
            if (doDebug)
                qDebug() << "---failed" << url << "httpCode" << httpCode << "curlRet" << curlRet << curl_easy_strerror(curlRet);

            Q_EMIT finished(httpCode, QByteArray());
        }
    }    
}

long EasyHandlePri::appendReceivedData(QByteArray payload, long size) {
    receivedData += payload;
    return size;
}

bool EasyHandlePri::getIsAbortRequested() const
{
    return isAbortRequested;
}

void EasyHandlePri::setIsAbortRequested(bool value)
{
    isAbortRequested = value;
}

//===
long EasyHandle::instanceCount = 0;
EasyHandle::EasyHandle()
{
    handlepri.moveToThread(&thread);
    thread.setObjectName("CurlEasyHandle" + QString::number(instanceCount++));
    thread.start();

    connect(&handlepri, &EasyHandlePri::finished, this, &EasyHandle::finished);
    connect(&handlepri, &EasyHandlePri::aborted, this, &EasyHandle::aborted);
}

EasyHandle::~EasyHandle() {
    qDebug() << Q_FUNC_INFO << QThread::currentThreadId();
}

void EasyHandle::get(QString url) {
    get(url, -1, -1);
}

void EasyHandle::get(QString url, long timeoutMsec, long retry) {
    if (retry > MAX_RETRY)
        retry = MAX_RETRY;

    handlepri.setIsAbortRequested(false);
    QMetaObject::invokeMethod(&handlepri, "onGet",
                              Q_ARG(QString, url),
                              Q_ARG(long, timeoutMsec),
                              Q_ARG(long, retry));
}

void EasyHandle::onDownloadProgress(long receivedBytes) {
    qDebug() << Q_FUNC_INFO << receivedBytes;
}

void EasyHandle::abort() {
    handlepri.setIsAbortRequested(true);
}
