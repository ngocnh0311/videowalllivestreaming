#include "clipperworker.h"

ClipperWorker::ClipperWorker()
{
    doDebug = true;

    workerProcess = Q_NULLPTR;
    downloader = Q_NULLPTR;
    easyHandle = Q_NULLPTR;
    m3u8Parser = Q_NULLPTR;

    nInitedBatchWorker = 0;

    MAX_BATCH_WORKER = 5;
    SEGMENT_PER_BATCH = 200;
}

void ClipperWorker::onInitialize() {
    if (workerProcess == Q_NULLPTR) {
        workerProcess = new QProcess(this);
        connect(this, &ClipperWorker::killProcess, workerProcess, &QProcess::kill);
    }

    if (downloader == Q_NULLPTR) {
        downloader = new Downloader;
        connect(downloader, &Downloader::downloadedPayload, this, &ClipperWorker::onDownloadedPayload);
    }

    if (easyHandle == Q_NULLPTR) {
        easyHandle = new EasyHandle;
        connect(easyHandle, &EasyHandle::finished, this, &ClipperWorker::onCurlEasyHandleFinished);
    }

    if (m3u8Parser == Q_NULLPTR) {
        m3u8Parser = new M3U8Parser;
    }

    for (int i = 0; i < MAX_BATCH_WORKER; i++) {
        BatchWorker *batchWorker = new BatchWorker(i);
        freeBatchWorkers.append(batchWorker);

        QThread *batchWorkerThread = new QThread;
        batchWorkerThreads.append(batchWorkerThread);

        batchWorker->moveToThread(batchWorkerThread);
        batchWorkerThread->start();

        connect(batchWorker, &BatchWorker::inited, this, &ClipperWorker::onBatchWokerInited);
        connect(batchWorker, &BatchWorker::downloadProgress, this, &ClipperWorker::onDownloadProgress);
        connect(batchWorker, &BatchWorker::batchFinished, this, &ClipperWorker::onBatchFinished);
        connect(batchWorker, &BatchWorker::batchCanceled, this, &ClipperWorker::onBatchCanceled);
        QMetaObject::invokeMethod(batchWorker, "onInitialize");
    }
}

void ClipperWorker::onBatchWokerInited() {
    nInitedBatchWorker++;
    if (nInitedBatchWorker == MAX_BATCH_WORKER) {
        Q_EMIT inited();
    }
}

void ClipperWorker::onStartNewCommand(ClipperCommand *command) {
    qDebug() << "\n\n" << Q_FUNC_INFO << command->getOutputFilename();

    setCommand(command);
    isCancelRequested = false;

    updateInfo();

    checkStartTimestampMsec = 0;
    checkEndTimestampMsec = 0;

    segmentCount = 0;
    nProcessedSegment = 0;
    nReceivedMsec = 0;

    lastEmitProcessingPercent = 0;
    lastEmitDataPercent = 0;

//    if (downloader != Q_NULLPTR) {
//        downloader->onDownloadRequest(0, m3u8Url, 10000, 3);
//    }

    if (easyHandle != Q_NULLPTR) {
        easyHandle->get(m3u8Url, 5000, 3);
    }
}

void ClipperWorker::setCommand(ClipperCommand* _command) {
    command = _command;
}

void ClipperWorker::updateInfo() {
    baseUrl = "http://" + command->getHostAddress() + "/rec/hls/";

    QString startTimestampMs = QString::number(command->getStartTimestamp() * 1000);
    QString lengthMs = QString::number((command->getEndTimestamp() - command->getStartTimestamp()) * 1000);
    m3u8Url = baseUrl + command->getChannelName() + "_" + startTimestampMs + "_" + lengthMs + ".m3u8";

    qDebug() << Q_FUNC_INFO << m3u8Url;
}

void ClipperWorker::onCurlEasyHandleFinished(long statusCode, QByteArray payload) {
    qDebug() << Q_FUNC_INFO << statusCode << payload.size();

    if (!payload.isEmpty()) {
        m3u8Parser->parser(payload.data(), payload.size());

        M3UMedia *m3u8Media = m3u8Parser->getM3uMedia();
        if (m3u8Media != Q_NULLPTR) {
            mediaSegments = QList<MediaSegment>::fromStdList(m3u8Media->getSegmentList());
            m3u8Media->resetInternal();

            if (!mediaSegments.isEmpty()) {
                MAX_SEGMENT = mediaSegments.size();
                updateCheckPoint();
            } else {
                doReport(clipperReport.CLIPPER_M3U8_FAILED, Q_NULLPTR);
            }
        }else{
            qDebug() << Q_FUNC_INFO <<"M3UMedia NULL";
            doReport(clipperReport.CLIPPER_M3U8_FAILED, Q_NULLPTR);
        }

        downloadSegments();
    } else {
        doReport(clipperReport.CLIPPER_M3U8_FAILED, Q_NULLPTR);
    }
}

void ClipperWorker::onDownloadedPayload(long key, QByteArray payload) {
    qDebug() << Q_FUNC_INFO << key << payload.size();

    if (!payload.isEmpty()) {
        m3u8Parser->parser(payload.data(), payload.size());

        M3UMedia *m3u8Media = m3u8Parser->getM3uMedia();
        if (m3u8Media != Q_NULLPTR) {
            mediaSegments = QList<MediaSegment>::fromStdList(m3u8Media->getSegmentList());
            m3u8Media->resetInternal();

            if (!mediaSegments.isEmpty()) {
                MAX_SEGMENT = mediaSegments.size();
                updateCheckPoint();
            } else {
                doReport(clipperReport.CLIPPER_M3U8_FAILED, Q_NULLPTR);
            }
        }

        downloadSegments();
    } else {
        doReport(clipperReport.CLIPPER_M3U8_FAILED, Q_NULLPTR);
    }
}

void ClipperWorker::updateCheckPoint() {
    QString firstUrl = QString::fromStdString(mediaSegments.first().getUri());
    QString startNbtpPart = firstUrl.section('.', 0, 0);
    long startBlockTimestamp = startNbtpPart.section('_', 0, 0).toLong();
    long startDts = startNbtpPart.section('_', 2, 2).toLong();
    long startTimestampMs = (startBlockTimestamp * 1000 + startDts * 10);
    long diff = startTimestampMs - command->getStartTimestamp() * 1000;
    if (diff < 3000) {
        checkStartTimestampMsec = startTimestampMs;
    } else {
        checkStartTimestampMsec = command->getStartTimestamp() * 1000;
    }


    QString lastUrl = QString::fromStdString(mediaSegments.last().getUri());
    QString endNbtpPart = lastUrl.section('.', 0, 0);
    long endBlockTimestamp = endNbtpPart.section('_', 3, 3).toLong();
    long endDts = endNbtpPart.section('_', 5, 5).toLong();
    long endTimestampMs = (endBlockTimestamp * 1000 + endDts * 10);
    long endDiff = endTimestampMs - command->getEndTimestamp() * 1000;
    if (endDiff > -3000) {
        checkEndTimestampMsec = endTimestampMs;
    } else {
        checkEndTimestampMsec = command->getEndTimestamp() * 1000;
    }

    qDebug() << Q_FUNC_INFO
             << "command/gotStartTimestamp (" + QString::number(command->getStartTimestamp()) + "," + QString::number(startTimestampMs) + ")"
             << "command/gotEndTimestamp (" + QString::number(command->getEndTimestamp()) + "," + QString::number(endTimestampMs) + ")"
             << "checkStartTimestamp/checkEndTimestamp (" + QString::number(checkStartTimestampMsec) + "," + QString::number(checkEndTimestampMsec) + ")";
}

void ClipperWorker::downloadSegments() {
    while (segmentCount < MAX_SEGMENT && freeBatchWorkers.size() > 0) {
        QList<MediaSegment> batchSegments = mediaSegments.mid(segmentCount, SEGMENT_PER_BATCH);

        long batchIdx = segmentCount / SEGMENT_PER_BATCH;
        QList<long> segmentIdx;
        QList<QString> urls;
        for (int i = 0; i < batchSegments.size(); i++) {
            segmentIdx.append(segmentCount + i);

            MediaSegment mediaSegment = batchSegments.at(i);
            QString segmentUrl = QString::fromStdString(mediaSegment.getUri()).replace(".ts", ".nbtp");
            QString url = baseUrl + segmentUrl;
            urls.append(url);
        }
        segmentCount += batchSegments.size();
        //qDebug() << Q_FUNC_INFO << segmentCount;

        BatchWorker *batchWorker = freeBatchWorkers.takeFirst();
        busyBatchWorkers.append(batchWorker);
        QMetaObject::invokeMethod(batchWorker, "onNewBatch",
                                  Q_ARG(long, batchIdx),
                                  Q_ARG(QList<long>, segmentIdx),
                                  Q_ARG(QList<QString>, urls));
    }
}

void ClipperWorker::onDownloadProgress(long batchIdx, long segmentIdx, bool downloadOk) {
    nProcessedSegment++;
    if (downloadOk) {
        nReceivedMsec += getSegmentMillisecond(segmentIdx);
    }
    double processingPercent = (double)nProcessedSegment / MAX_SEGMENT * 100.0;
    double dataPercent = (double)nReceivedMsec / (checkEndTimestampMsec - checkStartTimestampMsec) * 100.0;

    if (nProcessedSegment == MAX_SEGMENT
            || processingPercent - lastEmitProcessingPercent > 5
            || dataPercent - lastEmitDataPercent > 5) {
        Q_EMIT downloadProgress(command->getCommandId(), processingPercent, dataPercent);
        lastEmitProcessingPercent = processingPercent;
        lastEmitDataPercent = dataPercent;
    }
}

long ClipperWorker::getSegmentMillisecond(long segmentIdx) {
    MediaSegment segment = mediaSegments.at(segmentIdx);
    QString nbtpPart = QString::fromStdString(segment.getUri()).section('.', 0, 0);

    char sep = '_';
    long startBlockTimestamp = nbtpPart.section(sep, 0, 0).toLong();
    long startDts = nbtpPart.section(sep, 2, 2).toLong();
    long endBlockTimestamp = nbtpPart.section(sep, 3, 3).toLong();
    long endDts = nbtpPart.section(sep, 5, 5).toLong();

    return (endBlockTimestamp * 1000 + endDts * 10) - (startBlockTimestamp * 1000 + startDts * 10);
}

void ClipperWorker::onBatchFinished(long batchIdx, long nSegments, QString tempH264, SegmentedRange *batchParts) {
    if (isCancelRequested) return;

    BatchWorker *batchWorker = (BatchWorker *) QObject::sender();
    freeBatchWorkers.append(batchWorker);
    busyBatchWorkers.removeOne(batchWorker);

    downloadSegments();

    if (!tempH264.isEmpty() && batchParts != Q_NULLPTR) {
        qDebug() << "onBatchFinished" << batchWorker->getName() << batchIdx << nSegments << batchParts->toString();
        batchIdx2TmpFile.insert(batchIdx, tempH264);
        batchIdx2Parts.insert(batchIdx, batchParts);
    } else {
        qDebug() << "onBatchFinished" << batchWorker->getName() << batchIdx << nSegments << "no temp file";
    }

//    nProcessedSegment += nSegments;
    if (nProcessedSegment == MAX_SEGMENT) {
        doPostProcessing();
    }
}

void ClipperWorker::doPostProcessing() {
    qDebug() << Q_FUNC_INFO;
    Q_EMIT postProcessing();

    int retCode = convertH264RawToMp4();

    SegmentedRange *parts = joinSegments();

    doReport(retCode, parts);

    doCleanUp();
}

SegmentedRange *ClipperWorker::joinSegments() {
    SegmentedRange *segments = new SegmentedRange;

    if (!batchIdx2Parts.isEmpty()) {
        QMapIterator<long, SegmentedRange *> it(batchIdx2Parts);
        while (it.hasNext()) {
            it.next();

            SegmentedRange* segmentParts = it.value();
            QList<QPair<long, long>> pairs = segmentParts->getSegments();
            delete segmentParts;

            for (int i = 0; i < pairs.size(); i++) {
                QPair<long, long> pair = pairs.at(i);
                for (long j = pair.first; j <= pair.second; j++) {
                    segments->insert(j);
                }
            }
        }
        batchIdx2Parts.clear();
    }

    return segments;
}

int ClipperWorker::convertH264RawToMp4() {
    int retCode = clipperReport.CLIPPER_NODATA;

    QString program="ffmpeg";
    QStringList ffmpegArgs;
    if (!batchIdx2TmpFile.isEmpty()) {
        QString h264Concat = "concat:";

        QMapIterator<long, QString> it(batchIdx2TmpFile);
        while(it.hasNext()) {
            it.next();

            QFile tmpH264(it.value());
            if (tmpH264.exists())
                h264Concat += it.value() + "|";
        }

        qDebug() << h264Concat;

        QString mp4Path = command->getFilePath();
        ffmpegArgs << "-y" << "-loglevel" << "panic" << "-i" << h264Concat << "-codec" << "copy" << mp4Path;
        //        ffmpegArgs << "-y" << "-i" << h264Concat << "-codec" << "copy" << mp4Path;

        workerProcess->start(program, ffmpegArgs);
        bool ret = workerProcess->waitForFinished(30000);
        if (ret) {
            retCode = clipperReport.CLIPPER_FINISHED;
        } else {
            retCode = clipperReport.CLIPPER_CONVERT_MP4_FAILED;
        }
    }

    return retCode;
}

void ClipperWorker::doReport(int statusCode, SegmentedRange *segments) {
    Q_EMIT getClipDone(statusCode, segments);
}

void ClipperWorker::onCancelCommand() {
    qDebug() << "\n\n" << Q_FUNC_INFO;
    isCancelRequested = true;

    if (workerProcess->state() == QProcess::Running) {
        Q_EMIT killProcess();
    } else {
        //downloader->onCancelAllRequests();
        easyHandle->abort();
        if (!busyBatchWorkers.isEmpty()) {
            for (int i = 0; i < busyBatchWorkers.size(); i++) {
                BatchWorker *batchWorker = busyBatchWorkers.at(i);
                QMetaObject::invokeMethod(batchWorker, "onCancelBatch");
            }
        } else {
            doCleanUp();
            Q_EMIT commandCanceled();
        }
    }
}

void ClipperWorker::onBatchCanceled() {
    BatchWorker *batchWorker = (BatchWorker *) QObject::sender();
    busyBatchWorkers.removeOne(batchWorker);
    freeBatchWorkers.append(batchWorker);

    if (busyBatchWorkers.isEmpty()) {
        doCleanUp();
        Q_EMIT commandCanceled();
    }
}

void ClipperWorker::doCleanUp() {
    qDebug() << Q_FUNC_INFO;

    mediaSegments.clear();

    QMapIterator<long, SegmentedRange *> it(batchIdx2Parts);
    while (it.hasNext()) {
        it.next();

        SegmentedRange* batchParts = it.value();
        delete batchParts;
    }
    batchIdx2Parts.clear();

    removeTmpFiles();
    batchIdx2TmpFile.clear();
}

void ClipperWorker::removeTmpFiles() {
    QMapIterator<long, QString> batchIdx2TmpFileIt(batchIdx2TmpFile);
    while (batchIdx2TmpFileIt.hasNext()) {
        batchIdx2TmpFileIt.next();

        QString tmp = batchIdx2TmpFileIt.value();
        QFile tmpFile(tmp);
        tmpFile.remove();
    }
}
