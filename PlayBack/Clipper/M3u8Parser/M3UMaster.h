/**
 * author: oujiangping@dvt.dvt.com
 * date: 2016/06/15
 * describe: for the master playlist
 */

#ifndef M3UMASTER_H
#define M3UMASTER_H

#include "M3U8Base.h"

class M3UMaster : public M3UBase{
private:
    list<M3UMedia> *m3uMediaList;
public:
    list<M3uMedia>* getM3uMediaList();
    M3uMedia* getM3uMediaByBandWidth(int curBandWidth);
}

#endif
