#include "clippercommandqueue.h"

QString ClipperCommandQueue::getStorageBasePath() const
{
    return storageBasePath;
}

void ClipperCommandQueue::setStorageBasePath(const QString &value)
{
    storageBasePath = value;
    //qDebug() << Q_FUNC_INFO << storageBasePath;
}

ClipperCommandQueue::ClipperCommandQueue()
{
    savePath = QDir::homePath() + "/.videowall/Data/Clippers";
    if(!QDir(savePath).exists()) {
        qDebug() << "create path" << savePath;
        QDir().mkpath(savePath);
    }

    fileName = "clipperCommands.json";
    QSettings settings;
    QString pathVideosSaved  = settings.value("path_save_videos").toString();
    if(pathVideosSaved.isEmpty()){
        storageBasePath =  QDir::homePath() + "/MultimediaOfVideowall/Videos";
    }else{
        storageBasePath = pathVideosSaved;
    }
    QDir dir(storageBasePath);
    if (!dir.exists(storageBasePath)) {
        QDir().mkpath(storageBasePath);
    }

    loadFromJson();
}

void ClipperCommandQueue::enqueue(ClipperCommand* command) {
    commandsLock.lock();
    command->setCommandId(commandIdCount++);
    QSettings settings;
    QString pathVideosSaved  = settings.value("path_save_videos").toString();
    if(pathVideosSaved.isEmpty()){
        storageBasePath =  QDir::homePath() + "/MultimediaOfVideowall/Videos";
    }else{
        storageBasePath = pathVideosSaved;
    }
    command->setStorageBasePath(storageBasePath);
    commands.insert(command->getCommandId(), command);
    if(command->getPercent() != 100){
        penddingCommands.insert(command->getCommandId(), command);
    }
    commandsLock.unlock();

    saveToJson();
}

void ClipperCommandQueue::loadEnqueue(ClipperCommand* command) {
    commandsLock.lock();
    command->setCommandId(commandIdCount++);
    commands.insert(command->getCommandId(), command);
    if (command->getPercent() != 100) {
        penddingCommands.insert(command->getCommandId(), command);
    }
    commandsLock.unlock();
}

void ClipperCommandQueue::remove(long commandId) {
    commandsLock.lock();
    penddingCommands.remove(commandId);
    ClipperCommand* command = commands.take(commandId);
    delete command;
    commandsLock.unlock();

    saveToJson();
}

void ClipperCommandQueue::clear() {
    commandsLock.lock();

    penddingCommands.clear();
    qDebug() << Q_FUNC_INFO << commands.size();
    QMapIterator<long, ClipperCommand*> it(commands);
    while (it.hasNext()) {
        it.next();
        ClipperCommand *command = it.value();
        delete command;
    }

    commands.clear();

    commandsLock.unlock();

    saveToJson();
}

int ClipperCommandQueue::size() {
    int ret;

    commandsLock.lock();
    ret = commands.size();
    commandsLock.unlock();

    return ret;
}

QMap<long, ClipperCommand*> ClipperCommandQueue::getItems() {
    QMap<long, ClipperCommand*> ret;

    commandsLock.lock();
    ret = commands;
    commandsLock.unlock();

    return ret;
}

ClipperCommand* ClipperCommandQueue::pendingCommandDequeue() {
    ClipperCommand* ret = Q_NULLPTR;

    commandsLock.lock();
    if (!penddingCommands.isEmpty())
        ret = penddingCommands.first();
    commandsLock.unlock();

    return ret;
}

void ClipperCommandQueue::pendingCommandRemoveFirst() {
    commandsLock.lock();
    if (!penddingCommands.isEmpty()) {
        penddingCommands.remove(penddingCommands.firstKey());
    }
    commandsLock.unlock();

    saveToJson();
}

void ClipperCommandQueue::saveToJson() {
    qDebug() << Q_FUNC_INFO;

    QString saveFileName = savePath + "/" + fileName;
    QSaveFile saveFile(saveFileName);

    commandsLock.lock();

    if (!saveFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QJsonArray commandsArray;
    QMapIterator<long, ClipperCommand*> it(commands);
    while (it.hasNext()) {
        it.next();
        ClipperCommand* command = it.value();
        commandsArray.append(command->toJson());
    }

    if (commandsArray.size() > 0) {
        QJsonObject json;
        json["clipperCommands"] = commandsArray;
        QJsonDocument saveDoc(json);
        saveFile.write(saveDoc.toJson());
    }

    saveFile.commit();

    commandsLock.unlock();
}

void ClipperCommandQueue::loadFromJson() {
    QString loadFileName = savePath + "/" + fileName;
    QFile loadFile(loadFileName);

    if (loadFile.exists()) {
        if (!loadFile.open(QIODevice::ReadOnly)) {
            qWarning("Couldn't open save file.");
            return;
        }

        QByteArray saveData = loadFile.readAll();

        QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

        QJsonArray clipperCommandsArray = loadDoc.object()["clipperCommands"].toArray();
        qDebug() << "Load from" << fileName << clipperCommandsArray.size();
        for (int i = 0; i < clipperCommandsArray.size(); i++) {
            QJsonObject clipperCommandObject = clipperCommandsArray[i].toObject();
            ClipperCommand* command = new ClipperCommand(clipperCommandObject);
            loadEnqueue(command);
        }
    }
}
