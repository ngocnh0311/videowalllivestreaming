#ifndef CLIPPERCOMMANDQUEUE_H
#define CLIPPERCOMMANDQUEUE_H

#include <QDebug>
#include <QMutex>
#include <QList>
#include <QMap>
#include <QDir>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QSaveFile>
#include "clippercommand.h"
#include <QSettings>
class ClipperCommandQueue
{
    QString storageBasePath;

    QString savePath = "";
    QString fileName = "";
    long commandIdCount = 0;

    QMutex commandsLock;
    QMap<long, ClipperCommand *> penddingCommands;
    QMap<long, ClipperCommand *> commands;
public:
    ClipperCommandQueue();

    QMap<long, ClipperCommand *> getItems();
    int size();
    void enqueue(ClipperCommand* command);
    void loadEnqueue(ClipperCommand* command);
    ClipperCommand* pendingCommandDequeue();
    void pendingCommandRemoveFirst();

    void remove(long commandId);
    void clear();

    void saveToJson();
    void loadFromJson();
    QString getStorageBasePath() const;
    void setStorageBasePath(const QString &value);
};

#endif // CLIPPERCOMMANDQUEUE_H
