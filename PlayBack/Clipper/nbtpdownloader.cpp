#include "nbtpdownloader.h"

NBTPDownloader::NBTPDownloader()
{
    naManager = Q_NULLPTR;
}

void NBTPDownloader::onStartWorking() {
    qDebug() << Q_FUNC_INFO;

    if (dlTimer == Q_NULLPTR) {
        dlTimer = new QTimer;
        dlTimer->setSingleShot(true);
        connect(dlTimer, &QTimer::timeout, this, &NBTPDownloader::onDlTimerTimeout);
    }

    if (naManager == Q_NULLPTR) {
        naManager = new QNetworkAccessManager();
        qDebug() << Q_FUNC_INFO << "naManager" << naManager;
    }
}

void NBTPDownloader::onStopWorking() {
    qDebug() << Q_FUNC_INFO;

    if (downloadingDlspec.downloadId != -1) {
        cancelCurrentDlspec();
    }
}

void NBTPDownloader::setSource(QString _hostAddress, QString _channelName) {
    hostAddress = _hostAddress;
    channelName = _channelName;
}

void NBTPDownloader::onDlTimerTimeout() {
    cancelCurrentDlspec();

    if (timeoutRetry < 3) {
        qDebug() << "timeoutRetry. downloadId: " << downloadingDlspec.downloadId;
        timeoutRetry++;

        onReceiveDownloadVideo(downloadingDlspec);
        dlTimer->start(downloadingDlspec.timeout * 1000);
        return;
    }

    reportDownload(dlreport.DL_CANCELED);
}

void NBTPDownloader::cancelCurrentDlspec() {
    if (!downloadingDlspec.dtsReply.isEmpty()) {
        for (int i = 0; i < downloadingDlspec.dtsReply.size(); i++) {
            QNetworkReply* reply = downloadingDlspec.dtsReply[i];

            disconnect(reply, &QNetworkReply::finished, this, &NBTPDownloader::onDownloadFinishedByReply);

            reply->abort();
            reply->deleteLater();
        }

        downloadingDlspec.dtsReply.clear();
    }

    if (!downloadingDlspec.nbtpReply.isEmpty()) {
        for (int i = 0; i < downloadingDlspec.nbtpReply.size(); i++) {
            QNetworkReply* reply = downloadingDlspec.nbtpReply[i];

            disconnect(reply, &QNetworkReply::finished, this, &NBTPDownloader::onDownloadFinishedByReply);

            reply->abort();
            reply->deleteLater();
        }

        downloadingDlspec.nbtpReply.clear();
    }
}


void NBTPDownloader::onReceiveDownloadVideo(DownloadSpecs dlspec) {
    if (QThread::currentThread()->isInterruptionRequested()) {
        qDebug() << playerName << Q_FUNC_INFO << "Interrupted";
        return;
    }

    if (dlspec.downloadId != downloadingDlspec.downloadId) {
        if (downloadingDlspec.downloadId != -1) {
            cancelCurrentDlspec();
            return;
        }

        timeoutRetry = 0;
        dtsRetry = 0;
        nbtpRetries.clear();

        downloadingDlspec = dlspec;
        downloadingDlspec.startTimeBlockTimestamp = downloadingDlspec.startTime - (downloadingDlspec.startTime % 600);
        long endTime = downloadingDlspec.startTime + dlspec.length - 1;
        downloadingDlspec.endTimeBlockTimestamp = endTime - (endTime % 600);

        dlTimer->start(downloadingDlspec.timeout * 1000);

        dlreportSpec.downloadId = downloadingDlspec.downloadId;
        dlreportSpec.playerName = playerName;
        dlreportSpec.code = -1;
        dlreportSpec.dtsFailedStartBlock = false;
        dlreportSpec.dtsFailedEndBlock = false;
        dlreportSpec.numOfNbtpOk = 0;
        dlreportSpec.numOfNbtpFailed = 0;
        dlreportSpec.h264packets.clear();

        qDebug() << playerName << Q_FUNC_INFO
                 << "downloadId" << dlspec.downloadId
                 << "dlPos" << dlspec.requestDownloadPosition
                 << "startTime" << dlspec.startTime
                 << "length" << dlspec.length
                 << "timer interval" << dlTimer->interval();
    }

    if (downloadedDTSBlocks.contains(downloadingDlspec.startTimeBlockTimestamp)
            && downloadedDTSBlocks.contains(downloadingDlspec.endTimeBlockTimestamp)) {
        QList<QString> listNbtpUrls = buildNBTPUrls();

        if (listNbtpUrls.isEmpty()) {
            reportDownload(dlreport.DL_NBTP_NO_NBTP);
        } else {
            for (int i = 0; i < listNbtpUrls.size(); i++) {
                onReceiveDownloadNBTP(listNbtpUrls.at(i));
            }
        }
    } else {
        if (!downloadedDTSBlocks.contains(downloadingDlspec.startTimeBlockTimestamp)) {
            QString dtsUrl = dtsUrlFormat.arg(hostAddress)
                    .arg(channelName)
                    .arg(downloadingDlspec.startTimeBlockTimestamp);
            onReceiveDownloadDTS(dtsUrl);
        }

        if (downloadingDlspec.startTimeBlockTimestamp != downloadingDlspec.endTimeBlockTimestamp
                && !downloadedDTSBlocks.contains(downloadingDlspec.endTimeBlockTimestamp)) {
            QString dtsUrl = dtsUrlFormat.arg(hostAddress)
                    .arg(channelName)
                    .arg(downloadingDlspec.endTimeBlockTimestamp);
            onReceiveDownloadDTS(dtsUrl);
        }
    }
}

QList<QString> NBTPDownloader::buildNBTPUrls() {
    QList<QString> listNbtpUrls;

    int MAX_NBTP_LENGTH = 10;

    long startTime = downloadingDlspec.startTime;
    long endTime = downloadingDlspec.startTime + downloadingDlspec.length;
    long subStart = -1;
    long subEnd = -1;
    for (long i = startTime; i <= endTime; i++) {
        if (dtsmap.contains(i)) {
            if (subStart == -1) {
                subStart = i;
                if (subEnd != -1) {
                    //qDebug() << playerName << Q_FUNC_INFO << "No DTS1 " << subEnd + 1 << subStart;
                    subEnd = -1;
                }
            }
        } else {
            if (subStart != -1) {
                subEnd = i - 1;

                long partEnd;
                do {
                    partEnd = (subStart + MAX_NBTP_LENGTH) < subEnd ? (subStart + MAX_NBTP_LENGTH) : subEnd;
                    long subStartBlockTimestamp = subStart - (subStart % 600);
                    long subEndBlockTimestamp = partEnd - (partEnd % 600);


                    //qDebug() << "buildNBTP1" << subStart << partEnd;

                    if (subStartBlockTimestamp != subEndBlockTimestamp) {
                        QList<QPair<long,long>> pairs;
                        pairs.append(QPair<long,long>(subStart, subEndBlockTimestamp-1));
                        pairs.append(QPair<long,long>(subEndBlockTimestamp, partEnd));

                        for (int i=0; i < pairs.size(); i++) {
                            QPair<long, long> pair = pairs.at(i);

                            long subStart = pair.first;
                            long partEnd = pair.second;

                            long subStartBlockTimestamp = subStart - (subStart % 600);
                            long subEndBlockTimestamp = partEnd - (partEnd % 600);

                            DTSInfo dtsInfoStart = dtsmap.get(subStart);
                            DTSInfo dtsInfoEnd = dtsmap.get(partEnd);

                            if (dtsInfoEnd.dts2 > 59000) {
                                dtsInfoEnd.pkt_id2 += 1;
                                int temp = dtsInfoEnd.dts2 + 10;
                                dtsInfoEnd.dts2 = (temp > 60000) ? 60000 : temp;
                            }

                            QString nbtpUrl = nbtpUrlFormat.arg(hostAddress)
                                    .arg(subStartBlockTimestamp)
                                    .arg(dtsInfoStart.pkt_id1)
                                    .arg(dtsInfoStart.dts1)
                                    .arg(subEndBlockTimestamp)
                                    .arg(dtsInfoEnd.pkt_id2)
                                    .arg(dtsInfoEnd.dts2)
                                    .arg(channelName);

                            listNbtpUrls.append(nbtpUrl);

                            qDebug() << Q_FUNC_INFO << nbtpUrl;
                        }
                    } else {
                        DTSInfo dtsInfoStart = dtsmap.get(subStart);
                        DTSInfo dtsInfoEnd = dtsmap.get(partEnd);

                        if (dtsInfoEnd.dts2 > 59000) {
                            dtsInfoEnd.pkt_id2 += 1;
                            int temp = dtsInfoEnd.dts2 + 10;
                            dtsInfoEnd.dts2 = (temp > 60000) ? 60000 : temp;
                        }

                        QString nbtpUrl = nbtpUrlFormat.arg(hostAddress)
                                .arg(subStartBlockTimestamp)
                                .arg(dtsInfoStart.pkt_id1)
                                .arg(dtsInfoStart.dts1)
                                .arg(subEndBlockTimestamp)
                                .arg(dtsInfoEnd.pkt_id2)
                                .arg(dtsInfoEnd.dts2)
                                .arg(channelName);

                        listNbtpUrls.append(nbtpUrl);
                    }

                    subStart = partEnd + 1;
                } while (partEnd < subEnd);
                subStart = -1;
            }
        }
    }

    if (subEnd == -1) {
        if (subStart != -1) {
            subEnd = endTime;
            long partEnd;
            do {
                partEnd = (subStart + MAX_NBTP_LENGTH) < subEnd ? (subStart + MAX_NBTP_LENGTH) : subEnd;

                //qDebug() << "buildNBTP2" << subStart << partEnd;

                long subStartBlockTimestamp = subStart - (subStart % 600);
                long subEndBlockTimestamp = partEnd - (partEnd % 600);

                if (subStartBlockTimestamp != subEndBlockTimestamp) {
                    QList<QPair<long,long>> pairs;
                    pairs.append(QPair<long,long>(subStart, subEndBlockTimestamp-1));
                    pairs.append(QPair<long,long>(subEndBlockTimestamp, partEnd));

                    for (int i=0; i < pairs.size(); i++) {
                        QPair<long, long> pair = pairs.at(i);

                        long subStart = pair.first;
                        long partEnd = pair.second;

                        long subStartBlockTimestamp = subStart - (subStart % 600);
                        long subEndBlockTimestamp = partEnd - (partEnd % 600);

                        DTSInfo dtsInfoStart = dtsmap.get(subStart);
                        DTSInfo dtsInfoEnd = dtsmap.get(partEnd);

                        if (dtsInfoEnd.dts2 > 59000) {
                            dtsInfoEnd.pkt_id2 += 1;
                            int temp = dtsInfoEnd.dts2 + 10;
                            dtsInfoEnd.dts2 = (temp > 60000) ? 60000 : temp;
                        }

                        QString nbtpUrl = nbtpUrlFormat.arg(hostAddress)
                                .arg(subStartBlockTimestamp)
                                .arg(dtsInfoStart.pkt_id1)
                                .arg(dtsInfoStart.dts1)
                                .arg(subEndBlockTimestamp)
                                .arg(dtsInfoEnd.pkt_id2)
                                .arg(dtsInfoEnd.dts2)
                                .arg(channelName);

                        listNbtpUrls.append(nbtpUrl);

                        qDebug() << Q_FUNC_INFO << nbtpUrl;
                    }
                } else {
                    DTSInfo dtsInfoStart = dtsmap.get(subStart);
                    DTSInfo dtsInfoEnd = dtsmap.get(partEnd);

                    if (dtsInfoEnd.dts2 > 59000) {
                        dtsInfoEnd.pkt_id2 += 1;
                        int temp = dtsInfoEnd.dts2 + 10;
                        dtsInfoEnd.dts2 = (temp > 60000) ? 60000 : temp;
                    }

                    QString nbtpUrl = nbtpUrlFormat.arg(hostAddress)
                            .arg(subStartBlockTimestamp)
                            .arg(dtsInfoStart.pkt_id1)
                            .arg(dtsInfoStart.dts1)
                            .arg(subEndBlockTimestamp)
                            .arg(dtsInfoEnd.pkt_id2)
                            .arg(dtsInfoEnd.dts2)
                            .arg(channelName);

                    listNbtpUrls.append(nbtpUrl);
                }

                subStart = partEnd + 1;
            } while (partEnd < subEnd);

            subStart = -1;
        } else {
            //qDebug() << playerName << Q_FUNC_INFO << "No DTS2 " << startTime << endTime;
        }
    }

    return listNbtpUrls;
}

void NBTPDownloader::onReceiveDownloadDTS(QString url) {
    //qDebug() << playerName << Q_FUNC_INFO << url;

    QNetworkRequest req(url);
    QNetworkReply *reply = naManager->get(req);
    connect(reply, &QNetworkReply::finished, this, &NBTPDownloader::onDownloadFinishedByReply);
    //connect(reply, &QNetworkReply::downloadProgress, this, &NBTPDownloader::onDownloadProgress);

    downloadingDlspec.dtsReply.append(reply);
}

void NBTPDownloader::onReceiveDownloadNBTP(QString url) {
    qDebug() << playerName << Q_FUNC_INFO << url;

    QNetworkRequest req(url);
    QNetworkReply *reply = naManager->get(req);
    connect(reply, &QNetworkReply::finished, this, &NBTPDownloader::onDownloadFinishedByReply);

    downloadingDlspec.nbtpReply.append(reply);
}

void NBTPDownloader::onDownloadFinishedByReply() {
    QNetworkReply *reply = (QNetworkReply *) QObject::sender();

    disconnect(reply, &QNetworkReply::finished, this, &NBTPDownloader::onDownloadFinishedByReply);

    onDownloadFinished(reply);
}

void NBTPDownloader::onDownloadProgress(qint64 bytesReceived, qint64 bytesTotal) {
    qDebug() << playerName << Q_FUNC_INFO << bytesReceived << bytesTotal;
}

void NBTPDownloader::onDownloadFinished(QNetworkReply *reply) {
    if (QThread::currentThread()->isInterruptionRequested()) {
        qDebug() << playerName << Q_FUNC_INFO << "Interrupted";
        return;
    }

    QString url = reply->url().toString();
    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (statusCode == 200 && reply->bytesAvailable() > 0) {
        if (url.contains(".nbtp")) {
            downloadingDlspec.nbtpReply.removeOne(reply);
            qDebug() << "nbtpokok" << url.section('/', 5);
            onDownloadedNBTP(reply->readAll());
        } else {
            downloadingDlspec.dtsReply.removeOne(reply);

            if (url.contains(QString::number(downloadingDlspec.startTimeBlockTimestamp))) {
                onDownloadedDTS(downloadingDlspec.startTimeBlockTimestamp, reply->readAll());
            } else {
                onDownloadedDTS(downloadingDlspec.endTimeBlockTimestamp, reply->readAll());
            }
        }
    } else {
        if (url.contains(".nbtp")) {
            downloadingDlspec.nbtpReply.removeOne(reply);

            QString nbtpItem = url.section('/', 5);
            if (!nbtpRetries.contains(nbtpItem)) {
                nbtpRetries.insert(nbtpItem, 0);
            }
            QMap<QString, int>::iterator nbtpRetryIt = nbtpRetries.find(nbtpItem);
            if(nbtpRetryIt.value() < 3) {
                qDebug() << "nbtp retry. downloadId: " << downloadingDlspec.downloadId << url.section('/', 5);
                nbtpRetryIt.value() += 1;
                dlTimer->start(downloadingDlspec.timeout * 1000);

                onReceiveDownloadNBTP(url);
                return;
            }

            onDownloadNBTPFailed();
        } else {
            downloadingDlspec.dtsReply.removeOne(reply);

            if (dtsRetry < 3) {
                qDebug() << "dts retry. downloadId: " << downloadingDlspec.downloadId << url.section('/', 5);
                dlTimer->start(downloadingDlspec.timeout * 1000);

                dtsRetry++;
                onReceiveDownloadDTS(url);
                return;
            }

            if (url.contains(QString::number(downloadingDlspec.startTimeBlockTimestamp))) {
                onDownloadDTSFailed(downloadingDlspec.startTimeBlockTimestamp);
            } else {
                onDownloadDTSFailed(downloadingDlspec.endTimeBlockTimestamp);
            }
        }
    }

    reply->deleteLater();
}

void NBTPDownloader::onDownloadedDTS(long blockTimestamp, QByteArray payload) {
    parseDTS(blockTimestamp, payload);

    downloadedDTSBlocks.append(blockTimestamp);

    dlTimer->start(downloadingDlspec.timeout * 1000);

    if (downloadingDlspec.dtsReply.isEmpty()) {
        onReceiveDownloadVideo(downloadingDlspec);
    }
}

void NBTPDownloader::parseDTS(long blockTimestamp, QByteArray payload) {
    int MAX_DTS = 60000;

    int packet_id = 1;

    long frameSecond = 0;
    long lastFrameSecond = 0;

    int firstFramePartPktId = -1;
    int firstFramePartDts = -1;
    int lastFramePartPktId = -1;
    int lastFramePartDts = -1;

    QByteArray dtsPackage = payload;
    for (int index = 0; index < dtsPackage.size(); index += 2) {
        if (QThread::currentThread()->isInterruptionRequested()) {
            qDebug() << playerName << Q_FUNC_INFO << "Interrupted";
            return;
        }

        //cắt lần lượt hai byte liền nhau và hoán đổi vị trị để lấy được giá trị FrameDTS
        QByteArray frameDTS;
        frameDTS.insert(0, dtsPackage.at(index));
        frameDTS.insert(0, dtsPackage.at(index + 1));

        bool ok;
        int dts = frameDTS.toHex().toInt(&ok, 16);

        if (dts >= 0 && dts < MAX_DTS) {
            long frameMilisecondTimestamp = (blockTimestamp * 1000) + (dts * 10);
            frameSecond = (frameMilisecondTimestamp - (frameMilisecondTimestamp % 1000))/1000;

            if (firstFramePartPktId == -1) {
                firstFramePartPktId = packet_id;
                firstFramePartDts = dts;
            }

            if (frameSecond != lastFrameSecond && lastFramePartPktId != -1) {
                DTSInfo dtsInfo(firstFramePartPktId, firstFramePartDts, lastFramePartPktId, lastFramePartDts);
                dtsmap.add(lastFrameSecond, dtsInfo);
                //qDebug() << "DTS frameSecond" << lastFrameSecond;

                firstFramePartPktId = packet_id;
                firstFramePartDts = dts;

                lastFramePartPktId = -1;
                lastFramePartDts = -1;
            }

            lastFramePartPktId = packet_id;
            lastFramePartDts = dts;

            packet_id++;
            lastFrameSecond = frameSecond;
        } else {
            //qDebug() << playerName << Q_FUNC_INFO << "Invalid DTS" << dts;
        }
    }

    if (lastFramePartPktId != -1) {
        DTSInfo dtsInfo(firstFramePartPktId, firstFramePartDts, lastFramePartPktId, lastFramePartDts);
        dtsmap.add(lastFrameSecond, dtsInfo);
    }
}

void NBTPDownloader::onDownloadDTSFailed(long blockTimestamp) {
    bool isStartAndEndOnSameBlock = (downloadingDlspec.endTimeBlockTimestamp
                                     == downloadingDlspec.startTimeBlockTimestamp);
    if (isStartAndEndOnSameBlock) {
        reportDownload(dlreport.DL_DTS_FAILED_ONE_BLOCK);
    } else {
        if (blockTimestamp == downloadingDlspec.startTimeBlockTimestamp) {
            dlreportSpec.dtsFailedStartBlock = true;

            downloadingDlspec.length -= downloadingDlspec.endTimeBlockTimestamp - downloadingDlspec.startTime;
            downloadingDlspec.startTime = downloadingDlspec.endTimeBlockTimestamp;
            downloadingDlspec.startTimeBlockTimestamp = downloadingDlspec.endTimeBlockTimestamp;

            if (downloadedDTSBlocks.contains(downloadingDlspec.endTimeBlockTimestamp)) {
                onReceiveDownloadVideo(downloadingDlspec);
            } else if (downloadingDlspec.dtsReply.isEmpty()) {
                reportDownload(dlreport.DL_DTS_FAILED_TWO_BLOCK);
            }
        } else {
            dlreportSpec.dtsFailedEndBlock = true;

            downloadingDlspec.length = downloadingDlspec.endTimeBlockTimestamp - downloadingDlspec.startTime;
            downloadingDlspec.endTimeBlockTimestamp = downloadingDlspec.startTimeBlockTimestamp;

            if (downloadedDTSBlocks.contains(downloadingDlspec.startTimeBlockTimestamp)) {
                onReceiveDownloadVideo(downloadingDlspec);
            } else if (downloadingDlspec.dtsReply.isEmpty()) {
                reportDownload(dlreport.DL_DTS_FAILED_TWO_BLOCK);
            }
        }
    }
}

void NBTPDownloader::onDownloadedNBTP(QByteArray payload) {
    //qDebug() << playerName << Q_FUNC_INFO << "downloadId" << downloadingDlspec.downloadId;
    parseNBTP(payload);

    dlreportSpec.numOfNbtpOk += 1;

    if (downloadingDlspec.nbtpReply.isEmpty()) {
        reportDownload(dlreport.DL_FINISHED);
    }
}

void NBTPDownloader::parseNBTP(QByteArray payload) {
    //qDebug() << playerName << Q_FUNC_INFO;

    int HEADER_LENGTH = 12;
    bool IDRPictureStarted = false;
    QByteArray IDRPicture;

    QByteArray rawData = payload;
    if (!rawData.isEmpty() && (rawData.size() > HEADER_LENGTH)) {
        long passedBytes = 0;
        bool rtpIsOk = true;
        while (passedBytes < rawData.size() && rtpIsOk) {
            if (QThread::currentThread()->isInterruptionRequested()) {
                qDebug() << playerName << Q_FUNC_INFO << "Interrupted";
                return;
            }

            bool ret;
            int payloadSize = rawData.mid(passedBytes + 0, 2).toHex().toInt(&ret, 16);

            //convert timestamp for NBTP (trừ đi thời gian của 40 năm)
            long long timestring = rawData.mid(passedBytes + 2, 8).toHex().toLongLong(0, 16);
            double timestampDouble = reinterpret_cast<double&>(timestring);
            QString timestampToString = QString::number(timestampDouble, 'g', 19);
            long timestamp = timestampToString.toLong() + 1262304000000;

            int packet_id = rawData.mid(passedBytes + 10, 2).toHex().toInt(&ret, 16);

            // khac voi live thi size nay duoc dinh nghia kich thuoc cua h264
            // (NBTP payloadSize không tính 10 byte (8 byte timestamp + 2 byte index) như live)
            int lengthOfH264RawData = payloadSize;
            if ((passedBytes + HEADER_LENGTH + lengthOfH264RawData) > rawData.size()) {
                rtpIsOk = false;
                qDebug() << playerName << "something went wrong... in RTP passedBytes + HEADER_LENGTH + lengthOfH264RawData="
                         << (passedBytes + HEADER_LENGTH + lengthOfH264RawData) << "/" << rawData.size();
                break;
            }

            QByteArray h264Raw = rawData.mid(passedBytes + HEADER_LENGTH, lengthOfH264RawData);
            passedBytes = passedBytes + HEADER_LENGTH + payloadSize;

            // h264 processing
            int nalu_header_byte = h264Raw.mid(0, 1).toHex().toInt(&ret,16);
            int nalType = nalu_header_byte & 0x1F;//0b11111

            if((nalu_header_byte & 0b10000000) == 1){
                qDebug() << playerName << "The H.264 specification declares avalue of 1 as a syntax violation";
            }

            if (nalType != 28 && IDRPicture.size() > 0){
                onReceiveH264Packet(packet_id, NALUnit.IDR, timestamp, IDRPicture);
                IDRPicture.clear();
            }

            switch (nalType) {
            case NALUnit.SPS: // SPS Type = 7
            {
                onReceiveH264Packet(packet_id, NALUnit.SPS, timestamp, h264Raw);
            } break;

            case NALUnit.PPS: // PPS Type = 8
            {
                onReceiveH264Packet(packet_id, NALUnit.PPS, timestamp, h264Raw);
            } break;

            case NALUnit.IDR: // IDR Type = 5
            {
                onReceiveH264Packet(packet_id, NALUnit.IDR, timestamp, h264Raw);
            }break;

            case NALUnit.FU_H264: { // IDR Picture Type MULTIPLE PART (Fragmentation Units)
                int FU_A_byte = h264Raw.mid(1, 1).toHex().toInt(&ret,16);
                if (FU_A_byte & 0x80) { //0b10000000
                    if(IDRPicture.size() > 0){
                        onReceiveH264Packet(packet_id, NALUnit.IDR, timestamp, IDRPicture);
                        IDRPicture.clear();
                    }
                    //nếu bằng 1 tức start IDR
                    IDRPictureStarted = true;
                    QByteArray IDR_NAL_TYPE_TEMP;
                    IDR_NAL_TYPE_TEMP.append((nalu_header_byte & 0xe0) | (FU_A_byte & 0x1f));
                    IDRPicture = IDR_NAL_TYPE_TEMP + h264Raw.mid(2, lengthOfH264RawData);
                } else {
                    if (FU_A_byte & 0x40) { //0b01000000 // end of Fragment Unit
                        if (IDRPictureStarted == false) {
                            rtpIsOk = false;
                            break;
                        }
                        IDRPicture = IDRPicture + h264Raw.mid(2, lengthOfH264RawData);
                        onReceiveH264Packet(packet_id, NALUnit.IDR, timestamp, IDRPicture);
                        IDRPicture.clear();
                    } else { // middle of Fragment Unit
                        if (IDRPictureStarted == false) {
                            rtpIsOk = false;
                            break;
                        }
                        IDRPicture = IDRPicture + h264Raw.mid(2, lengthOfH264RawData);
                    }
                }

                if (FU_A_byte & 0x10) {//0b00100000
                    qDebug() << playerName << "The Reserved bit MUST be equal to 0 and MUST be ignored by the receiver";
                }
            } break;

            case NALUnit.PFRAME: { // P-Frame  Picture Type
                onReceiveH264Packet(packet_id, NALUnit.PFRAME, timestamp, h264Raw);
            } break;

            case 0: { // End Of 10 minute type
                qDebug() << playerName << " end of 10 minutes " << " nalu_header_byte" << nalu_header_byte;
            } break;

            default:
            {
                qDebug() << playerName << "NALUnit type "<< "non-catched :" << NALUnit.toString(nalType);
            } break;
            }
        }
    }
}

void NBTPDownloader::onReceiveH264Packet(int pkt_id, int naltype, long timestamp, QByteArray packet) {
    long packetSecond = (timestamp - timestamp % 1000)/1000;
    long blockTimestamp = (packetSecond - packetSecond % 600);
    long pivot = (blockTimestamp - (blockTimestamp - blockTimestamp % 1000)) * 100000;

    //qDebug() << playerName << Q_FUNC_INFO << "downloadId" << downloadingDlspec.downloadId << blockTimestamp << pkt_id << naltype << timestamp << packetSecond;

    H264Packet h264packet;
    h264packet.pkt_id = pkt_id;
    h264packet.naltype = naltype;
    h264packet.timestamp = timestamp;
    h264packet.packet = packet;

    dlreportSpec.h264packets.insert(pivot + pkt_id, h264packet);
}

void NBTPDownloader::onDownloadNBTPFailed() {
    //    qDebug() << playerName << Q_FUNC_INFO << downloadId;

    dlreportSpec.numOfNbtpFailed += 1;

    if (downloadingDlspec.nbtpReply.isEmpty()) {
        reportDownload(dlreport.DL_FINISHED);
    }
}

void NBTPDownloader::reportDownload(int reason) {
    bool timeout = true;
    if(dlTimer->isActive()) {
        dlTimer->stop();
        timeout = false;
    }

    if(reason == dlreport.DL_CANCELED && timeout) {
        dlreportSpec.code = dlreport.DL_TIMED_OUT;
    } else if (reason == dlreport.DL_DTS_FAILED_ONE_BLOCK &&
               (dlreportSpec.dtsFailedEndBlock | dlreportSpec.dtsFailedStartBlock)) {
        dlreportSpec.code = dlreport.DL_DTS_FAILED_TWO_BLOCK;
    } else {
        dlreportSpec.code = reason;
    }

    long requestDlPos = downloadingDlspec.requestDownloadPosition;
    long endPos = downloadingDlspec.requestDownloadPosition + downloadingDlspec.length;

    Q_EMIT finishDownload(dlreportSpec);

    downloadingDlspec.downloadId = -1;
}
