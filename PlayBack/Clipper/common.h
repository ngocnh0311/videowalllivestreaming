#ifndef COMMON_H
#define COMMON_H


#include <QString>
#include <QDebug>
#include <QNetworkReply>
#include <QMap>
#include <QByteArray>
#include <QList>
#include <QPair>
#include <QtAwesome/QtAwesome.h>

struct CLipperReport
{
    const static int CLIPPER_FINISHED = 0;
    const static int CLIPPER_M3U8_FAILED = 1;
    const static int CLIPPER_NODATA = 2;
    const static int CLIPPER_CONVERT_MP4_FAILED = 3;

    const static QString toString(int retCode) {
        switch (retCode) {
        case CLIPPER_FINISHED: { return "CLIPPER_FINISHED"; } break;
        case CLIPPER_M3U8_FAILED: { return "CLIPPER_M3U8_FAILED"; } break;
        case CLIPPER_NODATA: { return "CLIPPER_NODATA"; } break;
        case CLIPPER_CONVERT_MP4_FAILED: { return "CLIPPER_CONVERT_MP4_FAILED"; } break;
        default: {return "";} break;
        }
    }
};


#endif // COMMON_H
