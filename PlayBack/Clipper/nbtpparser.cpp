#include "nbtpparser.h"

NBTPParser::NBTPParser()
{

}

void NBTPParser::onParseNbtpRequest(long segmentIdx, QByteArray payload) {
    int HEADER_LENGTH = 12;
    segmentParts = new SegmentedRange();
    lastPacketSecond = 0;
    h264PayloadData.clear();

    QByteArray rawData = payload;
    if (!rawData.isEmpty() && (rawData.size() > HEADER_LENGTH)) {
        long passedBytes = 0;
        bool rtpIsOk = true;
        while (passedBytes < rawData.size() && rtpIsOk) {
            if (QThread::currentThread()->isInterruptionRequested()) {
                qDebug() << Q_FUNC_INFO << "Interrupted";
                return;
            }

            bool ret;
            int payloadSize = rawData.mid(passedBytes + 0, 2).toHex().toInt(&ret, 16);

            //convert timestamp for NBTP (trừ đi thời gian của 40 năm)
            long long timestring = rawData.mid(passedBytes + 2, 8).toHex().toLongLong(0, 16);
            double timestampDouble = reinterpret_cast<double&>(timestring);
            QString timestampToString = QString::number(timestampDouble, 'g', 19);
            long timestamp = timestampToString.toLong() + 1262304000000;

            int packet_id = rawData.mid(passedBytes + 10, 2).toHex().toInt(&ret, 16);

            // khac voi live thi size nay duoc dinh nghia kich thuoc cua h264
            // (NBTP payloadSize không tính 10 byte (8 byte timestamp + 2 byte index) như live)
            int lengthOfH264RawData = payloadSize;
            if ((passedBytes + HEADER_LENGTH + lengthOfH264RawData) > rawData.size()) {
                rtpIsOk = false;
                qDebug() << "something went wrong... in RTP passedBytes + HEADER_LENGTH + lengthOfH264RawData="
                         << (passedBytes + HEADER_LENGTH + lengthOfH264RawData) << "/" << rawData.size();
                break;
            }

            QByteArray h264Raw = rawData.mid(passedBytes + HEADER_LENGTH, lengthOfH264RawData);
            passedBytes = passedBytes + HEADER_LENGTH + payloadSize;

            // h264 processing
            int nalu_header_byte = h264Raw.mid(0, 1).toHex().toInt(&ret,16);
            int nalType = nalu_header_byte & 0x1F;//0b11111

            if((nalu_header_byte & 0b10000000) == 1){
                qDebug() << "The H.264 specification declares avalue of 1 as a syntax violation";
            }

            if (nalType != 28 && IDRPicture.size() > 0){
                onReceiveH264Packet(packet_id, NALUnit.IDR, timestamp, IDRPicture);
                IDRPicture.clear();
            }

            switch (nalType) {
            case NALUnit.SPS: // SPS Type = 7
            {
                onReceiveH264Packet(packet_id, NALUnit.SPS, timestamp, h264Raw);
            } break;

            case NALUnit.PPS: // PPS Type = 8
            {
                onReceiveH264Packet(packet_id, NALUnit.PPS, timestamp, h264Raw);
            } break;

            case NALUnit.IDR: // IDR Type = 5
            {
                onReceiveH264Packet(packet_id, NALUnit.IDR, timestamp, h264Raw);
            }break;

            case NALUnit.ACCESS_UNIT_DELIMITER: // IDR Type = 9
            {
                onReceiveH264Packet(packet_id, NALUnit.ACCESS_UNIT_DELIMITER, timestamp, h264Raw);
            }break;

            case NALUnit.FU_H264: { // IDR Picture Type MULTIPLE PART (Fragmentation Units)
                int FU_A_byte = h264Raw.mid(1, 1).toHex().toInt(&ret,16);
                if (FU_A_byte & 0x80) { //0b10000000
                    if(IDRPicture.size() > 0){
                        onReceiveH264Packet(packet_id, NALUnit.IDR, timestamp, IDRPicture);
                        IDRPicture.clear();
                    }
                    //nếu bằng 1 tức start IDR
                    IDRPictureStarted = true;
                    QByteArray IDR_NAL_TYPE_TEMP;
                    IDR_NAL_TYPE_TEMP.append((nalu_header_byte & 0xe0) | (FU_A_byte & 0x1f));
                    IDRPicture = IDR_NAL_TYPE_TEMP + h264Raw.mid(2, lengthOfH264RawData);
                } else {
                    if (FU_A_byte & 0x40) { //0b01000000 // end of Fragment Unit
                        if (IDRPictureStarted == false) {
                            rtpIsOk = false;
                            break;
                        }
                        IDRPicture = IDRPicture + h264Raw.mid(2, lengthOfH264RawData);
                        onReceiveH264Packet(packet_id, NALUnit.IDR, timestamp, IDRPicture);
                        IDRPicture.clear();
                    } else { // middle of Fragment Unit
                        if (IDRPictureStarted == false) {
                            rtpIsOk = false;
                            break;
                        }
                        IDRPicture = IDRPicture + h264Raw.mid(2, lengthOfH264RawData);
                    }
                }

                if (FU_A_byte & 0x10) {//0b00100000
                    qDebug() << "The Reserved bit MUST be equal to 0 and MUST be ignored by the receiver";
                }
            } break;

            case NALUnit.PFRAME: { // P-Frame  Picture Type
                onReceiveH264Packet(packet_id, NALUnit.PFRAME, timestamp, h264Raw);
            } break;

            case 0: { // End Of 10 minute type
                qDebug() << " end of 10 minutes " << " nalu_header_byte" << nalu_header_byte;
            } break;

            default:
            {
                qDebug() << "NALUnit type "<< "non-catched :" << NALUnit.toString(nalType) << nalType;
            } break;
            }
        }
    }

    Q_EMIT h264Payload(segmentIdx, h264PayloadData, segmentParts);
}

void NBTPParser::onReceiveH264Packet(int pkt_id, int naltype, long timestamp, QByteArray packet) {
    long packetSecond = (timestamp - timestamp % 1000)/1000;

//    qDebug() << Q_FUNC_INFO << pkt_id << naltype << timestamp << packetSecond;

    if (packetSecond != lastPacketSecond) {
        segmentParts->insert(packetSecond);
    }
    lastPacketSecond = packetSecond;

    h264PayloadData += NALUStartCode + packet;
}
