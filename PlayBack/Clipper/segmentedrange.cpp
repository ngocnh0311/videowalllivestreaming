#include "segmentedrange.h"

SegmentedRange::SegmentedRange()
{

}

void SegmentedRange::insert(long val) {
    //qDebug() << Q_FUNC_INFO << val;
    segmentsLock.lock();
    if (segments.size()) {
        QList<QPair<long, long>>::reverse_iterator pairIt = segments.rbegin();

        long endVal = pairIt->second;
        if (endVal == -1) {
            pairIt->second = val;
        } else {
            int diff = val - endVal;
            if (diff == 1) {
                pairIt->second = val;
            } else if (diff > 1) {
                QPair<long, long> pair(val, val);
                segments.append(pair);
            }
        }
    } else {
        QPair<long, long> pair(val, val);
        segments.append(pair);
    }
    segmentsLock.unlock();
}

long SegmentedRange::netLength() {
    long ret = 0;

    segmentsLock.lock();
    if (!segments.isEmpty()) {
        for(int i = 0; i < segments.size(); i++) {
            QPair<long, long> pair = segments.at(i);
            ret += pair.second - pair.first + 1;
        }
    }
    segmentsLock.unlock();

    return ret;
}

void SegmentedRange::clear() {
    segmentsLock.lock();
    segments.clear();
    segmentsLock.unlock();
}

int SegmentedRange::size() {
    int ret = 0;
    segmentsLock.lock();
    ret = segments.size();
    segmentsLock.unlock();

    return ret;
}

QPair<long, long> SegmentedRange::at(int index) {
    QPair<long, long> ret;
    segmentsLock.lock();
    ret = segments.at(index);
    segmentsLock.unlock();

    return ret;
}

QList<QPair<long, long>> SegmentedRange::getSegments() {
    QList<QPair<long, long>> ret;

    segmentsLock.lock();
    ret = segments;
    segmentsLock.unlock();

    return ret;
}

QString SegmentedRange::toString() {
    QString str = "";

    segmentsLock.lock();
    for (int i = 0; i < segments.size(); i++) {
        QPair<long, long> pair = segments.at(i);

        str += "[" + QString::number(pair.first) + ", " + QString::number(pair.second) + "] ";
    }
    segmentsLock.unlock();

    if (str.isEmpty())
        qDebug() << "No entry";

    return str;
}
