#ifndef NBTPDOWNLOADER_H
#define NBTPDOWNLOADER_H

#include <QDebug>
#include <QThread>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>

#include "dtsmap2.h"
#include "common.h"

#include "consts.h"
class NBTPDownloader : public QObject
{
    Q_OBJECT

    QString playerName = "playerName";
    QString hostAddress = "hostAddress";
    QString channelName = "channelName";

    NALUnitType NALUnit;
    QByteArray NALUStartCode = QByteArray("\x00\x00\x01", 3);

    QString dtsUrlFormat = "http://%1/rec/dts/%2.%3";
    QString nbtpUrlFormat = "http://%1/rec/hls/%2_%3_%4_%5_%6_%7.%8.nbtp";

    QNetworkAccessManager *naManager;

    DTSMap2 dtsmap;
    QTimer* dlTimer = Q_NULLPTR;

    DownloadSpecs downloadingDlspec;
    QList<long> downloadedDTSBlocks;
    QList<long> downloadingDTSBlocks;

    DownloadReport dlreport;
    DownloadReportSpecs dlreportSpec;

    int timeoutRetry = 0;
    int dtsRetry = 0;
    QMap<QString, int> nbtpRetries;

    // private function
    QList<QString> buildNBTPUrls();

    void onReceiveDownloadDTS(QString url);
    void onReceiveDownloadNBTP(QString url);

    void onDownloadedDTS(long blockTimestamp, QByteArray payload);
    void onDownloadDTSFailed(long blockTimestamp);
    void onDownloadedNBTP(QByteArray payload);
    void onDownloadNBTPFailed();

    void reportDownload(int reason);

    void parseDTS(long blockTimestamp, QByteArray payload);
    void parseNBTP(QByteArray payload);
    void onReceiveH264Packet(int pkt_id, int naltype, long timestamp, QByteArray packet);

public:
    NBTPDownloader();
    void setSource(QString _hostAddress, QString _channelName);

Q_SIGNALS:
    void finishDownload(DownloadReportSpecs dlreportSpec);

public Q_SLOTS:
    void onReceiveDownloadVideo(DownloadSpecs dlspec);
    void cancelCurrentDlspec();
    void onDownloadFinished(QNetworkReply *reply);
    void onDownloadFinishedByReply();
    void onDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);

    void onStartWorking();
    void onStopWorking();
    void onDlTimerTimeout();
};

#endif // NBTPDOWNLOADER_H
