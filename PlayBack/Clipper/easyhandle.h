#ifndef EASYHANDLE_H
#define EASYHANDLE_H

#include <QObject>
#include <QDebug>
#include <QThread>

#include <curl/curl.h>

#define MAX_RETRY 5

class EasyHandlePri;
struct PrivateData {
    EasyHandlePri *handle;
    bool *abortRequested = Q_NULLPTR;
    bool aborted = false;
    long receivedBytes = -1;
};


class EasyHandlePri : public QObject
{
    Q_OBJECT
    bool doDebug;

    CURL *handle;    
    bool isAbortRequested;
    QByteArray receivedData;
public:
    EasyHandlePri();
    ~EasyHandlePri();

    bool getIsAbortRequested() const;
    void setIsAbortRequested(bool value);
    long appendReceivedData(QByteArray payload, long size);

Q_SIGNALS:
    void aborted();
    void downloadProgress(long receivedBytes);
    void finished(long statusCode, QByteArray payload);

public Q_SLOTS:
    void onGet(QString url, long timeoutMsec, long retry);
};


class EasyHandle : public QObject
{
    Q_OBJECT
    static long instanceCount;
    EasyHandlePri handlepri;
    QThread thread;

public:
    EasyHandle();
    ~EasyHandle();

    void get(QString url);
    void get(QString url, long timeoutMsec, long retry);
    void abort();

Q_SIGNALS:
    void finished(long statusCode, QByteArray payload);
    void aborted();

public Q_SLOTS:
    void onDownloadProgress(long receivedBytes);
};

#endif // EASYHANDLE_H

