#include "downloader2.h"

Downloader2::Downloader2()
{
    doDebug = false;

    for (int i = 0; i < MAX_HANDLE; i++) {
        EasyHandle *handle = new EasyHandle;
        freeHandles.append(handle);
        connect(handle, &EasyHandle::finished, this, &Downloader2::onDownloadFinished);
        connect(handle, &EasyHandle::aborted, this, &Downloader2::onAborted);
    }
}

void Downloader2::onDownloadRequest(long key, QString url, long timeout, int retry) {
    EasyHandle *handle;
    if (!freeHandles.isEmpty()) {
        handle = freeHandles.takeFirst();
        runningHandles.append(handle);
    } else {
        if (doDebug)
            qDebug() << Q_FUNC_INFO << "ERROR: No handle";

        Q_EMIT downloadedPayload(key, QByteArray());
        return;
    }

    if (doDebug)
        qDebug() << "onDownloadRequest" << "handle" << handle << "key" << key << url;

    handle2key.insert(handle, key);
    handle->get(url, timeout, retry);
}

void Downloader2::onDownloadFinished(long statusCode, QByteArray payload) {
    EasyHandle * handle = (EasyHandle *) QObject::sender();

    long key = handle2key.take(handle);

    if (doDebug)
        qDebug() << "onDownloadFinished" << "key" << key << "handle" << handle;

    freeHandles.append(handle);
    runningHandles.removeOne(handle);

    Q_EMIT downloadedPayload(key, payload);
}

void Downloader2::onCancelAllRequests() {
    if (doDebug)
        qDebug() << Q_FUNC_INFO;

    if (!runningHandles.isEmpty()) {
        for (int i = 0; i < runningHandles.size(); i++)
            runningHandles.at(i)->abort();
    } else {
        Q_EMIT canceledAllRequests();
    }
}

void Downloader2::onAborted() {
    EasyHandle * handle = (EasyHandle *) QObject::sender();
    freeHandles.append(handle);
    runningHandles.removeOne(handle);

    if (runningHandles.isEmpty()) {
        Q_EMIT canceledAllRequests();
    }
}
