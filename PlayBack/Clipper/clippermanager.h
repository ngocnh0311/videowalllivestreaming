#ifndef CLIPPERMANAGER_H
#define CLIPPERMANAGER_H

#include <QDebug>
#include <QThread>
#include "clipperworker.h"
#include "clippercommandqueue.h"

class ClipperManager : public QObject
{
    Q_OBJECT

    ClipperCommandQueue* clipperCommandQueue;

    QTimer* pollingTimer;

    ClipperWorker *clipperWorker;
    QThread *clipperWorkerThread;

    ClipperCommand* command;

    CLipperReport clipperReport;
    bool isWorkerBusy = false;
    bool isCancelRequest = false;
    bool isClearAllRequest = false;
    QTime commandTiming;

    void doWork();
    void pollForNewWork(long delay);
    void getClip();
public:
    ClipperManager();
    ClipperManager(ClipperCommandQueue* _clipperCommandQueue);
    ~ClipperManager();

    void setQueue(ClipperCommandQueue* _clipperCommandQueue);

Q_SIGNALS:
    void initWorker();
    void startNewCommand(ClipperCommand *command);
    void downloadProgress();
    void cancelCommand();
    void doingCancelCommand();
    void commandCanceled(QString title);
    void clearedAllCommand();

public Q_SLOTS:
    void onStartWorking();
    void onCancelCommand();
    void onCommandCanceled();
    void onClearAllCommand();

    void onDownloadProgress(qint64 commandId, double processingPercent, double dataPercent);
    void onPostProcessing();
    void onGetClipDone(int statusCode, SegmentedRange *parts);
};

#endif // CLIPPERMANAGER_H
