#include "dtsmap2.h"

DTSInfo::DTSInfo() {
    pkt_id1 = dts1 = pkt_id2 = dts2 = 0;
}

DTSInfo::DTSInfo(int _pkt_id1, int _dts1, int _pkt_id2, int _dts2) {
    pkt_id1 = _pkt_id1;
    dts1 = _dts1;
    pkt_id2 = _pkt_id2;
    dts2 = _dts2;
}

DTSMap2::DTSMap2() {
}


void DTSMap2::add(long second, DTSInfo dtsInfo) {
    dtsInfosLock.lock();
    dtsInfos.insert(second, dtsInfo);
    dtsInfosLock.unlock();
}

DTSInfo DTSMap2::get(long second) {
    DTSInfo dtsInfo;

    dtsInfosLock.lock();
    if (dtsInfos.contains(second))
        dtsInfo = dtsInfos.value(second);
    dtsInfosLock.unlock();

    return dtsInfo;
}

bool DTSMap2::contains(long second) {
    bool ret;

    dtsInfosLock.lock();
    ret = dtsInfos.contains(second);
    dtsInfosLock.unlock();

    return ret;
}

void DTSMap2::clear() {
    dtsInfosLock.lock();
    dtsInfos.clear();
    dtsInfosLock.unlock();
}

