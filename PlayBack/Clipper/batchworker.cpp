#include "batchworker.h"

BatchWorker::BatchWorker(int id)
{
    doDebug = false;

    name = "BatchWorker" + QString::number(id);

    downloader = Q_NULLPTR;
    downloader2 = Q_NULLPTR;
    nbtpParser = Q_NULLPTR;

#ifdef MULTIPLE_NETWORK_ACCESS_MANAGER
    MAX_DOWNLOADING = MAX_NETWORK_ACCESS_MANAGER;
#else    
    MAX_DOWNLOADING = MAX_HANDLE;
#endif
    DOWNLOAD_TIMEOUT = 20000;
    DOWNLOAD_RETRY = 3;
}

void BatchWorker::onInitialize() {
    if (doDebug)
        qDebug() << name << "onInitialize";

    if (downloader == Q_NULLPTR) {
        downloader = new Downloader;
        connect(downloader, &Downloader::downloadedPayload, this, &BatchWorker::onDownloadedPayload);
    }

    if (downloader2 == Q_NULLPTR) {
        downloader2 = new Downloader2;
        connect(downloader2, &Downloader2::downloadedPayload, this, &BatchWorker::onDownloadedPayload);
        connect(downloader2, &Downloader2::canceledAllRequests, this, &BatchWorker::batchCanceled);
    }

    if (nbtpParser == Q_NULLPTR) {
        nbtpParser = new NBTPParser;
        nbtpParserThread = new QThread;
        nbtpParser->moveToThread(nbtpParserThread);
        connect(this, &BatchWorker::parseNbtp, nbtpParser, &NBTPParser::onParseNbtpRequest);
        connect(nbtpParser, &NBTPParser::h264Payload, this, &BatchWorker::onH264Payload);
        nbtpParserThread->start();
    }

    Q_EMIT inited();
}

void BatchWorker::onNewBatch(long _batchIdx, QList<long> _segmentIdxs, QList<QString> _segmentUrls) {
    if (doDebug)
        qDebug() << name << "onNewBatch" << _batchIdx << segmentIdxs.size() << _segmentUrls.size();

    segmentCount = 0;
    nProcessedSegments = 0;
    nDownloadingSegment = 0;

    batchIdx = _batchIdx;
    segmentIdxs = _segmentIdxs;
    segmentUrls = _segmentUrls;

    nSegmentsRequested = segmentIdxs.size();

    doDownloadSegments();
}

void BatchWorker::doDownloadSegments() {
    while (segmentCount < nSegmentsRequested && nDownloadingSegment < MAX_DOWNLOADING) {
        long segmentIdx = segmentIdxs.at(segmentCount);
        QString segmentUrl = segmentUrls.at(segmentCount);

#if DOWNLOADER==NETWORK_ACCESS_MANAGER
        downloader->onDownloadRequest(segmentIdx, segmentUrl, DOWNLOAD_TIMEOUT, DOWNLOAD_RETRY);
#elif DOWNLOADER==EASY_HANDLE_CURL
        downloader2->onDownloadRequest(segmentIdx, segmentUrl, DOWNLOAD_TIMEOUT, DOWNLOAD_RETRY);
#else
#endif
        segmentCount++;
        nDownloadingSegment++;
    }
}

void BatchWorker::onDownloadedPayload(long key, QByteArray payload) {
    int segmentIdx = key;

    if (doDebug)
        qDebug() << name << "onDownloadedPayload" << "batchIdx" << batchIdx << "segmentIdx" << segmentIdx << payload.size() << nDownloadingSegment;

    nDownloadingSegment--;
    doDownloadSegments();

    if (payload.size() > 0) {
        Q_EMIT parseNbtp(segmentIdx, payload);
    } else {
        onH264Payload(segmentIdx, QByteArray(), Q_NULLPTR);
    }
}

void BatchWorker::onH264Payload(long segmentIdx, QByteArray payload, SegmentedRange *segmentParts) {
    bool downloadOk = false;
    if (payload.size() > 0 && segmentParts != Q_NULLPTR) {
        if (doDebug)
            qDebug() << name << "onH264Payload" << "batchIdx" << batchIdx << "segmentIdx" << segmentIdx << payload.size() << segmentParts->toString();
        segmentIdx2H264Payload.insert(segmentIdx, payload);
        segmentIdx2SegmentParts.insert(segmentIdx, segmentParts);

        downloadOk = true;
    }

    Q_EMIT downloadProgress(batchIdx, segmentIdx, downloadOk);

    nProcessedSegments++;
    if (nProcessedSegments == nSegmentsRequested)
        handleBatch();
}

QString BatchWorker::getName() const
{
    return name;
}

void BatchWorker::setName(const QString &value)
{
    name = value;
}

void BatchWorker::handleBatch() {
    if (doDebug)
        qDebug() << name << "handleBatch" << batchIdx;

    QString tempH264;
    if (!segmentIdx2H264Payload.isEmpty()) {
        tempH264 = QDir::tempPath() + "/" + QString::number(batchIdx) + ".h264";
        QFile tempH264File(tempH264);
        if (tempH264File.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QMapIterator<long, QByteArray> it(segmentIdx2H264Payload);
            while (it.hasNext()) {
                it.next();

                QByteArray h264payload = it.value();
                tempH264File.write(h264payload);
            }
            tempH264File.close();
        } else {
            qDebug() << name << "handleBatch failed to open temp file" << batchIdx;
            tempH264 = "";
        }
        segmentIdx2H264Payload.clear();
    }
    SegmentedRange *batchParts = joinBatchSegments();

    Q_EMIT batchFinished(batchIdx, nSegmentsRequested, tempH264, batchParts);
}

SegmentedRange *BatchWorker::joinBatchSegments() {
    if (doDebug)
        qDebug() << name << "joinBatchSegments" << batchIdx;

    SegmentedRange *parts = new SegmentedRange;
    if (!segmentIdx2SegmentParts.isEmpty()) {

        QMapIterator<long, SegmentedRange *> it(segmentIdx2SegmentParts);
        while (it.hasNext()) {
            it.next();

            SegmentedRange* segmentParts = it.value();
            QList<QPair<long, long>> pairs = segmentParts->getSegments();
            delete segmentParts;

            for (int i = 0; i < pairs.size(); i++) {
                QPair<long, long> pair = pairs.at(i);
                for (long j = pair.first; j <= pair.second; j++) {
                    parts->insert(j);
                }
            }
        }
        segmentIdx2SegmentParts.clear();
    }

    return parts;
}

void BatchWorker::onCancelBatch() {
#if DOWNLOADER==NETWORK_ACCESS_MANAGER
    downloader->onCancelAllRequests();
    Q_EMIT batchCanceled();
#elif DOWNLOADER==EASY_HANDLE_CURL
    downloader2->onCancelAllRequests();
#else
#endif
}
