#ifndef DOWNLOADER2_H
#define DOWNLOADER2_H

#include <QObject>
#include <QDebug>
#include "easyhandle.h"

#define MAX_HANDLE 10

class Downloader2 : public QObject
{
    Q_OBJECT

    bool doDebug;

    QList<EasyHandle *> freeHandles;
    QList<EasyHandle *> runningHandles;
    QMap<EasyHandle *, long> handle2key;
public:
    Downloader2();

    void onDownloadRequest(long key, QString url, long timeout, int retry);
    void onCancelAllRequests();

Q_SIGNALS:
    void downloadedPayload(long key, QByteArray payload);
    void canceledAllRequests();

public Q_SLOTS:
    void onDownloadFinished(long statusCode, QByteArray payload);
    void onAborted();
};

#endif // DOWNLOADER2_H
