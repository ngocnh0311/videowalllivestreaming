#ifndef SEGMENTEDRANGE_H
#define SEGMENTEDRANGE_H

#include <QMap>
#include <QList>
#include <QPair>
#include <QMutex>
#include <QDebug>

class SegmentedRange
{
    QMutex segmentsLock;
    QList<QPair<long, long>> segments;

public:
    explicit SegmentedRange();

    void clear();
    int size();
    QPair<long, long> at(int index);
    QList<QPair<long, long>> getSegments();

    void insert(long val);
    long netLength();

    QString toString();
};

#endif // SEGMENTEDRANGE_H
