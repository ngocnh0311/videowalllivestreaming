#ifndef CLIPPERCOMMAND_H
#define CLIPPERCOMMAND_H

#include <QString>
#include <QJsonObject>
#include <QDebug>
#include <QFile>
#include "Common/generaldefine.h"
enum CommandType{
    TYPE_IMAGE = 0,
    TYPE_VIDEO = 1
};

class ClipperCommand
{
    qint64 commandId;
    int commandType = TYPE_VIDEO;
    QString storageBasePath;

    QString title;
    QString hostAddress;
    QString channelName;
    qint64 startTimestamp;
    qint64 endTimestamp;

    double percent;
    double dataPercent;
    QString outputFilename;
    QString filePath;
    bool done;
    QString statusCode;

public:
    ClipperCommand();
    ~ClipperCommand();
    ClipperCommand(QJsonObject json);
    ClipperCommand(QString _title, QString _hostAddress, QString _channelName, qint64 _startTimestamp, qint64 _endTimestamp);

    QJsonObject toJson();

    qint64 getCommandId() const;
    void setCommandId(const qint64 &value);
    QString getTitle() const;
    void setTitle(const QString &value);
    QString getHostAddress() const;
    void setHostAddress(const QString &value);
    QString getChannelName() const;
    void setChannelName(const QString &value);
    qint64 getStartTimestamp() const;
    void setStartTimestamp(const qint64 &value);
    qint64 getEndTimestamp() const;
    void setEndTimestamp(const qint64 &value);

    double getPercent() const;
    void setPercent(double value);
    double getDataPercent() const;
    void setDataPercent(double value);
    QString getOutputFilename() const;
    void setOutputFilename(const QString &value);

    bool getDone() const;
    void setDone(bool value);
    QString getStatusCode() const;
    void setStatusCode(const QString &value);
    QString getStorageBasePath() const;
    void setStorageBasePath(const QString &value);
    int getCommandType() const;
    void setCommandType(int value);
    QString getFilePath() const;
    void setFilePath(const QString &value);
};

#endif // CLIPPERCOMMAND_H
