#ifndef CLIPPERWORKER_H
#define CLIPPERWORKER_H

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QProcess>
#include <QMapIterator>
#include "clippercommandqueue.h"
#include "downloader.h"
#include "M3u8Parser/M3U8Parser.h"
#include "nbtpparser.h"
#include "batchworker.h"
#include "easyhandle.h"
#include "common.h"
#include <QDir>
class ClipperWorker : public QObject
{
    Q_OBJECT

    bool doDebug;

    long MAX_SEGMENT;
    long SEGMENT_PER_BATCH;
    long MAX_BATCH_WORKER;

    QProcess *workerProcess;
    Downloader *downloader;
    EasyHandle *easyHandle;
    M3U8Parser *m3u8Parser;

    QList<BatchWorker *> freeBatchWorkers;
    QList<BatchWorker *> busyBatchWorkers;
    QList<QThread *> batchWorkerThreads;
    long nInitedBatchWorker;

    ClipperCommand* command;
    bool isCancelRequested;

    QString baseUrl;
    QString m3u8Url;

    QList<MediaSegment> mediaSegments;
    long checkStartTimestampMsec;
    long checkEndTimestampMsec;

    long segmentCount;
    long nProcessedSegment;
    long nReceivedMsec;

    double lastEmitProcessingPercent;
    double lastEmitDataPercent;

    QMap<long, SegmentedRange *> batchIdx2Parts;
    QMap<long, QString> batchIdx2TmpFile;

    CLipperReport clipperReport;

    void setCommand(ClipperCommand* _command);
    void updateInfo();
    void updateCheckPoint();
    void downloadSegments();
    long getSegmentMillisecond(long segmentIdx);
    void handleBatch(long batchIndex);
    void doPostProcessing();
    int convertH264RawToMp4();
    SegmentedRange *joinBatchSegments(long batchIdx);
    SegmentedRange *joinSegments();
    void doReport(int statusCode, SegmentedRange *segment);
    void removeTmpFiles();
    void doCleanUp();

public:
    ClipperWorker();

Q_SIGNALS:
    void inited();

    void downloadProgress(qint64 commandId, double processingPercent, double dataPercent);
    void postProcessing();
    void getClipDone(int statusCode, SegmentedRange *segments);
    void commandCanceled();

    void killProcess();

public Q_SLOTS:
    void onInitialize();
    void onBatchWokerInited();
    void onStartNewCommand(ClipperCommand *command);
    void onCancelCommand();

    void onDownloadedPayload(long key, QByteArray payload);

    void onDownloadProgress(long batchIdx, long segmentIdx, bool downloadOk);
    void onBatchFinished(long batchIdx, long nSegments, QString tempH264, SegmentedRange *batchParts);
    void onBatchCanceled();
    void onCurlEasyHandleFinished(long statusCode, QByteArray payload);
};

#endif // CLIPPERWORKER_H
