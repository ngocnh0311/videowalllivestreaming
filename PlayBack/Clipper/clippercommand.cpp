#include "clippercommand.h"

QString ClipperCommand::getStorageBasePath() const
{
    return storageBasePath;
}

void ClipperCommand::setStorageBasePath(const QString &value)
{
    storageBasePath = value;
    if(commandType == TYPE_VIDEO){
        filePath = storageBasePath + "/" +  outputFilename + ".mp4";
    }
    //qDebug() << Q_FUNC_INFO << storageBasePath << outputFilename << h264Path << filePath;
}

int ClipperCommand::getCommandType() const
{
    return commandType;
}

void ClipperCommand::setCommandType(int value)
{
    commandType = value;
}

QString ClipperCommand::getFilePath() const
{
    return filePath;
}

void ClipperCommand::setFilePath(const QString &value)
{
    filePath = value;
}

ClipperCommand::ClipperCommand() {

}

ClipperCommand::~ClipperCommand() {
    qDebug() << Q_FUNC_INFO << outputFilename;
    QFile mp4(filePath);
    if (mp4.exists()) {
        mp4.remove();
    }
}

ClipperCommand::ClipperCommand(QString _title, QString _hostAddress, QString _channelName, qint64 _startTimestamp, qint64 _endTimestamp)
{
    title = _title;
    hostAddress = _hostAddress;
    channelName = _channelName;
    startTimestamp = _startTimestamp;
    endTimestamp = _endTimestamp;

    percent = 0;
    dataPercent = 0;
    QString dateTimeStringStart = GeneralDefine::instance().convertSecondsToTexts(startTimestamp).day + "_" + GeneralDefine::instance().convertSecondsToTexts(startTimestamp).time;
    QString dateTimeStringEnd = GeneralDefine::instance().convertSecondsToTexts(endTimestamp).day + "_" + GeneralDefine::instance().convertSecondsToTexts(endTimestamp).time;
    outputFilename = title + "_" +  dateTimeStringStart + "-" + dateTimeStringEnd;
    filePath = "";

    done = false;
    statusCode = "";
}

ClipperCommand::ClipperCommand(QJsonObject json) {
    title = json["title"].toString();
    commandType = json["commandType"].toInt();
    hostAddress = json["hostAddress"].toString();
    channelName = json["channelName"].toString();
    startTimestamp = json["startTimestamp"].toInt();
    endTimestamp = json["endTimestamp"].toInt();

    percent = json["percent"].toDouble();
    dataPercent = json["dataPercent"].toDouble();
    outputFilename = json["outputFilename"].toString();
    QString dateTimeStringStart = GeneralDefine::instance().convertSecondsToTexts(startTimestamp).day + "_" + GeneralDefine::instance().convertSecondsToTexts(startTimestamp).time;
    QString dateTimeStringEnd = GeneralDefine::instance().convertSecondsToTexts(endTimestamp).day + "_" + GeneralDefine::instance().convertSecondsToTexts(endTimestamp).time;
    if (outputFilename.isEmpty()) {
        outputFilename = title + "_" +  dateTimeStringStart + "-" + dateTimeStringEnd;
    }
    filePath = json["filePath"].toString();

    done = json["done"].toBool();
    statusCode = json["statusCode"].toString();
}

QJsonObject ClipperCommand::toJson() {
    QJsonObject clipperCommand;

    clipperCommand["title"] = title;
    clipperCommand["commandType"] = commandType;

    clipperCommand["hostAddress"] = hostAddress;
    clipperCommand["channelName"] = channelName;
    clipperCommand["startTimestamp"] = startTimestamp;
    clipperCommand["endTimestamp"] = endTimestamp;

    clipperCommand["percent"] = percent;
    clipperCommand["dataPercent"] = dataPercent;
    clipperCommand["outputFilename"] = outputFilename;
    clipperCommand["filePath"] = filePath;

    clipperCommand["done"] = done;
    clipperCommand["statusCode"] = statusCode;

    return clipperCommand;
}

qint64 ClipperCommand::getCommandId() const
{
    return commandId;
}

void ClipperCommand::setCommandId(const qint64 &value)
{
    commandId = value;
}

QString ClipperCommand::getTitle() const
{
    return title;
}

void ClipperCommand::setTitle(const QString &value)
{
    title = value;
}

QString ClipperCommand::getHostAddress() const
{
    return hostAddress;
}

void ClipperCommand::setHostAddress(const QString &value)
{
    hostAddress = value;
}

QString ClipperCommand::getChannelName() const
{
    return channelName;
}

void ClipperCommand::setChannelName(const QString &value)
{
    channelName = value;
}

qint64 ClipperCommand::getStartTimestamp() const
{
    return startTimestamp;
}

void ClipperCommand::setStartTimestamp(const qint64 &value)
{
    startTimestamp = value;
}

qint64 ClipperCommand::getEndTimestamp() const
{
    return endTimestamp;
}

void ClipperCommand::setEndTimestamp(const qint64 &value)
{
    endTimestamp = value;
}

double ClipperCommand::getPercent() const
{
    return percent;
}

void ClipperCommand::setPercent(double value)
{
    percent = value;
}

double ClipperCommand::getDataPercent() const
{
    return dataPercent;
}

void ClipperCommand::setDataPercent(double value)
{
    dataPercent = value;
}

QString ClipperCommand::getOutputFilename() const
{
    return outputFilename;
}

void ClipperCommand::setOutputFilename(const QString &value)
{
    outputFilename = value;
}

bool ClipperCommand::getDone() const
{
    return done;
}

void ClipperCommand::setDone(bool value)
{
    done = value;
}

QString ClipperCommand::getStatusCode() const
{
    return statusCode;
}

void ClipperCommand::setStatusCode(const QString &value)
{
    statusCode = value;
}
