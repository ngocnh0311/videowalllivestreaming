#include "downloader.h"

Downloader::Downloader()
{
    doDebug = true;

#ifdef MULTIPLE_NETWORK_ACCESS_MANAGER
    for (int i = 0; i < MAX_NETWORK_ACCESS_MANAGER; i++) {
        QNetworkAccessManager *nam = new QNetworkAccessManager;
        naManagers.append(nam);
    }
#else
    naManager = new QNetworkAccessManager;
#endif
}

void Downloader::onDownloadRequest(long key, QString url, long timeout, int retry) {
#ifdef MULTIPLE_NETWORK_ACCESS_MANAGER
    QNetworkAccessManager *naManager;
    if (!naManagers.isEmpty()) {
        naManager = naManagers.takeFirst();
        //qDebug() << "onDownloadRequest" << naManager;
    } else {
        return;
    }
#endif

    QTimer *timer = new QTimer;
    timer->setSingleShot(true);
    connect(timer, &QTimer::timeout, this, &Downloader::onDlTimerTimeout);

    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    QNetworkReply *reply = naManager->get(req);
    connect(reply, &QNetworkReply::finished, this, &Downloader::onDownloadFinishedByReply);
    //    connect(reply, &QNetworkReply::downloadProgress, this, &Downloader::onDownloadProgress);
    reply2Key.insert(reply, key);
    reply2Timer.insert(reply, timer);
    reply2RetryTimes.insert(reply, retry);

    timer2Reply.insert(timer, reply);
    timer->start(timeout);
}

void Downloader::onDownloadFinishedByReply() {
    QNetworkReply *reply = (QNetworkReply *) QObject::sender();
    onDownloadFinished(reply);
}

void Downloader::onDownloadProgress(qint64 bytesReceived, qint64 bytesTotal) {
    QNetworkReply *reply = (QNetworkReply *) QObject::sender();
    qDebug() << "onDownloadProgress" << reply->url().toString() << bytesReceived << bytesTotal;
}

void Downloader::onDownloadFinished(QNetworkReply *reply) {
    QTimer *timer;
    timer = reply2Timer.take(reply);
    disconnect(timer, &QTimer::timeout, this, &Downloader::onDlTimerTimeout);
    timer->stop();

    long key = reply2Key.take(reply);

    int retryTime = reply2RetryTimes.take(reply);
    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QString url = reply->url().toString();
    if (statusCode == 200 && reply->bytesAvailable() > 0) {
#ifdef MULTIPLE_NETWORK_ACCESS_MANAGER
        QNetworkAccessManager *nam = reply->manager();
        naManagers.append(nam);
#endif
        timer2Reply.remove(timer);
        timer->deleteLater();

        //        if (doDebug)
        //            qDebug() << "onDownloadFinished" << "+++ok" << url;

        Q_EMIT downloadedPayload(key, reply->readAll());
    } else {
#ifdef MULTIPLE_NETWORK_ACCESS_MANAGER
        QNetworkAccessManager *nam = reply->manager();
        nam->deleteLater();
        QNetworkAccessManager *naManager = new QNetworkAccessManager;
#endif
        if (retryTime > 0) {
            if (doDebug)
                qDebug() << "onDownloadFinished" << "---retry" << url << "statusCode" << statusCode << "retryTime" << retryTime;

            doRetry(naManager, key, timer, url, retryTime);
        } else {
            if (doDebug)
                qDebug() << "onDownloadFinished" << "---failed" << url << "statusCode" << statusCode;

            timer2Reply.remove(timer);
            timer->deleteLater();
#ifdef MULTIPLE_NETWORK_ACCESS_MANAGER
            naManagers.append(naManager);
#endif
            Q_EMIT downloadedPayload(key, QByteArray());
        }
    }

    reply->deleteLater();
}

void Downloader::onDlTimerTimeout() {
    QTimer *timer = (QTimer *) QObject::sender();
    disconnect(timer, &QTimer::timeout, this, &Downloader::onDlTimerTimeout);

    QNetworkReply *reply = timer2Reply.value(timer);
    QString url = reply->url().toString();
    int retryTime = reply2RetryTimes.take(reply);

    long key = reply2Key.take(reply);
#ifdef MULTIPLE_NETWORK_ACCESS_MANAGER
    QNetworkAccessManager *nam = reply->manager();
    nam->deleteLater();
    QNetworkAccessManager *naManager = new QNetworkAccessManager;
#endif

    reply2Timer.remove(reply);
    disconnect(reply, &QNetworkReply::finished, this, &Downloader::onDownloadFinishedByReply);
    reply->abort();
    reply->deleteLater();

    if (retryTime > 0) {
        if (doDebug)
            qDebug() << "onDlTimerTimeout" << "---retry" << url << "retryTime" << retryTime << "timerInterval" << timer->interval();

        doRetry(naManager, key, timer, url, retryTime);
    } else {
        if (doDebug)
            qDebug() << "onDlTimerTimeout" << "---failed" << url;

        timer2Reply.remove(timer);
        timer->deleteLater();
#ifdef MULTIPLE_NETWORK_ACCESS_MANAGER
        naManagers.append(naManager);
#endif

        Q_EMIT downloadedPayload(key, QByteArray());
    }
}

inline void Downloader::doRetry(QNetworkAccessManager* naManager, long key, QTimer *timer, QString url, long retryTime) {
    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    QNetworkReply *retryReply = naManager->get(req);
    connect(timer, &QTimer::timeout, this, &Downloader::onDlTimerTimeout);
    timer->start();

    reply2Key.insert(retryReply, key);
    reply2Timer.insert(retryReply, timer);
    reply2RetryTimes.insert(retryReply, --retryTime);
    timer2Reply.insert(timer, retryReply);
}

void Downloader::onCancelAllRequests() {
    if (doDebug)
        qDebug() << Q_FUNC_INFO;

    QMapIterator<QNetworkReply *, QTimer *> reply2TimerIt(reply2Timer);
    while (reply2TimerIt.hasNext()) {
        reply2TimerIt.next();

        QNetworkReply *reply = reply2TimerIt.key();
        disconnect(reply, &QNetworkReply::finished, this, &Downloader::onDownloadFinishedByReply);
        reply->abort();
        reply->deleteLater();

        QTimer *timer = reply2TimerIt.value();
        disconnect(timer, &QTimer::timeout, this, &Downloader::onDlTimerTimeout);
        timer->stop();
        timer->deleteLater();
    }

    reply2Key.clear();
    reply2Timer.clear();
    reply2RetryTimes.clear();
    timer2Reply.clear();
}
