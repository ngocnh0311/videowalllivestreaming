#ifndef BATCHWORKER_H
#define BATCHWORKER_H

#include <QObject>
#include <QDir>
#include <QThread>
#include <QDebug>
#include "nbtpparser.h"
#include "downloader.h"
#include "downloader2.h"

#define NETWORK_ACCESS_MANAGER 0
#define EASY_HANDLE_CURL 1

#define DOWNLOADER EASY_HANDLE_CURL

class BatchWorker : public QObject
{
    Q_OBJECT
    bool doDebug;
    QString name;

    long batchIdx;
    QList<long> segmentIdxs;
    QList<QString> segmentUrls;
    long segmentCount;
    long MAX_DOWNLOADING;
    long DOWNLOAD_TIMEOUT;
    long DOWNLOAD_RETRY;
    long nDownloadingSegment;
    long nSegmentsRequested;
    long nProcessedSegments;

    Downloader *downloader = Q_NULLPTR;
    Downloader2 *downloader2 = Q_NULLPTR;
    NBTPParser *nbtpParser = Q_NULLPTR;
    QThread *nbtpParserThread = Q_NULLPTR;

    QMap<long, QByteArray> segmentIdx2H264Payload;
    QMap<long, SegmentedRange *> segmentIdx2SegmentParts;

    void doDownloadSegments();
    void handleBatch();
    SegmentedRange *joinBatchSegments();

public:
    BatchWorker(int id);

    QString getName() const;
    void setName(const QString &value);
Q_SIGNALS:
    void inited();
    void parseNbtp(long segmentIdx, QByteArray payload);
    void batchFinished(long batchIdx, long nSegments, QString tempH264, SegmentedRange *batchParts);
    void batchCanceled();

    void downloadProgress(long batchIdx, long segmentIdx, bool downloadOk);

public Q_SLOTS:
    void onInitialize();

    void onNewBatch(long _batchIdx, QList<long> _segmentIdxs, QList<QString> _urls);
    void onDownloadedPayload(long key, QByteArray payload);
    void onH264Payload(long segmentIdx, QByteArray payload, SegmentedRange *segmentParts);

    void onCancelBatch();
};

#endif // BATCHWORKER_H
