#include "clippermanager.h"

ClipperManager::ClipperManager()
{
    qDebug() << Q_FUNC_INFO;

    clipperCommandQueue = Q_NULLPTR;

    pollingTimer = Q_NULLPTR;

    clipperWorker = new ClipperWorker();
    clipperWorkerThread = new QThread();
    clipperWorker->moveToThread(clipperWorkerThread);
    clipperWorkerThread->start();

    connect(this, &ClipperManager::initWorker, clipperWorker, &ClipperWorker::onInitialize);
    connect(clipperWorker, &ClipperWorker::inited, this, &ClipperManager::doWork);
    connect(this, &ClipperManager::startNewCommand, clipperWorker, &ClipperWorker::onStartNewCommand);
    connect(this, &ClipperManager::cancelCommand, clipperWorker, &ClipperWorker::onCancelCommand);

    connect(clipperWorker, &ClipperWorker::commandCanceled, this, &ClipperManager::onCommandCanceled);
    connect(clipperWorker, &ClipperWorker::downloadProgress, this, &ClipperManager::onDownloadProgress);
    connect(clipperWorker, &ClipperWorker::postProcessing, this, &ClipperManager::onPostProcessing);
    connect(clipperWorker, &ClipperWorker::getClipDone, this, &ClipperManager::onGetClipDone);

    isWorkerBusy = false;
}

ClipperManager::ClipperManager(ClipperCommandQueue* _clipperCommandQueue)
    : ClipperManager()
{
    clipperCommandQueue = _clipperCommandQueue;
}

ClipperManager::~ClipperManager() {
    qDebug() << Q_FUNC_INFO;
}

void ClipperManager::setQueue(ClipperCommandQueue* _clipperCommandQueue) {
    clipperCommandQueue = _clipperCommandQueue;
}

void ClipperManager::onStartWorking() {
    if (pollingTimer == Q_NULLPTR) {
        pollingTimer = new QTimer();
        pollingTimer->setSingleShot(true);

        connect(pollingTimer, &QTimer::timeout, this, &ClipperManager::doWork);
    }

    commandTiming.start();

    Q_EMIT initWorker();
}

void ClipperManager::doWork() {
    if (!isWorkerBusy) {
        command = clipperCommandQueue->pendingCommandDequeue();
        if (command == Q_NULLPTR || command->getTitle().isEmpty()) {
            pollForNewWork(100);
            return;
        }

        getClip();
    }
}

void ClipperManager::pollForNewWork(long delay) {
    pollingTimer->start(delay);
}

void ClipperManager::getClip() {
    commandTiming.restart();
    Q_EMIT startNewCommand(command);
    isWorkerBusy = true;
}

void ClipperManager::onDownloadProgress(qint64 commandId, double processingPercent, double dataPercent) {
    qDebug() << "ClipperManager::onDownloadProgress" << commandId << processingPercent << dataPercent;
    command->setPercent(processingPercent);
}

void ClipperManager::onPostProcessing() {
    qDebug() << Q_FUNC_INFO;
}

void ClipperManager::onGetClipDone(int statusCode, SegmentedRange *parts) {
    if(command != Q_NULLPTR){
        qDebug() << "===" << Q_FUNC_INFO;
        qDebug() << "===" << command->getTitle();
        qDebug() << "===" << clipperReport.toString(statusCode);
        qDebug() << "===" << "spent time" << commandTiming.elapsed() / 1000 << "seconds";
        command->setStatusCode(clipperReport.toString(statusCode));
        command->setDone(true);
        if (statusCode == clipperReport.CLIPPER_FINISHED) {
            qDebug() << "===" << command->getPercent();
            qDebug() << "===" << command->getFilePath();
            if (parts != Q_NULLPTR) {
                qDebug() << "===" << parts->toString() << "netSecond" << parts->netLength();
                delete parts;
            }
        }else{
            //download failure
            command->setPercent(100);
        }
        clipperCommandQueue->pendingCommandRemoveFirst();
        isWorkerBusy = false;
        pollForNewWork(0);
    }
}

void ClipperManager::onCancelCommand() {
    if (isWorkerBusy && !isCancelRequest && !isClearAllRequest) {
        isCancelRequest = true;
        Q_EMIT cancelCommand();
        Q_EMIT doingCancelCommand();
    }
}

void ClipperManager::onCommandCanceled() {
    if (isCancelRequest) {
        QString commandTitle = command->getTitle();

        clipperCommandQueue->remove(command->getCommandId());

        Q_EMIT commandCanceled(commandTitle);
        isCancelRequest = false;
    }

    if (isClearAllRequest) {
        clipperCommandQueue->clear();

        Q_EMIT clearedAllCommand();
        isClearAllRequest = false    ;
    }

    isWorkerBusy = false;
    pollForNewWork(0);
}

void ClipperManager::onClearAllCommand() {
    if (!isClearAllRequest) {
        isClearAllRequest = true;
        Q_EMIT cancelCommand();
    }
}
