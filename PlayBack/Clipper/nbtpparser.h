#ifndef NBTPPARSER_H
#define NBTPPARSER_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include "common.h"
#include "segmentedrange.h"
#include "consts.h"
class NBTPParser : public QObject
{
    Q_OBJECT

    NALUnitType NALUnit;
    QByteArray NALUStartCode = QByteArray("\x00\x00\x01", 3);
    QByteArray h264PayloadData;

    bool IDRPictureStarted = false;
    QByteArray IDRPicture;

    long lastPacketSecond;
    SegmentedRange *segmentParts;

    void onReceiveH264Packet(int pkt_id, int naltype, long timestamp, QByteArray packet);

public:
    NBTPParser();

Q_SIGNALS:
    void h264Payload(long segmentIdx, QByteArray payload, SegmentedRange *segmentParts);

public Q_SLOTS:
    void onParseNbtpRequest(long segmentIdx, QByteArray payload);
};

#endif // NBTPPARSER_H
