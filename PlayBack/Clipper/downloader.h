#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>

//#define MULTIPLE_NETWORK_ACCESS_MANAGER
#define MAX_NETWORK_ACCESS_MANAGER 10

class Downloader : public QObject
{
    Q_OBJECT

    bool doDebug;

#ifdef MULTIPLE_NETWORK_ACCESS_MANAGER
    QList<QNetworkAccessManager *> naManagers;
#else
    QNetworkAccessManager *naManager;
#endif

    QMap<QNetworkReply *, QTimer *> reply2Timer;
    QMap<QTimer *, QNetworkReply *> timer2Reply;
    QMap<QNetworkReply *, int> reply2RetryTimes;
    QMap<QNetworkReply *, long> reply2Key;

    inline void doRetry(QNetworkAccessManager* naManager, long key, QTimer *timer, QString url, long retryTime);

public:
    Downloader();

    void onCancelAllRequests();
    void onDownloadRequest(long key, QString url, long timeout, int retry);

Q_SIGNALS:
    void downloadedPayload(long key, QByteArray payload);

private Q_SLOTS:
    void onDownloadFinishedByReply();
    void onDownloadFinished(QNetworkReply *reply);
    void onDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void onDlTimerTimeout();
};

#endif // DOWNLOADER_H
