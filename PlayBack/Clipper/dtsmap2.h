#ifndef DTSMAP2_H
#define DTSMAP2_H

#include <QMap>
#include <QMutex>

class DTSInfo
{
public:
    int pkt_id1;
    int dts1;
    int pkt_id2;
    int dts2;

    DTSInfo();
    DTSInfo(int pkt_id1, int dts1, int pkt_id2, int dts2);
};

class DTSMap2
{
    QMutex dtsInfosLock;
    QMap<long, DTSInfo> dtsInfos;
public:
    DTSMap2();

    void add(long second, DTSInfo dtsInfo);
    DTSInfo get(long second);
    bool contains(long second);
    void clear();
};

#endif // DTSMAP2_H
