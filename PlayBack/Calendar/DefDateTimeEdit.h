#ifndef DEFDATETIMEEDIT_H
#define DEFDATETIMEEDIT_H

#include <QDateTimeEdit>
#include "DefineCalendar.h"

class DefDateTimeEdit : public QDateTimeEdit
{
	Q_OBJECT

public:
	DefDateTimeEdit(QWidget *parent);
	~DefDateTimeEdit();
	void setMyStytle();

    void setCurrentDateTime(QDateTime time);

Q_SIGNALS:
    void updateDateTime(const QDateTime &dateTime);

protected Q_SLOTS:
	void getDateTime(const QDateTime &dateTime);
    void onDateTimeChange(const QDateTime &dateTime);

private:
	DefineCalendar *m_DefCalendar;
	
};

#endif // DEFDATETIMEEDIT_H
