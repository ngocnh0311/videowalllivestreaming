#include "p_pb_layoutselector.h"
#include "Common/LayoutSet.h"

P_PBLayoutSelector::P_PBLayoutSelector(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    zone->setStyleSheet("background-color: #222");

    layoutSet = new LayoutSet(Q_NULLPTR);

    layout = new QHBoxLayout();
    layout->setMargin(0);
    layout->setSpacing(5);
    zone->setLayout(layout);

    for (int index = 0; (index < layoutSet->layoutList.size()) &&
         (layoutSet->layoutList.at(index).numberOfCameras <=
          Message.APP_PLAY_BACK_MAX_NUMBER_OF_PLAYERS);
         ++index) {
        QPushButton *button = new QPushButton(zone);
        QFont font = button->font();
        font.setPixelSize(38);
        button->setFont(font);
        button->setText(layoutSet->layoutList.at(index).label);
        button->setStyleSheet("background-color: black; color: #b3b3b3;");
        button->setMinimumHeight(button->width());
        buttons.append(button);
        layout->addWidget(button);
        connect(button, &QPushButton::clicked, this,
                &P_PBLayoutSelector::onSelectedLayout);
    }


}

void P_PBLayoutSelector::onSelectedLayout() {
    QPushButton *button = qobject_cast<QPushButton *>(sender());
    if (button != Q_NULLPTR) {
        int clickedButtonIndex = buttons.indexOf(button);
        if (clickedButtonIndex >= 0 && clickedButtonIndex < buttons.size()) {
            if (selectedButtonIndex != clickedButtonIndex) {
                clearOldSelectedButton(this->selectedButtonIndex);
                CamSite *camSite = control()->appContext->getSiteCameras();
                if (camSite != Q_NULLPTR) {
                    int maxCameraDisplay = camSite->getMaxCameraDisplay();
                    if(control()->appContext->getIsLoadDataWithDeviceId() && layoutSet->layoutList.at(this->selectedButtonIndex).numberOfCameras > maxCameraDisplay){
                        calibrateLayoutSetRemoteControl();
                    }
                }
                selectedButtonIndex = clickedButtonIndex;
                highlightNewSelectedButton(clickedButtonIndex);
                QVariant *dataStruct = new QVariant();
                dataStruct->setValue(layoutSet->layoutList.at(clickedButtonIndex));
                control()->newUserAction(Message.APP_PLAY_BACK_ZONE_LAYOUT_SELECTED,
                                         dataStruct);
            }
        }
    }
}

void P_PBLayoutSelector::clearOldSelectedButton(int buttonIndex) {
    if (buttonIndex >= 0 && buttonIndex < buttons.size()) {
        buttons.at(buttonIndex)
                ->setStyleSheet("background-color: black; color: #b3b3b3;");
    }
}

void P_PBLayoutSelector::highlightNewSelectedButton(int buttonIndex) {
    if (buttonIndex >= 0 && buttonIndex < buttons.size()) {
        buttons.at(buttonIndex)
                ->setStyleSheet("background-color: red; color: #b3b3b3;");
    }
}


void P_PBLayoutSelector::calibrateLayoutSet() {
    CamSite *camSite = control()->appContext->getSiteCameras();
    if (camSite != Q_NULLPTR) {
        int numberOfCamerasOfSite;

        int indexLayoutActiveLast = - 1;
        QSettings settings;
        QString offlineMode = settings.value("offline_mode").toString();
        if(offlineMode == "ON"){
            numberOfCamerasOfSite= camSite->getCamItems().size();
        }else{
            numberOfCamerasOfSite = camSite->getTotalCamItem();
        }
        for (int index = 0; (index < layoutSet->layoutList.size()) &&
             (layoutSet->layoutList.at(index).numberOfCameras <=
              Message.APP_PLAY_BACK_MAX_NUMBER_OF_PLAYERS) && (index < buttons.size());
             ++index) {
            if (layoutSet->layoutList.at(index).numberOfCameras <
                    numberOfCamerasOfSite) {
                buttons.at(index)->setEnabled(true);
                buttons.at(index)->setStyleSheet(
                            "background-color: black; color: #b3b3b3;");
                indexLayoutActiveLast = index;
            } else {
                buttons.at(index)->setDisabled(true);
                buttons.at(index)->setStyleSheet(
                            "background-color: black; color: #444;");
            }
        }
        if(indexLayoutActiveLast >= 0 && (indexLayoutActiveLast+1) < buttons.size()){
            buttons.at(indexLayoutActiveLast+1)->setEnabled(true);
            buttons.at(indexLayoutActiveLast+1)->setStyleSheet(
                        "background-color: black; color: #b3b3b3;");
        }
    }
}


// Nếu số lượng cam nhỏ hơn layout thì không cho chọn
//void P_PBLayoutSelector::calibrateLayoutSetRemoteControl() {
//    CamSite *camSite = control()->appContext->getSiteCameras();
//    if (camSite != Q_NULLPTR) {
//        int numberOfCamerasOfSite = camSite->getCamItems().size();
//        qDebug() << "calibrateLayoutSetRemoteControl" << "numberOfCamerasOfSite" << numberOfCamerasOfSite;
//        for (int index = 0; (index < layoutSet->layoutList.size()) &&
//             (layoutSet->layoutList.at(index).numberOfCameras <=
//              Message.APP_PLAY_BACK_MAX_NUMBER_OF_PLAYERS);
//             ++index) {
//            qDebug() << "layout numberOfCameras" << layoutSet->layoutList.at(index).numberOfCameras << numberOfCamerasOfSite;
//            if (layoutSet->layoutList.at(index).numberOfCameras <
//                    numberOfCamerasOfSite) {
//                buttons.at(index)->setEnabled(true);
//                buttons.at(index)->setStyleSheet(
//                            "background-color: black; color: #b3b3b3;");
//            } else {
//                buttons.at(index)->setDisabled(true);
//                buttons.at(index)->setStyleSheet(
//                            "background-color: black; color: #444;");
//            }
//        }
//    }
//}




void P_PBLayoutSelector::calibrateLayoutSetRemoteControl() {
    CamSite *camSite = control()->appContext->getSiteCameras();
    if (camSite != Q_NULLPTR) {
        int indexLayoutActiveLast = -1;
        int maxCameraDisplay = camSite->getMaxCameraDisplay();
        int totalCameraOfDevice = camSite->getCamItems().size();
        int layoutCamerasMin = qMin(maxCameraDisplay , totalCameraOfDevice);
        for (int index = 0; (index < layoutSet->layoutList.size()) &&
             (layoutSet->layoutList.at(index).numberOfCameras <=
              Message.APP_PLAY_BACK_MAX_NUMBER_OF_PLAYERS) && (index < buttons.size());
             ++index) {
            if (layoutSet->layoutList.at(index).numberOfCameras <=
                    layoutCamerasMin) {
                buttons.at(index)->setEnabled(true);
                buttons.at(index)->setStyleSheet(
                            "background-color: black; color: #b3b3b3;");
                indexLayoutActiveLast = index;
            } else {
                buttons.at(index)->setDisabled(true);
                buttons.at(index)->setStyleSheet(
                            "background-color: black; color: #444;");
            }
        }
        if(indexLayoutActiveLast >= 0){
            LayoutStruct layoutLast = layoutSet->layoutList.at(indexLayoutActiveLast);
            int indexLayoutNext = indexLayoutActiveLast+1;
            if((layoutLast.numberOfCameras < maxCameraDisplay) && (layoutLast.numberOfCameras < totalCameraOfDevice) && (indexLayoutNext) < buttons.size()){
                LayoutStruct layoutNext = layoutSet->layoutList.at(indexLayoutNext);
                if(layoutNext.numberOfCameras <= maxCameraDisplay){
                    buttons.at(indexLayoutNext)->setEnabled(true);
                    buttons.at(indexLayoutNext)->setStyleSheet(
                                "background-color: black; color: #b3b3b3;");
                }
            }
        }
    }
}

void P_PBLayoutSelector::updateLayoutRemoteControl(int layoutSelected){
    if(layoutSelected > 25) return;
    qDebug() << Q_FUNC_INFO <<"updateLayoutRemoteControl" << "layoutSelected" << layoutSelected;
    int indexButtonSelected = layoutSet->getLayoutWithNumberOfCameras(layoutSelected).code;
    if (indexButtonSelected >= 0 && indexButtonSelected < buttons.size()) {
        clearOldSelectedButton(this->selectedButtonIndex);
        calibrateLayoutSetRemoteControl();
        highlightNewSelectedButton(indexButtonSelected);
        this->selectedButtonIndex = indexButtonSelected;

        QVariant *data = new QVariant();
        data->setValue<LayoutStruct>(layoutSet->getLayoutWithNumberOfCameras(layoutSelected));
        control()->newUserAction(Message.APP_PLAY_BACK_PAGE_UPDATE_REMOTE_CONTROL, data);
    }
}

void P_PBLayoutSelector::setDefaultLayout() {
    qDebug() << Q_FUNC_INFO;
    clearOldSelectedButton(this->selectedButtonIndex);

    calibrateLayoutSet();

    int selectedLayoutSaved = -1;
    int defaultLayoutIndex = 1;

    if (!checkLayoutSaved){
        checkLayoutSaved = true;
        QSettings settings;
        settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
        settings.beginGroup("videowall");
        selectedLayoutSaved = settings.value("selected_layout").toInt();
    }


    if(selectedLayoutSaved >= 0 && defaultLayoutIndex <= 4 && checkLayoutSaved == true){
        defaultLayoutIndex = selectedLayoutSaved;
    }
    if(this->control()->appContext->getSiteCameras()->getCamItems().size()  <= 1){
        defaultLayoutIndex = 0;
    }


    highlightNewSelectedButton(defaultLayoutIndex);
    this->selectedButtonIndex = defaultLayoutIndex;

    QVariant *data = new QVariant();
    data->setValue(layoutSet->layoutList.at(defaultLayoutIndex));
    control()->newUserAction(Message.APP_PLAY_BACK_PAGE_DEFAULT_SET, data);
}


void P_PBLayoutSelector::show(QVariant *attachment) { Q_UNUSED(attachment) }

void P_PBLayoutSelector::update() {}

QObject *P_PBLayoutSelector::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}
void P_PBLayoutSelector::enabledLayoutButton() {
    qDebug() << Q_FUNC_INFO;
    for (int index = 0; index < buttons.size(); ++index) {
        buttons.at(index)->setEnabled(true);
        buttons.at(index)->setStyleSheet(
                    ("background-color: black; color: #b3b3b3;"));
    }
    highlightNewSelectedButton(this->selectedButtonIndex);
}
void P_PBLayoutSelector::disabledLayoutButton() {
    for (int index = 0; index < buttons.size(); ++index) {
        buttons.at(index)->setDisabled(true);
        buttons.at(index)->setStyleSheet("background-color: black; color: #444;");
    }
}
