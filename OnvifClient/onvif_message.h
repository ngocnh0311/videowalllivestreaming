#include <QString>
#include <QtCore>

#define ONVIF_PROXY_PORT      2222
#define ONVIF_CAMERA_SELECTED 12000
#define ONVIF_RTSP_STREAM_CHANGED    12001
#define ONVIF_GET_USERPASS    12002
#define ONVIF_GET_IMAGE_SETTING    12003

struct OnvifInfo{
    QString dev_url;
    QString username;
    QString password;
    QString proxy_addr;
    int proxy_port;
};

Q_DECLARE_METATYPE(OnvifInfo)
