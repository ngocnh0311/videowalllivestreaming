#ifndef C_OVCameraConfig_H
#define C_OVCameraConfig_H
#include <QString>
#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>

#include "PacModel/control.h"

class P_OVCameraConfig;
class A_OVCameraConfig;

class C_OVCameraLive;
class C_OVOnvifControl;

class C_OVCameraConfig : public Control {
 public:
  QWidget* zone;

  C_OVCameraLive *cCameraLive = Q_NULLPTR;
  C_OVOnvifControl *cOnvifControl = Q_NULLPTR;

 public:
  C_OVCameraConfig(Control* ctrl, QWidget *_zone);
  ~C_OVCameraConfig();

//  void* getParent() { return this->parent; }
  P_OVCameraConfig* presentation() { return (P_OVCameraConfig*)pres; }
  A_OVCameraConfig* abstraction() { return (A_OVCameraConfig*)abst; }

  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_OVCameraConfig_H
