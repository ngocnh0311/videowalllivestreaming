#ifndef A_OVCameraConfig_H
#define A_OVCameraConfig_H

#include <QObject>

#include "PacModel/abstraction.h"

class C_OVCameraConfig;
class A_OVCameraConfig : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_OVCameraConfig(Control *ctrl);
    C_OVCameraConfig *control() { return (C_OVCameraConfig *)this->ctrl; }
    void changeControl(Control *ctrl);

public Q_SLOTS:

};

#endif  // A_OVCameraConfig_H
