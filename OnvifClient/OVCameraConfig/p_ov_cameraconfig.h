#ifndef P_OVCameraConfig_H
#define P_OVCameraConfig_H
#include <QObject>
#include <QLayout>
#include <QWidget>
#include <QScrollArea>

#include <PacModel/presentation.h>

class C_OVCameraConfig;

class P_OVCameraConfig : public Presentation {
    // init ui control
private:
  QWidget *cameraLive = Q_NULLPTR;
  QWidget *onvifControl = Q_NULLPTR;

public:
    QLayout *layout = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;


    P_OVCameraConfig(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_OVCameraConfig *control() { return (C_OVCameraConfig *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

protected:
};

#endif  // P_OVCameraConfig_H
