#include "p_ov_cameraconfig.h"
#include "a_ov_cameraconfig.h"
#include "c_ov_cameraconfig.h"

#include "onvif_message.h"

#include "OVCameraLive/c_ov_cameralive.h"
#include "OVOnvifControl/c_ov_onvifcontrol.h"

/**
  * Contructor. Register the father in the pac hierarchy.
  * @param ctrl The reference on the father. Generally the pac agent which create this agent.
  **/
C_OVCameraConfig::C_OVCameraConfig(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    this->zone = _zone;

    this->abst = new A_OVCameraConfig(this);
    this->pres = new P_OVCameraConfig(this, zone);

    cCameraLive = new C_OVCameraLive(this, presentation()->getZone(0));
    cOnvifControl = new C_OVOnvifControl(this, presentation()->getZone(1));
}

C_OVCameraConfig::~C_OVCameraConfig() {
  delete this->cOnvifControl;
  delete this->cCameraLive;

  delete this->abst;
  delete this->pres;
}

/**
  * Method to receive a message from the Presentation Facet.
  * @param message    : A string which describe the request
  * @param attachment : A ref on an eventual object necessary to treat the request
  **/
void C_OVCameraConfig::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case 1:
        break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from the Astraction Facet.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OVCameraConfig::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from an other agent.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OVCameraConfig::newAction(int message, QVariant* attachment) {
  switch (message) {
  case ONVIF_CAMERA_SELECTED:
    this->cOnvifControl->newAction(message, attachment);
    break;

  case ONVIF_RTSP_STREAM_CHANGED:
    this->cCameraLive->newAction(message,attachment);
    break;

  default:
      qDebug() << "ERROR : General Internal pac action in" + getClass() +
                  "non-catched :" + Message.toString(message);
  }
}
