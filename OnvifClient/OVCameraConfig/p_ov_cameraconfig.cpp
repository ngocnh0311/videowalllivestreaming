#include "p_ov_cameraconfig.h"
#include "c_ov_cameraconfig.h"

/**
  * Generic method to override for updating the presention.
  **/

P_OVCameraConfig::P_OVCameraConfig(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
  this->zone = _zone;

  this->layout = new QHBoxLayout();
  this->layout->setSpacing(0);
  this->zone->setLayout(this->layout);

  this->cameraLive = new QWidget(this->zone);
  this->cameraLive->setMinimumSize(320,240);

  this->onvifControl = new QWidget();
  this->onvifControl->setFixedWidth(450);

  this->layout->addWidget(this->cameraLive);
  this->layout->addWidget(this->onvifControl);
}

void P_OVCameraConfig::show(QWidget *zone) { Q_UNUSED(zone) }

void P_OVCameraConfig::update() {}

QWidget *P_OVCameraConfig::getZone(int zoneId) {
    switch (zoneId) {
    case 0:
        return this->cameraLive;
    case 1:
        return this->onvifControl;

    default:
        return Q_NULLPTR;
    }
}

void P_OVCameraConfig::enterFullscreenMode() {
  /* rightBar->setFixedWidth(0);*/
}

void P_OVCameraConfig::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
