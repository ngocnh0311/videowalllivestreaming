#include "p_ov_cameralive.h"
#include "a_ov_cameralive.h"
#include "c_ov_cameralive.h"

#include "onvif_message.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_OVCameraLive::C_OVCameraLive(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    this->zone = _zone;

    this->abst = new A_OVCameraLive(this);
    this->pres = new P_OVCameraLive(this, zone);
}

/**
  * Method to receive a message from the Presentation Facet.
  * @param message    : A string which describe the request
  * @param attachment : A ref on an eventual object necessary to treat the
  *request
  **/
void C_OVCameraLive::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case 1:
        break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_OVCameraLive::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_OVCameraLive::newAction(int message, QVariant* attachment) {
    switch (message) {
    case ONVIF_RTSP_STREAM_CHANGED:
      this->presentation()->play_rtsp_stream(attachment);
      break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
