#ifndef P_OVCameraLive_H
#define P_OVCameraLive_H
#include <QObject>
#include <QLayout>
#include <QWidget>
#include <QPushButton>
#include <QListWidget>

#include "PacModel/presentation.h"
#include "Player/mpvwidget.h"

class C_OVCameraLive;

class P_OVCameraLive : public Presentation {
    // init ui control
private:
public:
    QLayout *layout = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;

    MpvWidget *mpvPlayer = Q_NULLPTR;

    P_OVCameraLive(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_OVCameraLive *control() { return (C_OVCameraLive *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

    void play_rtsp_stream(QVariant* attachment);

protected:
};

#endif  // P_OVCameraLive_H
