#ifndef A_OVCameraLive_H
#define A_OVCameraLive_H

#include <QObject>

#include "PacModel/abstraction.h"

class C_OVCameraLive;
class A_OVCameraLive : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_OVCameraLive(Control *ctrl);
    C_OVCameraLive *control() { return (C_OVCameraLive *)this->ctrl; }
    void changeControl(Control *ctrl);

public Q_SLOTS:

};

#endif  // A_OVCameraLive_H
