#ifndef C_OVCameraLive_H
#define C_OVCameraLive_H
#include <QString>
#include <QDebug>
#include <QObject>

#include "PacModel/control.h"

class P_OVCameraLive;
class A_OVCameraLive;

class C_OVCameraLive;
class C_OVCameraConfig;

class C_OVCameraLive : public Control {
 public:
  QWidget* zone;

//  C_OVCameraLive *cCameraLive = Q_NULLPTR;
//  C_OVCameraConfig *cCameraConfig = Q_NULLPTR;

  C_OVCameraLive(Control* ctrl, QWidget *_zone);
  void* getParent() { return (void*)this->parent; }

  P_OVCameraLive* presentation() { return (P_OVCameraLive*)pres; }
  A_OVCameraLive* abstraction() { return (A_OVCameraLive*)abst; }

  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_OVCameraLive_H
