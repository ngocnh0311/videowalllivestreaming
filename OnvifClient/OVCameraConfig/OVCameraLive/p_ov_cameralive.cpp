#include "p_ov_cameralive.h"
#include "c_ov_cameralive.h"

/**
  * Generic method to override for updating the presention.
  **/

P_OVCameraLive::P_OVCameraLive(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
  this->zone = _zone;

  this->layout = new QVBoxLayout();
  this->layout->setSpacing(0);
  this->zone->setLayout(this->layout);

  this->mpvPlayer = new MpvWidget(this->zone);
  this->mpvPlayer->setStyleSheet("background:#000000");

  this->layout->addWidget(this->mpvPlayer);
}


void P_OVCameraLive::show(QWidget *zone) { Q_UNUSED(zone) }

void P_OVCameraLive::update() {}

QWidget *P_OVCameraLive::getZone(int zoneId) {
    switch (zoneId) {

    default:
        return Q_NULLPTR;
    }
}

void P_OVCameraLive::enterFullscreenMode() {
  /* rightBar->setFixedWidth(0);*/
}

void P_OVCameraLive::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}

void P_OVCameraLive::play_rtsp_stream(QVariant* attachment)
{
  QString rtsp = attachment->value<QString>();

  this->mpvPlayer->open(rtsp);
  this->mpvPlayer->play();

}
