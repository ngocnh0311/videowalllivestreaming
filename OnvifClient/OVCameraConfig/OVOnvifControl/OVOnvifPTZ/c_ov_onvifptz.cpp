#include "p_ov_onvifptz.h"
#include "a_ov_onvifptz.h"
#include "c_ov_onvifptz.h"

#include "onvif_message.h"

/**
  * Contructor. Register the father in the pac hierarchy.
  * @param ctrl The reference on the father. Generally the pac agent which create this agent.
  **/
C_OVOnvifPTZ::C_OVOnvifPTZ(Control* ctrl, QWidget *_zone) : Control(ctrl) {
  this->zone = _zone;

  this->abst = new A_OVOnvifPTZ(this);
  this->pres = new P_OVOnvifPTZ(this, zone);
}

C_OVOnvifPTZ::~C_OVOnvifPTZ() {
  delete this->abst;
  delete this->pres;
}

/**
  * Method to receive a message from the Presentation Facet.
  * @param message    : A string which describe the request
  * @param attachment : A ref on an eventual object necessary to treat the request
  **/
void C_OVOnvifPTZ::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from the Astraction Facet.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OVOnvifPTZ::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from an other agent.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OVOnvifPTZ::newAction(int message, QVariant* attachment) {
  switch (message) {

  default:
      qDebug() << "ERROR : General Internal pac action in" + getClass() +
                  "non-catched :" + Message.toString(message);
  }
}
