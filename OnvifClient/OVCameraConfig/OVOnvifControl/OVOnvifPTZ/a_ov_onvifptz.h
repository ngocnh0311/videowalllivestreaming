#ifndef A_OVOnvifPTZ_H
#define A_OVOnvifPTZ_H

#include <QObject>

#include "PacModel/abstraction.h"

class C_OVOnvifPTZ;
class A_OVOnvifPTZ : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_OVOnvifPTZ(Control *ctrl);
    C_OVOnvifPTZ *control() { return (C_OVOnvifPTZ *)this->ctrl; }
    void changeControl(Control *ctrl);

public Q_SLOTS:

};

#endif  // A_OVOnvifPTZ_H
