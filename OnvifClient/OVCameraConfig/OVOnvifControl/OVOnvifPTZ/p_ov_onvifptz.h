#ifndef P_OVOnvifPTZ_H
#define P_OVOnvifPTZ_H
#include <QObject>
#include <QLayout>
#include <QWidget>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QTextBrowser>
#include <QPushButton>
#include <QRadioButton>
#include <QSpinBox>
#include <QDateTimeEdit>
#include <QJsonObject>
#include <QMessageBox>

#include <PacModel/presentation.h>

class C_OVOnvifPTZ;

class OnvifMedia;
class OnvifPTZ;

class P_OVOnvifPTZ : public Presentation {
    // init ui control
private:
  OnvifPTZ *onvifPTZ = Q_NULLPTR;
  OnvifMedia *onvifMedia = Q_NULLPTR;

  QFrame *frame_4;

  QPushButton *zoomOut;
  QPushButton *right;
  QPushButton *zoomIn;
  QPushButton *down;
  QPushButton *left;
  QPushButton *up;
  QSlider *leftrightSpeed;
  QSlider *updownSpeed;
  QPushButton *center;
  QSlider *zoom;

public:
    QLayout *layout = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;

    P_OVOnvifPTZ(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_OVOnvifPTZ *control() { return (C_OVOnvifPTZ *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

    void open_message_box(const QString &text);
    void connect_to_camera(QVariant* attachment);
protected:

public Q_SLOTS:
    void on_zoomIn_clicked();
    void on_zoomOut_clicked();
    void on_up_clicked();
    void on_down_clicked();
    void on_left_clicked();
    void on_right_clicked();
    void on_center_clicked();
};

#endif  // P_OVOnvifPTZ_H
