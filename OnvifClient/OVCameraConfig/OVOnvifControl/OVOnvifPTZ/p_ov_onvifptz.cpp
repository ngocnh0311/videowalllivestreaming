#include "p_ov_onvifptz.h"
#include "c_ov_onvifptz.h"

#include "onvif_message.h"

#include "Device/device.h"
#include "Images/imaging.h"
#include "Media/media.h"
#include "PTZ/ptz.h"

/**
  * Generic method to override for updating the presention.
  **/

P_OVOnvifPTZ::P_OVOnvifPTZ(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
  this->zone = _zone;

  this->layout = new QVBoxLayout();
  this->layout->setSpacing(0);
  this->zone->setLayout(this->layout);

  frame_4 = new QFrame(this->zone);
  frame_4->setObjectName(QStringLiteral("frame_4"));
//  frame_4->setGeometry(QRect(10, 250, 330, 171));
  frame_4->setFrameShape(QFrame::StyledPanel);
  frame_4->setFrameShadow(QFrame::Raised);
  zoomOut = new QPushButton(frame_4);
  zoomOut->setObjectName(QStringLiteral("zoomOut"));
  zoomOut->setGeometry(QRect(250, 120, 31, 31));
  right = new QPushButton(frame_4);
  right->setObjectName(QStringLiteral("right"));
  right->setGeometry(QRect(130, 80, 31, 31));
  zoomIn = new QPushButton(frame_4);
  zoomIn->setObjectName(QStringLiteral("zoomIn"));
  zoomIn->setGeometry(QRect(250, 40, 31, 31));
  down = new QPushButton(frame_4);
  down->setObjectName(QStringLiteral("down"));
  down->setGeometry(QRect(90, 120, 31, 31));
  left = new QPushButton(frame_4);
  left->setObjectName(QStringLiteral("left"));
  left->setGeometry(QRect(50, 80, 31, 31));
  left->setAutoFillBackground(true);
  up = new QPushButton(frame_4);
  up->setObjectName(QStringLiteral("up"));
  up->setGeometry(QRect(90, 40, 31, 31));
  leftrightSpeed = new QSlider(frame_4);
  leftrightSpeed->setObjectName(QStringLiteral("leftrightSpeed"));
  leftrightSpeed->setGeometry(QRect(50, 10, 111, 20));
  leftrightSpeed->setMinimum(5);
  leftrightSpeed->setMaximum(180);
  leftrightSpeed->setValue(180);
  leftrightSpeed->setTracking(false);
  leftrightSpeed->setOrientation(Qt::Horizontal);
  updownSpeed = new QSlider(frame_4);
  updownSpeed->setObjectName(QStringLiteral("updownSpeed"));
  updownSpeed->setGeometry(QRect(20, 40, 20, 111));
  updownSpeed->setMinimum(5);
  updownSpeed->setMaximum(179);
  updownSpeed->setValue(179);
  updownSpeed->setOrientation(Qt::Vertical);
  center = new QPushButton(frame_4);
  center->setObjectName(QStringLiteral("center"));
  center->setGeometry(QRect(90, 80, 31, 31));
  zoom = new QSlider(frame_4);
  zoom->setObjectName(QStringLiteral("zoom"));
  zoom->setGeometry(QRect(290, 40, 20, 111));
  zoom->setMinimum(5);
  zoom->setMaximum(100);
  zoom->setSingleStep(5);
  zoom->setValue(100);
  zoom->setOrientation(Qt::Vertical);
  zoomOut->setText("-");
  right->setText("R");
  zoomIn->setText("+");
  down->setText("D");
  left->setText("L");
  up->setText("U");

  connect(this->up, &QPushButton::clicked, this, &P_OVOnvifPTZ::on_up_clicked);
  connect(this->down, &QPushButton::clicked, this, &P_OVOnvifPTZ::on_down_clicked);
  connect(this->left, &QPushButton::clicked, this, &P_OVOnvifPTZ::on_left_clicked);
  connect(this->right, &QPushButton::clicked, this, &P_OVOnvifPTZ::on_right_clicked);
  connect(this->zoomIn, &QPushButton::clicked, this, &P_OVOnvifPTZ::on_zoomIn_clicked);
  connect(this->zoomOut, &QPushButton::clicked, this, &P_OVOnvifPTZ::on_zoomOut_clicked);
  connect(this->center, &QPushButton::clicked, this, &P_OVOnvifPTZ::on_center_clicked);

  this->layout->addWidget(this->frame_4);
//  this->zone->hide();
}


void P_OVOnvifPTZ::show(QWidget *zone) { Q_UNUSED(zone) }

void P_OVOnvifPTZ::update() {}

QWidget *P_OVOnvifPTZ::getZone(int zoneId) {
    switch (zoneId) {

    default:
        return Q_NULLPTR;
    }
}

void P_OVOnvifPTZ::enterFullscreenMode() {

}

void P_OVOnvifPTZ::exitFullscreenMode() {

}


void P_OVOnvifPTZ::open_message_box(const QString &text)
{
  QMessageBox Msgbox;
  Msgbox.setText(text);
  Msgbox.exec();
}

void P_OVOnvifPTZ::connect_to_camera(QVariant* attachment)
{
  this->zone->hide();

  if (this->onvifPTZ != Q_NULLPTR) {
    delete(this->onvifPTZ);
    this->onvifPTZ = Q_NULLPTR;
  }
  if (this->onvifMedia != Q_NULLPTR) {
    delete(this->onvifMedia);
    this->onvifMedia = Q_NULLPTR;
  }

  QString dev_url = attachment->value<QString>();

  this->onvifMedia = new OnvifMedia(dev_url.toLatin1().data(),
                                    "", //this->username->text().toLatin1().data(),
                                    "", //this->password->text().toLatin1().data(),
                                    "", //this->proxylist->currentText().toLatin1().data(),
                                    ONVIF_PROXY_PORT);
  if (this->onvifMedia->errorLog.size() > 0) {
    return ;
  }

  this->onvifPTZ = new OnvifPTZ(dev_url.toLatin1().data(),
                                        "", //this->username->text().toLatin1().data(),
                                        "", //this->password->text().toLatin1().data(),
                                        "", //this->proxylist->currentText().toLatin1().data(),
                                        ONVIF_PROXY_PORT);
  if (this->onvifPTZ->errorLog.size() > 0) {
    return ;
  }

  this->zone->show();
}

void P_OVOnvifPTZ::on_zoomIn_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  std::string profile_token = this->onvifMedia->Profiles[0]->token;

  this->onvifPTZ->zoomIn(profile_token, (float) this->zoom->value()/100);
}

void P_OVOnvifPTZ::on_zoomOut_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  std::string profile_token = this->onvifMedia->Profiles[0]->token;

  this->onvifPTZ->zoomOut(profile_token, this->zoom->value()/100);
}

void P_OVOnvifPTZ::on_up_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  std::string profile_token = this->onvifMedia->Profiles[0]->token;


  this->onvifPTZ->tiltUp(profile_token, this->updownSpeed->value());
}

void P_OVOnvifPTZ::on_down_clicked()
{
  if (this->onvifPTZ == NULL)
    return;

  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  std::string profile_token = this->onvifMedia->Profiles[0]->token;


  this->onvifPTZ->tiltDown(profile_token, this->updownSpeed->value());

}

void P_OVOnvifPTZ::on_left_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  std::string profile_token = this->onvifMedia->Profiles[0]->token;


  this->onvifPTZ->panLeft(profile_token, this->leftrightSpeed->value());

}

void P_OVOnvifPTZ::on_right_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  std::string profile_token = this->onvifMedia->Profiles[0]->token;

  this->onvifPTZ->panRight(profile_token, this->leftrightSpeed->value());
}

void P_OVOnvifPTZ::on_center_clicked()
{

}
