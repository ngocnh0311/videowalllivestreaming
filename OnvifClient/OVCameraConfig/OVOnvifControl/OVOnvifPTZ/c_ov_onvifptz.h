#ifndef C_OVOnvifPTZ_H
#define C_OVOnvifPTZ_H
#include <QString>
#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>

#include "PacModel/control.h"

class P_OVOnvifPTZ;
class A_OVOnvifPTZ;

class C_OVOnvifPTZ : public Control {
 public:
  QWidget* zone;

 public:
  C_OVOnvifPTZ(Control* ctrl, QWidget *_zone);
  ~C_OVOnvifPTZ();

  void* getParent() { return (void *)this->parent; }
  P_OVOnvifPTZ* presentation() { return (P_OVOnvifPTZ*)pres; }
  A_OVOnvifPTZ* abstraction() { return (A_OVOnvifPTZ*)abst; }

  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_OVOnvifPTZ_H
