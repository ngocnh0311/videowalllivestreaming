#ifndef C_OVOnvifControl_H
#define C_OVOnvifControl_H
#include <QString>
#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>

#include "PacModel/control.h"

class P_OVOnvifControl;
class A_OVOnvifControl;

class C_OVCameraConfig;

class C_OVOnvifLogin;
class C_OVOnvifDevice;
class C_OVOnvifImaging;
class C_OVOnvifMedia;
class C_OVOnvifPTZ;

class C_OVOnvifControl : public Control {
 public:
  QWidget* zone;

  C_OVOnvifLogin *cOnvifLogin = Q_NULLPTR;
  C_OVOnvifDevice *cOnvifDevice = Q_NULLPTR;
  C_OVOnvifImaging *cOnvifImaging = Q_NULLPTR;
  C_OVOnvifMedia *cOnvifMedia = Q_NULLPTR;
  C_OVOnvifPTZ *cOnvifPTZ = Q_NULLPTR;

 public:
  C_OVOnvifControl(Control* ctrl, QWidget *_zone);
  ~C_OVOnvifControl();

  C_OVCameraConfig* getParent() { return (C_OVCameraConfig *)this->parent; }
  P_OVOnvifControl* presentation() { return (P_OVOnvifControl*)pres; }
  A_OVOnvifControl* abstraction() { return (A_OVOnvifControl*)abst; }

  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_OVOnvifControl_H
