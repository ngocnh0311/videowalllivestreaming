#ifndef P_OVOnvifMedia_H
#define P_OVOnvifMedia_H
#include <QObject>
#include <QLayout>
#include <QWidget>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QTextBrowser>
#include <QPushButton>
#include <QRadioButton>
#include <QSpinBox>
#include <QDateTimeEdit>
#include <QJsonObject>
#include <QMessageBox>

#include <PacModel/presentation.h>

class C_OVOnvifMedia;

class OnvifDevice;
class OnvifMedia;
class OnvifMedia;
class OnvifPTZ;

class P_OVOnvifMedia : public Presentation {
    // init ui control
private:
  QString userName;
  QString passWord;

  OnvifMedia *onvifImaging = Q_NULLPTR;
  OnvifMedia *onvifMedia = Q_NULLPTR;

  QFrame *frame_7;
  QLabel *label_11;
  QLabel *label_12;
  QLabel *label_14;
  QLabel *label_15;
  QLabel *label_17;

  QComboBox *resolutions;
  QComboBox *streamList;
  QSpinBox *group_of_pictures;
  QSpinBox *frameRate;

public:
    QLayout *layout = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;

    P_OVOnvifMedia(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_OVOnvifMedia *control() { return (C_OVOnvifMedia *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

    void open_message_box(const QString &text);
    void connect_to_camera(QVariant* attachment);
    void get_video_setting(int index);

protected:

public Q_SLOTS:
    void on_frameRate_editingFinished();
    void on_group_of_pictures_editingFinished();
    void on_resolutions_currentTextChanged(const QString &arg1);
    void on_streamList_currentTextChanged(const QString &currentText);
};

#endif  // P_OVOnvifMedia_H
