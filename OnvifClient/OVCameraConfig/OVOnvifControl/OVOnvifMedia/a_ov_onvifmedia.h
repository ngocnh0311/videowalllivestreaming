#ifndef A_OVOnvifMedia_H
#define A_OVOnvifMedia_H

#include <QObject>

#include "PacModel/abstraction.h"

class C_OVOnvifMedia;
class A_OVOnvifMedia : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_OVOnvifMedia(Control *ctrl);
    C_OVOnvifMedia *control() { return (C_OVOnvifMedia *)this->ctrl; }
    void changeControl(Control *ctrl);

public Q_SLOTS:

};

#endif  // A_OVOnvifMedia_H
