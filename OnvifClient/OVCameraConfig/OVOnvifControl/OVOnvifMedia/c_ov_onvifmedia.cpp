#include "p_ov_onvifmedia.h"
#include "a_ov_onvifmedia.h"
#include "c_ov_onvifmedia.h"

#include "onvif_message.h"

/**
  * Contructor. Register the father in the pac hierarchy.
  * @param ctrl The reference on the father. Generally the pac agent which create this agent.
  **/
C_OVOnvifMedia::C_OVOnvifMedia(Control* ctrl, QWidget *_zone) : Control(ctrl) {
  this->zone = _zone;

  this->abst = new A_OVOnvifMedia(this);
  this->pres = new P_OVOnvifMedia(this, zone);
}

C_OVOnvifMedia::~C_OVOnvifMedia() {
  delete this->abst;
  delete this->pres;
}

/**
  * Method to receive a message from the Presentation Facet.
  * @param message    : A string which describe the request
  * @param attachment : A ref on an eventual object necessary to treat the request
  **/
void C_OVOnvifMedia::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from the Astraction Facet.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OVOnvifMedia::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from an other agent.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OVOnvifMedia::newAction(int message, QVariant* attachment) {
  switch (message) {

  default:
      qDebug() << "ERROR : General Internal pac action in" + getClass() +
                  "non-catched :" + Message.toString(message);
  }
}
