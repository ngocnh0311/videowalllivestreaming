#ifndef C_OVOnvifMedia_H
#define C_OVOnvifMedia_H
#include <QString>
#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>

#include "PacModel/control.h"

class P_OVOnvifMedia;
class A_OVOnvifMedia;

class C_OVOnvifMedia : public Control {
 public:
  QWidget* zone;

 public:
  C_OVOnvifMedia(Control* ctrl, QWidget *_zone);
  ~C_OVOnvifMedia();

  void* getParent() { return (void *)this->parent; }
  P_OVOnvifMedia* presentation() { return (P_OVOnvifMedia*)pres; }
  A_OVOnvifMedia* abstraction() { return (A_OVOnvifMedia*)abst; }

  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_OVOnvifMedia_H
