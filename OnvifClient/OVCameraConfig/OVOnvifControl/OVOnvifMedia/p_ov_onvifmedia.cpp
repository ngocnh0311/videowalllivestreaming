#include "p_ov_onvifmedia.h"
#include "c_ov_onvifmedia.h"

#include "onvif_message.h"

#include "Device/device.h"
#include "Images/imaging.h"
#include "Media/media.h"
#include "PTZ/ptz.h"

/**
  * Generic method to override for updating the presention.
  **/

P_OVOnvifMedia::P_OVOnvifMedia(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
  this->zone = _zone;

  this->layout = new QVBoxLayout();
  this->layout->setSpacing(0);
  this->layout->setMargin(0);
  this->zone->setLayout(this->layout);

  frame_7 = new QFrame(this->zone);
  frame_7->setObjectName(QStringLiteral("frame_7"));
  frame_7->setGeometry(QRect(10, 10, 400, 231));
  frame_7->setFrameShape(QFrame::StyledPanel);
  frame_7->setFrameShadow(QFrame::Raised);
  resolutions = new QComboBox(frame_7);
  resolutions->setObjectName(QStringLiteral("resolutions"));
  resolutions->setGeometry(QRect(130, 110, 131, 31));
  resolutions->setEditable(false);
  frameRate = new QSpinBox(frame_7);
  frameRate->setObjectName(QStringLiteral("frameRate"));
  frameRate->setGeometry(QRect(130, 150, 48, 31));
  frameRate->setMinimum(1);
  frameRate->setMaximum(30);
  label_11 = new QLabel(frame_7);
  label_11->setObjectName(QStringLiteral("label_11"));
  label_11->setGeometry(QRect(40, 150, 81, 25));
  label_12 = new QLabel(frame_7);
  label_12->setObjectName(QStringLiteral("label_12"));
  label_12->setGeometry(QRect(40, 110, 81, 25));
  label_14 = new QLabel(frame_7);
  label_14->setObjectName(QStringLiteral("label_14"));
  label_14->setGeometry(QRect(10, 10, 101, 25));
  label_15 = new QLabel(frame_7);
  label_15->setObjectName(QStringLiteral("label_15"));
  label_15->setGeometry(QRect(40, 190, 81, 25));
  group_of_pictures = new QSpinBox(frame_7);
  group_of_pictures->setObjectName(QStringLiteral("group_of_pictures"));
  group_of_pictures->setGeometry(QRect(130, 190, 48, 31));
  group_of_pictures->setMinimum(1);
  group_of_pictures->setMaximum(50);
  streamList = new QComboBox(frame_7);
  streamList->setObjectName(QStringLiteral("streamList"));
  streamList->setGeometry(QRect(40, 60, 341, 31));
  label_17 = new QLabel(frame_7);
  label_17->setObjectName(QStringLiteral("label_17"));
  label_17->setGeometry(QRect(40, 40, 91, 16));

  connect(this->streamList, &QComboBox::currentTextChanged, this, &P_OVOnvifMedia::on_streamList_currentTextChanged);
  connect(this->resolutions, &QComboBox::currentTextChanged, this, &P_OVOnvifMedia::on_resolutions_currentTextChanged);
  connect(this->group_of_pictures, &QSpinBox::editingFinished, this, &P_OVOnvifMedia::on_group_of_pictures_editingFinished);
  connect(this->frameRate, &QSpinBox::editingFinished, this, &P_OVOnvifMedia::on_frameRate_editingFinished);

  this->layout->addWidget(this->frame_7);
}


void P_OVOnvifMedia::show(QWidget *zone) { Q_UNUSED(zone) }

void P_OVOnvifMedia::update() {}

QWidget *P_OVOnvifMedia::getZone(int zoneId) {
    switch (zoneId) {

    default:
        return Q_NULLPTR;
    }
}

void P_OVOnvifMedia::enterFullscreenMode() {

}

void P_OVOnvifMedia::exitFullscreenMode() {

}


void P_OVOnvifMedia::open_message_box(const QString &text)
{
  QMessageBox Msgbox;
  Msgbox.setText(text);
  Msgbox.exec();
}

void P_OVOnvifMedia::connect_to_camera(QVariant* attachment)
{
  if (this->onvifImaging != Q_NULLPTR) {
    delete(this->onvifImaging);
    this->onvifImaging = Q_NULLPTR;
  }
  if (this->onvifMedia != Q_NULLPTR) {
    delete(this->onvifMedia);
    this->onvifMedia = Q_NULLPTR;
  }

  QString dev_url = attachment->value<QString>();

  this->onvifMedia = new OnvifMedia(dev_url.toLatin1().data(),
                                    "", //this->username->text().toLatin1().data(),
                                    "", //this->password->text().toLatin1().data(),
                                    "", //this->proxylist->currentText().toLatin1().data(),
                                    ONVIF_PROXY_PORT);
  if (this->onvifMedia->errorLog.size() > 0) {
    return ;
  }

  this->onvifImaging = new OnvifMedia(dev_url.toLatin1().data(),
                                        "", //this->username->text().toLatin1().data(),
                                        "", //this->password->text().toLatin1().data(),
                                        "", //this->proxylist->currentText().toLatin1().data(),
                                        ONVIF_PROXY_PORT);
  if (this->onvifImaging->errorLog.size() > 0) {
    return ;
  }
}

void P_OVOnvifMedia::on_streamList_currentTextChanged(const QString &currentText)
{
//  if ((this->onvifMedia == NULL) ||(currentText.isEmpty()))
  if (this->onvifMedia == NULL)
    return;

  QStringList temp = currentText.split("//", QString::SkipEmptyParts);

  QString user_pass = "rtsp://"+this->userName+":"+this->passWord;

  temp.replaceInStrings("rtsp:", user_pass);

  QString rtsp = temp.join("@");

  QVariant* attachment = new QVariant();
  attachment->setValue<QString>(rtsp);

  int index = this->streamList->currentIndex();

  this->control()->newUserAction(ONVIF_RTSP_STREAM_CHANGED, attachment);

  get_video_setting(index);
}

void P_OVOnvifMedia::get_video_setting(int index)
{
  if ((this->onvifMedia == NULL) || (index<0))
    return ;

  this->resolutions->setEnabled(false);
  this->frameRate->setEnabled(false);
  this->group_of_pictures->setEnabled(false);

  // Get VideoEncoderConfiguration
  std::string configurationToken = this->onvifMedia->Profiles[index]->VideoEncoderConfiguration->token;

  this->onvifMedia->getVideoEncoderConfiguration(configurationToken);
  if (this->onvifMedia->errorLog.size() > 0) {
    open_message_box(QString::fromStdString(this->onvifMedia->errorLog));
    return;
  }

  // Add available resolutions of camera
  tt__VideoEncoderConfiguration *video_encoder_configuration = this->onvifMedia->videoEncoderConfiguration;
  int fps = video_encoder_configuration->RateControl->FrameRateLimit;
  this->frameRate->setValue(fps);
  this->frameRate->setEnabled(true);

  this->resolutions->clear();
  std::string profileToken = this->onvifMedia->Profiles[index]->token;
  this->onvifMedia->getVideoEncoderConfigurationOptions(profileToken, configurationToken);
  if (this->onvifMedia->videoEncoderConfigurationOptions->H264 != NULL) {
    for (tt__VideoResolution * resolution : this->onvifMedia->videoEncoderConfigurationOptions->H264->ResolutionsAvailable) {

      int height = resolution->Height;
      int width = resolution->Width;

      QString resol = QString::fromStdString(std::to_string(width)+"x"+std::to_string(height)+"(*)");
      this->resolutions->addItem(resol);
    }
  }

  if (this->onvifMedia->videoEncoderConfigurationOptions->MPEG4 != NULL) {
    for (tt__VideoResolution * resolution : this->onvifMedia->videoEncoderConfigurationOptions->MPEG4->ResolutionsAvailable) {

      int height = resolution->Height;
      int width = resolution->Width;

      QString resol = QString::fromStdString(std::to_string(width)+"x"+std::to_string(height)+"(*)");

      this->resolutions->addItem(resol);
    }
  }

  int height = video_encoder_configuration->Resolution->Height;
  int width = video_encoder_configuration->Resolution->Width;

  // Display current resolution
  QString resolution = QString::fromStdString(std::to_string(width)+"x"+std::to_string(height)+"(*)");
  int resolution_index = this->resolutions->findText(resolution);
  if (resolution_index >= 0) {
    this->resolutions->setCurrentIndex(resolution_index);
  } else {
    this->resolutions->addItem(resolution);
    resolution_index = this->resolutions->findText(resolution);
    this->resolutions->setCurrentIndex(resolution_index);
  }

  this->resolutions->setEnabled(true);

  // Add default resolutions
  std::vector<QString> resolution_list = {"320x240","640x352","640x480","704x576","1280x720","1280x960",
                                          "1920x1080","2048x1536","2592x1944","3840x2160"};

  for (QString resol : resolution_list) {
    int tmp = this->resolutions->findText(resol+"(*)");
    if (tmp < 0) {
      this->resolutions->addItem(resol);
    }
  }

  this->group_of_pictures->setEnabled(true);
  if (video_encoder_configuration->Encoding == tt__VideoEncoding__H264) {
    int group_of_pictures = video_encoder_configuration->H264->GovLength;

    this->group_of_pictures->setValue(group_of_pictures);
  } else if (video_encoder_configuration->Encoding == tt__VideoEncoding__MPEG4) {
    int group_of_pictures = video_encoder_configuration->MPEG4->GovLength;

    this->group_of_pictures->setValue(group_of_pictures);
  } else {
    //tt__VideoEncoding__JPEG
  }
}

void P_OVOnvifMedia::on_frameRate_editingFinished()
{
  if (this->onvifMedia == NULL)
    return;

  int value = this->frameRate->value();

  int current_value = this->onvifMedia->videoEncoderConfiguration->RateControl->FrameRateLimit;
  if (current_value != value) {
    this->onvifMedia->videoEncoderConfiguration->RateControl->FrameRateLimit = value;

    this->onvifMedia->setVideoEncoderConfiguration();
    if (this->onvifMedia->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifMedia->errorLog));
      return;
    }
  }
}

void P_OVOnvifMedia::on_group_of_pictures_editingFinished()
{
  if (this->onvifMedia == NULL)
    return;

  int current_value;
  int value = this->group_of_pictures->value();

  if (this->onvifMedia->videoEncoderConfiguration->Encoding == tt__VideoEncoding__H264) {
    current_value = this->onvifMedia->videoEncoderConfiguration->H264->GovLength;
  } else if (this->onvifMedia->videoEncoderConfiguration->Encoding == tt__VideoEncoding__MPEG4) {
    current_value = this->onvifMedia->videoEncoderConfiguration->MPEG4->GovLength;
  } else { //tt__VideoEncoding__JPEG
    // Do nothing
  }

  if (current_value != value) {
    this->onvifMedia->setVideoEncoderConfiguration();
    if (this->onvifMedia->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifMedia->errorLog));
      return;
    }
  }
}

void P_OVOnvifMedia::on_resolutions_currentTextChanged(const QString &arg1)
{
  if ((this->onvifMedia == NULL) || (this->resolutions->currentIndex() < 0))
    return;

  QStringList tmp = arg1.split(QRegExp("\\W+"), QString::SkipEmptyParts);
  QStringList resolution = tmp[0].split('x', QString::SkipEmptyParts);
  this->onvifMedia->videoEncoderConfiguration->Resolution->Width = resolution[0].toInt();
  this->onvifMedia->videoEncoderConfiguration->Resolution->Height = resolution[1].toInt();

  this->onvifMedia->setVideoEncoderConfiguration();
  if (this->onvifMedia->errorLog.size() > 0) {
    open_message_box(QString::fromStdString(this->onvifMedia->errorLog));
    return;
  }
}
