#ifndef A_OVOnvifControl_H
#define A_OVOnvifControl_H

#include <QObject>

#include "PacModel/abstraction.h"

class C_OVOnvifControl;
class A_OVOnvifControl : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_OVOnvifControl(Control *ctrl);
    C_OVOnvifControl *control() { return (C_OVOnvifControl *)this->ctrl; }
    void changeControl(Control *ctrl);

public Q_SLOTS:

};

#endif  // A_OVOnvifControl_H
