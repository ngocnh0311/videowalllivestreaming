#include "p_ov_onvifcontrol.h"
#include "a_ov_onvifcontrol.h"
#include "c_ov_onvifcontrol.h"

#include "onvif_message.h"

#include "OVCameraConfig/c_ov_cameraconfig.h"

/**
  * Contructor. Register the father in the pac hierarchy.
  * @param ctrl The reference on the father. Generally the pac agent which create this agent.
  **/
C_OVOnvifControl::C_OVOnvifControl(Control* ctrl, QWidget *_zone) : Control(ctrl) {
  this->zone = _zone;

  this->abst = new A_OVOnvifControl(this);
  this->pres = new P_OVOnvifControl(this, zone);
}

C_OVOnvifControl::~C_OVOnvifControl() {
  delete this->abst;
  delete this->pres;
}

/**
  * Method to receive a message from the Presentation Facet.
  * @param message    : A string which describe the request
  * @param attachment : A ref on an eventual object necessary to treat the request
  **/
void C_OVOnvifControl::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case ONVIF_RTSP_STREAM_CHANGED:
      this->getParent()->newAction(message,attachment);
      break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from the Astraction Facet.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OVOnvifControl::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from an other agent.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OVOnvifControl::newAction(int message, QVariant* attachment) {
  switch (message) {
  case ONVIF_CAMERA_SELECTED:
    this->presentation()->connect_to_camera(attachment);
    break;


  default:
      qDebug() << "ERROR : General Internal pac action in" + getClass() +
                  "non-catched :" + Message.toString(message);
  }
}
