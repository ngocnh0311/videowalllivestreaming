#ifndef A_OVOnvifImaging_H
#define A_OVOnvifImaging_H

#include <QObject>

#include "PacModel/abstraction.h"

class C_OVOnvifImaging;
class A_OVOnvifImaging : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_OVOnvifImaging(Control *ctrl);
    C_OVOnvifImaging *control() { return (C_OVOnvifImaging *)this->ctrl; }
    void changeControl(Control *ctrl);

public Q_SLOTS:

};

#endif  // A_OVOnvifImaging_H
