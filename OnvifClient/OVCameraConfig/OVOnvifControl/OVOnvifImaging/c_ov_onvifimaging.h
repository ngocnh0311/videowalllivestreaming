#ifndef C_OVOnvifImaging_H
#define C_OVOnvifImaging_H
#include <QString>
#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>

#include "PacModel/control.h"

class P_OVOnvifImaging;
class A_OVOnvifImaging;

class C_OVOnvifImaging : public Control {
 public:
  QWidget* zone;

 public:
  C_OVOnvifImaging(Control* ctrl, QWidget *_zone);
  ~C_OVOnvifImaging();

  void* getParent() { return (void *)this->parent; }
  P_OVOnvifImaging* presentation() { return (P_OVOnvifImaging*)pres; }
  A_OVOnvifImaging* abstraction() { return (A_OVOnvifImaging*)abst; }

  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_OVOnvifImaging_H
