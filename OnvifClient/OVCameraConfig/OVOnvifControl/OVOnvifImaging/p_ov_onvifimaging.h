#ifndef P_OVOnvifImaging_H
#define P_OVOnvifImaging_H
#include <QObject>
#include <QLayout>
#include <QWidget>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QTextBrowser>
#include <QPushButton>
#include <QRadioButton>
#include <QSpinBox>
#include <QDateTimeEdit>
#include <QJsonObject>
#include <QMessageBox>

#include <PacModel/presentation.h>

class C_OVOnvifImaging;


class OnvifDevice;
class OnvifImaging;
class OnvifMedia;
class OnvifPTZ;

class P_OVOnvifImaging : public Presentation {
    // init ui control
private:
  OnvifImaging *onvifImaging = Q_NULLPTR;
  OnvifMedia *onvifMedia = Q_NULLPTR;

  QFrame *frame_6;
  QLabel *label_2;
  QLabel *label_4;
  QLabel *label_3;
  QLabel *label_6;
  QLabel *label;
  QLabel *label_10;
  QLabel *label_13;

  QSlider *sharpnessSlider;
  QSlider *contrastSlider;
  QSlider *CbGainSlider;
  QSlider *CrGainSlider;
  QSlider *saturationSlider;
  QSlider *brightnessSlider;

public:
    QLayout *layout = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;

    P_OVOnvifImaging(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_OVOnvifImaging *control() { return (C_OVOnvifImaging *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

    void open_message_box(const QString &text);
    void connect_to_camera(QVariant* attachment);
    void get_image_setting(void);


protected:

public Q_SLOTS:
    void on_CbGainSlider_valueChanged(int value);
    void on_CrGainSlider_valueChanged(int value);
    void on_brightnessSlider_valueChanged(int value);
    void on_contrastSlider_valueChanged(int value);
    void on_sharpnessSlider_valueChanged(int value);
    void on_saturationSlider_valueChanged(int value);
};

#endif  // P_OVOnvifImaging_H
