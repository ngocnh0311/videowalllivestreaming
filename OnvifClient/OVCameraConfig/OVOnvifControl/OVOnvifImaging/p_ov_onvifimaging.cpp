#include "p_ov_onvifimaging.h"
#include "c_ov_onvifimaging.h"

#include "onvif_message.h"

#include "Device/device.h"
#include "Images/imaging.h"
#include "Media/media.h"
#include "PTZ/ptz.h"

/**
  * Generic method to override for updating the presention.
  **/

P_OVOnvifImaging::P_OVOnvifImaging(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
  this->zone = _zone;

  this->layout = new QVBoxLayout();
  this->layout->setSpacing(0);
  this->layout->setMargin(0);
  this->zone->setLayout(this->layout);

  frame_6 = new QFrame(this->zone);
  frame_6->setObjectName(QStringLiteral("frame_6"));

  sharpnessSlider = new QSlider(frame_6);
  sharpnessSlider->setObjectName(QStringLiteral("sharpnessSlider"));
  sharpnessSlider->setGeometry(QRect(130, 169, 180, 16));
  sharpnessSlider->setMinimum(0);
  sharpnessSlider->setMaximum(255);
  sharpnessSlider->setValue(128);
  sharpnessSlider->setTracking(false);
  sharpnessSlider->setOrientation(Qt::Horizontal);
  contrastSlider = new QSlider(frame_6);
  contrastSlider->setObjectName(QStringLiteral("contrastSlider"));
  contrastSlider->setGeometry(QRect(130, 138, 180, 16));
  contrastSlider->setMaximum(255);
  contrastSlider->setValue(128);
  contrastSlider->setTracking(false);
  contrastSlider->setOrientation(Qt::Horizontal);
  label_2 = new QLabel(frame_6);
  label_2->setObjectName(QStringLiteral("label_2"));
  label_2->setGeometry(QRect(40, 100, 81, 25));
  CbGainSlider = new QSlider(frame_6);
  CbGainSlider->setObjectName(QStringLiteral("CbGainSlider"));
  CbGainSlider->setGeometry(QRect(130, 49, 180, 16));
  CbGainSlider->setMinimum(0);
  CbGainSlider->setMaximum(255);
  CbGainSlider->setValue(128);
  CbGainSlider->setTracking(false);
  CbGainSlider->setOrientation(Qt::Horizontal);
  label = new QLabel(frame_6);
  label->setObjectName(QStringLiteral("label"));
  label->setGeometry(QRect(40, 40, 51, 25));
  label_6 = new QLabel(frame_6);
  label_6->setObjectName(QStringLiteral("label_6"));
  label_6->setGeometry(QRect(40, 190, 73, 25));
  label_10 = new QLabel(frame_6);
  label_10->setObjectName(QStringLiteral("label_10"));
  label_10->setGeometry(QRect(40, 70, 51, 25));
  CrGainSlider = new QSlider(frame_6);
  CrGainSlider->setObjectName(QStringLiteral("CrGainSlider"));
  CrGainSlider->setGeometry(QRect(130, 80, 180, 16));
  CrGainSlider->setMinimum(0);
  CrGainSlider->setMaximum(255);
  CrGainSlider->setValue(128);
  CrGainSlider->setTracking(false);
  CrGainSlider->setOrientation(Qt::Horizontal);
  saturationSlider = new QSlider(frame_6);
  saturationSlider->setObjectName(QStringLiteral("saturationSlider"));
  saturationSlider->setGeometry(QRect(130, 200, 180, 16));
  saturationSlider->setMaximum(255);
  saturationSlider->setValue(128);
  saturationSlider->setTracking(false);
  saturationSlider->setOrientation(Qt::Horizontal);
  label_4 = new QLabel(frame_6);
  label_4->setObjectName(QStringLiteral("label_4"));
  label_4->setGeometry(QRect(40, 160, 73, 25));
  label_3 = new QLabel(frame_6);
  label_3->setObjectName(QStringLiteral("label_3"));
  label_3->setGeometry(QRect(40, 130, 73, 25));
  brightnessSlider = new QSlider(frame_6);
  brightnessSlider->setObjectName(QStringLiteral("brightnessSlider"));
  brightnessSlider->setGeometry(QRect(130, 107, 180, 16));
  brightnessSlider->setMinimum(0);
  brightnessSlider->setMaximum(255);
  brightnessSlider->setValue(128);
  brightnessSlider->setTracking(false);
  brightnessSlider->setOrientation(Qt::Horizontal);
  label_13 = new QLabel(frame_6);
  label_13->setObjectName(QStringLiteral("label_13"));
  label_13->setGeometry(QRect(10, 0, 101, 25));

  label_2->setText("Brightness:");
  label->setText("CbGain:");
  label_6->setText("Saturation:");
  label_10->setText("CrGain:");
  label_4->setText("Sharpness:");
  label_3->setText("Contrast:");
  label_13->setText("Image Setting");


  connect(this->CbGainSlider, &QSlider::valueChanged, this, &P_OVOnvifImaging::on_CbGainSlider_valueChanged);
  connect(this->CrGainSlider, &QSlider::valueChanged, this, &P_OVOnvifImaging::on_CrGainSlider_valueChanged);
  connect(this->brightnessSlider, &QSlider::valueChanged, this, &P_OVOnvifImaging::on_brightnessSlider_valueChanged);
  connect(this->contrastSlider, &QSlider::valueChanged, this, &P_OVOnvifImaging::on_contrastSlider_valueChanged);
  connect(this->saturationSlider, &QSlider::valueChanged, this, &P_OVOnvifImaging::on_saturationSlider_valueChanged);
  connect(this->sharpnessSlider, &QSlider::valueChanged, this, &P_OVOnvifImaging::on_sharpnessSlider_valueChanged);

  this->layout->addWidget(this->frame_6);
}


void P_OVOnvifImaging::show(QWidget *zone) { Q_UNUSED(zone) }

void P_OVOnvifImaging::update() {}

QWidget *P_OVOnvifImaging::getZone(int zoneId) {
    switch (zoneId) {

    default:
        return Q_NULLPTR;
    }
}

void P_OVOnvifImaging::enterFullscreenMode() {

}

void P_OVOnvifImaging::exitFullscreenMode() {

}


void P_OVOnvifImaging::open_message_box(const QString &text)
{
  QMessageBox Msgbox;
  Msgbox.setText(text);
  Msgbox.exec();
}

void P_OVOnvifImaging::connect_to_camera(QVariant* attachment)
{
  this->CbGainSlider->setEnabled(false);
  this->CrGainSlider->setEnabled(false);
  this->brightnessSlider->setEnabled(false);
  this->contrastSlider->setEnabled(false);
  this->saturationSlider->setEnabled(false);
  this->sharpnessSlider->setEnabled(false);

  if (this->onvifImaging != Q_NULLPTR) {
    delete(this->onvifImaging);
    this->onvifImaging = Q_NULLPTR;
  }
  if (this->onvifMedia != Q_NULLPTR) {
    delete(this->onvifMedia);
    this->onvifMedia = Q_NULLPTR;
  }

  QString dev_url = attachment->value<QString>();

  this->onvifMedia = new OnvifMedia(dev_url.toLatin1().data(),
                                    "", //this->username->text().toLatin1().data(),
                                    "", //this->password->text().toLatin1().data(),
                                    "", //this->proxylist->currentText().toLatin1().data(),
                                    ONVIF_PROXY_PORT);
  if (this->onvifMedia->errorLog.size() > 0) {
    return ;
  }

  this->onvifImaging = new OnvifImaging(dev_url.toLatin1().data(),
                                        "", //this->username->text().toLatin1().data(),
                                        "", //this->password->text().toLatin1().data(),
                                        "", //this->proxylist->currentText().toLatin1().data(),
                                        ONVIF_PROXY_PORT);
  if (this->onvifImaging->errorLog.size() > 0) {
    return ;
  }

  this->get_image_setting();
}

void P_OVOnvifImaging::get_image_setting(void)
{

  if ((this->onvifMedia == Q_NULLPTR) ||
      (this->onvifImaging == Q_NULLPTR)) {
    return ;
  }

  this->onvifMedia->getProfiles();
  if (this->onvifMedia->errorLog.size() > 0) {
    return;
  }

  std::string video_source_token = this->onvifMedia->Profiles[0]->VideoSourceConfiguration->SourceToken;

  this->onvifImaging->getImagingSettings(video_source_token);
  if (this->onvifImaging->errorLog.size() > 0) {
    open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
    return;
  }

  tt__ImagingSettings20 *imaging_settings = this->onvifImaging->imagingSettings;

  if (imaging_settings->WhiteBalance != NULL) {
    qDebug() << "CbGain: " << *imaging_settings->WhiteBalance->CbGain;
    qDebug() << "CrGain: " << *imaging_settings->WhiteBalance->CrGain;
    this->CbGainSlider->setValue((int) *imaging_settings->WhiteBalance->CbGain);
    this->CrGainSlider->setValue((int) *imaging_settings->WhiteBalance->CrGain);
    this->CbGainSlider->setEnabled(true);
    this->CrGainSlider->setEnabled(true);
  }

  if (imaging_settings->Brightness != NULL) {
    qDebug() << "Brightness: " << *imaging_settings->Brightness;
    this->brightnessSlider->setValue((int) *imaging_settings->Brightness);
    this->brightnessSlider->setEnabled(true);
  }
  if (imaging_settings->ColorSaturation != NULL) {
    qDebug() << "Saturation: " << *imaging_settings->ColorSaturation;
    this->saturationSlider->setValue((int) *imaging_settings->ColorSaturation);
    this->saturationSlider->setEnabled(true);
  }
  if (imaging_settings->Contrast != NULL) {
    qDebug() << "Contrast: " << *imaging_settings->Contrast;
    this->contrastSlider->setValue((int) *imaging_settings->Contrast);
    this->contrastSlider->setEnabled(true);
  }
  if (imaging_settings->Sharpness != NULL) {
    qDebug() << "Sharpness: " << *imaging_settings->Sharpness;
    this->sharpnessSlider->setValue((int) *imaging_settings->Sharpness);
    this->sharpnessSlider->setEnabled(true);
  }
}

void P_OVOnvifImaging::on_CbGainSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  std::string video_source_token = this->onvifMedia->Profiles[0]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->WhiteBalance->CbGain;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->WhiteBalance->CbGain = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifImaging::on_CrGainSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  std::string video_source_token = this->onvifMedia->Profiles[0]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->WhiteBalance->CrGain;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->WhiteBalance->CrGain = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifImaging::on_brightnessSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  std::string video_source_token = this->onvifMedia->Profiles[0]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->Brightness;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->Brightness = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifImaging::on_contrastSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  std::string video_source_token = this->onvifMedia->Profiles[0]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->Contrast;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->Contrast = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifImaging::on_sharpnessSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  std::string video_source_token = this->onvifMedia->Profiles[0]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->Sharpness;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->Sharpness = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifImaging::on_saturationSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  std::string video_source_token = this->onvifMedia->Profiles[0]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->ColorSaturation;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->ColorSaturation = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}
