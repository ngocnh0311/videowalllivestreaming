#include "p_ov_onvifcontrol.h"
#include "c_ov_onvifcontrol.h"

#include "onvif_message.h"

#include "Device/device.h"
#include "Images/imaging.h"
#include "Media/media.h"
#include "PTZ/ptz.h"

/**
  * Generic method to override for updating the presention.
  **/

P_OVOnvifControl::P_OVOnvifControl(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
  this->zone = _zone;

  this->layout = new QVBoxLayout();
  this->layout->setSpacing(0);
  this->zone->setLayout(this->layout);

  Main = new QWidget();
  Main->setObjectName(QStringLiteral("Main"));

  tab = new QWidget();
  tab->setObjectName(QStringLiteral("tab"));

  tab_2 = new QWidget();
  tab_2->setObjectName(QStringLiteral("tab_2"));

  frame_2 = new QFrame(Main);
  frame_2->setObjectName(QStringLiteral("frame_2"));
  frame_2->setGeometry(QRect(10, 10, 400, 111));
  frame_2->setFrameShape(QFrame::StyledPanel);
  frame_2->setFrameShadow(QFrame::Raised);
  label_8 = new QLabel(frame_2);
  label_8->setObjectName(QStringLiteral("label_8"));
  label_8->setGeometry(QRect(160, 80, 71, 20));
  username = new QLineEdit(frame_2);
  username->setObjectName(QStringLiteral("username"));
  username->setGeometry(QRect(90, 70, 61, 31));
  password = new QLineEdit(frame_2);
  password->setObjectName(QStringLiteral("password"));
  password->setGeometry(QRect(230, 70, 81, 31));
  label_7 = new QLabel(frame_2);
  label_7->setObjectName(QStringLiteral("label_7"));
  label_7->setGeometry(QRect(10, 80, 71, 16));
  proxylist = new QComboBox(frame_2);
  proxylist->addItem(QString());
  proxylist->addItem(QString());
  proxylist->setObjectName(QStringLiteral("proxylist"));
  proxylist->setGeometry(QRect(10, 30, 141, 31));
  proxylist->setEditable(true);
  label_9 = new QLabel(frame_2);
  label_9->setObjectName(QStringLiteral("label_9"));
  label_9->setGeometry(QRect(10, 10, 81, 16));
  devInfoText = new QTextBrowser(Main);
  devInfoText->setObjectName(QStringLiteral("devInfoText"));
  devInfoText->setGeometry(QRect(10, 130, 400, 111));
  frame_6 = new QFrame(Main);
  frame_6->setObjectName(QStringLiteral("frame_6"));
  frame_6->setGeometry(QRect(10, 250, 400, 231));
  frame_6->setFrameShape(QFrame::StyledPanel);
  frame_6->setFrameShadow(QFrame::Raised);
  sharpnessSlider = new QSlider(frame_6);
  sharpnessSlider->setObjectName(QStringLiteral("sharpnessSlider"));
  sharpnessSlider->setGeometry(QRect(130, 169, 256, 16));
  sharpnessSlider->setMinimum(0);
  sharpnessSlider->setMaximum(255);
  sharpnessSlider->setValue(128);
  sharpnessSlider->setTracking(false);
  sharpnessSlider->setOrientation(Qt::Horizontal);
  contrastSlider = new QSlider(frame_6);
  contrastSlider->setObjectName(QStringLiteral("contrastSlider"));
  contrastSlider->setGeometry(QRect(130, 138, 256, 16));
  contrastSlider->setMaximum(255);
  contrastSlider->setValue(128);
  contrastSlider->setTracking(false);
  contrastSlider->setOrientation(Qt::Horizontal);
  label_2 = new QLabel(frame_6);
  label_2->setObjectName(QStringLiteral("label_2"));
  label_2->setGeometry(QRect(40, 100, 81, 25));
  CbGainSlider = new QSlider(frame_6);
  CbGainSlider->setObjectName(QStringLiteral("CbGainSlider"));
  CbGainSlider->setGeometry(QRect(130, 49, 256, 16));
  CbGainSlider->setMinimum(0);
  CbGainSlider->setMaximum(255);
  CbGainSlider->setValue(128);
  CbGainSlider->setTracking(false);
  CbGainSlider->setOrientation(Qt::Horizontal);
  label = new QLabel(frame_6);
  label->setObjectName(QStringLiteral("label"));
  label->setGeometry(QRect(40, 40, 51, 25));
  label_6 = new QLabel(frame_6);
  label_6->setObjectName(QStringLiteral("label_6"));
  label_6->setGeometry(QRect(40, 190, 73, 25));
  label_10 = new QLabel(frame_6);
  label_10->setObjectName(QStringLiteral("label_10"));
  label_10->setGeometry(QRect(40, 70, 51, 25));
  CrGainSlider = new QSlider(frame_6);
  CrGainSlider->setObjectName(QStringLiteral("CrGainSlider"));
  CrGainSlider->setGeometry(QRect(130, 80, 256, 16));
  CrGainSlider->setMinimum(0);
  CrGainSlider->setMaximum(255);
  CrGainSlider->setValue(128);
  CrGainSlider->setTracking(false);
  CrGainSlider->setOrientation(Qt::Horizontal);
  saturationSlider = new QSlider(frame_6);
  saturationSlider->setObjectName(QStringLiteral("saturationSlider"));
  saturationSlider->setGeometry(QRect(130, 200, 256, 16));
  saturationSlider->setMaximum(255);
  saturationSlider->setValue(128);
  saturationSlider->setTracking(false);
  saturationSlider->setOrientation(Qt::Horizontal);
  label_4 = new QLabel(frame_6);
  label_4->setObjectName(QStringLiteral("label_4"));
  label_4->setGeometry(QRect(40, 160, 73, 25));
  label_3 = new QLabel(frame_6);
  label_3->setObjectName(QStringLiteral("label_3"));
  label_3->setGeometry(QRect(40, 130, 73, 25));
  brightnessSlider = new QSlider(frame_6);
  brightnessSlider->setObjectName(QStringLiteral("brightnessSlider"));
  brightnessSlider->setGeometry(QRect(130, 107, 256, 16));
  brightnessSlider->setMinimum(0);
  brightnessSlider->setMaximum(255);
  brightnessSlider->setValue(128);
  brightnessSlider->setTracking(false);
  brightnessSlider->setOrientation(Qt::Horizontal);
  label_13 = new QLabel(frame_6);
  label_13->setObjectName(QStringLiteral("label_13"));
  label_13->setGeometry(QRect(10, 0, 101, 25));
  saveConfig = new QPushButton(tab);
  saveConfig->setObjectName(QStringLiteral("savetime"));
  saveConfig->setGeometry(QRect(330, 180, 71, 31));
  manualRadioButton = new QRadioButton(tab);
  manualRadioButton->setObjectName(QStringLiteral("manualRadioButton"));
  manualRadioButton->setGeometry(QRect(10, 100, 121, 23));
  dateTimeEdit = new QDateTimeEdit(tab);
  dateTimeEdit->setObjectName(QStringLiteral("dateTimeEdit"));
  dateTimeEdit->setGeometry(QRect(10, 120, 201, 31));
  dateTimeEdit->setCalendarPopup(true);
  ntpRadioButtion = new QRadioButton(tab);
  ntpRadioButtion->setObjectName(QStringLiteral("ntpRadioButtion"));
  ntpRadioButtion->setEnabled(true);
  ntpRadioButtion->setGeometry(QRect(10, 10, 191, 23));
  ntpRadioButtion->setChecked(false);
  ntpRadioButtion->setAutoRepeat(false);
  ntp_list = new QComboBox(tab);
  ntp_list->setObjectName(QStringLiteral("ntp_list"));
  ntp_list->setGeometry(QRect(10, 40, 391, 31));
  ntp_list->setEditable(true);
  syncRadioButtion = new QRadioButton(tab);
  syncRadioButtion->setObjectName(QStringLiteral("syncRadioButtion"));
  syncRadioButtion->setGeometry(QRect(10, 180, 112, 23));
  syncRadioButtion->setChecked(true);
  syncRadioButtion->setAutoRepeat(false);
  timezone = new QComboBox(tab);
  timezone->setObjectName(QStringLiteral("timezone"));
  timezone->setGeometry(QRect(260, 120, 141, 31));
  timezone->setEditable(true);
  frame_7 = new QFrame(tab_2);
  frame_7->setObjectName(QStringLiteral("frame_7"));
  frame_7->setGeometry(QRect(10, 10, 400, 231));
  frame_7->setFrameShape(QFrame::StyledPanel);
  frame_7->setFrameShadow(QFrame::Raised);
  resolutions = new QComboBox(frame_7);
  resolutions->setObjectName(QStringLiteral("resolutions"));
  resolutions->setGeometry(QRect(130, 110, 131, 31));
  resolutions->setEditable(false);
  frameRate = new QSpinBox(frame_7);
  frameRate->setObjectName(QStringLiteral("frameRate"));
  frameRate->setGeometry(QRect(130, 150, 48, 31));
  frameRate->setMinimum(1);
  frameRate->setMaximum(30);
  label_11 = new QLabel(frame_7);
  label_11->setObjectName(QStringLiteral("label_11"));
  label_11->setGeometry(QRect(40, 150, 81, 25));
  label_12 = new QLabel(frame_7);
  label_12->setObjectName(QStringLiteral("label_12"));
  label_12->setGeometry(QRect(40, 110, 81, 25));
  label_14 = new QLabel(frame_7);
  label_14->setObjectName(QStringLiteral("label_14"));
  label_14->setGeometry(QRect(10, 10, 101, 25));
  label_15 = new QLabel(frame_7);
  label_15->setObjectName(QStringLiteral("label_15"));
  label_15->setGeometry(QRect(40, 190, 81, 25));
  group_of_pictures = new QSpinBox(frame_7);
  group_of_pictures->setObjectName(QStringLiteral("group_of_pictures"));
  group_of_pictures->setGeometry(QRect(130, 190, 48, 31));
  group_of_pictures->setMinimum(1);
  group_of_pictures->setMaximum(50);
  streamList = new QComboBox(frame_7);
  streamList->setObjectName(QStringLiteral("streamList"));
  streamList->setGeometry(QRect(40, 60, 341, 31));
  label_17 = new QLabel(frame_7);
  label_17->setObjectName(QStringLiteral("label_17"));
  label_17->setGeometry(QRect(40, 40, 91, 16));

  frame_4 = new QFrame(tab_2);
  frame_4->setObjectName(QStringLiteral("frame_4"));
  frame_4->setGeometry(QRect(10, 250, 400, 171));
  frame_4->setFrameShape(QFrame::StyledPanel);
  frame_4->setFrameShadow(QFrame::Raised);
  zoomOut = new QPushButton(frame_4);
  zoomOut->setObjectName(QStringLiteral("zoomOut"));
  zoomOut->setGeometry(QRect(320, 120, 31, 31));
  right = new QPushButton(frame_4);
  right->setObjectName(QStringLiteral("right"));
  right->setGeometry(QRect(130, 80, 31, 31));
  zoomIn = new QPushButton(frame_4);
  zoomIn->setObjectName(QStringLiteral("zoomIn"));
  zoomIn->setGeometry(QRect(320, 40, 31, 31));
  down = new QPushButton(frame_4);
  down->setObjectName(QStringLiteral("down"));
  down->setGeometry(QRect(90, 120, 31, 31));
  left = new QPushButton(frame_4);
  left->setObjectName(QStringLiteral("left"));
  left->setGeometry(QRect(50, 80, 31, 31));
  left->setAutoFillBackground(true);
  up = new QPushButton(frame_4);
  up->setObjectName(QStringLiteral("up"));
  up->setGeometry(QRect(90, 40, 31, 31));

  leftrightSpeed = new QSlider(frame_4);
  leftrightSpeed->setObjectName(QStringLiteral("leftrightSpeed"));
  leftrightSpeed->setGeometry(QRect(50, 10, 111, 20));
  leftrightSpeed->setMinimum(5);
  leftrightSpeed->setMaximum(180);
  leftrightSpeed->setValue(180);
  leftrightSpeed->setTracking(false);
  leftrightSpeed->setOrientation(Qt::Horizontal);
  updownSpeed = new QSlider(frame_4);
  updownSpeed->setObjectName(QStringLiteral("updownSpeed"));
  updownSpeed->setGeometry(QRect(20, 40, 20, 111));
  updownSpeed->setMinimum(5);
  updownSpeed->setMaximum(179);
  updownSpeed->setValue(179);
  updownSpeed->setOrientation(Qt::Vertical);
  center = new QPushButton(frame_4);
  center->setObjectName(QStringLiteral("center"));
  center->setGeometry(QRect(90, 80, 31, 31));
  zoom = new QSlider(frame_4);
  zoom->setObjectName(QStringLiteral("zoom"));
  zoom->setGeometry(QRect(360, 40, 20, 111));
  zoom->setMinimum(5);
  zoom->setMaximum(100);
  zoom->setSingleStep(5);
  zoom->setValue(100);
  zoom->setOrientation(Qt::Vertical);
  zoomOut->setText("-");
  right->setText("R");
  zoomIn->setText("+");
  down->setText("D");
  left->setText("L");
  up->setText("U");
  center->setText(QString());
  label_8->setText("Password:");
  username->setText("admin");
  password->setText("01CD9E");
  label_7->setText("Username:");

  label_9->setText("Proxy Addr:");
  label_2->setText("Brightness:");
  label->setText("CbGain:");
  label_6->setText("Saturation:");
  label_10->setText("CrGain:");
  label_4->setText("Sharpness:");
  label_3->setText("Contrast:");
  label_13->setText("Image Setting");
  saveConfig->setText("Save");
  manualRadioButton->setText("Manual Config");
  dateTimeEdit->setDisplayFormat("dd/MM/yyyy HH:mm:ss");
  ntpRadioButtion->setText("NTP Server");
  syncRadioButtion->setText("Sync with PC");
  label_11->setText("Frame rate:");
  label_12->setText("Resolution:");
  label_14->setText("Video Setting");
  label_15->setText("GOPs: ");
  label_17->setText("Stream URL:");

  tabWidget = new QTabWidget(this->zone);
  tabWidget->setObjectName(QStringLiteral("tabWidget"));
  tabWidget->setGeometry(QRect(810, 10, 420, 521));
  tabWidget->addTab(Main, QString());
  tabWidget->addTab(tab, QString());
  tabWidget->addTab(tab_2, QString());
  tabWidget->setTabText(tabWidget->indexOf(Main), "Main");
  tabWidget->setTabText(tabWidget->indexOf(tab), "Date Time");
  tabWidget->setTabText(tabWidget->indexOf(tab_2), "Video Config");
  tabWidget->setCurrentIndex(0);

  connect(this->ntpRadioButtion, &QRadioButton::clicked, this, &P_OVOnvifControl::on_ntpRadioButtion_clicked);
  connect(this->manualRadioButton, &QRadioButton::clicked, this, &P_OVOnvifControl::on_manualRadioButton_clicked);
  connect(this->saveConfig, &QPushButton::clicked, this, &P_OVOnvifControl::on_saveconfig_clicked);

  connect(this->streamList, &QComboBox::currentTextChanged, this, &P_OVOnvifControl::on_streamList_currentTextChanged);
  connect(this->resolutions, &QComboBox::currentTextChanged, this, &P_OVOnvifControl::on_resolutions_currentTextChanged);
  connect(this->group_of_pictures, &QSpinBox::editingFinished, this, &P_OVOnvifControl::on_group_of_pictures_editingFinished);
  connect(this->frameRate, &QSpinBox::editingFinished, this, &P_OVOnvifControl::on_frameRate_editingFinished);

  connect(this->CbGainSlider, &QSlider::valueChanged, this, &P_OVOnvifControl::on_CbGainSlider_valueChanged);
  connect(this->CrGainSlider, &QSlider::valueChanged, this, &P_OVOnvifControl::on_CrGainSlider_valueChanged);
  connect(this->brightnessSlider, &QSlider::valueChanged, this, &P_OVOnvifControl::on_brightnessSlider_valueChanged);
  connect(this->contrastSlider, &QSlider::valueChanged, this, &P_OVOnvifControl::on_contrastSlider_valueChanged);
  connect(this->saturationSlider, &QSlider::valueChanged, this, &P_OVOnvifControl::on_saturationSlider_valueChanged);
  connect(this->sharpnessSlider, &QSlider::valueChanged, this, &P_OVOnvifControl::on_sharpnessSlider_valueChanged);

  connect(this->up, &QPushButton::clicked, this, &P_OVOnvifControl::on_up_clicked);
  connect(this->down, &QPushButton::clicked, this, &P_OVOnvifControl::on_down_clicked);
  connect(this->left, &QPushButton::clicked, this, &P_OVOnvifControl::on_left_clicked);
  connect(this->right, &QPushButton::clicked, this, &P_OVOnvifControl::on_right_clicked);
  connect(this->zoomIn, &QPushButton::clicked, this, &P_OVOnvifControl::on_zoomIn_clicked);
  connect(this->zoomOut, &QPushButton::clicked, this, &P_OVOnvifControl::on_zoomOut_clicked);
  connect(this->center, &QPushButton::clicked, this, &P_OVOnvifControl::on_center_clicked);

  this->layout->addWidget(this->tabWidget);
}


void P_OVOnvifControl::show(QWidget *zone) { Q_UNUSED(zone) }

void P_OVOnvifControl::update() {}

QWidget *P_OVOnvifControl::getZone(int zoneId) {
    switch (zoneId) {

    default:
        return Q_NULLPTR;
    }
}

void P_OVOnvifControl::enterFullscreenMode() {

}

void P_OVOnvifControl::exitFullscreenMode() {

}


void P_OVOnvifControl::open_message_box(const QString &text)
{
  QMessageBox Msgbox;
  Msgbox.setText(text);
  Msgbox.exec();
}

void P_OVOnvifControl::connect_to_camera(QVariant* attachment)
{
  if (this->onvifDevice != Q_NULLPTR) {
    delete(this->onvifDevice);
    this->onvifDevice = Q_NULLPTR;
  }
  if (this->onvifImaging != Q_NULLPTR) {
    delete(this->onvifImaging);
    this->onvifImaging = Q_NULLPTR;
  }
  if (this->onvifMedia != Q_NULLPTR) {
    delete(this->onvifMedia);
    this->onvifMedia = Q_NULLPTR;
  }
  if (this->onvifPTZ != Q_NULLPTR) {
    delete(this->onvifPTZ);
    this->onvifPTZ = Q_NULLPTR;
  }

  QString dev_url = attachment->value<QString>();

  this->onvifDevice = new OnvifDevice(dev_url.toLatin1().data(),
                                      this->username->text().toLatin1().data(),
                                      this->password->text().toLatin1().data(),
                                      this->proxylist->currentText().toLatin1().data(),
                                      ONVIF_PROXY_PORT);

  if (onvifDevice->errorLog.size() > 0) {
    open_message_box(QString::fromStdString(this->onvifDevice->errorLog));
    delete(this->onvifDevice);
    this->onvifDevice = Q_NULLPTR;
    return ;
  } else
    qDebug() << "For device: " << dev_url;

  this->onvifMedia = new OnvifMedia(dev_url.toLatin1().data(),
                                    this->username->text().toLatin1().data(),
                                    this->password->text().toLatin1().data(),
                                    this->proxylist->currentText().toLatin1().data(),
                                    ONVIF_PROXY_PORT);
  if (this->onvifMedia->errorLog.size() > 0) {
    return ;
  }

  this->onvifImaging = new OnvifImaging(dev_url.toLatin1().data(),
                                        this->username->text().toLatin1().data(),
                                        this->password->text().toLatin1().data(),
                                        this->proxylist->currentText().toLatin1().data(),
                                        ONVIF_PROXY_PORT);

  this->onvifPTZ = new OnvifPTZ(dev_url.toLatin1().data(),
                                    this->username->text().toLatin1().data(),
                                    this->password->text().toLatin1().data(),
                                    this->proxylist->currentText().toLatin1().data(),
                                    ONVIF_PROXY_PORT);

  get_camera_info();
}

void P_OVOnvifControl::get_camera_info(void)
{

  this->dateTimeEdit->clear();
  this->timezone->clear();
  this->devInfoText->clear();
  this->ntp_list->clear();
  if (this->onvifDevice != Q_NULLPTR) {
    // Device Info
    std::string dev_info;
    this->onvifDevice->getDeviceInformation(dev_info);
    this->devInfoText->append(QString::fromStdString(dev_info));

//    on_ntpRadioButtion_clicked();
//    on_manualRadioButton_clicked();
  } else {
    return ;
  }

  this->streamList->clear();
  if (this->onvifMedia != Q_NULLPTR) {
    // RTSP Url
    std::string stream_uri;
    this->onvifMedia->getProfiles();
    if (this->onvifMedia->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifMedia->errorLog));
      return;
    }

    for (tt__Profile *profile : this->onvifMedia->Profiles) {
      this->onvifMedia->getStreamURI(profile->token, stream_uri);
      this->streamList->addItem(QString::fromStdString(stream_uri));
    }
  } else {
    return ;
  }

  if (this->onvifImaging != Q_NULLPTR) {

  }

  if (this->onvifPTZ != Q_NULLPTR) {

  }
}

void P_OVOnvifControl::on_saveconfig_clicked()
{
  if (this->onvifDevice == Q_NULLPTR)
    return;

  if (this->ntpRadioButtion->isChecked()){
    if (this->ntp_list->currentText().isEmpty()){
      return;
    }

    this->onvifDevice->dateTime->DateTimeType = tt__SetDateTimeType__NTP;
//    this->onvifDevice->setNTP();
  } else {
    this->onvifDevice->dateTime->DateTimeType = tt__SetDateTimeType__Manual;

    if (this->timezone->currentText().isEmpty())
      return ;
    this->onvifDevice->getSystemDateAndTime();
    if (this->onvifDevice->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifDevice->errorLog));
      return;
    }


    QDateTime dateTime;
    if (this->syncRadioButtion->isChecked()) {
      dateTime = QDateTime::currentDateTime();
    } else {
      dateTime = this->dateTimeEdit->dateTime();
    }

    // Temporary solutions for Enxun camera (ToanNX's request - 08/06/2018)
    if (this->onvifDevice->deviceInfo.Model.find("GRAIN") >= 0) {
      this->onvifDevice->dateTime->TimeZone->TZ = "GMT-07:00";
    } else {
      this->onvifDevice->dateTime->TimeZone->TZ = this->timezone->currentText().toLatin1().data();
    }

    this->onvifDevice->dateTime->DateTimeType = tt__SetDateTimeType__Manual;

    tt__DateTime *utc_datetime = this->onvifDevice->dateTime->UTCDateTime;
    utc_datetime->Time->Hour   = dateTime.toUTC().toString("hh").toInt();
    utc_datetime->Time->Minute = dateTime.toUTC().toString("mm").toInt();
    utc_datetime->Time->Second = dateTime.toUTC().toString("ss").toInt()+1;
    utc_datetime->Date->Day    = dateTime.toUTC().toString("dd").toInt();
    utc_datetime->Date->Month  = dateTime.toUTC().toString("MM").toInt();
    utc_datetime->Date->Year   = dateTime.toUTC().toString("yyyy").toInt();

    qDebug() << "Set Time: " << dateTime.toString("hh:mm:ss dd-MM-yyyy") << ", TZ: " << this->onvifDevice->dateTime->TimeZone->TZ.c_str();
    qDebug() << "Set UTC: " << dateTime.toUTC().toString("hh:mm:ss dd-MM-yyyy");
 }

  this->onvifDevice->setSystemDateAndTime();
  if (this->onvifDevice->errorLog.length() > 0) {
        open_message_box(QString::fromStdString(this->onvifDevice->errorLog));
    return;
  }
}

void P_OVOnvifControl::on_ntpRadioButtion_clicked()
{
  if (this->onvifDevice == Q_NULLPTR)
    return;

  this->ntp_list->clear();

  this->onvifDevice->getNTP();
  if (this->onvifDevice->errorLog.size() > 0) {
    return;
  }

  if (this->onvifDevice->ntpInfo == NULL) {
    this->ntp_list->addItem("Not configured ...");
    return;
  }

  std::vector<tt__NetworkHost *> ntp_list;
  if (this->onvifDevice->ntpInfo->FromDHCP) {
    ntp_list = this->onvifDevice->ntpInfo->NTPFromDHCP;
    this->ntpRadioButtion->setText("NTP from DHCP");
  } else {
    ntp_list = this->onvifDevice->ntpInfo->NTPManual;
    this->ntpRadioButtion->setText("NTP manual");
  }

  for (tt__NetworkHost * network_host : ntp_list) {
    switch (network_host->Type) {
      case tt__NetworkHostType__DNS:
        this->ntp_list->addItem(QString::fromStdString(network_host->DNSname->c_str()));
        break;
      case tt__NetworkHostType__IPv4:
        this->ntp_list->addItem(QString::fromStdString(network_host->IPv4Address->c_str()));
        break;
      case tt__NetworkHostType__IPv6:
        this->ntp_list->addItem(QString::fromStdString(network_host->IPv6Address->c_str()));
        break;
      default:
        break;
    }
  }
}

void P_OVOnvifControl::on_manualRadioButton_clicked()
{
  if (this->onvifDevice == Q_NULLPTR)
    return;

  this->timezone->clear();

  this->onvifDevice->getSystemDateAndTime();
  if (this->onvifDevice->errorLog.size() > 0) {
    open_message_box(QString::fromStdString(this->onvifDevice->errorLog));
    return;
  }

  this->timezone->addItem(QString::fromStdString(this->onvifDevice->dateTime->TimeZone->TZ));
  this->timezone->addItem(QString::fromStdString("GMT-07:00"));
  this->timezone->addItem(QString::fromStdString("GMT"));
  this->timezone->addItem(QString::fromStdString("GMT+07:00"));

  tt__DateTime *utc_datetime = this->onvifDevice->dateTime->UTCDateTime;
  QString date_str = QString::fromStdString(std::to_string(utc_datetime->Date->Year)+"-"+
                                            std::to_string(utc_datetime->Date->Month)+"-"+
                                            std::to_string(utc_datetime->Date->Day)+" "+
                                            std::to_string(utc_datetime->Time->Hour)+":"+
                                            std::to_string(utc_datetime->Time->Minute)+":"+
                                            std::to_string(utc_datetime->Time->Second));
  QString format = "yyyy-M-d h:m:s";
  QDateTime date_time = QDateTime::fromString(date_str, format);
  this->dateTimeEdit->setDisplayFormat("hh:mm:ss dd-MM-yyyy");
  this->dateTimeEdit->setDateTime(date_time);
}

void P_OVOnvifControl::on_streamList_currentTextChanged(const QString &currentText)
{
//  if ((this->onvifMedia == NULL) ||(currentText.isEmpty()))
  if (this->onvifMedia == NULL)
    return;

  QStringList temp = currentText.split("//", QString::SkipEmptyParts);

  QString user_pass = "rtsp://"+this->username->text()+":"+this->password->text();
  temp.replaceInStrings("rtsp:", user_pass);

  QString rtsp = temp.join("@");

  QVariant* attachment = new QVariant();
  attachment->setValue<QString>(rtsp);

  int index = this->streamList->currentIndex();

  this->control()->newUserAction(ONVIF_RTSP_STREAM_CHANGED, attachment);

  get_video_setting(index);
  get_image_setting(index);
}

void P_OVOnvifControl::get_video_setting(int index)
{
  if ((this->onvifMedia == NULL) || (index<0))
    return ;

  this->resolutions->setEnabled(false);
  this->frameRate->setEnabled(false);
  this->group_of_pictures->setEnabled(false);

  // Get VideoEncoderConfiguration
  std::string configurationToken = this->onvifMedia->Profiles[index]->VideoEncoderConfiguration->token;

  this->onvifMedia->getVideoEncoderConfiguration(configurationToken);
  if (this->onvifMedia->errorLog.size() > 0) {
    open_message_box(QString::fromStdString(this->onvifMedia->errorLog));
    return;
  }

  // Add available resolutions of camera
  tt__VideoEncoderConfiguration *video_encoder_configuration = this->onvifMedia->videoEncoderConfiguration;
  int fps = video_encoder_configuration->RateControl->FrameRateLimit;
  this->frameRate->setValue(fps);
  this->frameRate->setEnabled(true);

  this->resolutions->clear();
  std::string profileToken = this->onvifMedia->Profiles[index]->token;
  this->onvifMedia->getVideoEncoderConfigurationOptions(profileToken, configurationToken);
  if (this->onvifMedia->videoEncoderConfigurationOptions->H264 != NULL) {
    for (tt__VideoResolution * resolution : this->onvifMedia->videoEncoderConfigurationOptions->H264->ResolutionsAvailable) {

      int height = resolution->Height;
      int width = resolution->Width;

      QString resol = QString::fromStdString(std::to_string(width)+"x"+std::to_string(height)+"(*)");
      this->resolutions->addItem(resol);
    }
  }

  if (this->onvifMedia->videoEncoderConfigurationOptions->MPEG4 != NULL) {
    for (tt__VideoResolution * resolution : this->onvifMedia->videoEncoderConfigurationOptions->MPEG4->ResolutionsAvailable) {

      int height = resolution->Height;
      int width = resolution->Width;

      QString resol = QString::fromStdString(std::to_string(width)+"x"+std::to_string(height)+"(*)");

      this->resolutions->addItem(resol);
    }
  }

  int height = video_encoder_configuration->Resolution->Height;
  int width = video_encoder_configuration->Resolution->Width;

  // Display current resolution
  QString resolution = QString::fromStdString(std::to_string(width)+"x"+std::to_string(height)+"(*)");
  int resolution_index = this->resolutions->findText(resolution);
  if (resolution_index >= 0) {
    this->resolutions->setCurrentIndex(resolution_index);
  } else {
    this->resolutions->addItem(resolution);
    resolution_index = this->resolutions->findText(resolution);
    this->resolutions->setCurrentIndex(resolution_index);
  }

  this->resolutions->setEnabled(true);

  // Add default resolutions
  std::vector<QString> resolution_list = {"320x240","640x352","640x480","704x576","1280x720","1280x960",
                                          "1920x1080","2048x1536","2592x1944","3840x2160"};

  for (QString resol : resolution_list) {
    int tmp = this->resolutions->findText(resol+"(*)");
    if (tmp < 0) {
      this->resolutions->addItem(resol);
    }
  }

  this->group_of_pictures->setEnabled(true);
  if (video_encoder_configuration->Encoding == tt__VideoEncoding__H264) {
    int group_of_pictures = video_encoder_configuration->H264->GovLength;

    this->group_of_pictures->setValue(group_of_pictures);
  } else if (video_encoder_configuration->Encoding == tt__VideoEncoding__MPEG4) {
    int group_of_pictures = video_encoder_configuration->MPEG4->GovLength;

    this->group_of_pictures->setValue(group_of_pictures);
  } else {
    //tt__VideoEncoding__JPEG
  }
}

void P_OVOnvifControl::on_frameRate_editingFinished()
{
  if (this->onvifMedia == NULL)
    return;

  int value = this->frameRate->value();

  int current_value = this->onvifMedia->videoEncoderConfiguration->RateControl->FrameRateLimit;
  if (current_value != value) {
    this->onvifMedia->videoEncoderConfiguration->RateControl->FrameRateLimit = value;

    this->onvifMedia->setVideoEncoderConfiguration();
    if (this->onvifMedia->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifMedia->errorLog));
      return;
    }
  }
}

void P_OVOnvifControl::on_group_of_pictures_editingFinished()
{
  if (this->onvifMedia == NULL)
    return;

  int current_value;
  int value = this->group_of_pictures->value();

  if (this->onvifMedia->videoEncoderConfiguration->Encoding == tt__VideoEncoding__H264) {
    current_value = this->onvifMedia->videoEncoderConfiguration->H264->GovLength;
  } else if (this->onvifMedia->videoEncoderConfiguration->Encoding == tt__VideoEncoding__MPEG4) {
    current_value = this->onvifMedia->videoEncoderConfiguration->MPEG4->GovLength;
  } else { //tt__VideoEncoding__JPEG
    // Do nothing
  }

  if (current_value != value) {
    this->onvifMedia->setVideoEncoderConfiguration();
    if (this->onvifMedia->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifMedia->errorLog));
      return;
    }
  }
}

void P_OVOnvifControl::on_resolutions_currentTextChanged(const QString &arg1)
{
  if ((this->onvifMedia == NULL) || (this->resolutions->currentIndex() < 0))
    return;

  QStringList tmp = arg1.split(QRegExp("\\W+"), QString::SkipEmptyParts);
  QStringList resolution = tmp[0].split('x', QString::SkipEmptyParts);
  this->onvifMedia->videoEncoderConfiguration->Resolution->Width = resolution[0].toInt();
  this->onvifMedia->videoEncoderConfiguration->Resolution->Height = resolution[1].toInt();

  this->onvifMedia->setVideoEncoderConfiguration();
  if (this->onvifMedia->errorLog.size() > 0) {
    open_message_box(QString::fromStdString(this->onvifMedia->errorLog));
    return;
  }
}

void P_OVOnvifControl::get_image_setting(int index)
{
  if ((this->onvifImaging == NULL) || (index < 0))
    return ;

  this->CbGainSlider->setEnabled(false);
  this->CrGainSlider->setEnabled(false);
  this->brightnessSlider->setEnabled(false);
  this->contrastSlider->setEnabled(false);
  this->saturationSlider->setEnabled(false);
  this->sharpnessSlider->setEnabled(false);

  std::string video_source_token = this->onvifMedia->Profiles[index]->VideoSourceConfiguration->SourceToken;

  if (this->onvifImaging != NULL) {
    this->onvifImaging->getImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }

    tt__ImagingSettings20 *imaging_settings = this->onvifImaging->imagingSettings;

    if (imaging_settings->WhiteBalance != NULL) {
      qDebug() << "CbGain: " << *imaging_settings->WhiteBalance->CbGain;
      qDebug() << "CrGain: " << *imaging_settings->WhiteBalance->CrGain;
      this->CbGainSlider->setValue((int) *imaging_settings->WhiteBalance->CbGain);
      this->CrGainSlider->setValue((int) *imaging_settings->WhiteBalance->CrGain);
      this->CbGainSlider->setEnabled(true);
      this->CrGainSlider->setEnabled(true);
    }

    if (imaging_settings->Brightness != NULL) {
      qDebug() << "Brightness: " << *imaging_settings->Brightness;
      this->brightnessSlider->setValue((int) *imaging_settings->Brightness);
      this->brightnessSlider->setEnabled(true);
    }
    if (imaging_settings->ColorSaturation != NULL) {
      qDebug() << "Saturation: " << *imaging_settings->ColorSaturation;
      this->saturationSlider->setValue((int) *imaging_settings->ColorSaturation);
      this->saturationSlider->setEnabled(true);
    }
    if (imaging_settings->Contrast != NULL) {
      qDebug() << "Contrast: " << *imaging_settings->Contrast;
      this->contrastSlider->setValue((int) *imaging_settings->Contrast);
      this->contrastSlider->setEnabled(true);
    }
    if (imaging_settings->Sharpness != NULL) {
      qDebug() << "Sharpness: " << *imaging_settings->Sharpness;
      this->sharpnessSlider->setValue((int) *imaging_settings->Sharpness);
      this->sharpnessSlider->setEnabled(true);
    }
  }
}

void P_OVOnvifControl::on_CbGainSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string video_source_token = this->onvifMedia->Profiles[index]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->WhiteBalance->CbGain;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->WhiteBalance->CbGain = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifControl::on_CrGainSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string video_source_token = this->onvifMedia->Profiles[index]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->WhiteBalance->CrGain;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->WhiteBalance->CrGain = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifControl::on_brightnessSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string video_source_token = this->onvifMedia->Profiles[index]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->Brightness;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->Brightness = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifControl::on_contrastSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string video_source_token = this->onvifMedia->Profiles[index]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->Contrast;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->Contrast = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifControl::on_sharpnessSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string video_source_token = this->onvifMedia->Profiles[index]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->Sharpness;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->Sharpness = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifControl::on_saturationSlider_valueChanged(int value)
{
  if ((this->onvifImaging == NULL) || (this->onvifMedia == NULL))
    return;

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string video_source_token = this->onvifMedia->Profiles[index]->VideoSourceConfiguration->SourceToken;

  int current_value = *this->onvifImaging->imagingSettings->ColorSaturation;
  if (current_value != value) {
    *this->onvifImaging->imagingSettings->ColorSaturation = (float) value;

    this->onvifImaging->setImagingSettings(video_source_token);
    if (this->onvifImaging->errorLog.size() > 0) {
      open_message_box(QString::fromStdString(this->onvifImaging->errorLog));
      return;
    }
  }
}

void P_OVOnvifControl::on_zoomIn_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string profile_token = this->onvifMedia->Profiles[index]->token;

  this->onvifPTZ->zoomIn(profile_token, (float) this->zoom->value()/100);
}

void P_OVOnvifControl::on_zoomOut_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string profile_token = this->onvifMedia->Profiles[index]->token;

  this->onvifPTZ->zoomOut(profile_token, this->zoom->value()/100);
}

void P_OVOnvifControl::on_up_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string profile_token = this->onvifMedia->Profiles[index]->token;

  this->onvifPTZ->tiltUp(profile_token, this->updownSpeed->value());
}

void P_OVOnvifControl::on_down_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string profile_token = this->onvifMedia->Profiles[index]->token;


  this->onvifPTZ->tiltDown(profile_token, this->updownSpeed->value());
}

void P_OVOnvifControl::on_left_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string profile_token = this->onvifMedia->Profiles[index]->token;

  this->onvifPTZ->panLeft(profile_token, this->leftrightSpeed->value());
}

void P_OVOnvifControl::on_right_clicked()
{
  if ((this->onvifPTZ == NULL) || (this->onvifMedia == NULL)) {
    return;
  }

  int index = this->streamList->currentIndex();
  if (index < 0)
    return;

  std::string profile_token = this->onvifMedia->Profiles[index]->token;

  this->onvifPTZ->panRight(profile_token, this->leftrightSpeed->value());
}

void P_OVOnvifControl::on_center_clicked()
{

}
