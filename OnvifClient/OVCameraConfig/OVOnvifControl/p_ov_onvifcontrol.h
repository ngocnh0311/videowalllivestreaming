#ifndef P_OVOnvifControl_H
#define P_OVOnvifControl_H
#include <QObject>
#include <QLayout>
#include <QWidget>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QTextBrowser>
#include <QPushButton>
#include <QRadioButton>
#include <QSpinBox>
#include <QDateTimeEdit>
#include <QJsonObject>
#include <QMessageBox>

#include <PacModel/presentation.h>

class C_OVOnvifControl;


class OnvifDevice;
class OnvifImaging;
class OnvifMedia;
class OnvifPTZ;

class P_OVOnvifControl : public Presentation {
    // init ui control
private:
  OnvifDevice *onvifDevice = Q_NULLPTR;
  OnvifImaging *onvifImaging = Q_NULLPTR;
  OnvifMedia *onvifMedia = Q_NULLPTR;
  OnvifPTZ *onvifPTZ = Q_NULLPTR;

  QLabel *label_2;
  QLabel *label_4;
  QLabel *label_3;
  QLabel *label_8;
  QLabel *label_7;
  QLabel *label_9;
  QLabel *label_11;
  QLabel *label_12;
  QLabel *label_14;
  QLabel *label_15;
  QLabel *label_17;
  QLabel *label_13;
  QLabel *label;
  QLabel *label_6;
  QLabel *label_10;

  QFrame *frame_2;
  QFrame *frame;
  QFrame *frame_4;
  QFrame *frame_7;
  QFrame *frame_6;
  QWidget *tab;
  QWidget *tab_2;
  QWidget *Main;
  QTabWidget *tabWidget;

  QPushButton *zoomOut;
  QPushButton *right;
  QPushButton *zoomIn;
  QPushButton *down;
  QPushButton *left;
  QPushButton *up;
  QSlider *leftrightSpeed;
  QSlider *updownSpeed;
  QPushButton *center;
  QSlider *zoom;
  QLineEdit *username;
  QLineEdit *password;
  QComboBox *proxylist;
  QTextBrowser *devInfoText;
  QSlider *sharpnessSlider;
  QSlider *contrastSlider;
  QSlider *CbGainSlider;
  QSlider *CrGainSlider;
  QSlider *saturationSlider;
  QSlider *brightnessSlider;
  QPushButton *saveConfig;
  QRadioButton *manualRadioButton;
  QDateTimeEdit *dateTimeEdit;
  QRadioButton *ntpRadioButtion;
  QComboBox *ntp_list;
  QRadioButton *syncRadioButtion;
  QComboBox *timezone;
  QComboBox *resolutions;
  QSpinBox *frameRate;
  QSpinBox *group_of_pictures;
  QComboBox *streamList;

public:
    QLayout *layout = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;

    P_OVOnvifControl(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_OVOnvifControl *control() { return (C_OVOnvifControl *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

    void open_message_box(const QString &text);
    void connect_to_camera(QVariant* attachment);
    void get_camera_info(void);


protected:

public Q_SLOTS:
    void on_saveconfig_clicked();
    void on_manualRadioButton_clicked();
    void on_ntpRadioButtion_clicked();
    void on_streamList_currentTextChanged(const QString &currentText);
    void get_video_setting(int index);
    void on_frameRate_editingFinished();
    void on_group_of_pictures_editingFinished();
    void on_resolutions_currentTextChanged(const QString &arg1);

    void get_image_setting(int index);
    void on_CbGainSlider_valueChanged(int value);
    void on_CrGainSlider_valueChanged(int value);
    void on_brightnessSlider_valueChanged(int value);
    void on_contrastSlider_valueChanged(int value);
    void on_sharpnessSlider_valueChanged(int value);
    void on_saturationSlider_valueChanged(int value);

    void on_zoomIn_clicked();
    void on_zoomOut_clicked();
    void on_up_clicked();
    void on_down_clicked();
    void on_left_clicked();
    void on_right_clicked();
    void on_center_clicked();

};

#endif  // P_OVOnvifControl_H
