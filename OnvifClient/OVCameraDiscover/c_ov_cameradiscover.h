#ifndef C_OVCameraDiscover_H
#define C_OVCameraDiscover_H
#include <QString>
#include <QDebug>
#include <QObject>

#include "PacModel/control.h"

class P_OVCameraDiscover;
class A_OVCameraDiscover;

class C_OnvifClient;
class C_OVCameraDiscover;
class C_OVCameraConfig;

class C_OVCameraDiscover : public Control {
 public:
  QWidget* zone;

//  C_OVCameraDiscover *cCameraDiscover = Q_NULLPTR;
//  C_OVCameraConfig *cCameraConfig = Q_NULLPTR;

  C_OVCameraDiscover(Control* ctrl, QWidget *_zone);
  C_OnvifClient* getParent() { return (C_OnvifClient*)this->parent; }

  P_OVCameraDiscover* presentation() { return (P_OVCameraDiscover*)pres; }
  A_OVCameraDiscover* abstraction() { return (A_OVCameraDiscover*)abst; }

  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_OVCameraDiscover_H
