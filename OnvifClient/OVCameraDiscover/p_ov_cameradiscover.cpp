#include "p_ov_cameradiscover.h"
#include "c_ov_cameradiscover.h"

#include <regex>
#include "soapH.h"
#include "wsaapi.h"
#include "soapwsddProxy.h"

#include "onvif_message.h"

/**
  * Generic method to override for updating the presention.
  **/

P_OVCameraDiscover::P_OVCameraDiscover(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
  this->zone = _zone;

  this->layout = new QVBoxLayout();
  this->layout->setSpacing(0);
  this->zone->setLayout(this->layout);

  this->btnDiscover = new QPushButton(this->zone);
  this->btnDiscover->setFixedHeight(45);
  this->btnDiscover->setText("Discover");

  this->cameraList = new QListWidget(this->zone);

  this->layout->addWidget(this->btnDiscover);
  this->layout->addWidget(this->cameraList);

  connect(this->btnDiscover, &QPushButton::clicked, this, &P_OVCameraDiscover::on_btnDiscovery_clicked);
  connect(this->cameraList, &QListWidget::currentTextChanged, this, &P_OVCameraDiscover::on_cameraList_currentTextChanged);
}


void P_OVCameraDiscover::show(QWidget *zone) { Q_UNUSED(zone) }

void P_OVCameraDiscover::update() {}

QWidget *P_OVCameraDiscover::getZone(int zoneId) {
    switch (zoneId) {

    default:
        return Q_NULLPTR;
    }
}

void P_OVCameraDiscover::enterFullscreenMode() {
  /* rightBar->setFixedWidth(0);*/
}

void P_OVCameraDiscover::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}

static inline std::vector<std::string> string_split(const std::string& input, const std::string& regex) {
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    std::sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;
    return {first, last};
}

void P_OVCameraDiscover::on_btnDiscovery_clicked()
{
  this->cameraList->clear();

  devUrlList.clear();

  struct wsdd__ProbeType probe;
  struct __wsdd__ProbeMatches matches;

  probe.Scopes = new struct wsdd__ScopesType();
  probe.Types = (char*)"tdn:NetworkVideoTransmitter";

  struct soap *soap = soap_new();
  std::string tmpuuid = soap_wsa_rand_uuid(soap);

  wsddProxy *discoverProxy;
  discoverProxy = new wsddProxy("soap.udp://239.255.255.250:3702/");

  discoverProxy->soap_header(
    (char*)tmpuuid.c_str(),
    NULL,
    NULL,
    NULL,
    NULL,
    "urn:schemas-xmlsoap-org:ws:2005:04:discovery",
    "http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe",
    NULL,
    NULL);

  discoverProxy->recv_timeout = 1;
  discoverProxy->send_Probe(&probe);

  while ( discoverProxy->recv_ProbeMatches(matches) == SOAP_OK) {
    if (matches.wsdd__ProbeMatches != NULL) {
      for (int i=0; i < matches.wsdd__ProbeMatches->__sizeProbeMatch; ++i) {
        std::string dev_url = matches.wsdd__ProbeMatches->ProbeMatch->XAddrs;

        std::vector<std::string> tmp = string_split(dev_url, "/");

        devUrlList.push_back(tmp[2]);
      }
    } else {
      qDebug("SHOULD NOT GO HERE!!!");
      exit(EXIT_FAILURE);
    }
  }
  discoverProxy->destroy();

  soap_destroy(soap);
  soap_end(soap);
  soap_free(soap);

  if (devUrlList.size() == 0) {
      QMessageBox Msgbox;
      Msgbox.setText("Not found any camera!!!");
      Msgbox.exec();
      return;
  }

  for( std::string dev_url : devUrlList) {
    this->cameraList->addItem(QString::fromStdString(dev_url));
  }
}

void P_OVCameraDiscover::on_cameraList_currentTextChanged(const QString &currentText)
{
  if (currentText.isEmpty())
    return;

  QVariant *attachment = new QVariant();
  attachment->setValue<QString>(currentText);

  this->control()->newUserAction(ONVIF_CAMERA_SELECTED, attachment);
}
