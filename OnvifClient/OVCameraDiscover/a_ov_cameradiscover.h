#ifndef A_OVCameraDiscover_H
#define A_OVCameraDiscover_H

#include <QObject>

#include "PacModel/abstraction.h"

class C_OVCameraDiscover;
class A_OVCameraDiscover : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_OVCameraDiscover(Control *ctrl);
    C_OVCameraDiscover *control() { return (C_OVCameraDiscover *)this->ctrl; }
    void changeControl(Control *ctrl);

public Q_SLOTS:

};

#endif  // A_OVCameraDiscover_H
