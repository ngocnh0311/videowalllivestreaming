#include "p_ov_cameradiscover.h"
#include "a_ov_cameradiscover.h"
#include "c_ov_cameradiscover.h"

#include "onvif_message.h"

#include "OnvifClient/c_onvifclient.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_OVCameraDiscover::C_OVCameraDiscover(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    this->zone = _zone;

    this->abst = new A_OVCameraDiscover(this);
    this->pres = new P_OVCameraDiscover(this, zone);
}

/**
  * Method to receive a message from the Presentation Facet.
  * @param message    : A string which describe the request
  * @param attachment : A ref on an eventual object necessary to treat the
  *request
  **/
void C_OVCameraDiscover::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case ONVIF_CAMERA_SELECTED:
      this->getParent()->newAction(message,attachment);
      break;

    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_OVCameraDiscover::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_OVCameraDiscover::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_SHOW:{

    }break;
    case Message.PROJECT_RUN:{

    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
