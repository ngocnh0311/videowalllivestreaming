#ifndef P_OVCameraDiscover_H
#define P_OVCameraDiscover_H
#include <QObject>
#include <QLayout>
#include <QWidget>
#include <QPushButton>
#include <QListWidget>
#include <QMessageBox>

#include <PacModel/presentation.h>

class C_OVCameraDiscover;

class P_OVCameraDiscover : public Presentation {
    // init ui control
private:
    std::vector<std::string> devUrlList;

public:
    QLayout *layout = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;

    QPushButton *btnDiscover = Q_NULLPTR;
    QListWidget *cameraList = Q_NULLPTR;

    P_OVCameraDiscover(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_OVCameraDiscover *control() { return (C_OVCameraDiscover *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

protected:


public Q_SLOTS:
    void on_btnDiscovery_clicked();
    void on_cameraList_currentTextChanged(const QString &currentText);

};

#endif  // P_OVCameraDiscover_H
