#include "p_onvifclient.h"
#include "a_onvifclient.h"
#include "c_onvifclient.h"

#include "onvif_message.h"

#include "OVCameraDiscover/c_ov_cameradiscover.h"
#include "OVCameraConfig/c_ov_cameraconfig.h"

/**
  * Contructor. Register the father in the pac hierarchy.
  * @param ctrl The reference on the father. Generally the pac agent which create this agent.
  **/
C_OnvifClient::C_OnvifClient(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    this->zone = _zone;

    this->abst = new A_OnvifClient(this);
    this->pres = new P_OnvifClient(this, zone);

    cCameraDiscover = new C_OVCameraDiscover(this, presentation()->getZone(0));
    cCameraConfig = new C_OVCameraConfig(this, presentation()->getZone(1));
}

C_OnvifClient::~C_OnvifClient() {
  delete this->cCameraConfig;
  delete this->cCameraDiscover;

  delete this->abst;
  delete this->pres;
}

/**
  * Method to receive a message from the Presentation Facet.
  * @param message    : A string which describe the request
  * @param attachment : A ref on an eventual object necessary to treat the request
  **/
void C_OnvifClient::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case 1:
        break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from the Astraction Facet.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OnvifClient::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from an other agent.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_OnvifClient::newAction(int message, QVariant* attachment) {
  switch (message) {
  case Message.APP_SHOW:
    this->zone->show();
    break;

  case Message.PROJECT_RUN:
    this->zone->show();
    break;

  case ONVIF_CAMERA_SELECTED:
    this->cCameraConfig->newAction(message, attachment);
    break;

  default:
      qDebug() << "ERROR : General Internal pac action in" + getClass() +
                  "non-catched :" + Message.toString(message);
  }
}
