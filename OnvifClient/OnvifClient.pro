#-------------------------------------------------
#
# Project created by QtCreator 2018-05-11T11:55:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network multimediawidgets

TARGET = OnvifClient
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS WITH_NONAMESPACES WITH_DOM WITH_OPENSSL SOAP_DEBUG

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    common/stdsoap2.cpp \
    custom/duration.cpp \
    plugin/mecevp.cpp \
    plugin/smdevp.cpp \
    plugin/threads.cpp \
    plugin/wsaapi.cpp \
    plugin/wsseapi.cpp \
    plugin/dom.cpp \
        main.cpp \
        mainwindow.cpp \
    onvif_lib/soapC.cpp \
    onvif_lib/soapDeviceBindingProxy.cpp \
    onvif_lib/soapImagingBindingProxy.cpp \
    onvif_lib/soapMediaBindingProxy.cpp \
    onvif_lib/soapPTZBindingProxy.cpp \
    onvif_lib/soapwsddProxy.cpp \
    onvif_lib/Device/device.cpp \
    onvif_lib/Images/imaging.cpp \
    onvif_lib/Media/media.cpp \
    onvif_lib/PTZ/ptz.cpp \
    onvif_lib/soapTLSServerBindingProxy.cpp

HEADERS += \
        mainwindow.h \
    custom/duration.h \
    onvif_lib/soapDeviceBindingProxy.h \
    onvif_lib/soapH.h \
    onvif_lib/soapImagingBindingProxy.h \
    onvif_lib/soapMediaBindingProxy.h \
    onvif_lib/soapPTZBindingProxy.h \
    onvif_lib/soapStub.h \
    onvif_lib/soapwsddProxy.h \
    common/stdsoap2.h \
    plugin/mecevp.h \
    plugin/smdevp.h \
    plugin/threads.h \
    plugin/wsaapi.h \
    plugin/wsseapi.h \
    onvif_lib/Device/device.h \
    onvif_lib/wsdd.nsmap \
    ErrorLog.h \
    onvif_lib/Images/imaging.h \
    onvif_lib/Media/media.h \
    onvif_lib/PTZ/ptz.h \
    onvif_lib/soapTLSServerBindingProxy.h

INCLUDEPATH += $$PWD/common \
               $$PWD/onvif_lib \
               $$PWD/plugin

LIBS += -lssl -lcrypto

FORMS += \
        mainwindow.ui
