#ifndef C_OnvifClient_H
#define C_OnvifClient_H
#include <QString>
#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>

#include "PacModel/control.h"

class P_OnvifClient;
class A_OnvifClient;

class C_OVCameraDiscover;
class C_OVCameraConfig;

class C_OnvifClient : public Control {
 public:
  QWidget* zone;

  C_OVCameraDiscover *cCameraDiscover = Q_NULLPTR;
  C_OVCameraConfig *cCameraConfig = Q_NULLPTR;

 public:
  C_OnvifClient(Control* ctrl, QWidget *_zone);
  ~C_OnvifClient();

//  void* getParent() { return this->parent; }
  P_OnvifClient* presentation() { return (P_OnvifClient*)pres; }
  A_OnvifClient* abstraction() { return (A_OnvifClient*)abst; }

  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_OnvifClient_H
