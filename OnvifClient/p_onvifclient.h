#ifndef P_OnvifClient_H
#define P_OnvifClient_H
#include <QObject>
#include <QLayout>
#include <QWidget>

#include <PacModel/presentation.h>

class C_OnvifClient;

class P_OnvifClient : public Presentation {
    // init ui control
private:
public:
    QLayout *layout = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;

    QWidget *discover = Q_NULLPTR;
    QWidget *config = Q_NULLPTR;

    P_OnvifClient(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_OnvifClient *control() { return (C_OnvifClient *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

protected:
};

#endif  // P_OnvifClient_H
