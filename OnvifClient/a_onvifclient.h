#ifndef A_OnvifClient_H
#define A_OnvifClient_H

#include <QObject>

#include "PacModel/abstraction.h"

class C_OnvifClient;
class A_OnvifClient : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_OnvifClient(Control *ctrl);
    C_OnvifClient *control() { return (C_OnvifClient *)this->ctrl; }
    void changeControl(Control *ctrl);

public Q_SLOTS:

};

#endif  // A_OnvifClient_H
