#include "p_onvifclient.h"
#include "c_onvifclient.h"

/**
  * Generic method to override for updating the presention.
  **/

P_OnvifClient::P_OnvifClient(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
  this->zone = _zone;

  this->layout = new QHBoxLayout();
  this->layout->setSpacing(0);
  this->zone->setLayout(this->layout);
  this->zone->setStyleSheet("background-color: #222; color: white;");

  this->discover = new QWidget(this->zone);
  this->discover->setFixedWidth(200);

  this->config = new QWidget(this->zone);


  this->layout->addWidget(this->discover);
  this->layout->addWidget(this->config);
}


void P_OnvifClient::show(QWidget *zone) { Q_UNUSED(zone) }

void P_OnvifClient::update() {}

QWidget *P_OnvifClient::getZone(int zoneId) {
    switch (zoneId) {
    case 0:
        return this->discover;
    case 1:
        return this->config;

    default:
        return Q_NULLPTR;
    }
}

void P_OnvifClient::enterFullscreenMode() {
  /* rightBar->setFixedWidth(0);*/
}

void P_OnvifClient::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
