#include <QDebug>
#include <QObject>
#include <QString>

#include "imaging.h"
#include "wsseapi.h"

inline void printErr(struct soap* soap, std::string &err_log)
{
  char buf[256];

  soap_sprint_fault(soap, buf, 256);

  err_log.append(buf);
}


OnvifImaging::OnvifImaging(std::string url, std::string user_name, std::string pass_word, std::string proxy_host, int proxy_port)
{
  imagingSettings = NULL;
  errorLog.clear();
  imagingURL = "http://" + url + "/onvif/imaging_service";
  proxyImaging = new ImagingBindingProxy;

  proxyImaging->connect_timeout = 3;
  proxyImaging->send_timeout = 3;
  proxyImaging->recv_timeout = 3;

  soap_register_plugin(proxyImaging, soap_wsse);
  soap = soap_new();

  imagingSettings = NULL;
  proxyImaging->soap_endpoint = imagingURL.c_str();

  if (proxy_host.size()>0) {
    proxyHost = proxy_host;
    proxyPort = proxy_port;
    proxyImaging->proxy_host = proxyHost.c_str();
    proxyImaging->proxy_port = proxyPort;
  }

  if (SOAP_OK != soap_wsse_add_UsernameTokenDigest(proxyImaging, NULL, user_name.c_str(), pass_word.c_str())) {
    printErr(proxyImaging, errorLog);
  }
}

OnvifImaging::~OnvifImaging()
{
  delete(proxyImaging);

  soap_destroy(soap);
  soap_end(soap);
  soap_free(soap);
}

/*
 * Description:
 *   Get the ImagingConfiguration for the requested VideoSource.
 * SOAP action:
 *   http://www.onvif.org/ver20/imaging/wsdl/GetImagingSettings
 * Input:
 *   [GetImagingSettings]
 * Output:
 *   [GetImagingSettingsResponse]
 */
int OnvifImaging::getImagingSettings(std::string video_source_token) {

  //proxyImaging->soap_endpoint = imagingURL.c_str();

  _timg__GetImagingSettings *in = soap_new__timg__GetImagingSettings(soap, -1);
  _timg__GetImagingSettingsResponse *out = soap_new__timg__GetImagingSettingsResponse(soap, -1);

  in->VideoSourceToken = video_source_token;

  if (SOAP_OK == proxyImaging->GetImagingSettings(in, *out)) {
    imagingSettings = out->ImagingSettings;
  } else {
    printErr(proxyImaging, errorLog);
  }

  return 0;
}

/*
 *
 * Description:
 *   Set the ImagingConfiguration for the requested VideoSource.
 * SOAP action:
 *   http://www.onvif.org/ver20/imaging/wsdl/SetImagingSettings
 * Input:
 *   [SetImagingSettings]
 * Output:
 *   [SetImagingSettingsResponse]
 */
int OnvifImaging::setImagingSettings(std::string video_source_token) {

  //proxyImaging->soap_endpoint = imagingURL.c_str();

  _timg__SetImagingSettings *in = soap_new__timg__SetImagingSettings(soap, -1);
  _timg__SetImagingSettingsResponse *out = soap_new__timg__SetImagingSettingsResponse(soap, -1);

  in->VideoSourceToken = video_source_token;
//  in->ForcePersistence;
  in->ImagingSettings = imagingSettings;

  if (SOAP_OK != proxyImaging->SetImagingSettings(in, *out)) {
    printErr(proxyImaging, errorLog);
  }

  return 0;
}

/*
 * GetOptions
 * Description:
 *   This operation gets the valid ranges for the imaging parameters that have device specific ranges.
 *   This command is mandatory for all device implementing the imaging service.
 *   The command returns all supported parameters and their ranges such that these can be applied to the SetImagingSettings command.
 *   For read-only parameters which cannot be modified via the SetImagingSettings command only a single option or identical Min and Max values is provided.
 * SOAP action:
 *   http://www.onvif.org/ver20/imaging/wsdl/GetOptions
 * Input:
 *   [GetOptions]
 * Output:
 *   [GetOptionsResponse]
 */
int OnvifImaging::getOptions(std::string video_source_token, tt__ImagingOptions20 *imaging_options) {

  //proxyImaging->soap_endpoint = imagingURL.c_str();

  _timg__GetOptions *in = soap_new__timg__GetOptions(soap, -1);
  _timg__GetOptionsResponse *out = soap_new__timg__GetOptionsResponse(soap, -1);

  in->VideoSourceToken = video_source_token;

  if (SOAP_OK == proxyImaging->GetOptions(in, *out)) {
    imaging_options = out->ImagingOptions;
  } else {
    printErr(proxyImaging, errorLog);
  }

  return 0;
}
