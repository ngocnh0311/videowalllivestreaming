#ifndef IMAGING_H
#define IMAGING_H

#include <QString>
#include <string>

#include "soapImagingBindingProxy.h"

/*
 * https://www.onvif.org/specs/srv/img/ONVIF-Imaging-Service-Spec-v1706.pdf
 * https://www.onvif.org/ver20/imaging/wsdl/imaging.wsdl
 */

class OnvifImaging
{
private:
  std::string imagingURL;
  ImagingBindingProxy *proxyImaging;
  struct soap *soap;
  std::string proxyHost;
  int proxyPort;

public:
  std::string errorLog;

  tt__ImagingSettings20 *imagingSettings;

public:
  OnvifImaging(std::string url, std::string user_name, std::string pass_word, std::string proxy_host, int proxy_port);
  ~OnvifImaging();

  int getImagingSettings(std::string video_source_token);
  int setImagingSettings(std::string video_source_token);
  int getOptions(std::string video_source_token, tt__ImagingOptions20 *imaging_options);


  int stop();
};

#endif // IMAGING_H
