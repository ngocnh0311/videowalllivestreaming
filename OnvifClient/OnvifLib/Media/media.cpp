#include <QDebug>
#include <QObject>
#include <QString>

#include "media.h"
#include "wsseapi.h"

extern void processEventLog(char *fileName, uint lineNo, FILE *fp, const char *argList, ...);

inline void printErr(struct soap* soap, std::string &err_log)
{
  char buf[256];

  soap_sprint_fault(soap, buf, 256);

  err_log.append(buf);
}

OnvifMedia::OnvifMedia(std::string url, std::string user_name, std::string pass_word, std::string proxy_host, int proxy_port)
{
 // errorLog.clear();
  proxyMedia = new MediaBindingProxy;

  proxyMedia->connect_timeout = 3;
  proxyMedia->send_timeout = 3;
  proxyMedia->recv_timeout = 3;

  soap_register_plugin(proxyMedia, soap_wsse);
  soap = soap_new();

  videoEncoderConfiguration = NULL;
  mediaURL = "http://" + url + "/onvif/media_service";

  proxyMedia->soap_endpoint = mediaURL.c_str();
  this->userName = user_name;
  this->passWord = pass_word;

  if (proxy_host.size()>0) {
    proxyHost = proxy_host;
    proxyPort = proxy_port;
    proxyMedia->proxy_host = proxyHost.c_str();
    proxyMedia->proxy_port = proxyPort;
  }

  if (SOAP_OK != soap_wsse_add_UsernameTokenDigest(proxyMedia, NULL, user_name.c_str(), pass_word.c_str())) {
    printErr(proxyMedia, errorLog);
  }
}

OnvifMedia::~OnvifMedia()
{
  delete(proxyMedia);

  soap_destroy(soap);
  soap_end(soap);
  soap_free(soap);
}

/*
 * GetVideoSources
 * Description:
 *   This command lists all available physical video inputs of the device.
 * SOAP action:
 *   http://www.onvif.org/ver10/media/wsdlGetVideoSources/
 * Input:
 *   [GetVideoSources]
 * Output:
 *   [GetVideoSourcesResponse]
 */
int OnvifMedia::getVideoSources()
{
  errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__GetVideoSources *in = soap_new__trt__GetVideoSources(soap, -1);
  _trt__GetVideoSourcesResponse *out = soap_new__trt__GetVideoSourcesResponse(soap, -1);

  if(SOAP_OK == proxyMedia->GetVideoSources(in, *out)){
    videoSources = out->VideoSources;
  }else{
    printErr(proxyMedia, errorLog);
  }
}
/*
 * GetVideoSourceModes
 * Description:
 *   A device returns the information for current video source mode and settable video source modes of specified video source.
 *   A device that indicates a capability of VideoSourceModes shall support this command.
 * SOAP action:
 *   http://www.onvif.org/ver10/media/wsdl/GetVideoSourceModes
 * Input:
 *   [GetVideoSourceModes]
 * VideoSourceToken [ReferenceToken]
 *   Contains a video source reference for which a video source mode is requested.
 * Output:
 *   [GetVideoSourceModesResponse]
*/
int OnvifMedia::getVideoSourceModes(std::string &video_source_token)
{
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__GetVideoSourceModes *in = soap_new__trt__GetVideoSourceModes(soap, -1);
  _trt__GetVideoSourceModesResponse *out = soap_new__trt__GetVideoSourceModesResponse(soap, -1);

  in->VideoSourceToken = video_source_token;

  if(SOAP_OK == proxyMedia->GetVideoSourceModes(in, *out)){
    videoSourceModes = out->VideoSourceModes;
  }else{
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::getVideoSourceConfigurations()
{
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__GetVideoSourceConfigurations *trt__GetVideoSourceConfigurations = soap_new__trt__GetVideoSourceConfigurations(soap, -1);
  _trt__GetVideoSourceConfigurationsResponse *trt__GetVideoSourceConfigurationsResponse = soap_new__trt__GetVideoSourceConfigurationsResponse(soap, -1);

  if(SOAP_OK == proxyMedia->GetVideoSourceConfigurations(trt__GetVideoSourceConfigurations, *trt__GetVideoSourceConfigurationsResponse)){
    for(int i = 0; i<trt__GetVideoSourceConfigurationsResponse->Configurations.size(); ++i){
      std::cout << "Name:" << trt__GetVideoSourceConfigurationsResponse->Configurations[i]->Name << std::endl << "UseCount" <<trt__GetVideoSourceConfigurationsResponse->Configurations[i]->UseCount << std::endl << "Token:" << trt__GetVideoSourceConfigurationsResponse->Configurations[i]->token << std::endl <<"SourceToken:" << trt__GetVideoSourceConfigurationsResponse->Configurations[i]->SourceToken << std::endl << "X_bound: " << trt__GetVideoSourceConfigurationsResponse->Configurations[i]->Bounds->x << std::endl << "Y_bound: " << trt__GetVideoSourceConfigurationsResponse->Configurations[i]->Bounds->y << std::endl << "Width_bound::" << trt__GetVideoSourceConfigurationsResponse->Configurations[i]->Bounds->width << std::endl << "Height_bound" << trt__GetVideoSourceConfigurationsResponse->Configurations[i]->Bounds->height << std::endl;
      std::cout << std::endl;

    }
  }else{
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::getVideoEncoderConfigurations(){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__GetVideoEncoderConfigurations *trt__GetVideoEncoderConfigurations = soap_new__trt__GetVideoEncoderConfigurations(soap, -1);
  _trt__GetVideoEncoderConfigurationsResponse *trt__GetVideoEncoderConfigurationsResponse = soap_new__trt__GetVideoEncoderConfigurationsResponse(soap, -1);

  if(SOAP_OK == proxyMedia->GetVideoEncoderConfigurations(trt__GetVideoEncoderConfigurations, *trt__GetVideoEncoderConfigurationsResponse)){
    for(int i = 0; i<trt__GetVideoEncoderConfigurationsResponse->Configurations.size(); ++i){
      std::cout << "VideoEncoderToken #" << i << ": " << trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->token << std::endl;
      std::cout << "Name:" <<trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->Name << std::endl << "UseCount: " <<trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->UseCount << std::endl << "Encoding:" << trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->Encoding << std::endl << "Resolution:" <<trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->Resolution->Width << "x" << trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->Resolution->Height << std::endl << "Quality:" <<trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->Quality << std::endl << "FrameRateLimit:" << trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->RateControl->FrameRateLimit << std::endl << "EncodingInterval:" << trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->RateControl->EncodingInterval << std::endl << "BitrateLimit:" << trt__GetVideoEncoderConfigurationsResponse->Configurations[i]->RateControl->BitrateLimit << std::endl;
      std::cout << std::endl;
    }

  }else{
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::getVideoEncoderConfigurationOptions(std::string profileToken, std::string configurationToken){  
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__GetVideoEncoderConfigurationOptions *in = soap_new__trt__GetVideoEncoderConfigurationOptions(soap, -1);
  _trt__GetVideoEncoderConfigurationOptionsResponse *out = soap_new__trt__GetVideoEncoderConfigurationOptionsResponse(soap, -1);

  if(SOAP_OK == proxyMedia->GetVideoEncoderConfigurationOptions(in, *out)){
    videoEncoderConfigurationOptions = out->Options;

  }else{
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::getVideoEncoderConfiguration(std::string configuration_token){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__GetVideoEncoderConfiguration *in = soap_new__trt__GetVideoEncoderConfiguration(soap, -1);
  _trt__GetVideoEncoderConfigurationResponse *out = soap_new__trt__GetVideoEncoderConfigurationResponse(soap, -1);

  in->ConfigurationToken = configuration_token;
  if(SOAP_OK == proxyMedia->GetVideoEncoderConfiguration(in, *out)){
    videoEncoderConfiguration = out->Configuration;
  }else{
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::setVideoEncoderConfiguration(){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__SetVideoEncoderConfiguration *in = soap_new__trt__SetVideoEncoderConfiguration(soap, -1);
  _trt__SetVideoEncoderConfigurationResponse *out = soap_new__trt__SetVideoEncoderConfigurationResponse(soap, -1);
  in->Configuration = videoEncoderConfiguration;

  if(SOAP_OK != proxyMedia->SetVideoEncoderConfiguration(in, *out)){
    std::cout << "ERROR:" <<std::endl;
    printErr(proxyMedia, errorLog);
  }

}

int OnvifMedia::getStreamURI(std::string profileToken, std::string &stream_uri){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__GetStreamUri *trt__GetStreamUri = soap_new__trt__GetStreamUri(soap, -1);
  trt__GetStreamUri->StreamSetup = soap_new_tt__StreamSetup(soap, -1);
  trt__GetStreamUri->StreamSetup->Stream = tt__StreamType__RTP_Unicast;
  trt__GetStreamUri->StreamSetup->Transport = soap_new_tt__Transport(soap, -1);
  trt__GetStreamUri->StreamSetup->Transport->Protocol = tt__TransportProtocol__RTSP;
  trt__GetStreamUri->ProfileToken = profileToken;

  _trt__GetStreamUriResponse *trt__GetStreamUriResponse = soap_new__trt__GetStreamUriResponse(soap, -1);

  if(SOAP_OK == proxyMedia->GetStreamUri(trt__GetStreamUri, *trt__GetStreamUriResponse)){
    stream_uri = trt__GetStreamUriResponse->MediaUri->Uri;

  }else{
    printErr(proxyMedia, errorLog);
  }

}

int OnvifMedia::getProfile(std::string profileToken){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__GetProfile *trt__GetProfile = soap_new__trt__GetProfile(soap, -1);
  _trt__GetProfileResponse *trt__GetProfileResponse = soap_new__trt__GetProfileResponse(soap, -1);

  trt__GetProfile->ProfileToken = profileToken;

  if(SOAP_OK == proxyMedia->GetProfile(trt__GetProfile, *trt__GetProfileResponse)){
    std::cout << "Video Source token: " << trt__GetProfileResponse->Profile->VideoSourceConfiguration->token << std::endl;
    std::cout << "Video Encoder token: " << trt__GetProfileResponse->Profile->VideoEncoderConfiguration->token << std::endl;

  }
  else{
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::createProfile(std::string profileName, std::string profileToken){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__CreateProfile *trt__CreateProfile = soap_new__trt__CreateProfile(soap, -1);
  _trt__CreateProfileResponse *trt__CreateProfileResponse = soap_new__trt__CreateProfileResponse(soap, -1);

  trt__CreateProfile->Name = profileName;
  trt__CreateProfile->Token = &profileToken;

  if(SOAP_OK == proxyMedia->CreateProfile(trt__CreateProfile, *trt__CreateProfileResponse)){
  }else{
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::deleteProfile(std::string profileToken){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__DeleteProfile *trt__DeleteProfile = soap_new__trt__DeleteProfile(soap, -1);
  _trt__DeleteProfileResponse *trt__DeleteProfileResponse = soap_new__trt__DeleteProfileResponse(soap, -1);

  trt__DeleteProfile->ProfileToken = profileToken;

  if(SOAP_OK == proxyMedia->DeleteProfile(trt__DeleteProfile, *trt__DeleteProfileResponse)){

  } else {
    printErr(proxyMedia, errorLog);
  }
}

int  OnvifMedia::addPTZConfiguration(std::string profileToken, std::string configurationToken){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__AddPTZConfiguration *trt__AddPTZConfiguration = soap_new__trt__AddPTZConfiguration(soap, -1);
  _trt__AddPTZConfigurationResponse *trt__AddPTZConfigurationResponse = soap_new__trt__AddPTZConfigurationResponse(soap, -1);

  trt__AddPTZConfiguration->ProfileToken =profileToken;
  trt__AddPTZConfiguration->ConfigurationToken = configurationToken;

  if(SOAP_OK == proxyMedia->AddPTZConfiguration(trt__AddPTZConfiguration, *trt__AddPTZConfigurationResponse)){

  }else{
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::addVideoSourceConfiguration(std::string profileToken, std::string configurationToken){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__AddVideoSourceConfiguration *trt__AddVideoSourceConfiguration = soap_new__trt__AddVideoSourceConfiguration(soap, -1);
  _trt__AddVideoSourceConfigurationResponse *trt__AddVideoSourceConfigurationResponse = soap_new__trt__AddVideoSourceConfigurationResponse(soap, -1);

  trt__AddVideoSourceConfiguration->ProfileToken =profileToken;
  trt__AddVideoSourceConfiguration->ConfigurationToken = configurationToken;

  if(SOAP_OK == proxyMedia->AddVideoSourceConfiguration(trt__AddVideoSourceConfiguration, *trt__AddVideoSourceConfigurationResponse)){

  }else{
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::addVideoEncoderConfiguration(std::string profileToken, std::string configurationToken){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__AddVideoEncoderConfiguration *trt__AddVideoEncoderConfiguration = soap_new__trt__AddVideoEncoderConfiguration(soap, -1);
  _trt__AddVideoEncoderConfigurationResponse *trt__AddVideoEncoderConfigurationResponse = soap_new__trt__AddVideoEncoderConfigurationResponse(soap, -1);

  trt__AddVideoEncoderConfiguration->ProfileToken =profileToken;
  trt__AddVideoEncoderConfiguration->ConfigurationToken = configurationToken;

  if(SOAP_OK == proxyMedia->AddVideoEncoderConfiguration(trt__AddVideoEncoderConfiguration, *trt__AddVideoEncoderConfigurationResponse)){

  } else {
    printErr(proxyMedia, errorLog);
  }
}

int OnvifMedia::getProfiles(){
 // errorLog.clear();
  //proxyMedia->soap_endpoint = mediaURL.c_str();

  _trt__GetProfiles *trt__GetProfiles = soap_new__trt__GetProfiles(soap, -1);
  _trt__GetProfilesResponse *trt__GetProfilesResponse = soap_new__trt__GetProfilesResponse(soap, -1);

  if (SOAP_OK == proxyMedia->GetProfiles(trt__GetProfiles, *trt__GetProfilesResponse)) {
    Profiles = trt__GetProfilesResponse->Profiles;
  } else {
    printErr(proxyMedia, errorLog);
  }
}
