#ifndef MEDIA_H
#define MEDIA_H

#include <QString>
#include <string>

#include "soapMediaBindingProxy.h"

/*
 * https://www.onvif.org/specs/srv/media/ONVIF-Media-Service-Spec-v1706.pdf
 * https://www.onvif.org/ver10/media/wsdl/media.wsdl
 */

class OnvifMedia
{
private:
  std::string mediaURL;
  MediaBindingProxy *proxyMedia;
  struct soap *soap;
  std::string proxyHost;
  int proxyPort;

public:
  std::string userName;
  std::string passWord;

  std::string errorLog;

  std::vector<std::string> rtspUrlList;

  std::vector<tt__Profile *> Profiles;
  std::vector<tt__VideoSource *> videoSources;
  std::vector<trt__VideoSourceMode *> videoSourceModes;
  tt__VideoEncoderConfiguration *videoEncoderConfiguration;
  tt__VideoEncoderConfigurationOptions *videoEncoderConfigurationOptions;


  OnvifMedia(std::string url, std::string user_name, std::string pass_word, std::string proxy_host, int proxy_port);
  ~OnvifMedia();

  int getVideoSources();
  int getVideoSourceConfigurations();
  int getVideoEncoderConfigurations();
  int getVideoEncoderConfigurationOptions(std::string profileToken, std::string configurationToken);
  int setVideoEncoderConfiguration(std::string configurationToken);
  int getStreamURI(std::string profileToken, std::string &stream_uri);
  int getProfile(std::string profileToken);
  int createProfile(std::string profileName, std::string profileToken);
  int deleteProfile(std::string profileToken);
  int addPTZConfiguration(std::string profileToken, std::string configurationToken);
  int addVideoSourceConfiguration(std::string profileToken, std::string configurationToken);
  int addVideoEncoderConfiguration(std::string profileToken, std::string configurationToken);
  int getProfiles();

  int getVideoSourceModes(std::string &video_source_token);
  int getVideoEncoderConfiguration(std::string configuration_token);
  int setVideoEncoderConfiguration();





};

#endif // MEDIA_H
