#include "wsseapi.h"
#include "wsdd.nsmap"
#include "soapwsddProxy.h"
#include "wsaapi.h"
#include "soapH.h"

#include "device.h"
#include "ErrorLog.h"

inline void printErr(struct soap* soap, std::string &err_log)
{
  char buf[256];

  soap_sprint_fault(soap, buf, 256);

  err_log.append(buf);
}

OnvifDevice::OnvifDevice(std::string url, std::string user_name, std::string pass_word, std::string proxy_host, int proxy_port)
{
  errorLog.clear();
  // Init object
  this->hasImaging = false;
  this->hasMedia = false;
  this->hasPTZ = false;

  dateTime = NULL;
  ntpInfo = NULL;
  proxyDevice = new DeviceBindingProxy;

  proxyDevice->connect_timeout = 3;
  proxyDevice->send_timeout = 3;
  proxyDevice->recv_timeout = 3;

  soap_register_plugin(proxyDevice, soap_wsse);
  soap = soap_new();

  dateTime = NULL;
  ntpInfo = NULL;

  userName = user_name;
  passWord = pass_word;

  devURL = "http://" + url + "/onvif/device_service";
  proxyDevice->soap_endpoint = devURL.c_str();

  if (proxy_host.size()>0) {
    proxyHost = proxy_host;
    proxyPort = proxy_port;
    proxyDevice->proxy_host = proxyHost.c_str();
    proxyDevice->proxy_port = proxyPort;
  }

  if (SOAP_OK != soap_wsse_add_UsernameTokenDigest(proxyDevice, NULL, user_name.c_str(), pass_word.c_str())) {
    printErr(proxyDevice, errorLog);
    return;
  }

//  getCapabilities();
//  if (errorLog.size() > 0)
//    return;
}

OnvifDevice::~OnvifDevice()
{
  delete(proxyDevice);

  soap_destroy(soap);
  soap_end(soap);
  soap_free(soap);
}


/*
 * Description:
 *   This operation gets basic device information from the device.
 * SOAP action:
 *   http://www.onvif.org/ver10/device/wsdl/GetDeviceInformation
 * Input:
 *   [GetDeviceInformation]
 * Output:
 *   [GetDeviceInformationResponse]
 */
int OnvifDevice::getDeviceInformation(std::string &dev_info)
{
  int ret = -1;
  //struct soap *soap = soap_new();
  //proxyDevice->soap_endpoint = devURL.c_str();
  //proxyDevice->proxy_host = proxyHost.c_str();
  //proxyDevice->proxy_port = proxyPort;

  _tds__GetDeviceInformation *in = soap_new__tds__GetDeviceInformation(soap, -1);
  _tds__GetDeviceInformationResponse *out = soap_new__tds__GetDeviceInformationResponse(soap, -1);

  errorLog.clear();

  if (SOAP_OK != proxyDevice->GetDeviceInformation(in, *out)) {
    printErr(proxyDevice, errorLog);
  } else {
    dev_info = "Manufacturer: "+out->Manufacturer+"\n"+
               "Model: "+out->Model+"\n"+
               "Firmware Version: "+out->FirmwareVersion+"\n"+
               "Hardware ID: "+out->HardwareId+"\n"+
               "Serial Number: "+out->SerialNumber;
    deviceInfo.Manufacturer = out->Manufacturer;
    deviceInfo.Model = out->Model;
    deviceInfo.FirmwareVersion = out->FirmwareVersion;
    deviceInfo.HardwareId = out->HardwareId;
    deviceInfo.SerialNumber = out->SerialNumber;
    ret = 0;
  }

  //soap_destroy(soap);
  //soap_end(soap);
  //soap_free(soap);

  return ret;
}

/*
 * Description:
 *   Any endpoint can ask for the capabilities of a device using the capability exchange request response operation. The device shall indicate all its ONVIF compliant capabilities through the GetCapabilities command. The capability list includes references to the addresses (XAddr) of the service implementing the interface operations in the category. Apart from the addresses, the capabilities only reflect optional functions.
 * SOAP action:
 *   http://www.onvif.org/ver10/device/wsdl/GetCapabilities
 * Input:
 *  [GetCapabilities]
 * Output:
 *  [GetCapabilitiesResponse]
 */
void OnvifDevice::getCapabilities() {
  errorLog.clear();

  //proxyDevice->soap_endpoint = devURL.c_str();
  //proxyDevice->proxy_host = proxyHost.c_str();
  //proxyDevice->proxy_port = proxyPort;

  _tds__GetCapabilities *in = soap_new__tds__GetCapabilities(soap, -1);
  _tds__GetCapabilitiesResponse *out = soap_new__tds__GetCapabilitiesResponse(soap, -1);

  in->Category.push_back(tt__CapabilityCategory__All);

  if (SOAP_OK == proxyDevice->GetCapabilities(in, *out)) {
    if (out->Capabilities->Imaging != NULL) {
//      onvifImaging = new OnvifImaging(out->Capabilities->Imaging->XAddr,
//                                      userName, passWord, proxyHost, proxyPort);
      this->hasImaging = true;
    }
    if (out->Capabilities->Media != NULL){
//      onvifMedia = new OnvifMedia(out->Capabilities->Media->XAddr,
//                                  userName, passWord, proxyHost, proxyPort);
      this->hasMedia = true;
    }
    if (out->Capabilities->PTZ != NULL){
//      onvifPTZ = new OnvifPTZ(out->Capabilities->PTZ->XAddr,
//                              userName, passWord, proxyHost, proxyPort);
      this->hasPTZ = true;
    }
  } else {
      printErr(proxyDevice, errorLog);
  }

  return ;
}


/*
 * Description:
 *   This operation gets the device system date and time. The device shall support the return of the daylight saving setting and of the manual system date and time (if applicable) or indication of NTP time (if applicable) through the GetSystemDateAndTime command.
 *   A device shall provide the UTCDateTime information.
 * SOAP action:
 *   http://www.onvif.org/ver10/device/wsdl/GetSystemDateAndTime
 * Input:
 *   [GetSystemDateAndTime]
 * Output:
 *   [GetSystemDateAndTimeResponse]
 */
void OnvifDevice::getSystemDateAndTime()
{
  errorLog.clear();
  //struct soap *soap = soap_new();

  //proxyDevice->soap_endpoint = devURL.c_str();
  //proxyDevice->proxy_host = proxyHost.c_str();
  //proxyDevice->proxy_port = proxyPort;


  _tds__GetSystemDateAndTime *in = soap_new__tds__GetSystemDateAndTime(soap, -1);
  _tds__GetSystemDateAndTimeResponse *out = soap_new__tds__GetSystemDateAndTimeResponse(soap, -1);

  if (SOAP_OK == proxyDevice->GetSystemDateAndTime(in, *out)) {
    dateTime = out->SystemDateAndTime;

    if (dateTime->UTCDateTime != NULL) {
      tt__DateTime *utc_datetime = dateTime->UTCDateTime;

      processEventLog(__FILE__, __LINE__, stdout,
                      "UTCDateTime: %s %02d:%02d:%02d %02d-%02d-%04d",
                      dateTime->TimeZone->TZ.c_str(),
                      utc_datetime->Time->Hour,
                      utc_datetime->Time->Minute,
                      utc_datetime->Time->Second,
                      utc_datetime->Date->Day,
                      utc_datetime->Date->Month,
                      utc_datetime->Date->Year);
    }

    if (dateTime->LocalDateTime) {
      tt__DateTime *local_date_time = dateTime->LocalDateTime;

      processEventLog(__FILE__, __LINE__, stdout,
                      "LocalDateTime: %s %02d:%02d:%02d %02d-%02d-%04d",
                      dateTime->TimeZone->TZ.c_str(),
                      local_date_time->Time->Hour,
                      local_date_time->Time->Minute,
                      local_date_time->Time->Second,
                      local_date_time->Date->Day,
                      local_date_time->Date->Month,
                      local_date_time->Date->Year);

    } else {
      dateTime->LocalDateTime = soap_new_tt__DateTime(soap, -1);
      dateTime->LocalDateTime->Date = soap_new_tt__Date(soap, -1);
      dateTime->LocalDateTime->Time = soap_new_tt__Time(soap, -1);
    }
  } else {
      printErr(proxyDevice, errorLog);
  }
}

/*
 * Description:
 *   This operation sets the device system date and time. The device shall support the configuration of the daylight saving setting and of the manual system date and time (if applicable) or indication of NTP time (if applicable) through the SetSystemDateAndTime command.
 *   If system time and date are set manually, the client shall include UTCDateTime in the request.
 *   A TimeZone token which is not formed according to the rules of IEEE 1003.1 section 8.3 is considered as invalid timezone.
 *   The DayLightSavings flag should be set to true to activate any DST settings of the TimeZone string. Clear the DayLightSavings flag if the DST portion of the TimeZone settings should be ignored.
 * SOAP action:
 *   http://www.onvif.org/ver10/device/wsdl/SetSystemDateAndTime
 * Input:
 *   [SetSystemDateAndTime]
 * Output:
 *   [SetSystemDateAndTimeResponse]
 */
void OnvifDevice::setSystemDateAndTime()
{
  errorLog.clear();

  //proxyDevice->soap_endpoint = devURL.c_str();
  //proxyDevice->proxy_host = proxyHost.c_str();
  //proxyDevice->proxy_port = proxyPort;

  _tds__SetSystemDateAndTime *tds__SetSystemDateAndTime = soap_new__tds__SetSystemDateAndTime(soap, -1);
  _tds__SetSystemDateAndTimeResponse *tds__SetSystemDateAndTimeResponse = soap_new__tds__SetSystemDateAndTimeResponse(soap, -1);

  tds__SetSystemDateAndTime->DateTimeType    = dateTime->DateTimeType;
  tds__SetSystemDateAndTime->DaylightSavings = dateTime->DaylightSavings;
  tds__SetSystemDateAndTime->TimeZone        = dateTime->TimeZone;
  tds__SetSystemDateAndTime->UTCDateTime     = dateTime->UTCDateTime;

  if (SOAP_OK != proxyDevice->SetSystemDateAndTime(tds__SetSystemDateAndTime, *tds__SetSystemDateAndTimeResponse)) {
    printErr(proxyDevice, errorLog);
  }
}

/*
 * Description:
 *   This operation gets the NTP settings from a device.
 *   If the device supports NTP, it shall be possible to get the NTP server settings through the GetNTP command.
 * SOAP action:
 *   http://www.onvif.org/ver10/device/wsdl/GetNTP
 * Input:
 *   [GetNTP]
 * Output:
 *   [GetNTPResponse]
 */
void OnvifDevice::getNTP()
{
  errorLog.clear();
  //proxyDevice->soap_endpoint = devURL.c_str();
  //proxyDevice->proxy_host = proxyHost.c_str();
  //proxyDevice->proxy_port = proxyPort;

  _tds__GetNTP *tds__GetNTP = soap_new__tds__GetNTP(soap, -1);
  _tds__GetNTPResponse *tds__GetNTPResponse = soap_new__tds__GetNTPResponse(soap, -1);

  if (SOAP_OK == proxyDevice->GetNTP(tds__GetNTP, *tds__GetNTPResponse)) {
    if (tds__GetNTPResponse->NTPInformation != NULL) {
      ntpInfo = tds__GetNTPResponse->NTPInformation;
    } else {
      processEventLog(__FILE__, __LINE__, stdout, "NTPInformation is NULL");
    }
  } else {
    printErr(proxyDevice, errorLog);
  }
}

/*
 * Description:
 *   This operation sets the NTP settings on a device. If the device supports NTP, it shall be possible to set the NTP server settings through the SetNTP command.
 *   A device shall accept string formated according to RFC 1123 section 2.1 or alternatively to RFC 952, other string shall be considered as invalid strings.
 *   Changes to the NTP server list will not affect the clock mode DateTimeType. Use SetSystemDateAndTime to activate NTP operation.
 * SOAP action:
 *   http://www.onvif.org/ver10/device/wsdl/SetNTP
 * Input:
 *   [SetNTP]
 * Output:
 *   [SetNTPResponse]
 */
void OnvifDevice::setNTP()
{
  errorLog.clear();
  //struct soap *soap = soap_new();

  //proxyDevice->soap_endpoint = devURL.c_str();
  //proxyDevice->proxy_host = proxyHost.c_str();
  //proxyDevice->proxy_port = proxyPort;

    _tds__SetNTP *tds__SetNTP = soap_new__tds__SetNTP(soap, -1);
    _tds__SetNTPResponse *tds__SetNTPResponse = soap_new__tds__SetNTPResponse(soap, -1);

    tds__SetNTP->FromDHCP = false;

    tt__NetworkHost * network_host= soap_new_tt__NetworkHost(soap, -1);

    tds__SetNTP->NTPManual.push_back(network_host);

    if (SOAP_OK != proxyDevice->SetNTP(tds__SetNTP, *tds__SetNTPResponse)) {
      printErr(proxyDevice, errorLog);
    }

  //soap_destroy(soap);
  //soap_end(soap);
  //soap_free(soap);
}

/*
 * Description:
 *   This operation lists the registered users and corresponding credentials on a device.
 *   The device shall support retrieval of registered device users and their credentials for the user token through the GetUsers command.
 * SOAP action:
 *   http://www.onvif.org/ver10/device/wsdl/GetUsers
 * Input:
 *   [GetUsers]
 * Output:
 *   [GetUsersResponse]
 */
void OnvifDevice::getUsers() {
  errorLog.clear();

  _tds__GetUsers *tds__GetUsers = soap_new__tds__GetUsers(soap, -1);
  _tds__GetUsersResponse *tds__GetUsersResponse = soap_new__tds__GetUsersResponse(soap, -1);

  if (SOAP_OK == proxyDevice->GetUsers(tds__GetUsers, *tds__GetUsersResponse)) {

  } else {
      printErr(proxyDevice, errorLog);
  }
  return ;
}

/*
 * GetRemoteUser
 * Description:
 *   This operation returns the configured remote user (if any).
 *   A device supporting remote user handling shall support this operation. The user is only valid for the WS-UserToken profile or as a HTTP / RTSP user.
 *   The algorithm to use for deriving the password is described in section 5.12.2.1 of the core specification.
 * SOAP action:
 *   http://www.onvif.org/ver10/device/wsdl/GetRemoteUser
 * Input:
 *   [GetRemoteUser]
 * Output:
 *   [GetRemoteUserResponse]
 */
void OnvifDevice::getRemoteUser() {
  errorLog.clear();

  _tds__GetRemoteUser *in = soap_new__tds__GetRemoteUser(soap, -1);
  _tds__GetRemoteUserResponse *out = soap_new__tds__GetRemoteUserResponse(soap, -1);

  if (SOAP_OK == proxyDevice->GetRemoteUser(in, *out)) {

  } else {
      printErr(proxyDevice, errorLog);
  }
  return ;
}

void OnvifDevice::getServiceCapabilities() {
  errorLog.clear();

  _tds__GetServiceCapabilities *in = soap_new__tds__GetServiceCapabilities(soap, -1);
  _tds__GetServiceCapabilitiesResponse *out = soap_new__tds__GetServiceCapabilitiesResponse(soap, -1);

  if (SOAP_OK == proxyDevice->GetServiceCapabilities(in, *out)) {

  } else {
      printErr(proxyDevice, errorLog);
  }
  return ;
}


void OnvifDevice::getRemoteDiscoveryMode() {
  errorLog.clear();

  _tds__GetRemoteDiscoveryMode *in = soap_new__tds__GetRemoteDiscoveryMode(soap, -1);
  _tds__GetRemoteDiscoveryModeResponse *out = soap_new__tds__GetRemoteDiscoveryModeResponse(soap, -1);
  out->RemoteDiscoveryMode = tt__DiscoveryMode__NonDiscoverable;

  if (SOAP_OK == proxyDevice->GetRemoteDiscoveryMode(in, *out)) {
    if (out->RemoteDiscoveryMode == tt__DiscoveryMode__NonDiscoverable) {
      processEventLog(__FILE__, __LINE__, stdout,"NonDiscoverable\n");
    } else {
      processEventLog(__FILE__, __LINE__, stdout,"Discoverable");
    }

  } else {
      printErr(proxyDevice, errorLog);
  }
  return ;
}

void OnvifDevice::setRemoteDiscoveryMode() {
  errorLog.clear();

  _tds__SetRemoteDiscoveryMode *in = soap_new__tds__SetRemoteDiscoveryMode(soap, -1);
  _tds__SetRemoteDiscoveryModeResponse *out = soap_new__tds__SetRemoteDiscoveryModeResponse(soap, -1);

  in->RemoteDiscoveryMode = tt__DiscoveryMode__NonDiscoverable;

  if (SOAP_OK == proxyDevice->SetRemoteDiscoveryMode(in, *out)) {

  } else {
      printErr(proxyDevice, errorLog);
  }
  return ;
}



