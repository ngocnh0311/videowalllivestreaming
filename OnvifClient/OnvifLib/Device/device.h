#ifndef DEVICE_H
#define DEVICE_H

#include <string>

#include "soapDeviceBindingProxy.h"

/*
 * https://www.onvif.org/specs/core/ONVIF-Core-Specification-v1712.pdf
 * https://www.onvif.org/ver10/device/wsdl/devicemgmt.wsdl
 */

typedef struct {
  std::string Manufacturer;
  std::string Model;
  std::string FirmwareVersion;
  std::string SerialNumber;
  std::string HardwareId;
} OnvifDeviceInfo;

class OnvifDevice
{
private:

  std::string devURL;
  std::string proxyHost;
  int proxyPort;

  DeviceBindingProxy *proxyDevice;

  struct soap *soap;

public:
  bool hasImaging;
  bool hasMedia;
  bool hasPTZ;

  std::string userName;
  std::string passWord;

  std::string errorLog;

  tt__SystemDateTime *dateTime;
  tt__NTPInformation *ntpInfo;

  OnvifDeviceInfo deviceInfo;

public:
  int getDeviceInformation(std::string &dev_info);
  void getCapabilities();
  void getUsers();

  void getNTP();
  void setNTP();

  void getSystemDateAndTime();
  void setSystemDateAndTime();

  void getDiscoveryMode();
  void getRemoteDiscoveryMode();
  void setDiscoveryMode();
  void setRemoteDiscoveryMode();
  void setSystemFactoryDefault();

  void systemReboot();
  void getRemoteUser();
  void getServiceCapabilities();



public:
  OnvifDevice(std::string url, std::string user_name, std::string pass_word, std::string proxy_host, int proxy_port);
  ~OnvifDevice();

  void getStreamList();

};

#endif // DEVICE_H
