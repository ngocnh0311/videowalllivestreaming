#ifndef PTZ_H
#define PTZ_H

#include <QString>

#include "soapPTZBindingProxy.h"

/*
 * https://www.onvif.org/specs/srv/ptz/ONVIF-PTZ-Service-Spec-v1712.pdf
 * https://www.onvif.org/ver20/ptz/wsdl/ptz.wsdl
 */

class OnvifPTZ
{
private:
  std::string ptzURL;
  PTZBindingProxy *proxyPTZ;
  struct soap *soap;
  std::string proxyHost;
  int proxyPort;

public:
  std::string errorLog;

public:
  OnvifPTZ(std::string url, std::string user_name, std::string pass_word, std::string proxy_host, int proxy_port);
  ~OnvifPTZ();


  int getPTZConfigurations();
  int getStatus(std::string profileToken);
  int getCompatibleConfigurations(std::string profile_token);
  int getConfigurationOptions(std::string configurationToken);

  int absoluteMove(std::string profileToken, float pan, float panSpeed, float tilt, float tiltSpeed, float zoom, float zoomSpeed);
  int relativeMove(std::string profileToken, float pan, float panSpeed, float tilt, float tiltSpeed, float zoom, float zoomSpeed);
  int continuousMove(std::string profileToken, float panSpeed, float tiltSpeed, float zoomSpeed);
  int stop(std::string profileToken, bool panTilt, bool zoom);
  int setHomePosition(std::string profileToken);
  int goToHomePosition(std::string profileToken);
  int getConfiguration(std::string configurationToken);
  int panLeft(std::string profileToken, int nDegrees);
  int panRight(std::string profileToken, int nDegrees);
  int tiltDown(std::string profileToken, int nDegrees);
  int tiltUp(std::string profileToken, int nDegrees);
  int zoomIn(std::string profileToken, float zoom);
  int zoomOut(std::string profileToken, float zoom);
};

#endif // PTZ_H
