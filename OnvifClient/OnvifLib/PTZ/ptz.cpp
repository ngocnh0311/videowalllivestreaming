#include <QDebug>
#include <QObject>
#include <QString>

#include "ptz.h"
#include "wsseapi.h"


inline void printErr(struct soap* soap, std::string &err_log)
{
  char buf[256];

  soap_sprint_fault(soap, buf, 256);

  err_log.append(buf);
}


OnvifPTZ::OnvifPTZ(std::string url, std::string user_name, std::string pass_word, std::string proxy_host, int proxy_port)
{
  errorLog.clear();
  ptzURL = url;
  proxyPTZ = new PTZBindingProxy;

  proxyPTZ->connect_timeout = 3;
  proxyPTZ->send_timeout = 3;
  proxyPTZ->recv_timeout = 3;

  soap_register_plugin(proxyPTZ, soap_wsse);
  soap = soap_new();

  proxyPTZ->soap_endpoint = ptzURL.c_str();
  if (proxy_host.size()>0) {
    proxyHost = proxy_host;
    proxyPort = proxy_port;
    proxyPTZ->proxy_host = proxyHost.c_str();
    proxyPTZ->proxy_port = proxyPort;
  }

  if (SOAP_OK != soap_wsse_add_UsernameTokenDigest(proxyPTZ, NULL, user_name.c_str(), pass_word.c_str())) {
    printErr(proxyPTZ, errorLog);
  }
}

OnvifPTZ::~OnvifPTZ()
{
  delete(proxyPTZ);

  soap_destroy(soap);
  soap_end(soap);
  soap_free(soap);
}

/*
 * AbsoluteMove
 * Description:
 * Operation to move pan,tilt or zoom to a absolute destination.
 *   The speed argument is optional.
 *   If an x/y speed value is given it is up to the device to either use the x value as absolute resoluting speed vector or to map x and y to the component speed.
 *   If the speed argument is omitted, the default speed set by the PTZConfiguration will be used.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/AbsoluteMove
 * Input:
 *   [AbsoluteMove]
 * Output:
 *   [AbsoluteMoveResponse]
 */
int OnvifPTZ::absoluteMove(std::string profileToken, float pan, float panSpeed, float tilt, float tiltSpeed, float zoom, float zoomSpeed)
{

  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__AbsoluteMove *tptz__AbsoluteMove = soap_new__tptz__AbsoluteMove(soap, -1);
  _tptz__AbsoluteMoveResponse *tptz__AbsoluteMoveResponse = soap_new__tptz__AbsoluteMoveResponse(soap, -1);

  tptz__AbsoluteMove->ProfileToken = profileToken;

  //setting pan and tilt
  tptz__AbsoluteMove->Position = soap_new_tt__PTZVector(soap, -1);
  tptz__AbsoluteMove->Position->PanTilt = soap_new_tt__Vector2D(soap, -1);
  tptz__AbsoluteMove->Speed = soap_new_tt__PTZSpeed(soap, -1);
  tptz__AbsoluteMove->Speed->PanTilt = soap_new_tt__Vector2D(soap, -1);
  //pan
  tptz__AbsoluteMove->Position->PanTilt->x = pan;
  tptz__AbsoluteMove->Speed->PanTilt->x = panSpeed;
  //tilt
  tptz__AbsoluteMove->Position->PanTilt->y = tilt;
  tptz__AbsoluteMove->Speed->PanTilt->y = tiltSpeed;
  //setting zoom
  tptz__AbsoluteMove->Position->Zoom = soap_new_tt__Vector1D(soap, -1);
  tptz__AbsoluteMove->Speed->Zoom = soap_new_tt__Vector1D(soap, -1);
  tptz__AbsoluteMove->Position->Zoom->x = zoom;
  tptz__AbsoluteMove->Speed->Zoom->x = zoomSpeed;

  if(SOAP_OK == proxyPTZ->AbsoluteMove(tptz__AbsoluteMove, *tptz__AbsoluteMoveResponse)){

  }else{
    std::cout << "AbsoluteMove ERROR:" << std::endl;
    printErr(proxyPTZ, errorLog);
    std::cout << std::endl;
  }
}

/*
 * ContinuousMove
 * Description:
 *   Operation for continuous Pan/Tilt and Zoom movements.
 *   The operation is supported if the PTZNode supports at least one continuous Pan/Tilt or Zoom space.
 *   If the space argument is omitted, the default space set by the PTZConfiguration will be used.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/ContinuousMove
 * Input:
 *   [ContinuousMove]
 * Output:
 *   [ContinuousMoveResponse]
 */
int OnvifPTZ::continuousMove(std::string profileToken, float panSpeed, float tiltSpeed, float zoomSpeed){

  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__ContinuousMove *tptz__ContinuousMove = soap_new__tptz__ContinuousMove(soap, -1);
  _tptz__ContinuousMoveResponse *tptz__ContinuousMoveResponse = soap_new__tptz__ContinuousMoveResponse(soap, -1);

  tptz__ContinuousMove->ProfileToken = profileToken;

  //setting pan and tilt speed
  tptz__ContinuousMove->Velocity = soap_new_tt__PTZSpeed(soap, -1);
  tptz__ContinuousMove->Velocity->PanTilt = soap_new_tt__Vector2D(soap, -1);
  tptz__ContinuousMove->Velocity->PanTilt->x = panSpeed;
  tptz__ContinuousMove->Velocity->PanTilt->y = tiltSpeed;

  //setting zoom speed
  tptz__ContinuousMove->Velocity->Zoom = soap_new_tt__Vector1D(soap, -1);
  tptz__ContinuousMove->Velocity->Zoom->x = zoomSpeed;

  LONG64 tempo = 1;
  tptz__ContinuousMove->Timeout = &tempo;

  if(SOAP_OK == proxyPTZ->ContinuousMove(tptz__ContinuousMove, *tptz__ContinuousMoveResponse)){

  }else{
    std::cout << "ContinuousMove ERROR:" << std::endl;
    printErr(proxyPTZ, errorLog);
    std::cout;
  }
}
/*
 * CreatePresetTour
 * Description:
 *   Operation to create a preset tour for the selected media profile.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/CreatePresetTour
 * Input:
 *   [CreatePresetTour]
 * Output:
 *   [CreatePresetTourResponse]
 */

/*
 * GeoMove
 * Description:
 *   Operation to move pan,tilt or zoom to point to a destination based on the geolocation of the target.
 *   The speed argument is optional.
 *   If an x/y speed value is given it is up to the device to either use the x value as absolute resoluting speed vector or to map x and y to the component speed.
 *   If the speed argument is omitted, the default speed set by the PTZConfiguration will be used.
 *   The area height and area dwidth parameters are optional, they can be used independently and may be used by the device to automatically determine the best zoom level to show the target.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GeoMove
 * Input:
 *   [GeoMove]
 * Output:
 *   [GeoMoveResponse]
 */

/*
 * GetCompatibleConfigurations
 * Description:
 *   Operation to get all available PTZConfigurations that can be added to the referenced media profile.
 *   A device providing more than one PTZConfiguration or more than one VideoSourceConfiguration or which has any other resource interdependency between PTZConfiguration entities and other resources listable in a media profile should implement this operation.
 *   PTZConfiguration entities returned by this operation shall not fail on adding them to the referenced media profile.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetCompatibleConfigurations
 * Input:
 *   [GetCompatibleConfigurations]
 * Output:
 *   [GetCompatibleConfigurationsResponse]
 */
int OnvifPTZ::getCompatibleConfigurations(std::string profile_token){
  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__GetCompatibleConfigurations *in = soap_new__tptz__GetCompatibleConfigurations(soap, -1);
  _tptz__GetCompatibleConfigurationsResponse *out = soap_new__tptz__GetCompatibleConfigurationsResponse(soap, -1);
  in->ProfileToken = profile_token;

  if(SOAP_OK == proxyPTZ->GetCompatibleConfigurations(in, *out)){

  }else{
    printErr(proxyPTZ, errorLog);
  }
}

/*
 * GetConfiguration
 * Description:
 *   Get a specific PTZconfiguration from the device, identified by its reference token or name.
 *   The default Position/Translation/Velocity Spaces are introduced to allow NVCs sending move requests without the need to specify a certain coordinate system.
 *   The default Speeds are introduced to control the speed of move requests (absolute, relative, preset), where no explicit speed has been set.
 *   The allowed pan and tilt range for Pan/Tilt Limits is defined by a two-dimensional space range that is mapped to a specific Absolute Pan/Tilt Position Space.
 *   At least one Pan/Tilt Position Space is required by the PTZNode to support Pan/Tilt limits.
 *   The limits apply to all supported absolute, relative and continuous Pan/Tilt movements.
 *   The limits shall be checked within the coordinate system for which the limits have been specified.
 *   That means that even if movements are specified in a different coordinate system, the requested movements shall be transformed to the coordinate system of the limits where the limits can be checked.
 *   When a relative or continuous movements is specified, which would leave the specified limits, the PTZ unit has to move along the specified limits. The Zoom Limits have to be interpreted accordingly.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetConfiguration
 * Input:
 *   [GetConfiguration]
 * Output:
 *   [GetConfigurationResponse]
 */
int OnvifPTZ::getConfiguration(std::string configurationToken){
  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__GetConfiguration *tptz__GetConfiguration = soap_new__tptz__GetConfiguration(soap,-1);
  _tptz__GetConfigurationResponse *tptz__GetConfigurationResponse = soap_new__tptz__GetConfigurationResponse(soap,-1);

  tptz__GetConfiguration->PTZConfigurationToken = configurationToken;

  if(SOAP_OK == proxyPTZ->GetConfiguration(tptz__GetConfiguration, *tptz__GetConfigurationResponse)){

  }else{
    printErr(proxyPTZ, errorLog);
  }
}

/*
 * GetConfigurationOptions
 * Description:
 *   List supported coordinate systems including their range limitations.
 *   Therefore, the options MAY differ depending on whether the PTZ Configuration is assigned to a Profile containing a Video Source Configuration.
 *   In that case, the options may additionally contain coordinate systems referring to the image coordinate system described by the Video Source Configuration.
 *   If the PTZ Node supports continuous movements, it shall return a Timeout Range within which Timeouts are accepted by the PTZ Node.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetConfigurationOptions
 * Input:
 *   [GetConfigurationOptions]
 * Output:
 *   [GetConfigurationOptionsResponse]
 */
int OnvifPTZ::getConfigurationOptions(std::string configurationToken){
  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__GetConfigurationOptions *in = soap_new__tptz__GetConfigurationOptions(soap, -1);
  _tptz__GetConfigurationOptionsResponse *out = soap_new__tptz__GetConfigurationOptionsResponse(soap, -1);
  in->ConfigurationToken = configurationToken;

  if(SOAP_OK == proxyPTZ->GetConfigurationOptions(in, *out)){

  }else{
    printErr(proxyPTZ, errorLog);
  }
}
/*
 * GetConfigurations
 * Description:
 * Get all the existing PTZConfigurations from the device.
 * The default Position/Translation/Velocity Spaces are introduced to allow NVCs sending move requests without the need to specify a certain coordinate system.
 * The default Speeds are introduced to control the speed of move requests (absolute, relative, preset), where no explicit speed has been set.
 * The allowed pan and tilt range for Pan/Tilt Limits is defined by a two-dimensional space range that is mapped to a specific Absolute Pan/Tilt Position Space.
 * At least one Pan/Tilt Position Space is required by the PTZNode to support Pan/Tilt limits.
 * The limits apply to all supported absolute, relative and continuous Pan/Tilt movements. The limits shall be checked within the coordinate system for which the limits have been specified.
 * That means that even if movements are specified in a different coordinate system, the requested movements shall be transformed to the coordinate system of the limits where the limits can be checked.
 * When a relative or continuous movements is specified, which would leave the specified limits, the PTZ unit has to move along the specified limits. The Zoom Limits have to be interpreted accordingly.
 * SOAP action:
 * http://www.onvif.org/ver20/ptz/wsdl/GetConfigurations
 * Input:
 * [GetConfigurations]
 * Output:
 * [GetConfigurationsResponse]
 */

/*
 * GetNode
 * Description:
 *   Get a specific PTZ Node identified by a reference token or a name.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetNode
 * Input:
 *   [GetNode]
 * Output:
 *   [GetNodeResponse]
 */

/*
 * GetNodes
 * Description:
 *   Get the descriptions of the available PTZ Nodes.
 *   A PTZ-capable device may have multiple PTZ Nodes.
 *   The PTZ Nodes may represent mechanical PTZ drivers, uploaded PTZ drivers or digital PTZ drivers.
 *   PTZ Nodes are the lowest level entities in the PTZ control API and reflect the supported PTZ capabilities.
 *   The PTZ Node is referenced either by its name or by its reference token.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetNodes
 * Input:
 *   [GetNodes]
 * Output:
 *   [GetNodesResponse]
 */

/*
 * GetPresets
 * Description:
 *   Operation to request all PTZ presets for the PTZNode in the selected profile.
 *   The operation is supported if there is support for at least on PTZ preset by the PTZNode.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetPresets
 * Input:
 *   [GetPresets]
 * Output:
 *   [GetPresetsResponse]
 */

/*
 * GetPresetTour
 * Description:
 *   Operation to request a specific PTZ preset tour in the selected media profile.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetPresetTour
 * Input:
 *   [GetPresetTour]
 * Output:
 *   [GetPresetTourResponse]
 */

/*
 * GetPresetTourOptions
 * Description:
 *   Operation to request available options to configure PTZ preset tour.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetPresetTourOptions
 * Input:
 *   [GetPresetTourOptions]
 * Output:
 *   [GetPresetTourOptionsResponse]
 */

/*
 * GetPresetTours
 * Description:
 *   Operation to request PTZ preset tours in the selected media profiles.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetPresetTours
 * Input:
 *   [GetPresetTours]
 * Output:
 *   [GetPresetToursResponse]
 */

/*
 * GetServiceCapabilities
 * Description:
 *   Returns the capabilities of the PTZ service.
 *   The result is returned in a typed answer.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetServiceCapabilities
 * Input:
 *   [GetServiceCapabilities]
 * Output:
 *   [GetServiceCapabilitiesResponse]
 */

/*
 * GetStatus
 * Description:
 *   Operation to request PTZ status for the Node in the selected profile.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GetStatus
 * Input:
 *   [GetStatus]
 * Output:
 *   [GetStatusResponse]
 */
int OnvifPTZ::getStatus(std::string profileToken){
  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__GetStatus *tptz__GetStatus = soap_new__tptz__GetStatus(soap, -1);
  _tptz__GetStatusResponse *tptz__GetStatusResponse = soap_new__tptz__GetStatusResponse(soap, -1);

  tptz__GetStatus->ProfileToken = profileToken.c_str();

  if(SOAP_OK == proxyPTZ->GetStatus(tptz__GetStatus, *tptz__GetStatusResponse)){
    std::cout << "PAN: " << tptz__GetStatusResponse->PTZStatus->Position->PanTilt->x << std::endl << "TILT: " <<tptz__GetStatusResponse->PTZStatus->Position->PanTilt->y << std::endl;
    std::cout << "ZOOM:" << tptz__GetStatusResponse->PTZStatus->Position->Zoom->x << std::endl;

    //std::cout << "PanTilt:" << tptz__GetStatusResponse->PTZStatus->MoveStatus->PanTilt << std::endl << "ZoomStatus" << tptz__GetStatusResponse->PTZStatus->MoveStatus->Zoom << std::endl << "ERROR: " << tptz__GetStatusResponse->PTZStatus->Error << std::endl << "UtcTime: " << tptz__GetStatusResponse->PTZStatus->UtcTime << std::endl;
  }else{
    printErr(proxyPTZ, errorLog);
  }
}


/*
 * GotoHomePosition
 * Description:
 *   Operation to move the PTZ device to it's "home" position.
 *   The operation is supported if the HomeSupported element in the PTZNode is true.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GotoHomePosition
 * Input:
 *   [GotoHomePosition]
 * Output:
 *   [GotoHomePositionResponse]
 */
int OnvifPTZ::goToHomePosition(std::string profileToken){
  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__GotoHomePosition *tptz__GotoHomePosition = soap_new__tptz__GotoHomePosition(soap, -1);
  _tptz__GotoHomePositionResponse *tptz__GotoHomePositionResponse = soap_new__tptz__GotoHomePositionResponse(soap, -1);

  tptz__GotoHomePosition->ProfileToken = profileToken;

  if(SOAP_OK == proxyPTZ->GotoHomePosition(tptz__GotoHomePosition, *tptz__GotoHomePositionResponse)){

  }else{
    printErr(proxyPTZ, errorLog);
  }
}

/*
 * GotoPreset
 * Description:
 *   Operation to go to a saved preset position for the PTZNode in the selected profile.
 *   The operation is supported if there is support for at least on PTZ preset by the PTZNode.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/GotoPreset
 * Input:
 *   [GotoPreset]
 * Output:
 *   [GotoPresetResponse]
 */

/*
 * ModifyPresetTour
 * Description:
 *   Operation to modify a preset tour for the selected media profile.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/ModifyPresetTour
 * Input:
 *   [ModifyPresetTour]
 * Output:
 *   [ModifyPresetTourResponse]
 */

/*
 * OperatePresetTour
 * Description:
 *   Operation to perform specific operation on the preset tour in selected media profile.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/OperatePresetTour
 * Input:
 *   [OperatePresetTour]
 * Output:
 *   [OperatePresetTourResponse]
*/

/*
 * RelativeMove
 * Description:
 *   Operation for Relative Pan/Tilt and Zoom Move.
 *   The operation is supported if the PTZNode supports at least one relative Pan/Tilt or Zoom space.
 *   The speed argument is optional.
 *   If an x/y speed value is given it is up to the device to either use the x value as absolute resoluting speed vector or to map x and y to the component speed.
 *   If the speed argument is omitted, the default speed set by the PTZConfiguration will be used.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/RelativeMove
 * Input:
 *   [RelativeMove]
 * Output:
 *   [RelativeMoveResponse]
 */
int OnvifPTZ::relativeMove(std::string profileToken, float pan, float panSpeed, float tilt, float tiltSpeed, float zoom, float zoomSpeed){
  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__RelativeMove *tptz__RelativeMove = soap_new__tptz__RelativeMove(soap, -1);
  _tptz__RelativeMoveResponse *tptz__RelativeMoveResponse = soap_new__tptz__RelativeMoveResponse(soap, -1);

  tptz__RelativeMove->ProfileToken = profileToken;

  //setting pan and tilt
  tptz__RelativeMove->Translation = soap_new_tt__PTZVector(soap, -1);
  tptz__RelativeMove->Translation->PanTilt = soap_new_tt__Vector2D(soap, -1);
  tptz__RelativeMove->Speed = soap_new_tt__PTZSpeed(soap, -1);
  tptz__RelativeMove->Speed->PanTilt = soap_new_tt__Vector2D(soap, -1);
  //pan
  tptz__RelativeMove->Translation->PanTilt->x = pan;
  tptz__RelativeMove->Speed->PanTilt->x = panSpeed;
  //tilt
  tptz__RelativeMove->Translation->PanTilt->y = tilt;
  tptz__RelativeMove->Speed->PanTilt->y = tiltSpeed;
  //setting zoom
  tptz__RelativeMove->Translation->Zoom = soap_new_tt__Vector1D(soap, -1);
  tptz__RelativeMove->Speed->Zoom = soap_new_tt__Vector1D(soap, -1);
  tptz__RelativeMove->Translation->Zoom->x = zoom;
  tptz__RelativeMove->Speed->Zoom->x = zoomSpeed;

  if(SOAP_OK == proxyPTZ->RelativeMove(tptz__RelativeMove, *tptz__RelativeMoveResponse)){

  }else{
    std::cout << "RelativeMove ERROR:" << std::endl;
    printErr(proxyPTZ, errorLog);
    std::cout << std::endl;
  }
}

/*
 * RemovePreset
 * Description:
 *   Operation to remove a PTZ preset for the Node in the selected profile. The operation is supported if the PresetPosition capability exists for teh Node in the selected profile.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/RemovePreset
 * Input:
 *   [RemovePreset]
 * Output:
 *   [RemovePresetResponse]
 */

/*
 * RemovePresetTour
 * Description:
 *   Operation to delete a specific preset tour from the media profile.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/RemovePresetTour
 * Input:
 *   [RemovePresetTour]
 * Output:
 *   [RemovePresetTourResponse]
 */

/*
 * SendAuxiliaryCommand
 * Description:
 *   Operation to send auxiliary commands to the PTZ device mapped by the PTZNode in the selected profile.
 *   The operation is supported if the AuxiliarySupported element of the PTZNode is true
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/SendAuxiliaryCommand
 * Input:
 *   [SendAuxiliaryCommand]
 * Output:
 *   [SendAuxiliaryCommandResponse]
 */

/*
 * SetConfiguration
 * Description:
 *   Set/update a existing PTZConfiguration on the device.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/SetConfiguration
 * Input:
 *   [SetConfiguration]
 * Output:
 *   [SetConfigurationResponse]
 */

/*
 * SetHomePosition
 * Description:
 *   Operation to save current position as the home position.
 *   The SetHomePosition command returns with a failure if the “home” position is fixed and cannot be overwritten.
 *   If the SetHomePosition is successful, it is possible to recall the Home Position with the GotoHomePosition command.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/SetHomePosition
 * Input:
 *   [SetHomePosition]
 * Output:
 *   [SetHomePositionResponse]
 */
int OnvifPTZ::setHomePosition(std::string profileToken){
  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__SetHomePosition *tptz__SetHomePosition = soap_new__tptz__SetHomePosition(soap, -1);
  _tptz__SetHomePositionResponse *tptz__SetHomePositionResponse = soap_new__tptz__SetHomePositionResponse(soap, -1);

  tptz__SetHomePosition->ProfileToken = profileToken;

  if(SOAP_OK == proxyPTZ->SetHomePosition(tptz__SetHomePosition, *tptz__SetHomePositionResponse)){

  }else{
    printErr(proxyPTZ, errorLog);
  }
}

/*
 * SetPreset
 * Description:
 *   The SetPreset command saves the current device position parameters so that the device can move to the saved preset position through the GotoPreset operation.
 *   In order to create a new preset, the SetPresetRequest contains no PresetToken.
 *   If creation is successful, the Response contains the PresetToken which uniquely identifies the Preset.
 *   An existing Preset can be overwritten by specifying the PresetToken of the corresponding Preset.
 *   In both cases (overwriting or creation) an optional PresetName can be specified.
 *   The operation fails if the PTZ device is moving during the SetPreset operation.
 *   The device MAY internally save additional states such as imaging properties in the PTZ Preset which then should be recalled in the GotoPreset operation.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/SetPreset
 * Input:
 *   [SetPreset]
 * Output:
 *   [SetPresetResponse]
 */


/*
 * Stop
 * Description:
 *   Operation to stop ongoing pan, tilt and zoom movements of absolute relative and continuous type.
 *   If no stop argument for pan, tilt or zoom is set, the device will stop all ongoing pan, tilt and zoom movements.
 * SOAP action:
 *   http://www.onvif.org/ver20/ptz/wsdl/Stop
 * Input:
 *   [Stop]
 * Output:
 *   [StopResponse]
 */
int OnvifPTZ::stop(std::string profileToken, bool panTilt, bool zoom){

  //proxyPTZ->soap_endpoint = ptzURL.c_str();

  _tptz__Stop *tptz__Stop= soap_new__tptz__Stop(soap, -1);
  _tptz__StopResponse *tptz__StopResponse = soap_new__tptz__StopResponse(soap, -1);

  tptz__Stop->ProfileToken = profileToken;
  tptz__Stop->PanTilt = &panTilt;
  tptz__Stop->Zoom = &zoom;

  if(SOAP_OK == proxyPTZ->Stop(tptz__Stop, *tptz__StopResponse)){

  }else{
    printErr(proxyPTZ, errorLog);
  }
}

int OnvifPTZ::panLeft(std::string profileToken, int nDegrees){
  float pan = -nDegrees*0.002777778;
  return relativeMove(profileToken, pan, 1.0, 0.0, 0.0, 0.0, 0.0);
}

int OnvifPTZ::panRight(std::string profileToken, int nDegrees){
  float pan = nDegrees*0.002777778;
  return relativeMove(profileToken, pan, 1.0, 0.0, 0.0, 0.0, 0.0);
}

int OnvifPTZ::tiltDown(std::string profileToken, int nDegrees){
  float tilt = -nDegrees*0.005555556;
  return relativeMove(profileToken, 0.0, 0.0, tilt, 1.0, 0.0, 0.0);
}

int OnvifPTZ::tiltUp(std::string profileToken, int nDegrees){
  float tilt = nDegrees*0.005555556;
  return relativeMove(profileToken, 0.0, 0.0, tilt, 1.0, 0.0, 0.0);
}

int OnvifPTZ::zoomIn(std::string profileToken, float zoom){
//  return relativeMove(profileToken, 0.0, 0.0, 0.0, 0.0, 0.05, 1.0);
  return relativeMove(profileToken, 0.0, 0.0, 0.0, 0.0, zoom, 1.0);
}

int OnvifPTZ::zoomOut(std::string profileToken, float zoom){
//  return relativeMove(profileToken, 0.0, 0.0, 0.0, 0.0, -0.05, 1.0);
  return relativeMove(profileToken, 0.0, 0.0, 0.0, 0.0, -zoom, 1.0);
}
