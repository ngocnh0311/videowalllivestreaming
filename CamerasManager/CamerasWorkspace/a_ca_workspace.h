#ifndef A_CAWorkspace_H
#define A_CAWorkspace_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_ca_workspace.h"
class C_CAWorkspace;
class A_CAWorkspace : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CAWorkspace(Control *ctrl);
    C_CAWorkspace *control() { return (C_CAWorkspace *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CAWorkspace_H
