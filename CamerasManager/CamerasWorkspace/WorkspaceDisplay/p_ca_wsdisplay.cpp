#include "p_ca_wsdisplay.h"

/**
     * Generic method to override for updating the presention.
     **/

P_CA_WSDisplay::P_CA_WSDisplay(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    // init gui object
    this->zone = _zone;
    // init work space
    stackLayout = new QStackedLayout();
    stackLayout->setSpacing(0);
    stackLayout->setMargin(0);
    stackLayout->setAlignment(Qt::AlignCenter);
    zone->setLayout(stackLayout);

    workspaceLiveWidget = new QWidget(this->zone);
//    workspaceLiveWidget->setStyleSheet("background:#b1a162");
    workspaceOnvifWidget = new QWidget(this->zone);
//    workspaceOnvifWidget->setStyleSheet("background:#80b758");

    stackLayout->addWidget(workspaceLiveWidget);
    stackLayout->addWidget(workspaceOnvifWidget);

    stackLayout->setCurrentWidget(workspaceLiveWidget);
}
void P_CA_WSDisplay::showTabLive(){
    stackLayout->setCurrentWidget(workspaceLiveWidget);
}

void P_CA_WSDisplay::showTabOnvif(){
    stackLayout->setCurrentWidget(workspaceOnvifWidget);
}

void P_CA_WSDisplay::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CA_WSDisplay::update() {}

QWidget *P_CA_WSDisplay::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return workspaceLiveWidget;
    case 2:
        return workspaceOnvifWidget;
    default:
        return Q_NULLPTR;
    }
}

void P_CA_WSDisplay::enterFullscreenMode() {/* rightBar->setFixedWidth(0);*/ }

void P_CA_WSDisplay::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
