#ifndef P_CA_WSDisplay_H
#define P_CA_WSDisplay_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_ca_wsdisplay.h"
#include <QTextEdit>
#include <QCheckBox>
#include <QStackedLayout>
class C_CA_WSDisplay;

class P_CA_WSDisplay : public Presentation {
    // init ui control
private:
public:
    QWidget *zone = Q_NULLPTR;
    QStackedLayout *stackLayout = Q_NULLPTR;

    QWidget *workspaceLiveWidget = Q_NULLPTR;
    QWidget *workspaceOnvifWidget = Q_NULLPTR;

    P_CA_WSDisplay(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_CA_WSDisplay *control() { return (C_CA_WSDisplay *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();
    void showTabLive();
    void showTabOnvif();

protected:
};

#endif  // P_CA_WSDisplay_H
