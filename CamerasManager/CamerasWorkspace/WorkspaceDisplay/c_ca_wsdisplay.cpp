#include "c_ca_wsdisplay.h"

#include "TabPlayLive/c_ca_tabplaylive.h"
#include "OnvifClient/OVCameraConfig/OVOnvifControl/c_ov_onvifcontrol.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_CA_WSDisplay::C_CA_WSDisplay(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();
    this->zone = _zone;

    this->abst = new A_CA_WSDisplay(this);
    this->pres = new P_CA_WSDisplay(this, zone);

    cCATabPlayLive = new C_CATabPlayLive(this, presentation()->getZone(1));
    cOnvifControl = new C_OVOnvifControl(this, presentation()->getZone(2));
    // create others controls
    // cXXX = new C_XXX();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CA_WSDisplay::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case 1:
        break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CA_WSDisplay::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CA_WSDisplay::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_CAMERAS_CAMERA_SELECTED:{
        cCATabPlayLive->newAction(message, attachment);
    }    break;

    case Message.APP_SHOW:{

    }break;

    case Message.APP_CONTEXT_GET:{
        attachment->setValue(appContext);
    }break;

    case Message.APP_CAMERAS_SHOW_TAB_LIVE:{
        presentation()->showTabLive();
    }break;

    case Message.APP_CAMERAS_SHOW_TAB_ONVIF:{
        presentation()->showTabOnvif();
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
