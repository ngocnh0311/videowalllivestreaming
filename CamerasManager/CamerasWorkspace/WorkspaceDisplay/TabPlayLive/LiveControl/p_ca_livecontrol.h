#ifndef P_CALiveControl_H
#define P_CALiveControl_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include <QTextEdit>
#include <QCheckBox>

class P_CALiveControl : public Presentation {
    // init ui control
private:

public:
    QWidget *zone = Q_NULLPTR;
    QLayout *layout = Q_NULLPTR;

    QTabWidget *tabWidget;

    QWidget *image;
    QWidget *ptz;

    P_CALiveControl(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);
    float rightBarPercent = 0.2;

    void enterFullscreenMode();
    void exitFullscreenMode();
    void playSource(QString url);
protected:
};

#endif  // P_CALiveControl_H
