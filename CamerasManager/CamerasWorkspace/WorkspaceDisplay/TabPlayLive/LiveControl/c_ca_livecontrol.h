#ifndef C_CALiveControl_H
#define C_CALiveControl_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"

class P_CALiveControl;
class A_CALiveControl;

class C_CATabPlayLive;

class C_OVOnvifImaging;
class C_OVOnvifPTZ;

class C_CALiveControl : public Control {
 public:
  QWidget* zone;
  AppContext* appContext;
  QString appName = "Cameras";

  C_OVOnvifImaging *cOnvifImaging;
  C_OVOnvifPTZ *cOnvifPTZ;

  C_CALiveControl(Control* ctrl, QWidget *_zone);
  C_CATabPlayLive* getParent() { return (C_CATabPlayLive*)this->parent; }

  P_CALiveControl* presentation() { return (P_CALiveControl*)pres; }
  A_CALiveControl* abstraction() { return (A_CALiveControl*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CALiveControl_H
