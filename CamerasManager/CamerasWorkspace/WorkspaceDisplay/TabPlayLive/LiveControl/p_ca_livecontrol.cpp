#include "p_ca_livecontrol.h"
/**
     * Generic method to override for updating the presention.
     **/

P_CALiveControl::P_CALiveControl(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
  // init gui object
  this->zone = _zone;

  // init work space
  layout = new QVBoxLayout();
  layout->setSpacing(0);
  layout->setMargin(0);
  this->zone->setLayout(layout);

  tabWidget = new QTabWidget(this->zone);
  tabWidget->setObjectName(QStringLiteral("tabWidget"));

  this->image = new QWidget();
  this->ptz = new QWidget();

  tabWidget->addTab(image, QString("Image"));
  tabWidget->addTab(ptz, QString("PTZ"));

  this->layout->addWidget(this->tabWidget);
}

void P_CALiveControl::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CALiveControl::update() {}

QWidget *P_CALiveControl::getZone(int zoneId) {
  switch (zoneId) {
  case 1:
    return this->image;
  case 2:
    return this->ptz;
  default:
    return Q_NULLPTR;
  }
}

void P_CALiveControl::enterFullscreenMode() {/* rightBar->setFixedWidth(0);*/ }

void P_CALiveControl::exitFullscreenMode() {
  //    rightBar->setFixedWidth(0);
}
