#ifndef A_CALiveControl_H
#define A_CALiveControl_H

#include <QObject>
#include <QTimer>

#include "PacModel/control.h"

class C_CALiveControl;
class A_CALiveControl : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CALiveControl(Control *ctrl);
    C_CALiveControl *control() { return (C_CALiveControl *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CALiveControl_H
