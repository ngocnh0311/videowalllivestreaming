#include "p_ca_livecontrol.h"
#include "a_ca_livecontrol.h"
#include "c_ca_livecontrol.h"

#include "Player/cam9playerpool.h"
#include "CamerasManager/CamerasWorkspace/WorkspaceDisplay/TabPlayLive/c_ca_tabplaylive.h"

#include "OnvifClient/OVCameraConfig/OVOnvifControl/OVOnvifImaging/c_ov_onvifimaging.h"
#include "OnvifClient/OVCameraConfig/OVOnvifControl/OVOnvifPTZ/c_ov_onvifptz.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_CALiveControl::C_CALiveControl(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);

    this->appContext = dataStruct->value<AppContext*>();
    this->zone = _zone;

    this->abst = new A_CALiveControl(this);
    this->pres = new P_CALiveControl(this, zone);

    cOnvifImaging = new C_OVOnvifImaging(this, presentation()->getZone(1));
    cOnvifPTZ = new C_OVOnvifPTZ(this, presentation()->getZone(2));

    // create others controls
    // cXXX = new C_XXX();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CALiveControl::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case 1:
        break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CALiveControl::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CALiveControl::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_CAMERAS_CAMERA_SELECTED:{
        CamItem *camItem = attachment->value<CamItem *>();
        if(camItem != Q_NULLPTR){

        }
    }    break;

    case Message.APP_SHOW:{

    }break;

    case Message.APP_CONTEXT_GET:{
        attachment->setValue(appContext);
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
