#include "p_ca_tabplaylive.h"
/**
     * Generic method to override for updating the presention.
     **/

P_CATabPlayLive::P_CATabPlayLive(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    // init gui object
    this->zone = _zone;

    // init work space
    layout = new QHBoxLayout();
    layout->setSpacing(0);
    layout->setMargin(0);
    zone->setLayout(layout);

    leftPlayerWidget = new QWidget(this->zone);
    QVBoxLayout *leftPlayerLayout = new QVBoxLayout();
    leftPlayerLayout->setSpacing(0);
    leftPlayerLayout->setMargin(0);
    leftPlayerWidget->setLayout(leftPlayerLayout);

    mpvPlayer = new MpvWidget();
    leftPlayerLayout->addWidget(mpvPlayer);

    rightControlWidget = new QWidget(this->zone);
//    rightControlWidget->setStyleSheet("background:#4277b3");
    rightControlWidget->setFixedWidth(widthRight);
//    rightControlWidget->setStyleSheet("border: 3px solid white");

//    leftPlayerWidget->resize(this->zone->width() - widthRight , this->zone->height());
    rightControlWidget->resize(widthRight , this->zone->height());

    layout->addWidget(leftPlayerWidget);
    layout->addWidget(rightControlWidget);
}

void P_CATabPlayLive::playSource(QString url){
    mpvPlayer->open(url);
}

void P_CATabPlayLive::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CATabPlayLive::update() {}

QWidget *P_CATabPlayLive::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return leftPlayerWidget;
    case 2:
        return rightControlWidget;
    default:
        return Q_NULLPTR;
    }
}

void P_CATabPlayLive::enterFullscreenMode() {/* rightBar->setFixedWidth(0);*/ }

void P_CATabPlayLive::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
