#ifndef C_CATabPlayLive_H
#define C_CATabPlayLive_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "CamerasManager/CamerasWorkspace/WorkspaceDisplay/TabPlayLive/a_ca_tabplaylive.h"
#include "CamerasManager/CamerasWorkspace/WorkspaceDisplay/TabPlayLive/p_ca_tabplaylive.h"
#include "CamerasManager/CamerasWorkspace/WorkspaceDisplay/c_ca_wsdisplay.h"

class P_CATabPlayLive;
class A_CATabPlayLive;
class C_OVWorkspace;
class C_TopControlBar;
class C_CA_WSDisplay;
class C_CALiveControl;

class C_CATabPlayLive : public Control {
 public:
  QWidget* zone;
  AppContext* appContext;
  QString appName = "Cameras";

  C_CALiveControl *cLiveControl;

  C_CATabPlayLive(Control* ctrl, QWidget *_zone);
  C_CA_WSDisplay* getParent() { return (C_CA_WSDisplay*)this->parent; }
  P_CATabPlayLive* presentation() { return (P_CATabPlayLive*)pres; }
  A_CATabPlayLive* abstraction() { return (A_CATabPlayLive*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CATabPlayLive_H
