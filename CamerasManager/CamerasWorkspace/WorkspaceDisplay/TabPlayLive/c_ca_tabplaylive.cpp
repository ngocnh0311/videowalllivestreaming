#include "c_ca_tabplaylive.h"
#include "Player/cam9playerpool.h"
#include "LiveControl/c_ca_livecontrol.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_CATabPlayLive::C_CATabPlayLive(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();
    this->zone = _zone;

    this->abst = new A_CATabPlayLive(this);
    this->pres = new P_CATabPlayLive(this, zone);

    cLiveControl = new C_CALiveControl(this, presentation()->getZone(2));

    // create others controls
    // cXXX = new C_XXX();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CATabPlayLive::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case 1:
        break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CATabPlayLive::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CATabPlayLive::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_CAMERAS_CAMERA_SELECTED:{
        CamItem *camItem = attachment->value<CamItem *>();
        if(camItem != Q_NULLPTR){
            CamItemType camType;
            camType.dataSource = "CAM";
            camType.network = "LAN";
            camType.protocol = "RTSP";
            camType.name = "HD";

            CamStream *camStream = camItem->getCamStream(camType);
            if(camStream){
                QString sourceRTSP = camStream->getSource();
                presentation()->playSource(sourceRTSP);
            }
        }
    }    break;

    case Message.APP_SHOW:{

    }break;

    case Message.APP_CONTEXT_GET:{
        attachment->setValue(appContext);
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
