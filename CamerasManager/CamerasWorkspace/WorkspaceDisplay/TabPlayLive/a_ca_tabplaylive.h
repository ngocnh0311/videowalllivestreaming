#ifndef A_CATabPlayLive_H
#define A_CATabPlayLive_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_ca_tabplaylive.h"
class C_CATabPlayLive;
class A_CATabPlayLive : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CATabPlayLive(Control *ctrl);
    C_CATabPlayLive *control() { return (C_CATabPlayLive *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CATabPlayLive_H
