#ifndef P_CATabPlayLive_H
#define P_CATabPlayLive_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_ca_tabplaylive.h"
#include <QTextEdit>
#include <QCheckBox>
#include "Player/mpvwidget.h"

class C_OnvifClient;
class P_CATabPlayLive : public Presentation {
    // init ui control
private:
public:
    int widthRight = 335;
    QWidget *zone = Q_NULLPTR;
    QHBoxLayout *layout = Q_NULLPTR;

    MpvWidget *mpvPlayer = Q_NULLPTR;

    QWidget *leftPlayerWidget = Q_NULLPTR;
    QWidget *rightControlWidget = Q_NULLPTR;

    P_CATabPlayLive(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);
    float rightBarPercent = 0.2;

    C_OnvifClient *control() { return (C_OnvifClient *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();
    void playSource(QString url);
protected:
};

#endif  // P_CATabPlayLive_H
