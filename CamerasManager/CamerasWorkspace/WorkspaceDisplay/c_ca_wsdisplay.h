#ifndef C_CA_WSDisplay_H
#define C_CA_WSDisplay_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "CamerasManager/CamerasWorkspace/WorkspaceDisplay/a_ca_wsdisplay.h"
#include "CamerasManager/CamerasWorkspace/WorkspaceDisplay/p_ca_wsdisplay.h"
#include "CamerasManager/CamerasWorkspace/c_ca_workspace.h"

class P_CA_WSDisplay;
class A_CA_WSDisplay;
class C_CAWorkspace;
class C_CATabPlayLive;
class C_OVOnvifControl;

class C_CA_WSDisplay : public Control {
public:
    QWidget* zone;
    AppContext* appContext;

    C_CATabPlayLive *cCATabPlayLive = Q_NULLPTR;
    C_OVOnvifControl *cOnvifControl = Q_NULLPTR;

    C_CA_WSDisplay(Control* ctrl, QWidget *_zone);
    C_CAWorkspace* getParent() { return (C_CAWorkspace*)this->parent; }
    P_CA_WSDisplay* presentation() { return (P_CA_WSDisplay*)pres; }
    A_CA_WSDisplay* abstraction() { return (A_CA_WSDisplay*)abst; }
    /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
    void newUserAction(int message, QVariant* attachment) override;
    /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newSystemAction(int message, QVariant* attachment) override;

    /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CA_WSDisplay_H
