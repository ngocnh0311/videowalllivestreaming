#ifndef A_CA_WSDisplay_H
#define A_CA_WSDisplay_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_ca_wsdisplay.h"
class C_CA_WSDisplay;
class A_CA_WSDisplay : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CA_WSDisplay(Control *ctrl);
    C_CA_WSDisplay *control() { return (C_CA_WSDisplay *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CA_WSDisplay_H
