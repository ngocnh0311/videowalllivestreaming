#ifndef C_CAWorkspace_H
#define C_CAWorkspace_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "CamerasManager/CamerasWorkspace/p_ca_workspace.h"
#include "CamerasManager/CamerasWorkspace/a_ca_workspace.h"
#include "CamerasManager/CamerasWorkspace/WSTopControlBar/c_ca_topcontrolbar.h"
#include "CamerasManager/CamerasWorkspace/WorkspaceDisplay/c_ca_wsdisplay.h"

class P_CAWorkspace;
class A_CAWorkspace;
class C_CamerasManager;


class C_CAWorkspace : public Control {
public:
    QWidget* zone;
    AppContext* appContext;

    C_CA_WSDisplay *cWSDisplay = Q_NULLPTR;
    C_CA_TopControlBar *cCATopControlBar = Q_NULLPTR;

    C_CAWorkspace(Control* ctrl, QWidget *_zone);
    C_CamerasManager* getParent() { return (C_CamerasManager*)this->parent; }
    P_CAWorkspace* presentation() { return (P_CAWorkspace*)pres; }
    A_CAWorkspace* abstraction() { return (A_CAWorkspace*)abst; }
    /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
    void newUserAction(int message, QVariant* attachment) override;
    /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newSystemAction(int message, QVariant* attachment) override;

    /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CAWorkspace_H
