#ifndef P_CA_TopControlBar_H
#define P_CA_TopControlBar_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_ca_topcontrolbar.h"
#include <QTextEdit>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
class C_CA_TopControlBar;

class P_CA_TopControlBar : public Presentation {
    Q_OBJECT
    // init ui control
private:
public:
    QWidget *zone = Q_NULLPTR;
    QVBoxLayout *layout = Q_NULLPTR;

    int heightTop = 50;
    QWidget *topWidget = Q_NULLPTR;
    QHBoxLayout *topLayout = Q_NULLPTR;

    QWidget *bottomWidget = Q_NULLPTR;
    QHBoxLayout *bottomLayout = Q_NULLPTR;
    P_CA_TopControlBar(Control *ctrl, QWidget *zone);

    QLabel *statusLabel = Q_NULLPTR;
    QLabel *titleCameraLabel = Q_NULLPTR;

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_CA_TopControlBar *control() { return (C_CA_TopControlBar *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();
    void updateTitleCamera(QString titleCamera);
protected:
public Q_SLOTS:
    void tabLiveClicked();
    void tabOnvifClicked();
};

#endif  // P_CA_TopControlBar_H
