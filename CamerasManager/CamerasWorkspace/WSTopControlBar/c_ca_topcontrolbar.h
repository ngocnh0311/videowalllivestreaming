#ifndef C_CA_TopControlBar_H
#define C_CA_TopControlBar_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "CamerasManager/CamerasWorkspace/WSTopControlBar/p_ca_topcontrolbar.h"
#include "CamerasManager/CamerasWorkspace/WSTopControlBar/a_ca_topcontrolbar.h"
class P_CA_TopControlBar;
class A_CA_TopControlBar;
class C_CAWorkspace;
class C_CA_TopControlBar : public Control {
 public:
  QWidget* zone;
  AppContext* appContext;

  C_CA_TopControlBar(Control* ctrl, QWidget *_zone);
  C_CAWorkspace* getParent() { return (C_CAWorkspace*)this->parent; }
  P_CA_TopControlBar* presentation() { return (P_CA_TopControlBar*)pres; }
  A_CA_TopControlBar* abstraction() { return (A_CA_TopControlBar*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CA_TopControlBar_H
