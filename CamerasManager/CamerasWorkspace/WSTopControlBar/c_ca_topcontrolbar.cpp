#include "c_ca_topcontrolbar.h"
#include "CamerasManager/CamerasWorkspace/c_ca_workspace.h"
/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_CA_TopControlBar::C_CA_TopControlBar(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();
    this->zone = _zone;

    this->abst = new A_CA_TopControlBar(this);
    this->pres = new P_CA_TopControlBar(this, zone);
    // create others controls
    // cXXX = new C_XXX();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CA_TopControlBar::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_CAMERAS_SHOW_TAB_LIVE:
    case Message.APP_CAMERAS_SHOW_TAB_ONVIF:{
        getParent()->newAction(message, attachment);
    }break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CA_TopControlBar::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CA_TopControlBar::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_CAMERAS_CAMERA_SELECTED:{
        CamItem *camItem = attachment->value<CamItem *>();
        if(camItem){
            QString positonCamera = camItem->getPostion();
            presentation()->updateTitleCamera(positonCamera);
        }
    }    break;

    case Message.APP_SHOW:{

    }break;

    case Message.APP_CONTEXT_GET:{
        attachment->setValue(appContext);
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
