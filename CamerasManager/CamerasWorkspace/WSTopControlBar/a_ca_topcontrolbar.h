#ifndef A_CA_TopControlBar_H
#define A_CA_TopControlBar_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_ca_topcontrolbar.h"
class C_CA_TopControlBar;
class A_CA_TopControlBar : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CA_TopControlBar(Control *ctrl);
    C_CA_TopControlBar *control() { return (C_CA_TopControlBar *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CA_TopControlBar_H
