#include "p_ca_topcontrolbar.h"
#include "Common/resources.h"
/**
     * Generic method to override for updating the presention.
     **/

P_CA_TopControlBar::P_CA_TopControlBar(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    // init gui object
    this->zone = _zone;
    zone->setStyleSheet("background:#333;color:#fff");

    // init work space
    layout = new QVBoxLayout();
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->setAlignment(Qt::AlignTop);
    zone->setLayout(layout);

    topWidget  = new QWidget(this->zone);
    topWidget->setFixedHeight(heightTop);
    topLayout = new QHBoxLayout();
    topLayout->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    topLayout->setSpacing(5);
    topLayout->setContentsMargins(10,0,0,10);
    topWidget->setLayout(topLayout);


    QLabel *iconCameraLabel = new QLabel(topWidget);
    iconCameraLabel->setAlignment(Qt::AlignCenter);
    iconCameraLabel->setFixedSize(40,25);
    iconCameraLabel->setStyleSheet("color:#35aa47");
    iconCameraLabel->setFont(Resources::instance().getExtraLargeBoldButtonFont());
    iconCameraLabel->setText("");

//    statusLabel = new QLabel(topWidget);
//    statusLabel->setFixedSize(40,25);
//    statusLabel->setFont(Resources::instance().getSmallBoldButtonFont());
//    statusLabel->setAlignment(Qt::AlignCenter);
//    statusLabel->setStyleSheet("background:#35aa47;color:#fff");
//    statusLabel->setText("LIVE");

    titleCameraLabel = new QLabel(topWidget);
    titleCameraLabel->setStyleSheet("color:#fff");
    titleCameraLabel->setFont(Resources::instance().getLargeBoldButtonFont());
    titleCameraLabel->setText("Tên camera");

    topLayout->addWidget(iconCameraLabel);
//    topLayout->addWidget(statusLabel);
    topLayout->addWidget(titleCameraLabel);

    bottomWidget = new QWidget(this->zone);
    bottomLayout = new QHBoxLayout();
    bottomLayout->setAlignment(Qt::AlignLeft);
    bottomLayout->setSpacing(10);
    bottomLayout->setMargin(0);
    bottomWidget->setLayout(bottomLayout);

    QPushButton *tabLivebutton = new QPushButton(bottomWidget);
    tabLivebutton->setStyleSheet("background:#111;color:#fff");
    tabLivebutton->setFixedSize(80,40);
    tabLivebutton->setFont(Resources::instance().getMediumRegularButtonFont());
    tabLivebutton->setText("LIVE");

    QPushButton *tabOnvifbutton = new QPushButton(bottomWidget);
    tabOnvifbutton->setStyleSheet("background:#111;color:#fff");
    tabOnvifbutton->setFixedSize(80,40);
    tabOnvifbutton->setFont(Resources::instance().getMediumRegularButtonFont());
    tabOnvifbutton->setText("ONVIF");

    connect(tabLivebutton, &QPushButton::clicked ,this, &P_CA_TopControlBar::tabLiveClicked);
    connect(tabOnvifbutton, &QPushButton::clicked ,this, &P_CA_TopControlBar::tabOnvifClicked);

    bottomLayout->addWidget(tabLivebutton);
    bottomLayout->addWidget(tabOnvifbutton);

    layout->addWidget(topWidget);
    layout->addWidget(bottomWidget);
}

void P_CA_TopControlBar::updateTitleCamera(QString titleCamera){
    titleCameraLabel->setText(titleCamera);
}

void P_CA_TopControlBar::tabLiveClicked(){
    control()->newUserAction(Message.APP_CAMERAS_SHOW_TAB_LIVE, Q_NULLPTR);
}
void P_CA_TopControlBar::tabOnvifClicked(){
    control()->newUserAction(Message.APP_CAMERAS_SHOW_TAB_ONVIF, Q_NULLPTR);
}

void P_CA_TopControlBar::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CA_TopControlBar::update() {}

QWidget *P_CA_TopControlBar::getZone(int zoneId) {
    Q_UNUSED(zoneId)
    switch (zoneId) {
    case 1:
    default:
        return Q_NULLPTR;
    }
}

void P_CA_TopControlBar::enterFullscreenMode() {/* rightBar->setFixedWidth(0);*/ }

void P_CA_TopControlBar::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
