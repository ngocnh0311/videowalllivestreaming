#include "p_ca_workspace.h"

/**
     * Generic method to override for updating the presention.
     **/

P_CAWorkspace::P_CAWorkspace(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    // init gui object
    this->zone = _zone;
    topControlBarWidget = new QWidget(this->zone);
    topControlBarWidget->setFixedHeight(heightTop);
    workspaceDisplayWidget = new QWidget(this->zone);

    // init work space
    layout = new QVBoxLayout();
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->setAlignment(Qt::AlignTop);
    zone->setLayout(layout);

    layout->addWidget(topControlBarWidget);
    layout->addWidget(workspaceDisplayWidget);

}


void P_CAWorkspace::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CAWorkspace::update() {}

QWidget *P_CAWorkspace::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return topControlBarWidget;
    case 2:
        return workspaceDisplayWidget;
    default:
        return Q_NULLPTR;
    }
}

void P_CAWorkspace::enterFullscreenMode() {/* rightBar->setFixedWidth(0);*/ }

void P_CAWorkspace::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
