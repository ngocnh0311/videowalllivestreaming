#ifndef P_CAWorkspace_H
#define P_CAWorkspace_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_ca_workspace.h"
#include <QTextEdit>
#include <QCheckBox>
class C_CAWorkspace;

class P_CAWorkspace : public Presentation {
    // init ui control
private:
public:
    int heightTop = 80;
    QWidget *zone = Q_NULLPTR;
    QVBoxLayout *layout = Q_NULLPTR;

    QWidget *topControlBarWidget = Q_NULLPTR;
    QWidget *workspaceDisplayWidget = Q_NULLPTR;

    P_CAWorkspace(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);
    float rightBarPercent = 0.2;

    C_CAWorkspace *control() { return (C_CAWorkspace *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

protected:
};

#endif  // P_CAWorkspace_H
