#ifndef P_CALeftControlBar_H
#define P_CALeftControlBar_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_ca_leftcontrolbar.h"
#include <QTextEdit>
#include <QListWidget>
#include <QCheckBox>
class C_CALeftControlBar;

class P_CALeftControlBar : public Presentation {
    Q_OBJECT
    // init ui control
private:
public:
    QWidget *zone = Q_NULLPTR;
    QVBoxLayout *layout = Q_NULLPTR;

    QWidget *camerasSelectedWidget = Q_NULLPTR;
//    QWidget *camerasCmsWidget = Q_NULLPTR;
//    QWidget *camerasLocalWidget = Q_NULLPTR;

    P_CALeftControlBar(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_CALeftControlBar *control() { return (C_CALeftControlBar *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

protected:
public Q_SLOTS:
    void searchCamerasButtonsClicked();
};

#endif  // P_CALeftControlBar_H
