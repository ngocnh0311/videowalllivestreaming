#include "p_camerascms.h"
#include "Common/resources.h"
/**
     * Generic method to override for updating the presention.
     **/

P_CamerasCMS::P_CamerasCMS(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    // init gui object
    this->zone = _zone;
    // init work space
    layout = new QVBoxLayout();
    layout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
    layout->setSpacing(0);
    layout->setMargin(0);
    zone->setLayout(layout);

    topWidget = new QWidget(this->zone);
    topWidget->setStyleSheet("background:#5f5f5f");
    topLayout = new QHBoxLayout();
    topLayout->setSpacing(15);
    topLayout->setMargin(0);
    topWidget->setLayout(topLayout);


    bottomWidget = new QWidget(this->zone);
    bottomLayout = new QVBoxLayout();
    bottomLayout->setSpacing(15);
    bottomLayout->setMargin(0);
    bottomWidget->setLayout(bottomLayout);


    QWidget *leftCamerasLocalWidget = new QWidget(topWidget);
    QVBoxLayout *leftCamerasLocalLayout = new QVBoxLayout();
    leftCamerasLocalLayout->setMargin(0);
    leftCamerasLocalLayout->setSpacing(0);
    leftCamerasLocalWidget->setLayout(leftCamerasLocalLayout);

    QLabel *cameraCmsTitle = new QLabel(leftCamerasLocalWidget);
    cameraCmsTitle->setFont(Resources::instance().getMediumRegularButtonFont());
    cameraCmsTitle->setText("Cameras CMS: ");
    leftCamerasLocalLayout->addWidget(cameraCmsTitle);


    QWidget *rightCamerasLocalWidget = new QWidget(topWidget);
    QVBoxLayout *rightCamerasLocalLayout = new QVBoxLayout();
    rightCamerasLocalLayout->setMargin(0);
    rightCamerasLocalLayout->setSpacing(0);
    rightCamerasLocalWidget->setLayout(rightCamerasLocalLayout);

    QPushButton *addAllButton = new QPushButton(rightCamerasLocalWidget);
    addAllButton->setFont(Resources::instance().getMediumRegularButtonFont());
    addAllButton->setText("Add All");
    rightCamerasLocalLayout->addWidget(addAllButton);

    topLayout->addWidget(leftCamerasLocalWidget);
    topLayout->addWidget(rightCamerasLocalWidget);

    QWidget *bottomContainCamerasLocalWidget = new QWidget(bottomWidget);
    QVBoxLayout *bottomContainCamerasLocalLayout = new QVBoxLayout();
    bottomContainCamerasLocalLayout->setMargin(0);
    bottomContainCamerasLocalLayout->setSpacing(0);
    bottomContainCamerasLocalWidget->setLayout(bottomContainCamerasLocalLayout);
    bottomLayout->addWidget(bottomContainCamerasLocalWidget);

    listCamerasCmsWidget = new QListWidget(bottomContainCamerasLocalWidget);
    listCamerasCmsWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    bottomContainCamerasLocalLayout->addWidget(listCamerasCmsWidget);
    connect(listCamerasCmsWidget, &QListWidget::itemClicked , this, &P_CamerasCMS::onListCamerasCmsClicked);
    loadListCamerasCms();
    layout->addWidget(topWidget);
    layout->addWidget(bottomWidget);

}

void P_CamerasCMS::loadListCamerasCms(){
    listCamerasCmsWidget->clear();
    QStringList listBlackListDevice;
    listBlackListDevice.append("10.13.11.119:2000");
    listBlackListDevice.append("10.13.11.83:2000");
    listBlackListDevice.append("10.13.11.129:2000");
    listBlackListDevice.append("10.13.11.251:2000");
    listBlackListDevice.append("10.13.11.174:2000");
    listBlackListDevice.append("10.13.11.75:2000");
    listBlackListDevice.append("10.13.11.77:2000");
    listBlackListDevice.append("10.13.11.39:2000");
    listBlackListDevice.append("10.13.11.115:2000");
    listBlackListDevice.append("10.13.11.110:2000");

    for (int index  = 0; index < listCamerasCellCustom.size(); ++index) {
        CameraCmsCell* cameraCmsCell = listCamerasCellCustom.takeAt(index);
        delete cameraCmsCell;
    }
    for (int index  = 0; index < listWidgetItems.size(); ++index) {
        QListWidgetItem* item = listWidgetItems.takeAt(index);
        delete item;
    }

    for (int index = 0; index < listBlackListDevice.size(); ++index) {
        CameraCmsCell *cameraCmsCell = new CameraCmsCell(listCamerasCmsWidget, listBlackListDevice.at(index));

        if(index == 0){
            cameraCmsCell->updateBackground("#555" , "#FFF");
        }

        listCamerasCellCustom.append(cameraCmsCell);
        QListWidgetItem* listItem = new QListWidgetItem();
        listWidgetItems.append(listItem);
        listItem->setSizeHint(cameraCmsCell->sizeHint());
        listCamerasCmsWidget->addItem(listItem);
        listCamerasCmsWidget->setItemWidget(listItem, cameraCmsCell);
    }
}

void P_CamerasCMS::onListCamerasCmsClicked(QListWidgetItem *item){
    if(item){
        //        int indexItemSelected = listItemCellIpDevice.indexOf(item);
        //        qDebug() <<Q_FUNC_INFO << "indexItemSelected" <<indexItemSelected;
        //        //        if(indexItemSelected == indexItemSelectedLast) return;
        //        indexItemSelectedLast =  indexItemSelected;

        //        //reset color of cell before
        //        for (int index = 0; index < listIpDeviceCells.size(); ++index) {
        //            IPDeviceCell *ipDeviceCell = listIpDeviceCells.at(index);
        //            ipDeviceCell->updateBackground("#333", "#ddd");
        //        }

        //        IPDeviceCell *ipDeviceCell = listIpDeviceCells.at(indexItemSelected);
        //        ipDeviceCell->updateBackground("#555" , "#FFF");
        //        ipDeviceCell->updateSelected();
    }
}

void P_CamerasCMS::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CamerasCMS::update() {}

QWidget *P_CamerasCMS::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return this->zone;
    default:
        return Q_NULLPTR;
    }
}

void P_CamerasCMS::enterFullscreenMode() {/* rightBar->setFixedWidth(0);*/ }

void P_CamerasCMS::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
