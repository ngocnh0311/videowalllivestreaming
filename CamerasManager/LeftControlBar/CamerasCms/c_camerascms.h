#ifndef C_CamerasCMS_H
#define C_CamerasCMS_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "CamerasManager/LeftControlBar/CamerasCms/a_camerascms.h"
#include "CamerasManager/LeftControlBar/CamerasCms/p_camerascms.h"
#include "CamerasManager/c_camarasmanager.h"
class P_CamerasCMS;
class A_CamerasCMS;
class C_CamerasCMS : public Control {
 public:
  QWidget* zone;
  AppContext* appContext;

  C_CamerasCMS(Control* ctrl, QWidget *_zone);
  C_CamerasManager* getParent() { return (C_CamerasManager*)this->parent; }
  P_CamerasCMS* presentation() { return (P_CamerasCMS*)pres; }
  A_CamerasCMS* abstraction() { return (A_CamerasCMS*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CamerasCMS_H
