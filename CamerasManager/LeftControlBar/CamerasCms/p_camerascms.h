#ifndef P_CamerasCMS_H
#define P_CamerasCMS_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_camerascms.h"
#include <QTextEdit>
#include <QListWidget>
#include "CamerasManager/LeftControlBar/CamerasCms/CameraCmsCell/cameracmscell.h"

class C_CamerasCMS;
class P_CamerasCMS : public Presentation {
    Q_OBJECT
    // init ui control
private:
public:
    QWidget *zone = Q_NULLPTR;
    QVBoxLayout *layout = Q_NULLPTR;

    QWidget *topWidget = Q_NULLPTR;
    QHBoxLayout *topLayout = Q_NULLPTR;

    QWidget *bottomWidget = Q_NULLPTR;
    QVBoxLayout *bottomLayout = Q_NULLPTR;

    //camera cms
    QListWidget *listCamerasCmsWidget = Q_NULLPTR;
    QList<CamItem *> listCamerasCms;
    QList<CameraCmsCell *> listCamerasCellCustom;
    QList<QListWidgetItem *> listWidgetItems;

    P_CamerasCMS(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_CamerasCMS *control() { return (C_CamerasCMS *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();
    void loadListCamerasCms();

public Q_SLOTS:
    void onListCamerasCmsClicked(QListWidgetItem *item);
protected:
};

#endif  // P_CamerasCMS_H
