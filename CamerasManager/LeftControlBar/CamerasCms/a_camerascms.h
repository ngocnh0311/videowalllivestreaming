#ifndef A_CamerasCMS_H
#define A_CamerasCMS_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_camerascms.h"
class C_CamerasCMS;
class A_CamerasCMS : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CamerasCMS(Control *ctrl);
    C_CamerasCMS *control() { return (C_CamerasCMS *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CamerasCMS_H
