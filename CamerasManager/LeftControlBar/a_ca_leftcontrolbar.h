#ifndef A_CALeftControlBar_H
#define A_CALeftControlBar_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_ca_leftcontrolbar.h"
class C_CALeftControlBar;
class A_CALeftControlBar : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CALeftControlBar(Control *ctrl);
    C_CALeftControlBar *control() { return (C_CALeftControlBar *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CALeftControlBar_H
