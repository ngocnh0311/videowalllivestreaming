#ifndef C_CamerasLocal_H
#define C_CamerasLocal_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "CamerasManager/LeftControlBar/CamerasLocal/a_cameraslocal.h"
#include "CamerasManager/LeftControlBar/CamerasLocal/p_cameraslocal.h"
#include "CamerasManager/c_camarasmanager.h"
class P_CamerasLocal;
class A_CamerasLocal;

class C_CamerasLocal : public Control {
 public:
  QWidget* zone;
  AppContext* appContext;

  C_CamerasLocal(Control* ctrl, QWidget *_zone);
  C_CamerasManager* getParent() { return (C_CamerasManager*)this->parent; }
  P_CamerasLocal* presentation() { return (P_CamerasLocal*)pres; }
  A_CamerasLocal* abstraction() { return (A_CamerasLocal*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CamerasLocal_H
