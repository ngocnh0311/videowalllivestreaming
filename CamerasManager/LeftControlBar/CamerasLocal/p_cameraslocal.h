#ifndef P_CamerasLocal_H
#define P_CamerasLocal_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_cameraslocal.h"
#include <QTextEdit>
#include <QCheckBox>
#include <QListWidget>
#include "CamerasManager/LeftControlBar/CamerasLocal/CameraLocalCell/cameralocalcell.h"
class C_CamerasLocal;

class P_CamerasLocal : public Presentation {
    Q_OBJECT
    // init ui control
private:
public:
    QWidget *zone = Q_NULLPTR;
    QVBoxLayout *layout = Q_NULLPTR;

    QWidget *topWidget = Q_NULLPTR;
    QHBoxLayout *topLayout = Q_NULLPTR;

    QWidget *bottomWidget = Q_NULLPTR;
    QVBoxLayout *bottomLayout = Q_NULLPTR;

    //camera local
    QListWidget *listIpCamerasLocalWidget = Q_NULLPTR;

    QList<CamItem *> listIpCamerasLocal;
    QList<CameraLocalCell *> listCamerasCellCustom;
    QList<QListWidgetItem *> listWidgetItems;
    P_CamerasLocal(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_CamerasLocal *control() { return (C_CamerasLocal *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

    void loadListCamerasLocal();

protected:
public Q_SLOTS:
    void onListCamerasLocalClicked(QListWidgetItem *item);
};

#endif  // P_CamerasLocal_H
