#ifndef A_CamerasLocal_H
#define A_CamerasLocal_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_cameraslocal.h"
class C_CamerasLocal;
class A_CamerasLocal : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CamerasLocal(Control *ctrl);
    C_CamerasLocal *control() { return (C_CamerasLocal *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CamerasLocal_H
