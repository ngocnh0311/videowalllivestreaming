#ifndef CameraLocalCell_H
#define CameraLocalCell_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QCheckBox>
class CameraLocalCell: public QWidget
{
    Q_OBJECT
public:
    QString ipDeviceCurrent;
//    int widthLeft = 40;
//    int widthCenter = 350;
//    int widthRight = 250;
    QVBoxLayout *mainLayout = Q_NULLPTR;

    QHBoxLayout *containtLayout;
    QWidget *rightWidget = Q_NULLPTR;
    QHBoxLayout *rightLayout = Q_NULLPTR;

    QWidget *centerWidget = Q_NULLPTR;
    QVBoxLayout *centerLayout = Q_NULLPTR;

    QWidget *leftWidget = Q_NULLPTR;
    QVBoxLayout *leftLayout = Q_NULLPTR;
    QLabel *iconLeftLabel = Q_NULLPTR;

    QWidget *topWidget = Q_NULLPTR;
    QVBoxLayout *topLayout = Q_NULLPTR;

    QWidget *bottomWidget = Q_NULLPTR;


    void updateBackground(QString background, QString color);
    CameraLocalCell(QWidget *parent, QString _ipDevice, bool selected = false);

protected:
    virtual void resizeEvent(QResizeEvent *event);
public Q_SLOTS:
    void addCameraClicked();
Q_SIGNALS:
};

#endif // CameraLocalCell_H
