#include "p_ca_leftcontrolbar.h"
#include "Common/resources.h"
/**
     * Generic method to override for updating the presention.
     **/

P_CALeftControlBar::P_CALeftControlBar(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    // init gui object
    this->zone = _zone;
    // init work space
    layout = new QVBoxLayout();
    layout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
    layout->setSpacing(0);
    layout->setMargin(0);
    zone->setLayout(layout);

    camerasSelectedWidget = new QWidget(this->zone);
    layout->addWidget(camerasSelectedWidget);
    camerasSelectedWidget->resize(this->zone->width() , this->zone->height() );


    QPushButton *searchCamerasButton = new QPushButton(this->zone);
    searchCamerasButton->setFont(Resources::instance().getMediumBoldButtonFont());
    searchCamerasButton->setFixedHeight(40);
    searchCamerasButton->setText("Search Cameras");
    connect(searchCamerasButton, &QPushButton::clicked, this, &P_CALeftControlBar::searchCamerasButtonsClicked);
    layout->addWidget(searchCamerasButton);

    //    camerasCmsWidget = new QWidget(this->zone);
    //    camerasLocalWidget = new QWidget(this->zone);

    //    camerasCmsWidget->resize(this->zone->width() , 0);
    //    camerasLocalWidget->resize(this->zone->width() , 0);

    //    layout->addWidget(camerasCmsWidget);
    //    layout->addWidget(camerasLocalWidget);
}

void P_CALeftControlBar::searchCamerasButtonsClicked(){
    control()->newUserAction(Message.SHOW_FORM_SEARCH_CAMERA_IN_NETWORK, Q_NULLPTR);
}

void P_CALeftControlBar::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CALeftControlBar::update() {}

QWidget *P_CALeftControlBar::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return camerasSelectedWidget;
    default:
        return Q_NULLPTR;
    }
}

void P_CALeftControlBar::enterFullscreenMode() {/* rightBar->setFixedWidth(0);*/ }

void P_CALeftControlBar::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
