#ifndef P_CameraSelected_H
#define P_CameraSelected_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_camerasselected.h"
#include <QTextEdit>
#include <QListWidget>
#include <QCheckBox>
#include "CamerasManager/LeftControlBar/CamerasSelected/CameraSelectedCell/cameraselectedcell.h"

class C_CameraSelected;
class P_CameraSelected : public Presentation {
    Q_OBJECT
    // init ui control
private:
public:
    QWidget *zone = Q_NULLPTR;
    QVBoxLayout *layout = Q_NULLPTR;

    int heightTop = 50;
    QWidget *topWidget = Q_NULLPTR;
    QHBoxLayout *topLayout = Q_NULLPTR;

    QWidget *bottomWidget = Q_NULLPTR;
    QVBoxLayout *bottomLayout = Q_NULLPTR;

    //app selected
    QListWidget *listCamerasSelectedWidget = Q_NULLPTR;
    QList<CamItem *> listCamerasSelected;
    QList<CameraSelectedCell *> listCamerasCellCustom;
    QList<QListWidgetItem *> listWidgetItems;

    P_CameraSelected(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_CameraSelected *control() { return (C_CameraSelected *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();

    void loadListCamerasSelected();

protected:
public Q_SLOTS:
    void onListCamerasSelectedClicked(QListWidgetItem *item);
};

#endif  // P_CameraSelected_H
