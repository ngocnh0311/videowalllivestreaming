#include "p_camerasselected.h"
#include "Common/resources.h"
/**
     * Generic method to override for updating the presention.
     **/

P_CameraSelected::P_CameraSelected(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    // init gui object
    this->zone = _zone;
    // init work space
    layout = new QVBoxLayout();
    layout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
    layout->setSpacing(0);
    layout->setMargin(0);
    zone->setLayout(layout);

    topWidget = new QWidget(this->zone);
    topWidget->setFixedHeight(0);
    topWidget->setStyleSheet("background:#5f5f5f");
    topLayout = new QHBoxLayout();
    topLayout->setSpacing(15);
    topLayout->setMargin(0);
    topWidget->setLayout(topLayout);


    bottomWidget = new QWidget(this->zone);
    bottomLayout = new QVBoxLayout();
    bottomLayout->setSpacing(15);
    bottomLayout->setMargin(0);
    bottomWidget->setLayout(bottomLayout);


    QWidget *leftCamerasLocalWidget = new QWidget(topWidget);
    QVBoxLayout *leftCamerasLocalLayout = new QVBoxLayout();
    leftCamerasLocalLayout->setMargin(0);
    leftCamerasLocalLayout->setSpacing(0);
    leftCamerasLocalWidget->setLayout(leftCamerasLocalLayout);

    QLabel *cameraCmsTitle = new QLabel(leftCamerasLocalWidget);
    cameraCmsTitle->setFont(Resources::instance().getMediumRegularButtonFont());
    cameraCmsTitle->setText("Cameras Selected: ");
    leftCamerasLocalLayout->addWidget(cameraCmsTitle);


    QWidget *rightCamerasLocalWidget = new QWidget(topWidget);
    rightCamerasLocalWidget->setFixedSize(0,0);
    QVBoxLayout *rightCamerasLocalLayout = new QVBoxLayout();
    rightCamerasLocalLayout->setMargin(0);
    rightCamerasLocalLayout->setSpacing(0);
    rightCamerasLocalWidget->setLayout(rightCamerasLocalLayout);

    QPushButton *removeAllButton = new QPushButton(rightCamerasLocalWidget);
    removeAllButton->setFixedSize(80,25);
    removeAllButton->setFont(Resources::instance().getMediumRegularButtonFont());
    removeAllButton->setText("Remove All");
    rightCamerasLocalLayout->addWidget(removeAllButton);

    topLayout->addWidget(leftCamerasLocalWidget);
    topLayout->addWidget(rightCamerasLocalWidget);

    QWidget *bottomContainCamerasLocalWidget = new QWidget(bottomWidget);
    QVBoxLayout *bottomContainCamerasLocalLayout = new QVBoxLayout();
    bottomContainCamerasLocalLayout->setMargin(0);
    bottomContainCamerasLocalLayout->setSpacing(0);
    bottomContainCamerasLocalWidget->setLayout(bottomContainCamerasLocalLayout);
    bottomLayout->addWidget(bottomContainCamerasLocalWidget);

    listCamerasSelectedWidget = new QListWidget(bottomContainCamerasLocalWidget);
    listCamerasSelectedWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    listCamerasSelectedWidget->setFixedWidth(appSize.leftWidth);
    bottomContainCamerasLocalLayout->addWidget(listCamerasSelectedWidget);
    connect(listCamerasSelectedWidget, &QListWidget::itemClicked , this, &P_CameraSelected::onListCamerasSelectedClicked);
    layout->addWidget(topWidget);
    layout->addWidget(bottomWidget);
}


void P_CameraSelected::loadListCamerasSelected(){


    QList<QListWidgetItem*> items = listCamerasSelectedWidget->selectedItems();

    for (int index = 0; index < listWidgetItems.size(); ++index) {
        QListWidgetItem *item = listCamerasSelectedWidget->takeItem(index);
        delete item; // Qt documentation warnings you to destroy item to effectively remove it from QListWidget.
    }

    for (int index  = 0; index < listCamerasCellCustom.size(); ++index) {
        CameraSelectedCell* cameraSelectedCell= listCamerasCellCustom.takeAt(index);
        delete cameraSelectedCell;
    }
    listCamerasSelectedWidget->clear();
    listCamerasCellCustom.clear();
    listWidgetItems.clear();
    qDebug() << "listCamerasCellCustom SIze " <<listCamerasCellCustom.size();

    CamSite *camSite = this->control()->appContext->getSiteCameras();
    if(camSite!= Q_NULLPTR){
        QList<CamItem *> listCamItems = camSite->getCamItems();
        for (int index = 0; index < listCamItems.size(); ++index) {
            CamItem *camItem = listCamItems.at(index);
            CameraSelectedCell *cameraSelectedCell= new CameraSelectedCell(listCamerasSelectedWidget, camItem);

            if(index == 0){
                cameraSelectedCell->updateBackground("#555" , "#FFF");
            }

            listCamerasCellCustom.append(cameraSelectedCell);
            QListWidgetItem* listItem = new QListWidgetItem();
            listWidgetItems.append(listItem);
            listItem->setSizeHint(cameraSelectedCell->sizeHint());
            listCamerasSelectedWidget->addItem(listItem);
            listCamerasSelectedWidget->setItemWidget(listItem, cameraSelectedCell);
        }
    }
    if(listWidgetItems.size() > 0){
        QListWidgetItem *itemFirst = listWidgetItems.at(0);
        onListCamerasSelectedClicked(itemFirst);
    }

}


void P_CameraSelected::onListCamerasSelectedClicked(QListWidgetItem *item){
    if(item){
        int indexItemSelected = listWidgetItems.indexOf(item);
        qDebug() <<Q_FUNC_INFO << "indexItemSelected" <<indexItemSelected;

        //reset color of cell before
        for (int index = 0; index < listWidgetItems.size(); ++index) {
            CameraSelectedCell *cameraSelectedCell = listCamerasCellCustom.at(index);
            cameraSelectedCell->updateBackground("#333", "#ddd");
        }

        CameraSelectedCell *cameraSelectedCell = listCamerasCellCustom.at(indexItemSelected);
        cameraSelectedCell->updateBackground("#555" , "#FFF");

        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<CamItem *>(cameraSelectedCell->getCamItemCurrent());
        control()->newUserAction(Message.APP_CAMERAS_CAMERA_SELECTED, dataStruct);
    }
}

void P_CameraSelected::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CameraSelected::update() {}

QWidget *P_CameraSelected::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return this->zone;
    default:
        return Q_NULLPTR;
    }
}

void P_CameraSelected::enterFullscreenMode() {/* rightBar->setFixedWidth(0);*/ }

void P_CameraSelected::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
