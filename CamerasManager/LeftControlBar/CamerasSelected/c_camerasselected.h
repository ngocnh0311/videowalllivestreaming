#ifndef C_CameraSelected_H
#define C_CameraSelected_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "CamerasManager/LeftControlBar/CamerasSelected/a_camerasselected.h"
#include "CamerasManager/LeftControlBar/CamerasSelected/p_camerasselected.h"
#include "CamerasManager/LeftControlBar/c_ca_leftcontrolbar.h"
class P_CameraSelected;
class A_CameraSelected;

class C_CameraSelected : public Control {
 public:
  bool loadFirst = true;
  QWidget* zone;
  AppContext* appContext;

  C_CameraSelected(Control* ctrl, QWidget *_zone);
  C_CALeftControlBar* getParent() { return (C_CALeftControlBar*)this->parent; }
  P_CameraSelected* presentation() { return (P_CameraSelected*)pres; }
  A_CameraSelected* abstraction() { return (A_CameraSelected*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CameraSelected_H
