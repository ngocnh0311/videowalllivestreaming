#include "c_camerasselected.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_CameraSelected::C_CameraSelected(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();
    this->zone = _zone;

    this->abst = new A_CameraSelected(this);
    this->pres = new P_CameraSelected(this, zone);

    // create others controls
    // cXXX = new C_XXX();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CameraSelected::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_CAMERAS_CAMERA_SELECTED:{
        newAction(message, attachment);
    }    break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CameraSelected::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CameraSelected::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_SHOW:{
        if(loadFirst){
            presentation()->loadListCamerasSelected();
            loadFirst = false;
        }
    }break;

    case Message.APP_CAMERAS_CAMERA_SELECTED:{
        getParent()->newAction(message, attachment);
    }    break;

    case Message.APP_CONTEXT_GET:
        attachment->setValue(appContext);
        break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
