#ifndef A_CameraSelected_H
#define A_CameraSelected_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_camerasselected.h"
class C_CameraSelected;
class A_CameraSelected : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CameraSelected(Control *ctrl);
    C_CameraSelected *control() { return (C_CameraSelected *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CameraSelected_H
