#ifndef CameraSelectedCell_H
#define CameraSelectedCell_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QCheckBox>
#include "Camera/camitem.h"
class CameraSelectedCell: public QWidget
{
    Q_OBJECT
    CamItem *camItemCurrent = Q_NULLPTR;
public:
    int widthLeft = 40;
    //    int widthCenter = 350;
    int widthRight = 100;
    QVBoxLayout *mainLayout = Q_NULLPTR;

    QHBoxLayout *containtLayout;
    QWidget *rightWidget = Q_NULLPTR;
    QHBoxLayout *rightLayout = Q_NULLPTR;

    QWidget *centerWidget = Q_NULLPTR;
    QHBoxLayout *centerLayout = Q_NULLPTR;

    QWidget *leftWidget = Q_NULLPTR;
    QVBoxLayout *leftLayout = Q_NULLPTR;
    QLabel *iconLeftLabel = Q_NULLPTR;

    QWidget *topWidget = Q_NULLPTR;
    QVBoxLayout *topLayout = Q_NULLPTR;

    QWidget *bottomWidget = Q_NULLPTR;


    void updateBackground(QString background, QString color);
    CameraSelectedCell(QWidget *parent, CamItem *_camItemCurrent, bool selected = false);

    CamItem *getCamItemCurrent() const;

protected:
    virtual void resizeEvent(QResizeEvent *event);
public Q_SLOTS:
    void removeCameraClicked();
Q_SIGNALS:
};

#endif // CameraSelectedCell_H
