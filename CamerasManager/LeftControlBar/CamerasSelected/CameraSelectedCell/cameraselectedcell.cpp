#include "cameraselectedcell.h"
#include "Common/resources.h"
CameraSelectedCell::CameraSelectedCell(QWidget *parent, CamItem *_camItemCurrent, bool selected)
{
    this->camItemCurrent = _camItemCurrent;
    mainLayout = new QVBoxLayout();
    mainLayout->setAlignment(Qt::AlignTop);
    mainLayout->setSpacing(0);
    mainLayout->setMargin(0);
    this->setLayout(mainLayout);

    topWidget = new QWidget(this);
    bottomWidget = new QWidget(this);
    bottomWidget->setFixedHeight(0);
    bottomWidget->setStyleSheet("background-color: #333");

    mainLayout->addWidget(topWidget);
    mainLayout->addWidget(bottomWidget);

    containtLayout = new QHBoxLayout();
    this->topWidget->setLayout(containtLayout);
    containtLayout->setMargin(0);
    containtLayout->setSpacing(0);
    containtLayout->setAlignment(Qt::AlignLeft);
    this->setStyleSheet("background-color: #333; color: #ddd;");

    //init left widget
    leftWidget = new QWidget(topWidget);
    leftWidget->setStyleSheet("color:#00a45e");
    leftWidget->setFixedWidth(widthLeft);
    leftLayout = new QVBoxLayout();
    leftLayout->setAlignment(Qt::AlignCenter);
    leftWidget->setLayout(leftLayout);

    iconLeftLabel = new QLabel(leftWidget);
    iconLeftLabel->setStyleSheet("border-right:none;");
    iconLeftLabel->setFont(Resources::instance().getExtraLargeRegularButtonFont());
    //    QPixmap iconLeftPixMap(":/images/res/icon-road.png");
    //    iconLeftLabel->setPixmap(iconLeftPixMap.scaled(25,25,Qt::KeepAspectRatio));
    QString iconCamera = "";
    iconLeftLabel->setText(iconCamera);
    leftLayout->addWidget(iconLeftLabel);

    //init center
    centerWidget = new QWidget(topWidget);
    centerWidget->setStyleSheet("color:#fff");
    //    centerWidget->setFixedWidth(widthCenter);

    centerLayout = new QHBoxLayout();
    centerLayout->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    centerLayout->setSpacing(0);
    centerLayout->setContentsMargins(5,0,10,0);
    centerWidget->setLayout(centerLayout);


//    QLabel *ipDeviceTitleLabel = new QLabel(centerWidget);
//    ipDeviceTitleLabel->setAlignment(Qt::AlignLeft);
//    ipDeviceTitleLabel->setFixedWidth(80);
//    ipDeviceTitleLabel->setFont(Resources::instance().getMediumRegularButtonFont());
//    ipDeviceTitleLabel->setText("Ip Device:");
    //ip device
    QLabel *ipDeviceLabel = new QLabel(centerWidget);
    ipDeviceLabel->setFont(Resources::instance().getMediumRegularButtonFont());
    ipDeviceLabel->setAlignment(Qt::AlignLeft);
    ipDeviceLabel->setText(camItemCurrent->getPostion());

//    centerLayout->addWidget(ipDeviceTitleLabel);
    centerLayout->addWidget(ipDeviceLabel);


    //    //init right widget
    rightWidget = new QWidget(topWidget);
    rightWidget->setFixedWidth(0);
    rightLayout = new QHBoxLayout();
    rightLayout->setSpacing(0);
    rightLayout->setMargin(0);
    rightLayout->setAlignment(Qt::AlignCenter);
    rightWidget->setLayout(rightLayout);

    QPushButton *removeCamButton = new QPushButton(rightWidget);
    removeCamButton->setFixedSize(80,25);
    removeCamButton->setFont(Resources::instance().getMediumRegularButtonFont());
    removeCamButton->setText("Remove");
    rightLayout->addWidget(removeCamButton);
    connect(removeCamButton, &QPushButton::clicked, this, &CameraSelectedCell::removeCameraClicked);

    //resize widget
    leftWidget->move(0,0);
    centerWidget->move(widthLeft , 0);
    rightWidget->move(parent->size().width() - rightWidget->size().width(), 0);

    qDebug() << "parent->size().width() - rightWidget->size().width()" << parent->size().width() - rightWidget->size().width();
    //add widget

    containtLayout->addWidget(leftWidget);
    containtLayout->addWidget(centerWidget);
    containtLayout->addWidget(rightWidget);
}

CamItem *CameraSelectedCell::getCamItemCurrent() const
{
    return camItemCurrent;
}

void CameraSelectedCell::updateBackground(QString background, QString color){
    this->setStyleSheet("background-color : " + background +";" + "color:" + color);
    //update icon left if need
}

void CameraSelectedCell::removeCameraClicked(){
    qDebug() << Q_FUNC_INFO;
}

void CameraSelectedCell::resizeEvent(QResizeEvent *event){
    Q_UNUSED(event)
    //resize widget
    //    qDebug() << Q_FUNC_INFO << "SIZE CAMERA CELL" << this->size() << this->sizeHint();

}
