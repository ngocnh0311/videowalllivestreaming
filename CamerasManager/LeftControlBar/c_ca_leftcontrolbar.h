#ifndef C_CALeftControlBar_H
#define C_CALeftControlBar_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "CamerasManager/LeftControlBar/p_ca_leftcontrolbar.h"
#include "CamerasManager/LeftControlBar/a_ca_leftcontrolbar.h"
#include "CamerasManager/c_camarasmanager.h"
#include "CamerasCms/c_camerascms.h"
#include "CamerasLocal/c_cameraslocal.h"
#include "CamerasSelected/c_camerasselected.h"

class P_CALeftControlBar;
class A_CALeftControlBar;

class C_CALeftControlBar : public Control {
public:
    QWidget* zone;
    AppContext* appContext;

    C_CameraSelected *cCameraSelected = Q_NULLPTR;
    C_CamerasCMS *cCamerasCMS = Q_NULLPTR;
    C_CamerasLocal *cCamerasLocal = Q_NULLPTR;
    C_CALeftControlBar(Control* ctrl, QWidget *_zone);
    C_CamerasManager* getParent() { return (C_CamerasManager*)this->parent; }
    P_CALeftControlBar* presentation() { return (P_CALeftControlBar*)pres; }
    A_CALeftControlBar* abstraction() { return (A_CALeftControlBar*)abst; }
    /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
    void newUserAction(int message, QVariant* attachment) override;
    /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newSystemAction(int message, QVariant* attachment) override;

    /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CALeftControlBar_H
