#include "c_camarasmanager.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_CamerasManager::C_CamerasManager(Control* ctrl, QWidget *_zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();
    this->zone = _zone;

    this->abst = new A_CamerasManager(this);
    this->pres = new P_CamerasManager(this, zone);

    cLeftControlBar = new C_CALeftControlBar(this, presentation()->getZone(1));
    cWorkspace = new C_CAWorkspace(this, presentation()->getZone(2));
    cOnvifSearchCameras = new C_OnvifClient(this, presentation()->getZone(3));

    // create others controls
    // cXXX = new C_XXX();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CamerasManager::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case 1:
        break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CamerasManager::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_CamerasManager::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_CAMERAS_CAMERA_SELECTED:{
        cWorkspace->newAction(message, attachment);
    }    break;

    case Message.SHOW_FORM_SEARCH_CAMERA_IN_NETWORK:{
       presentation()->showSearchCamerasWidget();
    }break;

    case Message.APP_SHOW:{
        this->appContext->setWorkingApp("Cameras");
        cLeftControlBar->newAction(message, attachment);
    }break;

    case Message.APP_CONTEXT_GET:
        attachment->setValue(appContext);
        break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
