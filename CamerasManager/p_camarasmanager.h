#ifndef P_CamerasManager_H
#define P_CamerasManager_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_camarasmanager.h"
#include <QTextEdit>
#include <QCheckBox>
class C_CamerasManager;

class P_CamerasManager : public Presentation {
    // init ui control
private:
public:
    QWidget *zone = Q_NULLPTR;
    QHBoxLayout *layout = Q_NULLPTR;
    QWidget *lefttBar = Q_NULLPTR;
    QWidget *workSpace = Q_NULLPTR;

    QWidget *searchCamerasWidget = Q_NULLPTR;

    P_CamerasManager(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);
    float rightBarPercent = 0.2;

    C_CamerasManager *control() { return (C_CamerasManager *)ctrl; }

    void enterFullscreenMode();
    void exitFullscreenMode();
    void showSearchCamerasWidget();
    void hideSearchCamerasWidget();

protected:
};

#endif  // P_CamerasManager_H
