#include "p_camarasmanager.h"

/**
     * Generic method to override for updating the presention.
     **/

P_CamerasManager::P_CamerasManager(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    // init gui object
    this->zone = _zone;
    QRect screenGeometry = QApplication::desktop()->availableGeometry(this->zone);
    int withZone = screenGeometry.width();
    int heightZone = screenGeometry.height();
    this->zone->resize(withZone, heightZone );

    QSize screenSize = Resources::instance().getScreenSize();
    workSpace = new QWidget(zone);
    workSpace->setStyleSheet("background:#555");

    lefttBar = new QWidget(zone);
    lefttBar->setStyleSheet("background:#333");
    lefttBar->setFixedWidth(appSize.leftWidth);

    // init work space
    layout = new QHBoxLayout();
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->setAlignment(Qt::AlignLeft);
    zone->setLayout(layout);

    layout->addWidget(lefttBar);
    layout->addWidget(workSpace);

    searchCamerasWidget = new QWidget(Q_NULLPTR , Qt::WindowStaysOnTopHint);
    this->searchCamerasWidget->resize(screenSize.width() * 0.6f, screenSize.height() * 0.6f);
    this->searchCamerasWidget->move((screenSize.width() - this->searchCamerasWidget->width()) / 2,
                                    (screenSize.height() - this->searchCamerasWidget->height()) / 2);
    hideSearchCamerasWidget();
}

void P_CamerasManager::showSearchCamerasWidget(){
    searchCamerasWidget->setWindowFlag(Qt::WindowStaysOnTopHint);
    searchCamerasWidget->setWindowState(Qt::WindowActive) ;
    searchCamerasWidget->show();
    searchCamerasWidget->raise();

}

void P_CamerasManager::hideSearchCamerasWidget(){
    searchCamerasWidget->hide();
    searchCamerasWidget->lower();
}


void P_CamerasManager::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CamerasManager::update() {}

QWidget *P_CamerasManager::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return lefttBar;
    case 2:
        return workSpace;
    case 3:
        return searchCamerasWidget;
    default:
        return Q_NULLPTR;
    }
}

void P_CamerasManager::enterFullscreenMode() {/* rightBar->setFixedWidth(0);*/ }

void P_CamerasManager::exitFullscreenMode() {
    //    rightBar->setFixedWidth(0);
}
