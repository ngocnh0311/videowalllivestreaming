#ifndef C_CamerasManager_H
#define C_CamerasManager_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "CamerasManager/CamerasWorkspace/c_ca_workspace.h"
#include "CamerasManager/p_camarasmanager.h"
#include "CamerasManager/a_camarasmanager.h"
#include "CamerasManager/LeftControlBar/c_ca_leftcontrolbar.h"
#include "MainFrame/c_mainframe.h"
class P_CamerasManager;
class A_CamerasManager;
class C_MainFrame;
class C_TopControlBar;

class C_CamerasManager : public Control {
public:
    QWidget* zone;
    AppContext* appContext;
    QString appName = "Cameras";
    C_CALeftControlBar *cLeftControlBar = Q_NULLPTR;
    C_CAWorkspace *cWorkspace = Q_NULLPTR;
    C_OnvifClient *cOnvifSearchCameras = Q_NULLPTR;

    C_CamerasManager(Control* ctrl, QWidget *_zone);
    C_MainFrame* getParent() { return (C_MainFrame*)this->parent; }
    P_CamerasManager* presentation() { return (P_CamerasManager*)pres; }
    A_CamerasManager* abstraction() { return (A_CamerasManager*)abst; }
    /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
    void newUserAction(int message, QVariant* attachment) override;
    /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newSystemAction(int message, QVariant* attachment) override;

    /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newAction(int message, QVariant* attachment) override;
};

#endif  // C_CamerasManager_H
