#ifndef CAMERASMANAGEMENTUTILS_H
#define CAMERASMANAGEMENTUTILS_H
#include <QObject>
#include "Camera/camsite.h"
#include "Authentication/appcontext.h"
class CamerasManagementUtils: public QObject
{
public:

    AppContext *appContext = Q_NULLPTR;
    static CamerasManagementUtils* instance(){
        static CamerasManagementUtils *camerasManagementUtils = Q_NULLPTR;
        if(camerasManagementUtils == Q_NULLPTR){
            camerasManagementUtils = new CamerasManagementUtils();
        }
        return camerasManagementUtils;
    }

    CamerasManagementUtils();
    CamerasManagementUtils(CamerasManagementUtils const &) = delete;
    CamerasManagementUtils(CamerasManagementUtils &&) = delete;
    CamerasManagementUtils &operator=(CamerasManagementUtils const &) = delete;
    CamerasManagementUtils &operator=(CamerasManagementUtils &&) = delete;
    AppContext *getAppContext() const;
    void setAppContext(AppContext *value);
    void addCameras(CamItem *camItem);
    void removeCameras(CamItem *CamItem);
    CamItem *createCam(QJsonObject jsonObject);
    QJsonObject *createCamJson();

};

#endif // CAMERASMANAGEMENTUTILS_H
