#ifndef A_CamerasManager_H
#define A_CamerasManager_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_camarasmanager.h"
class C_CamerasManager;
class A_CamerasManager : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CamerasManager(Control *ctrl);
    C_CamerasManager *control() { return (C_CamerasManager *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CamerasManager_H
