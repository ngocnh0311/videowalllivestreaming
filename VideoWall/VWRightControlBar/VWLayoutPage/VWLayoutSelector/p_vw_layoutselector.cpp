#include "p_vw_layoutselector.h"
#include "Common/LayoutSet.h"

P_VWLayoutSelector::P_VWLayoutSelector(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    zone->setStyleSheet("background-color: #222");

    layoutSet = new LayoutSet(Q_NULLPTR);

    layout = new QHBoxLayout();
    layout->setMargin(0);
    layout->setSpacing(5);
    zone->setLayout(layout);

    for (int index = 0; (index < layoutSet->layoutList.size()) &&
         (layoutSet->layoutList.at(index).numberOfCameras <=
          Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS);
         ++index) {
        QPushButton *button = new QPushButton(zone);
        QFont font = button->font();
        font.setPixelSize(38);
        button->setFont(font);
        button->setText(layoutSet->layoutList.at(index).label);
        button->setStyleSheet("background-color: black; color: #b3b3b3;");
        button->setMinimumHeight(button->width());
        buttons.append(button);
        layout->addWidget(button);
        connect(button, &QPushButton::clicked, this,
                &P_VWLayoutSelector::onSelectedLayout);
    }
    waitingTimer = new QTimer();
}

void P_VWLayoutSelector::onSelectedLayout() {
    QPushButton *button = qobject_cast<QPushButton *>(sender());
    if (button != Q_NULLPTR) {
        int clickedButtonIndex = buttons.indexOf(button);
        if (clickedButtonIndex >= 0 && clickedButtonIndex < buttons.size()) {
            if (selectedButtonIndex != clickedButtonIndex) {
                clearOldSelectedButton(this->selectedButtonIndex);
                selectedButtonIndex = clickedButtonIndex;
                highlightNewSelectedButton(clickedButtonIndex);
            }
        }
        lastButton = button;
        if (!isWaiting) {
            startWaiting();
        } else {
            isWaiting = false;
            waitingTimer->stop();
            disconnect(waitingTimer, &QTimer::timeout, this,
                       &P_VWLayoutSelector::onEndWaiting);
            startWaiting();
        }
    }
}

void P_VWLayoutSelector::startWaiting() {
    if (!isWaiting) {
        isWaiting = true;
        connect(waitingTimer, &QTimer::timeout, this,
                &P_VWLayoutSelector::onEndWaiting);
        waitingTimer->start(500);
    }
}

void P_VWLayoutSelector::onEndWaiting() {
    isWaiting = false;
    if (lastButton != Q_NULLPTR) {
        int indexOfButton = buttons.indexOf(lastButton);
        this->selectedButtonIndex = indexOfButton;

        if(this->control()->appContext->getIsLoadDataWithDeviceId()){
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<LayoutStruct>(layoutSet->layoutList.at(indexOfButton));
            control()->newUserAction(Message.APP_VIDEO_WALL_ZONE_LAYOUT_SELECTED_BY_HAND,
                                     dataStruct);
        }else{
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<LayoutStruct>(layoutSet->layoutList.at(indexOfButton));
            control()->newUserAction(Message.APP_VIDEO_WALL_ZONE_LAYOUT_SELECTED,
                                     dataStruct);
        }
    }
    waitingTimer->stop();
    disconnect(waitingTimer, &QTimer::timeout, this,
               &P_VWLayoutSelector::onEndWaiting);
}

void P_VWLayoutSelector::clearOldSelectedButton(int buttonIndex) {
    if (buttonIndex >= 0 && buttonIndex < buttons.size()) {
        buttons.at(buttonIndex)
                ->setStyleSheet("background-color: black; color: #b3b3b3;");
    }
}

void P_VWLayoutSelector::highlightNewSelectedButton(int buttonIndex) {
    if (buttonIndex >= 0 && buttonIndex < buttons.size()) {
        buttons.at(buttonIndex)
                ->setStyleSheet("background-color: red; color: #b3b3b3;");
    }
}

void P_VWLayoutSelector::calibrateLayoutSet() {
    CamSite *camSite = control()->appContext->getSiteCameras();
    if (camSite != Q_NULLPTR) {
        int indexLayoutActiveLast = - 1;

        int numberOfCamerasOfSite;
        QSettings settings;
        QString offlineMode = settings.value("offline_mode").toString();
        if(offlineMode == "ON"){
            numberOfCamerasOfSite = camSite->getCamItems().size();
        }else{
            numberOfCamerasOfSite = camSite->getTotalCamItem();
        }
        for (int index = 0; (index < layoutSet->layoutList.size()) &&
             (layoutSet->layoutList.at(index).numberOfCameras <=
              Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) && (index < buttons.size());
             ++index) {
            if (layoutSet->layoutList.at(index).numberOfCameras <
                    numberOfCamerasOfSite) {
                buttons.at(index)->setEnabled(true);
                buttons.at(index)->setStyleSheet(
                            "background-color: black; color: #b3b3b3;");
                indexLayoutActiveLast = index;
            } else {
                buttons.at(index)->setDisabled(true);
                buttons.at(index)->setStyleSheet(
                            "background-color: black; color: #444;");
            }
        }
        if(indexLayoutActiveLast >= 0 && (indexLayoutActiveLast+1) < buttons.size()){
            buttons.at(indexLayoutActiveLast+1)->setEnabled(true);
            buttons.at(indexLayoutActiveLast+1)->setStyleSheet(
                        "background-color: black; color: #b3b3b3;");
        }
    }
}

//void P_VWLayoutSelector::calibrateLayoutSetRemoteControl() {
//    CamSite *camSite = control()->appContext->getSiteCameras();
//    if (camSite != Q_NULLPTR) {
//        int numberOfCamerasOfSite = camSite->getCamItems().size();
//        qDebug() << "calibrateLayoutSetRemoteControl" << "numberOfCamerasOfSite" << numberOfCamerasOfSite;
//        for (int index = 0; (index < layoutSet->layoutList.size()) &&
//             (layoutSet->layoutList.at(index).numberOfCameras <=
//              Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS);
//             ++index) {
//            qDebug() << "layout numberOfCameras" << layoutSet->layoutList.at(index).numberOfCameras << numberOfCamerasOfSite;
//            if (layoutSet->layoutList.at(index).numberOfCameras <
//                    numberOfCamerasOfSite) {
//                buttons.at(index)->setEnabled(true);
//                buttons.at(index)->setStyleSheet(
//                            "background-color: black; color: #b3b3b3;");
//            } else {
//                buttons.at(index)->setDisabled(true);
//                buttons.at(index)->setStyleSheet(
//                            "background-color: black; color: #444;");
//            }
//        }
//    }
//}



void P_VWLayoutSelector::calibrateLayoutSetRemoteControl() {
    CamSite *camSite = control()->appContext->getSiteCameras();
    if (camSite != Q_NULLPTR) {

        int indexLayoutActiveLast = -1;
        int maxCameraDisplay = camSite->getMaxCameraDisplay();
        int totalCameraOfDevice = camSite->getCamItems().size();
        int layoutCamerasMin = qMin(maxCameraDisplay , totalCameraOfDevice);
        for (int index = 0; (index < layoutSet->layoutList.size()) &&
             (layoutSet->layoutList.at(index).numberOfCameras <=
              Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS)  && (index < buttons.size());
             ++index) {
            if (layoutSet->layoutList.at(index).numberOfCameras <=
                    layoutCamerasMin) {
                buttons.at(index)->setEnabled(true);
                buttons.at(index)->setStyleSheet(
                            "background-color: black; color: #b3b3b3;");
                indexLayoutActiveLast = index;
            } else {
                buttons.at(index)->setDisabled(true);
                buttons.at(index)->setStyleSheet(
                            "background-color: black; color: #444;");
            }
        }
        if(indexLayoutActiveLast >= 0){
            LayoutStruct layoutLast = layoutSet->layoutList.at(indexLayoutActiveLast);
            int indexLayoutNext = indexLayoutActiveLast+1;
            if((layoutLast.numberOfCameras < maxCameraDisplay) && (layoutLast.numberOfCameras < totalCameraOfDevice) && (indexLayoutNext) < buttons.size()){
                LayoutStruct layoutNext = layoutSet->layoutList.at(indexLayoutNext);
                if(layoutNext.numberOfCameras <= maxCameraDisplay){
                    buttons.at(indexLayoutNext)->setEnabled(true);
                    buttons.at(indexLayoutNext)->setStyleSheet(
                                "background-color: black; color: #b3b3b3;");
                }
            }
        }
    }
}
void P_VWLayoutSelector::updateLayoutRemoteControl(LayoutStruct layoutStruct){
    if(layoutStruct.numberOfCameras > 25) return;
    qDebug() << Q_FUNC_INFO <<"updateLayoutRemoteControl " << layoutStruct.numberOfCameras << layoutStruct.selectedPage;
    int indexButtonSelected = layoutSet->getLayoutWithNumberOfCameras(layoutStruct.numberOfCameras).code;
    clearOldSelectedButton(this->selectedButtonIndex);
    calibrateLayoutSetRemoteControl();
    highlightNewSelectedButton(indexButtonSelected);
    this->selectedButtonIndex = indexButtonSelected;

    QVariant *data = new QVariant();
    data->setValue<LayoutStruct>(layoutStruct);
    control()->newUserAction(Message.APP_VIDEO_WALL_PAGE_UPDATE_REMOTE_CONTROL, data);
}



void P_VWLayoutSelector::loadLayoutOfWorkspace(QVariant *attachment){
    WindowsApp *selectedwindowsApp = attachment->value<WindowsApp *>();
    if(selectedwindowsApp != Q_NULLPTR){
        //load layout page of windows
        C_VWWorkSpace *cWorkspace = selectedwindowsApp->cWorkspace;
        if(cWorkspace != Q_NULLPTR){
            int selectedLayoutSaved = -1;
            int defaultLayoutIndex = 0;

            QSettings settings;
            settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
            settings.beginGroup("Workspace" + QString::number(selectedwindowsApp->idWindows));
            selectedLayoutSaved = settings.value("selected_layout_workspace", -1).toInt();
            settings.endGroup();

            if(selectedLayoutSaved >= 0){
                defaultLayoutIndex = selectedLayoutSaved;
            }

            clearOldSelectedButton(this->selectedButtonIndex);
            if(control()->appContext->getIsLoadDataWithDeviceId()){
                calibrateLayoutSetRemoteControl();
            }else{
                calibrateLayoutSet();
            }
            qDebug() << Q_FUNC_INFO <<"loadLayoutOfWorkspace" <<defaultLayoutIndex;

            highlightNewSelectedButton(defaultLayoutIndex);
            this->selectedButtonIndex = defaultLayoutIndex;
            QVariant *data = new QVariant();
            data->setValue<LayoutStruct>(layoutSet->layoutList.at(defaultLayoutIndex));
            control()->newUserAction(Message.APP_VIDEO_WALL_LOAD_PAGE_OF_WORKSPACE, data);

        }
    }
}

void P_VWLayoutSelector::setDefaultLayout() {
    WindowsApp *windowsVideowallSelected = this->control()->appContext->getWindowsVideowallSelected();
    if(windowsVideowallSelected != Q_NULLPTR){
        int idOfWidnowsSelected = windowsVideowallSelected->idWindows;

        qDebug() << Q_FUNC_INFO;
        clearOldSelectedButton(this->selectedButtonIndex);

        calibrateLayoutSet();

        int selectedLayoutSaved = -1;
        int defaultLayoutIndex = 0;

        if (!checkLayoutSaved){
            checkLayoutSaved = true;
            QSettings settings;
            settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
            settings.beginGroup("Workspace" + QString::number(idOfWidnowsSelected));
            selectedLayoutSaved = settings.value("selected_layout_workspace", -1).toInt();
            settings.endGroup();
        }

        if(selectedLayoutSaved >= 0 && defaultLayoutIndex <= 4 && checkLayoutSaved == true){
            defaultLayoutIndex = selectedLayoutSaved;
        }

        if(this->control()->appContext->getSiteCameras()->getCamItems().size()  <= 1){
            defaultLayoutIndex = 0;
        }

        highlightNewSelectedButton(defaultLayoutIndex);
        this->selectedButtonIndex = defaultLayoutIndex;

        QVariant *data = new QVariant();
        data->setValue<LayoutStruct>(layoutSet->layoutList.at(defaultLayoutIndex));
        control()->newUserAction(Message.APP_VIDEO_WALL_PAGE_DEFAULT_SET, data);
    }
}

void P_VWLayoutSelector::show(QVariant *attachment) { Q_UNUSED(attachment) }

void P_VWLayoutSelector::update() {}

QObject *P_VWLayoutSelector::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}
