#include "windowitem.h"
static struct IconWindow{
    QString desktopIcon = "";
    QString deleteIcon = "";
    QString fullscreenIcon = "";
    QString popupIcon = "";
    QString windows = "";
    QString notFullScreenIcon = "";
    QString windowRestore = "";
    QString windowMaximize = "";
}iconWindows;
int WindowItemCell::getHeightItem() const
{
    return heightItem;
}

WindowItemCell::WindowItemCell(WindowsApp *_windowsApp)
{
    this->windowsApp = _windowsApp;
    containtLayout = new QHBoxLayout();
    this->setLayout(containtLayout);
    containtLayout->setMargin(0);
    containtLayout->setSpacing(0);
    containtLayout->setAlignment(Qt::AlignLeft);
    this->setStyleSheet("background-color: #2f2f2f;border-bottom:1px solid #A1A1A1; color:white");

    //init left widget
    iconLeftWidget = new QWidget(this);
    iconLeftWidget->setStyleSheet("border-bottom:none;color:#bfc1c2");
    iconLeftWidget->setFixedWidth(30);
    iconLeftLayout = new QVBoxLayout();
    iconLeftLayout->setSpacing(0);
    iconLeftLayout->setMargin(0);
    iconLeftLayout->setAlignment(Qt::AlignCenter);
    iconLeftWidget->setLayout(iconLeftLayout);

    iconLeftLabel = new QLabel(iconLeftWidget);
    iconLeftLabel->setStyleSheet("border-bottom:none;color:#bfc1c2");
    iconLeftLabel->setAlignment(Qt::AlignCenter);
    iconLeftLabel->setText("");
    iconLeftLayout->addWidget(iconLeftLabel);

    //init center
    centerWidget = new QWidget(this);
    centerWidget->setStyleSheet("border-bottom:none;");

    centerWidget->setFixedWidth(250);
    centerLayout = new QVBoxLayout();
    centerLayout->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    centerLayout->setSpacing(0);
    centerLayout->setMargin(0);
    centerWidget->setLayout(centerLayout);

    QFont fontTitle;
    fontTitle.setBold(true);
    fontTitle.setPixelSize(13);
    //titler ruler
    QLabel *titleLable = new QLabel(centerWidget);
    titleLable->setFont(fontTitle);
    titleLable->setAlignment(Qt::AlignLeft);
    titleLable->setStyleSheet("border-bottom:none;color:#bfc1c2");
    centerLayout->addWidget(titleLable);
    titleLable->setText("Display " + QString::number(windowsApp->idWindows));

    //init right widget
    controlRightWidget = new QWidget(this);
    controlRightWidget->setStyleSheet("border-bottom:none;");
    controlRightWidget->setFixedWidth(130);
    controlRightLayout = new QHBoxLayout();
    controlRightLayout->setSpacing(10);
    controlRightLayout->setMargin(0);
    controlRightLayout->setAlignment(Qt::AlignCenter);
    controlRightWidget->setLayout(controlRightLayout);

    //    //button pop up

    QFont fontButton;
    fontButton.setPointSize(14);
    popupWindowButton = new QPushButton(controlRightWidget);
    popupWindowButton->setFont(fontButton);
    popupWindowButton->setStyleSheet("color:#bfc1c2");
    popupWindowButton->setFixedSize(30,30);
    if(this->windowsApp->idWindows == 0){
        popupWindowButton->setText(iconWindows.windowRestore);
    }else{
        popupWindowButton->setText(iconWindows.windowMaximize);
    }
    //button delete
    deleteWindowButton = new QPushButton(controlRightWidget);
    deleteWindowButton->setFont(fontButton);
    deleteWindowButton->setStyleSheet("color:#bfc1c2");
    deleteWindowButton->setFixedSize(30,30);
    deleteWindowButton->setText(iconWindows.deleteIcon);

    //button fullscreen
    fullscreenWindowButton = new QPushButton(controlRightWidget);
    fullscreenWindowButton->setFont(fontButton);
    fullscreenWindowButton->setStyleSheet("color:#bfc1c2");
    fullscreenWindowButton->setFixedSize(30,30);
    fullscreenWindowButton->setText(iconWindows.fullscreenIcon);


    popupWindowButton->installEventFilter(this);
    deleteWindowButton->installEventFilter(this);
    fullscreenWindowButton->installEventFilter(this);

    controlRightLayout->addWidget(fullscreenWindowButton);
    controlRightLayout->addWidget(popupWindowButton);
    controlRightLayout->addWidget(deleteWindowButton);

    //add widget
    containtLayout->addWidget(iconLeftWidget);
    containtLayout->addWidget(centerWidget);
    containtLayout->addWidget(controlRightWidget);


    //update all state of windows app
    this->updateAllStateOfWindowsApp();
}
void WindowItemCell::updateAllStateOfWindowsApp(){

    if(this->windowsApp->idWindows == 0){
        this->onHideControl();
    }

    if(windowsApp->stateOfWindows == StateOfWindows::POP_UP){
        isPopUp = true;
    }

    if(windowsApp->stateOfWindows == StateOfWindows::NOT_POP_UP){
        isPopUp = false;
    }

    if(windowsApp->stateOfWindows == StateOfWindows::FULL_SCREEN_WINDOW){
        isPopUp = true;
        isFullScreen = true;
    }

    if(!isPopUp){
        if(windowsApp->idWindows != 0){
            fullscreenWindowButton->setEnabled(false);
            fullscreenWindowButton->setStyleSheet("color:#222");
            popupWindowButton->setText(iconWindows.windowRestore);
        }
    }else{
        if(windowsApp->idWindows != 0){
            fullscreenWindowButton->setEnabled(true);
            popupWindowButton->setText(iconWindows.windowMaximize);
        }
    }

    if(isFullScreen){
        fullscreenWindowButton->setText(iconWindows.notFullScreenIcon);
    }else{
        fullscreenWindowButton->setText(iconWindows.fullscreenIcon);
    }
}


bool WindowItemCell::eventFilter(QObject *watched, QEvent *event){
    QPushButton *button = qobject_cast<QPushButton *>(watched);
    if(button == popupWindowButton){
        switch (event->type()) {
        case QEvent::Enter:
        {
            // The push button is hovered by mouse
            if(this->windowsApp->idWindows != 0){
                popupWindowButton->setStyleSheet("color:#98a9ee");
            }
            return false;
        }
            break;
        case QEvent::MouseButtonPress:{
            QMouseEvent *mouseEvent = (QMouseEvent *)(event);
            if(mouseEvent->button() == Qt::LeftButton){
                if(this->windowsApp->idWindows != 0){
                    //press button
                    if(!isPopUp){
                        isPopUp = true;
                        if(windowsApp->idWindows != 0){
                            fullscreenWindowButton->setEnabled(true);
                        }
                        popupWindowButton->setText(iconWindows.windowMaximize);
                        Q_EMIT popUpButtonClicked(this->windowsApp);
                    }else{
                        isPopUp = false;
                        if(windowsApp->idWindows != 0){
                            fullscreenWindowButton->setEnabled(false);
                        }
                        popupWindowButton->setText(iconWindows.windowRestore);
                        Q_EMIT closePopUpButtonClicked(this->windowsApp);
                    }
                }
            }
            return false;
        }break;

        case QEvent::Leave:{
            // The push button is not hovered by mouse
            if(this->windowsApp->idWindows != 0){
                popupWindowButton->setStyleSheet("color:#bfc1c2");
            }
            return false;
        } break;

        default:
            return false;
            break;
        }
    }

    if(button == fullscreenWindowButton){
        switch (event->type()) {
        case QEvent::Enter:
        {
            // The push button is hovered by mouse
            return false;
        }
            break;
        case QEvent::MouseButtonPress:{
            QMouseEvent *mouseEvent = (QMouseEvent *)(event);
            if(mouseEvent->button() == Qt::LeftButton){
                //press button
                if(!isFullScreen){
                    isFullScreen = true;
                    fullscreenWindowButton->setStyleSheet("color:#98a9ee");
                    fullscreenWindowButton->setText(iconWindows.notFullScreenIcon);
                    Q_EMIT fullscreenWindowButtonClicked(this->windowsApp);
                }else{
                    isFullScreen = false;
                    fullscreenWindowButton->setStyleSheet("color:#bfc1c2");
                    fullscreenWindowButton->setText(iconWindows.fullscreenIcon);
                    Q_EMIT hidescreenWindowButtonClicked(this->windowsApp);
                }
            }
            return false;
        }break;

        case QEvent::Leave:{
            // The push button is not hovered by mouse
            return false;
        } break;

        default:
            return false;
            break;
        }
    }

    if(button == deleteWindowButton){
        switch (event->type()) {
        case QEvent::Enter:
        {
            // The push button is hovered by mouse
            if(this->windowsApp->idWindows != 0){
                deleteWindowButton->setStyleSheet("color:#98a9ee");
            }
            return false;
        }
            break;
        case QEvent::MouseButtonPress:{
            QMouseEvent *mouseEvent = (QMouseEvent *)(event);
            if(mouseEvent->button() == Qt::LeftButton){
                //press button
                if(this->windowsApp->idWindows != 0){
                    deleteWindowButton->setStyleSheet("color:#98a9ee");
                    Q_EMIT deleteButtonClicked(this->windowsApp);
                }
            }
            return false;
        }break;

        case QEvent::Leave:{
            // The push button is not hovered by mouse
            if(this->windowsApp->idWindows != 0){
                deleteWindowButton->setStyleSheet("color:#bfc1c2");
            }
            return false;
        } break;

        default:
            return false;
            break;
        }
    }
    return false;
}

void WindowItemCell::onHideControl(){
    if(this->windowsApp->idWindows == 0){
        deleteWindowButton->setEnabled(false);
        deleteWindowButton->setStyleSheet("color:#222");
        popupWindowButton->setStyleSheet("color:#222");
        popupWindowButton->setText(iconWindows.windowRestore);
        popupWindowButton->setEnabled(false);
    }
}

void WindowItemCell::ondeleteButtonClicked(){

}

void WindowItemCell::onFullscreenWindowButtonClicked(){
    qDebug() << "onFullscreenWindowButtonClicked";
}

void WindowItemCell::onPopUpWindowButtonClicked(){
    qDebug() << "onPopUpWindowButtonClicked";
}
void WindowItemCell::updateBackground(QString background, QString color){
    this->setStyleSheet("background-color : " + background +";" + "color:" + color);
}
