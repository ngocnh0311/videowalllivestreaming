#ifndef C_VWWindowSelector_H
#define C_VWWindowSelector_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "VideoWall/VWRightControlBar/VWLayoutPage/c_vw_layoutpage.h"
#include "VideoWall/VWWorkSpace/c_vw_workspace.h"
#include "a_vm_windowselector.h"
#include "message.h"
#include "p_vm_windowselector.h"

class P_VWWindowSelector;
class A_VWWindowSelector;
class C_VWLayoutPage;

class C_VWWindowSelector : public Control {
 public:
  QWidget* zone;
  AppContext* appContext;
  C_VWWindowSelector(Control* ctrl, QWidget* zone);
  C_VWLayoutPage* getParent() { return (C_VWLayoutPage*)this->parent; }
  P_VWWindowSelector* presentation() { return (P_VWWindowSelector*)pres; }
  A_VWWindowSelector* abstraction() { return (A_VWWindowSelector*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_VWWindowSelector_H
