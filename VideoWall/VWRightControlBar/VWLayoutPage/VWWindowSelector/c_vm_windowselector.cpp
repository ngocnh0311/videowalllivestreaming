#include "c_vm_windowselector.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_VWWindowSelector::C_VWWindowSelector(Control* ctrl, QWidget* zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();

    this->pres = new P_VWWindowSelector(this, zone);
    this->zone = zone;

    // create others controls
    // cXXX = new C_XXX();

    // presentation()->loadDefaultWorkingApp();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VWWindowSelector::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_VIDEO_WALL_CLOSE_FULL_SCREEN_WORKSPACE:
    case Message.APP_VIDEO_WALL_CLOSE_POP_UP_WORKSPACE:
    case Message.APP_VIDEO_WALL_DELETE_WORKSPACE:
    case Message.APP_VIDEO_WALL_FULL_SCREEN_WORKSPACE:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_SHOW_WINDOWS_POP_UP_SELECTED:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_CLOSED_WINDOWS:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_POP_UP_WINDOWS_SELECTED:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_NEW_WINDOWS_SELECTED:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_NEW_SELECTED: {
        UserApp newWorkingApp = attachment->value<UserApp>();
        this->appContext->setWorkingApp(newWorkingApp);
        getParent()->newAction(message, attachment);
        switch (newWorkingApp.appCode) {
        case 1:
            getParent()->newAction(Message.APP_VIDEO_WALL_SHOW, Q_NULLPTR);
            break;
        case 2:
            getParent()->newAction(Message.APP_PLAY_BACK_SHOW, Q_NULLPTR);
            break;
        default:
            break;
        }
    } break;
    case Message.APP_SELECTOR_HIDE_ALL:
        getParent()->newAction(message, attachment);
        break;

    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VWWindowSelector::newSystemAction(int message, QVariant* attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case 1:
        break;
    default:
        qDebug() << "ERROR : General System action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VWWindowSelector::newAction(int message, QVariant* attachment) {

    switch (message) {
    case Message.APP_VIDEOWALL_UPDATE_LIST_WINDOWS:{
        QList<WindowsApp *> listWindowsApp = attachment->value<QList<WindowsApp*>>();
        presentation()->displayWindowsApp(listWindowsApp);
    }break;

    case Message.APP_VIDEOWALL_ADD_NEW_WINDOWS:{
        getParent()->newAction(message, attachment);
    }break;
    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to create a new pac agent exactly equals to the agent attached to
  *this control.
     * @return the control of the created clone.
     **/
// Control Control::getClone(){
//    try{
//        Presentation 	P = Q_NULLPTR;
//        Abstraction 	A = Q_NULLPTR;
//        Control 		C = (Control)clone();

//        if (pres != null)
//            P = (Presentation)(pres.getClone());
//        if (abst != null)
//            A = (Abstraction)(abst.getClone());

//        C.changeFacets(P, A);
//        return C;
//    } catch(Exception e) {System.out.println("ERROR: can't duplicate a
//    control.");}
//    return null;
//}
