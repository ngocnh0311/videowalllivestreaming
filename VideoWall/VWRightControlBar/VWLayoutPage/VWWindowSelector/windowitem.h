#ifndef WINDOWITEM_H
#define WINDOWITEM_H
#include <QVBoxLayout>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QObject>
#include "Authentication/windowsapp.h"
#include <QtAwesome/QtAwesome.h>
#include "Common/resources.h"
#include <QVariantMap>
#include <QTimer>
#include <QMouseEvent>
class WindowItemCell: public QWidget
{
    Q_OBJECT
private:
    int heightItem = 50;
public:
    bool isFullScreen = false;
    bool isPopUp = false;
    WindowsApp *windowsApp;
    int widthIconLeft = 50;
    int widthIconRight = 50;
    QHBoxLayout *containtLayout;
    QWidget *controlRightWidget = Q_NULLPTR;
    QHBoxLayout *controlRightLayout = Q_NULLPTR;

    QWidget *centerWidget = Q_NULLPTR;
    QVBoxLayout *centerLayout = Q_NULLPTR;

    QWidget *iconLeftWidget = Q_NULLPTR;
    QVBoxLayout *iconLeftLayout = Q_NULLPTR;
    QLabel *iconLeftLabel = Q_NULLPTR;

    QPushButton *popupWindowButton = Q_NULLPTR;
    QPushButton *deleteWindowButton = Q_NULLPTR;
    QPushButton *fullscreenWindowButton = Q_NULLPTR;

    WindowItemCell(WindowsApp *windowsApp);
    void updateBackground(QString background, QString color);
    int getHeightItem() const;
    void ondeleteButtonClicked();
    void onPopUpWindowButtonClicked();
    void onFullscreenWindowButtonClicked();
    void onHideControl();
    void updateAllStateOfWindowsApp();

protected:
    bool eventFilter(QObject *watched, QEvent *event);

Q_SIGNALS:
    void popUpButtonClicked(WindowsApp *windowsApp);
    void closePopUpButtonClicked(WindowsApp *windowsApp);
    void deleteButtonClicked(WindowsApp *windowsApp);
    void fullscreenWindowButtonClicked(WindowsApp *windowsApp);
    void hidescreenWindowButtonClicked(WindowsApp *windowsApp);

};

#endif // WINDOWITEM_H
