#ifndef A_VWWindowSelector_H
#define A_VWWindowSelector_H

#include <QObject>
#include "PacModel/control.h"
class C_VWWindowSelector;
class A_VWWindowSelector : public Abstraction {
  Q_OBJECT
  // A ref on the control facet
 private:
 public:
  A_VWWindowSelector(Control *ctrl);
  void changeControl(Control *ctrl);
};

#endif  // A_VWWindowSelector_H
