#include "p_vm_windowselector.h"

/**
     * Generic method to override for updating the presention.
     **/

P_VWWindowSelector::P_VWWindowSelector(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    this->zone->setStyleSheet(
                "background-color: #333; color: white; border:0px;");


    QVBoxLayout *mSelectionLayout = new QVBoxLayout();
    mSelectionLayout->setAlignment(Qt::AlignCenter);
    mSelectionLayout->setMargin(0);
    mSelectionLayout->setSpacing(0);
    this->zone->setLayout(mSelectionLayout);

    QWidget *bottomWidget = new QWidget(this->zone);
    QHBoxLayout *bottomLayout = new QHBoxLayout();
    bottomLayout->setAlignment(Qt::AlignCenter);
    bottomLayout->setSpacing(0);
    bottomLayout->setMargin(0);
    bottomWidget->setLayout(bottomLayout);

    windowsListwidget = new QListWidget(bottomWidget);
    windowsListwidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    windowsListwidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    bottomLayout->addWidget(windowsListwidget);
    connect(windowsListwidget, &QListWidget::itemClicked, this , &P_VWWindowSelector::onListWindowItemClicked);

    //add widget
    //    mSelectionLayout->addWidget(topWidget);
    mSelectionLayout->addWidget(bottomWidget);

}

void P_VWWindowSelector::onListWindowItemClicked(QListWidgetItem *item){
    if(item && item != itemDeteleButton){
        int indexItemSelected = listItemCellWindows.indexOf(item);
        //reset color of cell before
        for (int index = 0; index < listWindowItemCells.size(); ++index) {
            WindowItemCell *windowItemCell = listWindowItemCells.at(index);
            windowItemCell->updateBackground("#2f2f2f", "white");
        }

        WindowItemCell *windowItemCell = listWindowItemCells.at(indexItemSelected);
        windowItemCell->updateBackground("#414244" , "#FFF");

        WindowsApp* selectedNewWindowsApp = listWindowsApp.at(indexItemSelected);
        if(selectedNewWindowsApp != Q_NULLPTR){
            this->control()->appContext->setWindowsVideowallSelected(selectedNewWindowsApp);
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<WindowsApp *>(selectedNewWindowsApp);
            control()->newUserAction(Message.APP_VIDEOWALL_NEW_WINDOWS_SELECTED, dataStruct);
        }
        if(selectedNewWindowsApp->stateOfWindows == StateOfWindows::POP_UP || selectedNewWindowsApp->stateOfWindows == StateOfWindows::FULL_SCREEN_WINDOW){
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<WindowsApp *>(selectedNewWindowsApp);
            control()->newUserAction(Message.APP_VIDEOWALL_SHOW_WINDOWS_POP_UP_SELECTED , dataStruct);
        }
    }
}

void P_VWWindowSelector::onCloseWidget(WindowWidget* windowsWidget){
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<WindowWidget *>(windowsWidget);
    control()->newUserAction(Message.APP_VIDEOWALL_CLOSED_WINDOWS , dataStruct);
    qDebug() << Q_FUNC_INFO;
}


void P_VWWindowSelector::addNewWindows(){
    control()->newAction(Message.APP_VIDEOWALL_ADD_NEW_WINDOWS , Q_NULLPTR);
}

void P_VWWindowSelector::update() {}

void P_VWWindowSelector::show() {
    //  if (control()->zone->isVisible()) {
    //    control()->zone->hide();
    //  } else {
    //    control()->zone->show();

    //    control()->zone->raise();
    //  }
}

void P_VWWindowSelector::hide() { control()->zone->hide(); }

void P_VWWindowSelector::loadDefaultWorkingWindows() {
    //  QList<WindowsApp> userApps = control()->appContext->getUserApps();
    //  WindowsApp defaultApp = userApps.at(0);
    //  QVariant *dataStruct = new QVariant();
    //  dataStruct->setValue(defaultApp);
    //  control()->newUserAction(Message.APP_NEW_SELECTED, dataStruct);
}

void P_VWWindowSelector::displayWindowsApp(QList<WindowsApp *> listWindowsApp) {
    this->listWindowsApp.clear();
    listWindowItemCells.clear();
    listItemCellWindows.clear();
    windowsListwidget->clear();
    itemDeteleButton = Q_NULLPTR;

    this->listWindowsApp = listWindowsApp;

    QStringList windowsVideowallStringList;
    for (int index = 0; index < this->listWindowsApp.size(); ++index) {
        WindowsApp *windowsApp = this->listWindowsApp.at(index);
        QString workspaceName = windowsApp->windowsName;
        WindowItemCell *windowsItemCell = new WindowItemCell(windowsApp);

        connect(windowsItemCell, &WindowItemCell::popUpButtonClicked , this, &P_VWWindowSelector::onPopUpButtonClicked);
        connect(windowsItemCell, &WindowItemCell::closePopUpButtonClicked, this, &P_VWWindowSelector::onClosePopUpButtonClicked);
        connect(windowsItemCell, &WindowItemCell::deleteButtonClicked , this, &P_VWWindowSelector::onDeleteWorkspaceButtonClicked);

        connect(windowsItemCell, &WindowItemCell::fullscreenWindowButtonClicked , this, &P_VWWindowSelector::onFullscreenWindowButtonClicked);
        connect(windowsItemCell, &WindowItemCell::hidescreenWindowButtonClicked , this, &P_VWWindowSelector::onHidescreenWindowButtonClicked);

        listWindowItemCells.append(windowsItemCell);
        QListWidgetItem *listItem = new QListWidgetItem;

        listItemCellWindows.append(listItem);
        listItem->setSizeHint(QSize(listItem->sizeHint().width(), windowsItemCell->getHeightItem()));
        windowsListwidget->addItem(listItem);
        windowsListwidget->setItemWidget(listItem , windowsItemCell);
    }

    if(windowsListwidget->count() < Message.APP_VIDEO_WALL_MAX_WORKSPACE_INIT){
        //add button add
        QWidget *addButtonWidget = new QWidget();
        QHBoxLayout *addButtonLayout = new QHBoxLayout();
        addButtonLayout->setMargin(0);
        addButtonLayout->setSpacing(0);
        addButtonLayout->setAlignment(Qt::AlignLeft);
        addButtonWidget->setLayout(addButtonLayout);
        addButtonWidget->setStyleSheet("background-color: #2f2f2f; color:bfc1c2");

        //init left widget
        QWidget *iconLeftWidget = new QWidget(addButtonWidget);
        iconLeftWidget->setStyleSheet("border-bottom:none;color:#bfc1c2");
        iconLeftWidget->setFixedWidth(30);
        QVBoxLayout *iconLeftLayout = new QVBoxLayout();
        iconLeftLayout->setSpacing(0);
        iconLeftLayout->setMargin(0);
        iconLeftLayout->setAlignment(Qt::AlignCenter);
        iconLeftWidget->setLayout(iconLeftLayout);

        QLabel *iconLeftLabel = new QLabel(iconLeftWidget);
        iconLeftLabel->setStyleSheet("border-bottom:none;color:#bfc1c2");
        iconLeftLabel->setAlignment(Qt::AlignCenter);
        iconLeftLabel->setText("");
        iconLeftLayout->addWidget(iconLeftLabel);

        //init center
        QWidget *centerWidget = new QWidget(addButtonWidget);
        centerWidget->setStyleSheet("border:none;");
        QVBoxLayout *centerLayout = new QVBoxLayout();
        centerLayout->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
        centerLayout->setSpacing(0);
        centerLayout->setMargin(0);
        centerWidget->setLayout(centerLayout);

        addButtonLayout->addWidget(iconLeftWidget);
        addButtonLayout->addWidget(centerWidget);

        QPushButton *buttonAddWindows = new QPushButton(centerWidget);
        QFont fontButton;
        fontButton.setPointSize(10);
        fontButton.setBold(true);
        buttonAddWindows->setFont(fontButton);
        //        buttonAddWindows->setFixedSize(150,25);
        buttonAddWindows->setStyleSheet("color:#bfc1c2;text-align:left");
        buttonAddWindows->setText("Add New Window");
        disconnect(buttonAddWindows, &QPushButton::clicked , this, &P_VWWindowSelector::addNewWindows);
        connect(buttonAddWindows, &QPushButton::clicked , this, &P_VWWindowSelector::addNewWindows);
        centerLayout->addWidget(buttonAddWindows);

        QListWidgetItem *listItem = new QListWidgetItem;
        itemDeteleButton = listItem;
        listItemCellWindows.append(listItem);
        buttonAddWindows->setFixedSize(QSize(addButtonWidget->width(), 50));
        listItem->setSizeHint(QSize(listItem->sizeHint().width(), 50));
        windowsListwidget->addItem(listItem);
        windowsListwidget->setItemWidget(listItem , addButtonWidget);
    }

    WindowsApp *windowsAppSelected = this->control()->appContext->getWindowsVideowallSelected();
    if(windowsAppSelected != Q_NULLPTR && windowsAppSelected->idWindows >= 0){
        int indexWindowsSelected = this->listWindowsApp.indexOf(windowsAppSelected);
        if(indexWindowsSelected >= 0){
            windowsListwidget->setCurrentItem(listItemCellWindows.at(indexWindowsSelected));
            //reset color of cell before
            for (int index = 0; index < listWindowItemCells.size(); ++index) {
                WindowItemCell *windowItemCell = listWindowItemCells.at(index);
                windowItemCell->updateBackground("#2f2f2f", "white");
            }

            WindowItemCell *windowItemCell = listWindowItemCells.at(indexWindowsSelected);
            windowItemCell->updateBackground("#414244" , "#FFF");
        }
    }

}


void P_VWWindowSelector::onClosePopUpButtonClicked(WindowsApp *windowsApp){
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<WindowsApp *>(windowsApp);
    control()->newUserAction(Message.APP_VIDEO_WALL_CLOSE_POP_UP_WORKSPACE , dataStruct);
}

void P_VWWindowSelector::onPopUpButtonClicked(WindowsApp *windowsApp){
    if(windowsApp != Q_NULLPTR){
        if(windowsApp->stateOfWindows != POP_UP){
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<WindowsApp *>(windowsApp);
            control()->newUserAction(Message.APP_VIDEOWALL_POP_UP_WINDOWS_SELECTED , dataStruct);
        }else{
            //show windows parent
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<WindowsApp *>(windowsApp);
            control()->newUserAction(Message.APP_VIDEOWALL_SHOW_WINDOWS_POP_UP_SELECTED , dataStruct);
        }
    }
}

void P_VWWindowSelector::onDeleteWorkspaceButtonClicked(WindowsApp *windowsApp){
    QMessageBox messageBoxDelete;
    messageBoxDelete.setFixedSize(QSize(600, 120));
    messageBoxDelete.setIcon(QMessageBox::Warning);
    messageBoxDelete.setText("Khi cửa sổ này bị xóa, ứng dụng sẽ bị khởi động lại.\n"
                                        "Bạn còn muốn xóa không?");
    messageBoxDelete.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
    messageBoxDelete.setDefaultButton(QMessageBox::Cancel);
    int ret = messageBoxDelete.exec();
    switch (ret) {
    case QMessageBox::Ok:{
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<WindowsApp *>(windowsApp);
        control()->newUserAction(Message.APP_VIDEO_WALL_DELETE_WORKSPACE , dataStruct);
    }break;
    case QMessageBox::Cancel:{}break;
    default:
        break;
    }
}

void P_VWWindowSelector::onFullscreenWindowButtonClicked(WindowsApp *windowsApp){
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<WindowsApp *>(windowsApp);
    control()->newUserAction(Message.APP_VIDEO_WALL_FULL_SCREEN_WORKSPACE , dataStruct);
}

void P_VWWindowSelector::onHidescreenWindowButtonClicked(WindowsApp *windowsApp){
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<WindowsApp *>(windowsApp);
    control()->newUserAction(Message.APP_VIDEO_WALL_CLOSE_FULL_SCREEN_WORKSPACE , dataStruct);
}


QWidget *P_VWWindowSelector::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}

bool P_VWWindowSelector::eventFilter(QObject *watched, QEvent *event) {
    //    QListView *listview = qobject_cast<QListView *>(watched);
    //    if (listview == mWindowsListView) {
    //        switch (event->type()) {
    //        case QEvent::HoverEnter:
    //            hoverEnter(static_cast<QHoverEvent *>(event));
    //            return true;
    //            break;
    //        case QEvent::HoverLeave:
    //            hoverLeave(static_cast<QHoverEvent *>(event));
    //            return true;
    //            break;
    //        case QEvent::HoverMove:
    //            hoverMove(static_cast<QHoverEvent *>(event));
    //            return true;
    //            break;
    //        default:
    //            break;
    //        }
    //    }

    //    //  return QWidget::eventFilter(watched, event);
    //    return false;
}

void P_VWWindowSelector::hoverEnter(QHoverEvent *) {}

void P_VWWindowSelector::hoverLeave(QHoverEvent *) {

    qDebug() << Q_FUNC_INFO ;
}

void P_VWWindowSelector::hoverMove(QHoverEvent *) { /* qDebug() << "hover move";*/
}
