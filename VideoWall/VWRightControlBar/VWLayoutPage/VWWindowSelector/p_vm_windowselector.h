#ifndef P_VWWindowSelector_H
#define P_VWWindowSelector_H

#include <Authentication/windowsapp.h>
#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QDebug>
#include <QFont>
#include <QHash>
#include <QHoverEvent>
#include <QLabel>
#include <QListWidget>
#include <QMouseEvent>
#include <QObject>
#include <QPushButton>
#include <QStringListModel>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/resources.h"
#include "c_vm_windowselector.h"
#include "windowitem.h"
class WindowWidget;
static struct IconWindowsSelector{
    QString plus = "";
    QString minus= "";
    QString windows = "";
}iconWindowsSelector;

class C_VWWindowSelector;
class WindowsApp;
class P_VWWindowSelector : public Presentation {
    // init ui control
private:
public:
    QWidget *zone;
    int indexItemSelectedLast = -1;
    int maxWindows = 4;
    P_VWWindowSelector(Control *ctrl, QWidget *zone);
    // init ui control
    QList<WindowsApp *> listWindowsApp;
    QListWidget *windowsListwidget;
    QList<QListWidgetItem *> listItemCellWindows;

    QList<WindowItemCell *> listWindowItemCells;
    QListWidgetItem *itemDeteleButton = Q_NULLPTR;

    C_VWWindowSelector *control() { return (C_VWWindowSelector *)this->ctrl; }
    void changeControl(Control *ctrl);
    void update();
    QWidget *getZone(int zoneId);

    void show();
    void hide();
    void displayWindowsApp(QList<WindowsApp*> listWindowsApp);
    void loadDefaultWorkingWindows();
public Q_SLOTS:
    void addNewWindows();
    void onCloseWidget(WindowWidget* windowsWidget);
    void onListWindowItemClicked(QListWidgetItem *item);
    void onPopUpButtonClicked(WindowsApp *windowsApp);
    void onDeleteWorkspaceButtonClicked(WindowsApp *windowsApp);
    void onFullscreenWindowButtonClicked(WindowsApp *windowsApp);
    void onHidescreenWindowButtonClicked(WindowsApp *windowsApp);
    void onClosePopUpButtonClicked(WindowsApp *windowsApp);

protected:
    void hoverEnter(QHoverEvent *event);
    void hoverLeave(QHoverEvent *event);
    void hoverMove(QHoverEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
};

#endif  // P_VWWindowSelector_H
