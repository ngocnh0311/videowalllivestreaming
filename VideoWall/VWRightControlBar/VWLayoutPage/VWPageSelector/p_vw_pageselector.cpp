#include "p_vw_pageselector.h"
#include "VideoWall/VWWorkSpace/c_vw_workspace.h"
int P_VWPageSelector::getNumberOfPages() const { return numberOfPages; }

void P_VWPageSelector::setNumberOfPages(int value) { numberOfPages = value; }

int P_VWPageSelector::getSelectedPage() const { return selectedPage; }

void P_VWPageSelector::setSelectedPage(int value) { selectedPage = value; }

P_VWPageSelector::P_VWPageSelector(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    this->zone->setStyleSheet("background-color: #222;border:none");
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(0);
    mainLayout->setSpacing(4);
    mainLayout->setAlignment(Qt::AlignHCenter);
    this->zone->setLayout(mainLayout);

    QScrollArea *scrollArea = new QScrollArea(this->zone);
    scrollArea->setAlignment(Qt::AlignHCenter);
    mainLayout->addWidget(scrollArea);

    pageWidget = new QWidget(scrollArea);
    scrollArea->setWidget(pageWidget);
    this->pageLayout = new QGridLayout();
    pageLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
    pageLayout->setMargin(0);
    pageLayout->setSpacing(2);
    pageWidget->setLayout(this->pageLayout);

    waitingTimer = new QTimer();

}

void P_VWPageSelector::loadPageOfWorkspace(LayoutStruct layout){
    WindowsApp *selectedwindowsApp = this->control()->appContext->getWindowsVideowallSelected();
    if(selectedwindowsApp != Q_NULLPTR){
        //load layout page of windows
        C_VWWorkSpace *cWorkspace = selectedwindowsApp->cWorkspace;
        if(cWorkspace != Q_NULLPTR){
            this->lastSelectedLayout = layout;
            if(control()->appContext->getIsLoadDataWithDeviceId()){
                updateNumberOfPagesRemoteControl(layout.numberOfCameras);
            }else{
                updateNumberOfPages(layout.numberOfCameras);
            }
            clearPages();
            displayPages();
            // send the selected layout/page to workspace agent
            // re-calculate default selected page
            displaySelectedPage();
            lastSelectedLayout.selectedPage =  this->selectedPage;
        }
    }
}

void P_VWPageSelector::show(QVariant *attachment) { Q_UNUSED(attachment) }


void P_VWPageSelector::updatePageRemoteControl(LayoutStruct selectedLayout) {
    this->lastSelectedLayout = selectedLayout;
    this->selectedPage = selectedLayout.selectedPage;

    updateNumberOfPagesRemoteControl(selectedLayout.numberOfCameras);
    clearPages();
    displayPages();
    // send the selected layout/page to workspace agent
    // re-calculate default selected page
    displaySelectedPage();
    lastSelectedLayout.selectedPage = this->selectedPage;
    // update
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<LayoutStruct>(lastSelectedLayout);
    control()->newUserAction(Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED,
                             dataStruct);
}

void P_VWPageSelector::updateLayoutSelectedByHandOfRemoteControl(LayoutStruct selectedLayout) {
    this->lastSelectedLayout = selectedLayout;
    updateNumberOfPagesRemoteControl(selectedLayout.numberOfCameras);

    if(this->numberOfPages > 0){
        this->selectedPage = 1;
    }else{
        this->selectedPage = 0;
    }
    this->control()->appContext->setPageSelected(this->selectedPage);

    clearPages();
    displayPages();
    // send the selected layout/page to workspace agent
    // re-calculate default selected page
    displaySelectedPage();
    lastSelectedLayout.selectedPage = this->selectedPage;
    // update
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<LayoutStruct>(lastSelectedLayout);
    control()->newUserAction(Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED_HANDEL,
                             dataStruct);
}


void P_VWPageSelector::update(LayoutStruct selectedLayout) {

    //neu chon bang tay load data voi layout page
    this->lastSelectedLayout = selectedLayout;
    updateNumberOfPages(selectedLayout.numberOfCameras);
    if (this->numberOfPages > 0){
        this->selectedPage = 1;
    }else{
        this->selectedPage = 0;
    }
    clearPages();
    displayPages();

    // send the selected layout/page to workspace agent
    // re-calculate default selected page
    displaySelectedPage();
    lastSelectedLayout.selectedPage = this->selectedPage;
    // update
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<LayoutStruct>(lastSelectedLayout);
    control()->newUserAction(Message.APP_VIDEO_WALL_ZONE_PAGE_SELECTED,
                             dataStruct);
}


void P_VWPageSelector::updateDefaulPage(LayoutStruct layoutDefault) {
    this->lastSelectedLayout = layoutDefault;
    updateNumberOfPages(layoutDefault.numberOfCameras);
    clearPages();
    displayPages();
    // send the selected layout/page to workspace agent
    // re-calculate default selected page
    displaySelectedPage();
    lastSelectedLayout.selectedPage = this->selectedPage;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<LayoutStruct>(this->lastSelectedLayout);
    control()->newUserAction(Message.APP_VIDEO_WALL_UPDATE_PAGE_DEFAULT,
                             dataStruct);
}

void P_VWPageSelector::clearPages() {
    while (buttons.size() > 0) {
        QPushButton *button = buttons.last();
        button->hide();
        pageLayout->removeWidget(button);
        buttons.removeLast();
        button->deleteLater();
    }
}

void P_VWPageSelector::displayPages() {
    qDebug() << Q_FUNC_INFO << "this->numberOfPages" <<this->numberOfPages;
    buttons.clear();
    for (int index = 0; index < this->numberOfPages; ++index) {
        QPushButton *button = new QPushButton(pageWidget);
        button->setText(QString("%1").arg(index + 1));
        button->setFont(Resources::instance().getMediumRegularButtonFont());

        button->setStyleSheet("background-color: white; color:#222; "
                    "border:1px solid #333; "
                    "padding: 5px "
                    "0px;");
        button->setFixedSize(60, 25);

        buttons.append(button);
        int r = index / col;
        int c = index - r * col;
        this->pageLayout->addWidget(button, r, c);
        connect(button, &QPushButton::clicked, this,
                &P_VWPageSelector::onPageSelected);
    }
    if (!buttons.isEmpty()) {
        int rows =
                this->numberOfPages / col + (this->numberOfPages % col > 0 ? 1 : 0);
        pageWidget->setFixedSize(appSize.rightWidth - 50,
                                 (rows) * (buttons.first()->height() + 5));
    } else {
        pageWidget->setFixedSize(appSize.rightWidth - 50, 0);
    }
}

void P_VWPageSelector::displaySelectedPage() {
    for (int index = 0; index < buttons.size(); ++index) {
        QPushButton *button = buttons.at(index);
        if (selectedPage == index + 1) {
            button->setStyleSheet("background-color: red; color: white");
        } else {
            button->setStyleSheet("background-color: white; color: black");
        }
    }
}

void P_VWPageSelector::onPageSelected() {
    QPushButton *button = qobject_cast<QPushButton *>(sender());
    if (button != Q_NULLPTR) {
        int index = buttons.indexOf(button);
        if (index >= 0) {
            this->selectedPage = index + 1;
            displaySelectedPage();
        }
        lastButton = button;

        if (!isWaiting) {
            startWaiting();
        } else {
            isWaiting = false;
            waitingTimer->stop();
            disconnect(waitingTimer, &QTimer::timeout, this,
                       &P_VWPageSelector::onEndWaiting);
            startWaiting();
        }
    }
    lastButton = button;

    if (!isWaiting) {
        isWaiting = true;
        connect(waitingTimer, &QTimer::timeout, this,
                &P_VWPageSelector::onEndWaiting);
        waitingTimer->start(0);
    }
}

void P_VWPageSelector::startWaiting() {
    if (!isWaiting) {
        isWaiting = true;
        connect(waitingTimer, &QTimer::timeout, this,
                &P_VWPageSelector::onEndWaiting);
        waitingTimer->start(0);
    }
}

void P_VWPageSelector::onEndWaiting() {
    isWaiting = false;
    if (lastButton != Q_NULLPTR) {
        int indexOfButton = buttons.indexOf(lastButton);
        this->selectedPage = indexOfButton + 1;
        this->lastSelectedLayout.selectedPage = this->selectedPage;
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue(this->lastSelectedLayout);
        if(control()->appContext->getIsLoadDataWithDeviceId()){
            control()->newUserAction(Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED_HANDEL,
                                     dataStruct);
        }else{
            control()->newUserAction(Message.APP_VIDEO_WALL_ZONE_PAGE_SELECTED,
                                     dataStruct);
        }
        lastButton = Q_NULLPTR;
    }
    waitingTimer->stop();
    disconnect(waitingTimer, &QTimer::timeout, this,
               &P_VWPageSelector::onEndWaiting);
}

QObject *P_VWPageSelector::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}

void P_VWPageSelector::updateNumberOfPagesRemoteControl(int numberOfCamerasPerPage) {
    if (numberOfCamerasPerPage == 0) {
        this->numberOfPages = 0;
        this->selectedPage = 0;
        return;
    }
    WindowsApp *windowsVideowallSelected = this->control()->appContext->getWindowsVideowallSelected();
    if(windowsVideowallSelected != Q_NULLPTR){
        int idOfWidnowsSelected = windowsVideowallSelected->idWindows;
        CamSite *camSite = control()->appContext->getSiteCameras();
        if (camSite != Q_NULLPTR) {
            int numberOfCameras = camSite->getCamItems().size();
            int totalPage = numberOfCameras / numberOfCamerasPerPage;
            if (numberOfCameras % numberOfCamerasPerPage != 0) {
                totalPage += 1;
            }

            this->numberOfPages = totalPage;
            control()->appContext->setTotalPages(this->numberOfPages);

            int selectedPageSaved = -1;
            //chi load page saved ban dau
            QSettings settings;
            settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
            settings.beginGroup("Workspace" + QString::number(idOfWidnowsSelected));
            selectedPageSaved  = settings.value("selected_page_workspace", -1).toInt();
            settings.endGroup();
            if (this->numberOfPages > 0){
                this->selectedPage = 1;
                if (selectedPageSaved > 0 && selectedPageSaved <= this->numberOfPages){
                    this->selectedPage = selectedPageSaved;
                }
            }else{
                this->selectedPage = 0;
            }
        }
        else{
            this->selectedPage = 0;
        }
        qDebug() << Q_FUNC_INFO << "selectedPage " << this->selectedPage << "numberOfCamerasPerPage" <<numberOfCamerasPerPage;
    }
}

void P_VWPageSelector::updateNumberOfPages(int numberOfCamerasPerPage) {
    if (numberOfCamerasPerPage == 0) return;
    WindowsApp *windowsVideowallSelected = this->control()->appContext->getWindowsVideowallSelected();
    if(windowsVideowallSelected != Q_NULLPTR){
        int idOfWidnowsSelected = windowsVideowallSelected->idWindows;
        CamSite *camSite = control()->appContext->getSiteCameras();
        if (camSite != Q_NULLPTR) {
            int numberOfCameras;
            QSettings settings;
            QString offlineMode = settings.value("offline_mode").toString();
            if(offlineMode == "ON"){
                numberOfCameras = camSite->getCamItems().size();
            }else{
                numberOfCameras = camSite->getTotalCamItem();
            }

            int totalPage = numberOfCameras / numberOfCamerasPerPage;
            if (numberOfCameras % numberOfCamerasPerPage != 0) {
                totalPage += 1;
            }
            this->numberOfPages = totalPage;
            control()->appContext->setTotalPages(this->numberOfPages);

            int selectedPageSaved = -1;


            settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
            settings.beginGroup("Workspace" + QString::number(idOfWidnowsSelected));
            selectedPageSaved  = settings.value("selected_page_workspace", -1).toInt();
            settings.endGroup();

            if (this->numberOfPages > 0){
                this->selectedPage = 1;
                if (selectedPageSaved > 0 && selectedPageSaved <= this->numberOfPages){
                    this->selectedPage = selectedPageSaved;
                }
            }
        }
        else{
            this->selectedPage = 0;
        }
    }
    qDebug() << Q_FUNC_INFO << "lastSelectedLayout.numberOfCameras " << this->selectedPage << lastSelectedLayout.numberOfCameras;
}

void P_VWPageSelector::pageTransitionBegin() {
    qDebug() << Q_FUNC_INFO;
    WindowsApp *windowsAppSelected = this->control()->appContext->getWindowsVideowallSelected();
    if(windowsAppSelected != Q_NULLPTR){
        C_VWWorkSpace *cVMWorkspace = windowsAppSelected->cWorkspace;
        if(cVMWorkspace != Q_NULLPTR){
            LayoutStruct layoutWorkspaceSelected = cVMWorkspace->presentation()->getSelectedLayout();
            int selectedPageWorkspace = layoutWorkspaceSelected.selectedPage;
            if(this->numberOfPages <= 1) return;
            this->selectedPage = selectedPageWorkspace;
            displaySelectedPage();
            this->lastSelectedLayout.selectedPage = this->selectedPage;
        }
    }
    //    QVariant *dataStruct = new QVariant();
    //    dataStruct->setValue(this->lastSelectedLayout);
    //    control()->newUserAction(Message.APP_VIDEO_WALL_ZONE_PAGE_SELECTED,
    //                             dataStruct);
}
