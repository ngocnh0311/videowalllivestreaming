#include "c_vw_workspace.h"
/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
QString C_VWWorkSpace::getAppName() const { return appName; }

void C_VWWorkSpace::setAppName(const QString& value) { appName = value; }

QWidget *C_VWWorkSpace::getZone() const
{
    return zone;
}

void C_VWWorkSpace::setZone(QWidget *value)
{
    zone = value;
}

C_VWWorkSpace::C_VWWorkSpace(WindowsApp *windowsApp, QString appName, Control* ctrl, QWidget* zone) : Control(ctrl) {
    this->appName = appName;
    this->windowsAppOfWorkspace = windowsApp;

    // update app context
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();

    // init facets
    this->abst = new A_VWWorkSpace(this);
    this->pres = new P_VWWorkSpace(this->windowsAppOfWorkspace, this->appName, this, zone);
    this->zone = zone;
    if(appName == "Video Wall 1"){
        qDebug() <<"appName Zone" <<this->zone;
    }

    qDebug() << "C_VWWorkSpace init Thread" <<QThread::currentThread();


    //    // create player controls
    // create player controls
    for (int index = 0; index < Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS;
         ++index) {
        QWidget* newZone = presentation()->getZone(index);
        Cam9PlayerPool::instance().getCam9RTCPlayer(this, newZone, appName , QString::number(index));
    }
}

void C_VWWorkSpace::refreshPlayerList(QWidget *newZone){
    this->zone = newZone;
    this->presentation()->refreshPlayerZoneList(this->zone);
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VWWorkSpace::newUserAction(int message, QVariant* attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID:{
        getParent()->newAction(message,attachment);
    }break;

    case Message.SHOW:
        break;

    case Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE:{
        getParent()->newAction(message,attachment);
    }break;

    default:
        qDebug() << "ERROR : General User action in" << getClass()
                 << "non-catched :" << Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VWWorkSpace::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.WORKSPACE_PAGE_TRANSITION_BEGIN:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_CONTEXT_GET:
        attachment->setValue(this->appContext);
        break;

    default:
        qDebug() << "ERROR : General System action in" << getClass()
                 << "non-catched :" << Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VWWorkSpace::newAction(int message, QVariant* attachment) {
    switch (message) {
    //    case Message.APP_VIDEOWALL_STOP_ALL_WHEN_CREATE_NEW_WORKPSACE:
    //    case Message.APP_VIDEOWALL_POP_UP_WINDOWS_SELECTED:
    //    case Message.APP_VIDEOWALL_CLOSED_WINDOWS:{
    //        //        presentation()->stopPlaying();
    //    }break;
    case Message.VIDEO_WALL_RENDER_UNSUBSCRIBE_CRAWLER_CAMERA:
    case Message.VIDEO_WALL_RENDER_SUBSCRIBE_CRAWLER_CAMERA:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_UPDATE_DATA_LOCAL_SUCCESS:{
        if(this->appName.contains(this->appContext->getWorkingApp().appName, Qt::CaseInsensitive)){
            presentation()->refreshAppShow();
        }
    }break;

    case Message.PAGE_TRANSITION_BEGIN:{
        presentation()->pageTransitionBegin();
    }break;

    case Message.PREPARE_FREE_WORKSPACE_NEED_PLAYER_STOP:
    case Message.APP_VIDEOWALL_WORKSPACE_STOP_PLAYER_WORKING:
    case Message.APP_VIDEOWALL_NEW_WINDOWS_SELECTED:{
        presentation()->stopPlaying();
    }break;

    case Message.APP_VIDEOWALL_REFRESH_TAB_LAYOUT_PAGE:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE:{
        presentation()->refreshDataWorkspace(attachment);
    }break;

    case Message.UPDATE_STATE_USE_FREE_SPACE:{
        QString useFreeSpace = attachment->value<QString>();
        presentation()->updateUseFreeSpace(useFreeSpace);
    }break;

    case Message.UPDATE_MODE_RENDER_DEBUG:{
        QString modeDebug = attachment->value<QString>();
        presentation()->updateModeDebug(message, attachment);
    }break;

    case Message.PLAYER_BEGIN_HIDE_FULLSCREEN_HAND_CLICK_OF_REMOTE_CONTROL:{
        this->presentation()->updateToServerCameraHideSceen();
        //        int index = attachment->value<int>();
        //        presentation()->hideFullCellAt(index);
    }break;

    case Message.PLAYER_BEGIN_SHOW_FULLSCREEN_HAND_CLICK_OF_REMOTE_CONTROL:{
        int index = attachment->value<int>();
        this->presentation()->updateToServerCameraFullSceen(index);
        //        presentation()->showFullCellAt(index);

    }break;

    case Message.APP_VIDEO_WALL_UPDATE_CAMERA_ORDER_REMOTE_CONTROL:{
        this->getParent()->newAction(message, attachment);
    }break;

        //click by user
    case Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED_HANDEL:{
        LayoutStruct layout = attachment->value<LayoutStruct>();
        QSettings settings;
        settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
        WindowsApp *windowsVideowallSelected = this->appContext->getWindowsVideowallSelected();
        if(windowsVideowallSelected != Q_NULLPTR){
            int idOfWidnowsSelected = windowsVideowallSelected->idWindows;
            settings.beginGroup("Workspace" + QString::number(idOfWidnowsSelected));
            settings.setValue("selected_layout_workspace", layout.code);
            settings.setValue("selected_page_workspace",layout.selectedPage);
            settings.endGroup();
        }
        settings.endGroup();
        if(this->appContext->getIsNeedSynchronizedWithServer()){
            //need synchronized with server
            presentation()->refreshLayoutPageSelectedHandelRemoteControl(layout);
        }else{
            presentation()->refresh(layout);
        }
    }break;

    case Message.UPDATE_CDN_TYPE_SELECTED:{
        CamItemType type = attachment->value<CamItemType>();
        this->presentation()->updateNetworkTypeSelected(type);
    }break;

    case Message.APP_NETWORK_IS_REACHABLE:
    case Message.APP_NETWORK_IS_UNREACHABLE:
    {
        this->presentation()->networkChangeStatus(message, attachment);
    }
        break;

    case Message.APP_SHOW:{
        if(!firstShow){
            if(this->appName.contains(this->appContext->getWorkingApp().appName, Qt::CaseInsensitive)){
                presentation()->refreshAppShow();
            }
        }
        firstShow = false;
    }
        break;

    case Message.APP_CHANGED:
        if(this->appName.contains(this->appContext->getWorkingApp().appName, Qt::CaseInsensitive) == false){
            presentation()->stopPlaying();
        }
        break;

    case Message.APP_CONTEXT_GET: {
        attachment->setValue(this->appContext);
    } break;

        //remote with cms
    case Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED: {
        LayoutStruct layout = attachment->value<LayoutStruct>();
        QSettings settings;
        settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
        WindowsApp *windowsVideowallSelected = this->appContext->getWindowsVideowallSelected();
        if(windowsVideowallSelected != Q_NULLPTR){
            int idOfWidnowsSelected = windowsVideowallSelected->idWindows;
            settings.beginGroup("Workspace" + QString::number(idOfWidnowsSelected));
            settings.setValue("selected_layout_workspace", layout.code);
            settings.setValue("selected_page_workspace",layout.selectedPage);
            settings.endGroup();
        }
        settings.endGroup();
        if(this->appContext->getIsLoadDataWithDeviceId()){
            if(this->appContext->getIsNeedSynchronizedWithServer()){
                presentation()->refreshRemoteControl(layout);
            }else{
                presentation()->refreshPlayerRemoteControlWithLayoutNew(layout);
            }
        }else{
            presentation()->refresh(layout);
        }
    } break;

        //load with site
    case Message.APP_VIDEO_WALL_ZONE_PAGE_SELECTED: {
        LayoutStruct layout = attachment->value<LayoutStruct>();
        QSettings settings;
        settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
        WindowsApp *windowsVideowallSelected = this->appContext->getWindowsVideowallSelected();
        if(windowsVideowallSelected != Q_NULLPTR){
            int idOfWidnowsSelected = windowsVideowallSelected->idWindows;
            settings.beginGroup("Workspace" + QString::number(idOfWidnowsSelected));
            settings.setValue("selected_layout_workspace", layout.code);
            settings.setValue("selected_page_workspace",layout.selectedPage);
            settings.endGroup();
        }

        if(this->appContext->getIsLoadDataWithDeviceId()){
            if(this->appContext->getIsNeedSynchronizedWithServer()){
                //need synchronized with server auto page transition
                presentation()->refreshLayoutPageSelectedHandelRemoteControl(layout);
            }else{
                presentation()->refresh(layout);
            }
        }else{
            presentation()->refresh(layout);
        }
    }break;

    case Message.CMS_CHANGE_CAM_ORDER_REFRESH_WORKSPACE:{
        LayoutStruct layout = attachment->value<LayoutStruct>();
        presentation()->refeshWorkspaceWhenCmsChangeCamOrder(layout);
    }break;

    case Message.APP_VIDEO_WALL_SHOW_SITE_TOP: {
        siteSelector->newAction(message, attachment);
    } break;

    case Message.PLAYER_BEGIN_HIDE_FULLSCREEN_AUTO:{

    }break;

    case Message.PLAYER_BEGIN_SHOW_FULLSCREEN_AUTO:{
        //        int index = attachment->value<int>();

    }break;


    case Message.PLAYER_BEGIN_SHOW_FULLSCREEN: {
        WindowsApp *windowsAppSelectedCurrent = this->appContext->getWindowsVideowallSelected();
        if(windowsAppSelectedCurrent != Q_NULLPTR){
            QString windowsAppSelected = windowsAppSelectedCurrent->appName;
            QSettings settings;
            QString offlineMode = settings.value("offline_mode").toString();
            int index = attachment->value<int>();
            if(this->appContext->getIsLoadDataWithDeviceId()){
                if(this->appContext->getIsNeedSynchronizedWithServer() && offlineMode != "ON" && windowsAppSelected == this->appName){
                    int cameraIdFullScreen = 0;
                    QList<CamItem *> pageCamerasCurrentPlay = this->appContext->getListCamerasWorkingCurrent();
                    if(index != -1 && index != 0){
                        //if offline mode == off need update to server
                        cameraIdFullScreen = pageCamerasCurrentPlay.at(index)->getCameraId();
                        this->appContext->setCameraIdFullSceen(cameraIdFullScreen);
                        this->presentation()->updateOrderCameraToServer();
                    }
                }else{
                    // not need synchronzied || offline mode == ON || not same app
                    presentation()->showFullCellAt(index);
                }
            }else{
                //load with site
                presentation()->showFullCellAt(index);
            }
        }
    } break;

    case Message.PLAYER_BEGIN_HIDE_FULLSCREEN: {
        WindowsApp *windowsAppSelectedCurrent = this->appContext->getWindowsVideowallSelected();
        if(windowsAppSelectedCurrent != Q_NULLPTR){
            QString windowsAppSelected = windowsAppSelectedCurrent->appName;
            QSettings settings;
            QString offlineMode = settings.value("offline_mode").toString();
            int index = attachment->value<int>();
            if(this->appContext->getIsLoadDataWithDeviceId()){
                if(this->appContext->getIsNeedSynchronizedWithServer() && offlineMode != "ON" && windowsAppSelected == this->appName){
                    this->appContext->setCameraIdFullSceen(0);
                    this->presentation()->updateOrderCameraToServer();
                }else{
                    // not need synchronzied || offline mode == ON || not same app
                    presentation()->hideFullCellAt(index);
                }
            }else{
                //load with site
                presentation()->hideFullCellAt(index);
            }
        }
    } break;

        //load with site
    case Message.APP_VIDEO_WALL_UPDATE_PAGE_DEFAULT: {
        LayoutStruct defaultLayout = attachment->value<LayoutStruct>();
        QSettings settings;
        settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));

        WindowsApp *windowsVideowallSelected = this->appContext->getWindowsVideowallSelected();
        if(windowsVideowallSelected != Q_NULLPTR){
            int idOfWidnowsSelected = windowsVideowallSelected->idWindows;
            settings.beginGroup("Workspace" + QString::number(idOfWidnowsSelected));
            settings.setValue("selected_layout_workspace", defaultLayout.code);
            settings.setValue("selected_page_workspace",defaultLayout.selectedPage);
            settings.endGroup();
        }
        settings.endGroup();
        presentation()->updateDefaultLayout(defaultLayout);
    } break;


    case Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS_LOCAL:{
        DataOfWorkspace dataOfWorkspace = attachment->value<DataOfWorkspace>();
        LayoutStruct layoutStruct = dataOfWorkspace.layoutPageOfWorkspace;
        int pageNumber = layoutStruct.selectedPage;
        int layoutNumber = layoutStruct.numberOfCameras;
        presentation()->refreshNewLayoutPageLocal(pageNumber, layoutNumber);
    }break;

    case Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS:{
        presentation()->refreshNewLayoutPage();
    }break;

    case Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_SITE_ID_SUCCESS:{
        presentation()->refreshNewLayoutPage();
    }break;

    case Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID_SUCCESS:{
        presentation()->refreshDataForPlayerRemoteControl();
    }break;

    case Message.ENTER_FULLSCREEN_MODE :{
        presentation()->resizePlayerWhenEnterFullScreen();
    }break;

    case Message.EXIT_FULLSCREEN_MODE :{
        presentation()->resizePlayerWhenExitFullScreen();
    }break;


    default:
        qDebug() << "ERROR : General Internal pac action in" << getClass()
                 << "non-catched :" << Message.toString(message);
    }
}
