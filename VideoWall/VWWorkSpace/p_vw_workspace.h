#ifndef P_VW_WORKSPACE_H
#define P_VW_WORKSPACE_H

#include <PacModel/presentation.h>
#include <QEvent>
#include <QObject>
#include <QStackedLayout>
#include <QVBoxLayout>
#include <QWidget>
#include <QMutex>
#include "Authentication/appcontext.h"
#include "Player/cam9playerpool.h"
#include "c_vw_workspace.h"
#include <QPropertyAnimation>
#include "Common/LayoutSet.h"
class C_VWWorkSpace;

class P_VWWorkSpace : public Presentation {
private:
    WindowsApp *windowsAppOfWorkspace = Q_NULLPTR;
    bool checkLayoutPageSaved = false;

    LayoutSet *layoutSet = Q_NULLPTR;
    LayoutStruct layoutStructTmp; //use free space
    int rowsNewUseFreeSpace;

    bool isShowFullScreen = false;
    int animationDuration = 150;
    QString appName = "Video Wall";
    bool isAnimating = false;
    int selectedIndex = -1;
    QSize videoSize;
    int selectedIndexCurrent = -1;

    QList<CamItem *> pageCameras;
    QList<CamItem *> pageCamerasLast;
    QList<CamItem *> pageCamerasCurrentPlay;
    int numberOfPlayingPlayer = 0;

    bool firstRefresh = true;
    CamItemType networkTypeWorking;

    bool isShowing = false;
    bool isHiding = false;

    //zone + layout
    QWidget *zone = Q_NULLPTR;
    QGridLayout *gridLayout = Q_NULLPTR;
    QList<QWidget *> playerZoneList;
    QList<QWidget *> playerZoneListOrigin;

    LayoutStruct selectedLayout;
    LayoutStruct selectedLayoutLast;

    int cameraFullScreen = 0;
    bool fullScreenRemoteControlFirst = true;
    bool resizeZoneFirst = false;

    // Swap player area
    int indexSourcePlayer = -1;
    int indexTargetPlayer = -1;
    bool isClickable = true; // enable to swap
    bool isLockedSwap = false;
    bool isAllowSwapEveryTime = true;
    bool isAnimateSwap = true;
    int mCurrentSize = 0;

    int indexPlayerSourceOrderChange = -1;
    int indexPlayerTargetOrderChange = -1;
    int totalPageOfWorkspace = 0;

public:
    C_VWWorkSpace *control() { return (C_VWWorkSpace *)this->ctrl; }
    P_VWWorkSpace(WindowsApp *windowsApp, QString appName, Control *ctrl, QWidget *zone);
    void refreshPlayerZoneList(QWidget *newZone);

    void stopPlaying();
    void changeControl(Control *ctrl);
    void show(QWidget *zone);
    void update();
    QWidget *getZone(int zoneId);

    void refreshAppShow();
    void refresh(LayoutStruct layout);
    void refreshNewLayoutPage();

    void refreshDataWorkspace(QVariant *attachment);
    void loadLayoutPageDefaultOfWorkspace(WindowsApp *windowsApp);

    //load data local
    void refreshNewLayoutPageLocal(int page, int layout);
    //refresh when nvr change ip
    void refreshDataWhenNVRChangeIpOrNetworkDisconnect(QVariant *attachment);

    //refresh when pickup/kickout camera
    void refeshWorkspaceWhenCmsChangeCamOrder(LayoutStruct layoutStruct);
    //refresh data when remote control
    void refreshDataForPlayerRemoteControl();
    void refreshPlayerRemoteControlWithLayoutNew(LayoutStruct layoutNew);
    void refreshRemoteControl(LayoutStruct layout);
    void refreshNewLayoutPageRemoteControl();
    void refreshLayoutPageSelectedHandelRemoteControl(LayoutStruct layout);

    //calibrate
    void calibrateNumberPlayerDisplay(QList<CamItem *> listCameras);
    void calibrateTotalPageOfWorkspace();

    //update to server
    void updateOrderCameraToServer();
    void updateToServerCameraHideSceen();
    void updateToServerCameraFullSceen(int indexShowFullScreen);

    CamItem* findCamItemFullSceen(int cameraId,  QList<CamItem *> listCamItem);

    void updateDefaultLayout(LayoutStruct defaultLayout);

    bool getIsShowFullScreen() const;
    void setIsShowFullScreen(bool value);

    bool checkOrderCamChange();

    // Swap cell area
    void onStartSwapVideoCell(bool &,int &, int &);
    void onEndSwapVideoCell(bool &,int &, int &);
    void swapPlayerArea(bool &, int &, int &);

    //resize zone
    void resizePlayerWhenExitFullScreen();
    void resizePlayerWhenEnterFullScreen();
    //settings update
    void updateUseFreeSpace(QString useFreeSpace);
    void pageTransitionBegin();
    void networkChangeStatus(int message, QVariant *attachement);
    void updateModeDebug(int message,QVariant *attachment);
    void updateNetworkTypeSelected(CamItemType networkType);

    //get + set member
    LayoutStruct getSelectedLayout() const;

protected:
    bool eventFilter(QObject *watched, QEvent *event);

public Q_SLOTS:
    void showFullCellAt(int index);
    void hideFullCellAt(int index);

};

#endif  // PRESENTATION_H
