#include "p_vw_workspace.h"
#include "Authentication/appcontext.h"
#include "Common/resources.h"
#define Q_FOREACH(variable,container)                                                         \

/**
     * Generic method to override for updating the presention.
     **/

P_VWWorkSpace::P_VWWorkSpace(WindowsApp *windowsApp, QString appName, Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {

    this->windowsAppOfWorkspace = windowsApp;
    this->appName = appName;

    this->zone = zone;
    QRect screenGeometry = QApplication::desktop()->availableGeometry(this->zone);
    int withZone = screenGeometry.width();
    int heightZone = screenGeometry.height();

    this->zone->resize(withZone, heightZone );
    this->zone->installEventFilter(this);
    this->zone->setMouseTracking(true);

    // create panels for grid
    for (int index = 0; index < Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS;
         ++index) {
        QWidget *playerZone = new QWidget(this->zone);
        playerZoneList.append(playerZone);
        playerZoneListOrigin.append(playerZone);
    }

    layoutSet = new LayoutSet();
    this->selectedLayoutLast.numberOfCameras = -1;
    this->selectedLayout.numberOfCameras = -1;
    this->layoutStructTmp.numberOfCameras = -1;

    // init layout
    gridLayout = new QGridLayout(this->zone);
    gridLayout->setSpacing(4);
    gridLayout->setMargin(0);
    this->zone->setLayout(gridLayout);

    //cdn Type Default
    networkTypeWorking = control()->appContext->getNetworkType();
}

void P_VWWorkSpace::refreshPlayerZoneList(QWidget *newZone){

    this->zone->setAttribute(Qt::WA_DeleteOnClose);
    this->zone->close();
    for (int index = 0; index < Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS; ++index) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(index));
        cPlayer->newAction(Message.APP_VIDEOWALL_PLAYER_STOP_RENDERING, Q_NULLPTR);

    }
    this->zone = newZone;
    this->zone->setLayout(gridLayout);

    for (int index = 0; index < playerZoneList.size(); ++index) {
        QWidget *zonePlayer = playerZoneList.at(index);
        zonePlayer->setParent(this->zone);
    }
    for (int index = 0; index < Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS; ++index) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(index));
        cPlayer->newAction(Message.APP_VIDEOWALL_PLAYER_START_RENDERING, Q_NULLPTR);
    }
}


void P_VWWorkSpace::loadLayoutPageDefaultOfWorkspace(WindowsApp *windowsApp){
    int idOfWorkspace = windowsApp->idWindows;
    int layoutWorkspaceSaved = -1;
    int defaultLayoutIndex = 0;
    int pageDefault = 1;
    int pageWorkspaceSaved = -1;
    QSettings settings;
    settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
    settings.beginGroup("Workspace" + QString::number(idOfWorkspace));
    layoutWorkspaceSaved = settings.value("selected_layout_workspace", -1).toInt();
    pageWorkspaceSaved  = settings.value("selected_page_workspace", -1).toInt();
    settings.endGroup();

    if(layoutWorkspaceSaved >= 0){
        defaultLayoutIndex = layoutWorkspaceSaved;
    }

    if(pageWorkspaceSaved >= 0){
        pageDefault = pageWorkspaceSaved;
    }

    //save layout page for workspace
    settings.beginGroup("Workspace" + QString::number(idOfWorkspace));
    settings.setValue("selected_layout_workspace", defaultLayoutIndex);
    settings.setValue("selected_page_workspace", pageDefault);
    settings.endGroup();
    settings.endGroup();
    //layout with index
    LayoutStruct layoutWorkSpace = layoutSet->getLayout(defaultLayoutIndex);
    layoutWorkSpace.selectedPage = pageDefault;

    this->selectedLayout = layoutWorkSpace;
    this->layoutStructTmp = layoutWorkSpace;
    WindowsApp *windowsAppSelected = this->control()->appContext->getWindowsVideowallSelected();
    if(windowsAppSelected != Q_NULLPTR){
        if(windowsApp->idWindows == windowsAppSelected->idWindows){
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<WindowsApp *>(windowsApp);
            control()->newAction(Message.APP_VIDEOWALL_REFRESH_TAB_LAYOUT_PAGE , dataStruct);
        }
    }
}

void P_VWWorkSpace::refreshDataWorkspace(QVariant *attachment){
    WindowsApp *windowsApp = attachment->value<WindowsApp *>();
    if(windowsApp != Q_NULLPTR){
        if(windowsApp->stateOfWorkspace == StateOfWorkspace::PLAYING) return;
        qDebug() << Q_FUNC_INFO << "ID OF WINDOWS WORKSPACE" << windowsAppOfWorkspace->idWindows;
        loadLayoutPageDefaultOfWorkspace(windowsApp);
        refresh(this->selectedLayout);//load data of workspace
        windowsApp->stateOfWorkspace = StateOfWorkspace::PLAYING;
    }
}

void P_VWWorkSpace::refeshWorkspaceWhenCmsChangeCamOrder(LayoutStruct layoutStruct){
    CamSite *camSite = this->control()->appContext->getSiteCameras();
    if(camSite != Q_NULLPTR){
        LayoutStruct layoutWorkspace;
        if(this->control()->appContext->getIsLoadDataWithDeviceId()){
            int numberCamerasNew = camSite->getCamItems().size();
            if(numberCamerasNew < pageCamerasLast.size()){
                layoutWorkspace = layoutStruct;
                int idOfWorkspace = windowsAppOfWorkspace->idWindows;
                QSettings settings;
                settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
                //save layout page for workspace
                settings.beginGroup("Workspace" + QString::number(idOfWorkspace));
                settings.setValue("selected_layout_workspace", layoutStruct.code);
                settings.setValue("selected_page_workspace", layoutStruct.selectedPage);
                settings.endGroup();
                settings.endGroup();
            }else{
                layoutWorkspace = this->selectedLayout;
            }
        }else{
            //site
            layoutWorkspace = layoutStruct;
        }
        qDebug() << Q_FUNC_INFO << QString::number(windowsAppOfWorkspace->idWindows) << "layout " << layoutWorkspace.numberOfCameras << "page" << layoutWorkspace.selectedPage;

        refresh(layoutWorkspace);//load data of workspace
        WindowsApp *windowsAppSelected = this->control()->appContext->getWindowsVideowallSelected();
        if(windowsAppSelected != Q_NULLPTR){
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<WindowsApp *>(windowsAppSelected);
            control()->newAction(Message.APP_VIDEOWALL_REFRESH_TAB_LAYOUT_PAGE , dataStruct);
        }
    }
}

void P_VWWorkSpace::updateNetworkTypeSelected(CamItemType networkTypeSelected){
    networkTypeWorking = networkTypeSelected;
    if(this->appName.contains(this->control()->appContext->getWorkingApp().appName, Qt::CaseInsensitive)){
        QSettings settings;
        settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
        QList<QString> listCameras;
        settings.beginGroup(QString::number(windowsAppOfWorkspace->idWindows));
        int sizeCamerasSave = settings.beginReadArray("cameras");
        qDebug() << Q_FUNC_INFO << "windowsAppOfWorkspace ID" << windowsAppOfWorkspace->idWindows << "sizeCameras" << sizeCamerasSave;
        for (int index = 0; index < sizeCamerasSave; ++index) {
            settings.setArrayIndex(index);
            QString cameraIdString = settings.value("cameraid").toString();
            listCameras.append(cameraIdString);
        }
        settings.endArray();

        settings.beginWriteArray("cameras");
        for (int index = 0; index < listCameras.size(); ++index) {
            settings.setArrayIndex(index);
            settings.setValue("cameraid", listCameras.at(index));
            settings.setValue("network", networkTypeWorking.network);
            settings.setValue("data_source_camera" , networkTypeWorking.dataSource);
        }
        settings.endArray();
        settings.endGroup();
        if(this->control()->appContext->getIsLoadDataWithDeviceId()){
            refreshDataForPlayerRemoteControl();
        }else{
            refresh(this->selectedLayout);// refresh lại khi chọn luồng chơi mới
        }
    }
}


void P_VWWorkSpace::stopPlaying() {
    qDebug() << Q_FUNC_INFO;
    for (int camIndex = 0; camIndex < selectedLayout.numberOfCameras; ++camIndex) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(camIndex));
        cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
    }
    windowsAppOfWorkspace->stateOfWorkspace = StateOfWorkspace::STOPPED;
}

void P_VWWorkSpace::updateDefaultLayout(LayoutStruct defaultLayout) {
    if (!defaultLayout.label.isEmpty()) {
        this->selectedLayout = defaultLayout;
        this->layoutStructTmp = defaultLayout;
        qDebug() << "App Name Current" << this->appName;
        bool checkApp = this->appName.contains(control()->appContext->getWorkingApp().appName, Qt::CaseInsensitive);
        qDebug() << "checkApp" <<checkApp;
        if(checkApp){
            QSettings settings;
            QString offline_mode = settings.value("offline_mode").toString();
            if(offline_mode != "ON"){
                refresh(this->selectedLayout);
            }else{
                int page = this->selectedLayout.selectedPage;
                int layout = this->selectedLayout.numberOfCameras;
                refreshNewLayoutPageLocal(page, layout);
            }
        }
    }
}

void P_VWWorkSpace::refreshAppShow() {
    this->loadLayoutPageDefaultOfWorkspace(this->windowsAppOfWorkspace);
    if(control()->appContext->getIsLoadDataWithDeviceId()){
        refreshDataForPlayerRemoteControl();
    }else{
        qDebug() << Q_FUNC_INFO;
        if (!this->selectedLayout.label.isEmpty()) {
            refresh(this->selectedLayout);
        }
    }
}
//truong hop chon bang tay layoutpage
void P_VWWorkSpace::refreshLayoutPageSelectedHandelRemoteControl(LayoutStruct layoutNew) {
    this->selectedLayout = layoutNew;
    this->layoutStructTmp = layoutNew;
    //    refreshDataForPlayerRemoteControl();
    this->selectedIndex = -1;
    this->control()->appContext->setCameraIdFullSceen(0);
    updateOrderCameraToServer();
}

void P_VWWorkSpace::updateToServerCameraFullSceen(int indexShowFullScreen){
    if (isShowing) return;
    qDebug() << "updateToServerCameraFullSceen"  <<"indexShowFullScreen" <<indexShowFullScreen;
    isShowing = true;
    this->control()->appContext->setCameraIdFullSceen(pageCamerasCurrentPlay.at(indexShowFullScreen)->getCameraId());
    updateOrderCameraToServer();

}
void P_VWWorkSpace::updateToServerCameraHideSceen(){
    if (isHiding) return;
    qDebug() << "updateToServerCameraHideSceen" ;

    isHiding = true;
    this->control()->appContext->setCameraIdFullSceen(0);
    updateOrderCameraToServer();
}
void P_VWWorkSpace::updateModeDebug(int message,QVariant *attachment){
    for (int index = 0; index < Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS; ++index) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(index));
        cPlayer->newAction(message, attachment);
    }
}

void P_VWWorkSpace::updateUseFreeSpace(QString useFreeSpace){
    calibrateNumberPlayerDisplay(pageCamerasCurrentPlay);
}

void P_VWWorkSpace::updateOrderCameraToServer(){
    this->control()->appContext->setListCamerasWorkingCurrent(pageCamerasCurrentPlay);
    this->control()->appContext->setPageSelected(selectedLayout.selectedPage);
    this->control()->appContext->setLayoutSelected(selectedLayout.numberOfCameras);
    this->control()->newAction(Message.APP_VIDEO_WALL_UPDATE_CAMERA_ORDER_REMOTE_CONTROL, Q_NULLPTR);
}

LayoutStruct P_VWWorkSpace::getSelectedLayout() const
{
    return selectedLayout;
}

void P_VWWorkSpace::refreshRemoteControl(LayoutStruct layoutNew) {
    this->selectedLayout = layoutNew;
    this->layoutStructTmp = layoutNew;
    refreshNewLayoutPageRemoteControl();
}

void P_VWWorkSpace::pageTransitionBegin(){
    if(!this->appName.contains(this->control()->appContext->getWorkingApp().appName, Qt::CaseInsensitive)) return;
    int pageWorkspaceSelectedCurrent = this->selectedLayout.selectedPage;
    this->calibrateTotalPageOfWorkspace();
    if(totalPageOfWorkspace <= 1) return;
    if (pageWorkspaceSelectedCurrent >= totalPageOfWorkspace) {
        pageWorkspaceSelectedCurrent = 0;
    }
    pageWorkspaceSelectedCurrent = pageWorkspaceSelectedCurrent + 1;
    this->selectedLayout.selectedPage = pageWorkspaceSelectedCurrent;
    //refresh workspace
    this->refresh(this->selectedLayout);

    QSettings settings;
    settings.beginGroup(QString::number(this->control()->appContext->getWorkingUser()->getUserId()));
    int idOfWidnowsOfWorkspace = windowsAppOfWorkspace->idWindows;
    settings.beginGroup("Workspace" + QString::number(idOfWidnowsOfWorkspace));
    settings.setValue("selected_layout_workspace", this->selectedLayout.code);
    settings.setValue("selected_page_workspace",this->selectedLayout.selectedPage);
    settings.endGroup();
    settings.endGroup();
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<LayoutStruct>(this->selectedLayout);
    control()->newSystemAction(Message.WORKSPACE_PAGE_TRANSITION_BEGIN ,  dataStruct);
}

void P_VWWorkSpace::calibrateTotalPageOfWorkspace(){
    CamSite *camSite = control()->appContext->getSiteCameras();
    int numberOfCamerasPerPage = this->selectedLayout.numberOfCameras;
    int numberOfCameras, totalPage;
    if(camSite != Q_NULLPTR){
        if(control()->appContext->getIsLoadDataWithDeviceId()){
            //load with device
            numberOfCameras = camSite->getCamItems().size();
            totalPage = numberOfCameras / numberOfCamerasPerPage;
            if (numberOfCameras % numberOfCamerasPerPage != 0) {
                totalPage += 1;
            }
        }else{
            //load with site
            QSettings settings;
            QString offlineMode = settings.value("offline_mode").toString();
            if(offlineMode == "ON"){
                numberOfCameras = camSite->getCamItems().size();
            }else{
                numberOfCameras = camSite->getTotalCamItem();
            }

            totalPage = numberOfCameras / numberOfCamerasPerPage;
            if (numberOfCameras % numberOfCamerasPerPage != 0) {
                totalPage += 1;
            }
        }
    }
    totalPageOfWorkspace = totalPage;
}

void P_VWWorkSpace::refresh(LayoutStruct layoutNew) {
    Site *workingSite = control()->appContext->getWorkingSite();
    if(workingSite != Q_NULLPTR){
        int idWorkingSite = workingSite->getSiteId();
    }
    QString tokenUserWorking =
            control()->appContext->getWorkingUser()->getToken();
    int pageNumber = layoutNew.selectedPage;
    int layoutNumber = layoutNew.numberOfCameras;

    qDebug() << Q_FUNC_INFO << "pageNumber" << pageNumber << "layoutNumber" << layoutNumber;

    this->selectedLayout = layoutNew;
    this->layoutStructTmp = layoutNew;

    if(this->control()->appContext->getIsLoadDataWithDeviceId()){
        refreshNewLayoutPageLocal(pageNumber, layoutNumber);
    }else{
        //send message load list cameras of site with page and layout
        if(!layoutNew.label.isEmpty()){
            QVariant *dataStruct = new QVariant();
            DataOfWorkspace dataOfWorkspace;
            dataOfWorkspace.cVWWorkSpace= this->control();
            dataOfWorkspace.layoutPageOfWorkspace = layoutNew;
            dataStruct->setValue<DataOfWorkspace>(dataOfWorkspace);
            control()->newUserAction(Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE, dataStruct);
        }
    }


    //api 003

    //    if(!layoutNew.label.isEmpty()){
    //        this->selectedLayout = layoutNew;
    //        QVariant *dataStruct = new QVariant();
    //        dataStruct->setValue<LayoutStruct>(layoutNew);
    //        control()->newUserAction(Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_SITE_ID, dataStruct);
    //    }

    // get all cam of site
    /*  CamSite::getCamerasBy(
      idWorkingSite, tokenUserWorking,
      [this](CamSite *camSites) {
        control()->appContext->setListCamItemsOfSite(camSites->getCamItems());
      },
      [this](QString message) { qDebug() << Q_FUNC_INFO << message; });*/
}


void P_VWWorkSpace::calibrateNumberPlayerDisplay(QList<CamItem *> listCameras){
    if(listCameras.size() == 0) return;
    qDebug() << Q_FUNC_INFO << "layout " << this->selectedLayout.numberOfCameras << "page before calibrate" <<this->selectedLayout.selectedPage;

    QList<CamItem *> allCameras = this->control()->appContext->getSiteCameras()->getCamItems();
    int sizeAllCamera = allCameras.size();
    for (int index = 0; index < playerZoneList.size(); ++index) {
        QWidget *playerZone = playerZoneList.at(index);
        playerZone->hide();
        playerZone->lower();
        gridLayout->removeWidget(playerZone);
    }

    QSettings settings;
    QString userFreeSpaceSaved = settings.value("use_free_space").toString();
    int sizeListCameras = listCameras.size();
    if(userFreeSpaceSaved == "ON"/* && this->selectedLayout.selectedPage == this->control()->appContext->getTotalPages()*/){
        int indexLayoutTemp = 0;
        QList<int> listlayout;
        for (int index = 0; (index < layoutSet->layoutList.size()) &&
             (layoutSet->layoutList.at(index).numberOfCameras <=
              Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS);
             ++index) {
            if(layoutSet->layoutList.at(index).numberOfCameras <= sizeListCameras){
                listlayout.append(index);
            }
        }
        //get value max
        if (!listlayout.isEmpty()) {
            qSort(listlayout.begin(), listlayout.end());
            indexLayoutTemp = listlayout.last();
            if(sizeListCameras > layoutSet->layoutList.at(indexLayoutTemp).numberOfCameras){
                indexLayoutTemp+=1;
            }
        }

        if ((indexLayoutTemp < layoutSet->layoutList.size()) &&
                (layoutSet->layoutList.at(indexLayoutTemp).numberOfCameras <=
                 Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS)) {
            layoutStructTmp = layoutSet->layoutList.at(indexLayoutTemp);
            qDebug() << "layoutStructTmp" <<"layoutStructTmp Number cameras" <<layoutStructTmp.numberOfCameras;
        }

        //if number camera same layouttmp
        bool checkSameLayoutTmp = false;
        for (int index = 0; (index < layoutSet->layoutList.size()) &&
             (layoutSet->layoutList.at(index).numberOfCameras <=
              Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS);
             ++index) {
            if(layoutSet->layoutList.at(index).numberOfCameras == layoutStructTmp.numberOfCameras && sizeListCameras == layoutSet->layoutList.at(index).numberOfCameras){
                checkSameLayoutTmp = true;
            }
        }

        bool isChangeLayoutDisplay = false;
        if(!checkSameLayoutTmp){
            if ((layoutStructTmp.numberOfCameras % sizeListCameras) >= layoutStructTmp.cols) {
                isChangeLayoutDisplay = true;
            }
        }

        if (isChangeLayoutDisplay) {
            rowsNewUseFreeSpace = this->layoutStructTmp.rows - 1;
            if (layoutStructTmp.numberOfCameras >= 4) {
                if(rowsNewUseFreeSpace < 2) rowsNewUseFreeSpace = 2;
            }
            if (layoutStructTmp.numberOfCameras == 1) {
                rowsNewUseFreeSpace = 1;
            }
        }else{
            rowsNewUseFreeSpace = this->layoutStructTmp.rows;
        }
    }else{
        layoutStructTmp = this->selectedLayout;
        rowsNewUseFreeSpace = this->layoutStructTmp.rows;
    }


    for (int row = 0; row < rowsNewUseFreeSpace; ++row) {
        for (int col = 0; col < layoutStructTmp.cols; ++col) {
            int zoneIndex = row * layoutStructTmp.cols + col;
            if (zoneIndex < playerZoneList.size()) {
                if(zoneIndex < listCameras.size()){
                    CamItem *camItem = listCameras.at(zoneIndex);
                    if (camItem != Q_NULLPTR) {
                        camItem->setLayoutSelectedCurrent(this->layoutStructTmp);
                        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                                    control(), Q_NULLPTR, appName, QString::number(zoneIndex));
                        QVariant *dataStruct = new QVariant();
                        dataStruct->setValue(camItem);
                        cPlayer->newAction(Message.PLAYER_UPDATE_INFO_USE_FREE_SPACE,
                                           dataStruct);
                        qDebug() << "PLAYER_UPDATE_INFO" << QString::number(zoneIndex);
                    }
                }

                QWidget *playerZone = playerZoneList.at(zoneIndex);
                this->gridLayout->addWidget(playerZone, row, col);
                playerZone->show();
                playerZone->raise();

            }
        }
    }
}

void P_VWWorkSpace::refreshNewLayoutPage(){
    if(this->control()->appContext->getIsLoadDataWithDeviceId()) return;
    qDebug() << "P_VWWorkSpace::refreshNewLayoutPage" <<"THREAD CURRENT" << QThread::currentThread();
    qDebug() <<"app name" << this->appName << "refreshNewLayoutPage" << "SIze page camera" << pageCameras.size() << "Selected layout" << this->selectedLayout.numberOfCameras << "selected Page" << this->selectedLayout.selectedPage;
    // get list camItem with layoutpage
    pageCameras = this->control()->appContext->getSiteCameras()->getCamItems(); //get all camares of layoutpage current

    qDebug() << "pageCameras Size" << pageCameras.size();
    for (int index = 0; index < playerZoneList.size(); ++index) {
        QWidget *playerZone = playerZoneList.at(index);
        playerZone->hide();
        playerZone->lower();
        gridLayout->removeWidget(playerZone);
    }

    if(pageCameras.size() == 0){
        QWidget *playerZone = playerZoneList.at(0);
        this->gridLayout->addWidget(playerZone, 0, 0);
        playerZone->show();
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(0));
        cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
        return;
    }

    //player unsubscribe crawler
    for (int index = 0; index < selectedLayoutLast.numberOfCameras; ++index) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(control(), Q_NULLPTR, appName, QString::number(index));
        cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
    }

    //calibrate display list player
    calibrateNumberPlayerDisplay(pageCameras);


    if (selectedIndexCurrent != -1) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName,
                    QString::number(selectedIndexCurrent));
        cPlayer->newAction(Message.PLAYER_EXIT_POP_OUT_MODE, Q_NULLPTR);
    }

    if (this->layoutStructTmp.cols > 0 && this->layoutStructTmp.rows > 0) {
        videoSize = QSize(this->zone->width() / this->layoutStructTmp.cols,
                          this->zone->height() / this->layoutStructTmp.rows);
    }

    numberOfPlayingPlayer = 0;
    pageCamerasCurrentPlay.clear();
    for (int camIndex = 0; camIndex < pageCameras.size(); ++camIndex) {
        CamItem *camItem = pageCameras.at(camIndex);
        if (camItem != Q_NULLPTR) {
            // count actual player used in the page
            numberOfPlayingPlayer++;

            camItem->setIsMain(this->selectedLayout.numberOfCameras <= 4 ? true : false);
            camItem->setNetworkType(networkTypeWorking);
            camItem->setOrder(camIndex);
            camItem->setLayoutSelectedCurrent(this->layoutStructTmp);

            C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                        control(), Q_NULLPTR, appName, QString::number(camIndex));
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue(camItem);
            pageCamerasCurrentPlay.append(camItem);
            cPlayer->newAction(Message.PLAYER_NEW_LIVE_SOURCE_SET,
                               dataStruct);
            qDebug() << "PLAYER_NEW_LIVE_SOURCE_SET" << QString::number(camIndex);
        }
    }



    if (numberOfPlayingPlayer < this->selectedLayoutLast.numberOfCameras) {
        for (int index = numberOfPlayingPlayer;
             index < this->selectedLayoutLast.numberOfCameras; ++index) {
            C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                        control(), Q_NULLPTR, appName, QString::number(index));
            cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
        }
    }

    for (int index = 0; index < this->selectedLayoutLast.numberOfCameras; ++index) {
        if(index < 0 || index >= Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) continue;
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(index));
        cPlayer->newAction(Message.RESET_SHOW_FULL_SCREEN, Q_NULLPTR);
    }

    this->selectedLayoutLast = this->selectedLayout;
}


//refresh with data local
void P_VWWorkSpace::refreshNewLayoutPageLocal(int pageNumber, int layoutNumber){
    //    if(this->control()->appContext->getIsLoadDataWithDeviceId()) return;
    qDebug() << "P_VWWorkSpace::refreshNewLayoutPageLocal" <<"Windows id----" << this->windowsAppOfWorkspace->idWindows <<"THREAD CURRENT" << QThread::currentThread() << "pageNumber" <<pageNumber << "layoutNumber" << layoutNumber;
    // get list camItem with layoutpage
    pageCameras.clear();
    QList<CamItem *> listCamItems = this->control()->appContext->getSiteCameras()->getCamItems(); //get all camares of layoutpage current
    pageCamerasLast = listCamItems;

    if(layoutNumber >= listCamItems.size()){
        pageCameras = listCamItems;
    }else{
        for (int index = ((pageNumber-1)*layoutNumber); index < ((pageNumber-1)*layoutNumber) + layoutNumber; ++index) {
            if(index < 0 || index >= listCamItems.size()) break;
            CamItem *camItem = listCamItems[index];
            pageCameras.append(camItem);
        }
    }

    for (int index = 0; index < playerZoneList.size(); ++index) {
        QWidget *playerZone = playerZoneList.at(index);
        playerZone->hide();
        playerZone->lower();
        gridLayout->removeWidget(playerZone);
    }

    if(pageCameras.size() == 0){
        QWidget *playerZone = playerZoneList.at(0);
        this->gridLayout->addWidget(playerZone, 0, 0);
        playerZone->show();
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(0));
        cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
        return;
    }

    //player unsubscribe crawler
    for (int index = 0; index < selectedLayoutLast.numberOfCameras; ++index) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(index));
        cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
    }


    if (selectedIndexCurrent != -1) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName,
                    QString::number(selectedIndexCurrent));
        cPlayer->newAction(Message.PLAYER_EXIT_POP_OUT_MODE, Q_NULLPTR);
    }


    //calibrate display list player
    calibrateNumberPlayerDisplay(pageCameras);

    if (this->layoutStructTmp.cols > 0 && this->layoutStructTmp.rows > 0) {
        videoSize = QSize(this->zone->width() / this->layoutStructTmp.cols,
                          this->zone->height() / this->layoutStructTmp.rows);
    }

    numberOfPlayingPlayer = 0;
    pageCamerasCurrentPlay.clear();

    //    qDebug() << "selectedLayout " <<selectedLayout.numberOfCameras << selectedLayout.rows <<selectedLayout.cols << selectedLayout.label<<selectedLayout.selectedPage;

    for (int camIndex = 0; camIndex < pageCameras.size(); ++camIndex) {
        CamItem *camItem = pageCameras.at(camIndex);
        if (camItem != Q_NULLPTR) {
            // count actual player used in the page
            numberOfPlayingPlayer++;

            camItem->setIsMain(this->selectedLayout.numberOfCameras <= 4 ? true : false);
            camItem->setNetworkType(networkTypeWorking);
            camItem->setOrder(camIndex);
            camItem->setLayoutSelectedCurrent(this->layoutStructTmp);

            C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                        control(), Q_NULLPTR, appName, QString::number(camIndex));
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue(camItem);
            pageCamerasCurrentPlay.append(camItem);

            cPlayer->newAction(Message.PLAYER_NEW_LIVE_SOURCE_SET,
                               dataStruct);
        }
    }



    if (numberOfPlayingPlayer < this->selectedLayoutLast.numberOfCameras) {
        for (int index = numberOfPlayingPlayer;
             index < this->selectedLayoutLast.numberOfCameras; ++index) {
            C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                        control(), Q_NULLPTR, appName, QString::number(index));
            cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
        }
    }

    for (int index = 0; index < this->selectedLayoutLast.numberOfCameras; ++index) {
        if(index < 0 || index >= Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) continue;
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(index));
        cPlayer->newAction(Message.RESET_SHOW_FULL_SCREEN, Q_NULLPTR);
    }

    this->selectedLayoutLast = this->selectedLayout;
}

CamItem* P_VWWorkSpace::findCamItemFullSceen(int cameraId,  QList<CamItem *> listCamItem){
    CamItem* camItemResult = Q_NULLPTR;
    for (int index = 0; index < listCamItem.size(); ++index) {
        CamItem* camItem = listCamItem.at(index);
        if(camItem != Q_NULLPTR){
            if(cameraId == camItem->getCameraId()){
                camItemResult = camItem;
                break;
            }
        }
    }
    return camItemResult;
}


void P_VWWorkSpace::refreshDataForPlayerRemoteControl(){
    if(this->appName.contains(this->control()->appContext->getWorkingApp().appName, Qt::CaseInsensitive) == false) return;

    Cam9PlayerPool::instance().resetIndexPlayers(appName,playerZoneList);
    for (int index = 0; index < playerZoneList.size(); ++index) {
        QWidget *playerZone = playerZoneList.at(index);
        playerZone->hide();
        playerZone->lower();
        gridLayout->removeWidget(playerZone);
    }
    pageCameras.clear();
    pageCamerasLast.clear();
    pageCameras = this->control()->appContext->getSiteCameras()->getCamItems(); //get all camares of layoutpage current
    qDebug() <<"app name" << this->appName << "refreshDataForPlayerRemoteControl" << "SIze page camera" << pageCameras.size() << "Selected layout" << this->selectedLayout.numberOfCameras << "selected Page" << this->selectedLayout.selectedPage;
    pageCamerasLast = pageCameras;
    if(pageCameras.size() == 0){
        QWidget *playerZone = playerZoneList.at(0);
        this->gridLayout->addWidget(playerZone, 0, 0);
        playerZone->show();
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(0));
        cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
        return;
    }

    //player unsubscribe crawler
    for (int index = 0; index < selectedLayoutLast.numberOfCameras; ++index) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(index));
        cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
    }

    if (selectedIndexCurrent != -1) {
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName,
                    QString::number(selectedIndexCurrent));
        cPlayer->newAction(Message.PLAYER_EXIT_POP_OUT_MODE, Q_NULLPTR);

    }



    pageCamerasCurrentPlay.clear();
    if(this->selectedLayout.numberOfCameras >= pageCameras.size()){
        pageCamerasCurrentPlay = pageCameras;
    }else{
        for (int camIndex = 0; camIndex < selectedLayout.numberOfCameras; ++camIndex) {
            int camIndexOrder = camIndex + (this->selectedLayout.numberOfCameras *(this->selectedLayout.selectedPage - 1));
            //            qDebug() << "camIndexOrder"  <<camIndexOrder <<this->selectedLayout.numberOfCameras << this->selectedLayout.selectedPage ;
            if(camIndexOrder >= pageCameras.size() || camIndexOrder < 0) continue;
            CamItem *camItem = pageCameras[camIndexOrder];
            if (camItem != Q_NULLPTR) {
                pageCamerasCurrentPlay.append(camItem);
            }
        }
    }

    //calibrate display list player
    calibrateNumberPlayerDisplay(pageCamerasCurrentPlay);

    if (this->layoutStructTmp.cols > 0 && this->layoutStructTmp.rows > 0) {
        videoSize = QSize(this->zone->width() / this->layoutStructTmp.cols,
                          this->zone->height() / this->layoutStructTmp.rows);
    }

    numberOfPlayingPlayer = 0;
    for (int camIndex = 0; camIndex < pageCamerasCurrentPlay.size(); ++camIndex) {
        CamItem *camItem = pageCamerasCurrentPlay[camIndex];
        if (camItem != Q_NULLPTR) {
            // count actual player used in the page
            numberOfPlayingPlayer++;
            camItem->setIsMain(this->selectedLayout.numberOfCameras <= 4 ? true : false);
            camItem->setNetworkType(networkTypeWorking);
            camItem->setOrder(camIndex);
            camItem->setLayoutSelectedCurrent(this->layoutStructTmp);
            C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                        control(), Q_NULLPTR, appName, QString::number(camIndex));

            qDebug() << "refreshDataForPlayerRemoteControl player " <<  cPlayer->getDisplayName() << "VIDEOWALL" << "Position cam" << camItem->getPostion() ;
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue(camItem);
            cPlayer->newAction(Message.PLAYER_NEW_LIVE_SOURCE_SET,
                               dataStruct);
        }
    }

    for (int index = 0; index < pageCamerasCurrentPlay.size(); ++index) {
        qDebug() << "pageCamerasCurrentPlay" <<"order" <<pageCamerasCurrentPlay.at(index)->getOrder() << "Positon" <<pageCamerasCurrentPlay.at(index)->getPostion();
    }

    //update pageCamerasCurrentPlay
    this->control()->appContext->setListCamerasWorkingCurrent(pageCamerasCurrentPlay);

    if (numberOfPlayingPlayer < this->selectedLayoutLast.numberOfCameras) {
        for (int index =  numberOfPlayingPlayer;
             index < this->selectedLayoutLast.numberOfCameras; ++index) {
            C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                        control(), Q_NULLPTR, appName, QString::number(index));
            cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);

        }
    }

    for (int index = 0; index < this->selectedLayoutLast.numberOfCameras; ++index) {
        if(index < 0 || index >= Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) continue;
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(index));
        cPlayer->newAction(Message.RESET_SHOW_FULL_SCREEN, Q_NULLPTR);
    }

    this->selectedLayoutLast = this->selectedLayout;
}

bool P_VWWorkSpace::checkOrderCamChange(){
    if(pageCameras.size() == 0 || pageCamerasLast.size() == 0 || pageCameras.size() !=  pageCamerasLast.size()) return false;
    for (int index = 0; index < pageCamerasLast.size(); ++index) {
        CamItem *camItem =  pageCamerasLast.at(index);
        if(camItem){
            camItem->setIndexOfCamInListCamera(index);
        }
    }

    indexPlayerSourceOrderChange = -1;
    indexPlayerTargetOrderChange = -1;
    for (int index = 0; index < pageCamerasLast.size(); ++index) {
        CamItem *camItemCurrent = pageCameras.at(index);
        CamItem *camItemLast = pageCamerasLast.at(index);
        if(camItemCurrent != Q_NULLPTR && camItemLast != Q_NULLPTR ){
            if(camItemCurrent->getCameraId() != camItemLast->getCameraId()){
                int orderCamera = pageCamerasLast.at(index)->getIndexOfCamInListCamera();
                if(indexPlayerSourceOrderChange < 0)
                {
                    indexPlayerSourceOrderChange = orderCamera;
                    qDebug() << "checkOrderCamChange"  << "indexPlayerSourceOrderChange" <<indexPlayerSourceOrderChange;
                }else{
                    indexPlayerTargetOrderChange = orderCamera;
                    qDebug() << "checkOrderCamChange"  << "indexPlayerTargetOrderChange" <<indexPlayerTargetOrderChange;

                    return true;
                }
            }
        }
    }
    return false;
}


void P_VWWorkSpace::refreshPlayerRemoteControlWithLayoutNew(LayoutStruct layoutNew){
    this->selectedLayout = layoutNew;
    this->layoutStructTmp = layoutNew;

    refreshDataForPlayerRemoteControl();
    cameraFullScreen = this->control()->appContext->getSiteCameras()->getPlayCamFullScreen();
    if(pageCameras.size() == 0){
        QWidget *playerZone = playerZoneList.at(0);
        this->gridLayout->addWidget(playerZone, 0, 0);
        playerZone->show();
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(0));
        cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
        return;
    }

    //ban dau chua co kich thuoc chuan
    if(cameraFullScreen != 0 && cameraFullScreen != -1){
        CamItem *camItemFullScreen = findCamItemFullSceen(cameraFullScreen, pageCamerasCurrentPlay);
        if(camItemFullScreen){
            int orderCam = camItemFullScreen->getOrder();
            cameraFullScreen = 0;
            QTimer::singleShot(1000,this, [this, orderCam]{
                C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                            control(), Q_NULLPTR, appName, QString::number(orderCam));
                cPlayer->newAction(Message.DEVICE_ON_SHOW_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
            });
        }
    }else{
        if(this->selectedIndex != -1){
            C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                        control(), Q_NULLPTR, appName, QString::number(this->selectedIndex));
            cPlayer->newAction(Message.DEVICE_ON_HIDE_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
        }
    }
}

void P_VWWorkSpace::refreshNewLayoutPageRemoteControl(){
    if(this->appName.contains(this->control()->appContext->getWorkingApp().appName, Qt::CaseInsensitive) == false) return;
    if(this->control()->appContext->getIsLoadDataWithSiteId()) return;
    //update data cameras working current
    qDebug() << "P_VWWorkSpace::refreshNewLayoutPageRemoteControl" <<"THREAD CURRENT" << QThread::currentThread();
    // get list camItem with layoutpage
    //    pageCameras = this->control()->appContext->getSiteCameras()->getCamItems(); //get all camares of layoutpage current
    //    refreshDataForPlayerRemoteControl();
    //    cameraFullScreen = this->control()->appContext->getSiteCameras()->getPlayCamFullScreen();

    //    //ban dau chua co kich thuoc chuan
    //    if(!fullScreenRemoteControlFirst){
    //        if(cameraFullScreen != 0 && cameraFullScreen != -1){
    //            CamItem *camItemFullScreen = findCamItemFullSceen(cameraFullScreen, pageCameras);
    //            if(camItemFullScreen){
    //                int orderCam = camItemFullScreen->getOrder();
    //                cameraFullScreen = 0;
    //                QTimer::singleShot(1000,this, [this, orderCam]{
    //                    showFullCellAt(orderCam);
    //                    C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
    //                                control(), Q_NULLPTR, appName, QString::number(orderCam));

    //                    cPlayer->newAction(Message.DEVICE_ON_SHOW_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
    //                });
    //            }
    //        }else{
    //            if(this->selectedIndex != -1){
    //                hideFullCellAt(this->selectedIndex);
    //                C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
    //                            control(), Q_NULLPTR, appName, QString::number(this->selectedIndex));
    //                cPlayer->newAction(Message.DEVICE_ON_HIDE_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
    //            }
    //        }
    //    }
    //    fullScreenRemoteControlFirst = false;

    pageCameras.clear();
    pageCameras = this->control()->appContext->getSiteCameras()->getCamItems(); //get all camares of layoutpage current

    QList<CamItem *> listCameraWithLayoutPage;
    //Kiểm tra list danh sách camera đang chơi so với layout mà workspace nó đang chọn
    for (int camIndex = 0; camIndex < selectedLayout.numberOfCameras; ++camIndex) {
        int camIndexOrder = camIndex + (this->selectedLayout.numberOfCameras *(this->selectedLayout.selectedPage - 1));
        qDebug() << "camIndexOrder"  <<camIndexOrder <<this->selectedLayout.numberOfCameras << this->selectedLayout.selectedPage ;
        if(camIndexOrder >= pageCameras.size() || camIndexOrder < 0) continue;
        CamItem *camItem = pageCameras[camIndexOrder];
        if (camItem != Q_NULLPTR) {
            listCameraWithLayoutPage.append(camItem);
        }
    }

    if(listCameraWithLayoutPage.size() != pageCamerasCurrentPlay.size()){
        refreshDataForPlayerRemoteControl();
    }

    qDebug() << "refreshNewLayoutPageRemoteControl Size camera" << pageCameras.size();
    cameraFullScreen = this->control()->appContext->getSiteCameras()->getPlayCamFullScreen();
    if(pageCameras.size() == 0){
        QWidget *playerZone = playerZoneList.at(0);
        this->gridLayout->addWidget(playerZone, 0, 0);
        playerZone->show();
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(0));
        cPlayer->newAction(Message.PLAYER_SOURCE_CLEAR, Q_NULLPTR);
        return;
    }
    //pick up/ kick out camera
    if(pageCameras.size() != pageCamerasLast.size()){
        refreshDataForPlayerRemoteControl();
    }

    qDebug() << "Full Screen" << cameraFullScreen;
    qDebug() << "\n";
    //ban dau chua co kich thuoc chuan
    //    if(fullScreenRemoteControlFirst){
    //        //nếu lần đầu refresh dữ liệu
    //        refreshDataForPlayerRemoteControl();
    //    }else{
    //các lần tiếp remote control nếu có camera full screen
    if(cameraFullScreen != 0){
        if(checkOrderCamChange()){
            refreshDataForPlayerRemoteControl();
        }


        //Kiểm tra thay đổi của cms có đã xóa đi cam đang full hiện tại không
        CamItem *camItemFullScreenCurrent = findCamItemFullSceen(cameraFullScreen, pageCameras);
        if(camItemFullScreenCurrent == Q_NULLPTR){
            refreshDataForPlayerRemoteControl();
            for (int index = 0; index < this->selectedLayoutLast.numberOfCameras; ++index) {
                if(index < 0 || index >= Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) continue;
                C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                            control(), Q_NULLPTR, appName, QString::number(index));
                cPlayer->newAction(Message.RESET_SHOW_FULL_SCREEN, Q_NULLPTR);
            }

        }else{
            //have camera full
            CamItem *camItemFullScreen = findCamItemFullSceen(cameraFullScreen, pageCamerasCurrentPlay);
            if(camItemFullScreen != Q_NULLPTR){
                if(pageCameras.size() != pageCamerasLast.size()){
                    // trường hợp có cam đang full và đồng thời có pickup/kickout cam
                    refreshDataForPlayerRemoteControl();
                    //nếu tìm thấy camitem fullscreen nằm đúng trong danh sách cam đang chơi hiện tại thì full cam lên
                    int orderCam = camItemFullScreen->getOrder();
                    qDebug() << "camItemFullScreen Order" <<orderCam;
                    if(this->selectedIndex != -1){
                        if(control()->appContext->getLayoutSelected() == 1 ) return;
                        hideFullCellAt(this->selectedIndex);
                        if(this->selectedIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;
                        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                                    control(), Q_NULLPTR, appName, QString::number(this->selectedIndex));
                        cPlayer->newAction(Message.DEVICE_ON_HIDE_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
                    }

                    QTimer::singleShot(1000,this, [this, orderCam]{
                        if(control()->appContext->getLayoutSelected() == 1 ) return;
                        showFullCellAt(orderCam);
                        if(this->selectedIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;
                        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                                    control(), Q_NULLPTR, appName, QString::number(orderCam));
                        cPlayer->newAction(Message.DEVICE_ON_SHOW_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
                    });

                }else{

                    //nếu tìm thấy camitem fullscreen nằm đúng trong danh sách cam đang chơi hiện tại thì full cam lên
                    int orderCam = camItemFullScreen->getOrder();
                    qDebug() << "camItemFullScreen Order" <<orderCam;

                    if(this->selectedIndex != -1){
                        if(control()->appContext->getLayoutSelected() ==1 ) return;
                        hideFullCellAt(this->selectedIndex);
                        if(this->selectedIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;
                        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                                    control(), Q_NULLPTR, appName, QString::number(this->selectedIndex));
                        cPlayer->newAction(Message.DEVICE_ON_HIDE_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
                    }

                    QTimer::singleShot(1000,this, [this, orderCam]{
                        if(control()->appContext->getLayoutSelected() ==1 ) return;
                        showFullCellAt(orderCam);
                        if(this->selectedIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;
                        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                                    control(), Q_NULLPTR, appName, QString::number(orderCam));
                        cPlayer->newAction(Message.DEVICE_ON_SHOW_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
                    });
                }
            }else{
                //neu cam cần fullscreen không nằm trong danh sách các cam đang chơi
                refreshDataForPlayerRemoteControl();
            }
        }
    }else{
        //dang full thi hide
        if(this->selectedIndex != -1){
            if(this->selectedLayoutLast.selectedPage !=  this->layoutStructTmp.selectedPage || this->layoutStructTmp.numberOfCameras != this->selectedLayoutLast.numberOfCameras){
                //neu full o page khac layout/page hien tai
                refreshDataForPlayerRemoteControl();

            }else{
                hideFullCellAt(this->selectedIndex);
                if(this->selectedIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;
                C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                            control(), Q_NULLPTR, appName, QString::number(this->selectedIndex));
                cPlayer->newAction(Message.DEVICE_ON_HIDE_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
            }
        }else{
            if(checkOrderCamChange()){
                int minIndexOfLayoutPageCurrent = (this->layoutStructTmp.numberOfCameras *this->layoutStructTmp.selectedPage) - this->layoutStructTmp.numberOfCameras  ;
                int maxIndexOfLayoutPageCurrent = (this->layoutStructTmp.numberOfCameras *this->layoutStructTmp.selectedPage) - 1;
                if(indexPlayerSourceOrderChange >= minIndexOfLayoutPageCurrent && indexPlayerSourceOrderChange <= maxIndexOfLayoutPageCurrent
                        && indexPlayerTargetOrderChange >= minIndexOfLayoutPageCurrent && indexPlayerTargetOrderChange <= maxIndexOfLayoutPageCurrent ){
                    pageCamerasLast.swap(indexPlayerSourceOrderChange, indexPlayerTargetOrderChange);

                    indexPlayerSourceOrderChange = indexPlayerSourceOrderChange - (this->selectedLayoutLast.numberOfCameras *(this->selectedLayoutLast.selectedPage-1));
                    indexPlayerTargetOrderChange = indexPlayerTargetOrderChange - (this->selectedLayoutLast.numberOfCameras *(this->selectedLayoutLast.selectedPage-1));
                    qDebug() << "playerZoneList indexPlayerSourceOrderChange" <<"SWAP" <<indexPlayerSourceOrderChange << "indexPlayerTargetOrderChange"  <<indexPlayerTargetOrderChange;
                    swapPlayerArea(isLockedSwap, indexPlayerSourceOrderChange,indexPlayerTargetOrderChange);
                }else{//nếu swap ở trang khác
                    refreshDataForPlayerRemoteControl();
                }
            }else{
                //sự kiện chuyển layout page cần cập nhật
                refreshDataForPlayerRemoteControl();
            }
        }
    }
    //    }
    this->selectedLayoutLast = this->selectedLayout;
    //    fullScreenRemoteControlFirst = false;
}


void P_VWWorkSpace::networkChangeStatus(int message, QVariant *attachement) {
    for (int camIndex = 0; camIndex < selectedLayout.numberOfCameras; ++camIndex) {
        if(camIndex >= pageCameras.size()) continue;
        if(camIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(camIndex));
        cPlayer->newAction(message, attachement);
    }
}

void P_VWWorkSpace::show(QWidget *zone) { Q_UNUSED(zone) }

void P_VWWorkSpace::update() {}

QWidget *P_VWWorkSpace::getZone(int zoneId) {
    switch (zoneId) {
    default:
        return playerZoneList.at(zoneId);
    }
}

void P_VWWorkSpace::showFullCellAt(int index) {
    qDebug() << "showFullCellAt" << "INDEX " << index;
    if(pageCamerasCurrentPlay.size() == 0 || layoutStructTmp.numberOfCameras <= 1)  return;
    isShowing = true;
    if (index >= 0 && index < this->numberOfPlayingPlayer) {
        this->selectedIndex = index;
        this->selectedIndexCurrent = index;
        if(this->selectedIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(selectedIndex));
        if(cPlayer == Q_NULLPTR) return;
        QWidget *playerZone = cPlayer->presentation()->getZone();
        gridLayout->removeWidget(playerZone);
        playerZone->raise();
        QPropertyAnimation *geometryAnimation =
                new QPropertyAnimation(playerZone, "geometry");
        geometryAnimation->setDuration(animationDuration);
        QRect startValue = QRect(playerZone->x(), playerZone->y(),
                                 playerZone->width(), playerZone->height());

        QRect endValue = QRect(0, 0, this->zone->width(), this->zone->height());
        geometryAnimation->setStartValue(startValue);
        geometryAnimation->setEndValue(endValue);
        geometryAnimation->setEasingCurve(QEasingCurve::Linear);
        geometryAnimation->start(QPropertyAnimation::DeleteWhenStopped);
        connect(geometryAnimation, &QPropertyAnimation::finished, this,
                [this, cPlayer, index] {
            CamItem *camItemSelected = Q_NULLPTR;
            if(this->control()->appContext->getIsLoadDataWithDeviceId()){
                //                int camItemIndexOrder = index + (this->layoutStructTmp.numberOfCameras *(this->layoutStructTmp.selectedPage-1));
                camItemSelected = pageCamerasCurrentPlay.at(index);
            }else{
                camItemSelected = pageCameras.at(index);
            }
            if(camItemSelected){
                LayoutStruct layoutStruct;
                layoutStruct.numberOfCameras = 1;
                camItemSelected->setLayoutSelectedCurrent(layoutStruct);

                QVariant *dataStruct= new QVariant();
                dataStruct->setValue(camItemSelected);
                cPlayer->newAction(Message.PLAYER_UPDATE_INFO_WHEN_SHOW_FULL_SCREEN , dataStruct);
                //                dropLinkDownloadForSavingBandwidth(); // for saving bw
            }

            cPlayer->newAction(Message.PLAYER_END_SHOW_FULLSCREEN,
                               Q_NULLPTR);
        });
    }
    isShowing = false;
}
void P_VWWorkSpace::hideFullCellAt(int index) {
    if(pageCamerasCurrentPlay.size() == 0 || layoutStructTmp.numberOfCameras <= 1)  return;
    isHiding = true;
    if (index >= 0 && index < numberOfPlayingPlayer) {
        //        stopDropLinkDownloadForSavingBandwidth();
        qDebug() << "hideFullCellAt selectedIndex" <<selectedIndex << "layoutStructTmp" <<"col" <<layoutStructTmp.cols << "rowsNewUseFreeSpace" << rowsNewUseFreeSpace << "this->layoutStructTmp.rows" <<this->layoutStructTmp.rows;
        selectedIndex = index;
        selectedIndexCurrent = index;
        if(this->selectedIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;

        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(selectedIndex));
        if(cPlayer == Q_NULLPTR) return;

        int row = selectedIndex / this->layoutStructTmp.rows;
        int col = selectedIndex - row * this->layoutStructTmp.rows;
        qDebug() << Q_FUNC_INFO << "row" << row << "col" << col;
        QWidget *playerZone = cPlayer->presentation()->getZone();
        playerZone->raise();

        QPropertyAnimation *geometryAnimation =
                new QPropertyAnimation(playerZone, "geometry");
        geometryAnimation->setDuration(animationDuration);
        videoSize = QSize(this->zone->width() / this->layoutStructTmp.cols,
                          this->zone->height() / this->rowsNewUseFreeSpace);
        QRect startValue = QRect(playerZone->x(), playerZone->y(),
                                 playerZone->width(), playerZone->height());
        QRect endValue = QRect(col * videoSize.width(), row * videoSize.height(),
                               videoSize.width(), videoSize.height());

        geometryAnimation->setStartValue(startValue);
        geometryAnimation->setEndValue(endValue);
        geometryAnimation->setEasingCurve(QEasingCurve::Linear);
        geometryAnimation->start(QPropertyAnimation::DeleteWhenStopped);
        connect(geometryAnimation, &QPropertyAnimation::finished, this,
                [this, cPlayer, playerZone,index] {
            CamItem *camItemSelected = Q_NULLPTR;
            if(this->control()->appContext->getIsLoadDataWithDeviceId()){
                //                int camItemIndexOrder = index + (this->layoutStructTmp.numberOfCameras *(this->layoutStructTmp.selectedPage-1));
                camItemSelected = pageCamerasCurrentPlay.at(index);
            }else{
                camItemSelected = pageCameras.at(index);
            }
            if(camItemSelected){
                //hide hd Button
                QVariant *dataStructPlayer = new QVariant();
                dataStructPlayer->setValue(cPlayer);
                cPlayer->newAction(Message.VIDEO_WALL_PLAYER_SWITCH_TO_SD_WHEN_EXIT_FULL_SCREEN , dataStructPlayer);
                cPlayer->newAction(Message.PLAYER_RESET_RENDER_WIDGET, Q_NULLPTR);
                camItemSelected->setLayoutSelectedCurrent(this->layoutStructTmp);
                qDebug() << Q_FUNC_INFO << "hideFullCellAt" <<this->layoutStructTmp.numberOfCameras;
                QVariant *dataStruct1= new QVariant();
                dataStruct1->setValue(camItemSelected);
                cPlayer->newAction(Message.PLAYER_UPDATE_INFO_WHEN_HIDE_FULL_SCREEN , dataStruct1);
            }

            int row = selectedIndex / this->layoutStructTmp.rows;
            int col = selectedIndex - row * this->layoutStructTmp.rows;
            this->gridLayout->addWidget(playerZone, row, col);
            this->selectedIndex = -1;

            cPlayer->newAction(Message.PLAYER_END_HIDE_FULLSCREEN,
                               Q_NULLPTR);
        });
    }
    isHiding = false;
}


void P_VWWorkSpace::resizePlayerWhenEnterFullScreen(){
    //neu van dang full screen  thi resize player
    if(selectedIndex != - 1){
        if(this->selectedIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(selectedIndex));
        if(cPlayer == Q_NULLPTR) return;
        cPlayer->newAction(Message.ENTER_FULLSCREEN_MODE, Q_NULLPTR);
    }

}
void P_VWWorkSpace::resizePlayerWhenExitFullScreen(){
    //neu van dang full screen  thi resize player
    if(selectedIndex != - 1){
        if(this->selectedIndex > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return;
        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                    control(), Q_NULLPTR, appName, QString::number(selectedIndex));
        if(cPlayer == Q_NULLPTR) return;
        cPlayer->newAction(Message.EXIT_FULLSCREEN_MODE, Q_NULLPTR);
    }

}

bool P_VWWorkSpace::eventFilter(QObject *watched, QEvent *event) {
    QWidget *sender = qobject_cast<QWidget *>(watched);
    if (sender != Q_NULLPTR && sender == this->zone) {
        switch (event->type()) {
        case QEvent::Resize: {
            QResizeEvent *resizeEvent = (QResizeEvent *)(event);
            QSize newSize = resizeEvent->size();
            for (int camIndex = 0; camIndex < selectedLayout.numberOfCameras; ++camIndex) {
                C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
                            control(), Q_NULLPTR, appName, QString::number(camIndex));
                if(camIndex >= Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) break;
                QWidget*zonePlayer = cPlayer->presentation()->getZone();
                zonePlayer->update();
            }

            //            if(!resizeZoneFirst && this->control()->appContext->getIsLoadDataWithDeviceId() && (this->control()->appContext->getLayoutSelected() != 1)){
            //                if(cameraFullScreen != 0 && cameraFullScreen !=-1){
            //                    CamItem *camItemFullScreen = findCamItemFullSceen(cameraFullScreen, pageCameras);
            //                    if(camItemFullScreen){
            //                        int orderCam = camItemFullScreen->getOrder();
            //                        if(orderCam >= this->control()->appContext->getLayoutSelected()) return false;
            //                        showFullCellAt(orderCam);
            //                        if(orderCam > Message.APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS) return false;

            //                        C_Cam9RTCPlayer *cPlayer = Cam9PlayerPool::instance().getCam9RTCPlayer(
            //                                    control(), Q_NULLPTR, appName, QString::number(orderCam));
            //                        if(cPlayer == Q_NULLPTR) return false;
            //                        cPlayer->newAction(Message.DEVICE_ON_SHOW_FULL_SCREEN_CLICK_AUTO, Q_NULLPTR);
            //                        cameraFullScreen = - 1;
            //                        resizeZoneFirst = true;
            //                    }
            //                }
            //            }
        } break;
        default:
            break;
        }
    }
    return true;
}




void P_VWWorkSpace::swapPlayerArea(bool &isLockedSwap,int &indexSourcePlayer, int &indexTargetPlayer) {
    qDebug() << "START SWAP VIDEO CELL" << "indexSourcePlayer" << indexSourcePlayer << "indexTargetPlayer" <<indexTargetPlayer;
    if(indexSourcePlayer < 0 || indexTargetPlayer < 0) return;
    if (isAllowSwapEveryTime) {
        this->onStartSwapVideoCell(isLockedSwap, indexSourcePlayer, indexTargetPlayer);
        if (isAnimateSwap) {
            QWidget *playerSourceZone = playerZoneList.at(indexSourcePlayer);
            QWidget *playerTagetZone = playerZoneList.at(indexTargetPlayer);
            playerSourceZone->raise();
            playerTagetZone->raise();

            if(playerSourceZone == Q_NULLPTR || playerTagetZone == Q_NULLPTR) return;
            QPoint sourcePos = playerSourceZone->pos();
            QPoint targetPos = playerTagetZone->pos();
            qDebug() << "SWAP" << sourcePos << "->" << targetPos;

            QPropertyAnimation *sourceAnimation = new QPropertyAnimation(playerSourceZone, "pos");
            sourceAnimation->setDuration(animationDuration);
            sourceAnimation->setStartValue(sourcePos);
            sourceAnimation->setEndValue(targetPos);
            sourceAnimation->start(QAbstractAnimation::DeleteWhenStopped);

            QPropertyAnimation *targetAnimation = new QPropertyAnimation(playerTagetZone, "pos");
            targetAnimation->setDuration(animationDuration);
            targetAnimation->setStartValue(targetPos);
            targetAnimation->setEndValue(sourcePos);
            targetAnimation->start(QAbstractAnimation::DeleteWhenStopped);

            connect(targetAnimation, &QPropertyAnimation::finished, this, [this, &isLockedSwap, &indexSourcePlayer, &indexTargetPlayer] {
                this->onEndSwapVideoCell(isLockedSwap, indexSourcePlayer, indexTargetPlayer);
            });
        } else {
            this->onEndSwapVideoCell(isLockedSwap, indexSourcePlayer, indexTargetPlayer);
        }
    } else {
        indexSourcePlayer = -1;
        indexTargetPlayer = -1;
        qDebug() << "SWAP" << "one of two or both video aren't playing";
    }
}

void P_VWWorkSpace::onStartSwapVideoCell(bool &isLockedSwap,int &indexSourcePlayer, int &indexTargetPlayer) {
    isLockedSwap = true;
}


void P_VWWorkSpace::onEndSwapVideoCell(bool &isLockedSwap,int &indexSourcePlayer, int &indexTargetPlayer) {
    qDebug() << "onEndSwapVideoCell" << "indexSourcePlayer" << indexSourcePlayer << "indexTargetPlayer" <<indexTargetPlayer;
    if(indexSourcePlayer < 0 || indexTargetPlayer < 0) return;
    if (isLockedSwap) {
        mCurrentSize = (int)sqrt(this->control()->appContext->getLayoutSelected());
        qDebug() << "START_SWAP" << isLockedSwap << indexSourcePlayer << indexTargetPlayer;

        //    mCamSite->getCamItems()->swap(indexSourcePlayer, indexTargetPlayer);
        //    mVideoCellWidgets.swap(indexSourcePlayer, indexTargetPlayer);
        //    int zoneIndex = row * this->layoutStructTmp.cols + col;
        QWidget *playerSourceZone;
        QWidget *playerTagetZone;
        playerSourceZone = playerZoneList.at(indexSourcePlayer);
        playerTagetZone = playerZoneList.at(indexTargetPlayer);
        playerSourceZone->raise();
        playerTagetZone->raise();

        if(playerSourceZone == Q_NULLPTR || playerTagetZone == Q_NULLPTR) return;
        int sourceRow = (indexSourcePlayer / mCurrentSize) ;
        int sourceCol = indexSourcePlayer - (sourceRow * mCurrentSize);
        int targetRow = indexTargetPlayer / mCurrentSize;
        int targetCol = indexTargetPlayer - (targetRow * mCurrentSize);
        QPoint sourcePos = playerSourceZone->pos();
        QPoint targetPos = playerTagetZone->pos();
        gridLayout->removeWidget(playerSourceZone);
        gridLayout->addWidget(playerSourceZone, targetRow, targetCol);
        gridLayout->removeWidget(playerTagetZone);
        gridLayout->addWidget(playerTagetZone, sourceRow, sourceCol);

        playerZoneList.swap(indexPlayerSourceOrderChange, indexPlayerTargetOrderChange);
        pageCamerasCurrentPlay.at(indexPlayerSourceOrderChange)->setOrder(indexPlayerTargetOrderChange);
        pageCamerasCurrentPlay.at(indexPlayerTargetOrderChange)->setOrder(indexPlayerSourceOrderChange);
        pageCamerasCurrentPlay.swap(indexPlayerSourceOrderChange,indexPlayerTargetOrderChange );
        Cam9PlayerPool::instance().swapPlayer(appName,indexPlayerSourceOrderChange, indexPlayerTargetOrderChange);
        isLockedSwap = false;
        indexSourcePlayer = -1;
        indexTargetPlayer = -1;
    }
}


bool P_VWWorkSpace::getIsShowFullScreen() const
{
    return isShowFullScreen;
}

void P_VWWorkSpace::setIsShowFullScreen(bool value)
{
    isShowFullScreen = value;
}
