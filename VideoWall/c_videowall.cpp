#include "VideoWall/c_videowall.h"
/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
void C_VideoWall::setAppName(const QString &value)
{
    appName = value;
}

C_VideoWall::C_VideoWall(Control* ctrl, QWidget* zone) : Control(ctrl) {
    this->appName = appName;
    // update app context
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();

    // init facets
    this->abst = new A_VideoWall(this);
    this->pres = new P_VideoWall(this, zone);
    this->zone = zone;
    cLayoutPage = new C_VWLayoutPage(this, presentation()->getZone(2));
    cWindowsManager = new C_WindowsManager(this, presentation()->getZone(1));
    qDebug() << "C_VideoWall end";
}

void C_VideoWall::loadWorkspaceWindows(){
    //load workspace
    QSettings settings;
    QString isShowWindowsList =  settings.value("show_windows_list").toString();
    settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
    int sizeCamerasSave = settings.beginReadArray("Workspaces");
    if(appContext->getIsShowWindowsList() == "SHOW"){
        if(sizeCamerasSave > 0){
            for (int index = 0; index < sizeCamerasSave; ++index) {
                cWindowsManager->presentation()->createNewWindowsAppVideowall(false);
            }
            settings.endArray();
        }else{
            cWindowsManager->presentation()->createNewWindowsAppVideowall(false);
        }
    }else{
        //default
        cWindowsManager->presentation()->createNewWindowsAppVideowall(false);
    }
    settings.endGroup();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VideoWall::newUserAction(int message, QVariant* attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case Message.APP_CONTEXT_GET:
        break;

    default:
        qDebug() << "ERROR : General User action in" << getClass()
                 << "non-catched :" << Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VideoWall::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_CONTEXT_GET:
        attachment->setValue(this->appContext);
        break;

    default:
        qDebug() << "ERROR : General System action in" << getClass()
                 << "non-catched :" << Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VideoWall::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.VIDEO_WALL_RENDER_UNSUBSCRIBE_CRAWLER_CAMERA:
    case Message.VIDEO_WALL_RENDER_SUBSCRIBE_CRAWLER_CAMERA:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.UPDATE_DATA_NORMAL_MODE_AFTER_NETWORK_IS_UNREACHABLE:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.CMS_CHANGE_CAM_ORDER_REFRESH_WORKSPACE:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_UPDATE_DATA_LOCAL_SUCCESS:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.WORKSPACE_PAGE_TRANSITION_BEGIN:{
        cLayoutPage->newAction(message, attachment);
    }break;

    case Message.UPDATE_STATE_SHOW_WINDOWS_LIST:{
        cLayoutPage->newAction(message, attachment);
    }break;

    case Message.APP_VIDEO_WALL_HIDE_FULLSCREEN_WORKSPACE_ORIGIN:
    case Message.APP_VIDEO_WALL_SHOW_FULLSCREEN_WORKSPACE_ORIGIN:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_VIDEO_WALL_CLOSE_FULL_SCREEN_WORKSPACE:
    case Message.APP_VIDEO_WALL_CLOSE_POP_UP_WORKSPACE:
    case Message.APP_VIDEO_WALL_DELETE_WORKSPACE:
    case Message.APP_VIDEO_WALL_FULL_SCREEN_WORKSPACE:
    {
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_REFRESH_TAB_LAYOUT_PAGE:{
        cLayoutPage->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_SHOW_WINDOWS_POP_UP_SELECTED:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_CLOSED_WINDOWS:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_POP_UP_WINDOWS_SELECTED:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_NEW_WINDOWS_SELECTED:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_ADD_NEW_WINDOWS:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_UPDATE_LIST_WINDOWS:{
        cLayoutPage->newAction(message, attachment);
    }break;


    case Message.UPDATE_STATE_USE_FREE_SPACE:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS_LOCAL:{
        cWindowsManager->newAction(message,attachment);
    }break;

    case Message.UPDATE_MODE_RENDER_DEBUG:{
        cWindowsManager->newAction(message,attachment);
    }break;

    case Message.APP_VIDEO_WALL_UPDATE_CAMERA_ORDER_REMOTE_CONTROL:{
        this->getParent()->newAction(message, attachment);
    }break;


    case Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED_HANDEL:{
        cWindowsManager->newAction(message,attachment);
    }break;

    case Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID_SUCCESS:{
        cWindowsManager->newAction(message,attachment);
    }break;

    case Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID:{
        getParent()->newAction(message,attachment);
    }break;

    case Message.APP_VIDEO_WALL_LAYOUT_PAGE_UPDATE_REMOTE_CONTROL:{
        cLayoutPage->newAction(message, attachment);
    }break;

    case Message.NVR_CHANGE_IP_NEED_REFRESH:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_PLAY_BACK_GET_CLIP_RECORD_ERROR:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.UPDATE_CDN_TYPE_SELECTED:{
        cWindowsManager->newAction(message,attachment);
    }break;

    case Message.APP_NETWORK_IS_REACHABLE:
    {
        //        qDebug() << QDateTime::currentDateTime() << "APP_NETWORK_IS_REACHABLE";
        cWindowsManager->newAction(message, attachment);
    }
        break;
    case Message.APP_NETWORK_IS_UNREACHABLE:
    {
        //        qDebug() << QDateTime::currentDateTime() << "APP_NETWORK_IS_UNREACHABLE";
        cWindowsManager->newAction(message, attachment);

    }
        break;

    case Message.APP_SHOW_SETTINGS: {
        getParent()->newAction(message, attachment);
    } break;

    case Message.APP_CHANGED: {
        QString userAppName = attachment->value<QString>();
        if(this->appName.contains(userAppName, Qt::CaseInsensitive) == false){
            cWindowsManager->newAction(message, attachment);
        }
    } break;

    case Message.SITE_CHANGED: {
        cLayoutPage->newAction(message, attachment);
    } break;

    case Message.APP_SHOW: {
        this->appContext->setWorkingApp("Video Wall");
        ((P_VideoWall*)this->pres)->show(this->zone);
        cWindowsManager->newAction(Message.APP_SHOW, attachment);
    } break;

    case Message.APP_CONTEXT_GET:{
        attachment->setValue(this->appContext);
    }
        break;

    case Message.APP_VIDEO_WALL_ZONE_PAGE_SELECTED:
        cWindowsManager->newAction(message, attachment);
        break;

    case Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED: {
        cWindowsManager->newAction(message, attachment);

    } break;


    case Message.APP_VIDEO_WALL_SHOW_SITE_TOP:{
        cWindowsManager->newAction(message, attachment);
    }
        break;

    case Message.ENTER_FULLSCREEN_MODE: {
        presentation()->enterFullscreenMode();
        cWindowsManager->newAction(message, attachment);

    } break;

    case Message.EXIT_FULLSCREEN_MODE: {
        presentation()->exitFullscreenMode();
        cWindowsManager->newAction(message, attachment);
    } break;

    case Message.APP_VIDEO_WALL_LAYOUT_DEFAULT_SET: {
        cLayoutPage->newAction(message, attachment);

    } break;

    case Message.PAGE_TRANSITION_BEGIN: {
        cWindowsManager->newAction(message, attachment);
    } break;

    case Message.APP_VIDEO_WALL_UPDATE_PAGE_DEFAULT: {
        cWindowsManager->newAction(message, attachment);
    } break;

    case Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE:{
        this->getParent()->newAction(message,attachment);
    }break;

    case Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_SITE_ID_SUCCESS:{
        cWindowsManager->newAction(message, attachment);
    }break;

    case Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_SITE_ID:{
        this->getParent()->newAction(message,attachment);
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" << getClass()
                 << "non-catched :" << Message.toString(message);
    }
}
