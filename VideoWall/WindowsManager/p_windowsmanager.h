#ifndef P_WindowsManager_H
#define P_WindowsManager_H

#include <PacModel/presentation.h>
#include <QGraphicsDropShadowEffect>
#include <QHBoxLayout>
#include <QLabel>
#include <QObject>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QStackedWidget>
#include <QToolButton>
#include <QVBoxLayout>
#include <functional>
#include "Common/resources.h"
#include "c_windowsmanager.h"
#include "windowwidget.h"
#include <QStackedLayout>
class C_VideoWall;
class C_WindowsManager;
class WindowsApp;
class P_WindowsManager : public Presentation {
    Q_OBJECT
private:
    QWidget *zone;
    QStackedLayout *stackLayout;
    QWidget *zoneParentSelectedLast;
    QWidget *zoneWorkspaceFirst = Q_NULLPTR;
public:
    P_WindowsManager(Control *ctrl, QWidget *zone);
    C_WindowsManager *control() { return (C_WindowsManager *)this->ctrl; }
    void changeControl(Control *ctrl);
    void update();
    QWidget *getZone();
    //    QWidget *getZoneFirst();

    void createNewWindowsAppVideowall(bool needLoadData);
    void displayNewWindowsSelected(WindowsApp *windowsNewSelected);
    void destroyWindows(QWidget *zone);
    void onClosedwindows(WindowsApp *windowsApp);
    void popUpWorkspaceWindows(WindowsApp *windowsApp);
    void deleteWindowsSelected(WindowsApp *windowsApp);
    void showFullScreenWindows(WindowsApp *windowsApp);
    void hideFullScreenWindows(WindowsApp *windowsApp);
    void selectPreviousWorkspace(int idOfWindowsAppSelected);
    void setCurrentStackWindows(WindowsApp *windowsApp);
    void setCurrentWidgetDisplay(QWidget *widget);
    QList<QWidget *> listWidget;
public Q_SLOTS:
    void onCloseWidget(WindowWidget* windowsWidget, QSize sizeWidget , QPoint postionWidget, int screenCurrent);
    void onResizedWidget(WindowWidget *windowsWidget, QSize sizeWidget , QPoint postionWidget, int screenCurrent);
    void onMovedWidget(WindowWidget *windowsWidget, QSize sizeWidget , QPoint postionWidget, int screenCurrent);
    void closePopUpWindowsApp(WindowsApp *windowsApp);
protected:
    bool eventFilter(QObject *watched, QEvent *event);
};

#endif  // P_WindowsManager_H
