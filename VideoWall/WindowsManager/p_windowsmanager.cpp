#include "p_windowsmanager.h"

/**
     * Generic method to override for updating the presention.
     **/

P_WindowsManager::P_WindowsManager(Control *ctrl, QWidget *zone) : Presentation(ctrl) {
    QSize screenSize = Resources::instance().getScreenSize();
    this->zone = zone;
    stackLayout = new QStackedLayout(this->zone);
    stackLayout->setSpacing(0);
    stackLayout->setMargin(0);
    this->zone->setLayout(this->stackLayout);
    this->zone->installEventFilter(this);
    //  this->zone->setStyleSheet(
    //      "background-color: #333; border: 0px solid #8a8a92; border-radius: "
    //      "5px;");
}

void P_WindowsManager::deleteWindowsSelected(WindowsApp *windowsAppSelected){
    if(windowsAppSelected != Q_NULLPTR){
        //        //delete windowsapp
        //        //stop player
        //        C_VWWorkSpace *cVWWorkSpaceFree = windowsAppSelected->cWorkspace;
        //        cVWWorkSpaceFree->newAction(Message.PREPARE_FREE_WORKSPACE_NEED_PLAYER_STOP , Q_NULLPTR);
        //        //remove player in cam9playerpool
        //        Cam9PlayerPool::instance().removePlayerWithAppName(windowsAppSelected->appName);
        //        //can delete object windowsAppSelected

        //        //refresh id of windows object
        //        QList<WindowsApp *> listWindowsAppVideowallTempChange;
        //        for (int index = 0; index < listWindowsAppVideowallTemp.size(); ++index) {
        //            WindowsApp *windowsApp = listWindowsAppVideowallTemp.at(index);
        //            windowsApp->changeIdWindows(index);
        //            listWindowsAppVideowallTempChange.append(windowsApp);
        //        }



        //        control()->listWindowsAppVideowall = listWindowsAppVideowallTempChange;


        QList<WorkspaceWindows> listAllWorkSpaceWindowsSaved = control()->getAllWorkspacesWindows();
        QList<WorkspaceWindows> listAllWorkSpaceWindowsNew;
        for (int index = 0; index < listAllWorkSpaceWindowsSaved.size(); ++index) {
            WorkspaceWindows workspaceWindowsSave = listAllWorkSpaceWindowsSaved.at(index);
            WorkspaceWindows workspaceWindowsTemp;
            if(workspaceWindowsSave.workspaceId != QString::number(windowsAppSelected->idWindows)){
                workspaceWindowsTemp.workspaceId = QString::number(index);
                workspaceWindowsTemp.workspaceName =  "Workspace" + QString::number(index);
                workspaceWindowsTemp.stateWindows = workspaceWindowsSave.stateWindows;
                workspaceWindowsTemp.geometryOfwindowsParent = workspaceWindowsSave.geometryOfwindowsParent;
                workspaceWindowsTemp.positonWindows = workspaceWindowsSave.positonWindows;
                workspaceWindowsTemp.sizeWindows = workspaceWindowsSave.sizeWindows;
                workspaceWindowsTemp.screenOfWindows = workspaceWindowsSave.screenOfWindows;
                listAllWorkSpaceWindowsNew.append(workspaceWindowsTemp);
            }
        }

        control()->saveAllWindowsWorkspaces(listAllWorkSpaceWindowsNew);
        QSettings settings;
        settings.beginGroup(QString::number(this->control()->appContext->getWorkingUser()->getUserId()));
        //save select to disk
        settings.setValue("selected_workspace", 0);
        settings.endGroup();
        //refresh app
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }
}

void P_WindowsManager::showFullScreenWindows(WindowsApp *windowsAppSelected){
    WorkspaceWindows workspaceWindows;
    workspaceWindows.workspaceName = windowsAppSelected->windowsName;
    workspaceWindows.workspaceId = QString::number(windowsAppSelected->idWindows);
    workspaceWindows.stateWindows = 2;
    control()->saveWorkspaceWindows(workspaceWindows);

    if(windowsAppSelected->idWindows == 0){
        //show full screen mainframe
        control()->newUserAction(Message.APP_VIDEO_WALL_SHOW_FULLSCREEN_WORKSPACE_ORIGIN, Q_NULLPTR);
    }else{
        QWidget *zoneParent = windowsAppSelected->zoneParent;
        if(zoneParent != Q_NULLPTR){
            zoneParent->showFullScreen();
        }
    }
}

void P_WindowsManager::hideFullScreenWindows(WindowsApp *windowsAppSelected){
    WorkspaceWindows workspaceWindows;

    if(windowsAppSelected->idWindows == 0){
        workspaceWindows.workspaceName = windowsAppSelected->windowsName;
        workspaceWindows.workspaceId = QString::number(windowsAppSelected->idWindows);
        workspaceWindows.stateWindows = 0;
    }else{
        workspaceWindows.workspaceName = windowsAppSelected->windowsName;
        workspaceWindows.workspaceId = QString::number(windowsAppSelected->idWindows);
        workspaceWindows.stateWindows = 1;
    }

    control()->saveWorkspaceWindows(workspaceWindows);

    if(windowsAppSelected->idWindows == 0){
        //show full screen mainframe
        control()->newUserAction(Message.APP_VIDEO_WALL_HIDE_FULLSCREEN_WORKSPACE_ORIGIN, Q_NULLPTR);
    }else{
        QWidget *zoneParent = windowsAppSelected->zoneParent;
        if(zoneParent != Q_NULLPTR){
            zoneParent->showNormal();
            zoneParent->raise();
        }
    }
}


void P_WindowsManager::onClosedwindows(WindowsApp *windowsApp){
    C_VWWorkSpace *cVWWorkspace = windowsApp->cWorkspace;
    windowsApp->zoneParent->lower();
    if(windowsApp->zoneParent != this->zone){
        windowsApp->zoneParent->hide();
    }
    QWidget *zoneWorkspaceOld = windowsApp->zone;
    zoneWorkspaceOld->hide();
    zoneWorkspaceOld->lower();

    QWidget *zoneWorkspaceNew = new QWidget(this->zone);
    windowsApp->zoneParent = this->zone;
    windowsApp->zone = zoneWorkspaceNew;

    listWidget.insert(windowsApp->idWindows,zoneWorkspaceNew);
    stackLayout->addWidget(zoneWorkspaceNew);
    if(windowsApp->idWindows == control()->selectedWindows->idWindows){
        cVWWorkspace->newAction(Message.APP_VIDEOWALL_WORKSPACE_STOP_PLAYER_WORKING , Q_NULLPTR);
        //refresh app
        cVWWorkspace->setZone(zoneWorkspaceNew);
        cVWWorkspace->refreshPlayerList(zoneWorkspaceNew);
        setCurrentWidgetDisplay(zoneWorkspaceNew);
        //after show need load data
        QVariant *dataStructLoadLayoutPage = new QVariant();
        dataStructLoadLayoutPage->setValue<WindowsApp *>(windowsApp);
        cVWWorkspace->newAction(Message.APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE, dataStructLoadLayoutPage);

        //stop player previous and not pop up
        for (int index = 0; index <  control()->listWindowsAppVideowall.size(); ++index) {
            WindowsApp *windowsAppPrev = control()->listWindowsAppVideowall.at(index);
            if(windowsAppPrev->idWindows < windowsApp->idWindows && windowsAppPrev->stateOfWindows == StateOfWindows::NOT_POP_UP){
                if(windowsAppPrev != Q_NULLPTR){
                    C_VWWorkSpace *cVWWorkspacePre = windowsAppPrev->cWorkspace;
                    if(cVWWorkspacePre != Q_NULLPTR){
                        cVWWorkspacePre->newAction(Message.APP_VIDEOWALL_WORKSPACE_STOP_PLAYER_WORKING , Q_NULLPTR);
                    }
                }
            }
        }
    }else{
        qDebug() <<Q_FUNC_INFO <<  "selectedWindows ID" << control()->selectedWindows->idWindows;
        setCurrentWidgetDisplay(control()->selectedWindows->zone);
        cVWWorkspace->newAction(Message.APP_VIDEOWALL_WORKSPACE_STOP_PLAYER_WORKING , Q_NULLPTR);
        //refresh app
        cVWWorkspace->setZone(zoneWorkspaceNew);
        cVWWorkspace->refreshPlayerList(zoneWorkspaceNew);
    }
}

//QWidget * P_WindowsManager::getZoneFirst(){
//    QWidget *zoneReturn = new QWidget(this->zone);
//    zoneReturn->resize(this->zone->size());
//    stackLayout->addWidget(zoneReturn);
//    stackLayout->setCurrentWidget(zoneReturn);
//    return zoneReturn;
//}

QWidget *P_WindowsManager::getZone(){
    return this->zone;
}
void P_WindowsManager::createNewWindowsAppVideowall(bool needLoadData){
    int sizeWindows = control()->listWindowsAppVideowall.size();
    WindowsApp *windowsApp = new WindowsApp(sizeWindows);
    windowsApp->needLoadData = needLoadData;
    QWidget *newWindows = new QWidget(this->zone);
    windowsApp->zoneParent = this->zone;
    newWindows->resize(this->zone->size());
    QString nameNewWindows = "Workspace" + QString::number(windowsApp->idWindows);
    windowsApp->initWindowsApp(newWindows, nameNewWindows);
    control()->addWindowsApp(windowsApp); //app new cvideowall
    newWindows->setWindowTitle(nameNewWindows);

    //add to stack

    this->stackLayout->addWidget(newWindows);
    listWidget.insert(windowsApp->idWindows, newWindows);

    if(needLoadData == true){
        setCurrentWidgetDisplay(newWindows);
    }else{
        //load first
        if(windowsApp->idWindows == 0){
            control()->appContext->setWindowsVideowallSelected(windowsApp); //selected default
            control()->setSelectedWindows(windowsApp); // selectedWindows
            setCurrentWidgetDisplay(windowsApp->zone);
        }
        //        QSettings settings;
        //        settings.beginGroup(QString::number(this->control()->appContext->getWorkingUser()->getUserId()));
        //        int selectedWorkspaceId = settings.value("selected_workspace", -1).toInt();
        //        if(selectedWorkspaceId == -1 && windowsApp->stateOfWindows == StateOfWindows::NOT_POP_UP){
        //            this->stackLayout->setCurrentWidget(newWindows);
        //        }

        //        QString workspaceIdString = QString::number(windowsApp->idWindows);
        //        QList<WorkspaceWindows> listWorkspaceWindowSave = control()->getAllWorkspacesWindows();
        //        for (int index = 0; index < listWorkspaceWindowSave.size(); ++index) {
        //            WorkspaceWindows workspaceWindowsTemp = listWorkspaceWindowSave.at(index);
        //            if(workspaceWindowsTemp.workspaceId == workspaceIdString  && selectedWorkspaceId == windowsApp->idWindows && workspaceWindowsTemp.stateWindows != StateOfWindows::POP_UP && workspaceWindowsTemp.stateWindows != StateOfWindows::FULL_SCREEN_WINDOW){
        //                this->stackLayout->setCurrentWidget(newWindows);
        //            }

        //            if(workspaceWindowsTemp.workspaceId == workspaceIdString  && selectedWorkspaceId == windowsApp->idWindows && workspaceWindowsTemp.stateWindows == StateOfWindows::POP_UP && workspaceWindowsTemp.stateWindows == StateOfWindows::FULL_SCREEN_WINDOW){
        //                this->stackLayout->setCurrentWidget(newWindows);
        //                //remove item popop up to stack
        //                this->stackLayout->removeWidget(newWindows);
        //                listWidget.removeAt(windowsApp->idWindows);
        //                //select windows 0
        //                if(sizeWindows > 0){
        //                    for (int index = 0; index < control()->listWindowsAppVideowall.size(); ++index) {
        //                        WindowsApp *windowsApp0 = control()->listWindowsAppVideowall.at(index);
        //                        if(windowsApp0->idWindows == 0){
        //                            control()->appContext->setWindowsVideowallSelected(windowsApp0); //selected default
        //                            control()->setSelectedWindows(windowsApp0); // selectedWindows
        //                            this->stackLayout->setCurrentWidget(windowsApp0->zone);
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        }
    }

    //    int selectedWorkspaceId = settings.value("selected_workspace", -1).toInt();
    //    int sizeWorkpsaces = settings.beginReadArray("Workspaces");
    //    for (int index = 0; index < sizeWorkpsaces; ++index) {
    //        settings.setArrayIndex(index);
    //        QString workspaceIdSave = settings.value("workspace_id").toString();
    //        if(workspaceIdString == workspaceIdSave){


    //        }
    //    }
    //    settings.endArray();

    qDebug() << Q_FUNC_INFO << "SIZE" << listWidget.size();
}

void P_WindowsManager::selectPreviousWorkspace(int idOfWindowsAppSelected){
    int idOfWindowsAppPrevious = idOfWindowsAppSelected - 1;
    WindowsApp *windowsAppPrevious = Q_NULLPTR;
    do {
        if(idOfWindowsAppPrevious < 0){
            idOfWindowsAppPrevious = 0;
        }
        windowsAppPrevious = control()->listWindowsAppVideowall.at(idOfWindowsAppPrevious);
        if(windowsAppPrevious->idWindows == 0){
            break;
        }
        idOfWindowsAppPrevious = windowsAppPrevious->idWindows - 1;
        qDebug() << "WHITe";
    } while (windowsAppPrevious->stateOfWindows != StateOfWindows::NOT_POP_UP && windowsAppPrevious->stateOfWindows != 0);

    if(windowsAppPrevious != Q_NULLPTR){
        control()->setSelectedWindows(windowsAppPrevious);
        this->control()->appContext->setWindowsVideowallSelected(windowsAppPrevious);
        setCurrentStackWindows(windowsAppPrevious);
        QVariant *dataStructLoadLayoutPage = new QVariant();
        dataStructLoadLayoutPage->setValue<WindowsApp *>(windowsAppPrevious);
        C_VWWorkSpace *cWorkspace = windowsAppPrevious->cWorkspace;
        qDebug() << Q_FUNC_INFO<< "--windowsAppPrevious " << windowsAppPrevious->idWindows;
        cWorkspace->newAction(Message.APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE, dataStructLoadLayoutPage);
    }
}

void P_WindowsManager::setCurrentStackWindows(WindowsApp *windowsApp){
    if(windowsApp != Q_NULLPTR){
        setCurrentWidgetDisplay(windowsApp->zone);
    }
}
void P_WindowsManager::setCurrentWidgetDisplay(QWidget *widget){
    if(listWidget.contains(widget)){
        stackLayout->setCurrentWidget(widget);
    }
}


void P_WindowsManager::popUpWorkspaceWindows(WindowsApp *windowsApp){
    WorkspaceWindows workspaceWindowsSave;
    workspaceWindowsSave.workspaceName = windowsApp->windowsName;
    workspaceWindowsSave.workspaceId = QString::number(windowsApp->idWindows);
    //add state of windows when pop up
    if(windowsApp->stateOfWindows != StateOfWindows::FULL_SCREEN_WINDOW){
        workspaceWindowsSave.stateWindows = StateOfWindows::POP_UP;
        windowsApp->stateOfWindows = StateOfWindows::POP_UP;
    }
    if(windowsApp->stateOfWindows == StateOfWindows::FULL_SCREEN_WINDOW){
        workspaceWindowsSave.stateWindows = StateOfWindows::FULL_SCREEN_WINDOW;
        windowsApp->stateOfWindows = StateOfWindows::FULL_SCREEN_WINDOW;
    }
    control()->saveWorkspaceWindows(workspaceWindowsSave);

    QWidget *zoneWorkspaceOld = windowsApp->zone;
    listWidget.removeAt(windowsApp->idWindows);
    stackLayout->removeWidget(zoneWorkspaceOld);
    zoneWorkspaceOld->hide();
    zoneWorkspaceOld->lower();

    WindowWidget *widgetWindows = new WindowWidget(this->zone);
    widgetWindows->setIdWorkspace(windowsApp->idWindows);
    widgetWindows->setNameWorkspace(windowsApp->windowsName);
    widgetWindows->setWindowTitle(windowsApp->windowsName);
    connect(widgetWindows, &WindowWidget::closedWidget, this, &P_WindowsManager::onCloseWidget);
    connect(widgetWindows, &WindowWidget::resizedWidget, this, &P_WindowsManager::onResizedWidget);
    connect(widgetWindows, &WindowWidget::movedWidget, this, &P_WindowsManager::onMovedWidget);


    QWidget *zoneWorkspace =  new QWidget(widgetWindows);
    windowsApp->zoneParent = widgetWindows;
    QVBoxLayout *layoutWindows = new QVBoxLayout();
    layoutWindows->setSpacing(0);
    layoutWindows->setMargin(0);
    layoutWindows->addWidget(zoneWorkspace);
    widgetWindows->setLayout(layoutWindows);
    windowsApp->zone = zoneWorkspace;

    QList<WorkspaceWindows> listWorkspaceSaved = control()->getAllWorkspacesWindows();
    int screenWindows = 0; //default
    for (int index = 0; index < listWorkspaceSaved.size(); ++index) {
        WorkspaceWindows workspaceWindows = listWorkspaceSaved.at(index);
        if(workspaceWindows.workspaceId == QString::number(windowsApp->idWindows)){
            if(workspaceWindows.stateWindows == 1){
                QSize sizeWindows = workspaceWindows.sizeWindows;
                QPoint positionWindows = workspaceWindows.positonWindows;
                screenWindows = workspaceWindows.screenOfWindows;
                if(screenWindows == -1 || QApplication::desktop()->numScreens() <= 1){
                    screenWindows = 0;
                }

                widgetWindows->setGeometry(QApplication::desktop()->availableGeometry(screenWindows));
                widgetWindows->move(positionWindows);
                // widgetWindows->restoreGeometry(workspaceWindows.geometryOfwindowsParent);
                if(sizeWindows.width() > 0 && sizeWindows.height() > 0){
                    widgetWindows->resize(sizeWindows);
                }else{
                    //size save empty
                    QSize fullScreen = QApplication::desktop()->availableGeometry(0).size();
                    widgetWindows->resize(fullScreen/2);
                }
                widgetWindows->show();
                widgetWindows->raise();
            }

            if(workspaceWindows.stateWindows == 2){
                QSize sizeWindows = workspaceWindows.sizeWindows;
                QPoint positionWindows = workspaceWindows.positonWindows;
                screenWindows = workspaceWindows.screenOfWindows;
                if(screenWindows == -1 || QApplication::desktop()->numScreens() <= 1){
                    screenWindows = 0;
                }
                widgetWindows->setGeometry(QApplication::desktop()->availableGeometry(screenWindows));
                widgetWindows->move(positionWindows);
                widgetWindows->resize(sizeWindows);
                widgetWindows->raise();
                widgetWindows->showFullScreen();
            }
        }
    }
    //save to disk info
    WorkspaceWindows workspaceWindows;
    workspaceWindows.workspaceId = QString::number(windowsApp->idWindows);
    workspaceWindows.sizeWindows = widgetWindows->size();
    workspaceWindows.positonWindows = widgetWindows->pos();
    workspaceWindows.stateWindows =  windowsApp->stateOfWindows;
    workspaceWindows.screenOfWindows =  screenWindows;
    control()->saveWorkspaceWindows(workspaceWindows);


    C_VWWorkSpace *cVMWorkspace = windowsApp->cWorkspace;
    //stop player working
    cVMWorkspace->newAction(Message.APP_VIDEOWALL_WORKSPACE_STOP_PLAYER_WORKING, Q_NULLPTR);

    cVMWorkspace->setZone(zoneWorkspace);
    cVMWorkspace->refreshPlayerList(zoneWorkspace);
    //after show need load data
    QVariant *dataStructLoadLayoutPage = new QVariant();
    dataStructLoadLayoutPage->setValue<WindowsApp *>(windowsApp);
    cVMWorkspace->newAction(Message.APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE, dataStructLoadLayoutPage);

    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<int>(windowsApp->idWindows);
    control()->newUserAction(Message.APP_VIDEOWALL_POP_UP_WINDOWS_SELECTED, dataStruct);
}
void P_WindowsManager::onMovedWidget(WindowWidget *windowsWidget, QSize sizeWidget , QPoint postionWidget, int screenCurrent){
    WorkspaceWindows workspaceWindows;
    //    qDebug() << Q_FUNC_INFO << "(windowsWidget->getIdWorkspace()" <<windowsWidget->getIdWorkspace() << "pos" << postionWidget;
    workspaceWindows.workspaceName = windowsWidget->getNameWorkspace();
    workspaceWindows.workspaceId = QString::number(windowsWidget->getIdWorkspace());
    workspaceWindows.geometryOfwindowsParent = windowsWidget->saveGeometry();
    workspaceWindows.sizeWindows = sizeWidget;
    workspaceWindows.positonWindows = postionWidget;
    workspaceWindows.screenOfWindows = screenCurrent;
    control()->saveWorkspaceWindows(workspaceWindows);
}

void P_WindowsManager::onResizedWidget(WindowWidget *windowsWidget, QSize sizeWidget , QPoint postionWidget, int screenCurrent){
    WorkspaceWindows workspaceWindows;
    workspaceWindows.workspaceName = windowsWidget->getNameWorkspace();
    workspaceWindows.workspaceId = QString::number(windowsWidget->getIdWorkspace());
    workspaceWindows.geometryOfwindowsParent = windowsWidget->saveGeometry();
    workspaceWindows.sizeWindows = sizeWidget;
    workspaceWindows.positonWindows = postionWidget;
    workspaceWindows.screenOfWindows = screenCurrent;
    control()->saveWorkspaceWindows(workspaceWindows);
}

void P_WindowsManager::onCloseWidget(WindowWidget* windowsWidget, QSize sizeWidget , QPoint postionWidget, int screenCurrent){
    //save state of windows workspace
    //    geometry-windows-parent
    WorkspaceWindows workspaceWindows;
    workspaceWindows.workspaceName = windowsWidget->getNameWorkspace();
    workspaceWindows.workspaceId = QString::number(windowsWidget->getIdWorkspace());
    workspaceWindows.geometryOfwindowsParent = windowsWidget->saveGeometry();
    workspaceWindows.stateWindows = 0;
    workspaceWindows.sizeWindows = sizeWidget;
    workspaceWindows.screenOfWindows = 0;
    control()->saveWorkspaceWindows(workspaceWindows);
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<int>(windowsWidget->getIdWorkspace());
    control()->newUserAction(Message.APP_VIDEOWALL_CLOSED_WINDOWS, dataStruct);
}
void P_WindowsManager::closePopUpWindowsApp(WindowsApp *windowsApp){
    WorkspaceWindows workspaceWindows;
    workspaceWindows.workspaceName = windowsApp->windowsName;
    workspaceWindows.workspaceId = QString::number(windowsApp->idWindows);
    workspaceWindows.stateWindows = StateOfWindows::NOT_POP_UP;
    workspaceWindows.screenOfWindows = 0;
    control()->saveWorkspaceWindows(workspaceWindows);
    windowsApp->stateOfWindows = StateOfWindows::NOT_POP_UP;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<int>(windowsApp->idWindows);
    control()->newUserAction(Message.APP_VIDEOWALL_CLOSED_WINDOWS, dataStruct);
}

void P_WindowsManager::displayNewWindowsSelected(WindowsApp *windowsNewSelected){
    if(windowsNewSelected != Q_NULLPTR){
        QWidget *zoneParent = windowsNewSelected->zoneParent;

        QWidget *zoneWorkspace = windowsNewSelected->zone;
        //        if(zoneParentSelectedLast == Q_NULLPTR){
        //            zoneParentSelectedLast = zoneParent;
        //        }else{
        //            zoneParentSelectedLast->lower();
        //            zoneParentSelectedLast = zoneParent;
        //        }
        zoneParent->show();
        zoneParent->raise();
        zoneWorkspace->show();
        zoneWorkspace->activateWindow();
        zoneWorkspace->raise();
        if(windowsNewSelected->stateOfWindows != POP_UP || windowsNewSelected->stateOfWindows != FULL_SCREEN_WINDOW){
            setCurrentWidgetDisplay(zoneWorkspace);
        }
        //        qDebug() << Q_FUNC_INFO << "listWidget size" <<listWidget.size() << "stackLayout->count();" << stackLayout->count();

    }
}


bool P_WindowsManager::eventFilter(QObject *watched, QEvent *event) {
    QWidget *sender = qobject_cast<QWidget *>(watched);
    if (sender != Q_NULLPTR && sender == this->zone) {
        switch (event->type()) {
        case QEvent::Resize: {
            QResizeEvent *resizeEvent = (QResizeEvent *)(event);
            QSize newSize = resizeEvent->size();
            this->zone->resize(newSize);
            //            qDebug() << Q_FUNC_INFO << "new Size" << newSize;
            for (int index = 0; index < listWidget.size(); ++index) {
                QWidget *widget = listWidget.at(index);
                widget->resize(newSize);
                widget->update();
            }
        } break;

        default:
            break;
        }
    }
    return true;
}

