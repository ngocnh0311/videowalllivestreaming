#include "windowwidget.h"

WindowWidget::WindowWidget(QWidget *parent): QWidget(parent,  Qt::Window) {
    int screenCurrent =  QApplication::desktop()->screenNumber(this);
    this->screenWidgetCurrent = screenCurrent;
    this->positionWidgetCurrent = this->pos();
    this->sizeOfWidgetCurrent = this->size();
    this->installEventFilter(this);
}

int WindowWidget::getIdWorkspace() const
{
    return idWorkspace;
}

void WindowWidget::setIdWorkspace(int value)
{
    idWorkspace = value;
}

QString WindowWidget::getNameWorkspace() const
{
    return nameWorkspace;
}

void WindowWidget::setNameWorkspace(const QString &value)
{
    nameWorkspace = value;
}

void WindowWidget::closeEvent(QCloseEvent *event){
}

void WindowWidget::mouseMoveEvent(QMouseEvent *event){

}

void WindowWidget::resizeEvent(QResizeEvent *event){

}

QSize WindowWidget::getSizeOfWidgetCurrent() const
{
    return sizeOfWidgetCurrent;
}

void WindowWidget::setSizeOfWidgetCurrent(const QSize &value)
{
    sizeOfWidgetCurrent = value;
}

QPoint WindowWidget::getPositionWidgetCurrent() const
{
    return positionWidgetCurrent;
}

void WindowWidget::setPositionWidgetCurrent(const QPoint &value)
{
    positionWidgetCurrent = value;
}

int WindowWidget::getScreenWidgetCurrent() const
{
    return screenWidgetCurrent;
}

void WindowWidget::setScreenWidgetCurrent(int value)
{
    screenWidgetCurrent = value;
}

void WindowWidget::initCenterZone() {
    this->containWidget = new QWidget(this);
    this->zoneLayout->addWidget(containWidget);
}

QWidget* WindowWidget::getZone(int zoneId){
    return containWidget;
}

void WindowWidget::initTopZone() {
    this->topWidget = new QWidget(this);
    this->topWidget->setFixedSize(this->width(), 100);
    this->topWidget->setStyleSheet(
                "background: QLinearGradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 "
                "#eff0f5, stop: 1 #dedede); border: 0px solid #8a8a92; "
                "border-top-left-radius: 5px; border-top-right-radius: 5px; "
                "border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;");
    //  this->topWidget->move(0, 0);
    //  this->topWidget->resize(this->width(), 100);

    this->topLayout = new QVBoxLayout();
    this->topLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    this->topLayout->setSpacing(5);
    this->topLayout->setContentsMargins(0, 0, 0, 0);

    this->topWidget->setLayout(this->topLayout);

    this->zoneLayout->addWidget(this->topWidget);
}

void WindowWidget::initTitleZone() {
    titleWidget = new QWidget(this->topWidget);
    titleWidget->setMaximumWidth(this->topWidget->width());
    titleWidget->setStyleSheet("background-color: #00000000");
    titleWidget->installEventFilter(this);
    titleWidget->setMouseTracking(true);
    titleWidget->setAttribute(Qt::WA_Hover);

    QHBoxLayout *titleLayout = new QHBoxLayout();
    titleLayout->setAlignment(Qt::AlignLeft);
    titleLayout->setSpacing(10);
    titleLayout->setContentsMargins(10, 10, 10, 0);
    titleWidget->setLayout(titleLayout);

    this->closeButton =
            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
                         "#aaff3b30", "#808080", "#ff3b30", "");
    this->closeButton->setIcon(QIcon(":/images/res/icon_tab_close.png"));
    this->closeButton->setIconSize(QSize(10, 10));
    connect(this->closeButton, &QPushButton::clicked, this,
            &WindowWidget::onCloseButtonClicked);

    this->minimizeButton =
            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
                         "#aaffcc00", "#ffcc00", "#ffcc00", "");
    this->minimizeButton->setIcon(QIcon(":/images/res/icon_tab_minimize.png"));
    this->minimizeButton->setIconSize(QSize(10, 10));
    connect(this->minimizeButton, &QPushButton::clicked, this,
            &WindowWidget::onMinimizeButtonClicked);

    this->maximizeButton =
            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
                         "#aa4cd964", "#4cd964", "#4cd964", "");
    this->maximizeButton->setIcon(QIcon(":/images/res/icon_tab_maximize.png"));
    this->maximizeButton->setIconSize(QSize(10, 10));
    connect(this->maximizeButton, &QPushButton::clicked, this,
            &WindowWidget::onMaximizeButtonClicked);

    this->titleLabel = new QLabel(titleWidget);
    this->titleLabel->setMinimumWidth(this->topWidget->width() - 110);
    this->titleLabel->setAlignment(Qt::AlignCenter);
    this->titleLabel->setFont(Resources::instance().getLargeBoldButtonFont());
    this->titleLabel->setStyleSheet(
                "background-color: #00000000; color: #1e1e1e");
    this->titleLabel->setText("Cài đặt");

    titleLayout->addWidget(this->closeButton);
    titleLayout->addWidget(this->minimizeButton);
    titleLayout->addWidget(this->maximizeButton);
    titleLayout->addWidget(this->titleLabel);

    this->topLayout->addWidget(titleWidget);
}

void WindowWidget::initToolBarZone() {
    this->toolBarWidget = new QWidget(this->topWidget);
    this->toolBarWidget->setStyleSheet("background-color: #00000000;");

    this->toolBarLayout = new QHBoxLayout();
    this->toolBarLayout->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    this->toolBarLayout->setContentsMargins(10, 5, 10, 0);
    this->toolBarLayout->setSpacing(5);

    this->toolBarWidget->setLayout(this->toolBarLayout);

    this->topLayout->addWidget(this->toolBarWidget);
}

void WindowWidget::onCloseButtonClicked() { this->hideSettings(); }

void WindowWidget::onMinimizeButtonClicked() {}

void WindowWidget::onMaximizeButtonClicked() {}

bool WindowWidget::eventFilter(QObject *watched, QEvent *event) {
    QWidget *widget = (QWidget *)watched;
    if (widget == this) {
        if( event->type() == QEvent::Move){
//            qDebug() << "mouseMoveEvent";
            QMoveEvent *moveEvent = (QMoveEvent *)(event);
            QPoint posCurrent = moveEvent->pos();
            int screenCurrent =  QApplication::desktop()->screenNumber(this);
            positionWidgetCurrent = posCurrent;
            screenWidgetCurrent = screenCurrent;
            QPoint posOld = moveEvent->pos();
            Q_EMIT movedWidget(this, sizeOfWidgetCurrent , posCurrent, screenWidgetCurrent);
            return QWidget::eventFilter(watched, event);
        }else if(event->type() == QEvent::Close){
//            qDebug() << "close widget";
            Q_EMIT closedWidget(this, sizeOfWidgetCurrent , positionWidgetCurrent, screenWidgetCurrent);
            return QWidget::eventFilter(watched, event);
        }else if(event->type() == QEvent::Resize){
            QResizeEvent *resizeEvent = (QResizeEvent *)(event);
            QSize newSize = resizeEvent->size();
            sizeOfWidgetCurrent = newSize;
            Q_EMIT movedWidget(this, sizeOfWidgetCurrent , positionWidgetCurrent, screenWidgetCurrent);
//            qDebug() << "Resize widget" << newSize;
            return QWidget::eventFilter(watched, event);
        }
    }
    return QWidget::eventFilter(watched, event);
}


void WindowWidget::hideSettings() {
    this->zone->hide();
    this->zone->raise();
}

