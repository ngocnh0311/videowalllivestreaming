#ifndef A_WindowsManager_H
#define A_WindowsManager_H

#include <QObject>
#include "PacModel/control.h"
#include "c_windowsmanager.h"

class C_WindowsManager;
class A_WindowsManager : public Abstraction {
  Q_OBJECT
 private:
 public:
  A_WindowsManager(Control *ctrl);
  C_WindowsManager *control() { return (C_WindowsManager *)this->ctrl; }
  void changeControl(Control *ctrl);
};

#endif  // A_WindowsManager_H
