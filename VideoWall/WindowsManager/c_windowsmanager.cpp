#include "c_windowsmanager.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
AppContext* C_WindowsManager::getAppContext() const { return appContext; }

void C_WindowsManager::setAppContext(AppContext* value) { appContext = value; }

QList<WindowsApp *> C_WindowsManager::getListWindowsAppVideowall() const
{
    return listWindowsAppVideowall;
}

void C_WindowsManager::setSelectedWindows(WindowsApp *value)
{
    selectedWindows = value;
}

C_WindowsManager::C_WindowsManager(Control* ctrl, QWidget* zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();
    layoutSet = new LayoutSet();
    this->pres = new P_WindowsManager(this, zone);
    this->abst = new A_WindowsManager(this);
    this->zone = zone;

}
void C_WindowsManager::addWindowsApp(WindowsApp* windowsApp){
    windowsApp->windowsName = "Workspace" + QString::number(windowsApp->idWindows);
    //create new workspace
    QString appName = "Video Wall " + QString::number(windowsApp->idWindows);
    C_VWWorkSpace *cWorkspace = new C_VWWorkSpace(windowsApp, appName, this, windowsApp->zone);
    windowsApp->cWorkspace = cWorkspace;
    windowsApp->appName = appName;

    //load state pop save
    QList<WorkspaceWindows> listWorkspaceWindowsSave = this->getAllWorkspacesWindows();
    if(windowsApp->idWindows < listWorkspaceWindowsSave.size()){
        WorkspaceWindows workspaceWindowsSaved = listWorkspaceWindowsSave.at(windowsApp->idWindows);
        windowsApp->stateOfWindows = StateOfWindows::NOT_POP_UP;//defaul
        if(workspaceWindowsSaved.stateWindows == 1){
            windowsApp->stateOfWindows = StateOfWindows::POP_UP;
        }
        if(workspaceWindowsSaved.stateWindows == 2){
            windowsApp->stateOfWindows = StateOfWindows::FULL_SCREEN_WINDOW;
        }
    }


    if(windowsApp->idWindows == 0){
        this->appContext->setWindowsVideowallSelected(windowsApp); //selected default
        this->setSelectedWindows(windowsApp); // selectedWindows

        QVariant *dataStructLoadLayoutPage = new QVariant();
        dataStructLoadLayoutPage->setValue<WindowsApp *>(windowsApp);
        cWorkspace->newAction(Message.APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE, dataStructLoadLayoutPage);
        QString workspaceIdString = QString::number(windowsApp->idWindows);
        //load windowssave
        QSettings settings;
        settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
        int sizeWorkpsaces = settings.beginReadArray("Workspaces");
        for (int index = 0; index < sizeWorkpsaces; ++index) {
            settings.setArrayIndex(index);
            QString workspaceIdSave = settings.value("workspace_id").toString();
            if(workspaceIdString == workspaceIdSave){
                int stateOfWindowsString = settings.value("state_workspace_windows", -1).toInt();
                if(stateOfWindowsString == 2){
                    this->newUserAction(Message.APP_VIDEO_WALL_SHOW_FULLSCREEN_WORKSPACE_ORIGIN, Q_NULLPTR);
                }
            }
        }
        settings.endArray();
    }else{
        //set layout default
        if(windowsApp->needLoadData){//create new workspace
            //create new set workspaces
            this->appContext->setWindowsVideowallSelected(windowsApp); //selected default
            this->setSelectedWindows(windowsApp); // selectedWindows

            QVariant *dataStructLoadLayoutPage = new QVariant();
            dataStructLoadLayoutPage->setValue<WindowsApp *>(windowsApp);
            cWorkspace->newAction(Message.APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE, dataStructLoadLayoutPage);
        }else{
            QString workspaceIdString = QString::number(windowsApp->idWindows);
            //load windowssave
            QSettings settings;
            settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
            int sizeWorkpsaces = settings.beginReadArray("Workspaces");
            for (int index = 0; index < sizeWorkpsaces; ++index) {
                settings.setArrayIndex(index);
                QString workspaceIdSave = settings.value("workspace_id").toString();
                if(workspaceIdString == workspaceIdSave){
                    int stateOfWindowsString = settings.value("state_workspace_windows", -1).toInt();
                    if(stateOfWindowsString == 1 || stateOfWindowsString == 2){
                        presentation()->popUpWorkspaceWindows(windowsApp);
                    }else{
                        //not pop up
                        //                        QVariant *dataStructLoadLayoutPage = new QVariant();
                        //                        dataStructLoadLayoutPage->setValue<WindowsApp *>(windowsApp);
                        //                        cWorkspace->newAction(Message.APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE, dataStructLoadLayoutPage);
                    }
                }
            }
            settings.endArray();
        }
    }

    //save to disk info
    WorkspaceWindows workspaceWindowsSaved;
    workspaceWindowsSaved.workspaceId = QString::number(windowsApp->idWindows);
    workspaceWindowsSaved.workspaceName = windowsApp->windowsName;
    workspaceWindowsSaved.stateWindows =  windowsApp->stateOfWindows;
    this->saveWorkspaceWindows(workspaceWindowsSaved);

    listWindowsAppVideowall.insert(windowsApp->idWindows , windowsApp);
    QVariant *dataStruct1 = new QVariant();
    dataStruct1->setValue<QList<WindowsApp *>>(listWindowsAppVideowall);
    this->newAction(Message.APP_VIDEOWALL_UPDATE_LIST_WINDOWS , dataStruct1);

}

void C_WindowsManager::removeWorkspaceSavedWindows(WorkspaceWindows workspaceWindows){
    int indexWorkspace = -1;
    QString workspaceIdString = workspaceWindows.workspaceId;
    QSettings settings;
    settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
    //check all camera save if exits
    int sizeWorkpsaces = settings.beginReadArray("Workspaces");
    for (int index = 0; index < sizeWorkpsaces; ++index) {
        settings.setArrayIndex(index);
        QString workspaceIdSave = settings.value("workspace_id").toString();
        if(workspaceIdString == workspaceIdSave){
            indexWorkspace = index;
        }
    }
    settings.endArray();

    if(indexWorkspace != -1){

        //read all after write all
        QList<WorkspaceWindows> workspacesWindows = getAllWorkspacesWindows();
        workspacesWindows.removeAt(indexWorkspace);
        //saveAllCameraNetwork
        saveAllWindowsWorkspaces(workspacesWindows);
    }
}

void C_WindowsManager::saveWorkspaceWindows(WorkspaceWindows workspaceWindows){

    //    qDebug() << Q_FUNC_INFO << "workspaceWindows id"<< workspaceWindows.workspaceId;
    int indexWorkspace = -1;
    QString workspaceIdString = workspaceWindows.workspaceId;
    QSettings settings;
    settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
    //check all camera save if exits
    int sizeWorkpsaces = settings.beginReadArray("Workspaces");
    for (int index = 0; index < sizeWorkpsaces; ++index) {
        settings.setArrayIndex(index);
        QString workspaceIdSave = settings.value("workspace_id").toString();
        if(workspaceIdString == workspaceIdSave){
            indexWorkspace = index;
        }
    }
    settings.endArray();

    if(indexWorkspace != -1){
        //workspace id exist
        WorkspaceWindows workspaceWindowsNew;
        workspaceWindowsNew.workspaceId = workspaceIdString;

        //read all after write all
        QList<WorkspaceWindows> workspacesWindows = getAllWorkspacesWindows();
        WorkspaceWindows workspaceWindowsSave = workspacesWindows.at(indexWorkspace);
        workspaceWindowsNew.workspaceId = workspaceWindows.workspaceId;
        if(!workspaceWindows.workspaceName.isEmpty()){
            workspaceWindowsNew.workspaceName = workspaceWindows.workspaceName;
        }else{
            workspaceWindowsNew.workspaceName = workspaceWindowsSave.workspaceName;
        }

        if(workspaceWindows.stateWindows != -1){
            workspaceWindowsNew.stateWindows = workspaceWindows.stateWindows;
        }else{
            workspaceWindowsNew.stateWindows = workspaceWindowsSave.stateWindows;
        }

        if(workspaceWindows.screenOfWindows != -1){
            workspaceWindowsNew.screenOfWindows = workspaceWindows.screenOfWindows;
        }else{
            workspaceWindowsNew.screenOfWindows = workspaceWindowsSave.screenOfWindows;
        }

        if(!workspaceWindows.positonWindows.isNull()){
            workspaceWindowsNew.positonWindows = workspaceWindows.positonWindows;
        }else{
            workspaceWindowsNew.positonWindows = workspaceWindowsSave.positonWindows;
        }

        if(!workspaceWindows.sizeWindows.isEmpty()){
            workspaceWindowsNew.sizeWindows = workspaceWindows.sizeWindows;
        }else{
            workspaceWindowsNew.sizeWindows = workspaceWindowsSave.sizeWindows;
        }

        if(!workspaceWindows.geometryOfwindowsParent.isEmpty()){
            workspaceWindowsNew.geometryOfwindowsParent = workspaceWindows.geometryOfwindowsParent;
        }else{
            workspaceWindowsNew.geometryOfwindowsParent = workspaceWindowsSave.geometryOfwindowsParent;
        }

        workspacesWindows.replace(indexWorkspace, workspaceWindowsNew);
        //saveAllCameraNetwork
        saveAllWindowsWorkspaces(workspacesWindows);
    }else{
        //camera id not exits
        //create new cameranetwork save
        WorkspaceWindows workspaceWindowsNew;
        workspaceWindowsNew.workspaceId = workspaceWindows.workspaceId;;
        workspaceWindowsNew.workspaceName = workspaceWindows.workspaceName;
        workspaceWindowsNew.stateWindows = workspaceWindows.stateWindows;
        workspaceWindowsNew.geometryOfwindowsParent = workspaceWindows.geometryOfwindowsParent;
        workspaceWindowsNew.screenOfWindows = workspaceWindows.screenOfWindows;
        workspaceWindowsNew.positonWindows = workspaceWindows.positonWindows;
        workspaceWindowsNew.sizeWindows = workspaceWindows.sizeWindows;

        //read all after write all
        QList<WorkspaceWindows> workspacesWindows = getAllWorkspacesWindows();
        workspacesWindows.append(workspaceWindowsNew);
        //saveAllCameraNetwork
        saveAllWindowsWorkspaces(workspacesWindows);
    }
}
void C_WindowsManager::saveAllWindowsWorkspaces(QList<WorkspaceWindows> workspacesWindows){
    QSettings settings;
    settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
    //delete all before save
    settings.beginWriteArray("Workspaces", 0);
    settings.remove("");
    settings.endArray ();

    settings.beginWriteArray("Workspaces");
    for (int index = 0; index < workspacesWindows.size(); ++index) {
        settings.setArrayIndex(index);
        WorkspaceWindows workspaceWindows = workspacesWindows.at(index);
        settings.setValue("workspace_id" , workspaceWindows.workspaceId);
        settings.setValue("workspace_name", workspaceWindows.workspaceName);
        settings.setValue("state_workspace_windows", workspaceWindows.stateWindows);
        settings.setValue("geometry_windows_parent", workspaceWindows.geometryOfwindowsParent);
        settings.setValue("position_windows", workspaceWindows.positonWindows);
        settings.setValue("size_windows", workspaceWindows.sizeWindows);
        settings.setValue("screen_windows", workspaceWindows.screenOfWindows);
    }
    settings.endArray();
}

QList<WorkspaceWindows> C_WindowsManager::getAllWorkspacesWindows(){
    QList<WorkspaceWindows> workspacesWindows;
    QSettings settings;
    settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
    int sizeWorkpsaces = settings.beginReadArray("Workspaces");
    for (int index = 0; index < sizeWorkpsaces; ++index) {
        settings.setArrayIndex(index);
        WorkspaceWindows workspaceInfoSave;
        QString workspaceIdString = settings.value("workspace_id").toString();
        QString workspaceNameString = settings.value("workspace_name").toString();
        int stateWorspaceWindowsInt = settings.value("state_workspace_windows", -1).toInt();
        QByteArray geometryOfwindowsParent = settings.value("geometry_windows_parent").toByteArray();
        QSize sizeWindows = settings.value("size_windows").toSize();
        QPoint positionWindows = settings.value("position_windows").toPoint();
        int screenWindows = settings.value("screen_windows",-1).toInt();


        workspaceInfoSave.workspaceId = workspaceIdString;
        workspaceInfoSave.workspaceName = workspaceNameString;
        workspaceInfoSave.stateWindows = stateWorspaceWindowsInt;
        workspaceInfoSave.geometryOfwindowsParent = geometryOfwindowsParent;
        workspaceInfoSave.sizeWindows = sizeWindows;
        workspaceInfoSave.positonWindows = positionWindows;
        workspaceInfoSave.screenOfWindows = screenWindows;
        workspacesWindows.append(workspaceInfoSave);
    }
    settings.endArray();
    return workspacesWindows;
}


/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_WindowsManager::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_VIDEO_WALL_SHOW_FULLSCREEN_WORKSPACE_ORIGIN:
    case Message.APP_VIDEO_WALL_HIDE_FULLSCREEN_WORKSPACE_ORIGIN:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_STOP_ALL_WHEN_CREATE_NEW_WORKPSACE:
    {
        for (int index = 0; index < listWindowsAppVideowall.size(); ++index) {
            WindowsApp *windowsApp = listWindowsAppVideowall.at(index);
            if(windowsApp->stateOfWindows != StateOfWindows::POP_UP){
                C_VWWorkSpace *cVMWorkspace = windowsApp->cWorkspace;
                if(cVMWorkspace != Q_NULLPTR){
                    cVMWorkspace->newAction(Message.APP_VIDEOWALL_STOP_ALL_WHEN_CREATE_NEW_WORKPSACE, Q_NULLPTR);
                }
            }
        }

        C_VWWorkSpace *cVMWorkspace = selectedWindows->cWorkspace;
        //refresh app
        cVMWorkspace->newAction(Message.APP_SHOW, Q_NULLPTR);
    }
        break;


    case Message.APP_VIDEOWALL_POP_UP_WINDOWS_SELECTED:{
        int idOfWindowsApp = attachment->value<int>();
        for (int index = 0; index < listWindowsAppVideowall.size(); ++index) {
            WindowsApp *windowsApp = listWindowsAppVideowall.at(index);
            if(windowsApp->idWindows == idOfWindowsApp){
                windowsApp->stateOfWindows = StateOfWindows::POP_UP;
                break;
            }
        }
        //set previous workspace
        presentation()->selectPreviousWorkspace(idOfWindowsApp);

        QVariant *dataStruct1 = new QVariant();
        dataStruct1->setValue<QList<WindowsApp *>>(listWindowsAppVideowall);
        this->newAction(Message.APP_VIDEOWALL_UPDATE_LIST_WINDOWS , dataStruct1);
    }break;

    case Message.APP_VIDEOWALL_CLOSED_WINDOWS:{
        //stop player working current
        int idOfWindowsApp = attachment->value<int>();
        for (int index = 0; index < listWindowsAppVideowall.size(); ++index) {
            WindowsApp *windowsApp = listWindowsAppVideowall.at(index);
            if(windowsApp->idWindows == idOfWindowsApp){
                windowsApp->stateOfWindows = StateOfWindows::NOT_POP_UP;
                presentation()->onClosedwindows(windowsApp);
                break;
            }
        }
        QVariant *dataStruct1 = new QVariant();
        dataStruct1->setValue<QList<WindowsApp *>>(listWindowsAppVideowall);
        this->newAction(Message.APP_VIDEOWALL_UPDATE_LIST_WINDOWS , dataStruct1);
    }break;

    default:
        qDebug() << "ERROR : General User action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_WindowsManager::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE:
    case Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID:{
        this->newAction(message, attachment);
    }break;

    default:
        qDebug() << "ERROR : General System action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_WindowsManager::newAction(int message, QVariant* attachment) {
    switch (message) {

    case Message.VIDEO_WALL_RENDER_UNSUBSCRIBE_CRAWLER_CAMERA:
    case Message.VIDEO_WALL_RENDER_SUBSCRIBE_CRAWLER_CAMERA:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.WORKSPACE_PAGE_TRANSITION_BEGIN:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_VIDEO_WALL_CLOSE_POP_UP_WORKSPACE:{
        WindowsApp *windowsApp = attachment->value<WindowsApp *>();
        presentation()->closePopUpWindowsApp(windowsApp);
    }break;

    case Message.APP_VIDEO_WALL_DELETE_WORKSPACE:{
        WindowsApp *windowsApp = attachment->value<WindowsApp *>();
        presentation()->deleteWindowsSelected(windowsApp);
    }break;

    case Message.APP_VIDEO_WALL_CLOSE_FULL_SCREEN_WORKSPACE:{
        WindowsApp *windowsApp = attachment->value<WindowsApp *>();
        presentation()->hideFullScreenWindows(windowsApp);
    }break;


    case Message.APP_VIDEO_WALL_FULL_SCREEN_WORKSPACE:{
        WindowsApp *windowsApp = attachment->value<WindowsApp *>();
        presentation()->showFullScreenWindows(windowsApp);
    }break;

    case Message.APP_VIDEOWALL_REFRESH_TAB_LAYOUT_PAGE:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_VIDEOWALL_POP_UP_WINDOWS_SELECTED:{
        WindowsApp *windowsApp = attachment->value<WindowsApp *>();
        presentation()->popUpWorkspaceWindows(windowsApp);
    }break;

    case Message.APP_VIDEOWALL_NEW_WINDOWS_SELECTED:{

        WindowsApp* windowsNewSelected = attachment->value<WindowsApp *>();
        this->setSelectedWindows(windowsNewSelected); // selectedWindows
        int idOfWindowsAppPrevious = windowsNewSelected->idWindows - 1;
        WindowsApp *windowsAppPrevious = Q_NULLPTR;
        do {
            if(idOfWindowsAppPrevious < 0){
                idOfWindowsAppPrevious = 0;
            }
            windowsAppPrevious = this->listWindowsAppVideowall.at(idOfWindowsAppPrevious);
            if(windowsAppPrevious->idWindows == 0){
                break;
            }
            idOfWindowsAppPrevious = windowsAppPrevious->idWindows - 1;
            qDebug() << "WHITe";
        } while (windowsAppPrevious->stateOfWindows != StateOfWindows::NOT_POP_UP && windowsAppPrevious->stateOfWindows != 0);

        for (int index = 0; index < listWindowsAppVideowall.size(); ++index) {
            WindowsApp *windowsApp = listWindowsAppVideowall.at(index);
            if(windowsApp->idWindows != windowsNewSelected->idWindows){
                if(windowsApp->stateOfWindows != StateOfWindows::POP_UP && windowsApp->stateOfWindows != StateOfWindows::FULL_SCREEN_WINDOW){
                    C_VWWorkSpace *cVMWorkspace = windowsApp->cWorkspace;
                    if(cVMWorkspace != Q_NULLPTR){
                        if(windowsApp->idWindows != windowsAppPrevious->idWindows){
                            cVMWorkspace->newAction(Message.APP_VIDEOWALL_NEW_WINDOWS_SELECTED, Q_NULLPTR);
                        }
                    }
                }
            }
        }

        //load workspace previous
        if(listWindowsAppVideowall.size() > 1){
            if(windowsNewSelected->idWindows != 0 && windowsNewSelected->stateOfWindows == StateOfWindows::POP_UP || windowsNewSelected->stateOfWindows == StateOfWindows::FULL_SCREEN_WINDOW){
                presentation()->setCurrentStackWindows(windowsAppPrevious);
                QVariant *dataStructLoadLayoutPage = new QVariant();
                dataStructLoadLayoutPage->setValue<WindowsApp *>(windowsAppPrevious);
                C_VWWorkSpace *cWorkspace = windowsAppPrevious->cWorkspace;
                if(cWorkspace != Q_NULLPTR){
                    cWorkspace->newAction(Message.APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE, dataStructLoadLayoutPage);
                }
            }
        }


        //load data new windows selected
        if(windowsNewSelected->stateOfWindows != StateOfWindows::POP_UP && windowsNewSelected->stateOfWindows != StateOfWindows::FULL_SCREEN_WINDOW){
            QVariant *dataStructLoadLayoutPage = new QVariant();
            dataStructLoadLayoutPage->setValue<WindowsApp *>(windowsNewSelected);
            C_VWWorkSpace *cWorkspace = windowsNewSelected->cWorkspace;
            if(cWorkspace != Q_NULLPTR){
                cWorkspace->newAction(Message.APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE, dataStructLoadLayoutPage);
            }

            //stop all workspace not pop up

            for (int index = 0; index < this->listWindowsAppVideowall.size(); ++index) {
                WindowsApp *windowsAppPre = this->listWindowsAppVideowall.at(index);
                if((windowsAppPre->idWindows != windowsNewSelected->idWindows) && windowsAppPre->stateOfWindows == StateOfWindows::NOT_POP_UP){
                    C_VWWorkSpace *cWorkspacePre = windowsAppPre->cWorkspace;
                    cWorkspacePre->newAction(Message.APP_VIDEOWALL_WORKSPACE_STOP_PLAYER_WORKING, Q_NULLPTR);
                }
            }
        }

        presentation()->displayNewWindowsSelected(windowsNewSelected);
    }break;

    case Message.APP_VIDEOWALL_SHOW_WINDOWS_POP_UP_SELECTED:{
        WindowsApp* windowsPopUpSelected = attachment->value<WindowsApp *>();
        presentation()->displayNewWindowsSelected(windowsPopUpSelected);
    }break;

    case Message.APP_CONTEXT_GET:
        attachment->setValue(appContext);
        break;
    case Message.APP_VIDEOWALL_ADD_NEW_WINDOWS:{
        presentation()->createNewWindowsAppVideowall(true);
    }break;
        //to all workspace
    case Message.APP_UPDATE_DATA_LOCAL_SUCCESS:
    case Message.APP_SHOW:
    case Message.APP_CHANGED:
    case Message.UPDATE_CDN_TYPE_SELECTED:
    case Message.APP_NETWORK_IS_REACHABLE:
    case Message.APP_NETWORK_IS_UNREACHABLE:
    case Message.UPDATE_STATE_USE_FREE_SPACE:
    case Message.ENTER_FULLSCREEN_MODE:
    case Message.EXIT_FULLSCREEN_MODE:
    case Message.UPDATE_MODE_RENDER_DEBUG:
    case Message.PAGE_TRANSITION_BEGIN:
    case Message.CMS_CHANGE_CAM_ORDER_REFRESH_WORKSPACE:
    case Message.APP_VIDEO_WALL_UPDATE_PAGE_DEFAULT:
    {
        for (int index = 0; index < listWindowsAppVideowall.size(); ++index) {
            WindowsApp *windowsApp = listWindowsAppVideowall.at(index);
            C_VWWorkSpace *cVMWorkspace = windowsApp->cWorkspace;
            if(cVMWorkspace != Q_NULLPTR){
                cVMWorkspace->newAction(message, attachment);
            }
        }
    }break;


    case Message.UPDATE_DATA_NORMAL_MODE_AFTER_NETWORK_IS_UNREACHABLE:{
        QString messageData = "UPDATE_DATA_NORMAL_MODE_AFTER_NETWORK_IS_UNREACHABLE";
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>(messageData);
        this->refreshDataWhenNVRChangeIpOrAfterNetworkDisconnect(dataStruct);
    }break;

    case Message.NVR_CHANGE_IP_NEED_REFRESH:{
        this->refreshDataWhenNVRChangeIpOrAfterNetworkDisconnect(attachment);
    }break;

    case Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID_SUCCESS:{
        if(attachment != Q_NULLPTR){
            QString valueString = attachment->value<QString>();
            if(valueString == "CMS_CHANGE_CAM_ORDER"){
                //  cms pickup/ kick out camera

                QVariant *dataStruct = new QVariant();
                int layoutSelected = this->appContext->getLayoutSelected();
                int pageSelected = this->appContext->getPageSelected();
                LayoutStruct layoutStructCurrent = layoutSet->getLayoutWithNumberOfCameras(layoutSelected);
                layoutStructCurrent.selectedPage = pageSelected;
                dataStruct->setValue<LayoutStruct>(layoutStructCurrent);

                for (int index = 0; index < listWindowsAppVideowall.size(); ++index) {
                    WindowsApp *windowsApp = listWindowsAppVideowall.at(index);
                    C_VWWorkSpace *cVMWorkspace = windowsApp->cWorkspace;
                    if(cVMWorkspace != Q_NULLPTR){
                        cVMWorkspace->newAction(Message.CMS_CHANGE_CAM_ORDER_REFRESH_WORKSPACE, dataStruct);
                    }
                }
                qDebug()<< Q_FUNC_INFO <<"CMS_CHANGE_CAM_ORDER :: PICKUP OR KICKOUT CAMERA";

            }else if(valueString == "CMS_NVR_CHANGE_IP" || valueString == "UPDATE_DATA_NORMAL_MODE_AFTER_NETWORK_IS_UNREACHABLE"){
                //nvr change ip of device
                qDebug()<< Q_FUNC_INFO << valueString ;
                for (int index = 0; index < listWindowsAppVideowall.size(); ++index) {
                    WindowsApp *windowsApp = listWindowsAppVideowall.at(index);
                    C_VWWorkSpace *cVMWorkspace = windowsApp->cWorkspace;
                    if(cVMWorkspace != Q_NULLPTR){
                        cVMWorkspace->newAction(Message.APP_SHOW, attachment);
                    }
                }
            }
        }
    }break;

    case Message.APP_VIDEO_WALL_ZONE_PAGE_SELECTED:
    case Message.APP_VIDEO_WALL_SHOW_SITE_TOP:
    case Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS_LOCAL:
    case Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED_HANDEL:
    case Message.APP_PLAY_BACK_GET_CLIP_RECORD_ERROR:
    case Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED:
    case Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS:
    case Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_SITE_ID_SUCCESS:{
        if(selectedWindows != Q_NULLPTR){
            C_VWWorkSpace *cVWWorkSpace = selectedWindows->cWorkspace;
            if(cVWWorkSpace != Q_NULLPTR){
                cVWWorkSpace->newAction(message, attachment);
            }
        }
    }break;

    case Message.APP_VIDEOWALL_UPDATE_LIST_WINDOWS:
    case Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID:
    case Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE:
    case Message.APP_VIDEO_WALL_UPDATE_CAMERA_ORDER_REMOTE_CONTROL:{
        getParent()->newAction(message, attachment);
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}


//nvr change ip or network is unreachable
void C_WindowsManager::refreshDataWhenNVRChangeIpOrAfterNetworkDisconnect(QVariant *attachment){
    QSettings settings;
    QString networkType = settings.value("network_type").toString();
    QString appName = "Video Wall";
    if(networkType == "CDN" || (appName.contains(this->appContext->getWorkingApp().appName, Qt::CaseInsensitive) == false)) return;
    qDebug() << Q_FUNC_INFO;
    if(this->appContext->getIsLoadDataWithDeviceId()){
        this->newSystemAction(Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID, attachment);
    }else{
        //load with site
        for (int index = 0; index < listWindowsAppVideowall.size(); ++index) {
            WindowsApp *windowsApp = listWindowsAppVideowall.at(index);
            C_VWWorkSpace *cVMWorkspace = windowsApp->cWorkspace;
            if(cVMWorkspace != Q_NULLPTR){
                QVariant *dataStruct = new QVariant();
                DataOfWorkspace dataOfWorkspace;
                dataOfWorkspace.cVWWorkSpace = cVMWorkspace;
                dataOfWorkspace.layoutPageOfWorkspace = cVMWorkspace->presentation()->getSelectedLayout();
                dataStruct->setValue<DataOfWorkspace>(dataOfWorkspace);
                this->newSystemAction(Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE, dataStruct);
            }
        }
    }
}
