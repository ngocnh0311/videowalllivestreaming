#ifndef WINDOWWIDGET_H
#define WINDOWWIDGET_H

#include <QGraphicsDropShadowEffect>
#include <QHBoxLayout>
#include <QLabel>
#include <QObject>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QStackedWidget>
#include <QToolButton>
#include <QVBoxLayout>
#include <functional>
#include <Common/resources.h>
#include <QMoveEvent>
#include <QResizeEvent>
#include <QDesktopWidget>
#include <QPoint>
#include <QTimer>
class WindowWidget: public QWidget
{
    Q_OBJECT
private:
    int idWorkspace;
    QString nameWorkspace;

    QWidget *zone;
    QVBoxLayout *zoneLayout;
    QPushButton *closeButton;
    QPushButton *minimizeButton;
    QPushButton *maximizeButton;
    QWidget *titleWidget;
    QLabel *titleLabel;
    QWidget *topWidget;
    QVBoxLayout *topLayout;

    QWidget *toolBarWidget;
    QHBoxLayout *toolBarLayout;

    QWidget *containWidget;

    QList<QWidget *> zoneList;
    QList<QToolButton *> buttonList;
    int selectedTabIndex = -1;


    int screenWidgetCurrent = -1;
    QPoint positionWidgetCurrent;
    QSize sizeOfWidgetCurrent;
protected:
    QPoint oldPos;
    bool isMoving = false;
    bool eventFilter(QObject *watched, QEvent *event);
    void closeEvent(QCloseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *event);
public:
    WindowWidget(QWidget *parent);

    void hideSettings();

    void initTopZone();
    void initTitleZone();
    void initToolBarZone();
    void initCenterZone();


    QPushButton *createButton(QWidget *parent, QSize size, QRect borderRadius,
                              int borderWidth, QString backngroundColor,
                              QString textColor, QString borderColor,
                              QString text) {
        QPushButton *button = new QPushButton(parent);
        button->setFixedSize(size);
        QString css =
                "background-color: %1; color: %2; border: %3px solid %4;"
                "border-top-left-radius: %5; border-top-right-radius: %6; "
                "border-bottom-left-radius: %7; border-bottom-right-radius: %8; ";
        button->setStyleSheet(css.arg(backngroundColor)
                              .arg(textColor)
                              .arg(borderWidth)
                              .arg(borderColor)
                              .arg(borderRadius.x())
                              .arg(borderRadius.y())
                              .arg(borderRadius.width())
                              .arg(borderRadius.height()));
        button->setText(text);
        button->setFont(Resources::instance().getExtraSmallRegularButtonFont());
        return button;
    }
    QWidget* getZone(int zoneId);
    int getIdWorkspace() const;
    void setIdWorkspace(int value);

    QString getNameWorkspace() const;
    void setNameWorkspace(const QString &value);

    int getScreenWidgetCurrent() const;
    void setScreenWidgetCurrent(int value);

    QPoint getPositionWidgetCurrent() const;
    void setPositionWidgetCurrent(const QPoint &value);

    QSize getSizeOfWidgetCurrent() const;
    void setSizeOfWidgetCurrent(const QSize &value);

public Q_SLOTS:
    void onCloseButtonClicked();
    void onMinimizeButtonClicked();
    void onMaximizeButtonClicked();
Q_SIGNALS:
    void closedWidget(WindowWidget *windowsWidget, QSize sizeWidget , QPoint postionWidget, int screenCurrent);
    void movedWidget(WindowWidget *windowsWidget, QSize sizeWidget , QPoint postionWidget, int screenCurrent);
    void resizedWidget(WindowWidget *windowsWidget, QSize sizeWidget , QPoint postionWidget, int screenCurrent);
};

#endif // WINDOWWIDGET_H
