#ifndef C_WindowsManager_H
#define C_WindowsManager_H

#include <QDebug>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QWidget>
#include "VideoWall/VWWorkSpace/c_vw_workspace.h"
#include "PacModel/control.h"
#include "a_windowsmanager.h"
#include "message.h"
#include "p_windowsmanager.h"
#include "Authentication/windowsapp.h"
#include "VideoWall/c_videowall.h"
class C_SettingGeneral;
class C_SettingVideoWall;
class C_SettingPlayBack;

class P_WindowsManager;
class A_WindowsManager;
class C_VWWorkSpace;
class WindowsApp;
class C_WindowsManager : public Control {
  Q_OBJECT
  QWidget* zone;
 public:
  LayoutSet *layoutSet = Q_NULLPTR;
  AppContext* appContext = Q_NULLPTR;
  WindowsApp *selectedWindows;
  QList<WindowsApp *> listWindowsAppVideowall;
  C_WindowsManager(Control* ctrl, QWidget* zone);
  C_VideoWall* getParent() { return (C_VideoWall*)this->parent; }
  A_WindowsManager* abstraction() { return (A_WindowsManager*)this->abst; }
  P_WindowsManager* presentation() { return (P_WindowsManager*)this->pres; }

  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
  AppContext* getAppContext() const;
  void setAppContext(AppContext* value);
  void addWindowsApp(WindowsApp* windowsApp);
  QList<WindowsApp *> getListWindowsAppVideowall() const;
  void setSelectedWindows(WindowsApp *value);
  void saveAllWindowsWorkspaces(QList<WorkspaceWindows> WorkspacesWindows);
  void saveWorkspaceWindows(WorkspaceWindows workspaceWindows);
  void removeWorkspaceSavedWindows(WorkspaceWindows workspaceWindows);
  QList<WorkspaceWindows> getAllWorkspacesWindows();
  void refreshDataWhenNVRChangeIpOrAfterNetworkDisconnect(QVariant *attachment);

};

#endif  // C_WindowsManager_H
