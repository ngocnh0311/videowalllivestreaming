#include <QApplication>
#include <QCoreApplication>
#include <QDateTime>
#include <QMessageBox>
#include <QProcess>
#include <QTimer>
#include <QSettings>
#include "MainFrame/c_mainframe.h"

#include <sys/types.h>
#include <unistd.h>
#include <fstream>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <QFontDatabase>
#define MAX_BUF 1024
#define PID_LIST_BLOCK 32
//#define WRITE_LOG_DEBUG_TO_FILE

void myMessageHandler(QtMsgType type, const QMessageLogContext &, const QString & msg)
{
    QString txt;
    QDateTime dateTime;
    QString currentDateTimeDebug = dateTime.currentDateTime().toString("dd-MM-yyyy HH:mm:ss");
    switch (type) {
    case QtDebugMsg:
        txt = QString("[Debug]    : %1 === %2").arg(currentDateTimeDebug).arg(msg);
        break;
    case QtWarningMsg:
        txt = QString("[Warning]  : %1 === %2").arg(currentDateTimeDebug).arg(msg);
        break;
    case QtCriticalMsg:
        txt = QString("[Critical] : %1 === %2").arg(currentDateTimeDebug).arg(msg);
        break;
    case QtFatalMsg:
        txt = QString("[Fatal]    : %1 === %2").arg(currentDateTimeDebug).arg(msg);
        break;
    }

    QString folderLog = QDir::homePath() + "/.videowall" + "/log";
    if(!QDir(folderLog).exists()){
        QDir().mkdir(folderLog);
    }
    QString fileLogPath = folderLog + "/videowallApp.log";
    QFile outFile(fileLogPath);
    QStorageInfo storage = QStorageInfo::root();
    qint64 sizeFile = outFile.size();
    qint64 sizeStorageFree = storage.bytesFree();
    qint64 diff = sizeStorageFree - sizeFile;
    if(outFile.open(QIODevice::WriteOnly | QIODevice::Append) && diff > (diff > 100*1000000)){ //> 100MB
        QTextStream ts(&outFile);
        ts << txt << endl;
    }else{
        qDebug() <<Q_FUNC_INFO << "Can't not open file or storate is low";
    }
}

void killApplication() {
    qApp->quit();
    QProcess process;
    process.execute(
                QString("kill %1").arg(QApplication::QCoreApplication::applicationPid()));
}

void showMessageNotFindAppConfig() {
    QMessageBox messageBox;
    messageBox.setButtonText(1, "Thoát");
    messageBox.setText(
                "Bạn chưa có file cấu hình!\nBạn hãy copy file cấu hình vào thư mục "
                "chứa chương trình và chạy lại.");
    if (messageBox.exec() == QMessageBox::Ok) {
        messageBox.close();
        killApplication();
    }
}

void canNotFindFileConfig() {
    QMessageBox messageBox;
    messageBox.setButtonText(1, "Thoát");
    messageBox.setText("Không thể copy được file cấu hình");
    if (messageBox.exec() == QMessageBox::Ok) {
        messageBox.close();
        killApplication();
    }
}



void warningWhenAnInstanceApplicationIsRunning() {
    QMessageBox messageBox;
    messageBox.setButtonText(1, "Thoát");
    messageBox.setIcon(QMessageBox::Warning);
    messageBox.setText(
                "Ứng dụng Video Wall đã được chạy.\n"
                "Ứng dụng chỉ được chạy khởi tạo một lần. Xin cảm ơn!");
    QTimer::singleShot(20000, [&messageBox]{
        messageBox.hide();
    });
    messageBox.exec();
}


/* checks if the string is purely an integer
 * we can do it with `strtol' also
 */
int check_if_number (char *str) {
    for (int i=0; str[i] != '\0'; i++) {
        if (!isdigit (str[i]))
            return 0;
    }
    return 1;
}

int *pidof (char *pname)
{
    DIR *dirp;
    FILE *fp;
    struct dirent *entry;
    int *pidlist, pidlist_index = 0, pidlist_realloc_count = 1;
    char path[MAX_BUF], read_buf[MAX_BUF];

    dirp = opendir ("/proc/");
    if (dirp == NULL) {
        perror ("Fail");
        return NULL;
    }

    pidlist = (int*)malloc (sizeof (int) * PID_LIST_BLOCK);
    if (pidlist == NULL)
        return NULL;

    while ((entry = readdir (dirp)) != NULL) {
        if (check_if_number (entry->d_name)) {
            strcpy (path, "/proc/");
            strcat (path, entry->d_name);
            strcat (path, "/comm");

            /* A file may not exist, it may have been removed.
       * dut to termination of the process. Actually we need to
       * make sure the error is actually file does not exist to
       * be accurate.
       */
            fp = fopen (path, "r");
            if (fp != NULL)
            {
                fscanf (fp, "%s", read_buf);
                if (strcmp (read_buf, pname) == 0)
                {
                    /* add to list and expand list if needed */
                    pidlist[pidlist_index++] = atoi (entry->d_name);
                    if (pidlist_index == PID_LIST_BLOCK * pidlist_realloc_count)
                    {
                        pidlist_realloc_count++;
                        pidlist = (int*)realloc (pidlist, sizeof (int) * PID_LIST_BLOCK * pidlist_realloc_count); //Error check todo
                        if (pidlist == NULL)
                        {
                            return NULL;
                        }
                    }
                }
                fclose (fp);
            }
        }
    }

    closedir (dirp);
    pidlist[pidlist_index] = -1; /* indicates end of list */
    return pidlist;
}

int main(int argc, char *argv[]) {
    //    av_log_set_level(AV_LOG_TRACE);
    av_register_all();
    avcodec_register_all();
    avformat_network_init();

    QCoreApplication::setApplicationName("VideoWall");
    QCoreApplication::setOrganizationName("VP9");
    QCoreApplication::setOrganizationDomain("vp9.vn");
    QCoreApplication::setApplicationVersion("2017.10.05.0");
    QApplication a(argc, argv);
#ifdef WRITE_LOG_DEBUG_TO_FILE
    qInstallMessageHandler(myMessageHandler);
#endif
    QSettings settings;
    //version app settings
    QString appVersion = "2.15.23";
    QString dateBuild = "11:40 20/06/2018 (GMT+7)";

    settings.setValue("app_version", appVersion);
    settings.setValue("date_build", dateBuild);

    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-Regular.ttf");
    qApp->setFont(QFont("Ubuntu Regular"));
    QString welcomeApp = "===============**************// START RUN APP VIDEOWALL - Version: %1 + Date Build: %2 \\\**************==============";
    qDebug() << welcomeApp.arg(appVersion).arg(dateBuild);
    qDebug() << "APPLICATION_PID_START: " <<QApplication::QCoreApplication::applicationPid() << "MAIN THREAD" << QThread::currentThread();

    //    int *list, i;
    //    list = pidof ("VideoWall");
    //    for (i=0; list[i] != -1; i++)
    //    {
    //      if(list[i] == getpid() )
    //          continue;

    //      printf ("Process running with ID : %d \n", list[i]);
    //      QString killCommand("kill -9 ");
    //      killCommand.append(QString("%1").arg(list[i]));
    //      system(killCommand.toLatin1().data());
    //    }
    //    free (list);

    qDebug() << "THREAD MAIN" << QThread::currentThread();

    int numberScreen = QApplication::desktop()->numScreens();
    qDebug() << "numScreens " << numberScreen;

    setlocale(LC_NUMERIC, "C");
    NetworkUtils::instance();
    Resources::instance();


    QLockFile lockFile(QDir::temp().absoluteFilePath("<videowallPC-ID>.lock"));

    /* Trying to close the Lock File, if the attempt is unsuccessful for 100 milliseconds,
       * then there is a Lock File already created by another process.
       / Therefore, we throw a warning and close the program
       * */
    if(!lockFile.tryLock(100)){
        warningWhenAnInstanceApplicationIsRunning();
        exit(0);
    }

    AppProfile::copyConfigFile();
    AppProfile::openAppProfiles();
    if (AppProfile::getAppProfile() != Q_NULLPTR) {
        QWidget *window = new QWidget();
        C_MainFrame *cFrame = new C_MainFrame(Q_NULLPTR, window);
        AppMessage Message;
        cFrame->newAction(Message.PROJECT_RUN, 0);
    } else {
        showMessageNotFindAppConfig();
    }
    return a.exec();
}

/// check app with semaphore
//    QSystemSemaphore semaphore("<uniq id>", 1);  // create semaphore
//    semaphore.acquire(); // Raise the semaphore, barring other instances to work with shared memory

//#ifndef Q_OS_WIN32
//    // in linux / unix shared memory is not freed when the application terminates abnormally,
//    // so you need to get rid of the garbage
//    QSharedMemory nix_fix_shared_memory("<uniq id 2>");
//    if(nix_fix_shared_memory.attach()){
//        nix_fix_shared_memory.detach();
//    }
//#endif

//    QSharedMemory sharedMemory("<uniq id 2>");  // Create a copy of the shared memory
//    bool is_running;            // variable to test the already running application
//    if (sharedMemory.attach()){ // We are trying to attach a copy of the shared memory
//        // To an existing segment
//        is_running = true;      // If successful, it determines that there is already a running instance
//    }else{
//        sharedMemory.create(1); // Otherwise allocate 1 byte of memory
//        is_running = false;     // And determines that another instance is not running
//    }
//    semaphore.release();
//    // If you already run one instance of the application, then we inform the user about it
//    // and complete the current instance of the application
//    if(is_running){
//        warningWhenAnInstanceApplicationIsRunning();
//        exit(0);
//    }

/// get md5
//    QFile theFile(QCoreApplication::applicationFilePath());
//    QByteArray thisFile;
//    if (theFile.open(QIODevice::ReadOnly))
//    {
//        thisFile = theFile.readAll();
//    }
//    else
//    {
//        qDebug() << "Can't open file.";
//    }

//    QString fileMd5 = QString(QCryptographicHash::hash((thisFile), QCryptographicHash::Md5).toHex());
//    qDebug() << "FILE MD5: " << fileMd5;
