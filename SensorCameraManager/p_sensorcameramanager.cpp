#include "p_sensorcameramanager.h"
#include "Common/LayoutSet.h"

P_SensorCameraManager::P_SensorCameraManager(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    this->zone->setStyleSheet("background-color: #333");
    layout = new QHBoxLayout();
    layout->setMargin(0);
    layout->setSpacing(5);
    this->zone->setLayout(layout);


}
void P_SensorCameraManager::show(QVariant *attachment) { Q_UNUSED(attachment) }

void P_SensorCameraManager::update() {}

QObject *P_SensorCameraManager::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}
