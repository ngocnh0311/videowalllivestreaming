#ifndef C_SENSORCAMERAMANAGER_H
#define C_SENSORCAMERAMANAGER_H

#include <QDebug>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QWidget>
#include "PacModel/control.h"
#include "SensorCameraManager/a_sensorcameramanager.h"
#include "SensorCameraManager/p_sensorcameramanager.h"
#include "message.h"
#include "Authentication/appcontext.h"
#include "MainFrame/c_mainframe.h"
#include "SensorCameraManager/SensorCamera/c_sensorcamera.h"

class P_SensorCameraManager;
class C_MainFrame;
class A_SensorCameraManager;
class C_SensorCamera;
class C_SensorCameraManager : public Control {
public:
    QMap<int, C_SensorCamera *> listSensorCamrera;
    QList<CamItem *> listCamItemOfSite;
    QWidget* zone;
    AppContext* appContext;
    C_MainFrame * getParent() { return (C_MainFrame*)this->parent; }
    A_SensorCameraManager* abstraction() { return (A_SensorCameraManager*)this->abst; }
    P_SensorCameraManager* presentation() { return (P_SensorCameraManager*)this->pres; }
    C_SensorCameraManager(Control* ctrl, QWidget* zone = Q_NULLPTR);
    void creatListSensorCamera(QList<CamItem *> listCamItem);

    void show(QVariant* attachment);
    /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
    void newUserAction(int message, QVariant* attachment) override;
    /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newSystemAction(int message, QVariant* attachment) override;

    /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newAction(int message, QVariant* attachment) override;
    AppContext *getAppContext() const;
    void setAppContext(AppContext *value);
};

#endif  // C_SENSORCAMERAMANAGER_H
