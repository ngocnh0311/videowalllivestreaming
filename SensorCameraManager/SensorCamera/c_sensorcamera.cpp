#include "c_sensorcamera.h"

/**
* Contructor. Register the father in the pac hierarchy.
* @param ctrl The reference on the father. Generally the pac agent which
* create this agent.
**/
C_SensorCamera::C_SensorCamera(Control* ctrl, QWidget* _zone)
    : Control(ctrl) {
    qRegisterMetaType<CrawlerType>();
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    appContext = dataStruct->value<AppContext*>();

    //    pres = new P_SensorCamera(this, zone);
    abst = new A_SensorCamera(this);
    zone = _zone;
}

/**
 * Method to receive a message from the Presentation Facet.
 * @param message    : A string which describe the request
 * @param attachment : A ref on an eventual object necessary to treat the request
 **/
void C_SensorCamera::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General User action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
 * Method to receive a message from the Astraction Facet.
 * @param message    : A string which describe the request
 * @param attachment : A ref on an eventual object necessary to treat the request
 **/
void C_SensorCamera::newSystemAction(int message, QVariant* attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case Message.APP_CONTEXT_GET:
        attachment->setValue(appContext);
        break;
    default:
        qDebug() << "ERROR : General System action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
 * Method to receive a message from an other agent.
 * @param message    : A string which describe the request
 * @param attachment : A ref on an eventual object necessary to treat the request
 **/
void C_SensorCamera::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.APP_CONTEXT_GET:{
        attachment->setValue(appContext);
    }break;

    case Message.VIDEO_WALL_RENDER_SUBSCRIBE_CRAWLER_CAMERA:{
        C_CrawlerCamera *crawler = findCrawlerCamera(attachment);
        if(crawler != Q_NULLPTR){
            crawler->newAction(Message.START_CRAWLER_WORKING, Q_NULLPTR);
        }
    }break;

    case Message.VIDEO_WALL_RENDER_UNSUBSCRIBE_CRAWLER_CAMERA:{
        DataStopCrawler *dataStopCrawler = attachment->value<DataStopCrawler *>();
        if(dataStopCrawler != Q_NULLPTR){
            C_CrawlerCamera * crawler = listCrawlers.value(getNameType(dataStopCrawler->crawlerType));
            if(crawler != Q_NULLPTR){
                crawler->newAction(Message.REMOVE_SUBCRIBER_CRAWLER, attachment);
            }
        }
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

C_CrawlerCamera* C_SensorCamera::findCrawlerCamera(QVariant *attachment) {
    C_CrawlerCamera *crawler = Q_NULLPTR;
    if(attachment == Q_NULLPTR)
        return crawler;
    DataGetCrawler *dataGetCrawler = attachment->value<DataGetCrawler*>();
    if(dataGetCrawler == Q_NULLPTR)
        return crawler;
    CrawlerType crawlerType = dataGetCrawler->crawlerType;
    crawler = listCrawlers.value(getNameType(crawlerType));
    if(crawler == Q_NULLPTR){
        qDebug() <<Q_FUNC_INFO << "getNameType(crawlerType)" << getNameType(crawlerType);
        crawler = new C_CrawlerCamera(camItem, this, crawlerType);
        connect(crawler, &C_CrawlerCamera::finished, this, &C_SensorCamera::crawlerFinished);

        QThread *threadCrawler = new QThread;
        threadCrawler->setObjectName("THREAD" + getNameType(crawlerType));
        crawler->moveToThread(threadCrawler);
        threadCrawler->start();
        qDebug() << "START INIT WORKER";
        QTimer::singleShot(0,crawler, &C_CrawlerCamera::onInitWorker);
        listCrawlers.insert(getNameType(crawlerType), crawler);
        listThreads.insert(getNameType(crawlerType), threadCrawler);
        qDebug() << Q_FUNC_INFO << "Crawler Name" << getNameType(crawlerType) << "listCrawlers Size" <<listCrawlers.size();
    }else{
        QThread *threadCrawler = listThreads.value(getNameType(crawlerType));
        threadCrawler->start();
    }
    crawler->newAction(Message.ADD_SUBCRIBER_CRAWLER, attachment);
    dataGetCrawler->pCrawlerCamera = crawler;
    return crawler;
}

void C_SensorCamera::crawlerFinished(CrawlerType crawlerType){
    qDebug() << Q_FUNC_INFO << "QThread::currentThread()" << QThread::currentThread() << "Crawler name thread quit" << getNameType(crawlerType);
    QThread *threadCrawlerFinnish = listThreads.value(getNameType(crawlerType));
    threadCrawlerFinnish->quit();
    threadCrawlerFinnish->wait();
}

CamItem *C_SensorCamera::getCamItem() const
{
    return camItem;
}

void C_SensorCamera::setCamItem(CamItem *value)
{
    camItem = value;
}

QString C_SensorCamera::getNameType(CrawlerType crawlerType){
    return QString::number(crawlerType.cameraId)  + crawlerType.protocol + crawlerType.network + crawlerType.channelName + crawlerType.dataSource;
}

void C_SensorCamera::show(QVariant* attachment) {
    newUserAction(Message.SHOW, attachment);
}
