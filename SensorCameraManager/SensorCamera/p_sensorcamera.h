#ifndef P_SensorCamera_H
#define P_SensorCamera_H

#include <PacModel/presentation.h>
#include <QGridLayout>
#include <QList>
#include <QObject>
#include <QPushButton>
#include <QWidget>
#include "c_sensorcamera.h"
#include "SensorCameraManager/SensorCamera/c_sensorcamera.h"
class C_SensorCamera;
class P_SensorCamera : public Presentation {
 private:
  QWidget *zone = Q_NULLPTR;

 public:
  P_SensorCamera(Control *ctrl, QWidget *zone);
  C_SensorCamera *control() { return (C_SensorCamera *)this->ctrl; }
  void changeControl(Control *ctrl);
  void update();
  void show(QVariant *attactment);
  QWidget *getZone(int zoneId);

 public Q_SLOTS:
};

#endif  // P_SensorCamera_H
