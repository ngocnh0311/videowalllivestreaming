#ifndef A_SensorCamera_H
#define A_SensorCamera_H

#include <QObject>
#include "Camera/camsite.h"
#include "PacModel/control.h"

class A_SensorCamera : public Abstraction {
  Q_OBJECT
 public:
  A_SensorCamera(Control *ctrl);
  void changeControl(Control *ctrl);
};

#endif  // A_SensorCamera_H
