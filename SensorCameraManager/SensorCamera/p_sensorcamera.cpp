#include "p_sensorcamera.h"

P_SensorCamera::P_SensorCamera(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    this->zone->setStyleSheet("background-color: #222; color: white;");
    QVBoxLayout *layout = new QVBoxLayout();
    layout->setSpacing(20);
    layout->setMargin(10);
    layout->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
    this->zone->setLayout(layout);
}
void P_SensorCamera::show(QVariant *attachment) { Q_UNUSED(attachment) }

void P_SensorCamera::update() {}

QWidget *P_SensorCamera::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return zone;
    default:
        return Q_NULLPTR;
    }
}
