#ifndef C_SENSORCAMERA_H
#define C_SENSORCAMERA_H

#include <QDebug>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QWidget>
#include "PacModel/control.h"
#include "SensorCameraManager/SensorCamera/a_sensorcamera.h"
#include "SensorCameraManager/SensorCamera/p_sensorcamera.h"

#include "message.h"
#include "Authentication/appcontext.h"
#include <QNetworkAccessManager>
#include "SensorCameraManager/c_sensorcameramanager.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/c_crawlercamera.h"
#include "Common/datagetcrawler.h"
class P_SensorCamera;
class A_SensorCamera;
class C_CrawlerCamera;
class C_SensorCamera : public Control {
    Q_OBJECT
public:
    QHash<QString ,C_CrawlerCamera *> listCrawlers;
    QHash<QString ,QThread *> listThreads;
    CamItem *camItem = Q_NULLPTR;
    QWidget* zone;
    AppContext* appContext;
    C_SensorCameraManager * getParent() { return (C_SensorCameraManager*)this->parent; }
    A_SensorCamera* abstraction() { return (A_SensorCamera*)this->abst; }
    P_SensorCamera* presentation() { return (P_SensorCamera*)this->pres; }
    C_SensorCamera(Control* ctrl, QWidget* zone = Q_NULLPTR);

    void show(QVariant* attachment);
    /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
    void newUserAction(int message, QVariant* attachment) override;
    /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newSystemAction(int message, QVariant* attachment) override;

    /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newAction(int message, QVariant* attachment) override;
    CamItem *getCamItem() const;
    void setCamItem(CamItem *value);

    C_CrawlerCamera* findCrawlerCamera(QVariant *attachment);
    QString getNameType(CrawlerType crawlerType);
public Q_SLOTS:
    void crawlerFinished(CrawlerType crawlerType);

Q_SIGNALS:
};

#endif  // C_SENSORCAMERA_H
