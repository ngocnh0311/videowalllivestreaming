﻿#include "crawlersocket.h"

CrawlerSocket::CrawlerSocket(C_CrawlerCamera *_crawlerCamera) {

    pCrawlerCamera = _crawlerCamera;
    socket = new QWebSocket();
    connect(socket, &QWebSocket::connected, this, &CrawlerSocket::onSocketConnected);
    connect(socket, &QWebSocket::disconnected, this, &CrawlerSocket::onSocketDisconnected);
    connect(socket, &QWebSocket::textMessageReceived, this, &CrawlerSocket::onRawPackageIndexReceived);

    //reconnectTimer = new QTimer(this);
    //connect(reconnectTimer, &QTimer::timeout, this, &CrawlerSocket::onReconnonSocketDisconnectedectSocket);
    pDownloader = pCrawlerCamera->getDownloader();
}

CrawlerSocket::~CrawlerSocket() {
    socket->deleteLater();
}

void CrawlerSocket::openSource() {
    if (socket->isValid()) return;
    isSecondIdrReached = true;
    openSocket();
}

void CrawlerSocket::closeSource() {
    qDebug() << "QThread::currentThread()"<<QThread::currentThread() << playerName << "::CrawlerSocket::closeSource" << this->socketUrl ;
    linkIndex = 0;
    needToOpenSocketWhenOldSocketDisconnected = false;

    if (socket->isValid()) {
        socket->flush();
        socket->close();
        socket->abort();
        qDebug() << "Close socket Success";
    }
}

void CrawlerSocket::setSource(QString source) {
    socketUrl = source;
    //buildLinkFormat for download link
    QString tmpUrl = socketUrl;
    tmpUrl.replace("//", "/");
    QStringList splitedUrl = tmpUrl.split("/");
    if (splitedUrl.length() == 5) {
        host = splitedUrl.at(1);
        mac = splitedUrl.at(3);
        cam = splitedUrl.at(4);
    }
    linkFormat = QString("http://%1/live/g/%2/%3").arg(host).arg(cam).arg("%1");
}


void CrawlerSocket::openSocket() {
    qDebug() << playerName << "::CrawlerSocket::openSocket" << QThread::currentThread() ;
    if (!socketUrl.isEmpty()) {
        if (!isNetworkReachable) { return; }

        qDebug() << Q_FUNC_INFO << "SOURCE - CRAWLER" << socketUrl;
        socket->open(QUrl(socketUrl));
    }
    Q_EMIT resetTimerReopenSouce();
}
void CrawlerSocket::onReOpenSocket(){
    closeSource();
    openSocket();
}

void CrawlerSocket::onSocketConnected() {
    qDebug() << playerName << "::CrawlerSocket::onSocketConnected" ;
    sendSignature();

    qDebug() <<playerName<< "HOST ADDRESS OF SOCKET" <<socket->localAddress().toString() << "PORT" << socket->localPort();

    //Q_EMIT socketConnected();
}

void CrawlerSocket::sendSignature() {
    qDebug() << playerName << "::CrawlerSocket::sendSignature" ;
    if(pCrawlerCamera){
        CrawlerImageQueue *imageQueue = pCrawlerCamera->getImageQueue();
        CrawlerRTPQueue *rtpQueue = pCrawlerCamera->getRtpQueue();
        rtpQueue->empty();
        imageQueue->empty();

        playerIsPlaying = true;

        QString signature = "{ \"action\":\"hello\", "
                            "\"version\":\"2.0\", "
                            "\"host_id\":\"%1\", "
                            "\"signature\":\"RESERVED\", "
                            "\"timestamp\":\"%2\" }";
        signature = signature.arg(mac).arg(QDateTime::currentMSecsSinceEpoch());
        //  qDebug() << "signature" << signature;
        socket->sendTextMessage(signature);
        socketState = CRAWLERS_SentSignature;
    }
}

void CrawlerSocket::onRawPackageIndexReceived(const QString &message) {
    switch (socketState) {
    case CRAWLERS_SentSignature: {
        socketState = CRAWLERS_ReceivedSignature;
        linkIndex = 0;
        //        onReceivedGopPackage(message);
        //qDebug() << "CrawlerSocket::" << message;
    } break;
    case CRAWLERS_ReceivedSignature: {
        socketState = CRAWLERS_ReceivedDownloadLink;
        //qDebug() << "CrawlerSocket::Index[" << linkIndex << "]=" << message;
        prepareLinkToDownload(message);
    } break;
    case CRAWLERS_ReceivedDownloadLink: {
        prepareLinkToDownload(message);
//        qDebug() << "CrawlerSocket::Index[" << linkIndex << "]=" << message;
    } break;
    default:
        break;
    }
}

void CrawlerSocket::onReceivedGopPackage(QString message){
    QJsonDocument document = QJsonDocument::fromJson(message.toUtf8());
    QJsonObject jsonObject = document.object();
    QJsonValue gopJsonValue = jsonObject.take("gop");
    QJsonArray jsonArrayTimestamp = gopJsonValue.toArray();
    for (int index = 0; index < jsonArrayTimestamp.size(); ++index) {
        double timestampPackage  = jsonArrayTimestamp.at(index).toDouble();
        QString timestampString = QString::number(timestampPackage, 'g' , 13);
        QString downloadLink = linkFormat.arg(timestampString);
        pDownloader->onReceiveDowloadRTCData(linkIndex++, downloadLink);
    }
}

void CrawlerSocket::prepareLinkToDownload(QString message) {
    QString mess = message.replace("[", "").replace("]", "");
    QStringList listValue = mess.split(",");
    QString timestamp = listValue.at(0);
    QString idrByte = listValue.at(3);
    int isIDRFrame = idrByte.toInt();
    if (!isSecondIdrReached) {
        if (isIDRFrame == 0) {
            return;
        } else {
            isSecondIdrReached = true;
        }
    }

    QString downloadLink = linkFormat.arg(timestamp);
    pDownloader->onReceiveDowloadRTCData(linkIndex++, downloadLink);
}


void CrawlerSocket::onSocketDisconnected() {
    qDebug() << playerName << "::CrawlerSocket::onSocketDisconnected== NAM" ;
    socketState = CRAWLERS_Disconnected;
    //    if (needToOpenSocketWhenOldSocketDisconnected && isNetworkReachable) {
    //        needToOpenSocketWhenOldSocketDisconnected = false;
    //        openSocket();
    //    }
    //Q_EMIT socketDisconnected();
}

void CrawlerSocket::newAction(int message, QVariant *attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case Message.PLAYER_SOURCE_CLEAR: {
        closeSource();
    } break;

    case Message.APP_NETWORK_IS_REACHABLE: {
        isNetworkReachable = true;
        //        if ((this->pRTCPlayer->isPlayerPlaying() && socketState == CRAWLERS_ReceivedDownloadLink) || (this->pRTCPlayer->isPlayerPlaying() && socketState == CRAWLERS_Disconnected)) {
        //            //            openSocket();
        //        }
    } break;

    case Message.APP_NETWORK_IS_UNREACHABLE: {
        isNetworkReachable = false;
    } break;

    default:
        qDebug() << "ERROR : General Internal pac action in RTCPlayer "
                 << "non-catched :" + Message.toString(message);
    }
}

