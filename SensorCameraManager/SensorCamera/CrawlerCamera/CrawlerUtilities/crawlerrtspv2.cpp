#include "crawlerrtspv2.h"

CrawlerRTSPv2::CrawlerRTSPv2()
{
    flagOpen = false;
}

void CrawlerRTSPv2::setSource(QString rtspUri_) {
    rtspUri = rtspUri_;

    qDebug() << Q_FUNC_INFO << name << rtspUri;
}

void CrawlerRTSPv2::setName(QString name_) {
    name = name_;
}

bool CrawlerRTSPv2::onOpenSource() {
    qDebug() << Q_FUNC_INFO << "log_level" << av_log_get_level();

    // Try to open input stream
    const char* stimeoutStrs[] = {"10000", "20000", "30000", "40000", "50000", "60000"};
    int stimeoutIdx = 0, retVal = -1, retryCount = 0;
    while (1) {
        if (QThread::currentThread()->isInterruptionRequested())
            return false;

        pAVFormatContext = avformat_alloc_context();

        AVDictionary *opts = NULL;
        av_dict_set(&opts, "stimeout", stimeoutStrs[stimeoutIdx], 0); // Set socket TCP I/O timeout in microseconds.
        av_dict_set(&opts, "rtsp_transport", "tcp", 0);
        retVal = avformat_open_input(&pAVFormatContext, rtspUri.toLatin1().constData(), NULL, &opts);
        av_dict_free(&opts);
        if (retVal < 0) {
            //            if (retryCount < 3) {
            if (1) {
                retryCount++;
                //                qDebug() << "retryCount" << retryCount;
                stimeoutIdx = retryCount % (sizeof(stimeoutStrs)/sizeof(stimeoutStrs[0]));
                QThread::msleep(1);
            } else {
                qDebug() << Q_FUNC_INFO << name << rtspUri << "Open video input error. Giveup this source. retryCount" << retryCount++;
                return false;
            }
        } else {
            break;
        }
    }

    // Get streams information
    retVal = avformat_find_stream_info(pAVFormatContext, NULL);
    if (retVal < 0) {
        qDebug() << Q_FUNC_INFO << name << "Find stream info error";

        avformat_close_input(&pAVFormatContext);
        return false;
    }

    // Find video stream
    videoStreamIndex = -1;
    for (uint i = 0; i < pAVFormatContext->nb_streams; i++) {
        pInputStream = pAVFormatContext->streams[i];
        pAVCodecContext = pInputStream->codec;
        if ((pAVCodecContext != Q_NULLPTR) && (pAVCodecContext->codec_type == AVMEDIA_TYPE_VIDEO)) {
            videoStreamIndex = i;
            break;
        }
    }
    if (videoStreamIndex == -1) {
        qDebug() << Q_FUNC_INFO << name << "Get stream index error";

        avformat_close_input(&pAVFormatContext);
        return false;
    }
    int width = pAVCodecContext->width;
    int height = pAVCodecContext->height;

    qDebug() <<Q_FUNC_INFO << "WIDTH AVCodec " << width << "HEIGH AVCODEC" << height;
    if(width <= 0 && height <= 0){
        return false;
    }

    // Find and open video decoder
    pAVCodec = avcodec_find_decoder(pAVCodecContext->codec_id);
    if (!pAVCodec) {
        qDebug() << Q_FUNC_INFO << name << "avcodec_find_decoder error for codec_id" << pAVCodecContext->codec_id;

        avformat_close_input(&pAVFormatContext);
        return false;
    }

    retVal = avcodec_open2(pAVCodecContext, pAVCodec, NULL);
    if (retVal < 0) {
        qDebug() << Q_FUNC_INFO << name << "Open codec error";

        avformat_close_input(&pAVFormatContext);
        return false;
    }

    qDebug() << Q_FUNC_INFO << name << "Init stream success. retryCount" << retryCount;
    flagOpen = true;

    return flagOpen;
}

void CrawlerRTSPv2::onCloseSource() {
    if (flagOpen) {
        if(pAVFormatContext) {
            avformat_close_input(&pAVFormatContext);
        }
        flagOpen = false;
    }
    qDebug() << Q_FUNC_INFO << name;
}


qreal CrawlerRTSPv2::getDAR(AVFrame *f)
{
    // lavf 54.5.100 av_guess_sample_aspect_ratio: stream.sar > frame.sar
    qreal dar = 0;
    if (f->height > 0)
        dar = (qreal)f->width/(qreal)f->height;
    // prefer sar from AVFrame if sar != 1/1
    if (f->sample_aspect_ratio.num > 1)
        dar *= av_q2d(f->sample_aspect_ratio);
    else if (pAVCodecContext && pAVCodecContext->sample_aspect_ratio.num > 1) // skip 1/1
        dar *= av_q2d(pAVCodecContext->sample_aspect_ratio);
    return dar;
}

void CrawlerRTSPv2::onPlay() {
    qDebug() << Q_FUNC_INFO << name << "Running";

reconnect:
    if (!onOpenSource()) {
        if (QThread::currentThread()->isInterruptionRequested())
            return;
        onCloseSource();
        goto reconnect;
    }

    int nAVReadPacketFailed = 0;
    int frameFinished = 0;
    int ret = 0;
    AVPacket pkt1, *pAVPacket = &pkt1;
    AVFrame *pAVFrame = av_frame_alloc();
    while (flagOpen && !QThread::currentThread()->isInterruptionRequested()) {
        frameFinished = 0;
        ret = 0;
        if (av_read_frame(pAVFormatContext, pAVPacket) >= 0) {
            nAVReadPacketFailed = 0;
            if( pAVPacket->stream_index == videoStreamIndex ) {
               ret = avcodec_decode_video2(pAVCodecContext, pAVFrame, &frameFinished, pAVPacket);
                if (frameFinished) {
                    VideoFrame videoFrame(pAVCodecContext->width, pAVCodecContext->height, VideoFormat((int)pAVCodecContext->pix_fmt));
                    videoFrame.setDisplayAspectRatio(getDAR(pAVFrame));
                    videoFrame.setBits(pAVFrame->data);
                    videoFrame.setBytesPerLine(pAVFrame->linesize);

                    long timestamp = 0;
                    if (pAVFrame->pkt_pts != AV_NOPTS_VALUE)
                        timestamp = (pAVFrame->pkt_pts * av_q2d(pAVCodecContext->pkt_timebase)) * 1000;

                    Q_EMIT addFrameObject(true, videoFrame.clone(), timestamp);
                }
            }
        } else {
            //            qDebug() << Q_FUNC_INFO << name << "read packet failed" << nAVReadPacketFailed;
            nAVReadPacketFailed++;
            if(nAVReadPacketFailed >= 25){
                av_packet_unref(pAVPacket);
                av_frame_free(&pAVFrame);
                onCloseSource();
                goto reconnect;
            }
        }

        av_packet_unref(pAVPacket);
    }

    av_frame_free(&pAVFrame);

    qDebug() << Q_FUNC_INFO << name << "Stopped";
}
