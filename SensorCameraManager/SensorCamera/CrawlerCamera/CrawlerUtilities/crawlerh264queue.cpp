#include "crawlerh264queue.h"

bool CrawlerH264Queue::isWorking()
{
    return working;
}

void CrawlerH264Queue::setWorking(bool value)
{
    queueUpdate.lock();
    working = value;
    queueUpdate.unlock();
}

CrawlerH264Queue::CrawlerH264Queue()
{
    this->name = name;
    this->dequeueIndex = 0;
}

quint64 CrawlerH264Queue::getNextPakageNumber()
{
    return dequeueIndex;
}

QString CrawlerH264Queue::getName()
{
    return name;
}

void CrawlerH264Queue::setName(const QString &value)
{
    name = value;
}

void CrawlerH264Queue::enqueue(quint64 index, QByteArray rtpData) {
    int size = getSize();
    int fps = 25;
    queueUpdate.lock();
    if (isWorking() && (index  >= dequeueIndex)) {
        hashmap.insert(index, rtpData);
    } else {
        qDebug() << getName() << "DROP!! incomming index=" << index << " dequeue index=" << dequeueIndex;
    }
    queueUpdate.unlock();

    if (size > 5 * fps) {
        qDebug() << getName() << " size=" << size << " .... EMPTY queue";
    }
}

QByteArray CrawlerH264Queue::dequeue() {
    QByteArray rtpData;
    int size = getSize();
    queueUpdate.lock();
//    qDebug() << name <<"CrawlerH264Queue::dequeue::[" << dequeueIndex << "] from " << hashmap.keys();

    rtpData = hashmap.take(dequeueIndex);
    if (!rtpData.isNull() && !rtpData.isEmpty()) {
        dequeueIndex++;
    }
    queueUpdate.unlock();
    return rtpData;
}


void CrawlerH264Queue::next() {
    queueUpdate.lock();
    hashmap.remove(dequeueIndex);
    if (hashmap.size() > 0) {
        quint64 firstKey = hashmap.firstKey();
        qDebug() << "CrawlerH264Queue::NEXT dequeueIndex + " << firstKey - dequeueIndex << " = " << firstKey;
        dequeueIndex = firstKey;
    }
    else {
        dequeueIndex++;
        //        qDebug() << "CrawlerH264Queue::NEXT dequeueIndex + 1 = " << dequeueIndex;
    }
    queueUpdate.unlock();
}

void CrawlerH264Queue::empty() {
    queueUpdate.lock();
    hashmap.clear();
    dequeueIndex = 0;
    queueUpdate.unlock();
}
