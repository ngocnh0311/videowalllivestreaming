#include "crawlerrtpqueue.h"

bool CrawlerRTPQueue::isWorking()
{
    return working;
}

void CrawlerRTPQueue::setWorking(bool value)
{
    queueUpdate.lock();
    working = value;
    queueUpdate.unlock();
}

CrawlerRTPQueue::CrawlerRTPQueue()
{
    this->name = name;
    this->dequeueIndex = 0;
}

quint64 CrawlerRTPQueue::getNextPakageNumber()
{
    return dequeueIndex;
}

QString CrawlerRTPQueue::getName()
{
    return name;
}

void CrawlerRTPQueue::setName(const QString &value)
{
    name = value;
}

void CrawlerRTPQueue::enqueue(quint64 index, QByteArray rtpData) {
    int size = getSize();
    int fps = 25;
    queueUpdate.lock();
    if (isWorking() && (index  >= dequeueIndex)) {
        hashmap.insert(index, rtpData);
    } else {
        qDebug() << getName() << "DROP!! incomming index=" << index << " dequeue index=" << dequeueIndex <<"isWorking"<<isWorking();
    }
    queueUpdate.unlock();

    if (size > 5 * fps) {
        qDebug() << getName() << " size=" << size << " .... EMPTY queue";
    }
}

QByteArray CrawlerRTPQueue::dequeue() {
    QByteArray rtpData;
    int size = getSize();
    queueUpdate.lock();
//    qDebug() << name <<"CrawlerRTPQueue::dequeue::[" << dequeueIndex << "] from " << hashmap.keys();

    rtpData = hashmap.take(dequeueIndex);
    if (!rtpData.isNull() && !rtpData.isEmpty()) {
        dequeueIndex++;
    }
    queueUpdate.unlock();
    return rtpData;
}


void CrawlerRTPQueue::next() {
    queueUpdate.lock();
    hashmap.remove(dequeueIndex);
    if (hashmap.size() > 0) {
        quint64 firstKey = hashmap.firstKey();
        qDebug() << "CrawlerRTPQueue::NEXT dequeueIndex + " << firstKey - dequeueIndex << " = " << firstKey;
        dequeueIndex = firstKey;
    }
    else {
        dequeueIndex++;
        //        qDebug() << "CrawlerRTPQueue::NEXT dequeueIndex + 1 = " << dequeueIndex;
    }
    queueUpdate.unlock();
}

void CrawlerRTPQueue::empty() {
    queueUpdate.lock();
    hashmap.clear();
    dequeueIndex = 0;
    queueUpdate.unlock();
}
