#ifndef CrawlerDownloader_H
#define CrawlerDownloader_H

#include <QObject>
#include <QHash>
#include <QDebug>
#include <QThread>
#include <QNetworkAccessManager>
#include "crawlersocket.h"
#include <QMutex>
#include "crawlerrtpqueue.h"
#include "../c_crawlercamera.h"
class Socket;
class CrawlerDownloader: public QObject
{
    Q_OBJECT

private:
    bool readyWorking;
    C_CrawlerCamera *cCrawlerCamera;
    QNetworkAccessManager *naManager;

    bool stopLinkDownloadForSavingBW = false;
    QMutex CrawlerDownloaderMutex;
    quint64 lastLinkIndex = 0;
    QHash<QNetworkReply*, quint64> replyToIndex;
    void onReceiveDownloadDataFailed(quint64 linkIndex, QString errorString);
    void onReceiveDownloadedData(quint64 linkIndex, QByteArray payload);
    void onDropLinkDownloadForSavingBW(quint64 linkIndex);

public:
    CrawlerDownloader(QNetworkAccessManager *naManager, C_CrawlerCamera *cCrawlerCamera);
    ~CrawlerDownloader();

    void resetInternalState();
    void onReceiveDowloadRTCData(quint64 linkIndex, QString url);

    bool getStopLinkDownloadForSavingBW();
    void setStopLinkDownloadForSavingBW(bool value);
    void setNetworkManager(QNetworkAccessManager *_naManager);
    void setReadyWorking(bool value);

public Q_SLOTS:
    void download_progress(qint64,qint64);
    void onDownloadFinished(QNetworkReply *reply);
};

#endif // CrawlerDownloader_H
