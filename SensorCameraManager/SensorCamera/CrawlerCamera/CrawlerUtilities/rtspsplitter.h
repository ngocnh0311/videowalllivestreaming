#ifndef RTSPRTPSPITTER_H
#define RTSPRTPSPITTER_H

#include <QObject>
#include <QDebug>
#include <QDateTime>
#include <QImage>
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
#include <libavformat/avformat.h>
}

typedef struct rtp_header_struct {
    unsigned int version:2;     /* protocol version */
    unsigned int padding:1;     /* padding flag */
    unsigned int extension:1;   /* header extension flag */
    unsigned int cc:4;          /* CSRC count */
    unsigned int marker:1;      /* marker bit */
    unsigned int pt:7;          /* payload type */
    uint16_t seq:16;            /* sequence number */
    uint32_t ts;                /* timestamp */
    uint32_t ssrc;              /* synchronization source */
    uint32_t csrc[1];           /* optional CSRC list */
} rtp_header_t;

typedef struct nal_header_struct {
    unsigned int nal_forbidden_zero:1;
    unsigned int nal_nri:2;
    unsigned int nal_type:5;
} nal_header_t;

/* Enumeration of H.264 NAL unit types */
enum {
    NAL_TYPE_UNDEFINED = 0,
    NAL_TYPE_SINGLE_NAL_MIN	= 1,
    NAL_TYPE_SINGLE_NAL_MAX	= 23,
    NAL_TYPE_STAP_A		= 24,
    NAL_TYPE_FU_A		= 28,
};

class RTSPSpitter : public QObject
{
    Q_OBJECT

    bool doDebug = false;

    QByteArray NALUStartCode = QByteArray("\x00\x00\x01", 3);

    AVCodec *avCodec;
    AVCodecContext *avCodecContext;
    struct SwsContext *swsContext;

    QByteArray rtspStreamData;
    QByteArray FUAPacket;
    long pktTimestampMsec;
    long lastRtpTimestamp;

    void parseRTPPacket(QByteArray payload);
    void displayRTPHeader(rtp_header_t rtp_header);
    void parseNalHeaderByte(char header, nal_header_t &nal_header);
    void displayNalHeader(nal_header_t nal_header);
    void parseFUAPacket(long timestamp, QByteArray payload);

    void handleH264Packet(long timestamp, QByteArray payload);
    QImage decodeH264Packet(long timestamp, QByteArray h264Packet);
    void decoderClose();
    void decoderOpen();
public:
    RTSPSpitter();

    void handleStreamData(QByteArray payload);
Q_SIGNALS:
    void addFrameObject(bool isUseFrameObject, QImage image, long timestamp);
};

#endif // RTSPRTPSPITTER_H
