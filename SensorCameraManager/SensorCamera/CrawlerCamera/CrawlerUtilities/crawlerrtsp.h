#ifndef CRAWLERRTSP_H
#define CRAWLERRTSP_H

#include <QObject>
#include <QDebug>
#include <QTcpSocket>
#include "Camera/camitem.h"
#include <SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/rtspsplitter.h>

enum RTSP_STEP {
    RTSP_DISCONNECTED = -1,
    RTSP_CONNECTED,
    RTSP_CMD_OPTIONS,
    RTSP_CMD_DESCRIBE,
    RTSP_CMD_SETUP,
    RTSP_CMD_PLAY,
    RTSP_CMD_TEARDOWN,
    RTSP_PLAYING
};

class CrawlerRTSP : public QObject
{
    Q_OBJECT
    bool doDebug = false;
    CrawlerType crawlerType;
    QString crawlerName;
    QString sourceUri;
    QString host;
    QString port;
    QString session;

    int rtspStep;
    int rtspCseq;

    QTcpSocket *socket;
    RTSPSpitter *rtspRtpSplitter;

    void updateServerInfo();
    void clearServerInfo();

    void rtspSendCommand(int command);
    void rtspCommandOptions();
    void rtspCommandDescribe();
    void rtspCommandSetup();
    void rtspCommandPlay();
    void rtspCommandTeardown();

    void rtspCommandStatus(QByteArray response);
    void updateSession(QByteArray response);

    void cseqInc();

    void connectSocketSignal();
    void disConnectSocketSignal();

public:
    CrawlerRTSP();

    void setUri(QString uri);
    void clearUri();
    void doConnect();

private Q_SLOTS:
    void onTcpsocketConnected();
    void onTcpsocketDisconnected();
    void onReadyRead();

public Q_SLOTS:
    void onStartWorking();
    void onStopWorking();

Q_SIGNALS:
    void addFrameObject(bool isUseFrameObject, QImage image, long timestamp);
    void stopped();
};

#endif // CRAWLERRTSP_H
