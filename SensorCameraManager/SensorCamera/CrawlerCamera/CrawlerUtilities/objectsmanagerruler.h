#ifndef OBJECTSMANAGERRULER_H
#define OBJECTSMANAGERRULER_H
#include <QThread>
#include <QObject>
class ObjectsManagerRuler : public QThread
{
protected:
    void run();
public:
    ObjectsManagerRuler();
};

#endif // OBJECTSMANAGERRULER_H
