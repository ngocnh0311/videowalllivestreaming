#include "crawlerimagequeue.h"

double CrawlerImageQueue::getTimeWait()
{
    double delay;
    queueUpdate.lock();
    delay = timeWait;
    queueUpdate.unlock();
    return delay;
}

QImage CrawlerImageQueue::getDequeueImage()
{
    QImage image;
    queueUpdate.lock();
    //    dequeueImage = this->hashmap.take(this->hashmap.firstKey());
    image = dequeueImage;
    queueUpdate.unlock();
    //    qDebug()<< QDateTime::currentMSecsSinceEpoch() << this << Q_FUNC_INFO << image.size();
    return image;
}

bool CrawlerImageQueue::isWorking()
{
    return working;
}

void CrawlerImageQueue::setWorking(bool value)
{
    queueUpdate.lock();
    working = value;
    queueUpdate.unlock();
}

CrawlerImageQueue::CrawlerImageQueue()
{
}

void CrawlerImageQueue::enqueue(double timestamp, QImage newImage) {
    int size = getSize();
    int fps = 25;
    if (size > 6*fps) {
        qDebug() << getName() << "::Image Enqueue size=" << size << " .... QUEUE NEED EMPTY";
        empty();
    }

    queueUpdate.lock();
    if(isWorking()){
        hashmap.insert(timestamp, newImage);
    }
    queueUpdate.unlock();
}

QString CrawlerImageQueue::getName() const
{
    return name;
}

void CrawlerImageQueue::setName(const QString &value)
{
    name = value;
}

void CrawlerImageQueue::dequeue() {
    QImage image;
    double currentImageKey, nextImageKey;
    queueUpdate.lock();
    if(hashmap.size() > 1 ) {
        currentImageKey   = hashmap.firstKey();
        image = hashmap.take(currentImageKey);
        nextImageKey = hashmap.firstKey();
        long delay = nextImageKey- currentImageKey;
        if (delay <= 0 || delay > 100) {
            this->timeWait = 40;
        } else {
            this->timeWait = delay;
        }
    }

    queueUpdate.unlock();
    this->dequeueImage = image;
}

void CrawlerImageQueue::empty() {
    queueUpdate.lock();
    hashmap.clear();
    queueUpdate.unlock();
}

// return video time length in second in the queue
long CrawlerImageQueue::getVideoTime() {
    long videoTime = 1;
    queueUpdate.lock();
    if (hashmap.size() > 0) {
        double firstTimestamp = hashmap.firstKey();
        double lastTimestamp = hashmap.lastKey();
        videoTime = lastTimestamp - firstTimestamp + 1;
    }
    queueUpdate.unlock();
    if (videoTime < 0) videoTime = 10000000;
    return videoTime;
}
