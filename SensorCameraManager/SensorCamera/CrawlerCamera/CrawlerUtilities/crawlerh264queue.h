#ifndef CrawlerH264Queue_H
#define CrawlerH264Queue_H

#include <QMap>
#include <QByteArray>
#include <QThread>
#include <QDebug>
#include <QMutex>

class CrawlerH264Queue
{
private:
    QMap<quint64, QByteArray> hashmap;
    quint64 dequeueIndex = 0;
    QMutex queueUpdate;
    long maxRtpSize = 0;
    QString name = "CrawlerH264Queue";
    bool working = false;

public:
    CrawlerH264Queue();
    QByteArray dequeue();
    void enqueue(quint64 index, QByteArray rtpData);
    void next();
    quint64 getNextPakageNumber();
    QString getName();
    void setName(const QString &value);
    void empty();
    int getSize() { int size; queueUpdate.lock(); size=hashmap.size(); queueUpdate.unlock(); return size;}
    bool isWorking();
    void setWorking(bool value);
};

#endif // CrawlerH264Queue_H
