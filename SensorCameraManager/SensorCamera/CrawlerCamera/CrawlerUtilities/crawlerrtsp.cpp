#include "crawlerrtsp.h"

CrawlerRTSP::CrawlerRTSP()
{
    doDebug = true;
    sourceUri = "";
    rtspCseq = 0;

    socket = Q_NULLPTR;

    rtspRtpSplitter = new RTSPSpitter();
    connect(rtspRtpSplitter , &RTSPSpitter::addFrameObject , this, &CrawlerRTSP::addFrameObject);
}

void CrawlerRTSP::setUri(QString uri) {
    sourceUri = uri;

    updateServerInfo();

    if (doDebug)
        qDebug() << Q_FUNC_INFO << sourceUri;
}

void CrawlerRTSP::updateServerInfo() {
    // rtsp://10.13.11.211:554/av0_1

    QString server = sourceUri.section('/', 2, 2);
    host = server.section(':', 0,0);
    port = server.section(':', 1,1);
    if (port.isEmpty()) {
        port = "554";
    }

    if (doDebug)
        qDebug() << Q_FUNC_INFO << host << port;
}

void CrawlerRTSP::clearUri() {
    rtspCseq = 0;
    rtspStep = RTSP_DISCONNECTED;
}

void CrawlerRTSP::clearServerInfo() {
    host = port = "";
}


void CrawlerRTSP::connectSocketSignal() {
    connect(socket, &QTcpSocket::connected, this, &CrawlerRTSP::onTcpsocketConnected);
    connect(socket, &QTcpSocket::disconnected, this, &CrawlerRTSP::onTcpsocketDisconnected);
    connect(socket, &QTcpSocket::readyRead, this, &CrawlerRTSP::onReadyRead);
}

void CrawlerRTSP::doConnect() {
    socket->connectToHost(host, port.toInt());
}

void CrawlerRTSP::onTcpsocketConnected() {
    if (doDebug)
        qDebug() << Q_FUNC_INFO;

    rtspStep = RTSP_CONNECTED;
    rtspSendCommand(RTSP_CMD_OPTIONS);
}

void CrawlerRTSP::rtspSendCommand(int command) {
    switch (command) {
    case RTSP_CMD_OPTIONS:
        rtspCommandOptions();
        break;
    case RTSP_CMD_DESCRIBE:
        rtspCommandDescribe();
        break;
    case RTSP_CMD_SETUP:
        rtspCommandSetup();
        break;
    case RTSP_CMD_PLAY:
        rtspCommandPlay();
        break;
    case RTSP_CMD_TEARDOWN:
        rtspCommandTeardown();
        break;
    default:
        break;
    }
}

void CrawlerRTSP::rtspCommandOptions() {
    rtspStep = RTSP_CMD_OPTIONS;

    QString cmd = QString("OPTIONS rtsp://%1:%2 RTSP/1.0\r\nCSeq: %3\r\n\r\n")
            .arg(host, port, QString::number(rtspCseq));
    socket->write(cmd.toLatin1().data());
    cseqInc();

    if (doDebug)
        qDebug() << Q_FUNC_INFO << cmd;
}

void CrawlerRTSP::rtspCommandDescribe() {
    rtspStep = RTSP_CMD_DESCRIBE;

    QString cmd = QString("DESCRIBE %1 RTSP/1.0\r\nCSeq: %2\r\nAccept: application/sdp\r\n\r\n")
            .arg(sourceUri, QString::number(rtspCseq));
    socket->write(cmd.toLatin1().data());
    cseqInc();

    if (doDebug)
        qDebug() << Q_FUNC_INFO << cmd;
}

void CrawlerRTSP::rtspCommandSetup() {
    rtspStep = RTSP_CMD_SETUP;

    QString cmd = QString("SETUP %1/trackID=1 RTSP/1.0\r\nCSeq: %2\r\nTransport: RTP/AVP/TCP;interleaved=0-1;\r\n\r\n")
            .arg(sourceUri, QString::number(rtspCseq));
    socket->write(cmd.toLatin1().data());
    cseqInc();

    if (doDebug)
        qDebug() << Q_FUNC_INFO << cmd;
}

void CrawlerRTSP::rtspCommandPlay() {
    rtspStep = RTSP_CMD_PLAY;

    QString cmd = QString("PLAY %1 RTSP/1.0\r\nCSeq: %2\r\nSession: %3\r\nRange: npt=0.00-\r\n\r\n")
            .arg(sourceUri, QString::number(rtspCseq), session);
    socket->write(cmd.toLatin1().data());
    cseqInc();

    if (doDebug)
        qDebug() << Q_FUNC_INFO << cmd;
}

void CrawlerRTSP::rtspCommandTeardown() {
    rtspStep = RTSP_CMD_TEARDOWN;

    QString cmd = QString("TEARDOWN %1  RTSP/1.0\r\n\r\nCSeq:%2\r\n\r\n")
            .arg(sourceUri, QString::number(rtspCseq));
    socket->write(cmd.toLatin1().data());
    cseqInc();

    if (doDebug)
        qDebug() << Q_FUNC_INFO << cmd;
}

void CrawlerRTSP::onReadyRead() {
    QByteArray payload = socket->readAll();
    if (rtspStep == RTSP_PLAYING) {
        rtspRtpSplitter->handleStreamData(payload);
    } else {
        rtspCommandStatus(payload);
    }
}

void CrawlerRTSP::rtspCommandStatus(QByteArray response) {
    if (doDebug)
        qDebug() << Q_FUNC_INFO << "Step" << rtspStep << response;

    if (rtspStep == RTSP_CMD_SETUP) {
        updateSession(response);
    }

    if (rtspStep == RTSP_CMD_PLAY) {
        rtspStep = RTSP_PLAYING;
    }

    if (rtspStep > RTSP_CONNECTED && rtspStep < RTSP_CMD_PLAY) {
        int nextCmd;
        if (rtspStep == RTSP_CMD_OPTIONS) {
            nextCmd = RTSP_CMD_DESCRIBE;
        } else if (rtspStep == RTSP_CMD_DESCRIBE) {
            nextCmd = RTSP_CMD_SETUP;
        } else if (rtspStep == RTSP_CMD_SETUP) {
            nextCmd = RTSP_CMD_PLAY;
        }
        rtspSendCommand(nextCmd);
    }
}

void CrawlerRTSP::updateSession(QByteArray response) {
    QString responseStr(response.constData());

    int sessionStrStart = responseStr.indexOf("Session");
    int sessionStrEnd = responseStr.indexOf("\r\n", sessionStrStart);
    QString sessionStr = responseStr.mid(sessionStrStart, sessionStrEnd - sessionStrStart);
    session = sessionStr.replace(" ", "").section(':', 1);

    if (doDebug)
        qDebug() << Q_FUNC_INFO << session;
}

void CrawlerRTSP::cseqInc() {
    rtspCseq++;
}
void CrawlerRTSP::onStartWorking() {
    qDebug() << Q_FUNC_INFO<< QThread::currentThread();

    if (socket == Q_NULLPTR) {
        socket = new QTcpSocket;
    }

    connectSocketSignal();

    doConnect();
}


void CrawlerRTSP::onStopWorking() {
    qDebug() << Q_FUNC_INFO << "CrawlerRTSP onStopWorking" << QThread::currentThread();
    if (rtspStep == RTSP_PLAYING) {
        disConnectSocketSignal();
        rtspSendCommand(RTSP_CMD_TEARDOWN);
        clearUri();
        socket->abort();
        Q_EMIT stopped();
    }
}

void CrawlerRTSP::disConnectSocketSignal() {
    disconnect(socket, &QTcpSocket::connected, this, &CrawlerRTSP::onTcpsocketConnected);
    disconnect(socket, &QTcpSocket::disconnected, this, &CrawlerRTSP::onTcpsocketDisconnected);
    disconnect(socket, &QTcpSocket::readyRead, this, &CrawlerRTSP::onReadyRead);
}

void CrawlerRTSP::onTcpsocketDisconnected() {
    if (doDebug)
        qDebug() << Q_FUNC_INFO;
    rtspStep = RTSP_DISCONNECTED;
}
