#include "rtspsplitter.h"

RTSPSpitter::RTSPSpitter()
{
    doDebug = false;

    rtspStreamData.clear();
    FUAPacket.clear();

    pktTimestampMsec = -1;
    lastRtpTimestamp = -1;

    decoderOpen();
}

void RTSPSpitter::decoderOpen() {
    avCodecContext = Q_NULLPTR;

    avCodec = avcodec_find_decoder(AV_CODEC_ID_H264);
    if (!avCodec) {
        // TODO
    }
    avCodecContext = avcodec_alloc_context3(avCodec);
    if (!avCodecContext) {
        // TODO
    }else{
        if (avcodec_open2(avCodecContext, avCodec, NULL) < 0) {
            // TODO
        }
        avCodecContext->thread_type = FF_THREAD_SLICE | FF_THREAD_FRAME;//default;
        avCodecContext->thread_count = 8;
        avCodecContext->skip_frame = AVDISCARD_DEFAULT;
        avCodecContext->skip_loop_filter = AVDISCARD_DEFAULT;
        avCodecContext->skip_idct = AVDISCARD_DEFAULT;
    }
}

void RTSPSpitter::decoderClose() {
    if(avCodecContext) {
        avcodec_free_context(&avCodecContext);
    }
}

void RTSPSpitter::handleStreamData(QByteArray payload) {
    rtspStreamData.append(payload);

    int dataLength = rtspStreamData.size();
    int dataOffset = 0;
    int channel = 0;
    int rtpLength = 0;
    while (dataOffset < (dataLength - 4)) {
        // format: [Magic Number(0x24) - 1byte][Transport Channel - 1byte][RTP data length - 2bytes][RTP data]
        if (rtspStreamData.at(dataOffset) == 0x24) {
            channel = rtspStreamData.at(dataOffset+1);
            bool ok;
            rtpLength = rtspStreamData.mid(dataOffset+2, 2).toHex().toInt(&ok, 16);

            if (doDebug)
                qDebug() << Q_FUNC_INFO << "RTSP Magic: 0x24" << "Channel:" << channel << "Payload Length: " << rtpLength;

            dataOffset += 4;

            if (rtpLength > (dataLength - dataOffset)) {
                dataOffset -= 4;
                if (doDebug)
                    qDebug() << Q_FUNC_INFO << "Pending" << rtpLength - (dataLength - dataOffset);
                break;
            }

            if (channel == 0) {
                if (doDebug)
                    qDebug() << Q_FUNC_INFO << "RTP data. Length: " << rtpLength;
                parseRTPPacket(rtspStreamData.mid(dataOffset, rtpLength));
            } else {
                if (doDebug)
                    qDebug() << Q_FUNC_INFO << "RTCP data";
            }
            dataOffset += rtpLength;
            continue;
        } else {
            dataOffset++;
            if (doDebug)
                qDebug() << Q_FUNC_INFO << "Next byte" << dataOffset;
        }
    }

    rtspStreamData = rtspStreamData.mid(dataOffset);
}

#define CHECK_BIT(var, pos) !!((var) & (1 << (pos)))
void RTSPSpitter::parseRTPPacket(QByteArray payload) {
    int dataOffset = 0;
    rtp_header_t rtp_header;

    char firstByte = payload.at(dataOffset);
    rtp_header.version = firstByte >> 6;
    rtp_header.padding = CHECK_BIT(firstByte, 5);
    rtp_header.extension = CHECK_BIT(firstByte, 6);
    rtp_header.cc = firstByte & 0xFF;
    dataOffset += 1;

    char secondByte = payload.at(dataOffset);
    rtp_header.marker = CHECK_BIT(secondByte, 8);
    rtp_header.pt = secondByte & 0x7F;
    dataOffset += 1;

    bool ok;
    rtp_header.seq = payload.mid(dataOffset, 2).toHex().toInt(&ok, 16);
    dataOffset += 2;

    rtp_header.ts = payload.mid(dataOffset, 4).toHex().toInt(&ok, 16);
    dataOffset += 4;

    rtp_header.ssrc = payload.mid(dataOffset, 4).toHex().toInt(&ok, 16);
    dataOffset += 4;

    if (doDebug)
        displayRTPHeader(rtp_header);

    QByteArray rtpPayload = payload.mid(dataOffset);

    nal_header_t nal_header;
    parseNalHeaderByte(rtpPayload.at(0), nal_header);

    if (doDebug)
        displayNalHeader(nal_header);

    int nal_type = nal_header.nal_type;
    /* Single NAL unit packet */
    if (nal_type >= NAL_TYPE_SINGLE_NAL_MIN &&
            nal_type <= NAL_TYPE_SINGLE_NAL_MAX) {
        handleH264Packet(rtp_header.ts, rtpPayload);
    }
    /*
     * Agregation packet - STAP-A
     * ------
     * http://www.ietf.org/rfc/rfc3984.txt
     *
     * 0                   1                   2                   3
     * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |                          RTP Header                           |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |STAP-A NAL HDR |         NALU 1 Size           | NALU 1 HDR    |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |                         NALU 1 Data                           |
     * :                                                               :
     * +               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |               | NALU 2 Size                   | NALU 2 HDR    |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |                         NALU 2 Data                           |
     * :                                                               :
     * |                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |                               :...OPTIONAL RTP padding        |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
    else if (nal_type == NAL_TYPE_STAP_A) {
    } else if (nal_type == NAL_TYPE_FU_A) {
        parseFUAPacket(rtp_header.ts, rtpPayload);
    } else if (nal_type == NAL_TYPE_UNDEFINED) {

    } else {

    }

    dataOffset += rtpPayload.size();
}

inline void RTSPSpitter::displayRTPHeader(rtp_header_t rtp_header) {
    qDebug() << "   >> RTP";
    qDebug() << "      Version     :" <<  rtp_header.version;
    qDebug() << "      Padding     :" <<  rtp_header.padding;
    qDebug() << "      Extension   :" <<  rtp_header.extension;
    qDebug() << "      CSRC Count  :" <<  rtp_header.cc;
    qDebug() << "      Marker      :" <<  rtp_header.marker;
    qDebug() << "      Payload Type:" <<  rtp_header.pt;
    qDebug() << "      Sequence    :" <<  rtp_header.seq;
    qDebug() << "      Timestamp   :" <<  rtp_header.ts;
    qDebug() << "      Sync Source :" <<  rtp_header.ssrc;
}

inline void RTSPSpitter::parseNalHeaderByte(char header, nal_header_t &nal_header) {
    /*
     * NAL, first byte header
     *
     *   +---------------+
     *   |0|1|2|3|4|5|6|7|
     *   +-+-+-+-+-+-+-+-+
     *   |F|NRI|  Type   |
     *   +---------------+
     */
    nal_header.nal_forbidden_zero = CHECK_BIT(header, 7);
    nal_header.nal_nri  = (header & 0x60) >> 5;
    nal_header.nal_type = (header & 0x1F);
}

inline void RTSPSpitter::parseFUAPacket(long timestamp, QByteArray payload) {
    char fuIndicator = payload.at(0);
    char fuHeader = payload.at(1);

    uint8_t h264_start_bit = fuHeader & 0x80;
    uint8_t h264_end_bit   = fuHeader & 0x40;
    uint8_t h264_type      = fuHeader & 0x1F;
    uint8_t h264_nri       = (fuIndicator & 0x60) >> 5;
    uint8_t h264_key       = (h264_nri << 5) | h264_type;

    QString fuType = " mid";
    if (h264_start_bit) {
        fuType = " start";
        QByteArray fuNalType;
        FUAPacket = fuNalType.append(h264_key);
    }
    FUAPacket += payload.mid(2);

    if (h264_end_bit) {
        fuType = " end";
        QString fuStr = "         >> Fragmentation Unit. NAL Type: " + QString::number(h264_type) + fuType;
        if (doDebug)
            qDebug() << fuStr.toLatin1().data();

        handleH264Packet(timestamp, FUAPacket);
        FUAPacket.clear();
    } else {
        QString fuStr = "         >> Fragmentation Unit. NAL Type: " + QString::number(h264_type) + fuType;
        if (doDebug)
            qDebug() << fuStr.toLatin1().data();
    }
}

#define VIDEO_CLOCK_RATE 90000 // HZ
inline void RTSPSpitter::handleH264Packet(long timestamp, QByteArray payload) {
    char nal_header_byte = payload.at(0);
    nal_header_t nal_header;
    parseNalHeaderByte(nal_header_byte, nal_header);

    if (doDebug)
        displayNalHeader(nal_header);

    long timeDiffMsec = 0;
    if (pktTimestampMsec == -1) {
        pktTimestampMsec = QDateTime::currentMSecsSinceEpoch();
    } else {
        timeDiffMsec = ((double)(timestamp - lastRtpTimestamp) / VIDEO_CLOCK_RATE) * 1000;
        pktTimestampMsec = pktTimestampMsec + timeDiffMsec;
    }

    if (doDebug)
        qDebug() << "         >> RTP timestamp" << timestamp << "Pkt timestamp" << pktTimestampMsec << "timeDiffMsec" << timeDiffMsec;
    QImage image = decodeH264Packet(pktTimestampMsec, NALUStartCode + payload);
    if(!image.isNull()) {
        Q_EMIT addFrameObject(true, image, timestamp);
        if (doDebug)
            qDebug() << "         >> Image timestamp" << timestamp << "Pkt timestamp" << pktTimestampMsec;
    }

    lastRtpTimestamp = timestamp;
}

inline void RTSPSpitter::displayNalHeader(nal_header_t nal_header) {
    qDebug() << "      >> NAL";
    qDebug() << "         Forbidden zero:" << nal_header.nal_forbidden_zero;
    qDebug() << "         NRI           :" << nal_header.nal_nri;
    qDebug() << "         Type          :" << nal_header.nal_type;
}

QImage RTSPSpitter::decodeH264Packet(long timestamp, QByteArray h264Packet) {
    QImage image;
    int payloadLength = h264Packet.size();
    if (payloadLength > 0) {
        int ret;
        uint8_t *data = new uint8_t[payloadLength];
        memcpy(data, h264Packet.constData(), payloadLength);

        AVPacket avPacket;
        av_init_packet(&avPacket);
        avPacket.size = payloadLength;
        avPacket.data = data;

        AVFrame *avFrame = av_frame_alloc();

#define USE_DEPRECATED_API
#ifdef USE_DEPRECATED_API
        int got_picture = 0;
        ret = avcodec_decode_video2(avCodecContext, avFrame, &got_picture, &avPacket);
        if (ret > 0 && got_picture > 0) {
            // convert frame pix-format to RGB24
            uint8_t *dst_data[4];
            int dst_linesize[4];
            struct SwsContext *swsContext = sws_getContext(avCodecContext->width, avCodecContext->height,
                                                           avCodecContext->pix_fmt,
                                                           avCodecContext->width, avCodecContext->height,
                                                           AV_PIX_FMT_RGB24,
                                                           SWS_BICUBIC, NULL, NULL, NULL);

            int bufferSize = av_image_get_buffer_size(AV_PIX_FMT_RGB24, avFrame->width, avFrame->height, 1);
            uint8_t *srcBuffer = (uint8_t *)av_malloc(bufferSize);
            av_image_fill_arrays(dst_data, dst_linesize,
                                 srcBuffer,
                                 AV_PIX_FMT_RGB24, avFrame->width, avFrame->height, 1);
            sws_scale(swsContext,
                      (const uint8_t * const *) avFrame->data, (const int *)avFrame->linesize,
                      0,
                      avFrame->height,
                      dst_data, dst_linesize);


            // create QImage and enqueue
            image = QImage(dst_data[0],
                    avFrame->width,
                    avFrame->height,
                    dst_linesize[0],
                    QImage::Format_RGB888).copy();

            av_free(srcBuffer);
            sws_freeContext(swsContext);
        }
#else
        char error[128];

        ret = avcodec_send_packet(avCodecContext, &avPacket);
        if (ret < 0) {
            av_strerror(ret, error, sizeof(error));
            //qDebug() << Q_FUNC_INFO << "Error avcodec_send_packet: " << QString(error) << (ret == AVERROR(EAGAIN) ? "EAGAIN" : "Not EAGAIN");
        }

        while (ret >= 0) {
            ret = avcodec_receive_frame(avCodecContext, avFrame);
            if (ret == AVERROR(EAGAIN)) {
                //qDebug() << Q_FUNC_INFO << "EAGAIN";
                break;
            }
            else if (ret < 0) {
                av_strerror(ret, error, sizeof(error));
                qDebug() << Q_FUNC_INFO << "Error during decoding: " << QString(error);
                break;
            }

            //qDebug() << Q_FUNC_INFO << "got picture" << avFrame->pict_type;

            // convert frame pix-format to RGB24
            uint8_t *dst_data[4];
            int dst_linesize[4];
            struct SwsContext *swsContext = sws_getContext(avCodecContext->width, avCodecContext->height,
                                                           avCodecContext->pix_fmt,
                                                           avCodecContext->width, avCodecContext->height,
                                                           AV_PIX_FMT_RGB24,
                                                           SWS_BICUBIC, NULL, NULL, NULL);

            int bufferSize = av_image_get_buffer_size(AV_PIX_FMT_RGB24, avFrame->width, avFrame->height, 1);
            uint8_t *srcBuffer = (uint8_t *)av_malloc(bufferSize);
            av_image_fill_arrays(dst_data, dst_linesize,
                                 srcBuffer,
                                 AV_PIX_FMT_RGB24, avFrame->width, avFrame->height, 1);
            sws_scale(swsContext,
                      (const uint8_t * const *) avFrame->data, (const int *)avFrame->linesize,
                      0,
                      avFrame->height,
                      dst_data, dst_linesize);


            // create QImage and enqueue
            image = QImage(dst_data[0],
                    avFrame->width,
                    avFrame->height,
                    dst_linesize[0],
                    QImage::Format_RGB888).copy();

            av_free(srcBuffer);
            sws_freeContext(swsContext);
        }
#endif
        av_frame_free(&avFrame);
        delete[] data;
    }

    return image;
}
