﻿#ifndef CrawlerSocket_H
#define CrawlerSocket_H

#include <QDateTime>
#include <QImage>
#include <QList>
#include <QObject>
#include <QQueue>
#include <QStack>
#include <QHash>
#include <QVector>

#include <QtWebSockets/QWebSocket>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include "crawlersplitter.h"
#include <QMutex>
#include <message.h>
#include "crawlerdownloader.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
class CrawlerDownloader;

enum CrawlerSocketState {
  CRAWLERS_Disconnected,
  CRAWLERS_Connected,
  CRAWLERS_SentSignature,
  CRAWLERS_ReceivedSignature,
  CRAWLERS_ReceivedInfomation,
  CRAWLERS_ReceivedDownloadLink
};

class CrawlerSocket : public QObject {
  Q_OBJECT

private:
    C_CrawlerCamera *pCrawlerCamera;
    QString playerName = "Socket Crawler";
    AppMessage Message;
    bool playerIsPlaying = false;
    CrawlerDownloader *pDownloader = Q_NULLPTR;

    QWebSocket *socket;
    CrawlerSocketState socketState = CRAWLERS_Disconnected;
    bool autoReconnect = true;
    QTimer *reconnectTimer = Q_NULLPTR;

    QString socketUrl = "";
    QString linkFormat = "";
    bool isAllowReceiving = true;
    bool isSourceChanged = false;
    bool needToOpenSocketWhenOldSocketDisconnected = false;
    bool playerIsPlayingAndNeedToReconnectWhenSocketDisconnectedByAccident = false;
    quint64 linkIndex = 0;
    bool isSecondIdrReached = false;

    bool isNetworkReachable = true;

    QString host;
    QString mac;
    QString cam;

    void prepareLinkToDownload(QString message);
    void resetInternalState();

public:
    CrawlerSocket(C_CrawlerCamera *_crawlerCamera);
    ~CrawlerSocket();

    void openSource();
    void closeSource();
    void setSource(QString source);

    void openSocket();
    void sendSignature();
    void newAction(int message, QVariant *attachment);

    void onReceivedGopPackage(QString message);

Q_SIGNALS:
    void socketConnected();
    void socketDisconnected();
    void downloadRTCData(quint64 linkIndex, QString url);
    void resetTimerReopenSouce();
public Q_SLOTS:
    void onReOpenSocket();
    void onSocketConnected();
    void onSocketDisconnected();
    void onRawPackageIndexReceived(const QString &message);
};

#endif  // CrawlerSocket_H
