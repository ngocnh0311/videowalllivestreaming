#ifndef CRAWLERRTSPV2_H
#define CRAWLERRTSPV2_H

#include <QObject>
#include <QDebug>
#include <QThread>
#include <QMutex>
#include <QDateTime>
#include "Player/RTCPlayer/RTCRenderWidget.h"
extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/avfilter.h>
#include <libswscale/swscale.h>
#include <libavutil/frame.h>
#include <libavutil/dict.h>
#include <libavutil/imgutils.h>
}

class CrawlerRTSPv2 : public QObject
{
    Q_OBJECT

    QString name = "";

    QString rtspUri = "";

    AVCodec *pAVCodec;
    AVFormatContext *pAVFormatContext;
    AVCodecContext *pAVCodecContext;
    AVStream *pInputStream;

    int videoStreamIndex;

    bool flagOpen;
    QMutex rtspMutex;
public:
    CrawlerRTSPv2();

    void setSource(QString rtspUri_);
    void setName(QString name_);
    qreal getDAR(AVFrame *f);
Q_SIGNALS:
    void addFrameObject(bool isUseFrameObject, QtAV::VideoFrame videoFrame, long timestamp);

public Q_SLOTS:
    bool onOpenSource();
    void onCloseSource();
    void onPlay();
};

#endif // CRAWLERRTSPV2_H
