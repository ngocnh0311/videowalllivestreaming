﻿#include "crawlersplitter.h"

CrawlerSplitter::CrawlerSplitter(C_CrawlerCamera *_crawlerCamera){
    pCrawlerCamera = _crawlerCamera;
    name = "Crawler CrawlerSplitter";

    decodeNalUnits.append((int) NALUnit.SPS);
    decodeNalUnits.append((int) NALUnit.PPS);
    decodeNalUnits.append((int) NALUnit.IDR);
    decodeNalUnits.append((int) NALUnit.PFRAME);

    linkCount = 0;

    this->setObjectName("Decoder");

    decoderOpen();
}

CrawlerSplitter::~CrawlerSplitter() {
    decoderClose();
}

void CrawlerSplitter::decoderOpen() {
    avCodecContext = Q_NULLPTR;

    avCodec = avcodec_find_decoder(AV_CODEC_ID_H264);
    if (!avCodec) {
        // TODO
    }
    avCodecContext = avcodec_alloc_context3(avCodec);
    if (!avCodecContext) {
        // TODO
    }else{
        if (avcodec_open2(avCodecContext, avCodec, NULL) < 0) {
            // TODO
        }
        avCodecContext->thread_type = FF_THREAD_SLICE | FF_THREAD_FRAME;//default;
        avCodecContext->thread_safe_callbacks = true; //?
        avCodecContext->thread_count = 8;
        avCodecContext->skip_frame = AVDISCARD_DEFAULT;
        avCodecContext->skip_loop_filter = AVDISCARD_DEFAULT;
        avCodecContext->skip_idct = AVDISCARD_DEFAULT;
    }
}

void CrawlerSplitter::decoderClose() {
    if(avCodecContext) {
        avcodec_free_context(&avCodecContext);
    }
}

void CrawlerSplitter::startWorking() {
    qDebug() << Q_FUNC_INFO;
    splitterMutex.lock();
    working = true;
    gotSPS = false;
    splitterMutex.unlock();
    if (!isRunning()) {
        QThread::start();
    }
}

void CrawlerSplitter::stopWorking() {
    splitterMutex.lock();
    working = false;
    gotSPS = false;
    splitterMutex.unlock();
}

bool CrawlerSplitter::isWorking() {
    bool w;
    splitterMutex.lock();
    w = working;
    splitterMutex.unlock();
    return w;
}

void CrawlerSplitter::run() {
    qDebug() << Q_FUNC_INFO << "STARTED";
    CrawlerRTPQueue *rtpQueue = pCrawlerCamera->getRtpQueue();
    while (isWorking()) {
        QByteArray rawData = dequeueWithTimeout(initRTPPacketWaitedTimeMsec);
        splitRTPPackage(linkCount++, rawData);
        QThread::msleep(1); // trong truong hop chay khong tai de cpu khong bi tang
    }
    qDebug() << Q_FUNC_INFO << "STOPPED";
}

QByteArray CrawlerSplitter::dequeueWithTimeout(long timeout) {
    CrawlerRTPQueue *rtpQueue = pCrawlerCamera->getRtpQueue();
    QByteArray rtpData;
    while (isWorking() && (timeout > 0) ) {
        rtpData = rtpQueue->dequeue();
        if (!rtpData.isEmpty()) {
            break;
        }

        if (rtpData.isEmpty()) {
            QThread::msleep(1);
            timeout -= 1;
            rtpPacketWaitedTimeMsec += 1;
        }

        if (rtpQueue->getSize() > 25) {
            break;
        }

        if (rtpPacketWaitedTimeMsec >= 15000) {
            rtpPacketWaitedTimeMsec = 0;
            Q_EMIT shouldReopenSouce();
        }
    }

    if (rtpData.isEmpty()) {
        //        qDebug() << Q_FUNC_INFO << "Skip rtpPacket" << linkCount << ". Waited time" << initRTPPacketWaitedTimeMsec - timeout;
        rtpQueue->next();
    }

    return rtpData;
}

void CrawlerSplitter::onResetTimerReopenSouce(){
    this->rtpPacketWaitedTimeMsec = 0;
}


void CrawlerSplitter::splitRTPPackage(long linkIndex, QByteArray rtpPacket) {
    //qDebug() << Q_FUNC_INFO << linkIndex << rtpPacket.size();

#ifdef SOCKET_TYPE_NBTP
    int HEADER_LENGTH = 10;
#else
    int HEADER_LENGTH = 12;
#endif
    if (!rtpPacket.isEmpty() && (rtpPacket.size() > HEADER_LENGTH)) {
        long passedBytes = 0;
        bool rtpIsOk = true;
        while (isWorking() && passedBytes < rtpPacket.size() && rtpIsOk) {
            bool ret;
#ifdef SOCKET_TYPE_NBTP
            long payloadSize = rtpPacket.size() - HEADER_LENGTH;
            long long timestring = rawData.mid(passedBytes + 0, 8).toHex().toLongLong(0, 16);
            double timestampDouble = reinterpret_cast<double&>(timestring);
            QString timestampToString = QString::number(timestampDouble, 'g', 19);
            long timestamp = timestampToString.toLong();

            int pktId = rtpPacket.mid(passedBytes + 8, 2).toHex().toInt(&ret,16);

            int lengthOfH264RawData = payloadSize;
#else
            int payloadSize = rtpPacket.mid(passedBytes + 0, 2).toHex().toInt(&ret,16);

            long long timestring =  rtpPacket.mid(passedBytes + 2, 8).toHex().toLongLong(0, 16);
            double timestampDb = reinterpret_cast<double&>(timestring);
            QString timestampStr = QString::number(timestampDb, 'g', 19);
            long timestamp = timestampStr.toLong();

            int pktId = rtpPacket.mid(passedBytes + 10, 2).toHex().toInt(&ret,16);

            int lengthOfH264RawData = payloadSize - 10;
#endif
            if (passedBytes + HEADER_LENGTH + lengthOfH264RawData > rtpPacket.size()) {
                qDebug() << "Invalid data size (passedBytes + HEADER_LENGTH + lengthOfH264RawData)/rawData.size"
                         << (passedBytes + HEADER_LENGTH + lengthOfH264RawData) << "/" << rtpPacket.size();

                rtpIsOk = false;
                break;
            }

            QByteArray h264Raw = rtpPacket.mid(passedBytes + HEADER_LENGTH, lengthOfH264RawData);
            passedBytes = passedBytes + HEADER_LENGTH + lengthOfH264RawData;

            // h264 processing
            int nalu_header_byte = h264Raw.mid(0, 1).toHex().toInt(&ret,16);
            int nalType = nalu_header_byte & 0x1F;
            if((nalu_header_byte & 0b10000000) == 1){
                qDebug() << "The H.264 specification declares avalue of 1 as a syntax violation";
            }

            if (nalType != 28 && FUPicture.size() > 0){
                onReceiveH264Packet(FUPictureSavedTimestamp, NALUStartCode + FUPicture);
                FUPicture.clear();
            }

            switch (nalType) {
            case NALUnit.SEQUENCE_PARAMETER_SET: // SPS Type = 7
            {
                gotSPS = true;

                updateImageMeta(linkIndex, pktId, nalType);
                onReceiveH264Packet(timestamp, NALUStartCode + h264Raw);
            } break;
            case NALUnit.PICTURE_PARAMETER_SET: // PPS Type = 8
            {
                updateImageMeta(linkIndex, pktId, nalType);
                onReceiveH264Packet(timestamp, NALUStartCode + h264Raw);
            } break;
            case NALUnit.ACCESS_UNIT_DELIMITER: // Access unit delimiter = 9
            {
                updateImageMeta(linkIndex, pktId, nalType);
                onReceiveH264Packet(timestamp, NALUStartCode + h264Raw);
            } break;
            case NALUnit.CODED_SLICE_OF_AN_IDR_PICTURE: // IDR Type = 5
            {
                updateImageMeta(linkIndex, pktId, nalType);
                onReceiveH264Packet(timestamp, NALUStartCode + h264Raw);
            } break;
            case NALUnit.FRAGMENTATION_UNITS: { // Picture Type MULTIPLE PART (Fragmentation Units)
                int FU_A_byte = h264Raw.mid(1, 1).toHex().toInt(&ret,16);
                if (FU_A_byte & 0x80) { //0b10000000
                    if(FUPicture.size() > 0) {
                        onReceiveH264Packet(FUPictureSavedTimestamp, NALUStartCode + FUPicture);
                        FUPicture.clear();
                    }

                    FUPictureStarted = true;
                    //0xe0: 11100000 (3 bit dau)
                    //0x1f :    11111 (3 bit cuoi)
                    QByteArray fuNalType;
                    fuNalType.append((nalu_header_byte & 0xe0) | (FU_A_byte & 0x1f));
                    FUPicture = fuNalType + h264Raw.mid(2, lengthOfH264RawData - 2);

                    updateImageMeta(linkIndex, pktId, (nalType << 8) | (FU_A_byte));
                } else {
                    if (FU_A_byte & 0x40) { //0b01000000
                        // end of Fragment Unit
                        if (FUPictureStarted == false) {
                            rtpIsOk = false;
                            break;
                        }

                        updateImageMeta(linkIndex, pktId, (nalType << 8) | (FU_A_byte));

                        FUPicture = FUPicture + h264Raw.mid(2, lengthOfH264RawData - 2);
                        onReceiveH264Packet(timestamp, NALUStartCode + FUPicture);
                        FUPicture.clear();
                    } else {
                        if (FUPictureStarted == false) {
                            rtpIsOk = false;
                            break;
                        }

                        FUPicture = FUPicture + h264Raw.mid(2, lengthOfH264RawData - 2);

                        updateImageMeta(linkIndex, pktId, (nalType << 8) | (FU_A_byte));
                    }

                    if(FU_A_byte & 0x10){//0b00100000
                        qDebug() << "The Reserved bit MUST be equal to 0 and MUST be ignored by the receiver";
                    }
                }

                FUPictureSavedTimestamp = timestamp;
            } break;
            case NALUnit.CODED_SLICE_OF_A_NON_IDR_PICTURE: { // P-Frame  Picture Type
                updateImageMeta(linkIndex, pktId, nalType);
                onReceiveH264Packet(timestamp, NALUStartCode + h264Raw);
            } break;
            case 0: { // End Of 10 minute type
                qDebug() << "End of 10 minutes";
            } break;
            default: {
                qDebug() << "non-catched NalType" << NALUnit.toString(nalType);
            }break;
            } // end switch(naltype)
        }

        rtpPacketWaitedTimeMsec = 0;
    }
}

void CrawlerSplitter::onReceiveH264Packet(long timestamp, QByteArray h264Packet) {
    timestampBefore = QDateTime::currentMSecsSinceEpoch();
    bool ret;
    int naltype = h264Packet.mid(NALUStartCode.size(), 1).toHex().toInt(&ret,16) & 0x1F;

    if (!gotSPS || !decodeNalUnits.contains(naltype)) return;
    //    QImage image = decodeH264Packet(timestamp, h264Packet);
    //    if (!image.isNull()) {
    //        QString imageMeta = NALUnit.toString(naltype) +  "-timestamp_" + QString::number(timestamp) + buildImageMetaStr();
    //        clearImageMeta();
    //        Q_EMIT addFrameObject(true, image, timestamp);
    //        updateFrameDuration(timestamp);
    //    }

    VideoFrame videoFrame = decodeRTPPackageToFrame(h264Packet);
    if(videoFrame.isValid()){
        Q_EMIT addFrameObject(true, videoFrame, timestamp);
    }
}


qreal CrawlerSplitter::getDAR(AVFrame *f)
{
    // lavf 54.5.100 av_guess_sample_aspect_ratio: stream.sar > frame.sar
    qreal dar = 0;
    if (f->height > 0)
        dar = (qreal)f->width/(qreal)f->height;
    // prefer sar from AVFrame if sar != 1/1
    if (f->sample_aspect_ratio.num > 1)
        dar *= av_q2d(f->sample_aspect_ratio);
    else if (avCodecContext && avCodecContext->sample_aspect_ratio.num > 1) // skip 1/1
        dar *= av_q2d(avCodecContext->sample_aspect_ratio);
    return dar;
}

VideoFrame CrawlerSplitter::decodeRTPPackageToFrame(QByteArray rtpPackage){
    int rtpSize = rtpPackage.size();
    if (rtpSize > 0) {
        quint8 *data = new quint8[rtpSize];
        memcpy(data, rtpPackage.constData(), rtpSize);
        AVPacket avPacket;
        av_init_packet(&avPacket);

        avPacket.size = rtpSize;
        avPacket.data = data;
        AVFrame *avFrame = av_frame_alloc();
        int got_frame_ptr = 0;
        int ret = 0;
        ret = avcodec_decode_video2(avCodecContext, avFrame, &got_frame_ptr, &avPacket);
        if ( !avCodecContext || avCodecContext->width <= 0 || avCodecContext->height <= 0 ||ret <= 0 || got_frame_ptr <= 0) {
            return VideoFrame();
        }
        // it's safe if width, height, pixfmt will not change, only data change
        VideoFrame videoFrame(avCodecContext->width, avCodecContext->height, VideoFormat((int)avCodecContext->pix_fmt));
        videoFrame.setDisplayAspectRatio(getDAR(avFrame));
        videoFrame.setBits(avFrame->data);
        videoFrame.setBytesPerLine(avFrame->linesize);
        av_frame_free(&avFrame);
        delete[] data;
        return videoFrame.clone();
    }else{
        return VideoFrame();
    }
}

QImage CrawlerSplitter::decodeH264Packet(long timestamp, QByteArray h264Packet) {
    bool check;
    int naltype = h264Packet.mid(NALUStartCode.size(), 1).toHex().toInt(&check,16) & 0x1F;
    QImage image;
    int payloadLength = h264Packet.size();

    if (payloadLength > 0) {
        timestampBefore = QDateTime::currentDateTime().currentMSecsSinceEpoch();
        int ret;
        uint8_t *data = new uint8_t[payloadLength];
        memcpy(data, h264Packet.constData(), payloadLength);

        AVPacket avPacket;
        av_init_packet(&avPacket);
        avPacket.size = payloadLength;
        avPacket.data = data;

        AVFrame *avFrame = av_frame_alloc();

#define USE_DEPRECATED_API
#ifdef USE_DEPRECATED_API
        int got_picture = 0;
        ret = avcodec_decode_video2(avCodecContext, avFrame, &got_picture, &avPacket);
        if (ret > 0 && got_picture > 0) {
            // convert frame pix-format to RGB24
            uint8_t *dst_data[4];
            int dst_linesize[4];

            struct SwsContext *swsContext = sws_getContext(avCodecContext->width, avCodecContext->height,
                                                           avCodecContext->pix_fmt,
                                                           avCodecContext->width, avCodecContext->height,
                                                           AV_PIX_FMT_RGB24,
                                                           SWS_BICUBIC, NULL, NULL, NULL);
            int bufferSize = av_image_get_buffer_size(AV_PIX_FMT_RGB24, avFrame->width, avFrame->height, 1);
            uint8_t *srcBuffer = (uint8_t *)av_malloc(bufferSize);
            av_image_fill_arrays(dst_data, dst_linesize,
                                 srcBuffer,
                                 AV_PIX_FMT_RGB24, avFrame->width, avFrame->height, 1);
            sws_scale(swsContext,
                      (const uint8_t * const *) avFrame->data, (const int *)avFrame->linesize,
                      0,
                      avFrame->height,
                      dst_data, dst_linesize);

            image = QImage(dst_data[0],
                    avFrame->width,
                    avFrame->height,
                    dst_linesize[0],
                    QImage::Format_RGB888).copy();

            av_free(srcBuffer);
            sws_freeContext(swsContext);
        }
#else
        char error[128];

        ret = avcodec_send_packet(avCodecContext, &avPacket);
        if (ret < 0) {
            av_strerror(ret, error, sizeof(error));
            //qDebug() << Q_FUNC_INFO << "Error avcodec_send_packet: " << QString(error) << (ret == AVERROR(EAGAIN) ? "EAGAIN" : "Not EAGAIN");
        }

        while (ret >= 0) {
            ret = avcodec_receive_frame(avCodecContext, avFrame);
            if (ret == AVERROR(EAGAIN)) {
                //qDebug() << Q_FUNC_INFO << "EAGAIN";
                break;
            }
            else if (ret < 0) {
                av_strerror(ret, error, sizeof(error));
                qDebug() << Q_FUNC_INFO << "Error during decoding: " << QString(error);
                break;
            }

            //qDebug() << Q_FUNC_INFO << "got picture" << avFrame->pict_type;

            // convert frame pix-format to RGB24
            uint8_t *dst_data[4];
            int dst_linesize[4];
            struct SwsContext *swsContext = sws_getContext(avCodecContext->width, avCodecContext->height,
                                                           avCodecContext->pix_fmt,
                                                           avCodecContext->width, avCodecContext->height,
                                                           AV_PIX_FMT_RGB24,
                                                           SWS_BICUBIC, NULL, NULL, NULL);

            int bufferSize = av_image_get_buffer_size(AV_PIX_FMT_RGB24, avFrame->width, avFrame->height, 1);
            uint8_t *srcBuffer = (uint8_t *)av_malloc(bufferSize);
            av_image_fill_arrays(dst_data, dst_linesize,
                                 srcBuffer,
                                 AV_PIX_FMT_RGB24, avFrame->width, avFrame->height, 1);
            sws_scale(swsContext,
                      (const uint8_t * const *) avFrame->data, (const int *)avFrame->linesize,
                      0,
                      avFrame->height,
                      dst_data, dst_linesize);


            // create QImage and enqueue
            image = QImage(dst_data[0],
                    avFrame->width,
                    avFrame->height,
                    dst_linesize[0],
                    QImage::Format_RGB888).copy();

            av_free(srcBuffer);
            sws_freeContext(swsContext);
        }
#endif

        av_frame_free(&avFrame);
        delete[] data;
    }
    long timestampCurrent = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    qDebug() << "DIFF TIME WAIT DECODE" << timestampCurrent - timestampBefore<< "SIZE PACKAGE"  << h264Packet.size() << "NAL" << naltype;

    return image;
}

void CrawlerSplitter::updateImageMeta(long linkIndex, int pktId, int nalType) {
    linkIndexes.append(linkIndex);
    pktIds.append(pktId);
    nalTypes.append(nalType);
}

void CrawlerSplitter::clearImageMeta() {
    linkIndexes.clear();
    pktIds.clear();
    nalTypes.clear();
}

QString CrawlerSplitter::buildImageMetaStr() {
    QString linkIndexesStr = "";
    QString pktIdsStr = "";
    QString nalTypesStr = "";
    for (int i = 0; i < linkIndexes.size(); i++) {
        linkIndexesStr += "_" + QString::number(linkIndexes.at(i));
        pktIdsStr += "_" +  QString::number(pktIds.at(i));
        nalTypesStr += "_" + QString::number(nalTypes.at(i));
    }

    linkIndexesStr = "-linkIndexes_" + linkIndexesStr.remove(0,1);
    pktIdsStr = "-pktIds_" + pktIdsStr.remove(0,1);
    nalTypesStr = "-nalTypes_" + nalTypesStr.remove(0,1);

    return linkIndexesStr + pktIdsStr + nalTypesStr;
}

void CrawlerSplitter::updateFrameDuration(long timestamp) {
    if (lastTimestamp != -1) {
        frameDurations.append(timestamp - lastTimestamp);
        if (frameDurations.size() > 10) {
            frameDurations.removeFirst();
        }
        double total = std::accumulate(frameDurations.begin(), frameDurations.end(), 0);
        double frameDuration = total / frameDurations.size();
    }
    lastTimestamp = timestamp;
}
