#include "crawlerdownloader.h"

CrawlerDownloader::CrawlerDownloader(QNetworkAccessManager* _naManager, C_CrawlerCamera *_crawlerCamera)
{
    this->naManager = _naManager;
    this->cCrawlerCamera = _crawlerCamera;
    connect(naManager, &QNetworkAccessManager::finished, this, &CrawlerDownloader::onDownloadFinished);
    readyWorking = true;
}
void CrawlerDownloader::setNetworkManager(QNetworkAccessManager *_naManager){
    this->naManager = _naManager;
    connect(naManager, &QNetworkAccessManager::finished, this, &CrawlerDownloader::onDownloadFinished);
    readyWorking = true;
}

void CrawlerDownloader::setReadyWorking(bool value){
    readyWorking = value;
}

CrawlerDownloader::~CrawlerDownloader()
{
}

void CrawlerDownloader::onReceiveDowloadRTCData(quint64 linkIndex, QString url)
{
    //  if (stopLinkDownloadForSavingBW) {
    //      onDropLinkDownloadForSavingBW(linkIndex);
    //      return;
    //  }
    if(naManager ==  Q_NULLPTR || !readyWorking) return;
    QNetworkRequest req(url);
    QNetworkReply *reply = naManager->get(req);
    replyToIndex[reply] = linkIndex;
    lastLinkIndex = linkIndex;
}

void CrawlerDownloader::download_progress(qint64, qint64) {
    //    quint64 linkIndex = replyToIndex.take(reply);
    //    if (reply->bytesAvailable() > 0) {
    //        bool ret;
    //       // read header 12 bytes
    //       QByteArray headerBytes = reply->read(12);
    //       int payloadSize = headerBytes.mid(0, 2).toHex().toInt(&ret,16);
    //       double timestamp =  headerBytes.mid(2, 8).toDouble(&ret);
    //       int index = headerBytes.mid(10, 2).toHex().toInt(&ret,16);

    //       // read 1 rtp package
    //       int lengthOfH264RawData = payloadSize - 10;
    //       QByteArray h264Raw = reply->read(lengthOfH264RawData);
    //       onReceiveDownloadedData(index, headerBytes + h264Raw);
    //    }
}

void CrawlerDownloader::onDownloadFinished(QNetworkReply *reply) {
    quint64 linkIndex = replyToIndex.take(reply);

    if (linkIndex > lastLinkIndex) {
        // Bo goi download ve cham cua kenh truoc
        // Do linkIndex da bi reset ve 0 nen cac goi cua kenh truoc se co index greater
        qDebug() << "Nhieu.....................................................";
        return;
    }

    if (reply->bytesAvailable() > 0) {
        onReceiveDownloadedData(linkIndex, reply->readAll());
    } else {
        onReceiveDownloadDataFailed(linkIndex, reply->errorString());
    }
    reply->deleteLater();
}

void CrawlerDownloader::resetInternalState() {
    replyToIndex.clear();
}

void CrawlerDownloader::onReceiveDownloadedData(quint64 linkIndex, QByteArray payload) {
    CrawlerRTPQueue* rtpQueue = this->cCrawlerCamera->getRtpQueue();
    rtpQueue->enqueue(linkIndex, payload);
    // qDebug() << linkIndex << "=====" << payload.size();
}

bool CrawlerDownloader::getStopLinkDownloadForSavingBW()
{
    bool d;
    CrawlerDownloaderMutex.lock();
    d = stopLinkDownloadForSavingBW;
    CrawlerDownloaderMutex.unlock();
    return d;
}

void CrawlerDownloader::setStopLinkDownloadForSavingBW(bool value)
{
    CrawlerDownloaderMutex.lock();
    stopLinkDownloadForSavingBW = value;
    CrawlerDownloaderMutex.unlock();
}

void CrawlerDownloader::onReceiveDownloadDataFailed(quint64 linkIndex, QString errorString) {
    QByteArray downloadFaildMessage;
    downloadFaildMessage.append((char)0);
    CrawlerRTPQueue* rtpQueue =this->cCrawlerCamera->getRtpQueue();
    rtpQueue->enqueue(linkIndex, downloadFaildMessage);

    //    QString playerName = "Player " + this->pRTCPlayer->getPlayerName();
    //    qDebug() << playerName << " Thread: " << this->thread()
    //             << "CrawlerDownloader onReceiveDownloadDataFailed - linkIndex: " << linkIndex
    //             << " error: "  << errorString
    //             << " networkAccessible: " << naManager->networkAccessible();
}

void CrawlerDownloader::onDropLinkDownloadForSavingBW(quint64 linkIndex) {
    QByteArray dropLinkDownloadForSavingBWMessage;
    dropLinkDownloadForSavingBWMessage.append((char)0);
    CrawlerRTPQueue* rtpQueue =this->cCrawlerCamera->getRtpQueue();
    rtpQueue->enqueue(linkIndex, dropLinkDownloadForSavingBWMessage);
}
