﻿#ifndef CrawlerSplitter_H
#define CrawlerSplitter_H

#include <QBuffer>
#include <QByteArray>
#include <QDebug>
#include <QFile>
#include <QImage>
#include <QMutex>
#include <QObject>
#include <QPixmap>
#include <QQueue>
#include <QStack>
#include <QTimer>
#include <QThread>
#include "crawlerh264queue.h"
#include "crawlerimagequeue.h"
#include <algorithm>
#include <iostream>
#include <consts.h>
#include "../c_crawlercamera.h"
#include "Player/RTCPlayer/RTCRender.h"
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
#include <libavformat/avformat.h>
}
class CrawlerSplitter : public QThread {
    Q_OBJECT

private:
    long timestampBefore;
    QString name;
    C_CrawlerCamera *pCrawlerCamera;
    QMutex splitterMutex;
    bool working = false;

    AVCodec *avCodec;
    AVCodecContext *avCodecContext;
    struct SwsContext *swsContext;

    NALUnitType NALUnit;
    QByteArray NALUStartCode = QByteArray("\x00\x00\x01", 3);
    QList<int> decodeNalUnits;

    bool gotSPS = false;

    QList<long> frameDurations;
    long lastTimestamp = -1;

    long linkCount = 0;
    long initRTPPacketWaitedTimeMsec = 4000;
    long rtpPacketWaitedTimeMsec = 0;

    bool FUPictureStarted = false;
    long FUPictureSavedTimestamp = 0;
    QByteArray FUPicture;

    QList<long> linkIndexes;
    QList<int> pktIds;
    QList<int> nalTypes;

    void decoderClose();
    void decoderOpen();

    QByteArray dequeueWithTimeout(long timeout);

    void updateImageMeta(long linkIndex, int pktId, int nalType);
    void clearImageMeta();
    QString buildImageMetaStr();

    void updateFrameDuration(long timestamp);
    void splitRTPPackage(long linkIndex, QByteArray payload);

    void onReceiveH264Packet(long timestamp, QByteArray payload);
    QImage decodeH264Packet(long timestamp, QByteArray payload);
    qreal getDAR(AVFrame *f);
    VideoFrame decodeRTPPackageToFrame(QByteArray rtpPackage);
public:
    explicit CrawlerSplitter(C_CrawlerCamera *_crawlerCamera);
    ~CrawlerSplitter();

    void run();
     void startWorking();
     void stopWorking();
     bool isWorking();

     QString byteArrayToString(QByteArray array, int length) {
         QString str = "";
         if (length > array.size()) {
             length = array.size();
         }
         for (int index = 0; index < length; ++index) {
             quint8 item = static_cast<quint8>(array.at(index));
             str += QString("%1").arg(item, 4);
         }
         return str;
     }

     QString byteArrayToStringReverse(QByteArray array, int length) {
         QString str = "";
         if (length > array.size()) {
             length = array.size();
         }
         for (int index = length - 1; index >= 0; --index) {
             quint8 item = static_cast<quint8>(array.at(array.size() - index - 1));
             str += QString("%1").arg(item, 4);
         }
         return str;
     }
public Q_SLOTS:
    void onResetTimerReopenSouce();
Q_SIGNALS:
    void splittedRTPPackage(int packageIndex, QByteArray rtpPakage);
    void socketLostConnection();
    void needToReconnect();
    void showLoadingSign();
    void shouldReopenSouce();
    void addFrameObject(bool isUseFrameObject, QtAV::VideoFrame videoFrame, long timestamp);
};

#endif  // CrawlerSplitter_H
