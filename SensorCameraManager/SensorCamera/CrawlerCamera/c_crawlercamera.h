#ifndef C_CrawlerCamera_H
#define C_CrawlerCamera_H

#include <QDebug>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QTimer>
#include <QWidget>
#include "PacModel/control.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/a_crawlercamera.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/p_crawlercamera.h"
#include "message.h"
#include "Authentication/appcontext.h"
#include "SensorCameraManager/SensorCamera/c_sensorcamera.h"
#include "Player/ObjectsManager/objectsmanager.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlersplitter.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlerdownloader.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlersocket.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlerimagequeue.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlerh264queue.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlerrtpqueue.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlerrtsp.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlerrtspv2.h"
#include "Common/generaldefine.h"

class P_CrawlerCamera;
class C_SensorCamera;
class A_CrawlerCamera;
class DataGetCrawler;
class CrawlerSplitter;
class CrawlerSocket;
class CrawlerDownloader;
class ObjectsManager;
class C_Cam9RTCPlayer;
Q_DECLARE_METATYPE(QSet<QString>)
class C_CrawlerCamera : public Control {
    Q_OBJECT

    QThread *crawlerRTSPThread;
    CrawlerRTSPv2 *crawlerRTSP = Q_NULLPTR;
    QMutex keysubscribersLock;
    QSet<QString> keysubscribers;
    ObjectsManager *objectsManager = Q_NULLPTR;

    bool isWorking = false;
    QString crawlerName = "";
    int refCount = 0;
    CrawlerType crawlerType;
    QString appName = "";
    CamItem *camItemWorkingCurrent = Q_NULLPTR;
    CamItemType networkType;
    QString sourceUrl = "";
    QString playerName;
    LayoutStruct selectedLayoutCurrent;

    CrawlerImageQueue *imageQueue = Q_NULLPTR;
    CrawlerRTPQueue *rtpQueue = Q_NULLPTR;

    CrawlerDownloader *pDownloader = Q_NULLPTR;
    CrawlerSplitter *pSplitter = Q_NULLPTR;
    QNetworkAccessManager *naManager = Q_NULLPTR;
    CrawlerSocket *pSocket = Q_NULLPTR;

    QWidget* zone;
    AppContext* appContext;


public:
    C_SensorCamera * getParent() { return (C_SensorCamera*)this->parent; }
    A_CrawlerCamera* abstraction() { return (A_CrawlerCamera*)this->abst; }
    P_CrawlerCamera* presentation() { return (P_CrawlerCamera*)this->pres; }
    C_CrawlerCamera(CamItem *_camItem, Control* ctrl , CrawlerType crawlerType , QWidget* zone = Q_NULLPTR);

    void show(QVariant* attachment);
    /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
    void newUserAction(int message, QVariant* attachment) override;
    /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newSystemAction(int message, QVariant* attachment) override;

    /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newAction(int message, QVariant* attachment) override;
    AppContext *getAppContext() const;
    void setAppContext(AppContext *value);

    //get queue.
    ObjectsManager *getObjectsManager() const{
        return objectsManager;
    }
    CrawlerImageQueue* getImageQueue() { return imageQueue; }
    CrawlerRTPQueue* getRtpQueue() { return rtpQueue; }

    CrawlerDownloader *getDownloader() const;

    CamItem *getCamItemWorkingCurrent() const;


    QString getCrawlerName() const;
    void setCrawlerName(const QString &value);

    int getRefCount() const;
    void setRefCount(int value);

    QString getAppName() const;
    void setAppName(const QString &value);
    void removeSubscriber(QString keyUnSubscriber);
    void addSubscriber(QString keySubscriber);

    bool hasSubscriber();
    QString findSourceWithCrawlerType(CrawlerType crawlerType);
public Q_SLOTS:
    void onStartWorking();
    void onStopWorking();
    void onInitWorker();
    void onAddFrameObject(bool isUseFrameObject, VideoFrame videoFrame, long timestamp);
Q_SIGNALS:
    void showLoadingSign();
    void showPlayerLayer();
    void showNoCameraLayer();
    void removeRenderingDone(QString renderName, FrameObject *frameObject);
    void addFrameObject(bool isUseFrameObject, VideoFrame videoFrame , long timestamp, const QSet<QString> keysubscribers);
    void finished(CrawlerType crawlerType);
    void deleteNetworkManager();
};

#endif  // C_CrawlerCamera_H
