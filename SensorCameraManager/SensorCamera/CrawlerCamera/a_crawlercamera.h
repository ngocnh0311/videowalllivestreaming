#ifndef A_CrawlerCamera_H
#define A_CrawlerCamera_H

#include <QObject>
#include "PacModel/control.h"
#include "Camera/camitem.h"
#include "Site/site.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/a_crawlercamera.h"
class C_CrawlerCamera;
class A_CrawlerCamera : public Abstraction {
    Q_OBJECT
private:
    Site *workingSite = Q_NULLPTR;
    QList<CamItem *> listCamSite;
public:
    A_CrawlerCamera(Control *ctrl);
    C_CrawlerCamera *control() { return (C_CrawlerCamera *)this->ctrl; }
    void changeControl(Control *ctrl);


};
#endif  // A_CrawlerCamera_H
