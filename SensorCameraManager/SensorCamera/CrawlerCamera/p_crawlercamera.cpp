#include "p_crawlercamera.h"

P_CrawlerCamera::P_CrawlerCamera(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    this->zone->setStyleSheet("background-color: #333");
    layout = new QHBoxLayout();
    layout->setMargin(0);
    layout->setSpacing(5);
    this->zone->setLayout(layout);


}
void P_CrawlerCamera::show(QVariant *attachment) { Q_UNUSED(attachment) }

void P_CrawlerCamera::update() {}

QObject *P_CrawlerCamera::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}
