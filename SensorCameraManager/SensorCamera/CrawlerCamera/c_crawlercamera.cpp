#include "c_crawlercamera.h"
#include "Common/datagetcrawler.h"
/**
* Contructor. Register the father in the pac hierarchy.
* @param ctrl The reference on the father. Generally the pac agent which
* create this agent.
**/
C_CrawlerCamera::C_CrawlerCamera(CamItem *_camItem, Control* ctrl, CrawlerType _crawlerType,  QWidget* _zone) : Control(ctrl){

    this->camItemWorkingCurrent = _camItem;
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    appContext = dataStruct->value<AppContext*>();
    abst = new A_CrawlerCamera(this);
    zone = _zone;

    crawlerType = _crawlerType;
    crawlerType.cameraId = _crawlerType.cameraId;
    crawlerType.channelName = _crawlerType.channelName;
    crawlerType.protocol = _crawlerType.protocol;
    crawlerType.network = _crawlerType.network;
    crawlerType.dataSource = _crawlerType.dataSource;
    crawlerName = QString::number(crawlerType.cameraId) + crawlerType.channelName + crawlerType.network + crawlerType.protocol + crawlerType.dataSource;

    sourceUrl = findSourceWithCrawlerType(this->crawlerType);
    qRegisterMetaType<QSet<QString>>();
    objectsManager = new ObjectsManager();
    objectsManager->setName(crawlerName);
}

void C_CrawlerCamera::onInitWorker(){

    qDebug() <<Q_FUNC_INFO << "objectsManager---" << objectsManager;
    if(crawlerType.protocol == protocolCamera.WS){
        imageQueue = new CrawlerImageQueue();
        rtpQueue = new CrawlerRTPQueue();
        naManager = new QNetworkAccessManager();
        pDownloader = new CrawlerDownloader(naManager, this);
        pSplitter = new CrawlerSplitter(this);

        pSocket = new CrawlerSocket(this);
        pSocket->setSource(this->sourceUrl);
        connect(pSplitter, &CrawlerSplitter::addFrameObject, this, &C_CrawlerCamera::onAddFrameObject);
        connect(pSplitter, &CrawlerSplitter::shouldReopenSouce, pSocket, &CrawlerSocket::onReOpenSocket);
        connect(pSocket , &CrawlerSocket::resetTimerReopenSouce , pSplitter , &CrawlerSplitter::onResetTimerReopenSouce);
    } else if(crawlerType.protocol == protocolCamera.RTSP) {
        crawlerRTSPThread = new QThread;
        crawlerRTSPThread->setObjectName("CrawlerRTSP");

        crawlerRTSP = new CrawlerRTSPv2();
        crawlerRTSP->setName(camItemWorkingCurrent->getPostion());
        crawlerRTSP->setSource(sourceUrl);
        crawlerRTSP->moveToThread(crawlerRTSPThread);
        connect(crawlerRTSP, &CrawlerRTSPv2::addFrameObject, this, &C_CrawlerCamera::onAddFrameObject);
    }
}

QString C_CrawlerCamera::findSourceWithCrawlerType(CrawlerType crawlerType){
    QString sourceReturn = "";
    if (camItemWorkingCurrent != Q_NULLPTR) {
        networkType = camItemWorkingCurrent->getNetworkType();
        networkType.name = crawlerType.channelName;
        networkType.network = crawlerType.network;
        networkType.protocol = crawlerType.protocol;
        networkType.dataSource = crawlerType.dataSource;
        CamStream *camStream = camItemWorkingCurrent->getCamStream(networkType);
        if (camStream != Q_NULLPTR){
            sourceReturn = camStream->getSource();
        }
        return sourceReturn;
    }
}

/**
* Method to receive a message from the Presentation Facet.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_CrawlerCamera::newUserAction(int message, QVariant* attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    default:
        qDebug() << "ERROR : General User action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from the Presentation Facet.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_CrawlerCamera::newSystemAction(int message, QVariant* attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case 1:
        break;

    default:
        qDebug() << "ERROR : General System action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
* Method to receive a message from an other agent.
* @param message    : A string which describe the request
* @param attachment : A ref on an eventual object necessary to treat the request
**/
void C_CrawlerCamera::newAction(int message, QVariant* attachment) {
    switch (message) {

    case Message.START_CRAWLER_WORKING:{
        QTimer::singleShot(0, this, &C_CrawlerCamera::onStartWorking);
    }break;

    case Message.STOP_CRALER_WORKING:{
        QTimer::singleShot(0, this, &C_CrawlerCamera::onStopWorking);
    }break;

    case Message.ADD_SUBCRIBER_CRAWLER:{
        DataGetCrawler *dataGetCrawler = attachment->value<DataGetCrawler*>();
        if(dataGetCrawler){
            addSubscriber(dataGetCrawler->keySubcriable);
        }
    }break;

    case Message.REMOVE_SUBCRIBER_CRAWLER:{
        DataStopCrawler *dataStopCrawler = attachment->value<DataStopCrawler *>();
        if(dataStopCrawler){
            removeSubscriber(dataStopCrawler->keyUnSubcriable);
            if(!hasSubscriber()){
                newAction(Message.STOP_CRALER_WORKING, Q_NULLPTR);
            }
        }
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

void C_CrawlerCamera::onStartWorking() {
    if(!isWorking){
        qDebug() << Q_FUNC_INFO <<"CrawlerStartWorking" << "CRAWLER NAME" << this->crawlerName << QThread::currentThread();
        isWorking = true;
        if(crawlerType.protocol == protocolCamera.WS){
            if(naManager == Q_NULLPTR){
                naManager = new QNetworkAccessManager();
                pDownloader->setNetworkManager(naManager);
            }else{
                pDownloader->setReadyWorking(true);
            }
            pSocket->openSource();

            rtpQueue->setWorking(true);
            rtpQueue->empty();

            imageQueue->setWorking(true);
            imageQueue->empty();

            pSplitter->startWorking();

        }else if(crawlerType.protocol == protocolCamera.RTSP) {
            qDebug() << Q_FUNC_INFO << "CrawlerStartWorking1" << protocolCamera.RTSP;
            if(!crawlerRTSPThread->isRunning()){
                crawlerRTSPThread->start();
            }
            QTimer::singleShot(0, crawlerRTSP, &CrawlerRTSPv2::onPlay);
        }
    }
}

void C_CrawlerCamera::onStopWorking() {
    if(isWorking){
        qDebug() << Q_FUNC_INFO <<"CrawlerStopWorking" << "CRAWLER NAME" << this->crawlerName << QThread::currentThread();
        isWorking = false;
        sourceUrl = "";
        if(crawlerType.protocol == protocolCamera.WS){
            pDownloader->setReadyWorking(false);
            delete naManager;
            naManager = Q_NULLPTR;
            pSocket->closeSource();

            rtpQueue->setWorking(false);
            rtpQueue->empty();

            imageQueue->setWorking(false);
            imageQueue->empty();

            pSplitter->stopWorking();
            objectsManager->removeAllFrameObject();
        } else if(crawlerType.protocol == protocolCamera.RTSP) {
            if (crawlerRTSPThread->isRunning()) {
                crawlerRTSPThread->quit();
                crawlerRTSPThread->requestInterruption();
                crawlerRTSPThread->wait();
                crawlerRTSP->onCloseSource();
            }
            objectsManager->removeAllFrameObject();
        }
    }

    Q_EMIT finished(crawlerType);
}

void C_CrawlerCamera::addSubscriber(QString keysubscriber){
    qDebug() << Q_FUNC_INFO << QThread::currentThread() << "keysubscriber" << keysubscriber;
    objectsManager->addSemaphoreToAllFrameObject(keysubscriber);
    keysubscribersLock.lock();
    keysubscribers.insert(keysubscriber);
    keysubscribersLock.unlock();
    qDebug() << Q_FUNC_INFO << QThread::currentThread() << "keysubscribers" << keysubscribers;
}

void C_CrawlerCamera::removeSubscriber(QString keyUnSubscriber){
    qDebug() << Q_FUNC_INFO << QThread::currentThread() << "keyUnSubscriber" << keyUnSubscriber;
    objectsManager->removeSemaphoreOutAllFrameObject(keyUnSubscriber);
    keysubscribersLock.lock();
    keysubscribers.remove(keyUnSubscriber);
    keysubscribersLock.unlock();
    qDebug() << Q_FUNC_INFO << QThread::currentThread() << "keysubscribers" << keysubscribers;
}

bool C_CrawlerCamera::hasSubscriber(){
    keysubscribersLock.lock();
    bool ret = keysubscribers.isEmpty() ? false : true;
    keysubscribersLock.unlock();
    return ret;
}

void C_CrawlerCamera::onAddFrameObject(bool isUseFrameObject, VideoFrame videoFrame, long timestamp){
    objectsManager->onAddFrameObject(isUseFrameObject, videoFrame, timestamp, keysubscribers);
}
////
void C_CrawlerCamera::show(QVariant* attachment) {
    newUserAction(Message.SHOW, attachment);
}

AppContext *C_CrawlerCamera::getAppContext() const
{
    return appContext;
}

void C_CrawlerCamera::setAppContext(AppContext *value)
{
    appContext = value;
}

CrawlerDownloader *C_CrawlerCamera::getDownloader() const
{
    return pDownloader;
}

CamItem *C_CrawlerCamera::getCamItemWorkingCurrent() const
{
    return camItemWorkingCurrent;
}


QString C_CrawlerCamera::getCrawlerName() const
{
    return crawlerName;
}

void C_CrawlerCamera::setCrawlerName(const QString &value)
{
    crawlerName = value;
}

int C_CrawlerCamera::getRefCount() const
{
    return refCount;
}

void C_CrawlerCamera::setRefCount(int value)
{
    refCount = value;
}

QString C_CrawlerCamera::getAppName() const
{
    return appName;
}

void C_CrawlerCamera::setAppName(const QString &value)
{
    appName = value;
}
