#ifndef P_CrawlerCamera_H
#define P_CrawlerCamera_H

#include <PacModel/presentation.h>
#include <QFont>
#include <QGridLayout>
#include <QList>
#include <QObject>
#include <QPushButton>
#include <QWidget>
#include <cmath>
#include "c_crawlercamera.h"
#include "message.h"
#include "Authentication/appcontext.h"
#include <QSettings>
class C_CrawlerCamera;
class P_CrawlerCamera : public Presentation {
    // init ui control
private:
    QWidget *zone = Q_NULLPTR;
    QHBoxLayout *layout = Q_NULLPTR;

public:
    P_CrawlerCamera(Control *ctrl, QWidget *zone);
    C_CrawlerCamera *control() { return (C_CrawlerCamera *)this->ctrl; }
    void changeControl(Control *ctrl);
    void update();
    void show(QVariant *attactment);
    QObject *getZone(int zoneId);

public Q_SLOTS:
};

#endif  // P_CrawlerCamera_H
