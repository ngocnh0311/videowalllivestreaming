#include "c_sensorcameramanager.h"

/**
 * Contructor. Register the father in the pac hierarchy.
 * @param ctrl The reference on the father. Generally the pac agent which
 * create this agent.
 **/
C_SensorCameraManager::C_SensorCameraManager(Control *ctrl, QWidget *_zone)
    : Control(ctrl) {
    QVariant *dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    appContext = dataStruct->value<AppContext *>();

    abst = new A_SensorCameraManager(this);
    zone = _zone;
}

/**
 * Method to receive a message from the Presentation Facet.
 * @param message    : A string which describe the request
 * @param attachment : A ref on an eventual object necessary to treat the request
 **/
void C_SensorCameraManager::newUserAction(int message, QVariant *attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General User action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
 * Method to receive a message from the Astraction Facet.
 * @param message    : A string which describe the request
 * @param attachment : A ref on an eventual object necessary to treat the request
 **/
void C_SensorCameraManager::newSystemAction(int message, QVariant *attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case Message.APP_CONTEXT_GET:
        attachment->setValue(appContext);
        break;

    default:
        qDebug() << "ERROR : General System action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
 * Method to receive a message from an other agent.
 * @param message    : A string which describe the request
 * @param attachment : A ref on an eventual object necessary to treat the request
 **/
void C_SensorCameraManager::newAction(int message, QVariant *attachment) {
    switch (message) {
    case Message.UPDATE_LIST_CAMERA_OF_DEVICE_FOR_SENSOR_CAMERA_MANAGER: {
        QList<CamItem *> listCameraOfDevice = attachment->value<QList<CamItem *>>();
        creatListSensorCamera(listCameraOfDevice);
    } break;

    case Message.CREAT_LIST_CAMERA_SENSOR: {
        QList<CamItem *> listCamItem = attachment->value<QList<CamItem *>>();
        creatListSensorCamera(listCamItem);
    } break;

    case Message.VIDEO_WALL_RENDER_SUBSCRIBE_CRAWLER_CAMERA: {
        DataGetCrawler *dataGetCrawler = attachment->value<DataGetCrawler *>();;
        C_SensorCamera *cSensorCamera = listSensorCamrera.value(dataGetCrawler->crawlerType.cameraId);
        if(cSensorCamera) {
            cSensorCamera->newAction(message, attachment);
        }
    } break;

    case Message.VIDEO_WALL_RENDER_UNSUBSCRIBE_CRAWLER_CAMERA: {
        DataStopCrawler *dataStopCrawler = attachment->value<DataStopCrawler *>();
        if(dataStopCrawler != Q_NULLPTR) {
            C_SensorCamera *cSensorCamera = listSensorCamrera.value(dataStopCrawler->crawlerType.cameraId);
            if(cSensorCamera) {
                cSensorCamera->newAction(message, attachment);
            }
        }
    } break;

    case Message.PLAYER_PLAY_LIVE_HD:
    case Message.PLAYER_PLAY_LIVE_SD: {
        CamItem *camItem = attachment->value<CamItem *>();
        if(camItem) {
            int cameraId = camItem->getCameraId();
            qDebug() << "camItemWorkingCurrent SD/HD CLICK" << cameraId;

            C_SensorCamera *cSensorCamera = listSensorCamrera.value(cameraId);
            if(cSensorCamera) {
                cSensorCamera->newAction(message, attachment);
            }
        }
    } break;
    case Message.APP_CONTEXT_GET: {
        attachment->setValue(appContext);
    } break;
    case Message.APP_UPDATE_WORKING_SITE: {
        Site *workingSite = attachment->value<Site *>();
        abstraction()->setWorkingSite(workingSite);
        abstraction()->loadDataCamerasOfSite();
    } break;

    default:
        qDebug() << "ERROR : General Internal pac action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

void C_SensorCameraManager::creatListSensorCamera(QList<CamItem *> listCamItem) {
    listCamItemOfSite = listCamItem;
    for (int index = 0; index < listCamItem.size(); ++index) {
        CamItem *camItem = listCamItem[index];
        int cameraID = camItem->getCameraId();
        if(!listSensorCamrera.contains(cameraID)){
            C_SensorCamera *sensorCamera = new C_SensorCamera(this, Q_NULLPTR);
            sensorCamera->setCamItem(camItem);
            listSensorCamrera.insert(cameraID, sensorCamera);
        }
    }
}

void C_SensorCameraManager::show(QVariant *attachment) {
    newUserAction(Message.SHOW, attachment);
}

AppContext *C_SensorCameraManager::getAppContext() const {
    return appContext;
}

void C_SensorCameraManager::setAppContext(AppContext *value) {
    appContext = value;
}
