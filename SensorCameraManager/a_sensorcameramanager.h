#ifndef A_SensorCameraManager_H
#define A_SensorCameraManager_H

#include <QObject>
#include "PacModel/control.h"
#include "Camera/camitem.h"
#include "Site/site.h"
#include "SensorCameraManager/c_sensorcameramanager.h"
class C_SensorCameraManager;
class A_SensorCameraManager : public Abstraction {
    Q_OBJECT
private:
    Site *workingSite = Q_NULLPTR;
    QList<CamItem *> listCamSite;
public:
    A_SensorCameraManager(Control *ctrl);
    C_SensorCameraManager *control() { return (C_SensorCameraManager *)this->ctrl; }

    void changeControl(Control *ctrl);
    QList<CamItem *> getListCamItem() ;
    void setListCamItem(const QList<CamItem *> &value);
    void loadAllCamerasOfSite(
            std::function<void(void)> onSuccess,
            std::function<void(void)> onFailure);
    void loadDataCamerasOfSite();
    CamSite* siteCameras = Q_NULLPTR;
    void setWorkingSite(Site *workingSite);

};
#endif  // A_SensorCameraManager_H
