#ifndef P_SensorCameraManager_H
#define P_SensorCameraManager_H

#include <PacModel/presentation.h>
#include <QFont>
#include <QGridLayout>
#include <QList>
#include <QObject>
#include <QPushButton>
#include <QWidget>
#include <cmath>
#include "c_sensorcameramanager.h"
#include "message.h"
#include "Authentication/appcontext.h"
#include <QSettings>
class C_SensorCameraManager;
class P_SensorCameraManager : public Presentation {
    // init ui control
private:
    QWidget *zone = Q_NULLPTR;
    QHBoxLayout *layout = Q_NULLPTR;

public:
    P_SensorCameraManager(Control *ctrl, QWidget *zone);
    C_SensorCameraManager *control() { return (C_SensorCameraManager *)this->ctrl; }
    void changeControl(Control *ctrl);
    void update();
    void show(QVariant *attactment);
    QObject *getZone(int zoneId);

public Q_SLOTS:
};

#endif  // P_SensorCameraManager_H
