#include "a_sensorcameramanager.h"
/**
 * Constructor for Abstraction facet. Register the control.
 * @param Crtl A ref on the control facet which manage this facet
 **/
A_SensorCameraManager::A_SensorCameraManager(Control* ctrl) : Abstraction(ctrl) {
    workingSite = new Site();
}

/**
 * Change the control of this abstraction
 * @param ctrl the new control for this abstraction facet
 **/
void A_SensorCameraManager::changeControl(Control* ctrl) { this->ctrl = ctrl; }

/**
 * Method to create a new abstraction exactly equals to this abstraction.
 * @return the created clone
 **/
// public Abstraction getClone(){
//    try{
//        return (Abstraction)clone();
//    } catch(Exception e) {System.out.println("ERROR: can't duplicate an
//    abstraction.");}
//    return null;
//}

void A_SensorCameraManager::setWorkingSite(Site *workingSite){
    this->workingSite = workingSite;
}




//load list camera with api 003
void A_SensorCameraManager::loadAllCamerasOfSite(
        std::function<void(void)> onSuccess,
        std::function<void(void)> onFailure) {
    if(workingSite){
        int siteId = workingSite->getSiteId();
        QString token = control()->getAppContext()->getWorkingUser()->getToken();

        qDebug() << "load site camera" << "SITE ID" << siteId;

        CamSite::getCamerasOfSite(
                    siteId, token,
                    [this, onSuccess](QJsonObject jsonObject) {

            this->siteCameras = CamSite::parseWithOrder(false, jsonObject);
            qDebug() << "TOTAL loadAllCamerasOfSite" <<   siteCameras->getCamItems().size();
            onSuccess();
        },
        [this, onFailure](QString msg) { onFailure(); });
    }
}


//api 003
void A_SensorCameraManager::loadDataCamerasOfSite() {
    loadAllCamerasOfSite([this] {
        listCamSite = siteCameras->getCamItems();
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QList<CamItem *>>(listCamSite);
        control()->newAction(Message.CREAT_LIST_CAMERA_SENSOR , dataStruct);
    },
    [this] {
        qDebug() << "LOAD SITE CAMERAS ERROR";
    });
}


