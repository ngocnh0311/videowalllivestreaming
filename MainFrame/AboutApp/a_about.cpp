#include "a_about.h"

/**
 * Constructor for Abstraction facet. Register the control.
 * @param Crtl A ref on the control facet which manage this facet
 **/
A_About::A_About(Control* ctrl) : Abstraction(ctrl) {

}

