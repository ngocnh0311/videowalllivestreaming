#ifndef A_Socket_H
#define A_Socket_H

#include <QObject>
#include "PacModel/control.h"
#include <sio_client.h>
#include "Authentication/user.h"
#include "Camera/camitem.h"
#include "Common/appprofile.h"
#include "Common/networkutils.h"
#include "c_socket.h"
using namespace sio;
using namespace std;

class CamItem;
class AppProfile;
class C_Socket;

class A_Socket : public Abstraction {
    Q_OBJECT
private:


public:
    QString videoNameRecord;
    C_Socket *control() { return (C_Socket *)ctrl; }
    A_Socket(Control *ctrl);
    void changeControl(Control *ctrl);
    bool joinClipRoomSuccess = false;
    int siteIdJoinCurrent = -1;
    std::unique_ptr<client> _io;
    std::map<std::string, std::string> mOptions;
    bool joinDeviceSuccess = false;
    bool joinSocketSuccess = false;
    QTimer timerJoinDevice;
    QTimer timerJoinSocket;


    QString joinSiteData = "{ \"site_id\": \"%1\"}";
    QString leaveSiteData = "{ \"room_id\": \"%1\"}";

    QString mDeviceID = "{\"device_id\":\"%1\"}";
    QString mUserID = "{\"user_id\":\"%1\"}";

    QString joinRoomData = "{data: {\"user_id\": \"%1\", \"type\": 'client'}}";
    QString urlRecordCurrent = "";
    //    QString getClipData = "{data: {\"camera_id\": \"%1\",\"camera_name\":\"%2\", \"url\":\"%3\"}}";
    QString getClipData = "{data: {'camera_id': %1 ,'camera_name':'%2', 'url':'%3'}}";

    QString cancelRecord = "data: { url: \"%1\"}";

    QString dataUpdateCameraOrder = "{\"list_order\":\"%1\",\"layout_server_view\": \"%2\",\"play_page\":\"%3\",\"device_id\":\"%4\",\"play_cam\":\"%5\"}";

    int idUserSocket = -1;
    void addSocketListener();
    void connectToSocket();
    //    void startGetClip(std::string const& cameraId,std::string const& cameraName,std::string const& urlVideo);
    void startGetClip(int cameraId,QString videoName,QString urlVideo);

    void cancelGetClip();
    void startJoinSite(int siteId);
    void updateCameraOrder();
    QString createStringListCameraOrder(QList<CamItem *> listCameras);
    void startJoinClipRoom();
    void startRegisterDevice();
    void startRegisterUser(int userId);
    bool isJoinedDevice = false;
    bool getIsJoinedDevice() const;
    void setIsJoinedDevice(bool value);
    void getCamerasWithDeviceId(QString token, std::function<void(CamSite *)> onSuccess,
                                         std::function<void(QString)> onFailure);
    void loadAllCamerasOfSiteSaveToDisk(QString token, int siteId, std::function<void(QJsonObject jsonObject)> onSuccess, std::function<void(QString)> onFailure);
private :
    void onConnected(std::string const &nsp);
    void onClosed(client::close_reason const& reason);
    void onFailed();

    void onConnectError(std::string const& name, message::ptr const& data,
                        bool hasAck, message::list& ack_resp);
    void onConnectTimeout(std::string const& name, message::ptr const& data,
                          bool hasAck, message::list& ack_resp);
    void onError(std::string const& name, message::ptr const& data,
                 bool hasAck, message::list& ack_resp);

    void onUserJoined(std::string const& name, message::ptr const& data,
                      bool hasAck, message::list& ack_resp);
    void onUserChangePassword(std::string const& name, message::ptr const& data,
                              bool hasAck, message::list& ack_resp);

    void onConnectedSuccess(std::string const &name, message::ptr const &data,
                            bool hasAck, message::list &ack_resp);
    void onJoinClipRoom(std::string const &name, message::ptr const &data,
                        bool hasAck, message::list &ack_resp);
    void onUpdateControl(std::string const &name, message::ptr const &data,
                         bool hasAck, message::list &ack_resp);
    void onCamOrderChanger(const string &name, const message::ptr &data,
                           bool hasAck, message::list &ack_resp);
    void onNVRChangeIP(std::string const& name, message::ptr const& data,
                       bool hasAck, message::list& ack_resp);

    void onCamOderCMSChanged(std::string const& name, message::ptr const& data,
                             bool hasAck, message::list& ack_resp);
    //getclip
    void onGetClipSuccess(std::string const &name, message::ptr const &data,
                          bool hasAck, message::list &ack_resp);
    void onCancelGetClipSuccess(std::string const &name, message::ptr const &data,
                          bool hasAck, message::list &ack_resp);
    void getClipRecordSuccess(QString nameVideo, QString urlVideo);
public Q_SLOTS:

    void socketReadyWorking();
    void onJoined();
    void getClipRecordError();
    void refreshDataWhenNVRChangeIP();
    void refreshDataWhenCamOrderCMSChanged();
    void remoteControlChanged();
    void rejoinDevice();
    void userChangedPasswordNeedLogout();
    void cancelCreateVideoSuccess();
    void updatePercentGenerateLink(int percent);
    void rejoinSocket();
Q_SIGNALS:
    void onSocketConnected();
    void onSocketDisconnected();
    void onSocketJoined();
    void onNVRChangeIPNeedRefresh();
    void onGetClipSocketSuccess(QString nameVideo, QString urlVideo);
    void onGetClipSocketError();
    void onUpdateRemoteControl();
    void onCamOderCMSChangedNeedRefresh();
    void onUserChangedPassword();
    void onCancelCreateVideoSuccess();
    void onUpdatePercentGenerateLink(int percent);
};

#endif  // ABSTRACTION_H
