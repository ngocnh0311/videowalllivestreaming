#include "c_socket.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_Socket::C_Socket(Control* ctrl, QWidget* zone) : Control(ctrl) {
    // update app context
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();

    this->pres = new P_Socket(this, zone);
    this->abst = new A_Socket(this);
    this->zone = zone;
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_Socket::newUserAction(int message, QVariant* attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case 1:
        break;

    default:
        qDebug() << "ERROR : General User action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_Socket::newSystemAction(int message, QVariant* attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case Message.UPDATE_PERCENT_SOCKET_GERNERATE_LINK_MP4_RECORD:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.CANCEL_CREATE_VIDEO_RECORD_SUCCESS:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.USER_CHANGED_PASSWORD_NEED_LOGOUT:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.LOAD_DATA_CAMERAS_WITH_DEVICE_ID:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.APP_PLAY_BACK_GET_CLIP_RECORD_ERROR:{
        getParent()->newAction(message, attachment);
    }break;


    case Message.APP_PLAY_BACK_GET_CLIP_RECORD_SUCCESS:{
        getParent()->newAction(message, attachment);
    }
        break;
    case Message.APP_SOCKET_CONNECTED:{
        getParent()->newAction(message, attachment);
    }
        break;
    case Message.APP_PLAY_BACK_GET_ALL_CLIP_RECORD_SUCCESS:{
        getParent()->newAction(message, attachment);
    }
        break;

    default:
        qDebug() << "ERROR : General System action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_Socket::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.UPDATE_DATA_STAND_ALONE_MODE_AFTER_NETWORK_IS_UNREACHABLE:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.CANCEL_CREATE_VIDEO_RECORD:{
        this->abstraction()->cancelGetClip();
    }break;

    case Message.APP_NETWORK_IS_REACHABLE:{
        if(!connectedNetwork){
            this->abstraction()->socketReadyWorking();
            connectedNetwork = true;
        }
    }break;

    case Message.APP_NETWORK_IS_UNREACHABLE:{
        connectedNetwork = false;
    }break;


    case Message.START_JOIN_USER_ID:{
        int userId = attachment->value<int>();
        this->abstraction()->startRegisterUser(userId);
    }break;

    case Message.APP_VIDEO_WALL_UPDATE_CAMERA_ORDER_REMOTE_CONTROL:{
        this->abstraction()->updateCameraOrder();
    }break;

    case Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED:
    case Message.APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED_HANDEL: {
        this->abstraction()->updateCameraOrder();
    } break;

    case Message.NVR_CHANGE_IP_NEED_REFRESH:{
        this->getParent()->newAction(message, attachment);
    }break;


    case Message.CMS_CHANGE_CAM_ORDER_NEED_REFRESH:{
        this->getParent()->newAction(message, attachment);
    }break;

    case Message.START_JOIN_SITE :{
        int siteId = attachment->value<int>();
        this->abstraction()->startJoinSite(siteId);
    }break;

    case Message.APP_PLAY_BACK_CANCEL_RECORD:{
        this->abstraction()->cancelGetClip();
    }break;
    case Message.APP_SOCKET_TO_CONNECT:{
        this->abstraction()->connectToSocket();
    }
        break;

    case Message.SHOW_INDICATOR:{
        this->getParent()->newAction(message, Q_NULLPTR);
    }

    case Message.HIDE_INDICATOR:{
        this->getParent()->newAction(message, Q_NULLPTR);
    }
        break;

    case Message.APP_PLAY_BACK_GET_CLIP_RECORD:{
        DataGetClip dataGetClip = attachment->value<DataGetClip>();
        this->abstraction()->startGetClip(dataGetClip.cameraId, dataGetClip.nameVideo, dataGetClip.urlVideo);
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}
