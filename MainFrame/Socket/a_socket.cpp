#include "a_socket.h"

#ifdef WIN32
#define BIND_EVENT(IO, EV, FN)         \
    do {                                 \
    socket::event_listener_aux l = FN; \
    IO->on(EV, l);                     \
    } while (0)
#else
#define BIND_EVENT(IO, EV, FN) IO->on(EV, FN)
#define OFF_EVENT(IO, EV) IO->off(EV)
#endif

/**
 * Constructor for Abstraction facet. Register the control.
 * @param Crtl A ref on the control facet which manage this facet
 **/
A_Socket::A_Socket(Control* ctrl) : Abstraction(ctrl), _io(new client()) {
    addSocketListener();
}

/**
 * Change the control of this abstraction
 * @param ctrl the new control for this abstraction facet
 **/
void A_Socket::changeControl(Control* ctrl) { this->ctrl = ctrl; }

void A_Socket::addSocketListener() {
    using std::placeholders::_1;
    using std::placeholders::_2;
    using std::placeholders::_3;
    using std::placeholders::_4;
    socket::ptr sock = _io->socket();

    //record
//    BIND_EVENT(sock, "JoinClipRoom",
//               std::bind(&A_Socket::onJoinClipRoom, this, _1, _2, _3, _4));
//    BIND_EVENT(sock, "GetClip",
//               std::bind(&A_Socket::onGetClipSuccess, this, _1, _2, _3, _4));
//    BIND_EVENT(sock, "cancel_clip",
//               std::bind(&A_Socket::onCancelGetClipSuccess, this, _1, _2, _3, _4));

    BIND_EVENT(sock, "message_data",
               std::bind(&A_Socket::onUpdateControl, this, _1, _2, _3, _4));

    BIND_EVENT(sock, "CameraOrderChanger",
               std::bind(&A_Socket::onCamOrderChanger, this, _1, _2, _3, _4));

    BIND_EVENT(sock, "nvr_on_change",
               std::bind(&A_Socket::onNVRChangeIP, this, _1, _2, _3, _4));
    //reload data for box with device id
    BIND_EVENT(sock, "CamOderCMS",
               std::bind(&A_Socket::onCamOderCMSChanged, this, _1, _2, _3, _4));

    BIND_EVENT(sock, "join",
               std::bind(&A_Socket::onUserJoined, this, _1, _2, _3, _4));

    BIND_EVENT(sock, "logout",
               std::bind(&A_Socket::onUserChangePassword, this, _1, _2, _3, _4));

    _io->set_socket_open_listener(std::bind(&A_Socket::onConnected,this,std::placeholders::_1));
    //physical connection closed or drop.
    _io->set_close_listener(std::bind(&A_Socket::onClosed,this,_1));
    //physical connection fail to establish.
    _io->set_fail_listener(std::bind(&A_Socket::onFailed,this));

    connect(this, &A_Socket::onNVRChangeIPNeedRefresh,this, &A_Socket::refreshDataWhenNVRChangeIP);
    connect(this, &A_Socket::onGetClipSocketSuccess, this, &A_Socket::getClipRecordSuccess);
    connect(this, &A_Socket::onGetClipSocketError, this, &A_Socket::getClipRecordError);
    connect(this, &A_Socket::onUpdateRemoteControl , this, &A_Socket::remoteControlChanged);
    connect(this, &A_Socket::onCamOderCMSChangedNeedRefresh , this, &A_Socket::refreshDataWhenCamOrderCMSChanged);
    connect(this, &A_Socket::onSocketJoined, this, &A_Socket::onJoined);
    connect(this, &A_Socket::onSocketConnected, this, &A_Socket::socketReadyWorking);
    connect(this, &A_Socket::onUserChangedPassword , this, &A_Socket::userChangedPasswordNeedLogout);
    connect(this, &A_Socket::onCancelCreateVideoSuccess , this, &A_Socket::cancelCreateVideoSuccess);
    connect(this, &A_Socket::onUpdatePercentGenerateLink , this, &A_Socket::updatePercentGenerateLink);

    connect(&timerJoinDevice, &QTimer::timeout, this, &A_Socket::rejoinDevice);
    connect(&timerJoinSocket , &QTimer::timeout, this, &A_Socket::rejoinSocket);
    timerJoinSocket.start(15000);
}

void A_Socket::rejoinSocket(){
    if(!joinSocketSuccess && this->control()->appContext->getNetworkConnected()){
        connectToSocket();
    }
}

void A_Socket::rejoinDevice(){
    qDebug() << "rejoinDevice";
    if(!this->control()->appContext->getJoinDeviceSuccess() && this->control()->appContext->getNetworkConnected() ){
        startRegisterDevice();
    }
}
void A_Socket::onNVRChangeIP(std::string const& name, message::ptr const& data,
                             bool hasAck, message::list& ack_resp){
    qDebug() << "onNVRChaDengeIP";
    Q_EMIT onNVRChangeIPNeedRefresh();
}

//khi pick up and kick out cam
void A_Socket::onCamOderCMSChanged(std::string const& name, message::ptr const& data,
                                   bool hasAck, message::list& ack_resp){
    Q_EMIT onCamOderCMSChangedNeedRefresh();

}

void A_Socket::refreshDataWhenNVRChangeIP(){
    qDebug() << "refreshDataWhenNVRChangeIP";
    QSettings settings;
    QString nvrChangeIp = "CMS_NVR_CHANGE_IP";
    QVariant *dataStructCmsChangeIp = new QVariant();
    dataStructCmsChangeIp->setValue<QString>(nvrChangeIp);
    QString offlineMode = settings.value("offline_mode").toString();
    if(offlineMode == "ON"){
        User *workingUser = this->control()->appContext->getWorkingUser();
        if(this->control()->appContext->getIsLoadDataWithDeviceId()){
            if(workingUser != Q_NULLPTR){
                QString tokenUser = workingUser->getToken();
                this->getCamerasWithDeviceId(tokenUser , [this, dataStructCmsChangeIp](CamSite *camSite){
                    control()->newAction(Message.NVR_CHANGE_IP_NEED_REFRESH, dataStructCmsChangeIp);
                },[this](QString message){
                    qDebug() << Q_FUNC_INFO << "Load data Failure";
                });
            }
        }else{
            Site *workingSite = this->control()->appContext->getWorkingSite();
            if(workingSite != Q_NULLPTR && workingUser != Q_NULLPTR){
                //load for site save to local
                QString tokenUser = workingUser->getToken();
                this->loadAllCamerasOfSiteSaveToDisk(tokenUser , workingSite->getSiteId() , [this, dataStructCmsChangeIp](QJsonObject jsonObject){
                    control()->newAction(Message.NVR_CHANGE_IP_NEED_REFRESH, dataStructCmsChangeIp);
                },[this](QString message){
                    qDebug() << Q_FUNC_INFO << "Load data Failure";
                });
            }
        }
    }else{
        control()->newAction(Message.NVR_CHANGE_IP_NEED_REFRESH, dataStructCmsChangeIp);
    }
}

//load with api 003 (load all cameras of site)
void A_Socket::loadAllCamerasOfSiteSaveToDisk(QString token, int siteId, std::function<void(QJsonObject jsonObject)> onSuccess, std::function<void(QString)> onFailure){
    std::function<void(QJsonObject)> onFetchSucess = [this ,onSuccess, siteId](QJsonObject jsonObject){
        if(!jsonObject.isEmpty()){
            LoadDataLocal::instance().saveDataCamerasOfSiteToDisk(siteId, jsonObject);
            onSuccess(jsonObject);
        }
    };

    std::function<void(QString)> onFetchFailure = [this, onFailure](QString message){
        onFailure(message);
        qDebug() << "Load camera of site error";
    };
    CamSite::getCamerasOfSiteSaveToLocal(siteId, token, onFetchSucess, onFetchFailure);
}

void A_Socket::getCamerasWithDeviceId(QString token, std::function<void(CamSite *)> onSuccess,
                                      std::function<void(QString)> onFailure) {
    QSettings settings;
    QString offlineMode = settings.value("offline_mode").toString();
    std::function<void(QJsonObject)> onFetchSuccess =
            [onSuccess, onFailure](QJsonObject jsonObject) {
        //        qDebug() << Q_FUNC_INFO << jsonObject;
        //        code	403
        //        success	false
        //        message	"Permission denied."

        QJsonValue jsonValue;
        int code = 0;
        bool success = false;
        QString message = "";
        jsonValue = jsonObject.take("code");
        if(!jsonValue.isNull()){
            code  = jsonValue.toInt();
        }
        jsonValue = jsonObject.take("success");
        if(!jsonValue.isNull()){
            success = jsonValue.toBool();
        }

        jsonValue = jsonObject.take("message");
        if(!jsonValue.isNull()){
            message = jsonValue.toString();
        }

        QString permisionDenied = "Permission denied.";
        if(code == 403 && success == false && message == permisionDenied){
            onFailure(message);
        }else{
            //save data to local
            CamSite *camSite = CamSite::parseWithOrder(true, jsonObject);
            //save data to local
            LoadDataLocal::instance().saveDataCamerasOfDeviceToDisk(jsonObject);
            onSuccess(camSite);
        }
    };

    std::function<void(QString)> onFetchFailure = [onFailure](QString message) {
        onFailure(message);
    };

    QMap<QString, QString> params;
    params["device_id"] = NetworkUtils::instance().getMacAddress();
    params["token"] = token;

    NetworkUtils::instance().getRequest(3000, AppProfile::getAppProfile()->getAppConfig()->getCamSiteByDeviceApiUri(),
                                        params, onFetchSuccess, onFetchFailure);
}

//pick up/ kick out camera
void A_Socket::refreshDataWhenCamOrderCMSChanged(){
    qDebug() << "refreshDataWhenCamOrderCMSChanged";
    control()->appContext->setIsPickUpOrKickOutCam(true);
    QSettings settings;
    QString cmsChangeCamOrder = "CMS_CHANGE_CAM_ORDER";
    QVariant *dataStructCmsChanged = new QVariant();
    dataStructCmsChanged->setValue<QString>(cmsChangeCamOrder);
    QString offlineMode = settings.value("offline_mode").toString();
    if(offlineMode == "ON"){
        User *workingUser = this->control()->appContext->getWorkingUser();
        if(this->control()->appContext->getIsLoadDataWithDeviceId()){
            if(workingUser != Q_NULLPTR){
                QString tokenUser = workingUser->getToken();
                this->getCamerasWithDeviceId(tokenUser , [this, dataStructCmsChanged](CamSite *camSite){
                    control()->newAction(Message.CMS_CHANGE_CAM_ORDER_NEED_REFRESH, dataStructCmsChanged);
                },[this](QString message){
                    qDebug() << Q_FUNC_INFO << "Load data Failure";
                });
            }
        }else{
            Site *workingSite = this->control()->appContext->getWorkingSite();
            if(workingSite != Q_NULLPTR && workingUser != Q_NULLPTR){
                //load for site save to local
                QString tokenUser = workingUser->getToken();
                this->loadAllCamerasOfSiteSaveToDisk(tokenUser , workingSite->getSiteId() , [this, dataStructCmsChanged](QJsonObject jsonObject){
                    control()->newAction(Message.CMS_CHANGE_CAM_ORDER_NEED_REFRESH, dataStructCmsChanged);
                },[this](QString message){
                    qDebug() << Q_FUNC_INFO << "Load data Failure";
                });
            }
        }
    }else{
        control()->newAction(Message.CMS_CHANGE_CAM_ORDER_NEED_REFRESH, dataStructCmsChanged);
    }
}

void A_Socket::startJoinSite(int siteId){
    siteIdJoinCurrent = siteId;
    if(siteIdJoinCurrent != -1){
        leaveSiteData.arg(siteIdJoinCurrent);
        QByteArray bytes = leaveSiteData.toUtf8();
        std::string Data(bytes.data(), bytes.length());
        _io->socket()->emit("leave_join",Data);
        qDebug() << "Start leave site";
    }

    QString optiton = joinSiteData.arg(QString::number(siteId));
    qDebug() << "Start join site" << optiton<< siteId;

    QByteArray bytes = optiton.toUtf8();
    std::string Data(bytes.data(), bytes.length());
    _io->socket()->emit("join_site",Data);
}

void A_Socket::connectToSocket() {

    QString token = this->control()->appContext->getWorkingUser()->getToken();
    mOptions["token"] = token.toStdString();
    std::string socketUrl =
            AppProfile::getAppProfile()->getAppConfig()->getSocketUrl().toStdString();
    _io->connect(socketUrl, mOptions);
    QSettings settings;
    QString offlineMode = settings.value("offline_mode").toString();
    if(offlineMode != "ON"){
        control()->newAction(Message.SHOW_INDICATOR, Q_NULLPTR);
    }
    if(!timerJoinDevice.isActive()) timerJoinDevice.start(5000);
}

void A_Socket::onConnected(std::string const &nsp) {
    Q_EMIT onSocketConnected();
}

void A_Socket::socketReadyWorking(){
    joinSocketSuccess = true;
    //    startJoinClipRoom();
    startRegisterDevice();
    startRegisterUser(idUserSocket);
}

void A_Socket::startJoinClipRoom(){
    ////“JoinClipRoom”: Request join room in socket server (data: {user_id: 123, type: 'client'});)
    ///
    int userIdWorking = this->control()->appContext->getWorkingUser()->getUserId();
    QString options = joinRoomData.arg(QString::number(userIdWorking));
    qDebug() << Q_FUNC_INFO << options;
    QByteArray bytes = options.toUtf8();
    std::string Data(bytes.data(), bytes.length());
    _io->socket()->emit("JoinClipRoom",Data);
}

void A_Socket::startRegisterDevice(){
    QString options = mDeviceID.arg(NetworkUtils::instance().getMacAddress()).toLower();
    QByteArray bytes = options.toUtf8();
    std::string deviceID(bytes.data(), bytes.length());
    _io->socket()->emit("join", deviceID);
    qDebug() << "startRegisterDevice THREAD" <<QThread::currentThread();
}

void A_Socket::startRegisterUser(int userId){
    idUserSocket = userId;
    QString optionsUser = mUserID.arg(userId).toLower();
    QByteArray bytes = optionsUser.toUtf8();
    std::string userID(bytes.data(), bytes.length());
    _io->socket()->emit("join", userID);
}

bool A_Socket::getIsJoinedDevice() const
{
    return isJoinedDevice;
}

void A_Socket::setIsJoinedDevice(bool value)
{
    isJoinedDevice = value;
}

void A_Socket::onClosed(client::close_reason const& reason) {
    qDebug() << Q_FUNC_INFO<< "Socket disconnect";
    if(!timerJoinDevice.isActive())
    {
        timerJoinDevice.start(3000);
    }

    if(!timerJoinSocket.isActive()){
        timerJoinSocket.start(15000);
    }
    this->control()->appContext->setJoinDeviceSuccess(false);
    joinSocketSuccess = false;
    Q_EMIT onSocketDisconnected();
    control()->newAction(Message.HIDE_INDICATOR, Q_NULLPTR);
}

void A_Socket::onFailed() {
    qDebug() << Q_FUNC_INFO;
    if(!timerJoinDevice.isActive())
    {
        timerJoinDevice.start(3000);
    }
    if(!timerJoinSocket.isActive()){
        timerJoinSocket.start(15000);
    }
    joinSocketSuccess = false;
    this->control()->appContext->setJoinDeviceSuccess(false);
    control()->newAction(Message.HIDE_INDICATOR, Q_NULLPTR);
}


void A_Socket::onConnectError(std::string const& name, message::ptr const& data,
                              bool hasAck, message::list& ack_resp) {
    if (data->get_flag() == message::flag_object) {
    }

}

void A_Socket::onConnectTimeout(std::string const& name, message::ptr const& data,
                                bool hasAck, message::list& ack_resp) {
    if (data->get_flag() == message::flag_object) {
        qDebug() <<"onConnectTimeout";
    }
}

void A_Socket::onError(std::string const& name, message::ptr const& data,
                       bool hasAck, message::list& ack_resp) {
    if (data->get_flag() == message::flag_object) {
        qDebug() << "onError";

    }
}

void A_Socket::onConnectedSuccess(std::string const& name, message::ptr const& data,
                                  bool hasAck, message::list& ack_resp) {
    if (data->get_flag() == message::flag_object) {
        qDebug() << "onConnectedSuccess";
    }
}

void A_Socket::onUserChangePassword(std::string const& name, message::ptr const& data,
                                    bool hasAck, message::list& ack_resp){
    qDebug() << "onUserChangePassword";
    if(data->get_flag() == message::flag_object){
        int code = data->get_map()["code"]->get_int();

        std::string msg = data->get_map()["message"]->get_string();
        QString messageString = QString::fromUtf8(msg.data(), msg.length());

        bool logout = data->get_map()["logout"]->get_bool();

        if(code == 0 && logout){
            Q_EMIT onUserChangedPassword();
        }
    }
}

void A_Socket::userChangedPasswordNeedLogout(){
    qDebug() <<Q_FUNC_INFO << "userChangedPasswordNeedLogout";
    control()->newSystemAction(Message.USER_CHANGED_PASSWORD_NEED_LOGOUT , Q_NULLPTR);
}


void A_Socket::onUserJoined(std::string const& name, message::ptr const& data,
                            bool hasAck, message::list& ack_resp) {
    qDebug() << Q_FUNC_INFO << "Join Socket success";
    if (data->get_flag() == message::flag_object) {
        std::string msg = data->get_map()["message"]->get_string();
        QString message = QString::fromUtf8(msg.data(), msg.length());
        Q_EMIT onSocketJoined();
    }
}

void A_Socket::onJoined() {
    qDebug() << "JOIN DEVICE SUCCESS====================================";
    control()->appContext->setJoinDeviceSuccess(true);
    if(timerJoinDevice.isActive()){
        timerJoinDevice.stop();
    }
    control()->newAction(Message.HIDE_INDICATOR, Q_NULLPTR);
    control()->newSystemAction(Message.APP_SOCKET_CONNECTED, Q_NULLPTR);
}

void A_Socket::onUpdateControl(std::string const& name,
                               message::ptr const& data, bool hasAck,
                               message::list& ack_resp) {
    if (data->get_flag() == message::flag_object) {
        std::string error = data->get_map()["error"]->get_string();
        std::string message = data->get_map()["message"]->get_string();
        QString codeError = QString::fromStdString(error);
        QString messageSuccess = QString::fromStdString(message);

        qDebug() << Q_FUNC_INFO << "onUpdateControl success"<< codeError << messageSuccess;
        if(error == "0" && messageSuccess == "Success"){
            std::string dataUpdate = data->get_map()["data"]->get_string();

            qDebug() << "DATA UPDATE CONTROL" << QString::fromStdString(dataUpdate);

        }
    }
}

void A_Socket::onCamOrderChanger(const string& name, const message::ptr& data,
                                 bool hasAck, message::list& ack_resp) {
    qDebug() << Q_FUNC_INFO;
    if (data->get_flag() == message::flag_object) {
        if(this->control()->appContext->getIsLoadDataWithDeviceId()){
            Q_EMIT onUpdateRemoteControl();
        }
    }
}
QString A_Socket::createStringListCameraOrder(QList<CamItem *> listCameras){
    QString result;
    QString formatCameraOrder = "%1:%2";
    for (int index = 0; index < listCameras.size(); ++index) {
        result.append(formatCameraOrder.arg(listCameras.at(index)->getCameraId()).arg(listCameras.at(index)->getOrder()));
        if(index != (listCameras.size() -1)){
            result.append(",");
        }
    }
    return result;
}
void A_Socket::updateCameraOrder(){
    int layoutSelected, pageSelected, cameraIdFullScreen;
    QString deviceId;
    layoutSelected = this->control()->appContext->getLayoutSelected();
    pageSelected = this->control()->appContext->getPageSelected();
    deviceId = NetworkUtils::instance().getMacAddress().toLower();
    cameraIdFullScreen = this->control()->appContext->getCameraIdFullSceen();
    qDebug() << "Size camera working = " << this->control()->appContext->getListCamerasWorkingCurrent().size();
    QString listOrder = createStringListCameraOrder(this->control()->appContext->getListCamerasWorkingCurrent());
    QString options = dataUpdateCameraOrder.arg("").arg(layoutSelected).arg(pageSelected).arg(deviceId).arg(cameraIdFullScreen);
    QByteArray bytes = options.toUtf8();
    std::string dataUpdateCamera(bytes.data(), bytes.length());
    qDebug() << "updateCameraOrder" << QString::fromStdString(dataUpdateCamera);
    _io->socket()->emit("updateCameraOrder", dataUpdateCamera);

}

void A_Socket::remoteControlChanged(){
    QSettings settings;
    QString offlineMode = settings.value("offline_mode").toString();
    if(offlineMode != "ON"){
        this->control()->appContext->setIsNeedSynchronizedWithServer(true);
        control()->newSystemAction(Message.LOAD_DATA_CAMERAS_WITH_DEVICE_ID, Q_NULLPTR);
    }else {
            qDebug() <<Q_FUNC_INFO << "remoteControlChanged " << "offlineMode" << offlineMode;
    }
}


//void A_Socket::startGetClip(std::string const& cameraId,std::string const& cameraName,std::string const& urlVideo){
//    ////“GetClip”: Request trích clip. (data: {"camera_id":3506,"camera_name":"Khu vực cầu thang máy 5",
//    /// "url":"http://2c1.vcam.viettel.vn/rec/hls/d02212d8bb28xyz11381_1506272520000_360000.m3u8"})
//    ///
//    QString options = getClipData.arg(QString::fromStdString(cameraId)).arg(QString::fromStdString(cameraName)).arg(QString::fromStdString(urlVideo));
//    QByteArray bytes = options.toUtf8();
//    std::string DataGetClip(bytes.data(), bytes.length());
//    _io->socket()->emit("GetClip",DataGetClip);
//}

void A_Socket::startGetClip(int cameraId,QString videoName,QString urlVideo){
    ////“GetClip”: Request trích clip. (data: {"camera_id":3506,"camera_name":"Khu vực cầu thang máy 5",
    /// "url":"http://2c1.vcam.viettel.vn/rec/hls/d02212d8bb28xyz11381_1506272520000_360000.m3u8"})
    ///
    //    QString options = getClipData.arg(QString::fromStdString(cameraId)).arg(QString::fromStdString(cameraName)).arg(QString::fromStdString(urlVideo));
    //    QString options = getClipData.arg(QString::number(cameraId)).arg(videoName).arg(urlVideo);
    //    qDebug() << "OPTION GET CLIP " << options;
    videoNameRecord = videoName;
    urlRecordCurrent = urlVideo;
    QJsonObject jsonObject;
    jsonObject.insert("camera_id",cameraId);
    jsonObject.insert("camera_name", videoName);
    jsonObject.insert("url", urlVideo);
    QJsonDocument document(jsonObject);
    QString strJson(document.toJson(QJsonDocument::Compact));
    qDebug() << "OPTION GET CLIP " << strJson;
    QByteArray bytes = strJson.toUtf8();
    std::string DataGetClip(bytes.data(), bytes.length());
    _io->socket()->emit("GetClip",DataGetClip);
}

void A_Socket::onCancelGetClipSuccess(std::string const &name, message::ptr const &data,
                                      bool hasAck, message::list &ack_resp){
    if (data->get_flag() == message::flag_object) {
        int code = data->get_map()["code"]->get_int();
        std::string messagestring = data->get_map()["message"]->get_string();
        QString message = QString::fromStdString(messagestring);
        qDebug() << "onCancelGetClipSuccess" << code ;
        if(code == 0  &&  message == "success"){
            Q_EMIT onCancelCreateVideoSuccess();
        }
    }
}

void A_Socket::cancelCreateVideoSuccess(){
    control()->newSystemAction(Message.CANCEL_CREATE_VIDEO_RECORD_SUCCESS, Q_NULLPTR );
}

void A_Socket::onGetClipSuccess(std::string const &name, message::ptr const &data,
                                bool hasAck, message::list &ack_resp){

    //    {code: 404, message: "Bad url !", user_id: 2203, url: "https://cdn1-vcam.viettelstore.vn/rec/hls/d02212eebc31xyz26367_1510362008000_25192000.m3u8"} //neu url lỗi

    qDebug() << "on get clip success";
    if(!urlRecordCurrent.isEmpty()){
        if (data->get_flag() == message::flag_object) {
            QString message = "";
            int code = 0;
            if(data->get_map().find("code") != data->get_map().end()){
                code = data->get_map()["code"]->get_int();
            }
            if(data->get_map().find("message") != data->get_map().end()){
                std::string messagestring = data->get_map()["message"]->get_string();
                message = QString::fromStdString(messagestring);
            }
            qDebug() << "onGetClipSuccess" << code << message;
            if(code == 0  &&  message == "success"){
                if (data->get_map().find("percent") != data->get_map().end()){//check exits persent
                    std::string percent = data->get_map()["percent"]->get_string();
                    qDebug() << "PERCENT DOWNLOAD" << QString::fromStdString(percent);
                    int percentInt = QString::fromStdString(percent).toInt();
                    Q_EMIT onUpdatePercentGenerateLink(percentInt);
                    if(QString::fromStdString(percent) == "100"){
                        std::string urlGetClip = data->get_map()["url"]->get_string();
                        QString urlGetClipString = QString::fromStdString(urlGetClip);
                        qDebug() << "urlGetClipString" << urlGetClipString;
                        if(urlGetClipString.split("//").last() ==  urlRecordCurrent.split("//").last()){
                            // neu dung voi url dang can record thi nhan
                            std::string urlMp4 = data->get_map()["url_file"]->get_string();
                            std::string fileName = data->get_map()["camera_name"]->get_string();
                            QString filenameRecord = videoNameRecord;
                            QString urlFileMp4Record = QString::fromUtf8(urlMp4.data(),urlMp4.length());
                            Q_EMIT onGetClipSocketSuccess(filenameRecord, urlFileMp4Record);
                        }
                    }
                }
            }else{
                Q_EMIT onGetClipSocketError();
            }
        }
    }
}

void A_Socket::updatePercentGenerateLink(int percent){
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<int>(percent);
    control()->newSystemAction(Message.UPDATE_PERCENT_SOCKET_GERNERATE_LINK_MP4_RECORD, dataStruct);
}

void A_Socket::getClipRecordError(){
    control()->newSystemAction(Message.APP_PLAY_BACK_GET_CLIP_RECORD_ERROR, Q_NULLPTR );
}


void A_Socket::getClipRecordSuccess(QString nameVideo, QString urlVideo){
    DataGetClip dataGetClip;
    dataGetClip.nameVideo = nameVideo;
    dataGetClip.urlVideo = urlVideo;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<DataGetClip>(dataGetClip);
    control()->newSystemAction(Message.APP_PLAY_BACK_GET_CLIP_RECORD_SUCCESS,dataStruct);
    urlRecordCurrent = ""; //reset link record
    videoNameRecord= "";
}

void A_Socket::cancelGetClip(){
    urlRecordCurrent = "";
    videoNameRecord = "";
    QJsonObject jsonObject;
    jsonObject.insert("url",urlRecordCurrent);
    QJsonDocument document(jsonObject);
    QString strJson(document.toJson(QJsonDocument::Compact));
    qDebug() << Q_FUNC_INFO << strJson;
    QByteArray bytes = strJson.toUtf8();
    std::string DataCancelGetClip(bytes.data(), bytes.length());
    _io->socket()->emit("cancel_clip",DataCancelGetClip);
}



void A_Socket::onJoinClipRoom(std::string const &name, message::ptr const &data,
                              bool hasAck, message::list &ack_resp){
    qDebug() << "onJoinClipRoom-Success";
    if (data->get_flag() == message::flag_object) {
        std::string name = data->get_map()["message"]->get_string();
        QString message = QString::fromUtf8(name.data(), name.length());
        qDebug() << Q_FUNC_INFO << message;
    }
}
