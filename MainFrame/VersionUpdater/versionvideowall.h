#ifndef VERSIONVIDEOWALL_H
#define VERSIONVIDEOWALL_H

#include <QObject>
#include <QJsonArray>
#include <QJsonDocument>
#include  <QJsonObject>
#include <QJsonValue>
class VersionVideowall : public QObject
{
    Q_OBJECT
private :
    int idIndex = -1;
    int versionX = -1;
    int versionY = -1;
    int versionZ = -1;
    QString lastupdate = "";
    QString newMessage = "";
    QString pathFile = "";
    int moduleId = -1;
    QString moduleName = "";
public:
    VersionVideowall();
    static VersionVideowall* paser(QJsonObject jsonObject);
    QString getNewMessage() const;
    int getModuleId() const;
    QString getModuleName() const;
    QString getPathFile() const;
    QString getLastupdate() const;
    int getVersionX() const;
    int getVersionY() const;
    int getVersionZ() const;
};

#endif // VERSIONVIDEOWALL_H
