#include "versionvideowall.h"


QString VersionVideowall::getNewMessage() const
{
    return newMessage;
}

int VersionVideowall::getModuleId() const
{
    return moduleId;
}

QString VersionVideowall::getModuleName() const
{
    return moduleName;
}

QString VersionVideowall::getPathFile() const
{
    return pathFile;
}

QString VersionVideowall::getLastupdate() const
{
    return lastupdate;
}

int VersionVideowall::getVersionX() const
{
    return versionX;
}

int VersionVideowall::getVersionY() const
{
    return versionY;
}

int VersionVideowall::getVersionZ() const
{
    return versionZ;
}

VersionVideowall::VersionVideowall()
{

}

VersionVideowall *VersionVideowall::paser(QJsonObject jsonObject)
{
    VersionVideowall* versionVideowall = new VersionVideowall();
    QJsonValue value;
    value = jsonObject.take("id");
    if(!value.isNull()){
        int valueInt =  value.toInt();
        versionVideowall->idIndex = valueInt;
    }
    value = jsonObject.take("version_x");
    if(!value.isNull()){
        QString valueString =  value.toString();
        int valueInt = valueString.toInt();
        versionVideowall->versionX = valueInt;
    }
    value = jsonObject.take("version_y");
    if(!value.isNull()){
        QString valueString =  value.toString();
        int valueInt = valueString.toInt();
        versionVideowall->versionY = valueInt;
    }
    value = jsonObject.take("version_z");
    if(!value.isNull()){
        QString valueString =  value.toString();
        int valueInt = valueString.toInt();
        versionVideowall->versionZ = valueInt;
    }

    value = jsonObject.take("lastupdate");
    if(!value.isNull()){
        versionVideowall->lastupdate = value.toString();
    }

    value = jsonObject.take("new_message");
    if(!value.isNull()){
        versionVideowall->newMessage = value.toString();
    }

    value = jsonObject.take("module_id");
    if(!value.isNull()){
        int valueInt =  value.toInt();
        versionVideowall->moduleId = valueInt;
    }
    value = jsonObject.take("module_name");
    if(!value.isNull()){
        QString moduleNameString = value.toString();
        versionVideowall->moduleName = moduleNameString;
    }

    value = jsonObject.take("path_file");
    if(!value.isNull()){
        versionVideowall->pathFile = value.toString();
    }

    return versionVideowall;
}
