#include "p_versionupdater.h"

/**
     * Generic method to override for updating the presention.
     **/

P_VersionUpdater::P_VersionUpdater(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {

    // init gui object
    this->zone = zone;
    this->zone->hide();
    this->zoneLayout = new QVBoxLayout();
    this->zoneLayout->setContentsMargins(marginContain,marginContain,marginContain,marginContain);
    this->zoneLayout->setSpacing(10);
    this->zoneLayout->setAlignment(Qt::AlignTop);
    this->zone->setLayout(this->zoneLayout);
    this->zone->setStyleSheet("background-color:#333; color:#FFFFFF; border:0px solid #8a8a92;border-radius :0px;");

    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect(this->zone);
    effect->setBlurRadius(80);
    effect->setColor(QColor("#000000"));
    effect->setOffset(0,0);
    this->zone->setGraphicsEffect(effect);
    this->zone->setFixedSize(QSize(600,400));
    this->zone->setAttribute(Qt::WA_Hover, true);
    this->zone->installEventFilter(this);
    this->zone->setMouseTracking(true);
    this->zone->setAttribute(Qt::WA_Hover);
    //    //init title
    //    this->topWidget = new QWidget(this->zone);
    //    this->topWidget->setFixedSize(this->zone->width(), heightTopForm);

    //    this->topWidget->setStyleSheet(
    //                "background: QLinearGradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 "
    //                "#eff0f5, stop: 1 #dedede); border: 0px solid #8a8a92; "
    //                "border-top-left-radius: 5px; border-top-right-radius: 5px; "
    //                "border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;");
    //    this->topLayout = new QVBoxLayout();
    //    this->topLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    //    this->topLayout->setSpacing(5);
    //    this->topLayout->setContentsMargins(0, 0, 0, 0);

    //    this->topWidget->setLayout(this->topLayout);
    //    this->zoneLayout->addWidget(this->topWidget);
    //    initTitleZone();
    //    //containwidget chứa toàn bộ widget ở phần dưới
    //    containBottomWidget = new QWidget(this->zone);
    //    containBottomWidget->move(0, this->heightTopForm);
    //    containBottomWidget->setFixedSize(this->zone->width() , this->zone->height() - heightTopForm);
    //    containBottomWidget->setStyleSheet("background:#FFFFFF;color:black");

    //    QVBoxLayout *containBottomLayout = new QVBoxLayout();
    //    containBottomLayout->setAlignment(Qt::AlignCenter);
    //    containBottomWidget->setLayout(containBottomLayout);
    //    containBottomLayout->setSpacing(0);
    //    containBottomLayout->setMargin(0);
    //    containBottomLayout->setAlignment(Qt::AlignCenter);
    //    this->zoneLayout->addWidget(containBottomWidget); // add contain


    //top widget
    //    topTitleWidget = new QWidget(this->zone);
    //    topTitleWidget->move(0,0);
    //    topTitleWidget->setFixedSize(this->zone->width() , this->heightTop);
    //    topTitleLayout = new QVBoxLayout();
    //    topTitleLayout->setAlignment(Qt::AlignCenter);
    //    topTitleLayout->setSpacing(0);
    //    topTitleLayout->setMargin(0);
    //    topTitleWidget->setLayout(topTitleLayout);
    //top title

    //    QLabel *descriptionTitleLable = new QLabel(this->topTitleWidget);
    //    descriptionTitleLable->setText("Mô tả bản cập nhật");
    //    topTitleLayout->addWidget(descriptionTitleLable);

    //contain widget
    containWidget = new QWidget(this->zone);
    //    containWidget->setStyleSheet("background:green");
    containWidget->setFixedSize(this->zone->width() - (2*marginContain) ,this->zone->height() - this->heightBottom - (2*marginContain) - this->heightCheckSaveBottom );
    containLayout = new QVBoxLayout();
    containLayout->setAlignment(Qt::AlignCenter);
    containLayout->setSpacing(0);
    containLayout->setMargin(0);
    containWidget->setLayout(containLayout);

    descriptionUpdateTextEdit = new QTextEdit(this->containWidget);
    descriptionUpdateTextEdit->setFixedSize(containWidget->size());

    QString descriptionUpdateNew = "+ Update (26.03.2018) (cập nhật chính của bản này) : <br/>"
                                   "- VideoWall trên nút cạnh HD, SD thêm nút mặc định hiện theo mode CDN/LAN hiện tại đang chọn trong setting. <br/>"
                                   " - Nếu ở một camera đang là CDN kích vào chuyển sang LAN (chơi luồng local) và ngược lại. (lưu lại trên ổ đĩa lần sau mở lại vẫn còn các lựa chọn cũ)<br/>"
                                   "- Khi chọn trong mục setting thì reset toàn bộ theo mode chọn đó.<br/>"
                                   "- Kick out/ Pick up camera ở CMS: VideowallPC không hoạt động đúng khi có camera đang Fullscreen<br/>"
                                   "+ Update 27.03.2018 :<br/>"
                                   " - Thêm setting tận dụng khoảng trống(ON/OFF) nếu chọn ON thì sẽ tự chọn layout phù hợp với số lượng camera của trang đó<br/>"
                                   "- Update thêm tính năng retry login khi router thay đổi ip liên tục";
    descriptionUpdateTextEdit->setHtml(descriptionUpdateNew);
    containLayout->addWidget(descriptionUpdateTextEdit);

    //bottom widget
    bottomWidget = new QWidget(this->zone);
    //    bottomWidget->setStyleSheet("background:red;color:white; border-radius:none");
    bottomWidget->setFixedSize(this->zone->width() - (2*marginContain) , this->heightBottom);
    bottomLayout = new QHBoxLayout();
    bottomLayout->setAlignment(Qt::AlignCenter);
    bottomLayout->setSpacing(25);
    bottomLayout->setMargin(0);
    bottomWidget->setLayout(bottomLayout);

    QPushButton *updateVersionButton =  new QPushButton(bottomWidget);
    updateVersionButton->setStyleSheet("background-color:#ef2636; color:white;border-radius :5px;");
    updateVersionButton->setFixedSize(150, 30);
    updateVersionButton->setText(iconButtonTops.download +" Đồng Ý Cập Nhật");
    connect(updateVersionButton , &QPushButton::clicked , this, &P_VersionUpdater::updateVersionButtonClicked);

    //update late
    QPushButton *updateVersionLateButton =  new QPushButton(bottomWidget);
    updateVersionLateButton->setStyleSheet("background-color:#DEDEDE; color:gray;border-radius :5px;");
    updateVersionLateButton->setFixedSize(150, 30);
    updateVersionLateButton->setText(iconButtonTops.close + " Đóng");
    connect(updateVersionLateButton , &QPushButton::clicked , this, &P_VersionUpdater::onCloseButtonClicked);

    bottomLayout->addWidget(updateVersionLateButton);
    bottomLayout->addWidget(updateVersionButton);



    //bottom  left widget
    bottomSaveUpdateWidget = new QWidget(this->zone);
    bottomSaveUpdateWidget->setStyleSheet("padding-left:50px");
    bottomSaveUpdateWidget->setFixedSize(this->bottomWidget->width() , this->heightCheckSaveBottom);
    bottomSaveUpdateLayout = new QHBoxLayout();
    bottomSaveUpdateLayout->setAlignment(Qt::AlignLeft);
    bottomSaveUpdateLayout->setSpacing(25);
    bottomSaveUpdateLayout->setMargin(0);
    bottomSaveUpdateWidget->setLayout(bottomSaveUpdateLayout);

    QString isAutoUpdateNewVersionDefault = this->control()->appContext->getIsAutoUpdateNewVersion();
    QSettings settings;
    QString isAutoUpdateNewVersionSave =  settings.value("auto_update_version").toString();
    if(isAutoUpdateNewVersionSave == "false" || isAutoUpdateNewVersionSave == "true"){
        isAutoUpdateNewVersionDefault = isAutoUpdateNewVersionSave;
    }


    QCheckBox *checkboxSaveAutoUpdate = new QCheckBox("Lần sau tự động cập nhật, không tự động hiển thị thông báo này", this->bottomSaveUpdateWidget);
    bottomSaveUpdateLayout->addWidget(checkboxSaveAutoUpdate);
    connect(checkboxSaveAutoUpdate , &QCheckBox::clicked , this, &P_VersionUpdater::autoUpdateLater);
    if(isAutoUpdateNewVersionDefault == "true"){
        checkboxSaveAutoUpdate->setChecked(true);
    }else{
        checkboxSaveAutoUpdate->setChecked(false);
    }

    //add widget
    //    containBottomLayout->addWidget(topTitleWidget);
    this->zoneLayout->addWidget(containWidget);
    this->zoneLayout->addWidget(bottomWidget);
    this->zoneLayout->addWidget(bottomSaveUpdateWidget);

}

QString P_VersionUpdater::createDescriptionVersionNewest(){
    QString descriptioneReturn;
    VersionInfo *versionInfo = control()->appContext->getVersionInfoNewest();
    if(versionInfo != Q_NULLPTR){
        for (int index = 0; index < versionInfo->getListVersionVideowallNew().size(); ++index) {
            VersionVideowall *versionVideowall = new VersionVideowall();
            QString versionHead = "Version " + QString::number(versionVideowall->getVersionX()) + "." + QString::number(versionVideowall->getVersionY()) + QString::number(versionVideowall->getVersionZ())+ ":" + "<br/>";
            QString desciption = versionVideowall->getNewMessage();
            descriptioneReturn.append(versionHead).append(desciption);
            int indexLast = descriptioneReturn.lastIndexOf("-");
            descriptioneReturn.insert(indexLast + 1, "<br/>");
        }
    }
    return descriptioneReturn;
}

void P_VersionUpdater::changeDescriptionVersionNewest(){
    QString descriptionUpdateNew = createDescriptionVersionNewest();
    descriptionUpdateTextEdit->setHtml(descriptionUpdateNew);
}

void P_VersionUpdater::autoUpdateLater(bool checked){
    QSettings settings;
    QString checkedString;
    if(checked){
        checkedString = "true";
    }else{
        checkedString = "false";
    }
    this->control()->appContext->setIsAutoUpdateNewVersion(checkedString);
    settings.setValue("auto_update_version", checkedString);
}

void P_VersionUpdater::updateVersionButtonClicked(){
    qDebug() << Q_FUNC_INFO;

    QString uri = "cms_api/servers/updateModuleVersion";
    QMap<QString, QString> params;
    QString userToken = this->control()->appContext->getWorkingUser()->getToken();
    if(!userToken.isEmpty()){
        params["token"] = userToken;

    }

}

void P_VersionUpdater::updateVersionLateButtonClicked(){
    qDebug() << Q_FUNC_INFO;

}
void P_VersionUpdater::initTitleZone() {
    titleWidget = new QWidget(this->topWidget);
    titleWidget->setFixedSize(this->topWidget->size());
    titleWidget->setStyleSheet("background-color: #00000000");


    QHBoxLayout *titleLayout = new QHBoxLayout();
    titleLayout->setAlignment(Qt::AlignLeft);
    titleLayout->setSpacing(10);
    titleLayout->setContentsMargins(8, 10, 10, 8);
    titleWidget->setLayout(titleLayout);

    this->closeButton =
            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
                         "#aaff3b30", "#ff3b30", "#ff3b30", "");
    QPixmap pix(":/images/res/icon_tab_close.png");
    QIcon iconClose(pix);
    this->closeButton->setIcon(iconClose);
    this->closeButton->setIconSize(QSize(10, 10));
    connect(this->closeButton, &QPushButton::clicked, this,
            &P_VersionUpdater::onCloseButtonClicked);

    //    this->minimizeButton =
    //            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
    //                         "#aaffcc00", "#ffcc00", "#ffcc00", "");
    //    this->minimizeButton->setIcon(QIcon(":/images/res/icon_tab_minimize.png"));
    //    this->minimizeButton->setIconSize(QSize(10, 10));
    //    connect(this->minimizeButton, &QPushButton::clicked, this,
    //            &P_VersionUpdater::onMinimizeButtonClicked);

    //    this->maximizeButton =
    //            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
    //                         "#aa4cd964", "#4cd964", "#4cd964", "");
    //    this->maximizeButton->setIcon(QIcon(":/images/res/icon_tab_maximize.png"));
    //    this->maximizeButton->setIconSize(QSize(10, 10));
    //    connect(this->maximizeButton, &QPushButton::clicked, this,
    //            &P_VersionUpdater::onMaximizeButtonClicked);

    this->titleLabel = new QLabel(titleWidget);
    this->titleLabel->setMinimumWidth(this->topWidget->width() - 110);
    this->titleLabel->setAlignment(Qt::AlignCenter);
    this->titleLabel->setFont(Resources::instance().getLargeBoldButtonFont());
    this->titleLabel->setStyleSheet(
                "background-color: #00000000; color: #1e1e1e");
    this->titleLabel->setText("VideowallPC - Phiên bản cập nhật");

    titleLayout->addWidget(this->closeButton);
    //    titleLayout->addWidget(this->minimizeButton);
    //    titleLayout->addWidget(this->maximizeButton);
    titleLayout->addWidget(this->titleLabel);

    this->topLayout->addWidget(titleWidget);
}
void P_VersionUpdater::onCloseButtonClicked(){
    control()->zone->hide();
}

void P_VersionUpdater::onMinimizeButtonClicked(){

}

void P_VersionUpdater::onMaximizeButtonClicked(){

}
void P_VersionUpdater::update() {}

void P_VersionUpdater::show() {
    if (control()->zone->isVisible()) {
        control()->zone->hide();
        control()->newAction(Message.TOP_BAR_OVER_LAY_CAN_HIDE, Q_NULLPTR);
    } else {
        control()->zone->show();
        control()->zone->raise();
        control()->newAction(Message.TOP_BAR_OVER_LAY_CAN_NOT_HIDE, Q_NULLPTR);
    }
}

void P_VersionUpdater::needShowContinue() {
    if (!control()->zone->isVisible()) {
        QSettings settings;
        QString isAutoUpdate =  settings.value("auto_update_version").toString();
        if(isAutoUpdate == "false"){
            control()->zone->show();
            control()->zone->raise();
            control()->newAction(Message.TOP_BAR_OVER_LAY_SHOW_WHEN_VERSION_UPDATE_SHOW, Q_NULLPTR);
        }
    }
}

void P_VersionUpdater::sizeTopControlBar(QVariant *dataStruct) {
    dataSizeTop = dataStruct->value<SizeTopControlBar>();
    int widthMove = dataSizeTop.widthTopBar - dataSizeTop.widthProfileSetting - dataSizeTop.widthFullScreen - dataSizeTop.widthInfoUpdateNewVersion - 65 ;
    this->zone->move(widthMove, appSize.topHeight);
}

void P_VersionUpdater::hide() {
    control()->zone->hide();
}


QWidget *P_VersionUpdater::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}

bool P_VersionUpdater::eventFilter(QObject *watched, QEvent *event) {
    QWidget *widget = qobject_cast<QWidget *>(watched);
    if (widget == this->zone) {
        switch (event->type()) {
        case QEvent::HoverEnter:
            hoverEnter(static_cast<QHoverEvent *>(event));
            return true;
            break;
        case QEvent::HoverLeave:
            hoverLeave(static_cast<QHoverEvent *>(event));
            return true;
            break;
        case QEvent::HoverMove:
            hoverMove(static_cast<QHoverEvent *>(event));
            return true;
            break;
        default:
            break;
        }
    }
    return false;
}

void P_VersionUpdater::hoverEnter(QHoverEvent *) {}

void P_VersionUpdater::hoverLeave(QHoverEvent *) {
    control()->newUserAction(Message.VERSION_UPDATER_HIDE_ALL, Q_NULLPTR);
    control()->zone->hide();
}

void P_VersionUpdater::hoverMove(QHoverEvent *) {}

