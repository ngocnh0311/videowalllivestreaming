#include "a_versionupdater.h"
/**
 * Constructor for Abstraction facet. Register the control.
 * @param Crtl A ref on the control facet which manage this facet
 **/
A_VersionUpdater::A_VersionUpdater(Control* ctrl) : Abstraction(ctrl) {
    timerShowFormUpdater = new QTimer();
    connect(timerShowFormUpdater , &QTimer::timeout, this , &A_VersionUpdater::onShowFormUpdater);
    timerRequestCheckVersion = new QTimer();
    timerRequestCheckVersion->start(timeoutRequestFirst);//10s first
    connect(timerRequestCheckVersion , &QTimer::timeout , this, &A_VersionUpdater::onCheckVersionCMS);
}

void A_VersionUpdater::onCheckVersionCMS(){
    //    /module?action=get_latest_version&device_id_server=00:1f:c6:9c:6c:01&check_version=true
    QSettings settings;
    QString appVersionCurrent = settings.value("app_version").toString();
    QString uri = "/module";
    QMap<QString, QString> params;
    params["device_id_server"] = NetworkUtils::instance().getMacAddress();
    params["version"] = appVersionCurrent;
    params["check_version"] = "true";
    QString moduleId = "216";
    QString moduleIdConfig = AppProfile::getAppProfile()->getAppConfig()->getVideowallModuleId();
    if(!moduleIdConfig.isEmpty()){
        moduleId = moduleIdConfig;
    }
    params["module_id"] =  moduleId; //fix module id
    params["action"] = "get_latest_version";

    std::function<void(QJsonObject)> onFetchSuccess = [this](QJsonObject jsonObject){
        if(!jsonObject.isEmpty()){
            VersionInfo *versionVWInfo = VersionInfo::paser(jsonObject);
            if(versionVWInfo != Q_NULLPTR){
                control()->appContext->setVersionInfoNewest(versionVWInfo);
                QSettings settings;
                QString versionAppCurrent = settings.value("app_version").toString();
                QStringList stringlistversionAppCurrent = versionAppCurrent.split(".");
                if(stringlistversionAppCurrent.size() == 3){
                    int versionAppCurrentX = stringlistversionAppCurrent[0].toInt();
                    int versionAppCurrentY = stringlistversionAppCurrent[1].toInt();
                    int versionAppCurrentZ = stringlistversionAppCurrent[2].toInt();
                    VersionVideowall *versionVideowallNewest = versionVWInfo->getVersionVideowallNewest();
                    bool isNeedUpdate = false;
                    if(versionVideowallNewest != Q_NULLPTR){
                        int versionXNewest = versionVideowallNewest->getVersionX() ;
                        int versionYNewest = versionVideowallNewest->getVersionY();
                        int versionZNewest = versionVideowallNewest->getVersionZ();
                        qDebug() << "versionZNewest" <<versionXNewest << versionYNewest <<versionZNewest;
                        qDebug() << "versionAppCurrentY" <<versionAppCurrentX << versionAppCurrentY <<versionAppCurrentZ;

                        if(versionXNewest > versionAppCurrentX){
                            isNeedUpdate = true;
                        }
                        if (versionYNewest > versionAppCurrentY) {
                            isNeedUpdate = true;
                        }
                        if(versionZNewest > versionAppCurrentZ){
                            isNeedUpdate = true;
                        }
                    }
                    isFirstRequest = false;
                    this->control()->appContext->setIsNeedUpdate(isNeedUpdate);
                    //need update
                    QSettings settings;
                    double timestampCurrent = QDateTime::currentDateTime().toSecsSinceEpoch();
                    QString timestampCurrentToString = QString::number(timestampCurrent, 'g', 19);
                    long timestampCurrentLong = timestampCurrentToString.toLong();
                    long timestampCurrentSavedLong = 0;
                    QString timestampCurrentSavedToString = settings.value("timestamp_start_update", "").toString();
                    if(!timestampCurrentSavedToString.isEmpty()){
                        timestampCurrentSavedLong = timestampCurrentSavedToString.toLong();
                    }

                    long timeMinus = timestampCurrentLong - timestampCurrentSavedLong;
                    qDebug() << "timeMinus" <<timeMinus;

                    if(isNeedUpdate){
                        qDebug() << "NEED UPDATE NEW VERSION" << "jsonObject" <<jsonObject;
                        int stateUpdate = settings.value("state_update_version", -1).toInt();
                        QString autoUpdateSaved = settings.value("auto_update_version", this->control()->appContext->getIsAutoUpdateNewVersion()).toString();
                        if(autoUpdateSaved == "ON" && stateUpdate != versionUpdateStates.APP_VERSION_UPDATING)
                        {//auto update
                            control()->newAction(Message.AUTO_CLICK_UPDATE_VERSION , Q_NULLPTR);
                        }

                        if(autoUpdateSaved == "OFF"){
                            //not auto update
                            qDebug() << "stateUpdate" << "state" << stateUpdate;
                            if (stateUpdate != versionUpdateStates.APP_VERSION_UPDATING) {
                                //chưa nhấn nút cập nhật
                                settings.setValue("state_update_version", versionUpdateStates.APP_HAVE_VERSION_NEWEST);
                            }
                        }
                        control()->newSystemAction(Message.SHOW_BUTTON_UPDATE_VERSION_NEWEST , Q_NULLPTR);
                        if(!timerShowFormUpdater->isActive()){
                            timerShowFormUpdater->start(timeoutRequest);
                        }
                    }else{
                        //if updated or is version newest
                        int stateUpdate = settings.value("state_update_version", -1).toInt();
                        if (stateUpdate == versionUpdateStates.APP_VERSION_UPDATING) {
                            //update
                            settings.setValue("state_update_version", versionUpdateStates.APP_VERSION_UPDATED );
                        }
                        int stateUpdated = settings.value("state_update_version", -1).toInt();
                        if(stateUpdated == versionUpdateStates.APP_VERSION_UPDATED){
                            settings.setValue("number_display_form_update_new_version", 0);
                            if(timeMinus <= 3600){
                                if(!timerShowFormUpdater->isActive()){
                                    timerShowFormUpdater->start(timeoutRequest);
                                }
                                control()->newSystemAction(Message.SHOW_BUTTON_UPDATE_VERSION_NEWEST , Q_NULLPTR);
                            }else{
                                qDebug() << "timestamp update last" << timeMinus;
                                control()->newSystemAction(Message.VERSION_NEWEST_VERSION_UPDATER_HIDE_ALL , Q_NULLPTR);
                            }
                        }
                    }
                }
            }
        }
    };
    std::function<void(QString)> onFetchFailure = [this](QString message){
        qDebug() << Q_FUNC_INFO << "ERROR: " << message;
    };
    NetworkUtils::instance().getRequest(10000, uri, params , onFetchSuccess , onFetchFailure);

    if(timerRequestCheckVersion->isActive() && !isFirstRequest){
        timerRequestCheckVersion->stop();
        timerRequestCheckVersion->start(timeoutRequest); //5m
    }
}
void A_VersionUpdater::stopTimerRequest(){
//    isStop = true;
//    if(timerRequestCheckVersion->isActive()){
//        timerRequestCheckVersion->stop();
//    }
}

void A_VersionUpdater::onShowFormUpdater(){
    control()->newSystemAction(Message.VERSION_UPDATER_DISPLAY_AUTO_SHOW , Q_NULLPTR );
}
/**
 * Change the control of this abstraction
 * @param ctrl the new control for this abstraction facet
 **/
void A_VersionUpdater::changeControl(Control* ctrl) { this->ctrl = ctrl; }

/**
 * Method to create a new abstraction exactly equals to this abstraction.
 * @return the created clone
 **/
// public Abstraction getClone(){
//    try{
//        return (Abstraction)clone();
//    } catch(Exception e) {System.out.println("ERROR: can't duplicate an
//    abstraction.");}
//    return null;
//}
