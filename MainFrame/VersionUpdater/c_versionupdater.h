#ifndef C_VersionUpdater_H
#define C_VersionUpdater_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "TopControlBar/c_topcontrolbar.h"
#include "VideoWall/VWWorkSpace/c_vw_workspace.h"
#include "a_versionupdater.h"
#include "p_versionupdater.h"
#include "message.h"
class P_VersionUpdater;
class A_VersionUpdater;
class C_VWWorkSpace;
class C_TopControlBar;
class C_VersionUpdater : public Control {
 public:
  QWidget* zone;
  AppContext* appContext;
  C_VersionUpdater(Control* ctrl);
  C_MainFrame* getParent() { return (C_MainFrame*)this->parent; }
  P_VersionUpdater* presentation() { return (P_VersionUpdater*)pres; }
  A_VersionUpdater* abstraction() { return (A_VersionUpdater*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
};

#endif  // C_VersionUpdater_H
