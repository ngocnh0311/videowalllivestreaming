#ifndef VERSIONINFO_H
#define VERSIONINFO_H

#include <QObject>
#include <QJsonArray>
#include <QJsonDocument>
#include  <QJsonObject>
#include <QJsonValue>
#include "versionvideowall.h"
#include "Common/appprofile.h"
class VersionInfo : QObject
{
    Q_OBJECT
private:
    int code = -1;
public:
    QList<VersionVideowall *> listVersionVideowallNew;
    VersionInfo();
    VersionVideowall *getVersionVideowallNewest(){
        VersionVideowall * versionVideowallReturn =  Q_NULLPTR;
        if(!listVersionVideowallNew.isEmpty()){
            versionVideowallReturn = listVersionVideowallNew.last();
            return versionVideowallReturn;
        }
        return versionVideowallReturn;
    }
    static VersionInfo* paser(QJsonObject jsonObject);
    QList<VersionVideowall *> getListVersionVideowallNew() const;
};

#endif // VERSIONINFO_H
