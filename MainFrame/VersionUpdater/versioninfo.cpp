#include "versioninfo.h"

QList<VersionVideowall *> VersionInfo::getListVersionVideowallNew() const
{
    return listVersionVideowallNew;
}

VersionInfo::VersionInfo()
{

}

VersionInfo*  VersionInfo::paser(QJsonObject jsonObject)
{
    VersionInfo *versionInfo = new VersionInfo();
    QJsonValue value;
    value = jsonObject.take("code");
    if(!value.isNull()){
        int codeInt = value.toInt();
        versionInfo->code = codeInt;
    }

    QJsonObject dataJson = jsonObject.take("data").toObject();
    QString moduleId = "216";
    QString moduleIdConfig = AppProfile::getAppProfile()->getAppConfig()->getVideowallModuleId();
    if(!moduleIdConfig.isEmpty()){
        moduleId = moduleIdConfig;
    }
    QJsonArray arrayVersion = dataJson.take(moduleId).toArray();

    for (int index = 0; index < arrayVersion.size(); ++index) {
        QJsonObject jsonVersionVideowall = arrayVersion.at(index).toObject();
        VersionVideowall *versionVideowall = VersionVideowall::paser(jsonVersionVideowall);
        versionInfo->listVersionVideowallNew.append(versionVideowall);
    }

    return versionInfo;
}
