#ifndef A_VersionUpdater_H
#define A_VersionUpdater_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_versionupdater.h"
#include "versioninfo.h"
class C_VersionUpdater;
class A_VersionUpdater : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:
    bool isFirstRequest = true;
    int timeoutRequest = 5*60*1000;
    int timeoutRequestFirst = 10000;

    QTimer *timerShowFormUpdater = Q_NULLPTR;
    QTimer *timerRequestCheckVersion = Q_NULLPTR;
public:
    A_VersionUpdater(Control *ctrl);
    C_VersionUpdater *control() { return (C_VersionUpdater *)this->ctrl; }
    void changeControl(Control *ctrl);
    void stopTimerRequest();
public Q_SLOTS:
    void onShowFormUpdater();
    void onCheckVersionCMS();
};

#endif  // A_VersionUpdater_H
