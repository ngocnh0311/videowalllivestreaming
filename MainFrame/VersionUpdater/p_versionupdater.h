#ifndef P_VersionUpdater_H
#define P_VersionUpdater_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "Site/sitechild.h"
#include "c_versionupdater.h"
#include <QTextEdit>
#include <QCheckBox>
class C_VersionUpdater;

class P_VersionUpdater : public Presentation {
    // init ui control
private:
    int heightTopForm = 35;
    int heightTop = 35;
    int heightBottom = 45;
    int heightCheckSaveBottom = 30;
    int marginContain = 10;

    // init ui control
    SizeTopControlBar dataSizeTop;
public:

    //init title
    QVBoxLayout *zoneLayout;
    QPushButton *closeButton;
    QPushButton *minimizeButton;
    QPushButton *maximizeButton;
    QWidget *titleWidget;
    QLabel *titleLabel;
    QWidget *topWidget;
    QVBoxLayout *topLayout;
    QWidget *containBottomWidget = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;

    QVBoxLayout *layout = Q_NULLPTR;

    QWidget *topTitleWidget = Q_NULLPTR;
    QVBoxLayout * topTitleLayout = Q_NULLPTR;

    QWidget *containWidget = Q_NULLPTR;
    QVBoxLayout * containLayout = Q_NULLPTR;

    QWidget *bottomWidget = Q_NULLPTR;
    QHBoxLayout * bottomLayout = Q_NULLPTR;

    QWidget *bottomSaveUpdateWidget = Q_NULLPTR;
    QHBoxLayout * bottomSaveUpdateLayout = Q_NULLPTR;

    QTextEdit *descriptionUpdateTextEdit = Q_NULLPTR;

    C_VersionUpdater *control() { return (C_VersionUpdater *)this->ctrl; }
    P_VersionUpdater(Control *ctrl, QWidget *zone);
    void changeControl(Control *ctrl);
    void update();
    QWidget *getZone(int zoneId);
    void show();
    void needShowContinue();
    void hide();
    void sizeTopControlBar(QVariant *dataStruct);
    void initTitleZone();
    QPushButton *createButton(QWidget *parent, QSize size, QRect borderRadius,
                              int borderWidth, QString backngroundColor,
                              QString textColor, QString borderColor,
                              QString text) {
        QPushButton *button = new QPushButton(parent);
        button->setFixedSize(size);
        QString css =
                "background-color: %1; color: %2; border: %3px solid %4;"
                "border-top-left-radius: %5; border-top-right-radius: %6; "
                "border-bottom-left-radius: %7; border-bottom-right-radius: %8; ";
        button->setStyleSheet(css.arg(backngroundColor)
                              .arg(textColor)
                              .arg(borderWidth)
                              .arg(borderColor)
                              .arg(borderRadius.x())
                              .arg(borderRadius.y())
                              .arg(borderRadius.width())
                              .arg(borderRadius.height()));
        button->setText(text);
        button->setFont(Resources::instance().getExtraSmallRegularButtonFont());
        return button;
    }
    void changeDescriptionVersionNewest();
    QString createDescriptionVersionNewest();

protected:
    void hoverEnter(QHoverEvent *event);
    void hoverLeave(QHoverEvent *event);
    void hoverMove(QHoverEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
public Q_SLOTS:
    void onCloseButtonClicked();
    void onMinimizeButtonClicked();
    void onMaximizeButtonClicked();
    void updateVersionButtonClicked();
    void updateVersionLateButtonClicked();
    void autoUpdateLater(bool checked);

};

#endif  // P_VersionUpdater_H
