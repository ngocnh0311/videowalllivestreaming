#include "p_helpuser.h"

/**
     * Generic method to override for updating the presention.
     **/

P_HelpUser::P_HelpUser(Control *ctrl, QWidget *zone) : Presentation(ctrl) {
    QSize screenSize = Resources::instance().getScreenSize();
    // init gui object
    this->zone = zone;
    //    this->zone->setStyleSheet("background: #EEEAE6;color:black;");
    this->zone->setStyleSheet("background-color:white;border:0px solid #8a8a92;border-radius :5px;");
    //    this->zone->move((screenSize.width() - this->zone->width()) / 2,
    //                     (screenSize.height() - this->zone->height()) / 2);

    this->zoneLayout = new QVBoxLayout();
    this->zoneLayout->setMargin(0);
    this->zoneLayout->setSpacing(0);
    this->zoneLayout->setAlignment(Qt::AlignTop);
    this->zone->setLayout(this->zoneLayout);
    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect(this->zone);
    effect->setBlurRadius(80);
    effect->setColor(QColor("#000000"));
    effect->setOffset(0,0);
    this->zone->setGraphicsEffect(effect);
    this->zone->setFixedSize(QSize(520,343));

    //init title
    this->topWidget = new QWidget(this->zone);
    this->topWidget->setFixedSize(this->zone->width(), 35);

    this->topWidget->setStyleSheet(
                "background: QLinearGradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 "
                "#eff0f5, stop: 1 #dedede); border: 0px solid #8a8a92; "
                "border-top-left-radius: 5px; border-top-right-radius: 5px; "
                "border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;");
    //  this->topWidget->move(0, 0);
    //  this->topWidget->resize(this->zone->width(), 100);

    this->topLayout = new QVBoxLayout();
    this->topLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    this->topLayout->setSpacing(5);
    this->topLayout->setContentsMargins(0, 0, 0, 0);

    this->topWidget->setLayout(this->topLayout);
    this->zoneLayout->addWidget(this->topWidget);
    initTitleZone();
    this->zone->setGeometry(
                QStyle::alignedRect(
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    this->zone->size(),
                    qApp->desktop()->availableGeometry()
                    )
                );

    //containwidget chứa toàn bộ widget ở phần dưới
    QWidget *containWidget = new QWidget(this->zone);
    containWidget->setStyleSheet("background: #FFFFFF;color:gray;");
    QVBoxLayout *containLayout = new QVBoxLayout();
    containLayout->setAlignment(Qt::AlignCenter);
    containWidget->setLayout(containLayout);
    containLayout->setSpacing(20);
    containLayout->setMargin(20);
    containLayout->setAlignment(Qt::AlignCenter);
    this->zoneLayout->addWidget(containWidget); // add contain

    QWidget *helpContaintWidget = new QWidget(containWidget);
    QVBoxLayout *helpContaintLayout = new QVBoxLayout();
    helpContaintLayout->setAlignment(Qt::AlignCenter);
    helpContaintLayout->setSpacing(20);
    helpContaintLayout->setMargin(20);
    helpContaintWidget->setLayout(helpContaintLayout);

    QFont fontHelp;
    fontHelp.setBold(true);
    fontHelp.setPixelSize(25);
    fontHelp.setFamily("Ubuntu Regular");

    QLabel *helpLabel = new QLabel(helpContaintWidget);
    helpLabel->setAlignment(Qt::AlignCenter);

    helpLabel->setText("Tổng đài hỗ trợ miễn phí");
    helpLabel->setFont(fontHelp);

    QFont fontNumberHelp;
    fontNumberHelp.setBold(true);
    fontNumberHelp.setPixelSize(28);

    QLabel *numberHelpLabel = new QLabel(containWidget);
    numberHelpLabel->setAlignment(Qt::AlignCenter);

    numberHelpLabel->setText("1800 80 00");
    numberHelpLabel->setFont(fontNumberHelp);

    QLabel *branchHelp = new QLabel(helpContaintWidget);
    branchHelp->setAlignment(Qt::AlignCenter);
    branchHelp->setFont(fontNumberHelp);
    branchHelp->setText("Nhánh 1");

    QFont fontCopyRight;
    fontCopyRight.setBold(true);
    fontCopyRight.setPixelSize(15);

    QLabel *copyrightLabel = new QLabel(helpContaintWidget);
    copyrightLabel->setFont(fontCopyRight);
    copyrightLabel->setAlignment(Qt::AlignCenter);
    copyrightLabel->setText("Nhánh 1");
    copyrightLabel->setText("2017 © bản quyền thuộc về vCam.");

    helpContaintLayout->addWidget(helpLabel);
    helpContaintLayout->addWidget(numberHelpLabel);
    helpContaintLayout->addWidget(branchHelp);
    helpContaintLayout->addWidget(copyrightLabel);


    QWidget *bottomWidget = new QWidget(containWidget);
    QVBoxLayout *bottomLayout = new QVBoxLayout();
    bottomLayout->setAlignment(Qt::AlignRight);
    bottomLayout->setSpacing(0);
    bottomLayout->setMargin(0);
    bottomWidget->setLayout(bottomLayout);

    containLayout->addWidget(helpContaintWidget);
    containLayout->addWidget(bottomWidget);


    QPushButton *closeAboutAppButton = new QPushButton(bottomWidget);
    closeAboutAppButton->setFont(Resources::instance().getMediumBoldButtonFont());
    closeAboutAppButton->setStyleSheet("background-color:#DEDEDE; border-radius:2px");
    closeAboutAppButton->setFixedSize(80,35);
    connect(closeAboutAppButton, &QPushButton::clicked, this , [this]{
        this->zone->hide();
    });

    closeAboutAppButton->setText("Đóng");
    bottomLayout->addWidget(closeAboutAppButton);

    this->zone->hide();
}

void P_HelpUser::initTopZone() {

}


void P_HelpUser::initTitleZone() {
    titleWidget = new QWidget(this->topWidget);
    titleWidget->setFixedSize(this->topWidget->size());
    titleWidget->setStyleSheet("background-color: #00000000");
    //    titleWidget->installEventFilter(this);
    //    titleWidget->setMouseTracking(true);
    //    titleWidget->setAttribute(Qt::WA_Hover);

    QHBoxLayout *titleLayout = new QHBoxLayout();
    titleLayout->setAlignment(Qt::AlignLeft);
    titleLayout->setSpacing(10);
    titleLayout->setContentsMargins(8, 10, 10, 8);
    titleWidget->setLayout(titleLayout);

    this->closeButton =
            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
                         "#aaff3b30", "#ff3b30", "#ff3b30", "");
    this->closeButton->setIcon(QIcon(":/images/res/icon_tab_close.png"));
    this->closeButton->setIconSize(QSize(10, 10));
    connect(this->closeButton, &QPushButton::clicked, this,
            &P_HelpUser::onCloseButtonClicked);

    this->minimizeButton =
            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
                         "#aaffcc00", "#ffcc00", "#ffcc00", "");
    this->minimizeButton->setIcon(QIcon(":/images/res/icon_tab_minimize.png"));
    this->minimizeButton->setIconSize(QSize(10, 10));
    connect(this->minimizeButton, &QPushButton::clicked, this,
            &P_HelpUser::onMinimizeButtonClicked);

    this->maximizeButton =
            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
                         "#aa4cd964", "#4cd964", "#4cd964", "");
    this->maximizeButton->setIcon(QIcon(":/images/res/icon_tab_maximize.png"));
    this->maximizeButton->setIconSize(QSize(10, 10));
    connect(this->maximizeButton, &QPushButton::clicked, this,
            &P_HelpUser::onMaximizeButtonClicked);

    this->titleLabel = new QLabel(titleWidget);
    this->titleLabel->setMinimumWidth(this->topWidget->width() - 110);
    this->titleLabel->setAlignment(Qt::AlignCenter);
    this->titleLabel->setFont(Resources::instance().getLargeBoldButtonFont());
    this->titleLabel->setStyleSheet(
                "background-color: #00000000; color: #1e1e1e");
    this->titleLabel->setText("Tổng đài hỗ trợ");

    titleLayout->addWidget(this->closeButton);
    titleLayout->addWidget(this->minimizeButton);
    titleLayout->addWidget(this->maximizeButton);
    titleLayout->addWidget(this->titleLabel);

    this->topLayout->addWidget(titleWidget);
}

void P_HelpUser::onCloseButtonClicked(){
    this->zone->hide();
}

void P_HelpUser::onMinimizeButtonClicked(){

}

void P_HelpUser::onMaximizeButtonClicked(){

}

//bool P_HelpUser::eventFilter(QObject *watched, QEvent *event) {
//  QWidget *widget = (QWidget *)watched;
//  if (widget == titleWidget) {
//      qDebug() << "EVEntFilter";
//    QMouseEvent *mouseEvent = (QMouseEvent *)event;
//    if (mouseEvent) {
//      if (mouseEvent->type() == QMouseEvent::MouseButtonPress) {
//        oldPos = mouseEvent->globalPos();
//        isMoving = true;
//      }
//      if (mouseEvent->type() == QMouseEvent::MouseMove) {
//        if (isMoving) {
//          const QPoint delta = mouseEvent->globalPos() - oldPos;
//          this->zone->move(this->zone->x() + delta.x(),
//                           this->zone->y() + delta.y());
//          oldPos = mouseEvent->globalPos();
//        }
//      }
//      if (mouseEvent->type() == QMouseEvent::MouseButtonRelease) {
//        isMoving = false;
//      }
//    }
//  }
//  return true;
//}




void P_HelpUser::update() {}

QObject *P_HelpUser::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}
void P_HelpUser::showHelpUser(){
    this->zone->show();
    this->zone->raise();
}

/**
     * Method to create a new presentation exactly equals (field by field) to
  *this presentation.
     * @return the created clone
     **/
//    public Presentation getClone(){
//        try{
//            return (Presentation)clone();
//        } catch(Exception e) {System.out.println("ERROR: can't duplicate a
//        presentation.");}
//        return null;
//    }
