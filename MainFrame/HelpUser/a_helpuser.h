#ifndef A_HelpUser_H
#define A_HelpUser_H

#include <QObject>
#include "c_helpuser.h"
#include "PacModel/control.h"

class C_HelpUser;
class A_HelpUser : public Abstraction {
    Q_OBJECT
public:
    C_HelpUser *control() { return (C_HelpUser *)ctrl; }
    A_HelpUser(Control *ctrl);

};

#endif  // ABSTRACTION_H
