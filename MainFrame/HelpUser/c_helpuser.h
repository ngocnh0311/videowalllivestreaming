#ifndef C_HelpUser_H
#define C_HelpUser_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>

#include "c_helpuser.h"
#include "a_helpuser.h"
#include "p_helpuser.h"
#include "PacModel/control.h"
#include "message.h"
#include "Authentication/appcontext.h"
#include "MainFrame/c_mainframe.h"
class P_HelpUser;
class A_HelpUser;
class C_MainFrame;
class C_HelpUser : public Control {
    QWidget* zone;

public:
    AppContext *appContext = Q_NULLPTR;
    C_HelpUser(Control* ctrl, QWidget* zone);
    P_HelpUser * presentation() {return (P_HelpUser *) this->pres;}
    A_HelpUser * abstraction() {return (A_HelpUser *) this->abst;}
    C_MainFrame *getParent() { return (C_MainFrame *)this->parent; }


    /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
    void newUserAction(int message, QVariant* attachment) override;
    /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newSystemAction(int message, QVariant* attachment) override;

    /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
    void newAction(int message, QVariant* attachment) override;
};

#endif  // CONTROL_H
