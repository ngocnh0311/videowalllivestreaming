#include "MainFrame/p_mainframe.h"
#include <QEvent>
#include <QStackedLayout>
#include <QTimer>
#include <QVBoxLayout>
#include "Login/loginwidget.h"
#include <QRect>
/**
     * Generic method to override for updating the presention.
     **/

P_MainFrame::P_MainFrame(Control *ctrl, QWidget *zone) : Presentation(ctrl) {
    this->zone = zone;
    QSize screenSize = Resources::instance().getScreenSize();
    QSettings settings;
    QString isShowWindowsList = settings.value("show_windows_list").toString();
    if(isShowWindowsList == "HIDE"){
        screenSize = QApplication::desktop()->screenGeometry().size();
    }else{
        screenSize = QApplication::desktop()->availableGeometry(this->zone).size();
    }

    int withZone = screenSize.width();
    int heightZone = screenSize.height();
    this->zone->setStyleSheet("background-color: #00000000; color: white;");
    this->zone->setGeometry(QApplication::desktop()->availableGeometry(0)); //main default show in screen 0
    if(isShowWindowsList == "HIDE"){
        this->zone->resize(withZone , heightZone);
        this->zone->showFullScreen();
    }else{
        this->zone->resize(withZone , heightZone);
        this->zone->show();
    }
    // init top bar
    topBar = new QWidget(this->zone);
    topBar->resize(this->zone->size().width(), appSize.topHeight);
    topBar->updateGeometry();

    // init top bar
    topBarOverlay = new QWidget(this->zone);
    topBarOverlay->resize(0, 0);
    topBarOverlay->show();
    topBarOverlay->raise();
    topBarOverlay->move(0, 0);
    topBarOverlay->updateGeometry();

    workspaceZone = new QWidget(this->zone);
    workspaceZone->resize(this->zone->width(), this->zone->height() - this->topBar->height());
    workSpaceLayout = new QStackedLayout();
    workspaceZone->setLayout(workSpaceLayout);




    // init work space
    workSpaceForCameras = new QWidget(this->workspaceZone);
    workSpaceForVideoWall = new QWidget(this->workspaceZone);
    workSpaceForVideoWall->setStyleSheet("background:black");
    workSpaceForPlayBack = new QWidget(this->workspaceZone);
    workSpaceForPlayBack->setStyleSheet("background:black");
    qDebug() << "workspaceZone->size()" << workspaceZone->size();

    workSpaceForCameras->resize(workspaceZone->size());
    workSpaceForVideoWall->resize(workspaceZone->size());
    workSpaceForPlayBack->resize(workspaceZone->size());
    // init main window
    QVBoxLayout *layout = new QVBoxLayout();
    layout->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
    layout->setSpacing(0);
    layout->setMargin(0);
    this->zone->setLayout(layout);

    layout->addWidget(topBar);
    layout->addWidget(workspaceZone);
    workSpaceLayout->addWidget(workSpaceForCameras);
    workSpaceLayout->addWidget(workSpaceForVideoWall);
    workSpaceLayout->addWidget(workSpaceForPlayBack);
    //    this->zone->setMouseTracking(true);
    //    this->zone->setAttribute(Qt::WA_Hover);
    //    this->zone->installEventFilter(this);

    workSpaceForCameras->setMouseTracking(true);
    workSpaceForCameras->setAttribute(Qt::WA_Hover);
    workSpaceForCameras->installEventFilter(this);

    workSpaceForVideoWall->setMouseTracking(true);
    workSpaceForVideoWall->setAttribute(Qt::WA_Hover);
    workSpaceForVideoWall->installEventFilter(this);

    workSpaceForPlayBack->setMouseTracking(true);
    workSpaceForPlayBack->setAttribute(Qt::WA_Hover);
    workSpaceForPlayBack->installEventFilter(this);

    workSpaceLayout->setCurrentWidget(workSpaceForVideoWall);


    topBarOverlay->setMouseTracking(true);
    topBarOverlay->setAttribute(Qt::WA_Hover);
    topBarOverlay->installEventFilter(this);


    settingZone = new QWidget(this->zone);

    aboutApp = new QWidget(this->zone);

    helpUser = new QWidget(this->zone);

    loadingIndicator = new QWidget(this->zone);

    this->loadingIndicator->resize(screenSize.width() * 0.6f, screenSize.height() * 0.6f);
    this->loadingIndicator->move((screenSize.width() - this->loadingIndicator->width()) / 2,
                                 (screenSize.height() - this->loadingIndicator->height()) / 2);

    discoverCameras = new QWidget(this->zone);

    this->discoverCameras->resize(screenSize.width() * 0.6f, screenSize.height() * 0.6f);
    this->discoverCameras->move((screenSize.width() - this->loadingIndicator->width()) / 2,
                                (screenSize.height() - this->loadingIndicator->height()) / 2);

    hideZone(Message.HIDE);

}

void P_MainFrame::update() {}

QWidget *P_MainFrame::getZone(int zoneId) {
    switch (zoneId) {
    case -1:
        return topBarOverlay;
    case 0:
        return topBar;
    case 1:
        return workSpaceForVideoWall;
    case 2:
        return workSpaceForPlayBack;
    case 3:
        return workSpaceForCameras;
    case 4:
        return siteTop;
    case 5:
        return settingZone;
    case 6:
        return loadingIndicator;
    case 7 :
        return aboutApp;
    case 8:
        return helpUser;
    case 9:
        return managerWindows;
    case 10:
        return discoverCameras;
    default:
        return Q_NULLPTR;
    }
}

QWidget * P_MainFrame::getZone(){
    return this->zone;
}

void P_MainFrame::showFullScreen(){
    QSize fullscreenSize = QApplication::desktop()->screenGeometry().size();
    qDebug() << "fullscreenSize" <<fullscreenSize;
    this->zone->showFullScreen();
    this->zone->resize(fullscreenSize);
    this->topBar->setFixedWidth(fullscreenSize.width());
    this->topBarOverlay->setFixedWidth(fullscreenSize.width());
    this->topBar->updateGeometry();
    this->topBarOverlay->updateGeometry();
}

void P_MainFrame::hideFullScreen(){
    qDebug() << "hideFullScreen";
    this->zone->showNormal();
    this->zone->raise();
    this->topBar->updateGeometry();
    this->topBarOverlay->updateGeometry();
}


void P_MainFrame::showApp(int message) {
    switch (message) {

    case Message.APP_VIDEO_WALL_SHOW:
    {
        workSpaceForVideoWall->show();
        workSpaceLayout->setCurrentWidget(workSpaceForVideoWall);
        hideZone(Message.HIDE_INDICATOR);
    }
        break;

    case Message.APP_PLAY_BACK_SHOW:
    {
        workSpaceForPlayBack->show();
        workSpaceLayout->setCurrentWidget(workSpaceForPlayBack);
        hideZone(Message.HIDE_INDICATOR);

    }
        break;

    case Message.APP_CAMERAS_MANAGER_SHOW:{
        workSpaceForCameras->show();
        workSpaceLayout->setCurrentWidget(workSpaceForCameras);
        hideZone(Message.HIDE_INDICATOR);
    }break;

    case Message.SHOW_INDICATOR:
    {
        loadingIndicator->show();
        loadingIndicator->raise();
    }
        break;

    case Message.SHOW_ZONE_DISCOVER_CAMERAS:{
        discoverCameras->show();
        discoverCameras->raise();
    }break;

    default:break;
    }
}

void P_MainFrame::hideZone(int message){
    switch (message) {
    case Message.HIDE_INDICATOR:
    {
        loadingIndicator->hide();
        loadingIndicator->lower();

    }
        break;

    case Message.HIDE_ZONE_DISCOVER_CAMERAS:{
        discoverCameras->hide();
        discoverCameras->lower();
    }break;


    case Message.HIDE:
    {
        workSpaceForCameras->hide();
        workSpaceForPlayBack->hide();
        workSpaceForVideoWall->hide();
        hideZone(Message.HIDE_INDICATOR);
        hideZone(Message.HIDE_ZONE_DISCOVER_CAMERAS);
    }
        break;

    default:
        break;
    }
}

void P_MainFrame::enterFullscreenMode() {
    topBar->setFixedHeight(0);
    control()->newUserAction(Message.ENTER_FULLSCREEN_MODE, Q_NULLPTR);
}

void P_MainFrame::exitFullscreenMode() {
    topBar->setFixedHeight(appSize.topHeight);
    control()->newUserAction(Message.EXIT_FULLSCREEN_MODE, Q_NULLPTR);
}

bool P_MainFrame::eventFilter(QObject *watched, QEvent *event) {
    QWidget *widget = qobject_cast<QWidget *>(watched);
    if (widget == workSpaceForVideoWall) {
        switch (event->type()) {
        case QEvent::HoverEnter:
            //          hoverEnter(static_cast<QHoverEvent *>(event));
            return false;
            break;
        case QEvent::HoverLeave:
            //          hoverLeave(static_cast<QHoverEvent *>(event));
            return false;
            break;
        case QEvent::HoverMove: {
            QPoint point = workSpaceForVideoWall->mapFromGlobal(QCursor::pos());
            if (point.y() < 3) {
                //          exitFullscreenMode();
                topBarOverlay->resize(this->zone->size().width(), appSize.topHeight);
                this->topBar->updateGeometry();
                this->topBarOverlay->updateGeometry();
                topBarOverlay->show();
                topBarOverlay->raise();
            }

            if(point.y() > 40){
                //                qDebug() << "workSpaceForVideoWall HoverMove > 40";
            }
            return false;
        } break;
        default:
            return false;
            break;
        }
    }

    if (widget == workSpaceForPlayBack) {
        switch (event->type()) {
        case QEvent::HoverEnter:
            //          hoverEnter(static_cast<QHoverEvent *>(event));
            return false;
            break;
        case QEvent::HoverLeave:
            //          hoverLeave(static_cast<QHoverEvent *>(event));
            return false;
            break;
        case QEvent::HoverMove: {
            QPoint point = workSpaceForPlayBack->mapFromGlobal(QCursor::pos());
            if (point.y() < 3) {
                topBarOverlay->resize(this->zone->size().width(), appSize.topHeight);
                this->topBar->updateGeometry();
                this->topBarOverlay->updateGeometry();
                topBarOverlay->show();
                topBarOverlay->raise();
                // exitFullscreenMode();
            }

            if(point.y() > 40){
                //                qDebug() << "workSpaceForPlayBack HoverMove > 40";
            }
            return false;
        } break;
        default:
            return false;
            break;
        }
    }

    if (widget == workSpaceForCameras) {
        switch (event->type()) {
        case QEvent::HoverEnter:
            //          hoverEnter(static_cast<QHoverEvent *>(event));
            return false;
            break;
        case QEvent::HoverLeave:
            //          hoverLeave(static_cast<QHoverEvent *>(event));
            return false;
            break;
        case QEvent::HoverMove: {
            QPoint point = workSpaceForCameras->mapFromGlobal(QCursor::pos());
            if (point.y() < 3) {
                topBarOverlay->resize(this->zone->size().width(), appSize.topHeight);
                this->topBar->updateGeometry();
                this->topBarOverlay->updateGeometry();
                topBarOverlay->show();
                topBarOverlay->raise();
                // exitFullscreenMode();
            }

            if(point.y() > 40){
                //                qDebug() << "workSpaceForCameras HoverMove > 40";
            }
            return false;
        } break;
        default:
            return false;
            break;
        }
    }

    if (widget == topBarOverlay) {
        switch (event->type()) {
        case QEvent::MouseButtonPress:
            //        topBarOverlay->resize(0, 0);
            //        exitFullscreenMode();
            return false;
            break;
        case QEvent::HoverEnter:
            //          hoverEnter(static_cast<QHoverEvent *>(event));
            return false;
            break;
        case QEvent::HoverLeave: {
            if (isTopBarOverlayCanHide == true) {
                topBarOverlay->resize(0,0);
            }
            return false;
        } break;
        case QEvent::HoverMove: {
            return false;
        } break;
        default:
            return false;
            break;
        }
    }
    if (widget == this->zone) {
        switch (event->type()) {
        case QEvent::Resize: {
            QResizeEvent *resizeEvent = (QResizeEvent *)(event);
            QSize newSize = resizeEvent->size();
            this->zone->resize(newSize);
            this->topBar->setFixedWidth(newSize.width());
            workspaceZone->resize(this->zone->width(), this->zone->height() - this->topBar->height());
            this->topBarOverlay->setFixedWidth(newSize.width());
            this->topBar->updateGeometry();
            this->topBarOverlay->updateGeometry();
            qDebug() << Q_FUNC_INFO << "newSize"<< newSize;
            return false;
        }break;
        default:
            return false;
            break;
        }
    }
    return false;
}

void P_MainFrame::showTopBarOverlay(){
    topBarOverlay->resize(this->zone->size().width(), appSize.topHeight);
    this->topBar->updateGeometry();
    this->topBarOverlay->updateGeometry();
    topBarOverlay->show();
    topBarOverlay->raise();
    isTopBarOverlayCanHide = false;
}

void P_MainFrame::topBarOverlayCanHide() {
    isTopBarOverlayCanHide = true;
    topBarOverlay->resize(0, 0);
    this->topBar->updateGeometry();
    this->topBarOverlay->updateGeometry();
}
void P_MainFrame::topBarOverlayCanNotHide() {
    topBarOverlay->show();
    topBarOverlay->raise();
    isTopBarOverlayCanHide = false;
}
