#include "p_network.h"

/**
     * Generic method to override for updating the presention.
     **/

P_Network::P_Network(Control *ctrl, QWidget *zone) : Presentation(ctrl) {
    this->zone = zone;
    messageBox.setButtonText(1, "Thoát ứng dụng");
    //    messageBox.setStyleSheet(
    //                "QLabel{min-width:200 px; font-size: 14px;} QPushButton{ width:100px; "
    //                "font-size: 12px; }");
}

void P_Network::showDialog(QString message) {
    if(!messageBox.isVisible()){
        messageBox.setFixedSize(200,150);
        messageBox.setIcon(QMessageBox::Warning);
        messageBox.setText(message);
        messageBox.show();
        if (messageBox.exec() == QMessageBox::Ok) {
            if (control()->abstraction()->getConnectedNetwork()) {

            } else {
                qDebug() << Q_FUNC_INFO <<"APPLICATION_PID_BEFORE_EXIT: " << QApplication::QCoreApplication::applicationPid();
                qApp->quit();
                QProcess process;
                process.execute(QString("kill %1").arg(
                                    QApplication::QCoreApplication::applicationPid()));
            }
        }
    }
}

void P_Network::hideDialog() {
    if(messageBox.isVisible()){
        messageBox.hide();
    }
}

void P_Network::update() {}

QObject *P_Network::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}
