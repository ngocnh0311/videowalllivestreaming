#include "a_network.h"

/**
 * Constructor for Abstraction facet. Register the control.
 * @param Crtl A ref on the control facet which manage this facet
 **/
bool A_Network::getConnectedNetwork() const
{
    return connectedNetwork;
}

A_Network::A_Network(Control* ctrl) : Abstraction(ctrl) {
    pingProcess = new QProcess(this);
    connect(pingProcess, &QProcess::readyReadStandardOutput, this,
            &A_Network::onReadyReadStandardOutput);

    //    connect(pingProcess, &QProcess::readyReadStandardError, this,
    //            &A_Network::onReadyReadStandardError);

    timerCheckNetwork = new QTimer(this);
    connect(timerCheckNetwork, &QTimer::timeout ,this, &A_Network::recheckNetwork);

    timerCheckNetworkDisconnect = new QTimer(this);
    connect(timerCheckNetworkDisconnect, &QTimer::timeout ,this, &A_Network::checkNetWorkDisconnect);

    //    QSettings settings;
    //    QString offlineMode = settings.value("offline_mode").toString();
    //    if(offlineMode == "ON"){
    //        needUpdateDataLocal = true;
    //    }
}

void A_Network::recheckNetwork(){
    connectedNetwork = false;
}

void A_Network::checkNetWorkDisconnect(){
    if(!connectedNetwork){
        onNetworkIsUnreachable();
    }
}

/**
 * Change the control of this abstraction
 * @param ctrl the new control for this abstraction facet
 **/
void A_Network::changeControl(Control* ctrl) { this->ctrl = ctrl; }



void A_Network::stopTimerRecheck(){
    //    loadDataFirstSuccess = true;
}

void A_Network::startChecking() {
    if(!timerCheckNetwork->isActive()){
        timerCheckNetwork->start(1200);
    }
    if(!timerCheckNetworkDisconnect->isActive()){
        timerCheckNetworkDisconnect->start(1100);
    }
    qDebug() << "start checking network";
    // get host from api url
    hostPing = AppProfile::getAppProfile()->getAppConfig()->getBaseApiUrl();
    QUrl url = QUrl(hostPing);
    hostPing = url.host();
    // ping with interval 200 ms
    QString exec = "ping";
    QStringList params;
    params << this->hostPing << "-i"
           << "1.0";
    qDebug() << "params network" << params;
    // start pinging
    pingProcess->start(exec, params, QIODevice::ReadOnly);
    QCoreApplication::processEvents();
}

void A_Network::onReadyReadStandardOutput() {
    if(timerCheckNetwork->isActive()){
        timerCheckNetwork->stop();
    }
    if(timerCheckNetworkDisconnect->isActive()){
        timerCheckNetworkDisconnect->stop();
    }

    if(!timerCheckNetworkDisconnect->isActive()){
        timerCheckNetworkDisconnect->start(1100);
    }
    if(!timerCheckNetwork->isActive()){
        timerCheckNetwork->start(1200);
    }

    connectedNetwork = true; //network is rechable
    QByteArray data = pingProcess->readAllStandardOutput();
    QString text(data);
    text.simplified();
    text.trimmed();
//    qDebug() << "onReadyReadStandardOutput +" << text;
    onNetworkIsReachable();
}

void A_Network::onReadyReadStandardError() {
    onNetworkIsUnreachable();
    QByteArray data = pingProcess->readAllStandardError();
    QString text(data);
    text.simplified();
    text.trimmed();
//    qDebug() << "onReadyReadStandardError" <<"-" << text;
}

void A_Network::stopChecking() { pingProcess->close(); }

//Network connected
void A_Network::onNetworkIsReachable() {
    this->control()->appContext->setNetworkConnected(true);
    QSettings settings;
    QString offlineMode = settings.value("offline_mode").toString();
    if(offlineMode != "ON"){
        control()->newSystemAction(Message.APP_NETWORK_IS_REACHABLE, Q_NULLPTR);
    }

    if(needUpdateDataAfterNetworkDisconnect){
        if(offlineMode == "ON"){
            needUpdateDataAfterNetworkDisconnect = false;
            control()->newSystemAction(Message.UPDATE_DATA_STAND_ALONE_MODE_AFTER_NETWORK_IS_UNREACHABLE , Q_NULLPTR);
        }else if(offlineMode == "OFF"){
            needUpdateDataAfterNetworkDisconnect = false;
            control()->newSystemAction(Message.UPDATE_DATA_NORMAL_MODE_AFTER_NETWORK_IS_UNREACHABLE , Q_NULLPTR);
        }
    }
}

//Disconnect Network
void A_Network::onNetworkIsUnreachable() {
//    this->control()->appContext->setNetworkConnected(false);
//    needUpdateDataAfterNetworkDisconnect = true;
//    QSettings settings;
//    QString offlineMode = settings.value("offline_mode").toString();
//    if(offlineMode != "ON"){
//        if (isFirst) {
//            isFirst = false;
//            control()->newSystemAction(Message.APP_NETWORK_IS_UNREACHABLE, Q_NULLPTR);
//        } else {
//            control()->newSystemAction(Message.APP_NETWORK_IS_UNREACHABLE, Q_NULLPTR);
//        }
//    }
}
