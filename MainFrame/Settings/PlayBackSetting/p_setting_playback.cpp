#include "p_setting_playback.h"

/**
     * Generic method to override for updating the presention.
     **/

P_SettingPlayBack::P_SettingPlayBack(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    this->zone->setStyleSheet("color:#4c4b52");
    //browse folder pictures
    QPushButton *browseFolderPicturesButton = new QPushButton(tr("&Browse..."), this->zone);
    browseFolderPicturesButton->setFont(Resources::instance().getLargeRegularButtonFont());

    connect(browseFolderPicturesButton, &QAbstractButton::clicked, this,
            &P_SettingPlayBack::browseFolderPictures);
    //browse folder videos
    QPushButton *browseFolderVideosButton = new QPushButton(tr("&Browse..."), this->zone);
    browseFolderVideosButton->setFont(Resources::instance().getLargeRegularButtonFont());

    connect(browseFolderVideosButton, &QAbstractButton::clicked, this,
            &P_SettingPlayBack::browseFolderVideos);

    QSettings settings;
    QString pathPicturesSaved  = settings.value("path_save_pictures").toString();
    QString pathVideosSaved  = settings.value("path_save_videos").toString();

    //    mkdir -p /mnt/hdd/videowallstore/pictures
    //    mkdir -p /mnt/hdd/videowallstore/videos

    QString directoryVideos, directoryPictures;
    //create folder videos
    if (pathVideosSaved.isEmpty()) {
        QString pathVideosDefault = QDir::homePath() + "/" + "MultimediaOfVideowall"+ "/" +  "Videos";
        directoryVideos = pathVideosDefault;
        if(!QDir(directoryVideos).exists()){
            QDir().mkpath(directoryVideos);
        }
        directoryVideosComboBox = createComboBox(QDir::toNativeSeparators(directoryVideos));
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>(directoryVideos);
        control()->newUserAction(Message.APP_PLAY_BACK_SET_PATH_SAVE_VIDEOS,
                                 dataStruct);
    } else {
        if(!QDir(pathVideosSaved).exists()){
            QDir().mkpath(pathVideosSaved);
        }
        directoryVideos = pathVideosSaved;
        directoryVideosComboBox = createComboBox(QDir::toNativeSeparators(pathVideosSaved));
    }


    //create folder pictures
    if (pathPicturesSaved.isEmpty()) {
        QString pathPicturesDefault = QDir::homePath() + "/" + "MultimediaOfVideowall"+ "/" +  "Pictures";
        directoryPictures = pathPicturesDefault;
        if(!QDir(directoryPictures).exists()){
            QDir().mkpath(directoryPictures);
        }

        directoryPicturesComboBox = createComboBox(QDir::toNativeSeparators(directoryPictures));
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>(directoryPictures);
        control()->newUserAction(Message.APP_PLAY_BACK_SET_PATH_SAVE_PICTURES,
                                 dataStruct);
    } else {
        if(!QDir(pathPicturesSaved).exists()){
            QDir().mkpath(pathPicturesSaved);
        }
        directoryPictures = pathPicturesSaved;
        directoryPicturesComboBox = createComboBox(QDir::toNativeSeparators(pathPicturesSaved));
    }

    settings.setValue("path_save_videos", directoryVideos);
    settings.setValue("path_save_pictures", directoryPictures);
//    connect(directoryPicturesComboBox->lineEdit(), &QLineEdit::returnPressed, this,
//            &P_SettingPlayBack::animateFindClick);

//    connect(directoryVideosComboBox->lineEdit(), &QLineEdit::returnPressed, this,
//            &P_SettingPlayBack::animateFindClick);


    QGridLayout *mainLayout = new QGridLayout(this->zone);
    mainLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    mainLayout->setSpacing(15);
    this->zone->setLayout(mainLayout);

    QLabel *directorySavePictureLabel = new QLabel(this->zone);
    directorySavePictureLabel->setText("Thư mục mặc định lưu hình:");
    directorySavePictureLabel->setFont(Resources::instance().getLargeRegularButtonFont());


    QLabel *directorySaveVideoLabel = new QLabel(this->zone);
    directorySaveVideoLabel->setText("Thư mục mặc định lưu video:");
    directorySaveVideoLabel->setFont(Resources::instance().getLargeRegularButtonFont());

    mainLayout->addWidget(directorySavePictureLabel, 0, 0);
    mainLayout->addWidget(directoryPicturesComboBox, 0, 1);
    mainLayout->addWidget(browseFolderPicturesButton, 0, 2);

    mainLayout->addWidget(directorySaveVideoLabel, 1, 0);
    mainLayout->addWidget(directoryVideosComboBox, 1, 1);
    mainLayout->addWidget(browseFolderVideosButton, 1, 2);


    connect(directoryPicturesComboBox, &QComboBox::currentTextChanged, this , &P_SettingPlayBack::directoryPicturesComboBoxChanged);
    connect(directoryVideosComboBox, &QComboBox::currentTextChanged, this , &P_SettingPlayBack::directoryVideosComboBoxChanged);

}

void P_SettingPlayBack::directoryPicturesComboBoxChanged(QString directoryPictures){
    QSettings settings;
    settings.setValue("path_save_pictures", directoryPictures);
    directoryPicturesComboBox->setCurrentText(directoryPictures);

}
void P_SettingPlayBack::directoryVideosComboBoxChanged(QString directoryVideos){
    QSettings settings;
    settings.setValue("path_save_videos", directoryVideos);
    directoryVideosComboBox->setCurrentText(directoryVideos);
}


QComboBox *P_SettingPlayBack::createComboBox(const QString &text) {
    QComboBox *comboBox = new QComboBox(this->zone);
    comboBox->setFont(Resources::instance().getLargeRegularButtonFont());
    comboBox->setEditable(false);
    comboBox->addItem(text);
    comboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    return comboBox;
}

void P_SettingPlayBack::animateFindClick() { findButton->animateClick(); }

void P_SettingPlayBack::browseFolderVideos() {
    QSettings settings;
    QString pathVideosSaved  = settings.value("path_save_videos").toString();

    //    QString path ="/mnt/hdd/videowallstores/Videos";
    QString choosesFolder = "Chooses folders";
    QString directory = QDir::toNativeSeparators(
                QFileDialog::getExistingDirectory(0, choosesFolder, pathVideosSaved));

    if (!directory.isEmpty()) {
        QFileInfo dirVideosNew(directory);
        if(dirVideosNew.isDir() && dirVideosNew.isWritable()){
            if (directoryVideosComboBox->findText(directory) == -1)
                directoryVideosComboBox->addItem(directory);
            directoryVideosComboBox->setCurrentIndex(directoryVideosComboBox->findText(directory));
            qDebug() << "path current " << directory;
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<QString>(directory);
            control()->newUserAction(Message.APP_PLAY_BACK_SET_PATH_SAVE_VIDEOS,
                                     dataStruct);
            settings.setValue("path_save_videos", directory);
        }else{
            QString pathVideosSaved = settings.value("path_save_videos").toString();
            directoryVideosComboBox->setCurrentText(pathVideosSaved);
            // Do something
            QMessageBox::warning(Q_NULLPTR, tr("Lỗi truy cập thư mục"), tr("Bạn không có quyền truy cập đọc và ghi tại \"%1\". Bạn cần cấp lại quyền truy cập thư mục này.")
                                 .arg(QDir::toNativeSeparators(directory)));
        }
    }
}

void P_SettingPlayBack::browseFolderPictures() {
    QSettings settings;
    QString pathVideosSaved  = settings.value("path_save_pictures").toString();
    //    QString path ="/mnt/hdd/videowallstores/Pictures";
    QString choosesFolder = "Chooses folders";
    QString directory = QDir::toNativeSeparators(
                QFileDialog::getExistingDirectory(0, choosesFolder, pathVideosSaved));
    if (!directory.isEmpty()) {
        QFileInfo dirVideosNew(directory);
        if(dirVideosNew.isDir() && dirVideosNew.isWritable()){
            if (directoryPicturesComboBox->findText(directory) == -1)
                directoryPicturesComboBox->addItem(directory);
            directoryPicturesComboBox->setCurrentIndex(directoryPicturesComboBox->findText(directory));
            qDebug() << "path current " << directory;
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<QString>(directory);
            control()->newUserAction(Message.APP_PLAY_BACK_SET_PATH_SAVE_PICTURES,
                                     dataStruct);
            settings.setValue("path_save_pictures", directory);
        }else{
            QString pathPicturesSaved = settings.value("path_save_pictures").toString();
            directoryPicturesComboBox->setCurrentText(pathPicturesSaved);
            // Do something
            QMessageBox::warning(Q_NULLPTR, tr("Lỗi truy cập thư mục"), tr("Bạn không có quyền truy cập đọc và ghi tại \"%1\". Bạn cần cấp lại quyền truy cập thư mục này.")
                                 .arg(QDir::toNativeSeparators(directory)));
        }
    }
}

QObject *P_SettingPlayBack::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}
