#include <QDirIterator>
#include <QStyledItemDelegate>
#include <QMapIterator>

#include "p_setting_clip.h"
#include "clipperitem.h"
#include "PlayBack/PBWorkSpace/p_pb_workspace.h"

/**
     * Generic method to override for updating the presention.
     **/

void clearWidgets(QLayout * layout) {
   if (! layout)
      return;
   while (auto item = layout->takeAt(0)) {
      delete item->widget();
      clearWidgets(item->layout());
   }
}

P_SettingClip::P_SettingClip(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    this->zone->setStyleSheet("color:#4c4b52");

    mainLayout = new QVBoxLayout;
    this->zone->setLayout(mainLayout);

    listWidget = new QListWidget;
    //listWidget->setFixedSize(800, 500);

    mainLayout->addWidget(listWidget);

    if(P_PBWorkSpace::mCommandQueue && P_PBWorkSpace::mCommandQueue->size() > 0){

        QMapIterator<long , ClipperCommand*> it(P_PBWorkSpace::mCommandQueue->getItems());
        while (it.hasNext()) {
            it.next();

            ClipperItem* clipperItem = new ClipperItem(it.value(), this->zone);
            clipperItemsList[it.key()] = clipperItem;

            QListWidgetItem* lwi = new QListWidgetItem;
            lwi->setSizeHint( clipperItem->sizeHint() );
            listWidget->addItem(lwi);
            listWidget->setItemWidget(lwi, clipperItem);
        }
    }

    QTimer* pollingTimer = new QTimer();
    connect(pollingTimer, &QTimer::timeout, this, &P_SettingClip::listFileUpdate);
    pollingTimer->start(1000);

    return;
}

void P_SettingClip::listFileUpdate() {

    QMap<long, ClipperItem*>::iterator itClipper;
    QMap<long,ClipperCommand*> listFileWidget = P_PBWorkSpace::mCommandQueue->getItems();

    if(P_PBWorkSpace::mCommandQueue && P_PBWorkSpace::mCommandQueue->size() > 0){

        QMapIterator<long , ClipperCommand*> it(listFileWidget);
        while (it.hasNext()) {
            it.next();

            itClipper = clipperItemsList.find(it.key());

            //If current list has no member like in Comman queue list, let create new one
            if(itClipper == clipperItemsList.end()) {

                ClipperItem* clipperItem = new ClipperItem(it.value(), this->zone);
                clipperItemsList[it.key()] = clipperItem;

                QListWidgetItem* lwi = new QListWidgetItem;
                lwi->setSizeHint( clipperItem->sizeHint() );
                listWidget->addItem(lwi);
                listWidget->setItemWidget(lwi, clipperItem);

            }
            else {

                //Or updating progress bar
                itClipper.value()->updateProgressBar( it.value());

            }
        }
    }

}


QComboBox *P_SettingClip::createComboBox(const QString &text) {
    QComboBox *comboBox = new QComboBox;
    comboBox->setEditable(true);
    comboBox->addItem(text);
    comboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    return comboBox;
}

void P_SettingClip::animateFindClick() { findButton->animateClick(); }



void P_SettingClip::browseFolderVideos() {
    QString path = QDir::homePath() + "/" + "MultimediaOfVideowall"+ "/" + "Videos";
    //    QString path ="/mnt/hdd/videowallstores/Videos";
    QString findFile = "Find files";
    QString directory = QDir::toNativeSeparators(
                QFileDialog::getExistingDirectory(this->zone, findFile, path));
    if (!directory.isEmpty()) {
        if (directoryVideosComboBox->findText(directory) == -1)
            directoryVideosComboBox->addItem(directory);
        directoryVideosComboBox->setCurrentIndex(directoryVideosComboBox->findText(directory));
        qDebug() << "path current " << directory;
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>(directory);
        control()->newUserAction(Message.APP_PLAY_BACK_SET_PATH_SAVE_VIDEOS,
                                 dataStruct);
    }
}

void P_SettingClip::browseFolderPictures() {
    QString path = QDir::homePath() + "/" + "MultimediaOfVideowall"+ "/" + "Pictures";
    //    QString path ="/mnt/hdd/videowallstores/Pictures";

    QString findFile = "Find files";
    QString directory = QDir::toNativeSeparators(
                QFileDialog::getExistingDirectory(this->zone, findFile, path));

    if (!directory.isEmpty()) {
        if (directoryPicturesComboBox->findText(directory) == -1)
            directoryPicturesComboBox->addItem(directory);
        directoryPicturesComboBox->setCurrentIndex(directoryPicturesComboBox->findText(directory));
        qDebug() << "path current " << directory;
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>(directory);
        control()->newUserAction(Message.APP_PLAY_BACK_SET_PATH_SAVE_PICTURES,
                                 dataStruct);
    }
}

QObject *P_SettingClip::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}
