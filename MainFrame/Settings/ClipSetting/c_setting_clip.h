#ifndef C_SettingClip_H
#define C_SettingClip_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "../c_settings.h"
#include "PacModel/control.h"
#include "a_setting_clip.h"
#include "message.h"
#include "p_setting_clip.h"
class C_Settings;
class P_SettingClip;
class A_SettingClip;
class C_SettingClip : public Control {
  QWidget* zone;

 public:
  AppContext* appContext;
  C_SettingClip(Control* ctrl, QWidget* zone);
  C_Settings* getParent() { return (C_Settings*)this->parent; }
  P_SettingClip* presentation() { return (P_SettingClip*)this->pres; }
  A_SettingClip* abstraction() { return (A_SettingClip*)this->abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
  AppContext* getAppContext() const;
  void setAppContext(AppContext* value);
};

#endif  // CONTROL_H
