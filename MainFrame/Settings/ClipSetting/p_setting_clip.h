#ifndef P_SETTINGCLIP_H
#define P_SETTINGCLIP_H

#include <PacModel/presentation.h>
#include <QAction>
#include <QComboBox>
#include <QDesktopWidget>
#include <QDialog>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QGridLayout>
#include <QLabel>
#include <QMenu>
#include <QMimeDatabase>
#include <QObject>
#include <QProgressDialog>
#include <QPushButton>
#include <QString>
#include <QStringList>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QWidget>
#include "../c_settings.h"
#include "c_setting_clip.h"
#include <QSettings>
#include <QListWidget>
#include <QListWidgetItem>
class C_SettingClip;
class C_Setting;
class ClipperItem;
class P_SettingClip : public Presentation {
  //  Q_OBJECT
 private:
  QWidget *zone;
 public:
  P_SettingClip(Control *ctrl, QWidget *zone);
  void update();
  C_SettingClip *control() { return (C_SettingClip *)ctrl; }
  QObject *getZone(int zoneId);

 private Q_SLOTS:
  void browseFolderPictures();
  void browseFolderVideos();

  void animateFindClick();
  void listFileUpdate();
  void onFileItemClicked(QListWidgetItem*);

 private:
  QComboBox *createComboBox(const QString &text = QString());
  QComboBox *fileComboBox;
  QComboBox *textComboBox;
  QComboBox *directoryPicturesComboBox;
  QComboBox *directoryVideosComboBox;

  QLabel *filesFoundLabel;
  QPushButton *findButton;
  QTableWidget *filesTable;

  QDir currentDir;

  QVBoxLayout* mainLayout;
  QListWidget* listWidget;

  QMap<long, ClipperItem*>  clipperItemsList;

};

#endif  // P_SETTINGCLIP_H
