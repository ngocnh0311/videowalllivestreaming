#ifndef A_SETTINGCLIP_H
#define A_SETTINGCLIP_H

#include <QObject>
#include "PacModel/control.h"
#include "c_setting_clip.h"

class C_SettingClip;
class A_SettingClip : public Abstraction {
  Q_OBJECT
 private:
 public:
  A_SettingClip(Control *ctrl);
  C_SettingClip *control() { return (C_SettingClip *)this->ctrl; }
  void changeControl(Control *ctrl);
};

#endif  // A_SETTINGCLIP_H
