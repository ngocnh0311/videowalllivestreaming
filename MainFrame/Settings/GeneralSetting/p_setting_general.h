#ifndef P_SETTINGGENERAL_H
#define P_SETTINGGENERAL_H

#include <PacModel/presentation.h>
#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QVBoxLayout>
#include <QWidget>
#include "../c_settings.h"
#include "Site/site.h"
#include "c_setting_general.h"
#include <QSettings>
#include <QRadioButton>
#include <QDebug>
class C_SettingGeneral;
class P_SettingGeneral : public Presentation {
    //  Q_OBJECT
private:
    QWidget *zone;

public:
    P_SettingGeneral(Control *ctrl, QWidget *zone);
    C_SettingGeneral *control() { return (C_SettingGeneral *)this->ctrl; }
    QLabel *siteWorkingLabel;
    QLabel *appVersionLabel;
    QLineEdit *workingSiteLineEdit;
    QLineEdit *versionLineEdit;
    //selected cdn
    QRadioButton *cdnRadioButton;
    QRadioButton *nvrRadioButton;
    QRadioButton *camRadioButton;
    QLabel *streamLabel;


    //mode debug
    QRadioButton *debugOnButton;
    QRadioButton *debugOffButton;
    QLabel *debugLabel;

    //Offline mode
    QRadioButton *onlineRadioButton;
    QRadioButton *offlineRadioButton;
    QLabel *offlineModeLabel;

    //use free space
    QRadioButton *onUseFreeSpaceRadioButton;
    QRadioButton *offUseFreeSpaceRadioButton;
    QLabel *useFreeSpaceLabel;


    //auto update
    QLabel *autoUpdateLabel;
    QRadioButton *onAutoUpdateRadioButton = Q_NULLPTR;
    QRadioButton *offAutoUpdateRadioButton = Q_NULLPTR;

    //windows list update
    QLabel *windowsListLabel;
    QRadioButton *showWindowsListRadioButton = Q_NULLPTR;
    QRadioButton *hideWindowsListRadioButton = Q_NULLPTR;

    void updateWorkingSite(Site *site);
    QObject *getZone(int zoneId);
    void onUpdateStateAutoUpDateVersion();

public Q_SLOTS:
    void cdnSelected(bool checked);
    void nvrSelected(bool checked);
    void camSelected(bool checked);

    void debugTurnOn(bool checked);
    void debugTurnOff(bool checked);
    void onlineModeSelected(bool checked);
    void offlineModeSelected(bool checked);

    void onUseFreeSpaceSelected(bool checked);
    void offUseFreeSpaceSelected(bool checked);
    void onAutoUpdateSelected(bool checked);
    void offAutoUpdateSelected(bool checked);

    void onShowWindowsListSelected(bool checked);
    void onHideWindowsListSelected(bool checked);
};

#endif  // P_SETTINGGENERAL_H
