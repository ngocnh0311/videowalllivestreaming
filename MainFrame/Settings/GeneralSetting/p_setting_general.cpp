#include "p_setting_general.h"

/**
     * Generic method to override for updating the presention.
     **/

P_SettingGeneral::P_SettingGeneral(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    this->zone->setStyleSheet("color:#4c4b52");

    QVBoxLayout *mainLayout = new QVBoxLayout();
    this->zone->setLayout(mainLayout);
    QWidget *gridWidget = new QWidget(this->zone);
    mainLayout->addWidget(gridWidget);
    mainLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);

    QGridLayout *gridLayout = new QGridLayout();
    gridLayout->setSpacing(5);
    gridLayout->setMargin(0);
    gridWidget->setLayout(gridLayout);

    //site current
    siteWorkingLabel = new QLabel(gridWidget);
    siteWorkingLabel->setFont(Resources::instance().getLargeRegularButtonFont());
    siteWorkingLabel->setText("Site hiện tại : ");

    QSettings settings;
    //settings debug
    QString modeDebugDefault = control()->appContext->getIsModeDebug();
    QString modeDebugSave = settings.value("mode_debug", modeDebugDefault).toString();
    if(modeDebugSave == "ON" || modeDebugSave == "OFF") { modeDebugDefault = modeDebugSave;}

    //save mode debug
    settings.setValue("mode_debug", modeDebugDefault);

    //data source camera
    QString dataSourceDefault = control()->appContext->getNetworkType().dataSource;
    QString dataSourceCameraSaved = settings.value("data_source_camera", dataSourceDefault).toString();
    QString protocolCameraDefault;
    QString networkCameraDefault;

    if(dataSourceCameraSaved == dataSourceCamera.CAM || dataSourceCameraSaved == dataSourceCamera.NVR || dataSourceCameraSaved == dataSourceCamera.CDN) {dataSourceDefault = dataSourceCameraSaved;}

    if(dataSourceDefault == dataSourceCamera.CDN || dataSourceDefault == dataSourceCamera.NVR){
        protocolCameraDefault = protocolCamera.WS;
    }

    if(dataSourceDefault == dataSourceCamera.CAM){
        protocolCameraDefault = protocolCamera.RTSP;
    }

    if(dataSourceDefault == dataSourceCamera.NVR) networkCameraDefault = networkCamera.LAN;
    if(dataSourceDefault == dataSourceCamera.CAM) networkCameraDefault = networkCamera.LAN;
    if(dataSourceDefault == dataSourceCamera.CDN) networkCameraDefault = networkCamera.CDN;

    control()->appContext->getNetworkType().network = networkCameraDefault;
    control()->appContext->getNetworkType().protocol = protocolCameraDefault;
    control()->appContext->getNetworkType().dataSource = dataSourceDefault;

    settings.setValue("protocol_type", protocolCameraDefault);
    settings.setValue("network_type", networkCameraDefault);
    settings.setValue("data_source_camera", dataSourceDefault);


    //offline mode
    QString offlineModeDefault = control()->appContext->getOfflineMode();
    QString offlineModeSaved = settings.value("offline_mode", offlineModeDefault).toString();

    if(offlineModeSaved == "ON" || offlineModeSaved == "OFF") {offlineModeDefault = offlineModeSaved;}

    //save offline mode
    settings.setValue("offline_mode", offlineModeDefault);

    //use free space
    QString useFreeSpaceDefault = control()->appContext->getUseFreeSpace();
    QString useFreeSpaceSave = settings.value("use_free_space", useFreeSpaceDefault).toString();
    if(useFreeSpaceSave == "ON" || useFreeSpaceSave == "OFF") {useFreeSpaceDefault = useFreeSpaceSave;}

    //save use free space
    settings.setValue("use_free_space", useFreeSpaceDefault);


    //update new version
    QString isAutoUpdateNewVersionDefault = this->control()->appContext->getIsAutoUpdateNewVersion();
    QString isAutoUpdateNewVersionSave =  settings.value("auto_update_version" , isAutoUpdateNewVersionDefault).toString();

    if(isAutoUpdateNewVersionSave == "ON" || isAutoUpdateNewVersionSave == "OFF"){
        isAutoUpdateNewVersionDefault = isAutoUpdateNewVersionSave;
    }

    //save  show windows list
    settings.setValue("auto_update_version", isAutoUpdateNewVersionDefault);

    //show windows list
    QString isShowWindowsListDefault = this->control()->appContext->getIsShowWindowsList();
    QString isShowWindowsListSave = settings.value("show_windows_list", isShowWindowsListDefault).toString();
    if(isShowWindowsListSave == "HIDE" || isShowWindowsListSave == "SHOW"){
        isShowWindowsListDefault = isShowWindowsListSave;
    }

    settings.beginGroup(QString::number(control()->getParent()->getAppContext()->getWorkingUser()->getUserId()));
    QString nameSite = settings.value("site_name").toString();

    workingSiteLineEdit = new QLineEdit(gridWidget);
    workingSiteLineEdit->setFont(Resources::instance().getLargeRegularButtonFont());
    workingSiteLineEdit->setEnabled(false);
    workingSiteLineEdit->setFont(
                Resources::instance().getLargeRegularButtonFont());
    workingSiteLineEdit->setText(nameSite);

    gridLayout->addWidget(siteWorkingLabel,0,0);
    gridLayout->addWidget(workingSiteLineEdit,0,1);

    //app version current

    //    appVersionLabel = new QLabel(gridWidget);
    //    appVersionLabel->setText("Phiên bản hiện tại: ");
    //    gridLayout->addWidget(appVersionLabel, 1,0);
    //    versionLineEdit = new QLineEdit(gridWidget);
    //    versionLineEdit->setText("Videowall " + appVersion);
    //    gridLayout->addWidget(versionLineEdit,1,1);

    streamLabel = new QLabel(gridWidget);
    streamLabel->setFont(Resources::instance().getLargeRegularButtonFont());
    streamLabel->setText("Chọn luồng : ");
    gridLayout->addWidget(streamLabel, 1,0);
    //select protocol
    QGroupBox *groupBoxStream = new QGroupBox();
    groupBoxStream->setFixedHeight(30);
    groupBoxStream->setMaximumWidth(429);
    cdnRadioButton = new QRadioButton(tr("CDN"));
    nvrRadioButton = new QRadioButton(tr("NVR"));
    camRadioButton = new QRadioButton(tr("CAM"));

    cdnRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());
    nvrRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());
    camRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());

    QHBoxLayout *layoutStream = new QHBoxLayout();
    layoutStream->setContentsMargins(30,0,20,0);

    groupBoxStream->setLayout(layoutStream);
    layoutStream->addWidget(camRadioButton);
    layoutStream->addWidget(nvrRadioButton);
    layoutStream->addWidget(cdnRadioButton);
    gridLayout->addWidget(groupBoxStream,1,1);

    //update cdn state
    if(dataSourceDefault == dataSourceCamera.CDN){
        cdnRadioButton->setChecked(true);
        nvrRadioButton->setChecked(false);
        camRadioButton->setChecked(false);
    }else if(dataSourceDefault == dataSourceCamera.NVR){
        cdnRadioButton->setChecked(false);
        nvrRadioButton->setChecked(true);
        camRadioButton->setChecked(false);
    } if(dataSourceDefault == dataSourceCamera.CAM){
        cdnRadioButton->setChecked(false);
        nvrRadioButton->setChecked(false);
        camRadioButton->setChecked(true);
    }
    connect(cdnRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::cdnSelected);
    connect(nvrRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::nvrSelected);
    connect(camRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::camSelected);


    //debug mode
    debugLabel = new QLabel(gridWidget);
    debugLabel->setFont(Resources::instance().getLargeRegularButtonFont());
    debugLabel->setText("Debug: ");
    gridLayout->addWidget(debugLabel, 2,0);
    //select protocol
    QGroupBox *groupBoxDebug = new QGroupBox();
    groupBoxDebug->setFixedHeight(30);
    groupBoxDebug->setMaximumWidth(300);
    debugOnButton = new QRadioButton(tr("ON"));
    debugOffButton = new QRadioButton(tr("OFF"));

    debugOnButton->setFont(Resources::instance().getLargeRegularButtonFont());
    debugOffButton->setFont(Resources::instance().getLargeRegularButtonFont());

    QHBoxLayout *layoutDebug = new QHBoxLayout();
    layoutDebug->setContentsMargins(30,0,20,0);

    groupBoxDebug->setLayout(layoutDebug);
    layoutDebug->addWidget(debugOnButton);
    layoutDebug->addWidget(debugOffButton);
    gridLayout->addWidget(groupBoxDebug,2,1);
    //update value
    if(modeDebugDefault == "ON"){
        debugOnButton->setChecked(true);
    }else if(modeDebugDefault == "OFF"){
        debugOffButton->setChecked(true);
    }

    connect(debugOnButton, &QRadioButton::clicked, this, &P_SettingGeneral::debugTurnOn);
    connect(debugOffButton, &QRadioButton::clicked, this, &P_SettingGeneral::debugTurnOff);

    //offline mode
    offlineModeLabel = new QLabel(gridWidget);
    offlineModeLabel->setFont(Resources::instance().getLargeRegularButtonFont());

    offlineModeLabel->setText("Chế độ chạy độc lập :");
    gridLayout->addWidget(offlineModeLabel, 3,0);

    //select protocol
    QGroupBox *groupBoxOfflineMode = new QGroupBox(gridWidget);
    groupBoxOfflineMode->setFixedHeight(30);
    groupBoxOfflineMode->setMaximumWidth(300);
    onlineRadioButton = new QRadioButton(tr("ON"));
    offlineRadioButton = new QRadioButton(tr("OFF"));

    onlineRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());
    offlineRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());

    QHBoxLayout *layoutOfflineMode = new QHBoxLayout();
    layoutOfflineMode->setContentsMargins(30,0,20,0);

    groupBoxOfflineMode->setLayout(layoutOfflineMode);
    layoutOfflineMode->addWidget(onlineRadioButton);
    layoutOfflineMode->addWidget(offlineRadioButton);
    gridLayout->addWidget(groupBoxOfflineMode,3,1);
    //update offline mode
    if(offlineModeDefault == "ON"){
        onlineRadioButton->setChecked(true);
        control()->appContext->setIsModeDebug("ON");
    }else if(offlineModeDefault == "OFF"){
        offlineRadioButton->setChecked(true);
        control()->appContext->setIsModeDebug("OFF");
    }
    connect(onlineRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::onlineModeSelected);
    connect(offlineRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::offlineModeSelected);

    //Tận dụng khoảng trống

    //offline mode
    useFreeSpaceLabel = new QLabel(gridWidget);
    useFreeSpaceLabel->setFont(Resources::instance().getLargeRegularButtonFont());

    useFreeSpaceLabel->setText("Tận dụng khoảng trống: ");
    gridLayout->addWidget(useFreeSpaceLabel, 4,0);


    QGroupBox *groupBoxUseFreeSpace= new QGroupBox(gridWidget);
    groupBoxUseFreeSpace->setFixedHeight(30);
    groupBoxUseFreeSpace->setMaximumWidth(300);
    onUseFreeSpaceRadioButton = new QRadioButton(tr("ON"));
    offUseFreeSpaceRadioButton = new QRadioButton(tr("OFF"));

    onUseFreeSpaceRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());
    offUseFreeSpaceRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());

    QHBoxLayout *layoutUseFreeSpace = new QHBoxLayout();
    layoutUseFreeSpace->setContentsMargins(30,0,20,0);

    groupBoxUseFreeSpace->setLayout(layoutUseFreeSpace);
    layoutUseFreeSpace->addWidget(onUseFreeSpaceRadioButton);
    layoutUseFreeSpace->addWidget(offUseFreeSpaceRadioButton);
    gridLayout->addWidget(groupBoxUseFreeSpace,4,1);
    //update sate offline mode
    if(useFreeSpaceDefault == "ON"){
        onUseFreeSpaceRadioButton->setChecked(true);
        control()->appContext->setUseFreeSpace("ON");
    }else if(useFreeSpaceDefault == "OFF"){
        offUseFreeSpaceRadioButton->setChecked(true);
        control()->appContext->setUseFreeSpace("OFF");
    }

    connect(onUseFreeSpaceRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::onUseFreeSpaceSelected);
    connect(offUseFreeSpaceRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::offUseFreeSpaceSelected);

    // Auto update
    autoUpdateLabel = new QLabel(gridWidget);
    autoUpdateLabel->setFont(Resources::instance().getLargeRegularButtonFont());

    autoUpdateLabel->setText("Tự động cập nhật phiên bản mới: ");
    gridLayout->addWidget(autoUpdateLabel,5,0);

    QGroupBox *groupBoxAutoUpdate = new QGroupBox(gridWidget);
    groupBoxAutoUpdate->setFixedHeight(30);
    groupBoxAutoUpdate->setMaximumWidth(300);
    onAutoUpdateRadioButton = new QRadioButton(tr("ON"));
    offAutoUpdateRadioButton = new QRadioButton(tr("OFF"));

    onAutoUpdateRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());
    offAutoUpdateRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());


    QHBoxLayout *layoutAutoUpdate = new QHBoxLayout();
    layoutAutoUpdate->setContentsMargins(30,0,20,0);
    groupBoxAutoUpdate->setLayout(layoutAutoUpdate);
    layoutAutoUpdate->addWidget(onAutoUpdateRadioButton);
    layoutAutoUpdate->addWidget(offAutoUpdateRadioButton);
    gridLayout->addWidget(groupBoxAutoUpdate,5,1);

    //update sate auto update
    if(isAutoUpdateNewVersionDefault == "ON"){
        onAutoUpdateRadioButton->setChecked(true);
        control()->appContext->setIsAutoUpdateNewVersion("ON");
    }else if(isAutoUpdateNewVersionDefault == "OFF"){
        offAutoUpdateRadioButton->setChecked(true);
        control()->appContext->setIsAutoUpdateNewVersion("OFF");
    }
    connect(onAutoUpdateRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::onAutoUpdateSelected);
    connect(offAutoUpdateRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::offAutoUpdateSelected);


    //    // windows list update
    windowsListLabel = new QLabel(gridWidget);
    windowsListLabel->setFont(Resources::instance().getLargeRegularButtonFont());

    windowsListLabel->setText("Multi Windows: ");
    gridLayout->addWidget(windowsListLabel,6,0);

    QGroupBox *groupBoxWindowsList = new QGroupBox(gridWidget);
    groupBoxWindowsList->setFixedHeight(30);
    groupBoxWindowsList->setMaximumWidth(300);
    showWindowsListRadioButton = new QRadioButton(tr("SHOW"));
    hideWindowsListRadioButton = new QRadioButton(tr("HIDE"));

    showWindowsListRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());
    hideWindowsListRadioButton->setFont(Resources::instance().getLargeRegularButtonFont());

    QHBoxLayout *layoutWindowsList = new QHBoxLayout();
    layoutWindowsList->setContentsMargins(30,0,20,0);
    groupBoxWindowsList->setLayout(layoutWindowsList);
    layoutWindowsList->addWidget(showWindowsListRadioButton);
    layoutWindowsList->addWidget(hideWindowsListRadioButton);
    gridLayout->addWidget(groupBoxWindowsList,6,1);
    //update windows list
    if(isShowWindowsListDefault == "SHOW"){
        showWindowsListRadioButton->setChecked(true);
        hideWindowsListRadioButton->setChecked(false);

        control()->appContext->setIsShowWindowsList("SHOW");
    }else if(isShowWindowsListDefault == "HIDE"){
        hideWindowsListRadioButton->setChecked(true);
        showWindowsListRadioButton->setChecked(false);
        control()->appContext->setIsShowWindowsList("HIDE");
    }
    connect(showWindowsListRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::onShowWindowsListSelected);
    connect(hideWindowsListRadioButton, &QRadioButton::clicked, this, &P_SettingGeneral::onHideWindowsListSelected);

}

void P_SettingGeneral::onShowWindowsListSelected(bool checked){
    if(checked && control()->appContext->getIsShowWindowsList() != "SHOW"){
        showWindowsListRadioButton->setChecked(true);
        hideWindowsListRadioButton->setChecked(false);

        int numberScreen = QApplication::desktop()->numScreens();
        if(numberScreen > 1){
            QMessageBox messageBoxDelete;
            messageBoxDelete.setFixedSize(QSize(600, 120));
            messageBoxDelete.setIcon(QMessageBox::Warning);
            messageBoxDelete.setText("Ứng dụng sẽ chạy lại để thực hiện yêu cầu.\n"
                                     "Bạn có muốn tiếp tục?");
            messageBoxDelete.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
            messageBoxDelete.setDefaultButton(QMessageBox::Cancel);
            int ret = messageBoxDelete.exec();
            switch (ret) {
            case QMessageBox::Ok:{
                control()->appContext->setIsShowWindowsList("SHOW");
                QSettings settings;
                settings.setValue("show_windows_list", "SHOW");
                QVariant *dataStruct = new QVariant();
                dataStruct->setValue<QString>("SHOW");
                control()->newUserAction(Message.UPDATE_STATE_SHOW_WINDOWS_LIST, dataStruct);
            }break;
            case QMessageBox::Cancel:{
                showWindowsListRadioButton->setChecked(false);
                hideWindowsListRadioButton->setChecked(true);
            }break;
            default:
                break;
            }
        }else{
            control()->appContext->setIsShowWindowsList("HIDE");
            QSettings settings;
            settings.setValue("show_windows_list", "HIDE");

            //can not show windows list
            QMessageBox messageBoxDelete;
            messageBoxDelete.setFixedSize(QSize(600, 120));
            messageBoxDelete.setIcon(QMessageBox::Warning);
            messageBoxDelete.setText("Số lượng màn hình phải lớn hơn 1.\n"
                                     "Để có thể sử dụng chức năng này!");
            messageBoxDelete.setStandardButtons(QMessageBox::Ok);
            messageBoxDelete.setDefaultButton(QMessageBox::Ok);
            int ret = messageBoxDelete.exec();
            switch (ret) {
            case QMessageBox::Ok:{
                showWindowsListRadioButton->setChecked(false);
                hideWindowsListRadioButton->setChecked(true);
            }break;
            default:
                break;
            }
        }
    }
}

void P_SettingGeneral::onHideWindowsListSelected(bool checked){
    if(checked && control()->appContext->getIsShowWindowsList() == "SHOW"){
        showWindowsListRadioButton->setChecked(false);
        hideWindowsListRadioButton->setChecked(true);
        QMessageBox messageBoxDelete;
        messageBoxDelete.setFixedSize(QSize(600, 120));
        messageBoxDelete.setIcon(QMessageBox::Warning);
        messageBoxDelete.setText("Ứng dụng sẽ chạy lại để thực hiện yêu cầu.\n"
                                 "Bạn có muốn tiếp tục?");
        messageBoxDelete.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
        messageBoxDelete.setDefaultButton(QMessageBox::Cancel);
        int ret = messageBoxDelete.exec();
        switch (ret) {
        case QMessageBox::Ok:{
            control()->appContext->setIsShowWindowsList("HIDE");
            QSettings settings;
            settings.setValue("show_windows_list", "HIDE");
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<QString>("HIDE");
            control()->newUserAction(Message.UPDATE_STATE_SHOW_WINDOWS_LIST, dataStruct);
        }break;
        case QMessageBox::Cancel:{
            showWindowsListRadioButton->setChecked(true);
            hideWindowsListRadioButton->setChecked(false);
        }break;
        default:
            break;
        }
    }
}

void P_SettingGeneral::onAutoUpdateSelected(bool checked){
    if(checked){
        QSettings settings;
        this->control()->appContext->setIsAutoUpdateNewVersion("ON");
        settings.setValue("auto_update_version", "ON");
        control()->newUserAction(Message.AUTO_UPDATE_VERSION , Q_NULLPTR);
    }
}

void P_SettingGeneral::offAutoUpdateSelected(bool checked){
    if(checked){
        QSettings settings;
        this->control()->appContext->setIsAutoUpdateNewVersion("OFF");
        settings.setValue("auto_update_version", "OFF");
        control()->newUserAction(Message.AUTO_UPDATE_VERSION , Q_NULLPTR);
    }
}

void P_SettingGeneral::onUpdateStateAutoUpDateVersion(){
    QSettings settings;
    QString value = settings.value("auto_update_version").toString();
    if(value == "ON"){
        onAutoUpdateRadioButton->setChecked(true);
    }else if(value == "OFF"){
        offAutoUpdateRadioButton->setChecked(true);
    }

}

void P_SettingGeneral::onUseFreeSpaceSelected(bool checked){
    if(checked){
        control()->appContext->setUseFreeSpace("ON");
        QSettings settings;
        settings.setValue("use_free_space", "ON");
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>("ON");
        control()->newUserAction(Message.UPDATE_STATE_USE_FREE_SPACE, dataStruct);
    }
}

void P_SettingGeneral::offUseFreeSpaceSelected(bool checked){
    if(checked){
        control()->appContext->setUseFreeSpace("OFF");
        QSettings settings;
        settings.setValue("use_free_space", "OFF");
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>("OFF");
        control()->newUserAction(Message.UPDATE_STATE_USE_FREE_SPACE, dataStruct);
    }
}

void P_SettingGeneral::debugTurnOn(bool checked){
    if(checked){
        control()->appContext->setIsModeDebug("ON");
        QSettings settings;
        settings.setValue("mode_debug", "ON");
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>("ON");
        control()->newUserAction(Message.UPDATE_MODE_RENDER_DEBUG, dataStruct);
    }
}

void P_SettingGeneral::debugTurnOff(bool checked){
    if(checked){
        control()->appContext->setIsModeDebug("OFF");
        QSettings settings;
        settings.setValue("mode_debug", "OFF");
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>("OFF");
        control()->newUserAction(Message.UPDATE_MODE_RENDER_DEBUG, dataStruct);
    }
}

void P_SettingGeneral::onlineModeSelected(bool checked){
    if(checked && control()->appContext->getOfflineMode() != "ON"){
        onlineRadioButton->setChecked(true);
        offlineRadioButton->setChecked(false);

        QMessageBox messageBoxDelete;
        messageBoxDelete.setFixedSize(QSize(600, 120));
        messageBoxDelete.setIcon(QMessageBox::Warning);
        messageBoxDelete.setText("Ứng dụng sẽ chạy lại để thực hiện yêu cầu.\n"
                                 "Bạn có muốn tiếp tục?");
        messageBoxDelete.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
        messageBoxDelete.setDefaultButton(QMessageBox::Cancel);
        int ret = messageBoxDelete.exec();
        switch (ret) {
        case QMessageBox::Ok:{
            control()->appContext->setOfflineMode("ON");
            QSettings settings;
            settings.setValue("offline_mode", "ON");
            control()->newUserAction(Message.TURN_ON_OFFLINE_MODE, Q_NULLPTR);
        }break;
        case QMessageBox::Cancel:{
            offlineRadioButton->setChecked(true);
            onlineRadioButton->setChecked(false);
        }break;
        default:
            break;
        }
    }
}
void P_SettingGeneral::offlineModeSelected(bool checked){
    if(checked && control()->appContext->getOfflineMode() == "ON"){
        offlineRadioButton->setChecked(true);
        onlineRadioButton->setChecked(false);

        QMessageBox messageBoxDelete;
        messageBoxDelete.setFixedSize(QSize(600, 120));
        messageBoxDelete.setIcon(QMessageBox::Warning);
        messageBoxDelete.setText("Ứng dụng sẽ chạy lại để thực hiện yêu cầu.\n"
                                 "Bạn có muốn tiếp tục?");
        messageBoxDelete.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
        messageBoxDelete.setDefaultButton(QMessageBox::Cancel);
        int ret = messageBoxDelete.exec();
        switch (ret) {
        case QMessageBox::Ok:{
            control()->appContext->setOfflineMode("OFF");
            QSettings settings;
            settings.setValue("offline_mode", "OFF");
            control()->newUserAction(Message.TURN_OFF_OFFLINE_MODE, Q_NULLPTR);
        }break;
        case QMessageBox::Cancel:{
            offlineRadioButton->setChecked(false);
            onlineRadioButton->setChecked(true);
        }break;
        default:
            break;
        }
    }
}



void P_SettingGeneral::cdnSelected(bool checked){
    QSettings settings;
    control()->appContext->getNetworkType().protocol = protocolCamera.WS;
    QString dataSourceSaved = settings.value("data_source_camera").toString();
    if(checked && dataSourceSaved != dataSourceCamera.CDN){
        control()->appContext->getNetworkType().network = networkCamera.CDN;
        CamItemType type;
        type.network = networkCamera.CDN;
        type.protocol = protocolCamera.WS;
        type.dataSource = dataSourceCamera.CDN;
        settings.setValue("protocol_type",type.protocol);
        settings.setValue("network_type", type.network);
        settings.setValue("data_source_camera", type.dataSource);
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<CamItemType>(type);
        control()->newUserAction(Message.UPDATE_CDN_TYPE_SELECTED, dataStruct);
    }
}

void P_SettingGeneral::nvrSelected(bool checked){
    QSettings settings;
    control()->appContext->getNetworkType().protocol = protocolCamera.WS;
    QString dataSourceSaved = settings.value("data_source_camera").toString();
    if(checked && dataSourceSaved != dataSourceCamera.NVR){
        control()->appContext->getNetworkType().network = networkCamera.LAN;
        CamItemType type;
        type.network = networkCamera.LAN;
        type.protocol = protocolCamera.WS;
        type.dataSource = dataSourceCamera.NVR;
        settings.setValue("protocol_type",type.protocol);
        settings.setValue("network_type", type.network);
        settings.setValue("data_source_camera", type.dataSource);

        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<CamItemType>(type);
        control()->newUserAction(Message.UPDATE_CDN_TYPE_SELECTED, dataStruct);
    }
}

void P_SettingGeneral::camSelected(bool checked){
    QSettings settings;
    control()->appContext->getNetworkType().protocol = protocolCamera.RTSP;
    QString dataSourceSaved = settings.value("data_source_camera").toString();
    if(checked && dataSourceSaved != dataSourceCamera.CAM){
        control()->appContext->getNetworkType().network = networkCamera.CDN;
        CamItemType type;
        type.network = networkCamera.LAN; //if network == cam is LAN
        type.protocol = protocolCamera.RTSP;
        type.dataSource = networkCamera.CAM;
        settings.setValue("protocol_type",type.protocol);
        settings.setValue("network_type", type.network);
        settings.setValue("data_source_camera", type.dataSource);

        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<CamItemType>(type);
        control()->newUserAction(Message.UPDATE_CDN_TYPE_SELECTED, dataStruct);
    }
}


QObject *P_SettingGeneral::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}

void P_SettingGeneral::updateWorkingSite(Site *site) {
    workingSiteLineEdit->setText(site->getSiteName());
}
