#include "c_mainframe.h"
#include "CamerasManager/c_camarasmanager.h"

#include "OnvifClient/OnvifLib/Device/device.h"

Q_DECLARE_METATYPE(QList<OnvifDevice*>)
/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
AppContext *C_MainFrame::getAppContext() { return appContext; }

void C_MainFrame::setAppContext(AppContext *value) { appContext = value; }

void C_MainFrame::updateSiteNewForSensorCameraManager(Site *workingSite){
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<Site *>(workingSite);
    cSensorCameraManager->newAction(Message.APP_UPDATE_WORKING_SITE, dataStruct);
}

void C_MainFrame::saveVideowallPid(){
    if(AppProfile::getAppProfile() != Q_NULLPTR){
        QString forderVideowallPid = AppProfile::getAppProfile()->getAppConfig()->getPathVideowallPid();
        QString charLast = forderVideowallPid.mid(forderVideowallPid.length() - 1, 1);
        QString pathVideowallPidAbsolution =  charLast == "/" ? forderVideowallPid.remove(forderVideowallPid.length() - 1, 1) : forderVideowallPid;
        if(!QDir(pathVideowallPidAbsolution).exists()){
            QDir().mkdir(pathVideowallPidAbsolution);
        }
        QString fileVideowallPidPath = pathVideowallPidAbsolution + "/VideoWall.AppImage.pid";
        QFile fileVideowallPid(fileVideowallPidPath);
        if(!fileVideowallPid.setPermissions(QFile::ReadOwner|QFile::WriteOwner|QFile::ExeOwner|QFile::ReadGroup|QFile::ExeGroup|QFile::ReadOther|QFile::ExeOther))
        {
            qDebug() << Q_FUNC_INFO << "Something wrong Permissions Of File!";
        }
        QString pidIdString = QString::number(QApplication::QCoreApplication::applicationPid());
        if(fileVideowallPid.open(QIODevice::WriteOnly | QIODevice::Truncate)){
            QTextStream ts(&fileVideowallPid);
            ts << pidIdString << endl;
            fileVideowallPid.close();
        }else{
            qDebug() <<Q_FUNC_INFO << "Can't not open file videowallPID";
        }
    }
}
void C_MainFrame::openMessagebBox(const QString &text)
{
    qDebug() << Q_FUNC_INFO << QThread::currentThread();
//    QMessageBox Msgbox;
//    Msgbox.setText(text);
//    Msgbox.exec();
}

C_MainFrame::C_MainFrame(Control *ctrl, QWidget *zone) : Control(ctrl) {
    this->zone = zone;
    QSettings settings;
    appContext = new AppContext();

//    qRegisterMetaType<QList<OnvifDevice*>>();
//    QList<OnvifDevice *> listOnvifDevices;

//    OnvifUtils *onvif = OnvifUtils::instances();
//    QThread *threadLoadOnvifDevice = new QThread;
//    onvif->moveToThread(threadLoadOnvifDevice);
//    threadLoadOnvifDevice->setObjectName("THREADONVIFDEVICE");
//    threadLoadOnvifDevice->start();
//    connect(this, &C_MainFrame::discoverIpDevicesInNetwork, onvif, &OnvifUtils::onStatDiscoverIpDevicesInNetwork);

//    connect(this , &C_MainFrame::startDiscoverIpCamera, onvif ,&OnvifUtils::loadInfoOfCameras);
//    connect(this , &C_MainFrame::startDiscoverBlackListDevice, onvif ,&OnvifUtils::loadInfoOfBlackListDevices);
//    connect(onvif , &OnvifUtils::notificationError, this ,&C_MainFrame::openMessagebBox);

    //    QDialog dialogWaitDiscover;
    //    dialogWaitDiscover.exec();
    //    connect(onvif, &OnvifUtils::loadFinished, &dialogWaitDiscover , &QDialog::close);

    //    OnvifUtils::instances().loadInfoOfCameras(listOnvifDevices);


    //    connect(OnvifUtils::instances(), &OnvifUtils::loadFinished,this, [&threadLoadOnvifDevice]() {
    //        threadLoadOnvifDevice->quit();
    //        threadLoadOnvifDevice->wait();
    //    });

//    settings.setValue("device_type", "B4");
//    settings.setValue("offline_mode", "ON");

//    //start show dialog discover ip devices in network
//    startDiscoverIpDevicesInNetwork();

    //    if(timerWritePidVideowall == Q_NULLPTR){
    //        timerWritePidVideowall = new QTimer;
    //        timerWritePidVideowall->start(5000);
    //        connect(timerWritePidVideowall, &QTimer::timeout , this, &C_MainFrame::saveVideowallPid);
    //    }


    int numberScreen = QApplication::desktop()->numScreens();
    if(numberScreen <= 1){
        settings.setValue("show_windows_list", "HIDE");
        appContext->setIsShowWindowsList("HIDE");
    }

    User *user;

    //user free space

    QString useFreeSpaceSaved = settings.value("use_free_space").toString();
    if(useFreeSpaceSaved.isEmpty()){
        //first defaul if setting use free space not exits
        settings.setValue("use_free_space", this->appContext->getUseFreeSpace());
    }else{
        this->appContext->setUseFreeSpace(useFreeSpaceSaved);
    }

    QString offlineModeDefault = appContext->getOfflineMode();
    QString offlineModeSaved = settings.value("offline_mode").toString();
    if(offlineModeSaved == "ON" || offlineModeSaved == "OFF") {
        offlineModeDefault = offlineModeSaved;
        appContext->setOfflineMode(offlineModeSaved);
    }
    if(offlineModeSaved == "ON"){
        UserInfo userInfo = User::loadUserInfo();
        user = User::getUserFrom(userInfo);
        if(user == Q_NULLPTR){
            user = User::createUserDefault();
        }
    }else{
        cLogin = new C_Login(this, Q_NULLPTR);
        QVariant *dataStruct = new QVariant();
        this->newAction(Message.APP_LOGIN_SHOW, dataStruct);
        user = dataStruct->value<User *>();  // login failed then quit
    }

    if (user == Q_NULLPTR) {
        qApp->quit();
        QProcess process;
        process.execute(QString("kill %1").arg(
                            QApplication::QCoreApplication::applicationPid()));
    } else {
        //init sensor camera manager
        cSensorCameraManager = new  C_SensorCameraManager(this, Q_NULLPTR);

        //cdn type default
        CamItemType camTypeDefault;
        camTypeDefault.protocol = protocolCamera.WS;
        camTypeDefault.network = networkCamera.LAN;
        camTypeDefault.dataSource = dataSourceCamera.NVR;

        //find cdn type in file session user
        QString dataSourceSaved = settings.value("data_source_camera").toString();
        if (dataSourceSaved == dataSourceCamera.CAM || dataSourceSaved == dataSourceCamera.CDN || dataSourceSaved == dataSourceCamera.NVR) {
            camTypeDefault.dataSource = dataSourceSaved;
        }

        if (!dataSourceSaved.isEmpty()) {
            if(dataSourceSaved == dataSourceCamera.CDN || dataSourceSaved == dataSourceCamera.NVR){
                camTypeDefault.protocol = protocolCamera.WS;
            }

            if(dataSourceSaved == dataSourceCamera.CAM){
                camTypeDefault.protocol = protocolCamera.RTSP;
            }

            if(dataSourceSaved == dataSourceCamera.NVR) camTypeDefault.network = networkCamera.LAN;
            if(dataSourceSaved == dataSourceCamera.CAM) camTypeDefault.network = networkCamera.LAN;
            if(dataSourceSaved == dataSourceCamera.CDN) camTypeDefault.network = networkCamera.CDN;
        }

        settings.setValue("network_type", camTypeDefault.network);
        settings.setValue("protocol_type", camTypeDefault.protocol);
        settings.setValue("data_source_camera", camTypeDefault.dataSource);

        //set cdn type working to appcontext
        appContext->setNetworkType(camTypeDefault);
        appContext->setWorkingUser(user);
        this->appContext->getUserSites()->appendListSiteChild(user->getSite());
        appContext->setMainWindow(zone);

        settings.beginGroup(QString::number(user->getUserId())); //tim xem co ton tai thong tin cua id nay chua
        QStringList listKey = settings.childKeys();
        qDebug() << "LIST KEY " << listKey;
        if(listKey.size() == 0){
            //chưa có SessionUser của nguoi dung
            AppSetttings::instance().createSettingsDefaults(user->getUserId());
            appContext->setSiteOfWorker(user->getSite());
            appContext->setWorkingSite(user->getSite());
        }else{
            //neu co
            int siteIdUserSession = settings.value("id_working_site").toInt();
            QString siteName = settings.value("site_name").toString();
            if (siteIdUserSession != -1 && siteName != "") {
                //neu ton tai siteid cu cua nguoi dung
                Site *siteSession = new Site();
                siteSession->setSiteId(siteIdUserSession);
                siteSession->setSiteName(siteName);
                appContext->setSiteOfWorker(siteSession);
                appContext->setWorkingSite(siteSession);
            }
        }

        this->pres = new P_MainFrame(this, this->zone);
        this->abst = new A_MainFrame(this);


        //app working first
        QString appNameVideoWall = "Video Wall";
        this->appContext->setWorkingApp(appNameVideoWall);

        //        cWindowsManager = new C_WindowsManager(this, presentation()->getZone());

        cTopControlBar = new C_TopControlBar(this, presentation()->getZone(0));
        cTopControlBarOverlay =
                new C_TopControlBar(this, presentation()->getZone(-1));

        cVideoWall = new C_VideoWall(this, presentation()->getZone(1));
        cPlayBack = new C_PlayBack(this, presentation()->getZone(2));
        cCamerasManager = new C_CamerasManager(this, presentation()->getZone(3));
        cSettings = new C_Settings(this, presentation()->getZone(5));


        //load all camera for sensorcamera manager
        updateSiteNewForSensorCameraManager(appContext->getWorkingSite());

        // init socket
        cSocket = new C_Socket(this, this->presentation()->getZone(6));
        cSocket->newAction(Message.APP_SOCKET_TO_CONNECT, Q_NULLPTR);
        QVariant *dataStructUser = new QVariant();
        dataStructUser->setValue<int>(this->appContext->getWorkingUser()->getUserId());
        cSocket->newAction(Message.START_JOIN_USER_ID, dataStructUser);
        QVariant *dataStruct = new QVariant();
        Site *workingSite = appContext->getWorkingSite();
        if(workingSite != Q_NULLPTR){
            dataStruct->setValue(workingSite->getSiteId());
            cSocket->newAction(Message.START_JOIN_SITE , dataStruct);
        }

        cNetwork = new C_Network(this, this->presentation()->getZone(6));
        cNetwork->newAction(Message.APP_NETWORK_START_CHECKING, Q_NULLPTR);
        cAboutApp = new C_About(this, this->presentation()->getZone(7));
        cHelpUser = new C_HelpUser(this, this->presentation()->getZone(8));

        //version updater
        cVersionUpdater = new C_VersionUpdater(this);

        // this->zone->showFullScreen();
        // newAction(Message.APP_PLAY_BACK_SHOW, 0);

        //      newAction(Message.APP_VIDEO_WALL_SHOW, 0);
        //      newAction(Message.SHOW_INDICATOR, Q_NULLPTR);
        //      cTopControlBarOverlay->newAction(Message.SHOW_INDICATOR,Q_NULLPTR);
        //      cTopControlBar->newAction(Message.SHOW_INDICATOR,Q_NULLPTR);

        // load data with mac address
        QString tokenLogin = this->appContext->getWorkingUser()->getToken();
        //if not offline mode
        if(offlineModeSaved != "ON"){
            this->abstraction()->loadDataCamerasWithDeviceId(tokenLogin);
        }else if(offlineModeSaved == "ON"){
            this->appContext->setIsLoadDataLocal(true);
            QJsonObject jsonObject = LoadDataLocal::instance().loadDataCamerasDeviceFromDisk();
            //load data with device mac
            if(!jsonObject.isEmpty()){
                QJsonDocument doc(jsonObject);
                QString jsonObjectToString(doc.toJson(QJsonDocument::Compact));
                this->abstraction()->loadDataCamerasWithDeviceId(tokenLogin);
            }else{
                if (user) {
                    this->abstraction()->loadAllCamerasOfSite([this]{
                        newSystemAction(Message.APP_DATA_LOADED_SUCCESS, Q_NULLPTR);

                        newSystemAction(Message.APP_VIDEO_WALL_LAYOUT_DEFAULT_SET,
                                        Q_NULLPTR);
                        newSystemAction(Message.APP_PLAY_BACK_LAYOUT_DEFAULT_SET,
                                        Q_NULLPTR);
                    }, [] {
                        qDebug() << "load data of site local success";
                    });
                    this->abstraction()->loadUserSites();
                }
            }
        }
    }
    currentDateSelected = QDateTime::currentDateTime();
    hourCurrent = currentDateSelected.time().hour();
    timestampCurrent = QDateTime::currentDateTime().currentSecsSinceEpoch();
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &C_MainFrame::timeUpdate);
    timer->start(1000);
}

void C_MainFrame::startDiscoverIpDevicesInNetwork(){
//    Q_EMIT discoverIpDevicesInNetwork();
//    OnvifUtils *onvif = OnvifUtils::instances();
//    QSettings settings;
//    //init filtercamera
//    QString deviceType = settings.value("device_type").toString();
//    if(deviceType == "B4" /*&& !LoadDataLocal::instance().checkFileData()*/){
//        DiscoverCameraWidget *discoverWidget = new DiscoverCameraWidget();
//        cDiscoverCameras = new C_DiscoverCameras(discoverWidget ,this, discoverWidget);
//        onvif->setDialogDiscover(discoverWidget);
//        onvif->setCDiscoverCameras(cDiscoverCameras);
//        discoverWidget->exec();
//    }

}

void C_MainFrame::timeUpdate() {
    long timestampCurrentNew = QDateTime::currentDateTime().currentSecsSinceEpoch();
    int diffTime = qFabs(timestampCurrentNew - timestampCurrent);
    timestampCurrent = QDateTime::currentDateTime().currentSecsSinceEpoch();
    QString day1 = currentDateSelected.date().toString("dd:MM:yyyy");
    QDateTime currentDate = QDateTime::currentDateTime();
    QString day2 = currentDate.date().toString("dd:MM:yyyy");
    hourFuture = currentDate.time().hour();
    countTimeDelayTransition++;
    countTimeCheckVersion++;
    countTimeShowVersionUpdater++;
    //update day for calendar playback
    if (day1 != day2) {
        currentDateSelected = QDateTime::currentDateTime();
        hourCurrent = currentDateSelected.time().hour();
        cPlayBack->newAction(Message.APP_PLAY_BACK_UPDATE_MAX_DATE_CALENDAR,
                             Q_NULLPTR);

    }
    //update time for calendar playback
    int currentTimeSecond = (3600 - (currentDate.time().minute()*60 + currentDate.time().second()));
    if (currentTimeSecond == 3599 || diffTime > 60) {
        cPlayBack->newAction(Message.APP_PLAY_BACK_UPDATE_TIME_PLAYLISTOFDAY,
                             Q_NULLPTR);
    }

    //default
    modePageTransition = Message.PAGE_TRANSITION_START_ON_CLICK;
    timeDelayTrasition = 300;

    QSettings settings;
    settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
    int timeTransitionDelaySave = settings.value("page_transition_delay").toInt();
    int modePageTransitionSave = settings.value("page_transition_start_mode").toInt();
    settings.endGroup();
    //    qDebug() << "timeTransitionDelaySave" <<timeTransitionDelaySave << "modePageTransitionSave" <<modePageTransitionSave;
    if(timeTransitionDelaySave > 0){
        timeDelayTrasition = timeTransitionDelaySave;
    }

    if(modePageTransitionSave > 0){
        modePageTransition = modePageTransitionSave ;
    }
    //    qDebug() << "timeDelayTrasition" <<timeDelayTrasition <<"countTimeDelayTransition" <<countTimeDelayTransition;

    if ((countTimeDelayTransition == timeDelayTrasition) && (modePageTransition == Message.PAGE_TRANSITION_START_AUTOMATICALLY)) {
        if (cVideoWall != Q_NULLPTR) {
            countTimeDelayTransition = 0;
            qDebug() << "PAGE_TRANSITION_BEGIN ";
            cVideoWall->newAction(Message.PAGE_TRANSITION_BEGIN, Q_NULLPTR);
        }
    }
    if (modePageTransition ==
            Message.PAGE_TRANSITION_START_ON_CLICK) {
        countTimeDelayTransition = 0;
    }

    //    qDebug() << "countTimeDelayTransition" <<countTimeDelayTransition;
}



/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_MainFrame::newUserAction(int message, QVariant *attachment) {
    switch (message) {
    case Message.TOP_CONTROL_BAR_GET_SITE_LIST: {
        if (cTopControlBar != Q_NULLPTR) {
            cTopControlBar->newUserAction(Message.TOP_CONTROL_BAR_GET_SITE_LIST,
                                          attachment);
        }
        if (cTopControlBarOverlay != Q_NULLPTR) {
            cTopControlBarOverlay->newUserAction(
                        Message.TOP_CONTROL_BAR_GET_SITE_LIST, attachment);
        }
    } break;

    case Message.ENTER_FULLSCREEN_MODE: {
        if (cVideoWall != Q_NULLPTR) {
            cVideoWall->newAction(message, attachment);
        }
        if (cPlayBack != Q_NULLPTR) {
            cPlayBack->newAction(message, attachment);
        }
    } break;

    case Message.EXIT_FULLSCREEN_MODE: {
        if (cVideoWall != Q_NULLPTR) {
            cVideoWall->newAction(message, attachment);
        }
        if (cPlayBack != Q_NULLPTR) {
            cPlayBack->newAction(message, attachment);
        }
    } break;

    default:
        qDebug() << "ERROR : General User action in" << getClass()
                 << "non-catched :" << Message.toString(message);
        break;
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_MainFrame::newSystemAction(int message, QVariant *attachment) {
    switch (message) {
    case Message.UPDATE_LIST_CAMERA_OF_DEVICE_FOR_SENSOR_CAMERA_MANAGER:{
        cSensorCameraManager->newAction(message, attachment);
    }break;

    case Message.APP_UPDATE_DATA_LOCAL_SUCCESS:{
        cVideoWall->newAction(message, attachment);
        cPlayBack->newAction(message, attachment);
    }break;

    case Message.APP_LOAD_DATA_REMOTE_CONTROL_PERMISSION_DENIED:{
        this->appContext->setIsLoadDataWithSiteId(true);
        this->abstraction()->loadDataCamerasWithLayoutPage();
        this->abstraction()->loadUserSites();
    }break;

    case Message.APP_VIDEO_WALL_LAYOUT_PAGE_UPDATE_REMOTE_CONTROL:{
        if (cVideoWall != Q_NULLPTR) {
            cVideoWall->newAction(message, attachment);
        }
    }break;

    case Message.APP_PLAY_BACK_LAYOUT_PAGE_UPDATE_REMOTE_CONTROL:{
        if (cPlayBack != Q_NULLPTR) {
            cPlayBack->newAction(message, attachment);
        }
    }break;


    case Message.APP_PLAY_BACK_CALENDAR_MAX_HISTORYDAY:{
        cPlayBack->newAction(message, attachment);
    }break;

    case Message.APP_UPDATE_USER_SITES: {
        cTopControlBar->newAction(message, attachment);
        cTopControlBarOverlay->newAction(message, attachment);

    } break;

    case Message.APP_VIDEO_WALL_WORKSPACES_INIT:{
        if(cVideoWall != Q_NULLPTR && isLoadWorkspaceFirst){
            isLoadWorkspaceFirst = false;
            cVideoWall->loadWorkspaceWindows();
        }
    }break;

    case Message.APP_DATA_LOADED_REMOTE_CONTROL_SUCCESS:{
        qDebug() << "APP_DATA_LOADED_REMOTE_CONTROL_SUCCESS";
        if(isShowVideoWallFirst){
            isShowVideoWallFirst = false;
//            newAction(Message.APP_VIDEO_WALL_SHOW, 0);
            newAction(Message.APP_CAMERAS_MANAGER_SHOW, Q_NULLPTR);
            newAction(Message.ENTER_FULLSCREEN_MODE, 0);
        }
    }break;



    case Message.APP_DATA_LOADED_SUCCESS:{
        qDebug() << "APP_DATA_LOADED_SUCCESS";
//        newAction(Message.APP_VIDEO_WALL_SHOW, 0);
        newAction(Message.APP_CAMERAS_MANAGER_SHOW, Q_NULLPTR);
        newAction(Message.ENTER_FULLSCREEN_MODE, 0);

    } break;

    case Message.APP_VIDEO_WALL_LAYOUT_DEFAULT_SET: {
        if (cVideoWall != Q_NULLPTR) {
            cVideoWall->newAction(message, attachment);
        }
    } break;

    case Message.APP_PLAY_BACK_LAYOUT_DEFAULT_SET: {
        if (cPlayBack != Q_NULLPTR) {
            cPlayBack->newAction(message, attachment);
        }
    } break;


    case Message.DISPLAY_LIST_APPS: {
        if (cTopControlBar != Q_NULLPTR) {
            cTopControlBar->newAction(message, attachment);
        }
    } break;

    default:
        qDebug() << "ERROR : General System action in" << getClass()
                 << "non-catched :" << Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_MainFrame::newAction(int message, QVariant *attachment) {
    switch (message) {
    case Message.SHOW_ZONE_DISCOVER_CAMERAS:{
        presentation()->showApp(message);
    }break;

    case Message.VIDEO_WALL_RENDER_UNSUBSCRIBE_CRAWLER_CAMERA:
    case Message.VIDEO_WALL_RENDER_SUBSCRIBE_CRAWLER_CAMERA:{
        cSensorCameraManager->newAction(message, attachment);
    }break;

    case Message.UPDATE_STATE_SHOW_WINDOWS_LIST:{
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }break;

    case Message.APP_VIDEO_WALL_HIDE_FULLSCREEN_WORKSPACE_ORIGIN:{
        presentation()->hideFullScreen();
    }break;

    case Message.APP_VIDEO_WALL_SHOW_FULLSCREEN_WORKSPACE_ORIGIN:{
        presentation()->showFullScreen();
    }break;

    case Message.APP_VIDEOWALL_CLOSED_WINDOWS:{
        cVideoWall->newAction(message, attachment);
    }break;

    case Message.AUTO_UPDATE_VERSION:{
        cSettings->newAction(Message.AUTO_UPDATE_VERSION_ALL, attachment);
    }break;

    case Message.VERSION_NEWEST_VERSION_UPDATER_HIDE_ALL:{
        cTopControlBar->newAction(message, attachment);
        cTopControlBarOverlay->newAction(message, attachment);
    }break;

    case Message.AUTO_CLICK_UPDATE_VERSION:{
        cTopControlBar->newAction(message, attachment);
        cTopControlBarOverlay->newAction(message, attachment);
    }break;

    case Message.UPDATE_STATE_BUTTON_TOP_BAR:{
        cVersionUpdater->newAction(message, attachment);
        cTopControlBar->newAction(Message.UPDATE_STATE_BUTTON_ALL_TOP_BAR, attachment);
        cTopControlBarOverlay->newAction(Message.UPDATE_STATE_BUTTON_ALL_TOP_BAR, attachment);
    }break;

    case Message.VERSION_UPDATER_DISPLAY_AUTO_SHOW:{
        cTopControlBar->newAction(message, attachment);
        cTopControlBarOverlay->newAction(message, attachment);
    }break;

    case Message.SHOW_BUTTON_UPDATE_VERSION_NEWEST:{
        cTopControlBar->newAction(message, attachment);
        cTopControlBarOverlay->newAction(message, attachment);
    }break;

    case Message.UPDATE_STATE_USE_FREE_SPACE:{
        cVideoWall->newAction(message, attachment);
        cPlayBack->newAction(message, attachment);
    }break;

    case Message.TURN_ON_OFFLINE_MODE:
    case Message.TURN_OFF_OFFLINE_MODE:{
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }break;

    case Message.UPDATE_DATA_STAND_ALONE_MODE_AFTER_NETWORK_IS_UNREACHABLE:{
        if(this->appContext->getIsLoadDataWithDeviceId()){
            this->abstraction()->updateDataCamerasWithDeviceId(this->appContext->getWorkingUser()->getToken());
        }else{
            this->abstraction()->loadUserSites(true);
        }
    }break;

    case Message.UPDATE_DATA_NORMAL_MODE_AFTER_NETWORK_IS_UNREACHABLE:{
        cPlayBack->newAction(message,attachment);
        cVideoWall->newAction(message,attachment);
    }break;

    case Message.UPDATE_MODE_RENDER_DEBUG:{
        cPlayBack->newAction(message,attachment);
        cVideoWall->newAction(message,attachment);
    }break;

    case Message.UPDATE_PERCENT_SOCKET_GERNERATE_LINK_MP4_RECORD:{
        cPlayBack->newAction(message, attachment);
    }break;

    case Message.CANCEL_CREATE_VIDEO_RECORD_SUCCESS:{
        cPlayBack->newAction(message, attachment);
    }break;
    case Message.CANCEL_CREATE_VIDEO_RECORD:{
        cSocket->newAction(message, attachment);
    }break;
    case Message.APP_VIDEO_WALL_UPDATE_CAMERA_ORDER_REMOTE_CONTROL:{
        cSocket->newAction(message, attachment);
    }break;

    case Message.LOAD_DATA_CAMERAS_WITH_DEVICE_ID:{
        QString token = this->appContext->getWorkingUser()->getToken();
        this->abstraction()->loadDataCamerasWithDeviceId(token);
    }break;

    case Message.NVR_CHANGE_IP_NEED_REFRESH:{
        cPlayBack->newAction(message,attachment);
        cVideoWall->newAction(message,attachment);
    }break;

    case Message.CMS_CHANGE_CAM_ORDER_NEED_REFRESH:{
        QString appNamePlayback = "Playback";
        QString appNameVideowall = "Video Wall";
        if(this->appContext->getIsLoadDataWithDeviceId()){
            //load with device
            if(this->appContext->getWorkingApp().appName.contains(appNameVideowall, Qt::CaseInsensitive)){
                newAction(Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID, attachment);
            } else if(this->appContext->getWorkingApp().appName.contains(appNamePlayback, Qt::CaseInsensitive)){
                newAction(Message.APP_PLAY_BACK_GET_CAMERAS_WITH_DEVICE_ID, Q_NULLPTR);
            }
        }else{
            LayoutStruct layoutStruct;
            layoutStruct.numberOfCameras = 1;
            layoutStruct.selectedPage = 1;
            QVariant *dataStruct = new QVariant();
            dataStruct->setValue<LayoutStruct>(layoutStruct);
            //load with site
            cVideoWall->newAction(Message.CMS_CHANGE_CAM_ORDER_REFRESH_WORKSPACE, dataStruct);
        }
    }break;

    case Message.APP_PLAY_BACK_GET_CLIP_RECORD_ERROR:{
        cPlayBack->newAction(message, attachment);
    }break;

    case Message.APP_PLAY_BACK_GET_CLIP_RECORD_SUCCESS:{
        cPlayBack->newAction(message, attachment);
    }
        break;

    case  Message.STOP_TIMER_RECHECK_NETWORK:{
        cNetwork->newAction(message, attachment);
    }break;


    case Message.UPDATE_CDN_TYPE_SELECTED:{
        cPlayBack->newAction(message,attachment);
        cVideoWall->newAction(message,attachment);
    }break;

    case Message.APP_PLAY_BACK_GET_ALL_CLIP_RECORD_SUCCESS:{
        cPlayBack->newAction(message, attachment);
    }
        break;


    case Message.APP_PLAY_BACK_GET_CLIP_RECORD:{
        cSocket->newAction(message, attachment);
    }break;

    case Message.APP_SHOW_SETTINGS: {
        this->cSettings->newAction(message, attachment);
    } break;

    case Message.APP_HIDE_SETTINGS: {
        this->cSettings->newAction(message, attachment);
    } break;

    case Message.USER_CHANGED_PASSWORD_NEED_LOGOUT:
    case Message.LOGOUT: {
        //        https://core.sandbox.cam9.tv/api/cms_api/users/logout?
        QString uriLogout = "cms_api/users/logout";
        QMap<QString, QString> params;
        params["token"] = this->appContext->getWorkingUser()->getToken();

        NetworkUtils::instance().getRequest(uriLogout,params, [this](QJsonObject jsonObject){
            qDebug() << "logout success";
        }, [this](QString message){

        });
        QSettings settings;
        settings.setValue("offline_mode" , "OFF");

        User::clearUserInfo();
        LoadDataLocal::instance().clearDataLocal();
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    } break;

    case Message.EXIT_APP: {
        qApp->quit();
        qDebug() << Q_FUNC_INFO << "APPLICATION_PID_BEFORE_EXIT: " << QApplication::QCoreApplication::applicationPid();
        QProcess process;
        process.execute(QString("kill %1").arg(
                            QApplication::QCoreApplication::applicationPid()));
    } break;

    case Message.SITE_NEW_SELECTED: {
        qDebug() << "SITE_NEW_SELECTED";
        Site *site = attachment->value<Site *>();
        getAppContext()->setWorkingSite(site);
        updateSiteNewForSensorCameraManager(appContext->getWorkingSite());

        QVariant *dataStruct = new QVariant();
        dataStruct->setValue(site->getSiteId());
        cSocket->newAction(Message.START_JOIN_SITE , dataStruct);

        QSettings settings;
        QString offlineMode = settings.value("offline_mode").toString();

        if(offlineMode == "ON"){
            //if selected offline mode
            qDebug() << "load data when change site " << site->getSiteId() << "Name" << site->getSiteName();
            this->abstraction()->loadDataCamerasOfSite(site->getSiteId());
        }else{
            abstraction()->changeSite();
        }

        cTopControlBarOverlay->newAction(Message.SITE_CHANGED, attachment);
        cTopControlBar->newAction(Message.SITE_CHANGED, attachment);
        cVideoWall->newAction(Message.SITE_CHANGED, attachment);
        cPlayBack->newAction(Message.SITE_CHANGED, attachment);
        cSettings->newAction(Message.SITE_CHANGED, attachment);
    } break;

    case Message.APP_NEW_SELECTED: {
        abstraction()->changeApp();
        cTopControlBarOverlay->newAction(Message.APP_CHANGED, attachment);
        cTopControlBar->newAction(Message.APP_CHANGED, attachment);
        cVideoWall->newAction(Message.APP_CHANGED, attachment);
        cPlayBack->newAction(Message.APP_CHANGED, attachment);
    } break;

    case Message.PROJECT_RUN:
        this->zone->show();
        break;

    case Message.APP_LOGIN_SHOW:
        cLogin->newAction(Message.APP_LOGIN_SHOW, attachment);
        break;

    case Message.APP_VIDEO_WALL_SHOW: {
        qDebug() << "APP_VIDEO_WALL_SHOW";
        presentation()->showApp(message);
        this->appContext->setWorkingApp("Video Wall");
        cVideoWall->newAction(Message.APP_SHOW, attachment);

        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>("Video Wall");
        //clear source change_app
        cPlayBack->newAction(Message.APP_CHANGED, dataStruct);
        cTopControlBarOverlay->newAction(Message.APP_CHANGED, dataStruct);
        cTopControlBar->newAction(Message.APP_CHANGED, dataStruct);

        QVariant *setingData = new QVariant();
        setingData->setValue(2);
        this->cSettings->newAction(Message.APP_UPDATE_INDEX_SETTINGS, setingData);
    } break;

    case Message.APP_PLAY_BACK_SHOW: {
        presentation()->showApp(message);
        this->appContext->setWorkingApp("Playback");
        cPlayBack->newAction(Message.APP_SHOW, attachment);

        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>("Playback");
        //clear source change_app
        cVideoWall->newAction(Message.APP_CHANGED, dataStruct);
        cTopControlBarOverlay->newAction(Message.APP_CHANGED, dataStruct);
        cTopControlBar->newAction(Message.APP_CHANGED, dataStruct);

        QVariant *setingData = new QVariant();
        setingData->setValue(3);
        this->cSettings->newAction(Message.APP_UPDATE_INDEX_SETTINGS, setingData);
    } break;


    case Message.APP_CAMERAS_MANAGER_SHOW:
    {
        qDebug() << "APP_CAMERAS_MANAGER_SHOW";
        presentation()->showApp(message);
        this->appContext->setWorkingApp("Cameras");
        cCamerasManager->newAction(Message.APP_SHOW, attachment);

        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<QString>("Cameras");
        //clear source change_app
        cVideoWall->newAction(Message.APP_CHANGED, dataStruct);
        cPlayBack->newAction(Message.APP_CHANGED, dataStruct);
        cTopControlBarOverlay->newAction(Message.APP_CHANGED, dataStruct);
        cTopControlBar->newAction(Message.APP_CHANGED, dataStruct);

        QVariant *setingData = new QVariant();
        setingData->setValue(1);
        this->cSettings->newAction(Message.APP_UPDATE_INDEX_SETTINGS, setingData);
    }break;

    case Message.APP_CONTEXT_GET:
        attachment->setValue(appContext);
        break;

    case Message.APP_CAM_SITE_GET: {
        attachment->setValue(appContext);
    } break;

    case Message.ENTER_FULLSCREEN_MODE: {
        presentation()->enterFullscreenMode();
        cTopControlBar->newAction(message, attachment);
        cTopControlBarOverlay->newAction(message, attachment);
    } break;

    case Message.TOP_BAR_OVER_LAY_CAN_HIDE: {
        presentation()->topBarOverlayCanHide();
    } break;

    case Message.TOP_BAR_OVER_LAY_SHOW_WHEN_VERSION_UPDATE_SHOW:{
        presentation()->showTopBarOverlay();
    }break;

    case Message.TOP_BAR_OVER_LAY_CAN_NOT_HIDE: {
        presentation()->topBarOverlayCanNotHide();
    } break;

    case Message.EXIT_FULLSCREEN_MODE: {
        presentation()->exitFullscreenMode();
    } break;

    case Message.APP_PLAY_BACK_SET_PATH_SAVE_PICTURES: {
        QString pathSaveMedia = attachment->value<QString>();
        if(!QDir(pathSaveMedia).exists()){
            QDir().mkpath(pathSaveMedia);
        }
        QSettings settings;
        settings.setValue("path_save_pictures",pathSaveMedia);
    } break;

    case Message.PAGE_TRANSITION_START_ON_CLICK: {
        this->appContext->setVideoWallPageTransitionStartMode(
                    Message.PAGE_TRANSITION_START_ON_CLICK);
        QSettings settings;
        settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
        settings.setValue("page_transition_start_mode",Message.PAGE_TRANSITION_START_ON_CLICK);

    } break;

    case Message.PAGE_TRANSITION_START_AUTOMATICALLY: {
        this->appContext->setVideoWallPageTransitionStartMode(
                    Message.PAGE_TRANSITION_START_AUTOMATICALLY);
        QSettings settings;
        settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
        settings.setValue("page_transition_start_mode",Message.PAGE_TRANSITION_START_AUTOMATICALLY);
    } break;

    case Message.PAGE_TRANSITION_DELAY: {
        int pageTransitionDelay = attachment->value<int>();
        this->appContext->setVideoWallPageTransitionDelay(pageTransitionDelay);
        QSettings settings;
        settings.beginGroup(QString::number(this->appContext->getWorkingUser()->getUserId()));
        settings.setValue("page_transition_delay",pageTransitionDelay);
    } break;

    case Message.APP_SOCKET_CONNECTED:{
        //        this->abstraction()->loadDataCamerasWithLayoutPage();
    }break;

    case Message.APP_PLAY_BACK_GET_CAMERAS_WITH_DEVICE_ID:{
        QString token = this->appContext->getWorkingUser()->getToken();
        this->abstraction()->loadDataCamerasOfDevice(Q_NULLPTR, token, [this]{
            cPlayBack->newAction(Message.APP_PLAY_BACK_GET_CAMERAS_WITH_DEVICE_ID_SUCCESS, Q_NULLPTR);
        }, [this] (QString message){
            qDebug() << "Load cameras of site error";
        });
    }break;

        //load data videowall
    case Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID:{
        QString token = this->appContext->getWorkingUser()->getToken();
        this->abstraction()->loadDataCamerasOfDevice(attachment, token, [this, attachment]{
            //attachment check nvr change ip or remote control by user
            cVideoWall->newAction(Message.APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID_SUCCESS, attachment);
        }, [this](QString message) {
            qDebug() << "Load cameras of site error";
        });
    }break;


    case Message.APP_PLAY_BACK_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE:{
        LayoutStruct layoutStruct = attachment->value<LayoutStruct>();
        int pageNumber = layoutStruct.selectedPage;
        int layoutNumber = layoutStruct.numberOfCameras;
        this->abstraction()->loadCamerasOfSiteWithLayoutPage(pageNumber, layoutNumber, [this]{
            cPlayBack->newAction(Message.APP_PLAY_BACK_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS, Q_NULLPTR);
        }, [] {
            qDebug() << "Load cameras of site error";
        });
    }break;

    case Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE:{

        DataOfWorkspace dataOfWorkspace = attachment->value<DataOfWorkspace>();
        LayoutStruct layoutPage = dataOfWorkspace.layoutPageOfWorkspace;
        C_VWWorkSpace *cVWWorkSpace = dataOfWorkspace.cVWWorkSpace;
        if(!this->appContext->getIsLoadDataLocal())
        {
            int pageNumber = layoutPage.selectedPage;
            int layoutNumber = layoutPage.numberOfCameras;
            this->abstraction()->loadCamerasOfSiteWithLayoutPage(pageNumber, layoutNumber, [this, cVWWorkSpace]{
                if(cVWWorkSpace != Q_NULLPTR){
                    cVWWorkSpace->newAction(Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS, Q_NULLPTR);
                }
            }, [] {
                qDebug() << "Load cameras of site error";
            });
        }

        if(this->appContext->getIsLoadDataLocal())
        {
            //load data local
            this->abstraction()->loadAllCamerasOfSite([this, attachment,cVWWorkSpace]{
                if(cVWWorkSpace != Q_NULLPTR){
                    cVWWorkSpace->newAction(Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS_LOCAL, attachment);
                }
            },[]{
                qDebug() << "Load cameras local of site error";
            });

        }
    }break;

    case Message.APP_PLAY_BACK_GET_CAMERAS_OF_SITE_WITH_SITE_ID:{
        LayoutStruct layoutStruct = attachment->value<LayoutStruct>();
        int pageNumber = layoutStruct.selectedPage;
        int layoutNumber = layoutStruct.numberOfCameras;
        this->abstraction()->loadAllCamerasOfSite([this]{
            cPlayBack->newAction(Message.APP_PLAY_BACK_GET_CAMERAS_OF_SITE_WITH_SITE_ID_SUCCESS, Q_NULLPTR);
        }, [] {
            qDebug() << "Load cameras of site error";
        });
    }break;

    case Message.APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_SITE_ID:{
        LayoutStruct layoutStruct = attachment->value<LayoutStruct>();
        int pageNumber = layoutStruct.selectedPage;
        int layoutNumber = layoutStruct.numberOfCameras;
        this->abstraction()->loadAllCamerasOfSite([this]{
            cVideoWall->newAction(Message.APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_SITE_ID_SUCCESS, Q_NULLPTR);
        }, [] {
            qDebug() << "Load cameras of site error";
        });
    }break;

    case Message.SHOW_INDICATOR:
    {
        qDebug() << "SHOW_INDICATOR";
        presentation()->showApp(message);
    }
        break;
    case Message.HIDE_INDICATOR:
    {
        qDebug() << "HIDE_INDICATOR";
        presentation()->hideZone(message);
    }
        break;

    case Message.APP_NETWORK_IS_REACHABLE:
    {
        //        qDebug() << QDateTime::currentDateTime() << "APP_NETWORK_IS_REACHABLE";
        cSocket->newAction(message, attachment);
        abstraction()->reloadDataWhenNetworkIsReachable();
        cVideoWall->newAction(message, attachment);
    }
        break;
    case Message.APP_NETWORK_IS_UNREACHABLE:
    {
        //        qDebug() << QDateTime::currentDateTime() << "APP_NETWORK_IS_UNREACHABLE";
        cSocket->newAction(message, attachment);
        cVideoWall->newAction(message, attachment);
    }
        break;

    case Message.SHOW_ABOUT_APP:{
        cAboutApp->newAction(message, attachment);

    }break;

    case Message.SHOW_HELP_USER:{
        cHelpUser->newAction(message, attachment);

    }break;

    default:

        qDebug() << "ERROR : General Internal pac action in" << getClass()
                 << "non-catched :" << Message.toString(message);
    }
}
