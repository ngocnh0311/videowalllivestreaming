#ifndef P_MAINFRAME_H
#define P_MAINFRAME_H

#include <PacModel/presentation.h>
#include <Site/changesite.h>
#include <QObject>
#include <QStackedLayout>
#include <QWidget>
#include "c_mainframe.h"
#include "message.h"
struct DataOfWorkspace{
    C_VWWorkSpace* cVWWorkSpace = Q_NULLPTR;
    LayoutStruct layoutPageOfWorkspace;
};
Q_DECLARE_METATYPE(DataOfWorkspace)

class C_MainFrame;
class P_MainFrame : public Presentation {
    Q_OBJECT
public:
    QSize sizeScreenAvailable;
    QWidget *zone = Q_NULLPTR;
    QWidget *topBar = Q_NULLPTR;
    QWidget *topBarOverlay = Q_NULLPTR;
    QWidget *workSpaceForCameras = Q_NULLPTR;
    QWidget *workSpaceForVideoWall = Q_NULLPTR;
    QWidget *workSpaceForPlayBack = Q_NULLPTR;
    QWidget *loadingIndicator = Q_NULLPTR;
    QWidget *discoverCameras = Q_NULLPTR;
    QWidget *workspaceZone = Q_NULLPTR;
    QStackedLayout *workSpaceLayout = Q_NULLPTR;
    ChangeSite *siteTop;
    C_MainFrame *control() { return (C_MainFrame *)this->ctrl; }
    QWidget *settingZone = Q_NULLPTR;
    QWidget *aboutApp = Q_NULLPTR;
    QWidget *helpUser = Q_NULLPTR;
    QWidget *managerWindows = Q_NULLPTR;

private:
    AppSize appSize;

public:
    bool isTopBarOverlayCanHide = true;
    P_MainFrame(Control *ctrl, QWidget *zone);
    void changeControl(Control *ctrl);
    void update();
    QWidget *getZone(int zoneId);
    QWidget * getZone();
    void showApp(int message);
    void hideZone(int message);

    void enterFullscreenMode();
    void exitFullscreenMode();

    void topBarOverlayCanHide();
    void topBarOverlayCanNotHide();
    void showTopBarOverlay();
    void showFullScreen();
    void hideFullScreen();
protected:
    void hoverEnter(QHoverEvent *event);
    void hoverLeave(QHoverEvent *event);
    void hoverMove(QHoverEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
    void resizeEvent(QResizeEvent *event);
};

#endif  // P_MAINFRAME_H
