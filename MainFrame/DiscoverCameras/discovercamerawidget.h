#ifndef DISCOVERCAMERAWIDGET_H
#define DISCOVERCAMERAWIDGET_H

#include <QStackedLayout>
#include <QPushButton>
#include <QDialog>
#include <QProcess>
class DiscoverCameraWidget: public QDialog
{
public:
    QWidget *zone = Q_NULLPTR;
    QVBoxLayout *mainLayout = Q_NULLPTR;
    DiscoverCameraWidget();
    QWidget* getZone();
    void updateDialog(QString title);
protected:
    void closeEvent (QCloseEvent *event);
};

#endif // DISCOVERCAMERAWIDGET_H
