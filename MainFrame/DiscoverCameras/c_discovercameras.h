#ifndef C_DiscoverCameras_H
#define C_DiscoverCameras_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "p_discovercameras.h"
#include "a_discovercameras.h"

class C_IPDevicesSelector;
class C_CameraFilter;
class P_DiscoverCameras;
class A_DiscoverCameras;
class DiscoverCameraWidget;
class C_MainFrame;
class C_DiscoverCameras : public Control {
    Q_OBJECT
 public:
  QWidget* zone;
  AppContext* appContext;

  C_IPDevicesSelector *cIPDevicesSelector = Q_NULLPTR;
  C_CameraFilter *cCameraFilter = Q_NULLPTR;
  C_DiscoverCameras(DiscoverCameraWidget *discoverWidget, Control* ctrl, QWidget *_zone);
  C_MainFrame* getParent() { return (C_MainFrame*)this->parent; }
  P_DiscoverCameras* presentation() { return (P_DiscoverCameras*)pres; }
  A_DiscoverCameras* abstraction() { return (A_DiscoverCameras*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
  C_IPDevicesSelector *getCIPDevicesSelector() const;

Q_SIGNALS:
  void finished();
};

#endif  // C_DiscoverCameras_H
