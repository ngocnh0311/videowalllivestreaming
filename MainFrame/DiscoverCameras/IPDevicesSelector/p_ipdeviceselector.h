#ifndef P_IPDevicesSelector_H
#define P_IPDevicesSelector_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_ipdeviceselector.h"
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QListWidget>
#include "IpDeviceCell/ipdevicecell.h"
class C_IPDevicesSelector;
class P_IPDevicesSelector : public Presentation {
    Q_OBJECT
    // init ui control
private:
public:
    QList<QString> listIpDevicesSelected;
    QList<QString> listIpDevicesOrigin;

    int indexItemSelectedLast;
    int heightTop = 50;
    int heightBottom = 50;

    QWidget *zone = Q_NULLPTR;
    QVBoxLayout *layout = Q_NULLPTR;

    QWidget *topWidget = Q_NULLPTR;
    QVBoxLayout *topLayout = Q_NULLPTR;
    QLineEdit *searchLineEdit = Q_NULLPTR;


    QWidget *centerWidget = Q_NULLPTR;
    QVBoxLayout *centerLayout = Q_NULLPTR;

    QListWidget *listIpDevicesWidget;
    QList<QListWidgetItem *> listItemCellIpDevice;
    QList<IPDeviceCell *> listIpDeviceCells;

    QWidget *bottomWidget = Q_NULLPTR;
    QVBoxLayout *bottomLayout = Q_NULLPTR;


    P_IPDevicesSelector(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_IPDevicesSelector *control() { return (C_IPDevicesSelector *)ctrl; }


    void setListIpDevicesOrigin(const QList<QString> &value);

protected:
    bool eventFilter(QObject *watched, QEvent *event);

public Q_SLOTS:
    void loadListIpDevices();
    void closeWindowSelectIpDevice();
    void onListIpDeviceClicked(QListWidgetItem *item);
Q_SIGNALS:
    void reloadListIpDevices();
};

#endif  // P_IPDevicesSelector_H
