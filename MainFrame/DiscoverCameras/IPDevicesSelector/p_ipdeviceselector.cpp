﻿#include "p_ipdeviceselector.h"

/**
     * Generic method to override for updating the presention.
     **/
P_IPDevicesSelector::P_IPDevicesSelector(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    connect(this, &P_IPDevicesSelector::reloadListIpDevices, this, &P_IPDevicesSelector::loadListIpDevices);
    // init gui object
    this->zone = _zone;
    this->zone->setStyleSheet("background:#ffffff; color:white");
    layout = new QVBoxLayout();
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    zone->setLayout(layout);

    topWidget = new QWidget(this->zone);
    topWidget->setStyleSheet("background:#EFEFEF");
    topWidget->setFixedHeight(heightTop);
    topLayout = new QVBoxLayout();
    topLayout->setAlignment(Qt::AlignHCenter);
    topLayout->setSpacing(10);
    topLayout->setContentsMargins(10,0,10,0);
    topWidget->setLayout(topLayout);

    searchLineEdit = new QLineEdit(topWidget);
    searchLineEdit->setMaximumHeight(30);
    searchLineEdit->setMinimumHeight(30);
    searchLineEdit->setStyleSheet("background : #ffffff;color:#4f4f4f");
    searchLineEdit->setTextMargins(5, 2, 5, 2);
    searchLineEdit->setPlaceholderText("Tìm kiếm...");
    searchLineEdit->setFont(Resources::instance().getMediumRegularButtonFont());
    topLayout->addWidget(searchLineEdit);

    centerWidget = new QWidget(this->zone);
    centerLayout = new QVBoxLayout();
    centerLayout->setSpacing(0);
    centerLayout->setMargin(0);
    centerWidget->setLayout(centerLayout);
    listIpDevicesWidget = new QListWidget(centerWidget);
    listIpDevicesWidget->setStyleSheet("QScrollBar:vertical{padding-left:1px;width:6px}QScrollBar::handle:vertical{background:#008098;border-radius:2px;min-height:10px}QScrollBar::add-line:horizontal,QScrollBar::add-line:vertical,QScrollBar::sub-line:horizontal,QScrollBar::sub-line:vertical{border:none}QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical{background:#383838;margin:0 2px}QScrollBar::down-arrow:vertical,QScrollBar::up-arrow:vertical{background:0 0}QScrollBar:horizontal{height:6px;padding-top:1px}QScrollBar::handle:horizontal{background:#008098;border-radius:2px;min-width:10px}QScrollBar::add-page:horizontal,QScrollBar::sub-page:horizontal{background:#383838;margin:2px 0}QScrollBar::left-arrow:horizontal,QScrollBar::right-arrow:horizontal{background:0 0}");
    listIpDevicesWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    connect(listIpDevicesWidget, &QListWidget::itemClicked, this , &P_IPDevicesSelector::onListIpDeviceClicked);
    centerLayout->addWidget(listIpDevicesWidget);

    loadListIpDevices();
    bottomWidget = new QWidget(this->zone);
    bottomWidget->setFixedHeight(heightBottom);
    bottomLayout = new QVBoxLayout();
    bottomLayout->setSpacing(5);
    bottomLayout->setAlignment(Qt::AlignCenter);
    bottomLayout->setMargin(0);
    bottomWidget->setLayout(bottomLayout);

    QPushButton *submitButton = new QPushButton(bottomWidget);
    submitButton->setStyleSheet("background:#333;color:white");
    submitButton->setFixedSize(130, 35);
    submitButton->setText("Đồng ý");
    connect(submitButton, &QPushButton::clicked, this,&P_IPDevicesSelector::closeWindowSelectIpDevice);
    bottomLayout->addWidget(submitButton);

    layout->addWidget(topWidget);
    layout->addWidget(centerWidget);
    layout->addWidget(bottomWidget);

}

void P_IPDevicesSelector::loadListIpDevices(){
    qDebug()<< Q_FUNC_INFO << QThread::currentThread();
    listIpDevicesWidget->clear();
    for (int index  = 0; index < listIpDeviceCells.size(); ++index) {
        IPDeviceCell* ipDeviceCellWidget = listIpDeviceCells.takeAt(index);
        delete ipDeviceCellWidget;
    }
    for (int index  = 0; index < listItemCellIpDevice.size(); ++index) {
        QListWidgetItem* item = listItemCellIpDevice.takeAt(index);
        delete item;
    }

    QStringList listBlackListDevice;
    listBlackListDevice.append("10.13.11.119:2000");
    listBlackListDevice.append("10.13.11.83:2000");
    listBlackListDevice.append("10.13.11.129:2000");
    listBlackListDevice.append("10.13.11.251:2000");
    listBlackListDevice.append("10.13.11.174:2000");
    listBlackListDevice.append("10.13.11.75:2000");
    listBlackListDevice.append("10.13.11.77:2000");
    listBlackListDevice.append("10.13.11.39:2000");
    listBlackListDevice.append("10.13.11.115:2000");
    listBlackListDevice.append("10.13.11.110:2000");

    for ( int index = 0 ; index < listIpDevicesOrigin.size() ; ++index )
    {
        QString ipDevice = listIpDevicesOrigin.at(index);
        bool checkSelect = listBlackListDevice.contains(ipDevice);
        IPDeviceCell* ipDeviceCellWidget = new IPDeviceCell(centerWidget, ipDevice , checkSelect);

        if(index == 0){
            ipDeviceCellWidget->updateBackground("#555" , "#FFF");
        }

        listIpDeviceCells.append(ipDeviceCellWidget);
        QListWidgetItem* listItem = new QListWidgetItem();
        listItemCellIpDevice.append(listItem);
        listItem->setSizeHint(ipDeviceCellWidget->sizeHint());
        listIpDevicesWidget->addItem(listItem);
        listIpDevicesWidget->setItemWidget(listItem, ipDeviceCellWidget);
    }
}

void P_IPDevicesSelector::onListIpDeviceClicked(QListWidgetItem *item){
    qDebug() <<Q_FUNC_INFO;

    if(item){
        int indexItemSelected = listItemCellIpDevice.indexOf(item);
        qDebug() <<Q_FUNC_INFO << "indexItemSelected" <<indexItemSelected;
        //        if(indexItemSelected == indexItemSelectedLast) return;
        indexItemSelectedLast =  indexItemSelected;

        //reset color of cell before
        for (int index = 0; index < listIpDeviceCells.size(); ++index) {
            IPDeviceCell *ipDeviceCell = listIpDeviceCells.at(index);
            ipDeviceCell->updateBackground("#333", "#ddd");
        }

        IPDeviceCell *ipDeviceCell = listIpDeviceCells.at(indexItemSelected);
        ipDeviceCell->updateBackground("#555" , "#FFF");
        ipDeviceCell->updateSelected();
    }
}

void P_IPDevicesSelector::closeWindowSelectIpDevice(){
    for (int index = 0; index < listIpDeviceCells.size(); ++index) {
        IPDeviceCell *ipDeviceCell = listIpDeviceCells.at(index);
        if(ipDeviceCell->isSelected()){
            listIpDevicesSelected.append(listIpDevicesOrigin.at(index));
        }
    }
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<QList<QString>>(listIpDevicesSelected);
    control()->newUserAction(Message.HIDE_ZONE_DISCOVER_IP_DEVICES, dataStruct);
}


void P_IPDevicesSelector::show(QWidget *zone) { Q_UNUSED(zone) }

void P_IPDevicesSelector::update() {}

QWidget *P_IPDevicesSelector::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
    default:
        return Q_NULLPTR;
    }
}



bool P_IPDevicesSelector::eventFilter(QObject *watched, QEvent *event){
    QWidget *widget = qobject_cast<QWidget *>(watched);
    if(widget == this->zone){
        switch (event->type()) {
        case QEvent::Resize:
        {
            QResizeEvent *resizeEvent = (QResizeEvent *)(event);
            QSize sizeNew = resizeEvent->size();
            Q_UNUSED(sizeNew)
        }
            break;
        default:
            return false;
        }
    }
    return QObject::eventFilter(watched, event);
}

void P_IPDevicesSelector::setListIpDevicesOrigin(const QList<QString> &value)
{
    listIpDevicesOrigin = value;
    Q_EMIT reloadListIpDevices();
}

