#ifndef A_IPDevicesSelector_H
#define A_IPDevicesSelector_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_ipdeviceselector.h"
class C_IPDevicesSelector;
class A_IPDevicesSelector : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_IPDevicesSelector(Control *ctrl);
    C_IPDevicesSelector *control() { return (C_IPDevicesSelector *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_IPDevicesSelector_H
