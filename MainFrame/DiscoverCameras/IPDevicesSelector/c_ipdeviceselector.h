#ifndef C_IPDevicesSelector_H
#define C_IPDevicesSelector_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "p_ipdeviceselector.h"
#include "a_ipdeviceselector.h"
#include "Camera/camsite.h"
class P_IPDevicesSelector;
class A_IPDevicesSelector;
class C_DiscoverCameras;
class C_IPDevicesSelector : public Control {
    Q_OBJECT
 public:
  QWidget* zone;
  AppContext* appContext;

  C_IPDevicesSelector(Control* ctrl, QWidget *_zone);
  C_DiscoverCameras* getParent() { return (C_DiscoverCameras*)this->parent; }
  P_IPDevicesSelector* presentation() { return (P_IPDevicesSelector*)pres; }
  A_IPDevicesSelector* abstraction() { return (A_IPDevicesSelector*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
Q_SIGNALS:
};

#endif  // C_IPDevicesSelector_H
