#ifndef IPDeviceCell_H
#define IPDeviceCell_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
class IPDeviceCell: public QWidget
{
    Q_OBJECT
public:
    QString ipDeviceCurrent;
    int widthLeft = 40;
    int widthCenter = 350;
    int widthRight = 250;
    QVBoxLayout *mainLayout = Q_NULLPTR;

    QHBoxLayout *containtLayout;
    QWidget *rightWidget = Q_NULLPTR;
    QHBoxLayout *rightLayout = Q_NULLPTR;
    QCheckBox *selectCamCheckBox = Q_NULLPTR;

    QWidget *centerWidget = Q_NULLPTR;
    QVBoxLayout *centerLayout = Q_NULLPTR;

    QWidget *leftWidget = Q_NULLPTR;
    QVBoxLayout *leftLayout = Q_NULLPTR;
    QLabel *iconLeftLabel = Q_NULLPTR;

    QWidget *topWidget = Q_NULLPTR;
    QVBoxLayout *topLayout = Q_NULLPTR;

    QWidget *bottomWidget = Q_NULLPTR;


    void updateBackground(QString background, QString color);
    IPDeviceCell(QWidget *parent, QString _ipDevice, bool selected = false);
    void updateSelected();
    bool isSelected();
protected:
    virtual void resizeEvent(QResizeEvent *event);
public Q_SLOTS:
    void checkBoxClicked(bool checked);
Q_SIGNALS:
    void selectedIpDevice(QString _ipDevice);
};

#endif // IPDeviceCell_H
