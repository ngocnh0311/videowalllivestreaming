#include "ipdevicecell.h"
#include "Common/resources.h"
IPDeviceCell::IPDeviceCell(QWidget *parent, QString _ipDevice, bool selected)
{
    this->ipDeviceCurrent = _ipDevice;
    mainLayout = new QVBoxLayout();
    mainLayout->setAlignment(Qt::AlignTop);
    mainLayout->setSpacing(0);
    mainLayout->setMargin(0);
    this->setLayout(mainLayout);

    topWidget = new QWidget(this);
    bottomWidget = new QWidget(this);
    bottomWidget->setFixedHeight(1);
    bottomWidget->setStyleSheet("background:white");

    mainLayout->addWidget(topWidget);
    mainLayout->addWidget(bottomWidget);

    containtLayout = new QHBoxLayout();
    this->topWidget->setLayout(containtLayout);
    containtLayout->setMargin(0);
    containtLayout->setSpacing(0);
    containtLayout->setAlignment(Qt::AlignLeft);
    this->setStyleSheet("background-color: #333; color: #ddd;");

    //init left widget
    leftWidget = new QWidget(topWidget);
    leftWidget->setStyleSheet("border-right:1px solid white");
    leftWidget->setFixedWidth(widthLeft);
    leftLayout = new QVBoxLayout();
    leftLayout->setSpacing(5);
    leftLayout->setMargin(5);
    leftLayout->setAlignment(Qt::AlignLeft);
    leftWidget->setLayout(leftLayout);

    iconLeftLabel = new QLabel(leftWidget);
    iconLeftLabel->setStyleSheet("border-right:none;");
    iconLeftLabel->setFont(Resources::instance().getExtraLargeRegularButtonFont());
    //    QPixmap iconLeftPixMap(":/images/res/icon-road.png");
    //    iconLeftLabel->setPixmap(iconLeftPixMap.scaled(25,25,Qt::KeepAspectRatio));
    QString iconCamera = "";
    iconLeftLabel->setText(iconCamera);
    leftLayout->addWidget(iconLeftLabel);

    //init center
    centerWidget = new QWidget(topWidget);
    centerWidget->setFixedWidth(widthCenter);

    centerLayout = new QVBoxLayout();
    centerLayout->setAlignment(Qt::AlignLeft);
    centerLayout->setSpacing(5);
    centerLayout->setMargin(5);
    centerWidget->setLayout(centerLayout);

    QWidget *ipDeviceCenterWidget = new QWidget(centerWidget);
    QHBoxLayout *ipDeviceCenterLayout = new QHBoxLayout;
    ipDeviceCenterLayout->setSpacing(5);
    ipDeviceCenterLayout->setMargin(5);
    ipDeviceCenterWidget->setLayout(ipDeviceCenterLayout);

    QLabel *ipDeviceTitleLabel = new QLabel(centerWidget);
    ipDeviceTitleLabel->setAlignment(Qt::AlignLeft);
    ipDeviceTitleLabel->setFixedSize(80,25);
    ipDeviceTitleLabel->setFont(Resources::instance().getMediumRegularButtonFont());
    ipDeviceTitleLabel->setText("Ip Device:");
    //ip device
    QLabel *ipDeviceLabel = new QLabel(centerWidget);
    ipDeviceLabel->setFont(Resources::instance().getMediumRegularButtonFont());
    ipDeviceLabel->setAlignment(Qt::AlignLeft);
    ipDeviceLabel->setText(ipDeviceCurrent);

    ipDeviceCenterLayout->addWidget(ipDeviceTitleLabel);
    ipDeviceCenterLayout->addWidget(ipDeviceLabel);

    centerLayout->addWidget(ipDeviceCenterWidget);

    //    //init right widget
    rightWidget = new QWidget(topWidget);
    rightWidget->setFixedWidth(widthRight);
    rightLayout = new QHBoxLayout();
    rightLayout->setSpacing(0);
    rightLayout->setMargin(0);
    rightLayout->setAlignment(Qt::AlignLeft);
    rightWidget->setLayout(rightLayout);

    QWidget *rightCheckBoxWidget = new QWidget(rightWidget);
    rightCheckBoxWidget->setFixedWidth(100);
    QHBoxLayout *rightCheckBoxLayout = new QHBoxLayout();
    rightCheckBoxLayout->setAlignment(Qt::AlignCenter);
    rightCheckBoxLayout->setSpacing(0);
    rightCheckBoxLayout->setMargin(0);
    rightCheckBoxWidget->setLayout(rightCheckBoxLayout);
    rightLayout->addWidget(rightCheckBoxWidget);

    QLabel *selectCamLabel = new QLabel(rightCheckBoxWidget);
    selectCamLabel->setFont(Resources::instance().getSmallRegularButtonFont());
    selectCamLabel->setText("Play: ");


    selectCamCheckBox = new QCheckBox(rightCheckBoxWidget);
    selectCamCheckBox->setStyleSheet("background:#fff;color:black");
    selectCamCheckBox->setChecked(selected);
    connect(selectCamCheckBox , &QCheckBox::clicked , this, &IPDeviceCell::checkBoxClicked);
    rightCheckBoxLayout->addWidget(selectCamLabel);
    rightCheckBoxLayout->addWidget(selectCamCheckBox);
    //resize widget
    leftWidget->move(0,0);
    centerWidget->move(leftWidget->size().width() , 0);
    rightWidget->move(parent->size().width() - leftWidget->size().width() - centerWidget->size().width() , 0);

    //add widget

    containtLayout->addWidget(leftWidget);
    containtLayout->addWidget(centerWidget);
    containtLayout->addWidget(rightWidget);
}

void IPDeviceCell::updateBackground(QString background, QString color){
    this->setStyleSheet("background-color : " + background +";" + "color:" + color);
    //update icon left if need
}

void IPDeviceCell::checkBoxClicked(bool checked){
    selectCamCheckBox->setChecked(checked);
    if(checked){
        Q_EMIT selectedIpDevice(ipDeviceCurrent);
    }
}

void IPDeviceCell::updateSelected(){
    selectCamCheckBox->setChecked(!selectCamCheckBox->isChecked());
}

bool IPDeviceCell::isSelected(){
    return selectCamCheckBox->isChecked();
}

void IPDeviceCell::resizeEvent(QResizeEvent *event){
    Q_UNUSED(event)
    //resize widget
    //    qDebug() << Q_FUNC_INFO << "SIZE CAMERA CELL" << this->size() << this->sizeHint();

}
