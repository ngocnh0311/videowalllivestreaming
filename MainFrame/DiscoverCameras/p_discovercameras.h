#ifndef P_DiscoverCameras_H
#define P_DiscoverCameras_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_discovercameras.h"
#include <QTextEdit>
#include <QPushButton>
#include <QLabel>
#include <QCheckBox>
#include <QStackedLayout>
#include <QMovie>
#include "Common/resources.h"
#include <QDialog>
class C_DiscoverCameras;
class DiscoverCameraWidget;
class OnvifDevice;
class P_DiscoverCameras : public Presentation {
    Q_OBJECT
    // init ui control
private:
public:
    DiscoverCameraWidget *discoverWidget = Q_NULLPTR;
    QWidget *zone = Q_NULLPTR;
    QStackedLayout *stacklayout = Q_NULLPTR;

    QWidget *loadingZone = Q_NULLPTR;
    QVBoxLayout *loadingLayout = Q_NULLPTR;
    QLabel *indicatorLoadingLabel = Q_NULLPTR;
    QMovie *loaderMovie = Q_NULLPTR;

    QWidget *filterWidget = Q_NULLPTR;
    QVBoxLayout *filterLayout = Q_NULLPTR;

    QWidget *ipDevicesWidget = Q_NULLPTR;

    QWidget *authenticLoadingZone = Q_NULLPTR;
    QVBoxLayout *authenticLoadingLayout = Q_NULLPTR;
    QLabel *authenticIndicatorLabel = Q_NULLPTR;
    QMovie *authenticLoaderMovie = Q_NULLPTR;


    P_DiscoverCameras(DiscoverCameraWidget *discoverWidget, Control *ctrl, QWidget *zone);
    C_DiscoverCameras *control() { return (C_DiscoverCameras *)ctrl; }

    void show();

    void update();

    QWidget *getZone(int zoneId);
    void showZoneFilterCameras();
    void showZoneDiscoverIpDevices();
    void hideZoneDiscoverIpDevices(QList<QString> listIpDevives);
    void showAuthenticLoadingZone();
    void adjustSize(QSize size);
public Q_SLOTS:
    void hide();
Q_SIGNALS:
    void startLoadInfoOfBlackListDevices(QList<OnvifDevice *> listOnvifDevices,QList<QString> listIpDevives);
protected:
    bool eventFilter(QObject *watched, QEvent *event);
};

#endif  // P_DiscoverCameras_H
