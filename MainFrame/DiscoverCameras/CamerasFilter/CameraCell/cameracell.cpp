#include "cameracell.h"
#include "Common/resources.h"
CameraCell::CameraCell(QWidget *parent, CamItem *_camItem, bool selected)
{
    this->camItemCurrent = _camItem;
    mainLayout = new QVBoxLayout();
    mainLayout->setAlignment(Qt::AlignTop);
    mainLayout->setSpacing(0);
    mainLayout->setMargin(0);
    this->setLayout(mainLayout);

    topWidget = new QWidget(this);
    bottomWidget = new QWidget(this);
    bottomWidget->setFixedHeight(1);
    bottomWidget->setStyleSheet("background:white");

    mainLayout->addWidget(topWidget);
    mainLayout->addWidget(bottomWidget);

    containtLayout = new QHBoxLayout();
    this->topWidget->setLayout(containtLayout);
    containtLayout->setMargin(0);
    containtLayout->setSpacing(0);
    containtLayout->setAlignment(Qt::AlignLeft);
    this->setStyleSheet("background-color: #333; color: #ddd;");

    //init left widget
    leftWidget = new QWidget(topWidget);
    leftWidget->setStyleSheet("border-right:1px solid white");
    leftWidget->setFixedWidth(widthLeft);
    leftLayout = new QVBoxLayout();
    leftLayout->setSpacing(5);
    leftLayout->setMargin(5);
    leftLayout->setAlignment(Qt::AlignLeft);
    leftWidget->setLayout(leftLayout);

    iconLeftLabel = new QLabel(leftWidget);
    iconLeftLabel->setStyleSheet("border-right:none;");
    iconLeftLabel->setFont(Resources::instance().getExtraLargeRegularButtonFont());
    //    QPixmap iconLeftPixMap(":/images/res/icon-road.png");
    //    iconLeftLabel->setPixmap(iconLeftPixMap.scaled(25,25,Qt::KeepAspectRatio));
    QString iconCamera = "";
    iconLeftLabel->setText(iconCamera);
    leftLayout->addWidget(iconLeftLabel);

    //init center
    centerWidget = new QWidget(topWidget);
    centerWidget->setFixedWidth(widthCenter);

    centerLayout = new QVBoxLayout();
    centerLayout->setAlignment(Qt::AlignLeft);
    centerLayout->setSpacing(5);
    centerLayout->setMargin(5);
    centerWidget->setLayout(centerLayout);

    centerTopWidget = new QWidget(centerWidget);
    centerTopLayout = new QHBoxLayout();
    centerTopWidget->setLayout(centerTopLayout);

    centerBottomWidget = new QWidget(centerWidget);
    centerBottomLayout = new QHBoxLayout();
    centerBottomWidget->setLayout(centerBottomLayout);

    centerLayout->addWidget(centerTopWidget);
    centerLayout->addWidget(centerBottomWidget);

    QWidget *ipDeviceCenterWidget = new QWidget(centerTopWidget);
    QHBoxLayout *ipDeviceCenterLayout = new QHBoxLayout;
    ipDeviceCenterLayout->setSpacing(5);
    ipDeviceCenterLayout->setMargin(5);
    ipDeviceCenterWidget->setLayout(ipDeviceCenterLayout);

    QLabel *ipDeviceTitleLabel = new QLabel(ipDeviceCenterWidget);
    ipDeviceTitleLabel->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    ipDeviceTitleLabel->setStyleSheet("background:green");
    ipDeviceTitleLabel->setFixedSize(150,25);
    ipDeviceTitleLabel->setFont(Resources::instance().getMediumRegularButtonFont());
    ipDeviceTitleLabel->setText("Ip Device:");
    //ip device
    QLabel *ipDeviceLabel = new QLabel(ipDeviceCenterWidget);
    ipDeviceLabel->setFont(Resources::instance().getMediumRegularButtonFont());
    ipDeviceLabel->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    ipDeviceLabel->setText(camItemCurrent->getPostion());

    ipDeviceCenterLayout->addWidget(ipDeviceTitleLabel);
    ipDeviceCenterLayout->addWidget(ipDeviceLabel);


    QWidget *macAddressCenterWidget = new QWidget(centerTopWidget);
    QHBoxLayout *macAddressCenterLayout = new QHBoxLayout;
    macAddressCenterLayout->setSpacing(5);
    macAddressCenterLayout->setMargin(5);
    macAddressCenterWidget->setLayout(macAddressCenterLayout);

    //mac
    QLabel *macAddressTitleLabel = new QLabel(macAddressCenterWidget);
    macAddressTitleLabel->setFixedSize(150,25);

    macAddressTitleLabel->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    macAddressTitleLabel->setFont(Resources::instance().getMediumRegularButtonFont());
    macAddressTitleLabel->setText("Mac Address:");

    QLabel *macAddressLabel = new QLabel(macAddressCenterWidget);
    macAddressLabel->setFont(Resources::instance().getSmallRegularButtonFont());
    macAddressLabel->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    macAddressLabel->setText("a0:c5:89:23:47:3b");

    macAddressCenterLayout->addWidget(macAddressTitleLabel);
    macAddressCenterLayout->addWidget(macAddressLabel);


    centerTopLayout->addWidget(ipDeviceCenterWidget);
    centerTopLayout->addWidget(macAddressCenterWidget);


    //username
    QWidget *usernameDeviceWidget = new QWidget(centerBottomWidget);
    QHBoxLayout *usernameDeviceLayout = new QHBoxLayout;
    usernameDeviceLayout->setSpacing(5);
    usernameDeviceLayout->setMargin(5);
    usernameDeviceWidget->setLayout(usernameDeviceLayout);

    QLabel *usernameTitleLabel = new QLabel(usernameDeviceWidget);
    usernameTitleLabel->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    usernameTitleLabel->setStyleSheet("background:green");
    usernameTitleLabel->setFixedSize(150,25);
    usernameTitleLabel->setFont(Resources::instance().getMediumRegularButtonFont());
    usernameTitleLabel->setText("Username:");

    QTextEdit *usernameTextEdit = new QTextEdit();
    usernameTextEdit->setFixedSize(150,25);
    usernameTextEdit->setFont(Resources::instance().getMediumRegularButtonFont());
    usernameTextEdit->setPlaceholderText("Username");

    usernameDeviceLayout->addWidget(usernameTitleLabel);
    usernameDeviceLayout->addWidget(usernameTextEdit);

    //password
    QWidget *passwordDeviceWidget = new QWidget(centerBottomWidget);
    QHBoxLayout *passwordDeviceLayout = new QHBoxLayout;
    passwordDeviceLayout->setSpacing(5);
    passwordDeviceLayout->setMargin(5);
    passwordDeviceWidget->setLayout(passwordDeviceLayout);

    QLabel *passwordTitleLabel = new QLabel(passwordDeviceWidget);
    passwordTitleLabel->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    passwordTitleLabel->setStyleSheet("background:green");
    passwordTitleLabel->setFixedSize(150,25);
    passwordTitleLabel->setFont(Resources::instance().getMediumRegularButtonFont());
    passwordTitleLabel->setText("Password:");

    QTextEdit *passwordTextEdit = new QTextEdit();
    passwordTextEdit->setFixedSize(150,25);
    passwordTextEdit->setFont(Resources::instance().getMediumRegularButtonFont());
    passwordTextEdit->setPlaceholderText("password");

    passwordDeviceLayout->addWidget(passwordTitleLabel);
    passwordDeviceLayout->addWidget(passwordTextEdit);

    centerBottomLayout->addWidget(usernameDeviceWidget);
    centerBottomLayout->addWidget(passwordDeviceWidget);

    //    //init right widget
    rightWidget = new QWidget(topWidget);
    rightWidget->setFixedWidth(widthRight);
    rightLayout = new QHBoxLayout();
    rightLayout->setSpacing(0);
    rightLayout->setMargin(0);
    rightLayout->setAlignment(Qt::AlignLeft);
    rightWidget->setLayout(rightLayout);

    QWidget *rightCheckBoxWidget = new QWidget(rightWidget);
    rightCheckBoxWidget->setFixedWidth(100);
    QHBoxLayout *rightCheckBoxLayout = new QHBoxLayout();
    rightCheckBoxLayout->setAlignment(Qt::AlignCenter);
    rightCheckBoxLayout->setSpacing(0);
    rightCheckBoxLayout->setMargin(0);
    rightCheckBoxWidget->setLayout(rightCheckBoxLayout);

    QLabel *selectCamLabel = new QLabel(rightCheckBoxWidget);
    selectCamLabel->setFont(Resources::instance().getSmallRegularButtonFont());
    selectCamLabel->setText("Play: ");


    selectCamCheckBox = new QCheckBox(rightCheckBoxWidget);
    selectCamCheckBox->setStyleSheet("background:#fff;color:black");
    selectCamCheckBox->setChecked(selected);
    connect(selectCamCheckBox , &QCheckBox::clicked , this, &CameraCell::checkBoxClicked);
    rightCheckBoxLayout->addWidget(selectCamLabel);
    rightCheckBoxLayout->addWidget(selectCamCheckBox);


    QWidget *rightConfigRecordWidget = new QWidget(rightWidget);
    QVBoxLayout *rightConfigRecordLayout = new QVBoxLayout();
    rightConfigRecordLayout->setAlignment(Qt::AlignCenter);
    rightConfigRecordLayout->setSpacing(15);
    rightConfigRecordLayout->setMargin(0);
    rightConfigRecordWidget->setLayout(rightConfigRecordLayout);


    rightLayout->addWidget(rightCheckBoxWidget);
    rightLayout->addWidget(rightConfigRecordWidget);

    QPushButton *configRecordButton = new QPushButton(rightConfigRecordWidget);
    configRecordButton->setFont(Resources::instance().getSmallRegularButtonFont());
    configRecordButton->setFixedSize(100,35);
    configRecordButton->setText("Cấu hình lưu");
    rightConfigRecordLayout->addWidget(configRecordButton);

    QPushButton *rediscoverButton = new QPushButton(rightConfigRecordWidget);
    rediscoverButton->setFont(Resources::instance().getSmallRegularButtonFont());
    rediscoverButton->setFixedSize(100,35);
    rediscoverButton->setText("Quét lại");
    rightConfigRecordLayout->addWidget(rediscoverButton);

    //resize widget
    leftWidget->move(0,0);
    centerWidget->move(leftWidget->size().width() , 0);
    rightWidget->move(parent->size().width() - leftWidget->size().width() - centerWidget->size().width() , 0);



    //    leftWidget->resize(widthLeft , this->topWidget->size().height());
    //    centerWidget->resize(parent->size().width() - widthLeft - widthRight , this->topWidget->size().height());
    //    rightWidget->resize(widthRight , this->topWidget->size().height());

    //add widget

    containtLayout->addWidget(leftWidget);
    containtLayout->addWidget(centerWidget);
    containtLayout->addWidget(rightWidget);
}

void CameraCell::updateBackground(QString background, QString color){
    this->setStyleSheet("background-color : " + background +";" + "color:" + color);
    //update icon left if need
}

void CameraCell::checkBoxClicked(bool checked){
    selectCamCheckBox->setChecked(checked);
    if(checked){
        Q_EMIT selectedCamera(camItemCurrent->getCameraId());
    }
}

void CameraCell::updateSelected(){
    selectCamCheckBox->setChecked(!selectCamCheckBox->isChecked());
}

void CameraCell::resizeEvent(QResizeEvent *event){
    Q_UNUSED(event)
    //resize widget
    //    qDebug() << Q_FUNC_INFO << "SIZE CAMERA CELL" << this->size() << this->sizeHint();

}
