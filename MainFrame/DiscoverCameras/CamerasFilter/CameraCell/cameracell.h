#ifndef CAMERACELL_H
#define CAMERACELL_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include "Camera/camitem.h"
#include <QCheckBox>
#include <QTextEdit>
class CameraCell: public QWidget
{
    Q_OBJECT
public:
    int widthLeft = 40;
    int widthCenter = 500;
    int widthRight = 260;
    QVBoxLayout *mainLayout = Q_NULLPTR;

    QHBoxLayout *containtLayout;
    QWidget *rightWidget = Q_NULLPTR;
    QHBoxLayout *rightLayout = Q_NULLPTR;
    QCheckBox *selectCamCheckBox = Q_NULLPTR;

    QWidget *centerWidget = Q_NULLPTR;
    QVBoxLayout *centerLayout = Q_NULLPTR;

    QWidget *centerTopWidget = Q_NULLPTR;
    QHBoxLayout *centerTopLayout = Q_NULLPTR;

    QWidget *centerBottomWidget = Q_NULLPTR;
    QHBoxLayout *centerBottomLayout = Q_NULLPTR;


    QWidget *leftWidget = Q_NULLPTR;
    QVBoxLayout *leftLayout = Q_NULLPTR;
    QLabel *iconLeftLabel = Q_NULLPTR;

    QWidget *topWidget = Q_NULLPTR;
    QVBoxLayout *topLayout = Q_NULLPTR;

    QWidget *bottomWidget = Q_NULLPTR;


    void updateBackground(QString background, QString color);
    CamItem *camItemCurrent = Q_NULLPTR;
    CameraCell(QWidget *parent, CamItem *_camItem, bool selected = false);
    void updateSelected();
protected:
    virtual void resizeEvent(QResizeEvent *event);
public Q_SLOTS:
    void checkBoxClicked(bool checked);
Q_SIGNALS:
    void selectedCamera(int cameraId);
};

#endif // CAMERACELL_H
