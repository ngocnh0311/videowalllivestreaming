#ifndef C_CameraFilter_H
#define C_CameraFilter_H
#include <QString>

#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QWidget>
#include "Authentication/appcontext.h"
#include "PacModel/control.h"
#include "message.h"
#include "p_camerafilter.h"
#include "a_camerafilter.h"
#include "Camera/camsite.h"
class P_CameraFilter;
class A_CameraFilter;
class C_MainFrame;
class C_CameraFilter : public Control {
    Q_OBJECT
 public:
  QWidget* zone;
  AppContext* appContext;

  CamSite *camSite;
  C_CameraFilter(Control* ctrl, QWidget *_zone);
  C_MainFrame* getParent() { return (C_MainFrame*)this->parent; }
  P_CameraFilter* presentation() { return (P_CameraFilter*)pres; }
  A_CameraFilter* abstraction() { return (A_CameraFilter*)abst; }
  /**
        * Method to receive a message from the Presentation Facet.
        * @param message    : A string which describe the request
        * @param attachment : A ref on an eventual object necessary to treat the
    *request
        **/
  void newUserAction(int message, QVariant* attachment) override;
  /**
    * Method to receive a message from the Astraction Facet.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newSystemAction(int message, QVariant* attachment) override;

  /**
    * Method to receive a message from an other agent.
    * @param message    : A string which describe the request
    * @param attachment : A ref on an eventual object necessary to treat the
    *request
    **/
  void newAction(int message, QVariant* attachment) override;
  void setCamSite(CamSite *value);
Q_SIGNALS:
  void reloadListCameraFilter();
};

#endif  // C_CameraFilter_H
