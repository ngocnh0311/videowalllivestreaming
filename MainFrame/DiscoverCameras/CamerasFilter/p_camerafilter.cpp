﻿#include "p_camerafilter.h"

/**
     * Generic method to override for updating the presention.
     **/

void P_CameraFilter::setListCameras(const QList<CamItem *> &value)
{
    listCameras = value;
}

P_CameraFilter::P_CameraFilter(Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
    // init gui object
    this->zone = _zone;
    this->zone->setStyleSheet("background:#ffffff; color:white");
    layout = new QVBoxLayout();
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    zone->setLayout(layout);

    topWidget = new QWidget(this->zone);
    topWidget->setStyleSheet("background:#EFEFEF");
    topWidget->setFixedHeight(heightTop);
    topLayout = new QVBoxLayout();
    topLayout->setAlignment(Qt::AlignHCenter);
    topLayout->setSpacing(10);
    topLayout->setContentsMargins(10,0,10,0);
    topWidget->setLayout(topLayout);

    searchLineEdit = new QLineEdit(topWidget);
    searchLineEdit->setMaximumHeight(30);
    searchLineEdit->setMinimumHeight(30);
    searchLineEdit->setStyleSheet("background : #ffffff;color:#4f4f4f");
    searchLineEdit->setTextMargins(5, 2, 5, 2);
    searchLineEdit->setPlaceholderText("Tìm kiếm...");
    searchLineEdit->setFont(Resources::instance().getMediumRegularButtonFont());
    topLayout->addWidget(searchLineEdit);

    centerWidget = new QWidget(this->zone);
    centerLayout = new QVBoxLayout();
    centerLayout->setSpacing(0);
    centerLayout->setMargin(0);
    centerWidget->setLayout(centerLayout);
    listCamerasWidget = new QListWidget(centerWidget);
    listCamerasWidget->setStyleSheet("QScrollBar:vertical{padding-left:1px;width:6px}QScrollBar::handle:vertical{background:#008098;border-radius:2px;min-height:10px}QScrollBar::add-line:horizontal,QScrollBar::add-line:vertical,QScrollBar::sub-line:horizontal,QScrollBar::sub-line:vertical{border:none}QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical{background:#383838;margin:0 2px}QScrollBar::down-arrow:vertical,QScrollBar::up-arrow:vertical{background:0 0}QScrollBar:horizontal{height:6px;padding-top:1px}QScrollBar::handle:horizontal{background:#008098;border-radius:2px;min-width:10px}QScrollBar::add-page:horizontal,QScrollBar::sub-page:horizontal{background:#383838;margin:2px 0}QScrollBar::left-arrow:horizontal,QScrollBar::right-arrow:horizontal{background:0 0}");
    listCamerasWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    connect(listCamerasWidget, &QListWidget::itemClicked, this , &P_CameraFilter::onListCameraItemClicked);

    centerLayout->addWidget(listCamerasWidget);

    loadListCamera();
    bottomWidget = new QWidget(this->zone);
    bottomWidget->setFixedHeight(heightBottom);
    bottomLayout = new QVBoxLayout();
    bottomLayout->setSpacing(5);
    bottomLayout->setAlignment(Qt::AlignCenter);
    bottomLayout->setMargin(0);
    bottomWidget->setLayout(bottomLayout);

    QPushButton *submitButton = new QPushButton(bottomWidget);
    submitButton->setStyleSheet("background:#333;color:white");
    submitButton->setFixedSize(130, 35);
    submitButton->setText("Đồng ý");
    connect(submitButton, &QPushButton::clicked, this,&P_CameraFilter::closeDiscoverDialog);
    bottomLayout->addWidget(submitButton);

    layout->addWidget(topWidget);
    layout->addWidget(centerWidget);
    layout->addWidget(bottomWidget);

}

void P_CameraFilter::loadListCamera(){
    listCamerasWidget->clear();
    for (int index  = 0; index < listCameraCells.size(); ++index) {
        CameraCell* cameraCellWidget = listCameraCells.takeAt(index);
        delete cameraCellWidget;
    }
    for (int index  = 0; index < listItemCellCamera.size(); ++index) {
        QListWidgetItem* item = listItemCellCamera.takeAt(index);
        delete item;
    }

    for ( int index = 0 ; index < listCameras.size() ; ++index )
    {
        CameraCell* cameraCellWidget = new CameraCell(centerWidget, listCameras.at(index));
        if(index == 0){
            cameraCellWidget->updateBackground("#555" , "#FFF");
        }

        listCameraCells.append(cameraCellWidget);
        QListWidgetItem* listItem = new QListWidgetItem();
        listItemCellCamera.append(listItem);
        listItem->setSizeHint(cameraCellWidget->sizeHint());
        listCamerasWidget->addItem(listItem);
        listCamerasWidget->setItemWidget(listItem, cameraCellWidget);
    }
}

void P_CameraFilter::onListCameraItemClicked(QListWidgetItem *item){
    qDebug() <<Q_FUNC_INFO;

    if(item){
        int indexItemSelected = listItemCellCamera.indexOf(item);
        qDebug() <<Q_FUNC_INFO << "indexItemSelected" <<indexItemSelected;
//        if(indexItemSelected == indexItemSelectedLast) return;
        indexItemSelectedLast =  indexItemSelected;

        //reset color of cell before
        for (int index = 0; index < listCameraCells.size(); ++index) {
            CameraCell *cameraCell = listCameraCells.at(index);
            cameraCell->updateBackground("#333", "#ddd");
        }

        CameraCell *cameraCell = listCameraCells.at(indexItemSelected);
        cameraCell->updateBackground("#555" , "#FFF");
        cameraCell->updateSelected();
    }
}

void P_CameraFilter::closeDiscoverDialog(){
    for (int index = 0; index < listCameraCells.size(); ++index) {
        CameraCell *cameraCell = listCameraCells.at(index);
        if(cameraCell){

        }
    }
//    OnvifUtils::instances()->getDialogDiscover()->close();
}


void P_CameraFilter::show(QWidget *zone) { Q_UNUSED(zone) }

void P_CameraFilter::update() {}

QWidget *P_CameraFilter::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
    default:
        return Q_NULLPTR;
    }
}



bool P_CameraFilter::eventFilter(QObject *watched, QEvent *event){
    QWidget *widget = qobject_cast<QWidget *>(watched);
    if(widget == this->zone){
        switch (event->type()) {
        case QEvent::Resize:
        {
             QResizeEvent *resizeEvent = (QResizeEvent *)(event);
             QSize sizeNew = resizeEvent->size();

        }
            break;
        default:
            return false;
        }
    }
    return QObject::eventFilter(watched, event);
}


