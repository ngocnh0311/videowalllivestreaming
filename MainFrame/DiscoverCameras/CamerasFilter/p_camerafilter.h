#ifndef P_CameraFilter_H
#define P_CameraFilter_H

#include <PacModel/presentation.h>
#include <QAbstractItemView>
#include <QEvent>
#include <QFont>
#include <QHoverEvent>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include "Common/generaldefine.h"
#include "c_camerafilter.h"
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QListWidget>
#include "CameraCell/cameracell.h"
#include "Camera/camitem.h"
class C_CameraFilter;
class P_CameraFilter : public Presentation {
    Q_OBJECT
    // init ui control
private:
public:
    QList<CamItem *> listCamsSelected;
    int indexItemSelectedLast;
    QList<CamItem *> listCameras;
    int heightTop = 50;
    int heightBottom = 50;

    QWidget *zone = Q_NULLPTR;
    QVBoxLayout *layout = Q_NULLPTR;

    QWidget *topWidget = Q_NULLPTR;
    QVBoxLayout *topLayout = Q_NULLPTR;
    QLineEdit *searchLineEdit = Q_NULLPTR;


    QWidget *centerWidget = Q_NULLPTR;
    QVBoxLayout *centerLayout = Q_NULLPTR;

    QListWidget *listCamerasWidget;
    QList<QListWidgetItem *> listItemCellCamera;
    QList<CameraCell *> listCameraCells;

    QWidget *bottomWidget = Q_NULLPTR;
    QVBoxLayout *bottomLayout = Q_NULLPTR;


    P_CameraFilter(Control *ctrl, QWidget *zone);

    void show(QWidget *zone);
    void update();

    QWidget *getZone(int zoneId);

    C_CameraFilter *control() { return (C_CameraFilter *)ctrl; }

    void setListCameras(const QList<CamItem *> &value);

protected:
    bool eventFilter(QObject *watched, QEvent *event);

public Q_SLOTS:
    void loadListCamera();
    void closeDiscoverDialog();
    void onListCameraItemClicked(QListWidgetItem *item);
};

#endif  // P_CameraFilter_H
