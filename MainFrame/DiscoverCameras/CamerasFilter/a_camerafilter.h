#ifndef A_CameraFilter_H
#define A_CameraFilter_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_camerafilter.h"
class C_CameraFilter;
class A_CameraFilter : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_CameraFilter(Control *ctrl);
    C_CameraFilter *control() { return (C_CameraFilter *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_CameraFilter_H
