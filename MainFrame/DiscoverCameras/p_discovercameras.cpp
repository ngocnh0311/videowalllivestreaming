#include "p_discovercameras.h"
#include "discovercamerawidget.h"

/**
     * Generic method to override for updating the presention.
     **/
P_DiscoverCameras::P_DiscoverCameras(DiscoverCameraWidget *_discoverWidget, Control *ctrl, QWidget *_zone) : Presentation(ctrl) {
//    OnvifUtils *onvif = OnvifUtils::instances();
//    connect(this, &P_DiscoverCameras::startLoadInfoOfBlackListDevices, onvif, &OnvifUtils::loadInfoOfBlackListDevices);
    discoverWidget = _discoverWidget;
    // init gui object
    this->zone = _zone;
    this->zone->setFixedSize(800,500);

    stacklayout = new QStackedLayout();
    stacklayout->setSpacing(0);
    stacklayout->setMargin(0);
    stacklayout->setAlignment(Qt::AlignCenter);
    zone->setLayout(stacklayout);
    zone->setStyleSheet("background:#f2f1f0; color:black");

    loadingZone = new QWidget(this->zone);
    loadingZone->setStyleSheet("background:#f2f1f0");
    loadingLayout = new QVBoxLayout();
    loadingLayout->setAlignment(Qt::AlignCenter);
    loadingLayout->setSpacing(10);
    loadingLayout->setMargin(0);
    loadingZone->setLayout(loadingLayout);

    this->indicatorLoadingLabel = new QLabel(this->zone);
    this->indicatorLoadingLabel->setStyleSheet("background:#00000000");
    this->indicatorLoadingLabel->setFont(Resources::instance().getMediumBoldButtonFont());
    this->loaderMovie = new QMovie(":/images/res/discovercameraloading.gif");
    this->loaderMovie->setSpeed(100);
    this->loaderMovie->start();
    this->indicatorLoadingLabel->setAlignment(Qt::AlignCenter);
    this->indicatorLoadingLabel->setMovie(this->loaderMovie);

    QLabel *loadingLabel = new QLabel(loadingZone);
    loadingLabel->setFont(Resources::instance().getMediumBoldButtonFont());
    loadingLabel->setText("Đang tìm kiếm camera trong mạng...");

    QPushButton *quitApp = new QPushButton(this->zone);
    quitApp->setFont(Resources::instance().getMediumBoldButtonFont());
    quitApp->setFixedHeight(30);
    quitApp->setText("Thoát ứng dụng");
    connect(quitApp, &QPushButton::clicked, this, [this]{
        qApp->quit();
        QProcess process;
        process.execute(QString("kill %1").arg(
                            QApplication::QCoreApplication::applicationPid()));
    });


    loadingLayout->addWidget(indicatorLoadingLabel);
    loadingLayout->addWidget(loadingLabel);
    loadingLayout->addWidget(quitApp);

    filterWidget = new QWidget(this->zone);
    ipDevicesWidget = new QWidget(this->zone);

    authenticLoadingZone = new QWidget(this->zone);
    authenticLoadingZone->setStyleSheet("background:#f2f1f0");
    authenticLoadingLayout = new QVBoxLayout();
    authenticLoadingLayout->setAlignment(Qt::AlignCenter);
    authenticLoadingLayout->setSpacing(10);
    authenticLoadingLayout->setMargin(0);
    authenticLoadingZone->setLayout(authenticLoadingLayout);

    this->authenticIndicatorLabel = new QLabel(this->zone);
    this->authenticIndicatorLabel->setStyleSheet("background:#00000000");
    this->authenticIndicatorLabel->setFont(Resources::instance().getMediumBoldButtonFont());
    this->authenticLoaderMovie = new QMovie(":/images/res/discovercameraloading.gif");
    this->authenticLoaderMovie->setSpeed(100);
    this->authenticLoaderMovie->start();
    this->authenticIndicatorLabel->setAlignment(Qt::AlignCenter);
    this->authenticIndicatorLabel->setMovie(this->authenticLoaderMovie);

    QLabel *authenticLoadingLabel = new QLabel(loadingZone);
    authenticLoadingLabel->setFont(Resources::instance().getMediumBoldButtonFont());
    authenticLoadingLabel->setText("Đang xác thực camera, Vui lòng chờ ...");

    QPushButton *quitAppQuick = new QPushButton(this->zone);
    quitAppQuick->setFont(Resources::instance().getMediumBoldButtonFont());
    quitAppQuick->setFixedHeight(30);
    quitAppQuick->setText("Thoát ứng dụng");
    connect(quitAppQuick, &QPushButton::clicked, this, [this]{
        qApp->quit();
        QProcess process;
        process.execute(QString("kill %1").arg(
                            QApplication::QCoreApplication::applicationPid()));
    });

    authenticLoadingLayout->addWidget(authenticIndicatorLabel);
    authenticLoadingLayout->addWidget(authenticLoadingLabel);
    authenticLoadingLayout->addWidget(quitAppQuick);

    stacklayout->addWidget(loadingZone);
    stacklayout->addWidget(authenticLoadingZone);
    stacklayout->addWidget(filterWidget);
    stacklayout->addWidget(ipDevicesWidget);
    stacklayout->setCurrentWidget(loadingZone);
}

void P_DiscoverCameras::showAuthenticLoadingZone(){
    adjustSize(QSize(800,500));
    stacklayout->setCurrentWidget(authenticLoadingZone);
    this->discoverWidget->updateDialog("Chờ xác thực thông tin camera");
}

void P_DiscoverCameras::showZoneDiscoverIpDevices(){
    adjustSize(QSize(800,500));
    stacklayout->setCurrentWidget(ipDevicesWidget);
    this->discoverWidget->updateDialog("Danh sách IP tìm thấy");
}

void P_DiscoverCameras::adjustSize(QSize size){
    this->zone->resize(size);
    loadingZone->resize(size);
    authenticLoadingZone->resize(size);
    filterWidget->resize(size);
    ipDevicesWidget->resize(size);
}

void P_DiscoverCameras::hideZoneDiscoverIpDevices(QList<QString> listIpDevives){
    showAuthenticLoadingZone();
    QList<OnvifDevice *> listOnvifDevices;
    //start authentic with camera
    Q_EMIT startLoadInfoOfBlackListDevices(listOnvifDevices, listIpDevives);
}

void P_DiscoverCameras::showZoneFilterCameras(){
    adjustSize(QSize(800,500));
    stacklayout->setCurrentWidget(filterWidget);
    this->discoverWidget->updateDialog("Danh sách camera xác thực được");
}

void P_DiscoverCameras::show() {
    this->zone->show();
}
void P_DiscoverCameras::hide() { this->zone->close(); }

void P_DiscoverCameras::update() {}

QWidget *P_DiscoverCameras::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return ipDevicesWidget;
    case 2:
        return filterWidget;
    default:
        return Q_NULLPTR;
    }
}

bool P_DiscoverCameras::eventFilter(QObject *watched, QEvent *event){
    QWidget *widget = qobject_cast<QWidget *>(watched);
    if(widget == this->zone){
        switch (event->type()) {
        case QEvent::Resize:
        {
            QResizeEvent *resizeEvent = (QResizeEvent *)(event);
            QSize sizeNew = resizeEvent->size();
            loadingZone->resize(sizeNew);
            filterWidget->resize(sizeNew);
            loadingZone->updateGeometry();
            filterWidget->updateGeometry();
        }
            break;
        default:
            break;
        }
    }
    return QObject::eventFilter(watched, event);
}



