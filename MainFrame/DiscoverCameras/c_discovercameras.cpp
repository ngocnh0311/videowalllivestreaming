#include "c_discovercameras.h"
#include "MainFrame/c_mainframe.h"
#include "MainFrame/DiscoverCameras/CamerasFilter/c_camerafilter.h"
#include "MainFrame/DiscoverCameras/IPDevicesSelector/c_ipdeviceselector.h"
/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_IPDevicesSelector *C_DiscoverCameras::getCIPDevicesSelector() const
{
    return cIPDevicesSelector;
}

C_DiscoverCameras::C_DiscoverCameras(DiscoverCameraWidget *discoverWidget, Control* ctrl, QWidget *_zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();
    this->zone = _zone;

    this->abst = new A_DiscoverCameras(this);
    this->pres = new P_DiscoverCameras(discoverWidget, this, zone);

    cIPDevicesSelector = new C_IPDevicesSelector(this, this->presentation()->getZone(1));
    cCameraFilter = new C_CameraFilter(this, this->presentation()->getZone(2));

    // create others controls
    // cXXX = new C_XXX();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_DiscoverCameras::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case 1:
        break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_DiscoverCameras::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_DiscoverCameras::newAction(int message, QVariant* attachment) {
    switch (message) {


    case Message.SHOW_ZONE_FILTER_DISCOVER_CAMERAS:{
        CamSite *camSite = attachment->value<CamSite *>();
        presentation()->showZoneFilterCameras();
        cCameraFilter->setCamSite(camSite);
    }break;

    case Message.SHOW_ZONE_DISCOVER_IP_DEVICES:{
        presentation()->showZoneDiscoverIpDevices();
    }break;

    case Message.HIDE_ZONE_DISCOVER_IP_DEVICES:
    {
        QList<QString> listIpDevives = attachment->value<QList<QString>>();
        presentation()->hideZoneDiscoverIpDevices(listIpDevives);
    }break;

    case Message.APP_CONTEXT_GET:{
        attachment->setValue(appContext);
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
