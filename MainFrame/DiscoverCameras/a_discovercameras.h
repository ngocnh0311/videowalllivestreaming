#ifndef A_DiscoverCameras_H
#define A_DiscoverCameras_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_discovercameras.h"
class C_DiscoverCameras;
class A_DiscoverCameras : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_DiscoverCameras(Control *ctrl);
    C_DiscoverCameras *control() { return (C_DiscoverCameras *)this->ctrl; }
    void changeControl(Control *ctrl);
public Q_SLOTS:

};

#endif  // A_DiscoverCameras_H
