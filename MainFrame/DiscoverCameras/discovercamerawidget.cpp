#include "discovercamerawidget.h"
#include <QApplication>
#include "Common/resources.h"
DiscoverCameraWidget::DiscoverCameraWidget()
{
    setWindowFlags(Qt::Widget);
    QString title = "Quét danh sách camera trong nội bộ";
    updateDialog(title);
    qDebug() << "";
}

void DiscoverCameraWidget::updateDialog(QString title){
    QSize screenSize = Resources::instance().getScreenSize();
    this->setWindowTitle(title);
    this->move((screenSize.width() - this->width()) / 2,
                                 (screenSize.height() - this->height()) / 2);
}
QWidget *DiscoverCameraWidget::getZone(){
    return this;
}

void DiscoverCameraWidget::closeEvent (QCloseEvent *event){
    qDebug() << Q_FUNC_INFO << "closed";
//    qApp->quit();
//    QProcess process;
//    process.execute(QString("kill %1").arg(
//                        QApplication::QCoreApplication::applicationPid()));
}
