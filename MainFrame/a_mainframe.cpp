#include "MainFrame/a_mainframe.h"
/**
 * Constructor for Abstraction facet. Register the control.
 * @param Crtl A ref on the control facet which manage this facet
 **/
CamSite* A_MainFrame::getSiteCameras() const { return siteCameras; }

void A_MainFrame::setSiteCameras(CamSite* value) { siteCameras = value; }

A_MainFrame::A_MainFrame(Control* ctrl) : Abstraction(ctrl) {
    layoutSet = new LayoutSet(Q_NULLPTR);
    this->loadUserApps();
    connect(this, &A_MainFrame::downloadedAllListSites, this, &A_MainFrame::loadCamerasOfSite);

}
void A_MainFrame::loadCamerasOfSite(bool updateData ,SiteChild *userSites){
    QString token = this->control()->appContext->getWorkingUser()->getToken();
    QList<Site *> listSitesUser;
    listSitesUser =  userSites->getListSite();
    for (int index = 0; index < listSitesUser.size(); ++index) {
        loadAllCamerasOfSiteSaveToDisk(updateData ,token, listSitesUser[index]->getSiteId(),[this](QJsonObject jsonObject){

        },[this](QString message){
            qDebug() << "error load data of site";
        });
    }
}

//load with api 003 (load all cameras of site)
void A_MainFrame::loadAllCamerasOfSiteSaveToDisk(bool updateData ,QString token, int siteId, std::function<void(QJsonObject)> onSuccess, std::function<void(QString)> onFailure){

    std::function<void(QJsonObject)> onFetchSucess = [this ,onSuccess, siteId, updateData](QJsonObject jsonObject){
        if(!jsonObject.isEmpty()){
            LoadDataLocal::instance().saveDataCamerasOfSiteToDisk(siteId, jsonObject);
            QJsonObject jsonObjectSave = LoadDataLocal::instance().loadDataCamerasOfSiteToDisk(siteId);
            if((updateData && this->control()->appContext->getWorkingSite()->getSiteId()) == siteId && (jsonObjectSave != jsonObject)){
                control()->newSystemAction(Message.APP_VIDEO_WALL_LAYOUT_DEFAULT_SET,
                                           Q_NULLPTR);
                control()->newSystemAction(Message.APP_PLAY_BACK_LAYOUT_DEFAULT_SET,
                                           Q_NULLPTR);
            }
        }
    };

    std::function<void(QString)> onFetchFailure = [this, onFailure](QString message){
        qDebug() << "Load camera of site error";
    };
    CamSite::getCamerasOfSiteSaveToLocal(siteId, token, onFetchSucess, onFetchFailure);
}


void A_MainFrame::connectToSocket() {
    Connector* connector = new Connector();
    connect(connector, &Connector::socketConnected, this,
            &A_MainFrame::onSocketConnected);
    connect(connector, &Connector::socketDisconnected, this,
            &A_MainFrame::onSocketDisconnected);
    connect(connector, &Connector::socketJoined, this,
            &A_MainFrame::onSocketJoined);
    connector->connectToSocket(
                control()->getAppContext()->getWorkingUser()->getToken());
}

void A_MainFrame::onSocketJoined() {
    //  QMutex mutex;
    //  mutex.lock();
    //  loadSiteCameras();
    //  mutex.unlock();
}
void A_MainFrame::onSocketDisconnected() {  }
void A_MainFrame::onSocketConnected() {}
/**
 * Change the control of this abstraction
 * @param ctrl the new control for this abstraction facet
 **/
void A_MainFrame::changeControl(Control* ctrl) { this->ctrl = ctrl; }

/**
 * Method to create a new abstraction exactly equals to this abstraction.
 * @return the created clone
 **/

// public Abstraction getClone(){
//    try{
//        return (Abstraction)clone();
//    } catch(Exception e) {System.out.println("ERROR: can't duplicate an
//    abstraction.");}
//    return null;
//}

void A_MainFrame::getUserSites(QVariant* attachment) {
    attachment->setValue(this->userSites);
}

void A_MainFrame::getUserApps(QVariant* attachment) {
    attachment->setValue(this->userApps);
}


//load all cam of device
void A_MainFrame::loadDataCamerasOfDevice(QVariant *attachment , QString token, std::function<void(void)> onSuccess,
                                          std::function<void(QString)> onFailure){
    qDebug() << " = =====================================loadDataCamerasOfDevice";
    CamSite::getCamerasWithDeviceId(token, [this, onSuccess, attachment](CamSite *siteCameras){
        if(siteCameras){
            //update camera for sensor camera manager
            QVariant *dataStruct1 = new QVariant();
            QList<CamItem *> listCameraOfDevice = siteCameras->getCamItems();
            dataStruct1->setValue<QList<CamItem *>>(listCameraOfDevice);
            control()->newSystemAction(Message.UPDATE_LIST_CAMERA_OF_DEVICE_FOR_SENSOR_CAMERA_MANAGER , dataStruct1);

            this->control()->appContext->setIsLoadDataWithDeviceId(true); //load data with device id success
            int layoutSelected = siteCameras->getLayout();
            int pageSelected = siteCameras->getPlayPage();
            qDebug() <<"layoutSelected" << layoutSelected <<"pageSelected" << pageSelected;
            //set data to appcontext
            control()->appContext->setLayoutSelected(layoutSelected);
            control()->appContext->setPageSelected(pageSelected);
            this->siteCameras = siteCameras;
            this->control()->getAppContext()->setSiteCameras(siteCameras);
            this->control()->getAppContext()->setTotalCameras(
                        siteCameras->getCamItems().size());


            QString appNameVideoWall = "Video Wall";
            QVariant *dataStruct = new QVariant();
            LayoutStruct layoutStructCurrent = layoutSet->getLayoutWithNumberOfCameras(layoutSelected);
            layoutStructCurrent.numberOfCameras = layoutSelected;
            layoutStructCurrent.selectedPage = pageSelected;
            dataStruct->setValue<LayoutStruct>(layoutStructCurrent);

            if(attachment == Q_NULLPTR){//remote control by user
                //save to disk
                WindowsApp *windowsAppSelected = this->control()->appContext->getWindowsVideowallSelected();
                if(windowsAppSelected != Q_NULLPTR && !loadDataFirstSuccess){
                    QSettings settings;
                    settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
                    settings.beginGroup("Workspace" + QString::number(windowsAppSelected->idWindows));
                    int indexOfLayout = layoutSet->getLayoutWithNumberOfCameras(layoutSelected).code;
                    settings.setValue("selected_layout_workspace",indexOfLayout);
                    settings.setValue("selected_page_workspace",pageSelected);
                    settings.endGroup();
                    settings.endGroup();
                    qDebug() << "TOTAL CAMERA" << control()->appContext->getTotalCameras();

                    if(this->control()->appContext->getWorkingApp().appName.contains(appNameVideoWall, Qt::CaseInsensitive)){
                        control()->newSystemAction(Message.APP_VIDEO_WALL_LAYOUT_PAGE_UPDATE_REMOTE_CONTROL,
                                                   dataStruct);
                    }
                }
            }

            //init workspace
            control()->newSystemAction(Message.APP_VIDEO_WALL_WORKSPACES_INIT, Q_NULLPTR);

            if(!updateLayoutRemoteControlForPlaybackFirst){
                updateLayoutRemoteControlForPlaybackFirst = true;
                control()->newSystemAction(Message.APP_PLAY_BACK_LAYOUT_PAGE_UPDATE_REMOTE_CONTROL, dataStruct);
            }


            if(control()->appContext->getIsPickUpOrKickOutCam()){
                qDebug() << "Kick up or kick out cam update layout page for playback";
                control()->appContext->setIsPickUpOrKickOutCam(false);
                control()->newSystemAction(Message.APP_PLAY_BACK_LAYOUT_PAGE_UPDATE_REMOTE_CONTROL, dataStruct);
            }
        }
        onSuccess();
    },[this, onFailure](QString message) {
        onFailure(message);
    });
}

//updateDataCamerasOfDevice
void A_MainFrame::updateDataCamerasOfDevice(QString token, std::function<void(void)> onSuccess,
                                            std::function<void(QString)> onFailure){
    qDebug() << " = =====================================updateDataCamerasOfDevice";
    CamSite::updateCamerasWithDeviceId(token, [this, onSuccess](CamSite *siteCameras){
        if(siteCameras){
            //            int layoutSelected = siteCameras->getLayout();
            //            int pageSelected = siteCameras->getPlayPage();
            //            control()->appContext->setLayoutSelected(layoutSelected);
            //            control()->appContext->setPageSelected(pageSelected);
            this->siteCameras = siteCameras;
            this->control()->getAppContext()->setSiteCameras(siteCameras);
            this->control()->getAppContext()->setTotalCameras(
                        siteCameras->getCamItems().size());
            onSuccess();
        }
    },[this, onFailure](QString message) {
        onFailure(message);
    });
}

//api get camera with device id data local
void A_MainFrame::updateDataCamerasWithDeviceId(QString token) {
    qDebug() << "updateDataCamerasWithDeviceId ->>> after network disconnect";
    qDebug() << "Token " << token;
    if(this->control()->appContext->getIsLoadDataWithSiteId() || !this->control()->appContext->getIsLoadDataLocal()) return;
    isRequestingWithDeviceId = true;
    updateDataCamerasOfDevice(token, [this]{
        this->control()->appContext->setIsLoadDataWithDeviceId(true); //load data with device id success
        control()->newSystemAction(Message.APP_UPDATE_DATA_LOCAL_SUCCESS, Q_NULLPTR);
    }, [this](QString message){
        QString permisionDenied = "Permission denied.";
        QString timeoutRequest = "Timeout Request";
        //load data with device id false switch to load with site
        //if device permission denied
        //load data with site id
        qDebug() << "loadDataCamerasOfDevice message" << message;

        if(message == timeoutRequest){
            QSettings settings;
            QString offlineMode = settings.value("offline_mode").toString();
            if(offlineMode != "ON"){
                //retry request
                updateDataCamerasWithDeviceId(control()->appContext->getWorkingUser()->getToken());
            }
        }
    });
}



// thao Add 24.08.2017
void A_MainFrame::loadTotalCamerasOfSite(
        int siteId, QString tokenUser, int pageNumber, int layoutNumber,
        std::function<void(CamSite*)> onSuccess,
        std::function<void(QString)> onFailure) {
    CamSite::getCamerasOfSiteWithLayoutPage(
                siteId, tokenUser, pageNumber, layoutNumber,
                [this, onSuccess](CamSite* siteCameras) { onSuccess(siteCameras); },
    [this, onFailure](QString message) { onFailure(message); });
}

//load list camera with api 004
void A_MainFrame::loadCamerasOfSiteWithLayoutPage(int pageNumber, int layoutNumber,
                                                  std::function<void(void)> onSuccess,
                                                  std::function<void(void)> onFailure) {
    QSettings settings;
    QString offline_mode = settings.value("offine_mode").toString();
    settings.beginGroup(QString::number(this->control()->getAppContext()->getWorkingUser()->getUserId()));
    int siteIdUserSession = settings.value("id_working_site").toInt();

    int siteId = control()->getAppContext()->getWorkingUser()->getSite()->getSiteId();
    qDebug() << "loadSiteCameras" << "SITE ID SESSION" << siteIdUserSession << "SITE ID LOGIN" <<  siteId;
    QString token = control()->getAppContext()->getWorkingUser()->getToken();
    if (siteIdUserSession != -1 && siteIdUserSession != 0) {
        siteId = siteIdUserSession;
    }
    qDebug() << "load site camera" << "SITE ID" << siteId;
    std::function<void(CamSite *)> onFetchSuccess = [this, onSuccess](CamSite *siteCameras){
        this->siteCameras = siteCameras;
        if(siteCameras){
            this->control()->appContext->setIsLoadDataWithSiteId(true); //load data with device id success
            qDebug() << Q_FUNC_INFO << siteCameras->getCamItems().size();

            this->control()->getAppContext()->setSiteCameras(siteCameras);
            this->control()->getAppContext()->setTotalCameras(
                        siteCameras->getTotalCamItem());

            //update maxhistoryday for calendar
            this->control()->newSystemAction(Message.APP_PLAY_BACK_CALENDAR_MAX_HISTORYDAY, Q_NULLPTR);

            qDebug() << "TOTAL" <<  siteCameras->getTotalCamItem();
        }
        onSuccess();
    };

    std::function<void(QString)> onFetchFailure = [this, onFailure](QString message){

    };
    if(offline_mode != "ON"){
        CamSite::getCamerasOfSiteWithLayoutPage(siteId, token, pageNumber, layoutNumber, onFetchSuccess, onFetchFailure);

    }

    //    if(offline_mode == "ON"){
    //         CamSite::getCamerasOfSiteWithLayoutPageLocal(siteId, pageNumber, layoutNumber, onFetchSuccess, onFetchSuccess);

    //    }
}


//load list camera with api 003
void A_MainFrame::loadAllCamerasOfSite(
        std::function<void(void)> onSuccess,
        std::function<void(void)> onFailure) {


    QSettings settings;
    QString offline_mode = settings.value("offline_mode").toString();
    settings.beginGroup(QString::number(this->control()->getAppContext()->getWorkingUser()->getUserId()));
    int siteIdUserSession = settings.value("id_working_site").toInt();
    int siteId = control()->getAppContext()->getWorkingUser()->getSite()->getSiteId();

    qDebug() << "loadSiteCameras" << "SITE ID SESSION" << siteIdUserSession << "SITE ID LOGIN" <<  siteId;

    QString token = control()->getAppContext()->getWorkingUser()->getToken();

    if (siteIdUserSession != -1 && siteIdUserSession != 0) {
        siteId = siteIdUserSession;
    }
    qDebug() << "load site camera" << "SITE ID" << siteId;


    std::function<void(QJsonObject)> onFetchSuccess = [this, onSuccess](QJsonObject jsonObject) {
        qDebug() << "loadAllCamerasOfSite";
        CamSite *siteCameras = CamSite::parseCamItems(jsonObject);
        this->siteCameras = siteCameras;
        this->control()->getAppContext()->setSiteCameras(siteCameras);
        this->control()->getAppContext()->setTotalCameras(siteCameras->getCamItems().size());
        //init workspace
        control()->newSystemAction(Message.APP_VIDEO_WALL_WORKSPACES_INIT, Q_NULLPTR);

        qDebug() << "TOTAL" <<   siteCameras->getCamItems().size();
        //            for (int index = 0; index < siteCameras->getCamItems().size(); ++index) {
        //                qDebug() << "loadAllCamerasOfSite" << siteCameras->getCamItems().at(index)->getCameraId();
        //            }
        onSuccess();
    };
    std::function<void(QString)> onFetchFailure = [this, onFailure](QString message) {
        onFailure();
    };

    if(offline_mode != "ON"){
        CamSite::getCamerasOfSite(siteId, token,onFetchSuccess,onFetchFailure);
    }

    if(offline_mode == "ON"){
        NetworkUtils::instance().getDataCamerasSiteLocal(siteId, onFetchSuccess,onFetchFailure);
    }
}


//load list camera with api 003
void A_MainFrame::loadAllCamerasOfSite(int siteId,
                                       std::function<void(void)> onSuccess,
                                       std::function<void(void)> onFailure) {
    std::function<void(QJsonObject)> onFetchSuccess = [this, onSuccess](QJsonObject jsonObject) {
        CamSite *siteCameras = CamSite::parseCamItems(jsonObject);
        this->siteCameras = siteCameras;
        this->control()->getAppContext()->setSiteCameras(siteCameras);
        this->control()->getAppContext()->setTotalCameras(siteCameras->getCamItems().size());
        //init workspace
        control()->newSystemAction(Message.APP_VIDEO_WALL_WORKSPACES_INIT, Q_NULLPTR);
        qDebug() << "TOTAL" <<   siteCameras->getCamItems().size();
        //            for (int index = 0; index < siteCameras->getCamItems().size(); ++index) {
        //                qDebug() << "loadAllCamerasOfSite" << siteCameras->getCamItems().at(index)->getCameraId();
        //            }

        onSuccess();
    };
    std::function<void(QString)> onFetchFailure = [this, onFailure](QString message) {
        onFailure();
    };

    NetworkUtils::instance().getDataCamerasSiteLocal(siteId, onFetchSuccess,onFetchFailure );
}

QString A_MainFrame::convertLinkToM3u8(QString live, long timestamp, int duration) {
    QString vod = "http://";
    QStringList lives = live.split("/");
    vod.append(lives.at(2));
    vod.append("/rec/hls/");
    vod.append(lives.last());
    vod.append("_");
    vod.append(QString::number(timestamp * 1000));
    vod.append("_");
    vod.append(QString::number(duration * 1000));
    vod.append("_h264.m3u8");
    return vod;
}

QList<QString> A_MainFrame::loadPlayListM3U8(long timeStampStart, int duration, QList<int> listCamerasId){
    // để lấy luồng SD thì truyền name = "SD", nếu cần lấy luồng HD thì name = "HD"
    CamItemType typeItem;
    typeItem.protocol = "WS";
    typeItem.network = "CDN";
    typeItem.name = "SD"; // nếu muốn lấy luồng hd thì value = "HD"

    QList<QString> playlistM3U8;
    if(this->siteCameras != Q_NULLPTR)
    {
        QList<CamItem * > listCamItems = this->siteCameras->getCamItems();
        QList<int> camrerasId;
        for (int index = 0; index < listCamItems.size(); ++index) {
            camrerasId.append(listCamItems.at(index)->getCameraId());
        }
        for (int index = 0; index < listCamerasId.size(); ++index) {
            int indexOfCameraId = camrerasId.indexOf(listCamerasId.at(index)); //lấy chỉ số của camraid tương ứng trong list danh sách cameras id load về
            if(indexOfCameraId == -1) {
                playlistM3U8.append(""); //nếu không tìm thấy thì add giá trị xâu rỗng
            }else{
                //nếu khác -1 tức camera id này tồn tại trong list danh sách load về
                //bắt đầu lấy link
                CamItem *camItem = listCamItems.at(indexOfCameraId);
                if(camItem != Q_NULLPTR){
                    CamStream *camStream = camItem->getCamStream(typeItem);
                    if(camStream != Q_NULLPTR){
                        //lay source live tuong ung cua tung cameraid
                        QString sourceLiveOfCamStream = camStream->getSource();
                        //chuyen doi link live thanh link m3u8
                        QString sourceM3u8OfCamStream = convertLinkToM3u8(sourceLiveOfCamStream, timeStampStart, duration);
                        playlistM3U8.append(sourceM3u8OfCamStream); //thêm source vào list trả về
                    }
                }
            }
        }
    }
    return playlistM3U8;
}


void A_MainFrame::loadUserSites() {
    isRequestingLoadUserSite = true;
    loadUserSiteSuccess = false;
    AppContext* appContext = control()->getAppContext();
    SiteChild::fetchListSiteChild(
                appContext->getWorkingUser()->getSite()->getSiteId(),
                appContext->getWorkingUser()->getToken(),
                [this](SiteChild* userSites) {
        this->control()->getAppContext()->getUserSites()->clearListSite();
        this->control()->getAppContext()->setUserSites(userSites);
        this->control()->newSystemAction(Message.APP_UPDATE_USER_SITES, Q_NULLPTR);
        loadUserSiteSuccess = true;
        isRequestingLoadUserSite = false;
        if(loadUserSiteSuccess && loadDataFirstWithLayoutPageSuccess){
            control()->newAction(Message.STOP_TIMER_RECHECK_NETWORK, Q_NULLPTR);
        }
        //load data all site
        if(userSites && !this->control()->appContext->getIsLoadDataLocal()){
            Q_EMIT downloadedAllListSites(false , userSites);
        }
    },
    [this](QString message) {
        loadUserSiteSuccess = false;
        isRequestingLoadUserSite = false;
    });
}


void A_MainFrame::loadUserSites(bool updateData) {
    isRequestingLoadUserSite = true;
    loadUserSiteSuccess = false;
    AppContext* appContext = control()->getAppContext();
    SiteChild::fetchListSiteChild(
                appContext->getWorkingUser()->getSite()->getSiteId(),
                appContext->getWorkingUser()->getToken(),
                [this, updateData](SiteChild* userSites) {
        this->control()->getAppContext()->getUserSites()->clearListSite();
        this->control()->getAppContext()->setUserSites(userSites);
        this->control()->newSystemAction(Message.APP_UPDATE_USER_SITES, Q_NULLPTR);
        loadUserSiteSuccess = true;
        isRequestingLoadUserSite = false;
        if(loadUserSiteSuccess && loadDataFirstWithLayoutPageSuccess){
            control()->newAction(Message.STOP_TIMER_RECHECK_NETWORK, Q_NULLPTR);
        }
        //load data all site
        if(userSites && !this->control()->appContext->getIsLoadDataLocal()){
            Q_EMIT downloadedAllListSites(updateData,userSites);
        }
    },
    [this](QString message) {
        loadUserSiteSuccess = false;
        isRequestingLoadUserSite = false;
    });
}


void A_MainFrame::loadUserApps() {
    this->userApps.append({"Cameras", 1, ""});
    this->userApps.append({"Video Wall", 2, ""});
    this->userApps.append({"Playback", 3, ""});
    this->control()->getAppContext()->setUserApps(userApps);
    //  control()->newSystemAction(Message.DISPLAY_LIST_APPS, Q_NULLPTR);
}

void A_MainFrame::loadWorkingSite() {}

void A_MainFrame::loadWorkingSiteData() {
    qDebug() << Q_FUNC_INFO;
    loadCamerasOfSiteWithLayoutPage(1, 1, [this] {  /*this->onLoadDataSuccess();*/ },
    [this] { /*this->onLoadDataFailure();*/ });
    //  loadUserSites();
}

void A_MainFrame::changeSite() { loadWorkingSiteData(); }

void A_MainFrame::changeApp() {}

C_MainFrame* A_MainFrame::control() { return ((C_MainFrame*)this->ctrl); }

//api 004
void A_MainFrame::loadDataCamerasWithLayoutPage() {
    if(control()->appContext->getIsLoadDataWithDeviceId()) return;
    isRequestingLoadDataWithLayoutPage = true;
    loadDataFirstSuccess = false;
    loadDataFirstWithLayoutPageSuccess = false;
    int defaultLayoutIndex = 0;
    int defaultPageIndex = 1;
    int selectedLayoutSaved = -1;
    int selectedPageSaved = -1;
    int numberLayout = 1;

    //    QSettings settings;
    //    settings.beginGroup(QString::number(this->control()->getAppContext()->getWorkingUser()->getUserId()));
    //    settings.beginGroup("videowall");
    //    selectedLayoutSaved = settings.value("selected_layout").toInt();
    //    selectedPageSaved = settings.value("selected_page").toInt();

    //    QSettings settings;
    //    settings.beginGroup(QString::number(this->control()->appContext->getWorkingUser()->getUserId()));
    //    WindowsApp *windowsVideowallSelected = this->control()->appContext->getWindowsVideowallSelected();
    //    if(windowsVideowallSelected != Q_NULLPTR){
    //        int idOfWidnowsSelected = windowsVideowallSelected->idWindows;
    //        settings.beginGroup("Workspace" + QString::number(idOfWidnowsSelected));
    //        selectedLayoutSaved = settings.value("selected_layout_workspace", -1).toInt();
    //        selectedPageSaved = settings.value("selected_page_workspace", -1).toInt();
    //        settings.endGroup();
    //    }else{
    //        settings.beginGroup("Workspace" + QString::number(0));
    //        selectedLayoutSaved = settings.value("selected_layout_workspace", -1).toInt();
    //        selectedPageSaved = settings.value("selected_page_workspace", -1).toInt();
    //        settings.endGroup();
    //    }
    //    settings.endGroup();

    //    qDebug() << "SETTINGS USER READ" << selectedLayoutSaved << selectedPageSaved;
    //    //select layout
    //    if(selectedLayoutSaved >= 0 && defaultLayoutIndex <= 4){
    //        defaultLayoutIndex = selectedLayoutSaved;
    //        numberLayout = layoutSet->layoutList.at(defaultLayoutIndex).numberOfCameras;
    //    }

    //    if (selectedPageSaved > 0){
    //        defaultPageIndex = selectedPageSaved;
    //    }

    qDebug() << "SETTINGS LAYOUT/PAGE Load " << selectedLayoutSaved << selectedPageSaved;

    loadCamerasOfSiteWithLayoutPage(defaultPageIndex,numberLayout , [this] {
        this->control()->appContext->setIsLoadDataWithSiteId(true); //load data with device id success
        control()->newSystemAction(Message.APP_VIDEO_WALL_WORKSPACES_INIT, Q_NULLPTR);
        control()->newSystemAction(Message.APP_DATA_LOADED_SUCCESS, Q_NULLPTR);
        //        control()->newSystemAction(Message.APP_VIDEO_WALL_LAYOUT_DEFAULT_SET,
        //                                   Q_NULLPTR);
        control()->newSystemAction(Message.APP_PLAY_BACK_LAYOUT_DEFAULT_SET,
                                   Q_NULLPTR);
        loadDataFirstSuccess = true;
        isRequestingLoadDataWithLayoutPage = false;
        loadDataFirstWithLayoutPageSuccess = true;
        if(loadUserSiteSuccess && loadDataFirstWithLayoutPageSuccess){
            control()->newAction(Message.STOP_TIMER_RECHECK_NETWORK, Q_NULLPTR);
        }
    },
    [this] {
        this->control()->appContext->setIsLoadDataWithSiteId(false); //load data with device id false
        loadDataFirstSuccess = false;
        isRequestingLoadDataWithLayoutPage = false;
        loadDataFirstWithLayoutPageSuccess = false;
        qDebug() << "LOAD SITE CAMERAS ERROR";
    });
}


//api get camera with device id
//void A_MainFrame::loadDataCamerasWithDeviceId(QString token) {
//    qDebug() << "loadDataCamerasWithDeviceId";
//    if(this->control()->appContext->getIsLoadDataWithSiteId()) return;
//    isRequestingWithDeviceId = true;
//    loadDataFirstSuccess = false;
//    loadDataCamerasOfDevice(Q_NULLPTR, token, [this]{
//        loadDataFirstSuccess = true;
//        control()->newSystemAction(Message.APP_DATA_LOADED_REMOTE_CONTROL_SUCCESS, Q_NULLPTR);
//        control()->newAction(Message.STOP_TIMER_RECHECK_NETWORK, Q_NULLPTR);
//    }, [this](QString message){
//        QString permisionDenied = "Permission denied.";
//        QString timeoutRequest = "Timeout Request";
//        loadDataFirstSuccess = false;
//        isRequestingWithDeviceId = false;
//        //load data with device id false switch to load with site
//        //if device permission denied
//        //load data with site id
//        qDebug() << "loadDataCamerasOfDevice message" << message;
//        if(message ==  permisionDenied){
//            this->control()->appContext->setIsLoadDataWithDeviceId(false);
//            control()->newSystemAction(Message.APP_LOAD_DATA_REMOTE_CONTROL_PERMISSION_DENIED , Q_NULLPTR);
//        }
//    });
//}

//api get camera with device id
void A_MainFrame::loadDataCamerasWithDeviceId(QString token) {
    qDebug() << "loadDataCamerasWithDeviceId ->>> after network disconnect";
    qDebug() << "Token " << token;
    if(this->control()->appContext->getIsLoadDataWithSiteId()) return;
    isRequestingWithDeviceId = true;
    loadDataFirstSuccess = false;
    loadDataCamerasOfDevice(Q_NULLPTR, token, [this]{
        this->control()->appContext->setIsLoadDataWithDeviceId(true); //load data with device id success
        control()->newSystemAction(Message.APP_DATA_LOADED_REMOTE_CONTROL_SUCCESS, Q_NULLPTR);
        loadDataFirstSuccess = true;
        control()->newAction(Message.STOP_TIMER_RECHECK_NETWORK, Q_NULLPTR);
    }, [this](QString message){
        QString permisionDenied = "Permission denied.";
        QString timeoutRequest = "Timeout Request";
        //load data with device id false switch to load with site
        //if device permission denied
        //load data with site id
        qDebug() << "loadDataCamerasOfDevice message" << message;
        if(message ==  permisionDenied){
            loadDataFirstSuccess = false;
            isRequestingWithDeviceId = false;
            this->control()->appContext->setIsLoadDataWithDeviceId(false);
            control()->newSystemAction(Message.APP_LOAD_DATA_REMOTE_CONTROL_PERMISSION_DENIED , Q_NULLPTR);
        }else{
            //time out request or request error
            loadDataFirstSuccess = false;
            //retry request
            loadDataCamerasWithDeviceId(control()->appContext->getWorkingUser()->getToken());
        }
    });
}




void A_MainFrame::reloadDataWhenNetworkIsReachable(){
    //    qDebug() <<"reloadDataWhenNetworkIsReachable" <<"loadDataFirstSuccess" <<loadDataFirstSuccess << "isRequestingWithDeviceId" << isRequestingWithDeviceId;
    if(!isRequestingWithDeviceId && loadDataFirstSuccess == false) {
        qDebug() <<"loadDataCameraWhenNetworkIsReachable";

        QString tokenLogin = control()->appContext->getWorkingUser()->getToken();
        //load data with deviceid first
        loadDataCamerasWithDeviceId(tokenLogin);
        if(!isRequestingLoadDataWithLayoutPage && !isRequestingWithDeviceId && !loadDataFirstSuccess && !loadDataFirstWithLayoutPageSuccess ){
            loadDataCamerasWithLayoutPage();
        }
    }

    if(!isRequestingLoadUserSite && loadUserSiteSuccess == false && loadDataFirstWithLayoutPageSuccess){
        qDebug() <<"loadUserSitesWhenNetworkIsReachable";
        loadUserSites();
    }
}

//api 003
void A_MainFrame::loadDataCamerasOfSite() {
    loadAllCamerasOfSite([this] {
        control()->newSystemAction(Message.APP_DATA_LOADED_SUCCESS, Q_NULLPTR);
    },
    [this] {
        qDebug() << "LOAD SITE CAMERAS ERROR";
    });
}

//api 003 load data local with site id
void A_MainFrame::loadDataCamerasOfSite(int siteId) {
    loadAllCamerasOfSite(siteId,[this] {
        control()->newSystemAction(Message.APP_DATA_LOADED_SUCCESS, Q_NULLPTR);

        control()->newSystemAction(Message.APP_VIDEO_WALL_LAYOUT_DEFAULT_SET,
                                   Q_NULLPTR);
        control()->newSystemAction(Message.APP_PLAY_BACK_LAYOUT_DEFAULT_SET,
                                   Q_NULLPTR);
    },
    [this] {
        qDebug() << "LOAD SITE CAMERAS ERROR";
    });
}

