#ifndef WINDOWAPP_H
#define WINDOWAPP_H
#include <QString>
#include <QWidget>
#include <QObject>
#include <QDebug>
class C_VWWorkSpace;
enum StateOfWindows{
    NOT_POP_UP = 0,
    POP_UP = 1,
    FULL_SCREEN_WINDOW = 2
};
enum StateOfWorkspace{
    PLAYING,
    STOPPED
};
class WindowsApp: public QObject {
    Q_OBJECT
public:
//    static int idIncreate;
    int idWindows;
    QString windowsIcon = "";
    QString windowsName;
    QWidget *zone = Q_NULLPTR;
    QWidget *zoneParent = Q_NULLPTR;

    QString appName = "";
    C_VWWorkSpace *cWorkspace = Q_NULLPTR;
    bool needLoadData = false;
    StateOfWindows stateOfWindows = StateOfWindows::NOT_POP_UP;
    StateOfWorkspace stateOfWorkspace = StateOfWorkspace::STOPPED;
    WindowsApp(int _idWindows);
    ~WindowsApp();

//    WindowsApp(int _idWindows , QString _windowsIcon,QString _windowsName , QWidget *_zone);
    void initWindowsApp(QWidget *zone, QString windowsName);
    void changeIdWindows(int idWindows);

};
#endif // WINDOWAPP_H
