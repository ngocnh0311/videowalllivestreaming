#include "appcontext.h"

Site *AppContext::getSiteOfWorker() const { return siteOfWorker; }

void AppContext::setSiteOfWorker(Site *value) { siteOfWorker = value; }

Site *AppContext::getWorkingSite() const { return workingSite; }



void AppContext::setWorkingSite(Site *value) {
    workingSite = value;
    QSettings settings;
    settings.beginGroup(QString::number(workingUser->getUserId()));
    settings.setValue("id_working_site", workingSite->getSiteId());
    settings.setValue("site_name", workingSite->getSiteName());
    settings.endGroup();
}

User *AppContext::getWorkingUser() const { return workingUser; }

void AppContext::setWorkingUser(User *value) {
    workingUser = value;
    qDebug() << "SETWorking user" << value->getUserId();
}

QWidget *AppContext::getMainWindow() const { return mainWindow; }

void AppContext::setMainWindow(QWidget *value) { mainWindow = value; }

CamSite *AppContext::getSiteCameras() const { return siteCameras; }

void AppContext::setSiteCameras(CamSite *value) { siteCameras = value; }

SiteChild *AppContext::getUserSites() const { return userSites; }

void AppContext::setUserSites(SiteChild *value) { userSites = value; }

QList<UserApp> AppContext::getUserApps() const { return userApps; }

void AppContext::setUserApps(const QList<UserApp> &value) { userApps = value; }

UserApp AppContext::getWorkingApp() const { return workingApp; }

void AppContext::setWorkingApp(const UserApp &value) { workingApp = value; }
void AppContext::setWorkingApp(QString appName) {
    for (int appIndex = 0; appIndex < userApps.size(); ++appIndex) {
        if (userApps.at(appIndex).appName == appName) {
            this->workingApp = userApps.at(appIndex);
            return;
        }
    }
}

int AppContext::getVideoWallPageTransitionStartMode() const {
    return videoWallPageTransitionStartMode;
}

void AppContext::setVideoWallPageTransitionStartMode(int value) {
    videoWallPageTransitionStartMode = value;
}

int AppContext::getVideoWallPageTransitionDelay() const {
    return videoWallPageTransitionDelay;
}

void AppContext::setVideoWallPageTransitionDelay(int value) {
    videoWallPageTransitionDelay = value;
}

QString AppContext::getLocalStoragePath() const { return localStoragePath; }

void AppContext::setLocalStoragePath(const QString &value) {
    localStoragePath = value;
}


int AppContext::getTotalCameras() const { return totalCameras; }

void AppContext::setTotalCameras(int value) { totalCameras = value; }

QList<CamItem *> AppContext::getListCamItemsOfSite() const {
    return listCamItemsOfSite;
}

void AppContext::setListCamItemsOfSite(const QList<CamItem *> &value) {
    listCamItemsOfSite = value;
}


CamItemType AppContext::getNetworkType() const
{
    return networkType;
}

void AppContext::setNetworkType(const CamItemType &value)
{
    networkType = value;
}

int AppContext::getLayoutSelected() const
{
    return layoutSelected;
}

void AppContext::setLayoutSelected(int value)
{
    layoutSelected = value;
}

int AppContext::getPageSelected() const
{
    return pageSelected;
}

void AppContext::setPageSelected(int value)
{
    pageSelected = value;
}

bool AppContext::getIsLoadDataWithDeviceId() const
{
    return isLoadDataWithDeviceId;
}

void AppContext::setIsLoadDataWithDeviceId(bool value)
{
    isLoadDataWithDeviceId = value;
}

bool AppContext::getIsLoadDataWithSiteId() const
{
    return isLoadDataWithSiteId;
}

void AppContext::setIsLoadDataWithSiteId(bool value)
{
    isLoadDataWithSiteId = value;
}

QList<CamItem *> AppContext::getListCamerasWorkingCurrent() const
{
    return listCamerasWorkingCurrent;
}

void AppContext::setListCamerasWorkingCurrent(const QList<CamItem *> &value)
{
    listCamerasWorkingCurrent = value;
}

int AppContext::getCameraIdFullSceen() const
{
    return cameraIdFullSceen;
}

void AppContext::setCameraIdFullSceen(int value)
{
    cameraIdFullSceen = value;
}

bool AppContext::getJoinDeviceSuccess() const
{
    return joinDeviceSuccess;
}

void AppContext::setJoinDeviceSuccess(bool value)
{
    joinDeviceSuccess = value;
}

bool AppContext::getIsPickUpOrKickOutCam() const
{
    return isPickUpOrKickOutCam;
}

void AppContext::setIsPickUpOrKickOutCam(bool value)
{
    isPickUpOrKickOutCam = value;
}

QString AppContext::getIsModeDebug() const
{
    return isModeDebug;
}

void AppContext::setIsModeDebug(const QString &value)
{
    isModeDebug = value;
}
QString AppContext::getOfflineMode() const
{
    return offlineMode;
}

void AppContext::setOfflineMode(const QString &value)
{
    offlineMode = value;
}

bool AppContext::getIsLoadDataLocal() const
{
    return isLoadDataLocal;
}

void AppContext::setIsLoadDataLocal(bool value)
{
    isLoadDataLocal = value;
}

bool AppContext::getNetworkConnected() const
{
    return networkConnected;
}

void AppContext::setNetworkConnected(bool value)
{
    networkConnected = value;
}

QString AppContext::getUseFreeSpace() const
{
    return useFreeSpace;
}

void AppContext::setUseFreeSpace(const QString &value)
{
    useFreeSpace = value;
}

int AppContext::getTotalPages() const
{
    return totalPages;
}

void AppContext::setTotalPages(int value)
{
    totalPages = value;
}

QString AppContext::getIsAutoUpdateNewVersion() const
{
    return isAutoUpdateNewVersion;
}

void AppContext::setIsAutoUpdateNewVersion(QString value)
{
    isAutoUpdateNewVersion = value;
}

void AppContext::setVersionInfoNewest(VersionInfo *value)
{
    versionInfoNewest = value;
}

VersionInfo *AppContext::getVersionInfoNewest() const
{
    return versionInfoNewest;
}

bool AppContext::getIsNeedUpdate() const
{
    return isNeedUpdate;
}

void AppContext::setIsNeedUpdate(bool value)
{
    isNeedUpdate = value;
}
QList<WindowsApp *> AppContext::getWindowsAppVideowall() const
{
    return windowsAppVideowall;
}

void AppContext::setWindowsAppVideowall(const QList<WindowsApp *> &value)
{
    windowsAppVideowall = value;
}

WindowsApp *AppContext::getWindowsVideowallSelected() const
{
    return windowsVideowallSelected;
}

void AppContext::setWindowsVideowallSelected(WindowsApp *value)
{
    windowsVideowallSelected = value;
}

QString AppContext::getIsShowWindowsList() const
{
    return isShowWindowsList;
}

void AppContext::setIsShowWindowsList(const QString &value)
{
    isShowWindowsList = value;
}

bool AppContext::getIsNeedSynchronizedWithServer() const
{
    return isNeedSynchronizedWithServer;
}

void AppContext::setIsNeedSynchronizedWithServer(bool value)
{
    isNeedSynchronizedWithServer = value;
}

QWidget *AppContext::getZoneSettings() const
{
    return zoneSettings;
}

void AppContext::setZoneSettings(QWidget *value)
{
    zoneSettings = value;
}

QWidget *AppContext::getZoneAppPlayback() const
{
    return zoneAppPlayback;
}

void AppContext::setZoneAppPlayback(QWidget *value)
{
    zoneAppPlayback = value;
}

AppContext::AppContext() : QObject(Q_NULLPTR) {
    userSites = new SiteChild;
}
void AppContext::loadWorkingSite() {
    // doc tu db ra oldWorkingSite cua phien lam viec lan truoc. neu khong luu thi
    // coi nhu ko co
    
    // doc tu db ra danh sach site cua user == userSiteList
    // kiem tra xem oldWorkingSite luu trong cau hinh co ton tai trong
    // userSiteList hay khong
    // Neu co thi lay workingSite nay de lam viec this->workingSite =
    // oldWorkingSite;
    // Neu khong co thi lay site cua user lam working site aka this->workingSite =
    // userProfile.site_id;

    //  if (sessionUser) {
    //    Site *site = new Site;
    //    qDebug() << sessionUser->getIdWorkingSite() <<
    //    sessionUser->getSiteName();
    //    site->setSiteId(sessionUser->getIdWorkingSite());
    //    site->setSiteName(sessionUser->getSiteName());
    //    this->workingSite = site;
    //  } else {

    this->workingSite = this->workingUser->getSite();
    //  for (int index = 0; index < this->userSites->getListSite().size();
    //  ++index) {
    //    if (sessionUser->getIdWorkingSite() ==
    //        this->userSites->getListSite().at(index)->getSiteId()) {
    //      this->workingSite = this->userSites->getListSite().at(index);
    //    }
    //  }
}


int AppContext::getMaxHistorydayOfSite(){
    int maxHistoryday = 0;
    int historyDayOfCamItem;
    bool ok;
    CamItem *camItem = Q_NULLPTR;
    if(siteCameras){
        QList<CamItem *> listCamItem = siteCameras->getCamItems();
        //select history day first
        if(listCamItem.size() > 0) {
            camItem = listCamItem.at(0);
            if(camItem){
                historyDayOfCamItem = camItem->getHistoryDay().toInt(&ok);
                if(ok){
                    maxHistoryday = historyDayOfCamItem;
                }
            }
        }
        //find and change maxhistoryday
        for (int index = 1; index < listCamItem.size(); ++index) {
            camItem = listCamItem.at(index);
            if(camItem){
                historyDayOfCamItem = camItem->getHistoryDay().toInt(&ok);
                if(maxHistoryday < historyDayOfCamItem){
                    maxHistoryday = historyDayOfCamItem;
                }
            }
        }
    }
    return maxHistoryday;
}


