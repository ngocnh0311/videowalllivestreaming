#ifndef APPCONTEXT_H
#define APPCONTEXT_H

#include <QObject>
#include <QSettings>
#include "Authentication/userapp.h"
#include "Authentication/windowsapp.h"
#include "Camera/camsite.h"
#include "Site/site.h"
#include "Site/sitechild.h"
#include "message.h"
#include "sessionuser.h"
#include "user.h"
#include <QSettings>
#include "MainFrame/VersionUpdater/versioninfo.h"
class QSettings;
class AppContext : public QObject {
    Q_OBJECT
private:
    QWidget *zoneSettings = Q_NULLPTR;
    QWidget *zoneAppPlayback = Q_NULLPTR;

    bool isNeedSynchronizedWithServer = false;
    bool isNeedUpdate = false;
    VersionInfo *versionInfoNewest = Q_NULLPTR;
    QString isShowWindowsList = "HIDE";
    QString isAutoUpdateNewVersion = "OFF";
    int totalPages;
    bool networkConnected = false;
    QString isModeDebug = "OFF";
    bool isLoadDataLocal = false;
    AppMessage Message;
    QString offlineMode = "OFF";
    QString useFreeSpace = "ON";

    bool isLoadDataWithDeviceId = false;
    bool isLoadDataWithSiteId = false;
    User *workingUser = Q_NULLPTR;
    Site *workingSite = Q_NULLPTR;
    Site *siteOfWorker = Q_NULLPTR;
    CamSite *siteCameras = Q_NULLPTR;
    QWidget *mainWindow = Q_NULLPTR;
    SiteChild *userSites = Q_NULLPTR;
    QList<UserApp> userApps;
    QList<WindowsApp *> windowsAppVideowall;
    WindowsApp* windowsVideowallSelected = Q_NULLPTR;

    UserApp workingApp;
    CamItemType networkType;
    bool joinDeviceSuccess = false;


    // main application
    QString localStoragePath = "Downloads";  // thu muc media

    // worker site settings (Settings/workerSiteID.txt):
    // + save workingSite
    // + save selectedLayout
    // + save selectedPage
    // + selectedDate
    // + selectedTimeSlot (hour)
    // + selectedPosition (timestamp)
    // + VideoWall app default setting
    int videoWallPageTransitionStartMode =
            Message.PAGE_TRANSITION_START_ON_CLICK; //PAGE_TRANSITION_START_AUTOMATICALLY

    int videoWallPageTransitionDelay = 0;  // 5 minute;

    int totalCameras;
    int layoutSelected;
    int pageSelected;
    QNetworkAccessManager *networkManager;
    QList<CamItem *> listCamItemsOfSite;
    QList<CamItem *> listCamerasWorkingCurrent;
    int cameraIdFullSceen = 0;
    bool isPickUpOrKickOutCam = false;

public:

    AppContext();
    Site *getSiteOfWorker() const;
    void setSiteOfWorker(Site *value);

    Site *getWorkingSite() const;
    void setWorkingSite(Site *value);

    User *getWorkingUser() const;
    void setWorkingUser(User *value);
    void loadWorkingSite();

    QWidget *getMainWindow() const;
    void setMainWindow(QWidget *value);

    CamSite *getSiteCameras() const;
    void setSiteCameras(CamSite *value);

    SiteChild *getUserSites() const;
    void setUserSites(SiteChild *value);

    QList<UserApp> getUserApps() const;
    void setUserApps(const QList<UserApp> &value);

    UserApp getWorkingApp() const;
    void setWorkingApp(const UserApp &value);
    void setWorkingApp(QString appName);

    int getVideoWallPageTransitionStartMode() const;
    void setVideoWallPageTransitionStartMode(int value);

    int getVideoWallPageTransitionDelay() const;
    void setVideoWallPageTransitionDelay(int value);

    QString getLocalStoragePath() const;
    void setLocalStoragePath(const QString &value);

    int getTotalCameras() const;
    void setTotalCameras(int value);

    QList<CamItem *> getListCamItemsOfSite() const;
    void setListCamItemsOfSite(const QList<CamItem *> &value);

    QSettings getAppSettings() const;
    int getMaxHistorydayOfSite();



    CamItemType getNetworkType() const;
    void setNetworkType(const CamItemType &value);

    int getLayoutSelected() const;
    void setLayoutSelected(int value);

    int getPageSelected() const;
    void setPageSelected(int value);

    bool getIsLoadDataWithDeviceId() const;
    void setIsLoadDataWithDeviceId(bool value);

    bool getIsLoadDataWithSiteId() const;
    void setIsLoadDataWithSiteId(bool value);

    QList<CamItem *> getListCamerasWorkingCurrent() const;
    void setListCamerasWorkingCurrent(const QList<CamItem *> &value);

    int getCameraIdFullSceen() const;
    void setCameraIdFullSceen(int value);

    bool getJoinDeviceSuccess() const;
    void setJoinDeviceSuccess(bool value);

    bool getIsPickUpOrKickOutCam() const;
    void setIsPickUpOrKickOutCam(bool value);

    QString getOfflineMode() const;
    void setOfflineMode(const QString &value);
    bool getIsLoadDataLocal() const;
   
    void setIsLoadDataLocal(bool value);
    QString getIsModeDebug() const;
    void setIsModeDebug(const QString &value);

    bool getNetworkConnected() const;
    void setNetworkConnected(bool value);

    QString getUseFreeSpace() const;
    void setUseFreeSpace(const QString &value);

    int getTotalPages() const;
    void setTotalPages(int value);

    QString getIsAutoUpdateNewVersion() const;
    void setIsAutoUpdateNewVersion(QString value);

    void setVersionInfoNewest(VersionInfo *value);

    VersionInfo *getVersionInfoNewest() const;

    bool getIsNeedUpdate() const;
    void setIsNeedUpdate(bool value);

    QList<WindowsApp *> getWindowsAppVideowall() const;
    void setWindowsAppVideowall(const QList<WindowsApp *> &value);

    WindowsApp *getWindowsVideowallSelected() const;
    void setWindowsVideowallSelected(WindowsApp *value);

    QString getIsShowWindowsList() const;
    void setIsShowWindowsList(const QString &value);

    bool getIsNeedSynchronizedWithServer() const;
    void setIsNeedSynchronizedWithServer(bool value);

    QWidget *getZoneSettings() const;
    void setZoneSettings(QWidget *value);

    QWidget *getZoneAppPlayback() const;
    void setZoneAppPlayback(QWidget *value);

public Q_SLOTS:
};

#endif  // APPCONTEXT_H
