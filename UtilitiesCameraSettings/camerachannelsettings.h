#ifndef CAMERACHANNELSETTINGS_H
#define CAMERACHANNELSETTINGS_H
#include <QList>
#include <QSettings>
#include <QString>
#include "Common/generaldefine.h"

class CameraChannelSettings
{
public:
    CameraChannelSettings();
    static QList<CameraChannel> getAllCameraChannelSave(int userId);
    static void saveAllCameraChannel(int userId, QList<CameraChannel> cameraChannels);
    static void changeChannelOfCameraSave(int userId, int cameraId , QString channelCamera);

};

#endif // CAMERACHANNELSETTINGS_H
