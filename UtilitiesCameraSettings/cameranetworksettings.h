#ifndef CAMERASNETWORKETTINGS_H
#define CAMERASNETWORKETTINGS_H
#include <QList>
#include <QSettings>
#include <QString>
#include "Common/generaldefine.h"
class CameraNetworkSettings
{
public:
    CameraNetworkSettings();
    static void changeDataSourceOfCameraSave(int workspaceId, int userId,int cameraId , QString dataSource, QString dataSourceDefault);
    static QList<CameraNetwork> getAllCameraNetworkSaveOfWorkspace(int workspaceId, int userId);
    static void saveAllCameraNetworkOfWorkspace(int workspaceId, int userId,QList<CameraNetwork> cameraNetworks, QString dataSourceOfAll);
    static QString getNetworkSaveOfCamera(int workspaceId, int userId, int cameraId);
    static QString getDataSourceSaveOfCamera(int workspaceId, int userId, int cameraId);

};

#endif // CAMERASNETWORKETTINGS_H
