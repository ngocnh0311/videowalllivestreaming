#include "camerachannelsettings.h"

CameraChannelSettings::CameraChannelSettings()
{

}

QList<CameraChannel> CameraChannelSettings::getAllCameraChannelSave(int userId){
    QList<CameraChannel> cameraChannels;
    QSettings settings;
    settings.beginGroup(QString::number(userId));
    int sizeCameras = settings.beginReadArray("cameras_channel");
    for (int index = 0; index < sizeCameras; ++index) {
        settings.setArrayIndex(index);
        CameraChannel cameraChannelSave;
        QString cameraIdSave = settings.value("cameraid").toString();
        QString channelSave = settings.value("channel").toString();
        cameraChannelSave.cameraid = cameraIdSave;
        cameraChannelSave.channel = channelSave;
        cameraChannels.append(cameraChannelSave);
    }
    settings.endArray();
    settings.endGroup();
    return cameraChannels;
}

void CameraChannelSettings::saveAllCameraChannel(int userId, QList<CameraChannel> cameraChannels){

    QSettings settings;
    settings.beginGroup(QString::number(userId));
    //delete all before save
    settings.beginWriteArray("cameras_channel", 0);
    settings.remove("");
    settings.endArray ();

    settings.beginWriteArray("cameras_channel");
    for (int index = 0; index < cameraChannels.size(); ++index) {
        settings.setArrayIndex(index);
        CameraChannel cameraChannelSave = cameraChannels.at(index);
        settings.setValue("cameraid" , cameraChannelSave.cameraid);
        settings.setValue("channel", cameraChannelSave.channel);
    }
    settings.endArray();
    settings.endGroup();
}


void CameraChannelSettings::changeChannelOfCameraSave(int userId, int cameraId , QString channelCamera){
    int indexCamera = -1;
    QString cameraIdString = QString::number(cameraId);
    QSettings settings;
    settings.beginGroup(QString::number(userId));
    //check all camera save if exits
    int sizeCameras = settings.beginReadArray("cameras_channel");
    qDebug() <<Q_FUNC_INFO << "sizeCameras" << sizeCameras;
    for (int index = 0; index < sizeCameras; ++index) {
        settings.setArrayIndex(index);
        QString cameraIdSave = settings.value("cameraid").toString();
        if(cameraIdString == cameraIdSave){
            indexCamera = index;
        }
    }
    settings.endArray();
    settings.endGroup();

    if(indexCamera != -1){
        //camera id exits
        //camera id not exits
        //create new cameranetwork save
        CameraChannel cameraChannelNew;
        cameraChannelNew.cameraid = cameraIdString;
        cameraChannelNew.channel = channelCamera;
        //read all after write all
        QList<CameraChannel> cameraChannels = getAllCameraChannelSave(userId);
        cameraChannels.replace(indexCamera,cameraChannelNew);
        //saveAll channel camera
        saveAllCameraChannel(userId, cameraChannels);
    }else{
        //camera id not exits
        //create new cameranetwork save
        CameraChannel cameraChannelNew;
        cameraChannelNew.cameraid = cameraIdString;
        cameraChannelNew.channel = channelCamera;
        //read all after write all
        QList<CameraChannel> cameraChannels = getAllCameraChannelSave(userId);
        cameraChannels.append(cameraChannelNew);
        //saveAllCameraNetwork
        saveAllCameraChannel(userId, cameraChannels);
    }
}
