#include "cameranetworksettings.h"

CameraNetworkSettings::CameraNetworkSettings()
{

}


void CameraNetworkSettings::changeDataSourceOfCameraSave(int workspaceId, int userId, int cameraId , QString dataSource,  QString dataSourceOfAll){
    int indexCamera = -1;
    QString cameraIdString = QString::number(cameraId);
    QSettings settings;
    settings.beginGroup(QString::number(userId));
    settings.beginGroup(QString::number(workspaceId));
    //check all camera save if exits
    int sizeCameras = settings.beginReadArray("cameras");
    qDebug() <<Q_FUNC_INFO << "sizeCameras" << sizeCameras;
    for (int index = 0; index < sizeCameras; ++index) {
        settings.setArrayIndex(index);
        QString cameraIdSave = settings.value("cameraid").toString();
        if(cameraIdString == cameraIdSave){
            indexCamera = index;
        }
    }
    settings.endArray();
    settings.endGroup();
    settings.endGroup();

    if(indexCamera != -1){
        //camera id exits
        //camera id not exits
        //create new cameranetwork save
        CameraNetwork cameraNetworkNew;
        cameraNetworkNew.cameraid = cameraIdString;
        cameraNetworkNew.dataSource = dataSource;
        if(dataSource == dataSourceCamera.CAM || dataSource == dataSourceCamera.NVR) cameraNetworkNew.network = networkCamera.LAN;
        if(dataSource == dataSourceCamera.CDN) cameraNetworkNew.network = networkCamera.CDN;

        //read all after write all
        QList<CameraNetwork> cameraNetworks = getAllCameraNetworkSaveOfWorkspace(workspaceId, userId);
        cameraNetworks.replace(indexCamera, cameraNetworkNew);
        //saveAllCameraNetwork
        saveAllCameraNetworkOfWorkspace(workspaceId, userId, cameraNetworks, dataSourceOfAll);


    }else{
        //camera id not exits
        //create new cameranetwork save
        CameraNetwork cameraNetworkNew;
        cameraNetworkNew.cameraid = cameraIdString;
        cameraNetworkNew.dataSource = dataSource;
        if(dataSource == dataSourceCamera.CAM || dataSource == dataSourceCamera.NVR) cameraNetworkNew.network = networkCamera.LAN;
        if(dataSource == dataSourceCamera.CDN) cameraNetworkNew.network = networkCamera.CDN;
        //read all after write all
        QList<CameraNetwork> cameraNetworks = getAllCameraNetworkSaveOfWorkspace(workspaceId, userId);
        cameraNetworks.append(cameraNetworkNew);
        //saveAllCameraNetwork
        saveAllCameraNetworkOfWorkspace(workspaceId, userId, cameraNetworks, dataSourceOfAll);
    }
}

QList<CameraNetwork> CameraNetworkSettings::getAllCameraNetworkSaveOfWorkspace(int workspaceId, int userId){
    QList<CameraNetwork> cameraNetworks;
    QSettings settings;
    settings.beginGroup(QString::number(userId));
    settings.beginGroup(QString::number(workspaceId));
    int sizeCameras = settings.beginReadArray("cameras");
    for (int index = 0; index < sizeCameras; ++index) {
        settings.setArrayIndex(index);
        CameraNetwork cameraInfoSave;
        QString cameraIdSave = settings.value("cameraid").toString();
        QString networkSave = settings.value("network").toString();
        QString dataSource = settings.value("data_source_camera").toString();
        cameraInfoSave.cameraid = cameraIdSave;
        cameraInfoSave.network = networkSave;
        cameraInfoSave.dataSource = dataSource;
        cameraNetworks.append(cameraInfoSave);
    }
    settings.endArray();
    settings.endGroup();
    settings.endGroup();
    return cameraNetworks;
}


void CameraNetworkSettings::saveAllCameraNetworkOfWorkspace(int workspaceId, int userId, QList<CameraNetwork> cameraNetworks, QString dataSourceOfAll){

    QSettings settings;
    settings.beginGroup(QString::number(userId));
    settings.beginGroup(QString::number(workspaceId));
    //delete all before save
    settings.beginWriteArray("cameras", 0);
    settings.remove("");
    settings.endArray ();

    settings.beginWriteArray("cameras");
    for (int index = 0; index < cameraNetworks.size(); ++index) {
        settings.setArrayIndex(index);
        CameraNetwork cameraInfoSave = cameraNetworks.at(index);
        settings.setValue("cameraid" , cameraInfoSave.cameraid);
        if(cameraInfoSave.dataSource.isEmpty()){
            cameraInfoSave.dataSource = dataSourceOfAll;
        }
        if(cameraInfoSave.dataSource == dataSourceCamera.CAM || cameraInfoSave.dataSource == dataSourceCamera.NVR) cameraInfoSave.network = networkCamera.LAN;
        if(cameraInfoSave.dataSource == dataSourceCamera.CDN) cameraInfoSave.network = networkCamera.CDN;

        settings.setValue("network", cameraInfoSave.network);
        settings.setValue("data_source_camera", cameraInfoSave.dataSource);
        settings.setValue("data_source_camera", cameraInfoSave.dataSource);
    }
    settings.endArray();
    settings.endGroup();
    settings.endGroup();
}


QString CameraNetworkSettings::getNetworkSaveOfCamera(int workspaceId, int userId, int cameraId){
    QString networkCamera = "";
    int indexCamera = -1;
    QString cameraIdString = QString::number(cameraId);
    QSettings settings;
    settings.beginGroup(QString::number(userId));
    //check all camera save if exits
    int sizeCameras = settings.beginReadArray("cameras");
    qDebug() <<Q_FUNC_INFO << "sizeCameras" << sizeCameras;

    for (int index = 0; index < sizeCameras; ++index) {
        settings.setArrayIndex(index);
        QString cameraIdSave = settings.value("cameraid").toString();
        if(cameraIdString == cameraIdSave){
            indexCamera = index;
        }
    }
    settings.endArray();
    QString cameraIdSave;
    if(indexCamera != -1){
        //if exits
        settings.beginReadArray("cameras");
        settings.setArrayIndex(indexCamera);
        cameraIdSave = settings.value("cameraid").toString();
        if(cameraIdSave == cameraIdString){
            networkCamera = settings.value("network").toString();
        }
        settings.endArray();
    }
    settings.endGroup();
    return networkCamera;
}



QString CameraNetworkSettings::getDataSourceSaveOfCamera(int workspaceId, int userId, int cameraId){
    QString dataSourceCamera = "";
    int indexCamera = -1;
    QString cameraIdString = QString::number(cameraId);
    QSettings settings;
    settings.beginGroup(QString::number(userId));
    settings.beginGroup(QString::number(workspaceId));
    //check all camera save if exits
    int sizeCameras = settings.beginReadArray("cameras");
    qDebug() <<Q_FUNC_INFO << "sizeCameras" << sizeCameras;

    for (int index = 0; index < sizeCameras; ++index) {
        settings.setArrayIndex(index);
        QString cameraIdSave = settings.value("cameraid").toString();
        if(cameraIdString == cameraIdSave){
            indexCamera = index;
        }
    }
    settings.endArray();

    QString cameraIdSave;
    if(indexCamera != -1){
        //if exits
        settings.beginReadArray("cameras");
        settings.setArrayIndex(indexCamera);
        cameraIdSave = settings.value("cameraid").toString();
        if(cameraIdSave == cameraIdString){
            dataSourceCamera = settings.value("data_source_camera").toString();
        }
        settings.endArray();
    }
    settings.endGroup();
    settings.endGroup();
    return dataSourceCamera;
}
