#include "p_topcontrolbar.h"

/**
     * Generic method to override for updating the presention.
     **/

P_TopControlBar::P_TopControlBar(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    this->zone->installEventFilter(this);
    mScreenSize = Resources::instance().getScreenSize();
    this->zone->setStyleSheet("background-color: #404244; color: #ddd");

    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect(this->zone);
    effect->setBlurRadius(80);
    effect->setColor(QColor("#000000"));
    effect->setOffset(0,0);
    this->zone->setGraphicsEffect(effect);

    // devide left & right
    QHBoxLayout *twoPartsLayout = new QHBoxLayout();
    twoPartsLayout->setSpacing(0);
    twoPartsLayout->setMargin(0);
    this->zone->setLayout(twoPartsLayout);
    leftZone = new QWidget(this->zone);
    rightZone = new QWidget(this->zone);
    twoPartsLayout->addWidget(leftZone);
    twoPartsLayout->addWidget(rightZone);

    // setup left zone
    leftZone->setMinimumWidth(this->zone->width() * 0.4);
    QHBoxLayout *leftZoneLayout = new QHBoxLayout();
    leftZoneLayout->setAlignment(Qt::AlignLeft);
    leftZoneLayout->setSpacing(20);
    leftZoneLayout->setContentsMargins(25, 0, 0, 0);
    leftZone->setLayout(leftZoneLayout);

    // setup left element
    brandNameZone = new QWidget(this->zone);
    //  brandNameZone->setFixedWidth(150);
    siteNameZone = new QWidget(this->zone);
    //  siteNameZone->setFixedWidth(250);

    appNameZone = new QWidget(this->zone);
    appNameZone->setFixedWidth(170);
    appNameZone->setStyleSheet("background-color:#404244");
    siteNameZone->setStyleSheet("background-color:#404244");
    QHBoxLayout *brandNameZoneLayout = new QHBoxLayout();
    brandNameZoneLayout->setAlignment(Qt::AlignLeft);
    brandNameZoneLayout->setSpacing(0);
    brandNameZoneLayout->setMargin(0);
    brandNameZone->setLayout(brandNameZoneLayout);

    QHBoxLayout *siteNameZoneLayout = new QHBoxLayout();
    siteNameZoneLayout->setAlignment(Qt::AlignLeft);
    siteNameZoneLayout->setSpacing(10);
    siteNameZoneLayout->setContentsMargins(15, 0, 15, 0);
    siteNameZone->setLayout(siteNameZoneLayout);

    QHBoxLayout *appNameZoneLayout = new QHBoxLayout();
    appNameZoneLayout->setAlignment(Qt::AlignLeft);
    appNameZoneLayout->setSpacing(10);
    appNameZoneLayout->setContentsMargins(15, 0, 15, 0);
    appNameZone->setLayout(appNameZoneLayout);

    leftZoneLayout->addWidget(brandNameZone);
    leftZoneLayout->addWidget(siteNameZone);
    leftZoneLayout->addWidget(appNameZone);

    brandLogo = new QLabel(brandNameZone);
    brandLogo->setFixedSize(75, 35);
    brandLogo->setScaledContents(true);
    loadLogoApp();
    // load logo

    QLabel *brandName = new QLabel(brandNameZone);
    brandName->setFont(Resources::instance().getExtraLargeRegularButtonFont());

    // site icon
    this->siteIcon = new QLabel(siteNameZone);
    this->siteIcon->setFont(Resources::instance().getExtraLargeRegularButtonFont());
    this->siteIcon->setText("");

    // site name
    this->siteName = new QPushButton(siteNameZone);
    this->siteName->setStyleSheet(
                "background-color: #00000000; color: #FFFFFF;border: none;");
    this->siteName->setFont(Resources::instance().getExtraLargeRegularButtonFont());

    // app icon
    this->appIcon = new QLabel(appNameZone);
    this->appIcon->setFont(Resources::instance().getExtraLargeRegularButtonFont());
    this->appName = new QPushButton(appNameZone);
    this->appName->setFont(Resources::instance().getExtraLargeRegularButtonFont());
    this->appName->setStyleSheet(
                "background-color: #00000000 ; color: #FFFFFF;border: none;");
    brandNameZoneLayout->addWidget(brandLogo);
    brandNameZoneLayout->addWidget(brandName);

    siteNameZoneLayout->addWidget(siteIcon);
    siteNameZoneLayout->addWidget(siteName);

    appNameZoneLayout->addWidget(appIcon);
    appNameZoneLayout->addWidget(appName);

    QString brandNameString =
            AppProfile::getAppProfile()->getAppConfig()->getBrandName();
    QString first = brandNameString.mid(0, 1);
    QString second = brandNameString.mid(1);
    brandName->setText(brandNameString);

    Site * workingSite = control()->appContext->getWorkingSite();
    if( workingSite != Q_NULLPTR){
        siteName->setText(workingSite->getSiteName());
    }
    UserApp appWorking = control()->appContext->getWorkingApp();
    appIcon->setText(appWorking.appIcon);
    appName->setText(appWorking.appName);

    // setup right zone
    rightZone->setMinimumWidth(this->zone->width() * 0.6);
    QHBoxLayout *rightZoneLayout = new QHBoxLayout();
    rightZoneLayout->setAlignment(Qt::AlignRight);
    rightZoneLayout->setSpacing(20);
    rightZoneLayout->setContentsMargins(0, 0, 5, 0);
    rightZone->setLayout(rightZoneLayout);

    //init update version videowall

    updateVersionWidget = new QWidget(rightZone);
    updateVersionWidget->hide();


    updateVersionWidget->setStyleSheet("background-color:#404244; color: #FFFFFF");
    rightZoneLayout->addWidget(updateVersionWidget);

    updateVersionButton = new QPushButton(updateVersionWidget);
    connect(updateVersionButton, &QPushButton::clicked, this,
            &P_TopControlBar::updateVersionButtonClicked);
    updateVersionButton->setStyleSheet(
                "background-color: #cf202e ; color: #FFFFFF; border: none;");
    stateButtonTopBar();
    updateVersionLayout = new QHBoxLayout();
    updateVersionLayout->setAlignment(Qt::AlignCenter);
    updateVersionLayout->setSpacing(5);
    updateVersionLayout->setMargin(0);
    updateVersionWidget->setLayout(updateVersionLayout);
    updateVersionLayout->addWidget(updateVersionButton);
    //first hide
    updateVersionButton->setFixedSize(0,0);
    updateVersionWidget->setFixedWidth(0);
    // init full screen

    fullScreenWidget = new QWidget(rightZone);
    fullScreenWidget->setStyleSheet("background-color:#404244");
    fullScreenWidget->setFixedWidth(120);
    rightZoneLayout->addWidget(fullScreenWidget);

    fullScreenButton = new QPushButton(fullScreenWidget);
    fullScreenButton->setFont(Resources::instance().getExtraLargeRegularButtonFont());
    fullScreenButton->setToolTip("Exit Fullscreen Mode");
    fullScreenButton->setFixedWidth(120);
    connect(fullScreenButton, &QPushButton::clicked, this,
            &P_TopControlBar::clickFullSreen);
    fullScreenButton->setStyleSheet(
                "background-color: #00000000 ; color: #FFFFFF; border: none;");
    fullScreenButton->setText("");
    QHBoxLayout *fullScreenLayout = new QHBoxLayout();
    fullScreenLayout->setSpacing(10);
    fullScreenLayout->setMargin(0);
    fullScreenWidget->setLayout(fullScreenLayout);
    fullScreenLayout->addWidget(fullScreenButton);

    //Init download screen
    downloadSelectorWidget = new QWidget(rightZone);
    downloadSelectorWidget->setStyleSheet("background-color:#404244");
    downloadSelectorWidget->setFixedWidth(120);
    rightZoneLayout->addWidget(downloadSelectorWidget);

    downloadScreenButton = new QPushButton(downloadSelectorWidget);
    downloadScreenButton->setFont(Resources::instance().getExtraLargeRegularButtonFont());
    downloadScreenButton->setFixedWidth(120);
    downloadScreenButton->setStyleSheet(
                "background-color: #00000000 ; color: #FFFFFF; border: none;");
    downloadScreenButton->setText("");
    QHBoxLayout *downloadScreenLayout = new QHBoxLayout();
    downloadScreenLayout->setSpacing(10);
    downloadScreenLayout->setMargin(0);
    downloadSelectorWidget->setLayout(downloadScreenLayout);
    downloadScreenLayout->addWidget(downloadScreenButton);
    //end download screen

    //init profile
    profileSettingWidget = new QWidget(rightZone);
    profileSettingWidget->setFixedWidth(150);
    profileSettingWidget->setStyleSheet("background-color:#404244");
    rightZoneLayout->addWidget(profileSettingWidget);

    QHBoxLayout *profileSettingLayout = new QHBoxLayout();
    profileSettingLayout->setSpacing(10);
    profileSettingLayout->setMargin(10);
    profileSettingWidget->setLayout(profileSettingLayout);

    profileSetting = new QPushButton(profileSettingWidget);
    profileSetting->setStyleSheet(
                "background-color: #00000000 ; color: #FFFFFF; border: none;");
    profileSettingLayout->addWidget(profileSetting);
    profileSetting->setFont(Resources::instance().getExtraLargeRegularButtonFont());
    profileSetting->setText("Chức Năng");

    // connect action left
    connect(siteName, &QPushButton::clicked, this,
            &P_TopControlBar::onSiteNameClick);
    connect(appName, &QPushButton::clicked, this,
            &P_TopControlBar::onAppNameClick);
    connect(profileSetting, &QPushButton::clicked, this,
            &P_TopControlBar::onProfileSettingClick);
    connect(downloadScreenButton, &QPushButton::clicked, this,
            &P_TopControlBar::onDownloadSettingClick);

    siteSelectorZone = new QWidget(control()->appContext->getMainWindow());
    appSelectorZone = new QWidget(control()->appContext->getMainWindow());
    profileSettingZone = new QWidget(control()->appContext->getMainWindow());
    infoNewVersionWidget = new QWidget(control()->appContext->getMainWindow());
    downloadSelectorZone = new QWidget(control()->appContext->getMainWindow());


    appNameZone->setMouseTracking(true);
    appNameZone->setAttribute(Qt::WA_Hover);
    appNameZone->installEventFilter(this);

    siteNameZone->setMouseTracking(true);
    siteNameZone->setAttribute(Qt::WA_Hover);
    siteNameZone->installEventFilter(this);

    profileSettingWidget->setMouseTracking(true);
    profileSettingWidget->setAttribute(Qt::WA_Hover);
    profileSettingWidget->installEventFilter(this);

    updateVersionWidget->setMouseTracking(true);
    updateVersionWidget->setAttribute(Qt::WA_Hover);
    updateVersionWidget->installEventFilter(this);

    downloadSelectorWidget->setMouseTracking(true);
    downloadSelectorWidget->setAttribute(Qt::WA_Hover);
    downloadSelectorWidget->installEventFilter(this);
    qDebug() << "P_TopControlBar Success";
}

void P_TopControlBar::stateButtonTopBar(){
    QSettings settings;
    int stateUpdateVersionCurrent = settings.value("state_update_version", -1).toInt();
    if(stateUpdateVersionCurrent ==  versionUpdateStates.APP_VERSION_UPDATING){
        updateVersionButton->setText(iconButtonTops.download + " " + versionUpdateStrings.APP_VERSION_UPDATING);
    }else if(stateUpdateVersionCurrent ==  versionUpdateStates.APP_HAVE_VERSION_NEWEST){
        updateVersionButton->setText(iconButtonTops.download + " " + versionUpdateStrings.APP_HAVE_VERSION_NEWEST);
    }if(stateUpdateVersionCurrent ==  versionUpdateStates.APP_VERSION_UPDATED){
        updateVersionButton->setText(iconButtonTops.download + " " + versionUpdateStrings.APP_VERSION_UPDATED);
    }
}
void P_TopControlBar::updateVersionButtonClicked(){

    qDebug() << "updateVersionButtonClicked";
    control()->newUserAction(Message.VERSION_UPDATER_SHOW, Q_NULLPTR);

}

void P_TopControlBar::showUpdateVersionNewest(){
    stateButtonTopBar();
    //if need update version newest
    updateVersionWidget->show();
    updateVersionButton->setFixedSize(widthUpdateVersion, 25);
    updateVersionWidget->setFixedWidth(widthUpdateVersion);
}

void P_TopControlBar::hideUpdateVersionNewest(){
    updateVersionWidget->hide();
    updateVersionButton->setFixedSize(0, 0);
    updateVersionWidget->setFixedSize(0,0);
}

// action left element
void P_TopControlBar::onSiteNameClick() {
    control()->newUserAction(Message.SITE_SELECTOR_SHOW, Q_NULLPTR);
}

void P_TopControlBar::onAppNameClick() {
    control()->newUserAction(Message.APP_SELECTOR_SHOW, Q_NULLPTR);
    //  appName->setStyleSheet("background:#e74c3c;color:#FFFFFF");
}

void P_TopControlBar::onProfileSettingClick() {
    control()->newUserAction(Message.PROFILE_SETTING_SHOW, Q_NULLPTR);
}

void P_TopControlBar::onDownloadSettingClick() {
    control()->newUserAction(Message.DOWNLOAD_SELECTOR_SHOW, Q_NULLPTR);
}

// action right element
void P_TopControlBar::onPageControlClick() {}

void P_TopControlBar::onLayoutControlClick() {}

void P_TopControlBar::show(QVariant *attachment) { Q_UNUSED(attachment) }

void P_TopControlBar::hide() {}

void P_TopControlBar::update() {}

void P_TopControlBar::updateStateButtonTopBar(){
    QSettings settings;
    int stateUpdateVersionCurrent = settings.value("state_update_version", -1).toInt();
    if(stateUpdateVersionCurrent == versionUpdateStates.APP_VERSION_UPDATING){
        updateVersionButton->setText(iconButtonTops.download + " " + versionUpdateStrings.APP_VERSION_UPDATING);
    }else if(stateUpdateVersionCurrent ==  versionUpdateStates.APP_VERSION_UPDATED){
        updateVersionButton->setText(iconButtonTops.download + " " + versionUpdateStrings.APP_VERSION_UPDATED);
    }
}

QWidget *P_TopControlBar::getZone(int zoneId) {
    switch (zoneId) {
    case Message.SITE_SELECTOR:
        return siteSelectorZone;
    case Message.APP_SELECTOR:
        return appSelectorZone;
    case Message.PROFILE_SETTING:
        return profileSettingZone;
    case Message.ZONE_VERSION_UPDATER:
        return infoNewVersionWidget;
    case Message.DOWNLOAD_SELECTOR:
        return downloadSelectorZone;
    default:
        return Q_NULLPTR;
    }
}

void P_TopControlBar::loadLogoApp() {
    QString folder = QDir::homePath();
    folder = folder.append("/.videowall/images/logo");
    if (!QDir(folder).exists()) {
        QDir().mkpath(folder);
    }
    QString path = folder + QString("/logo.png");
    QPixmap pixmap(path);

    brandLogo->setPixmap(pixmap);
}

void P_TopControlBar::siteChanged() {
    this->siteIcon->setText("");
    Site *workingSite = control()->appContext->getWorkingSite();
    if(workingSite != Q_NULLPTR){
        this->siteName->setText(workingSite->getSiteName());
    }
}


void P_TopControlBar::enableSelectedApp() {
    siteName->setEnabled(true);
    appName->setEnabled(true);
}
void P_TopControlBar::disableSelectedApp() {
    siteName->setEnabled(false);
    appName->setEnabled(false);
}
void P_TopControlBar::appChanged() {
    this->appIcon->setText(control()->appContext->getWorkingApp().appIcon);
    this->appName->setText(control()->appContext->getWorkingApp().appName);
}

bool P_TopControlBar::eventFilter(QObject *watched, QEvent *event) {
    QWidget *widget = qobject_cast<QWidget *>(watched);
    if(widget == this->zone){
        switch (event->type()) {
        case QEvent::Resize:{
            QResizeEvent *resizeEvent = (QResizeEvent*)(event);
            QSize newSizeZone = resizeEvent->size();
            this->zone->setFixedWidth(newSizeZone.width());
            leftZone->setFixedWidth(newSizeZone.width() * 0.4);
            rightZone->setFixedWidth(newSizeZone.width() * 0.6);

            QVariant *dataStruct = new QVariant();
            SizeTopControlBar dataSize;
            dataSize.widthBrand = brandNameZone->width();
            dataSize.widthSite = siteNameZone->width();
            dataSize.widthAppName = appNameZone->width();
            dataSize.widthProfileSetting = profileSetting->width();
            dataSize.widthTopBar = this->zone->width();
            dataSize.widthInfoUpdateNewVersion = this->infoNewVersionWidget->width();
            dataSize.widthFullScreen = this->fullScreenWidget->width();
            dataSize.widthDownloadSelector = this->downloadSelectorWidget->width();
            dataStruct->setValue<SizeTopControlBar>(dataSize);
            control()->newUserAction(Message.GET_SIZE_TOP_CONTROL_BAR,
                                     dataStruct);
            return false;
        }break;
        default:
            return false;
            break;
        }
    }

    if (widget == siteNameZone || widget == appNameZone ||
            widget == profileSettingWidget || widget == updateVersionWidget ||widget == downloadSelectorWidget) {
        switch (event->type()) {
        case QEvent::HoverEnter: {
            if (widget == appNameZone) {
                updateVersionWidget->setStyleSheet("background-color:#404244; color: #FFFFFF");
                updateVersionButton->setStyleSheet(
                            "background-color: #cf202e ; color: #FFFFFF; border: none;");
                appNameZone->setStyleSheet("background:#445a4d;color:#FFFFFF");
                siteNameZone->setStyleSheet("background:#404244;color:#FFFFFF");
                profileSettingWidget->setStyleSheet(
                            "background:#404244;color:#FFFFFF");
                downloadSelectorWidget->setStyleSheet(
                            "background:#404244;color:#FFFFFF");
                control()->newUserAction(Message.VERSION_UPDATER_HIDE , Q_NULLPTR);
                control()->newUserAction(Message.APP_SELECTOR_SHOW, Q_NULLPTR);
                control()->newUserAction(Message.SITE_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.PROFILE_SETTING_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.DOWNLOAD_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.TOP_BAR_OVER_LAY_CAN_NOT_HIDE,
                                         Q_NULLPTR);
            } else if (widget == siteNameZone) {
                updateVersionWidget->setStyleSheet("background-color:#404244; color: #FFFFFF");
                updateVersionButton->setStyleSheet(
                            "background-color: #cf202e ; color: #FFFFFF; border: none;");
                siteNameZone->setStyleSheet("background:#445a4d;color:#FFFFFF");
                appNameZone->setStyleSheet("background:#404244;color:#FFFFFF");
                profileSettingWidget->setStyleSheet(
                            "background:#404244;color:#FFFFFF");
                downloadSelectorWidget->setStyleSheet(
                            "background:#404244;color:#FFFFFF");
                control()->newUserAction(Message.DOWNLOAD_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.VERSION_UPDATER_HIDE , Q_NULLPTR);
                control()->newUserAction(Message.APP_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.SITE_SELECTOR_SHOW, Q_NULLPTR);
                control()->newUserAction(Message.PROFILE_SETTING_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.TOP_BAR_OVER_LAY_CAN_NOT_HIDE,
                                         Q_NULLPTR);
            } else if (widget == profileSettingWidget) {
                profileSettingWidget->setStyleSheet(
                            "background:#445a4d;color:#FFFFFF");
                siteNameZone->setStyleSheet("background:#404244;color:#FFFFFF");
                appNameZone->setStyleSheet("background:#404244;color:#FFFFFF");
                updateVersionWidget->setStyleSheet("background-color:#404244; color: #FFFFFF");
                updateVersionButton->setStyleSheet(
                            "background-color: #cf202e ; color: #FFFFFF; border: none;");
                control()->newUserAction(Message.VERSION_UPDATER_HIDE , Q_NULLPTR);
                downloadSelectorWidget->setStyleSheet(
                            "background:#404244;color:#FFFFFF");
                control()->newUserAction(Message.DOWNLOAD_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.APP_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.SITE_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.PROFILE_SETTING_SHOW, Q_NULLPTR);
                control()->newUserAction(Message.TOP_BAR_OVER_LAY_CAN_NOT_HIDE,
                                         Q_NULLPTR);
            }else if (widget == updateVersionWidget) {
                updateVersionWidget->setStyleSheet(
                            "background:#404244;color:#FFFFFF");
                updateVersionButton->setStyleSheet(
                            "background-color: #ef2636 ; color: #FFFFFF; border: none;");
                siteNameZone->setStyleSheet("background:#404244;color:#FFFFFF");
                appNameZone->setStyleSheet("background:#404244;color:#FFFFFF");
                control()->newUserAction(Message.APP_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.SITE_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.PROFILE_SETTING_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.VERSION_UPDATER_SHOW , Q_NULLPTR);
                control()->newUserAction(Message.TOP_BAR_OVER_LAY_CAN_NOT_HIDE,
                                         Q_NULLPTR);
            } else if (widget == downloadSelectorWidget) {
                downloadSelectorWidget->setStyleSheet(
                            "background:#445a4d;color:#FFFFFF");
                profileSettingWidget->setStyleSheet(
                            "background:#404244;color:#FFFFFF");
                siteNameZone->setStyleSheet("background:#404244;color:#FFFFFF");
                appNameZone->setStyleSheet("background:#404244;color:#FFFFFF");

                updateVersionWidget->setStyleSheet("background-color:#404244; color: #FFFFFF");
                updateVersionButton->setStyleSheet(
                            "background-color: #cf202e ; color: #FFFFFF; border: none;");
                control()->newUserAction(Message.VERSION_UPDATER_HIDE , Q_NULLPTR);
                control()->newUserAction(Message.APP_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.SITE_SELECTOR_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.PROFILE_SETTING_HIDE, Q_NULLPTR);
                control()->newUserAction(Message.DOWNLOAD_SELECTOR_SHOW, Q_NULLPTR);
                control()->newUserAction(Message.TOP_BAR_OVER_LAY_CAN_NOT_HIDE,
                                         Q_NULLPTR);
            }
            return false;
        } break;

        case QEvent::HoverLeave: {
            return false;
        } break;

        case QEvent::HoverMove: {
            return false;
        } break;

        case QEvent::Resize: {
            if (widget == siteNameZone) {
                QVariant *dataStruct = new QVariant();
                SizeTopControlBar dataSize;
                dataSize.widthBrand = brandNameZone->width();
                dataSize.widthSite = siteNameZone->width();
                dataSize.widthAppName = appNameZone->width();
                dataSize.widthProfileSetting = profileSetting->width();
                dataSize.widthDownloadSelector = downloadScreenButton->width();
                dataSize.widthTopBar = this->zone->width();
                dataSize.widthInfoUpdateNewVersion = this->infoNewVersionWidget->width();
                dataSize.widthFullScreen = this->fullScreenWidget->width();
                dataStruct->setValue<SizeTopControlBar>(dataSize);
                control()->newUserAction(Message.GET_SIZE_TOP_CONTROL_BAR,
                                         dataStruct);
                return false;
            }
        } break;

        default:
            return false;
            break;
        }
    }

    //  return QWidget::eventFilter(watched, event);
    return false;
}

void P_TopControlBar::hideElementTop() {
    appNameZone->setStyleSheet("background:#404244;color:#FFFFFF");
    siteNameZone->setStyleSheet("background:#404244;color:#FFFFFF");
    profileSettingWidget->setStyleSheet("background:#404244;color:#FFFFFF");
    updateVersionWidget->setStyleSheet(
                "background:#404244;color:#FFFFFF");
    updateVersionButton->setStyleSheet(
                "background-color: #cf202e ; color: #FFFFFF; border: none;");
    downloadSelectorWidget->setStyleSheet("background:#404244;color:#FFFFFF");
}

void P_TopControlBar::enterFullscreenMode() {
}

// void P_TopControlBar::exitFullscreenMode() {
//  rightBar->setFixedWidth(appSize.rightWidth);
//}

void P_TopControlBar::clickFullSreen() {
    if (isButtonFullcreenClick == false) {
        isButtonFullcreenClick = true;
        fullScreenButton->setToolTip("Fullscreen Mode");

        control()->getParent()->newAction(Message.EXIT_FULLSCREEN_MODE, Q_NULLPTR);
        fullScreenWidget->setStyleSheet("background:#445a4d;color:#FFFFFF");

    } else {
        fullScreenButton->setToolTip("Exit Fullscreen Mode");
        fullScreenWidget->setStyleSheet("background:#404244;color:#FFFFFF");
        isButtonFullcreenClick = false;
        control()->getParent()->newAction(Message.ENTER_FULLSCREEN_MODE, Q_NULLPTR);
        control()->getParent()->newAction(Message.TOP_BAR_OVER_LAY_CAN_HIDE,
                                          Q_NULLPTR);
    }
}
