#include "p_profilesetting.h"

/**
     * Generic method to override for updating the presention.
     **/

P_ProfileSetting::P_ProfileSetting(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    // init gui object
    this->zone->setStyleSheet("background-color: #fff");
    this->zone->hide();

    QVBoxLayout *layout = new QVBoxLayout();
    layout->setContentsMargins(0, 10, 0, 10);
    layout->setSpacing(0);
    layout->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    this->zone->setLayout(layout);

    settingButton = new QPushButton(this->zone);
    settingButton->setFixedSize(150 , 30);
    settingButton->setIcon(QIcon(":/images/res/icon_tab_general.png"));
    settingButton->setIconSize(QSize(16, 16));
    settingButton->setLayoutDirection(Qt::LayoutDirection::LeftToRight);
    settingButton->setFont(Resources::instance().getLargeRegularButtonFont());
    settingButton->setStyleSheet(
                "QPushButton {background-color: #00000000 ; color: #2c3e50;text-align:left;border:none; padding-left:10px;}"
                "QPushButton:hover {background-color: #9bdcd0 ; color: #2c3e50;text-align:left;border:none;padding-left:10px;}");

    settingButton->setText("Cài Đặt");
    connect(settingButton, &QPushButton::clicked, this,
            &P_ProfileSetting::onSettingClicked);

    exitButton = new QPushButton(this->zone);
    exitButton->setIcon(QIcon(":/images/res/icon_tab_exit.png"));
    exitButton->setIconSize(QSize(16, 16));
    exitButton->setLayoutDirection(Qt::LayoutDirection::LeftToRight);
    exitButton->setFont(Resources::instance().getLargeRegularButtonFont());
    exitButton->setFixedSize(150 , 30);
    exitButton->setStyleSheet(
                "QPushButton {background-color: #00000000 ; color: #2c3e50;text-align:left;border:none; padding-left:10px;}"
                "QPushButton:hover {background-color: #9bdcd0 ; color: #2c3e50;text-align:left;border:none;padding-left:10px;}");
    exitButton->setText("Thoát");
    connect(exitButton, &QPushButton::clicked, this,
            &P_ProfileSetting::onExitButtonClicked);

    logoutButton = new QPushButton(this->zone);
    logoutButton->setIcon(QIcon(":/images/res/icon_tab_logout.png"));
    logoutButton->setIconSize(QSize(16, 16));
    logoutButton->setLayoutDirection(Qt::LayoutDirection::LeftToRight);
    logoutButton->setFont(Resources::instance().getLargeRegularButtonFont());
    connect(logoutButton, &QPushButton::clicked, this,
            &P_ProfileSetting::onLogoutButtonClicked);
    logoutButton->setText("Đăng Xuất");
    QString offlineMode = control()->appContext->getOfflineMode();
    if(offlineMode == "OFF"){
        logoutButton->setFixedSize(150 , 30);
        logoutButton->show();
        logoutButton->setStyleSheet(
                    "QPushButton {background-color: #00000000 ; color: #2c3e50;text-align:left;border:none; padding-left:10px;}"
                    "QPushButton:hover {background-color: #9bdcd0 ; color: #2c3e50;text-align:left;border:none;padding-left:10px;}");
    }
    if(offlineMode == "ON"){
        logoutButton->setFixedSize(0 , 0);
        logoutButton->hide();
    }
    aboutButton = new QPushButton(this->zone);
    aboutButton->setIcon(QIcon(":/images/res/icon_about.png"));
    aboutButton->setIconSize(QSize(16, 16));
    aboutButton->setLayoutDirection(Qt::LayoutDirection::LeftToRight);
    aboutButton->setFont(Resources::instance().getLargeRegularButtonFont());
    connect(aboutButton, &QPushButton::clicked, this,
            &P_ProfileSetting::onAboutButtonClicked);
    aboutButton->setText("Giới Thiệu");
    aboutButton->setFixedSize(150 , 30);
    aboutButton->setStyleSheet(
                "QPushButton {background-color: #00000000 ; color: #2c3e50;text-align:left;border:none; padding-left:10px;}"
                "QPushButton:hover {background-color: #9bdcd0 ; color: #2c3e50;text-align:left;border:none;padding-left:10px;}");

    helpUserButton = new QPushButton(this->zone);
    helpUserButton->setIcon(QIcon(":/images/res/icon_help.png"));
    helpUserButton->setIconSize(QSize(16, 16));
    helpUserButton->setLayoutDirection(Qt::LayoutDirection::LeftToRight);
    helpUserButton->setFont(Resources::instance().getLargeRegularButtonFont());
    connect(helpUserButton, &QPushButton::clicked, this,
            &P_ProfileSetting::onHelpUserButtonClicked);
    helpUserButton->setText("Giúp Đỡ");
    helpUserButton->setFixedSize(150 , 30);
    helpUserButton->setStyleSheet(
                "QPushButton {background-color: #00000000 ; color: #2c3e50;text-align:left;border:none; padding-left:10px;}"
                "QPushButton:hover {background-color: #9bdcd0 ; color: #2c3e50;text-align:left;border:none;padding-left:10px;}");


    layout->addWidget(settingButton);
    layout->addWidget(helpUserButton);
    layout->addWidget(aboutButton);
    layout->addWidget(exitButton);
    layout->addWidget(logoutButton);


    this->zone->setMouseTracking(true);
    this->zone->setAttribute(Qt::WA_Hover);
    this->zone->installEventFilter(this);
}

void P_ProfileSetting::onHelpUserButtonClicked(){
    control()->newUserAction(Message.SHOW_HELP_USER, Q_NULLPTR);
}

void P_ProfileSetting::onAboutButtonClicked(){
    control()->newUserAction(Message.SHOW_ABOUT_APP, Q_NULLPTR);
}

void P_ProfileSetting::update() {}

void P_ProfileSetting::show() {
    if (control()->zone->isVisible()) {
        control()->zone->hide();
    } else {
        control()->zone->show();
        control()->zone->raise();
    }
}

void P_ProfileSetting::onExitButtonClicked() {
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue(Q_NULLPTR);
    control()->newUserAction(Message.EXIT_APP, dataStruct);
}

void P_ProfileSetting::onLogoutButtonClicked() {
    control()->newUserAction(Message.LOGOUT, Q_NULLPTR);
}

void P_ProfileSetting::onSettingClicked() {
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue(0);
    control()->newUserAction(Message.APP_SHOW_SETTINGS, dataStruct);
}

void P_ProfileSetting::hide() { control()->zone->hide(); }

QWidget *P_ProfileSetting::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}

bool P_ProfileSetting::eventFilter(QObject *watched, QEvent *event) {
    QWidget *zone = qobject_cast<QWidget *>(watched);
    if (zone == this->control()->zone) {
        switch (event->type()) {
        case QEvent::HoverEnter:{
            hoverEnter(static_cast<QHoverEvent *>(event));
            return false;
        } break;
        case QEvent::HoverLeave:{
            hoverLeave(static_cast<QHoverEvent *>(event));
            return false;
        }
            break;
        case QEvent::HoverMove:{
            hoverMove(static_cast<QHoverEvent *>(event));
            return false;
        }
            break;
        default:
            return false;
            break;
        }
    }
    //  return QWidget::eventFilter(watched, event);
    return false;
}

void P_ProfileSetting::hoverEnter(QHoverEvent *) {}

void P_ProfileSetting::hoverLeave(QHoverEvent *) {
    control()->newUserAction(Message.PROFILE_SETTING_HIDE_ALL, Q_NULLPTR);
    control()->zone->hide();
}

void P_ProfileSetting::hoverMove(QHoverEvent *) {}

void P_ProfileSetting::sizeTopControlBar(QVariant *dataStruct) {
    dataSizeTop = dataStruct->value<SizeTopControlBar>();
    int widthMove =
            dataSizeTop.widthTopBar - 5 - dataSizeTop.widthProfileSetting - 20;
    // init gui object
    this->zone->move(widthMove, appSize.topHeight);

    this->zone->setFixedSize(150, 150);
}
