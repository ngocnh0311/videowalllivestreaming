#ifndef CLIPPERITEM_H
#define CLIPPERITEM_H

#include <QWidget>
#include <QLabel>
#include <QProgressBar>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QtAwesome/QtAwesome.h>
#include <QPushButton>
#include <QListWidgetItem>
#include "PlayBack/Clipper/clippercommandqueue.h"
#include "PlayBack/Clipper/common.h"
extern QtAwesome* awesome;

class ClipperItem : public QWidget
{
    Q_OBJECT

    ClipperCommand command;

    QPushButton* icon;
    QLabel* title;
    QPushButton* cancel;
    QPushButton* play;
    QPushButton* browse;

    QProgressBar* progressBar;

    QHBoxLayout* clipperWrapperLayout;
    QVBoxLayout* clipperItemLayout;

    QWidget* mZone;

    bool bClicked;
    bool bNeedToRemove;

    QListWidgetItem* mWidgetItem;
    CLipperReport clipperReport;
    void initUi();
public:

    ClipperItem(ClipperCommand* _command, QWidget* qParent = Q_NULLPTR);
    QString getTitleValue() const;
    QProgressBar *getProgressBar() const;
    void updateProgressBar(ClipperCommand* cmd, bool bFocus = false);

    void changeColorWhenCliked();
    void resetColor();

    void setListItem(QListWidgetItem* item) { mWidgetItem = item ;}
    QListWidgetItem* getListItem() { return mWidgetItem;}

    bool isNeedToRemove() {return bNeedToRemove;}
    void setNeedToRemove(bool status) {bNeedToRemove = status ;}

    void setFocusColor(bool );

    ClipperCommand getCommand() { return command; }
public Q_SLOTS:
    void playClicked();
    void browseClicked();
    void cancelClicked();
};

#endif // CLIPPERITEM_H
