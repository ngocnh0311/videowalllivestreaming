#include "clipperitem.h"

#include <QDateTime>
#include <QFileDialog>
#include <QProcess>
#include <QFileInfo>
#include <qfiledialog.h>
#include <QDesktopServices>

#include "PlayBack/PBWorkSpace/p_pb_workspace.h"

ClipperItem::ClipperItem(ClipperCommand* _command, QWidget* parent)
{
    command = *_command;
    progressBar = Q_NULLPTR;

    bClicked = false;
    bNeedToRemove = false;

    mZone = parent;
    initUi();
}

QString ClipperItem::getTitleValue() const
{
    return command.getTitle();
}

QProgressBar *ClipperItem::getProgressBar() const
{
    return progressBar;
}

void ClipperItem::updateProgressBar(ClipperCommand* cmd, bool bFocus)
{
    command = *cmd;

    setFocusColor(bFocus);

    //Set status COMPLETED
    if(command.getPercent() == 100 && command.getDone())
    {
        //Hide progress bar and show all button
        progressBar->hide();
        browse->show();
        cancel->show();
        clipperItemLayout->removeWidget(progressBar);

    } else {

        //Updating progress bar
        progressBar->setValue(command.getPercent());
    }
}

void ClipperItem::setFocusColor(bool bFocus) {

    //Prepare File name and status to show
    QString fname, ftime, ftext;
    if(command.getDone())
    {
        QFileInfo fileinfo(command.getFilePath());
        fname = command.getOutputFilename();

        if( command.getStatusCode() == clipperReport.toString(clipperReport.CLIPPER_FINISHED) ) {

            if( fileinfo.size()/(1000*1000) > 0 )
                ftime = QString::number(fileinfo.size()/(1000*1000)) + " MB";
            else
                ftime = QString::number(fileinfo.size()/1000) + " kB";
        } else {
            ftime = "Failed";
        }

    }
    else {

        fname = command.getTitle();
        if(command.getPercent() == 0)
            ftime = "Waiting";
        else
            ftime = QString::number( (int) command.getPercent()) + " %";
    }

    if(bFocus)
        ftext = "<font size=2 color=#ffffff>%1</font><br><font size=1 color=#ffffff>%2</font>";
    else
        ftext = "<font size=2 color=#3f3f3f>%1</font><br><font size=1 color=#808080>%2</font>";

    title->setText(ftext.arg(fname.left(20) + ((fname.size() > 20)?"...":""), ftime));
}


void ClipperItem::changeColorWhenCliked() {

    bClicked = true;

    setFocusColor(bClicked);
}

void ClipperItem::resetColor() {

    bClicked = false;

    setFocusColor(bClicked);
}

void ClipperItem::initUi() {

    clipperWrapperLayout = new QHBoxLayout;
    clipperWrapperLayout->setSpacing(10);
    this->setLayout(clipperWrapperLayout);
    this->setStyleSheet("background:#00000000");

    //Add media type icon
    icon = new QPushButton(this);
    icon->setStyleSheet("border: none;");
    icon->setIcon(QIcon(":/images/res/icon_video_file_32_2.png"));
    icon->setFixedSize(25,25);
    icon->setIconSize(QSize(25,25));
    clipperWrapperLayout->addWidget(icon);

    clipperItemLayout = new QVBoxLayout;
    clipperWrapperLayout->addLayout(clipperItemLayout);

    //Create File name and status download
    QString fname, ftime;
    if( command.getDone() )
    {
        //If file complete, show real file name and file size
        QFileInfo fileinfo(command.getFilePath());
        fname = command.getOutputFilename();

        if( command.getStatusCode() == clipperReport.toString(clipperReport.CLIPPER_FINISHED) ) {

            if( fileinfo.size()/(1000*1000) > 0 )
                ftime = QString::number(fileinfo.size()/(1000*1000)) + " MB";
            else
                ftime = QString::number(fileinfo.size()/1000) + " kB";
        } else {
            ftime = "Failed";
        }

        if(fileinfo.suffix() == "png")
            icon->setIcon(QIcon(":/images/res/icon_image_5.png"));
    }
    else {
        //If still downloading, show file label and downloading percent
        fname = command.getTitle();

        if(command.getPercent() == 0)
            ftime = "Waiting";
        else
            ftime = QString::number( (int)command.getPercent()) + " %";
    }

    //QString ftime = "From: " + fromTime.toString(Qt::SystemLocaleShortDate) + " To: " + endTime.toString(Qt::SystemLocaleShortDate);
    QString ftext(  "<font size=2 color=#3f3f3f>%1</font><br><font size=1 color=#808080>%2</font>");

    title = new QLabel(this);
    title->setWordWrap(true);
    title->setText(ftext.arg(fname.left(25) + ((fname.size() > 25)?"...":""), ftime));
    clipperItemLayout->addWidget(title);

    //Update progress bar
    progressBar = new QProgressBar(this);
    progressBar->setFixedHeight(10);
    progressBar->setTextVisible(false);
    progressBar->setValue(command.getPercent());
    progressBar->hide();
    clipperItemLayout->addWidget(progressBar);

    //add Browse button
    browse = new QPushButton(this);
    browse->setStyleSheet("border: none;");
    browse->setIcon(QIcon(":/images/res/icon_search_3.png"));
    browse->setFixedSize(10,10);
    browse->setIconSize(QSize(10,10));

    if(command.getPercent() < 100)
        browse->hide();

    connect(browse , &QPushButton::clicked, this, &ClipperItem::browseClicked);
    clipperWrapperLayout->addWidget(browse);

    //Add cancel button
    cancel = new QPushButton(this);
    cancel->setStyleSheet("border: none;");
    cancel->setIcon(QIcon(":/images/res/icons_cancel.png"));
    cancel->setFixedSize(10,10);
    cancel->setIconSize(QSize(10,10));

    if(command.getPercent() < 100)
        cancel->hide();

    connect(cancel , &QPushButton::clicked, this, &ClipperItem::cancelClicked);
    clipperWrapperLayout->addWidget(cancel);
}

void ClipperItem::playClicked(){

    //Play file when file exist
    if(QFileInfo::exists(command.getFilePath())) {

        QFileInfo fileinfo(command.getFilePath());

        if(fileinfo.suffix() == "mp4") {

            QProcess::startDetached(QString("totem \"%1\" ").arg(command.getFilePath()));

        } else {

            QProcess::startDetached(QString("eog \"%1\" ").arg(command.getFilePath()));
        }

    }
}

void ClipperItem::browseClicked(){

    QFileInfo fileinfo(command.getFilePath());
    QString path;
    QSettings settings;
    //Open folder to view file
    path = fileinfo.absolutePath();
    QDesktopServices::openUrl(path);
}

void ClipperItem::cancelClicked(){

    //Remove file from list and file system
    if(QFileInfo::exists(command.getFilePath())) {

        QFileInfo fileinfo(command.getFilePath());

        QProcess::startDetached( QString("rm -rf \"%1\" ").arg(command.getFilePath()) );
    }

    qDebug() <<"cancelClicked";
    bNeedToRemove = true;

    //QTimer::singleShot(0, P_PBWorkSpace::clipperManager, &ClipperManager::abort);
}
