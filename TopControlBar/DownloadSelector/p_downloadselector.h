#ifndef P_DOWNLOADSELECTOR_H
#define P_DOWNLOADSELECTOR_H

#include <PacModel/presentation.h>
#include <TopControlBar/DownloadSelector/c_downloadselector.h>
#include <QAbstractItemView>
#include <QFont>
#include <QListView>
#include <QObject>
#include <QStringListModel>
#include <QVBoxLayout>
#include <QListWidget>
#include <QListWidgetItem>
#include "Common/generaldefine.h"
#include <Common/resources.h>
#include "Common/appconfig.h"
#include "Common/appprofile.h"
class C_DownloadSelector;
class ClipperItem;
class P_DownloadSelector : public Presentation {
  // init ui control
 private:
 public:
  QWidget *zone;
  SizeTopControlBar dataSizeTop;
  P_DownloadSelector(Control *ctrl, QWidget *zone);
  // init ui control
  QListView *mProfileListView;

  QLabel* bottomLabel;
  QListWidget* listWidget;
  //static QMap<long, ClipperItem*>  clipperItemsList;
  QMap<long, ClipperItem*>  clipperItemsList;
  QMutex listLock;


  C_DownloadSelector *control() { return (C_DownloadSelector *)this->ctrl; }
  void changeControl(Control *ctrl);
  void update();
  QWidget *getZone(int zoneId);
  void show();
  void hide();
  void sizeTopControlBar(QVariant *dataStruct);
  void listFileUpdate();
 private Q_SLOTS:


 protected:
  void hoverEnter(QHoverEvent *event);
  void hoverLeave(QHoverEvent *event);
  void hoverMove(QHoverEvent *event);
  bool eventFilter(QObject *watched, QEvent *event);

public Q_SLOTS:

  void onListItemDoubleClicked(QListWidgetItem* );
};

#endif  // P_DOWNLOADSELECTOR_H
