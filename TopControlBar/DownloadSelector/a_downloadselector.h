#ifndef A_DownloadSelector_H
#define A_DownloadSelector_H

#include <QObject>
#include "PacModel/control.h"
class C_DownloadSelector;
class A_DownloadSelector : public Abstraction {
  Q_OBJECT
  // A ref on the control facet
 private:
 public:
  A_DownloadSelector(Control *ctrl);
  void changeControl(Control *ctrl);
};

#endif  // A_DownloadSelector_H
