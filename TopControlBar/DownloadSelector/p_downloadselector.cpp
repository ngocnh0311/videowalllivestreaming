#include "p_downloadselector.h"

#include <QMapIterator>
#include <QScrollBar>
#include <QStorageInfo>

#include "TopControlBar/DownloadSelector/clipperitem.h"
#include "PlayBack/PBWorkSpace/p_pb_workspace.h"

/**
     * Generic method to override for updating the presention.
     **/

//QMap<long, ClipperItem*>  P_DownloadSelector::clipperItemsList;

P_DownloadSelector::P_DownloadSelector(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {
    this->zone = zone;
    // init gui object
    this->zone->hide();

    QVBoxLayout* mainLayout = new QVBoxLayout;
    this->zone->setLayout(mainLayout);


    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(0, 11, 0, 11);

    this->zone->setStyleSheet("background-color: #fff; border: none"); //414244
    //this->zone->setStyleSheet("background-color: rgba(255,255,255,200); border: none"); //414244

    //Add Label and Clear button
    //    QWidget *p1 = new QWidget;
    //    QHBoxLayout* downloadLayout = new QHBoxLayout;
    //    p1->setLayout(downloadLayout);
    //    downloadLayout->setSpacing(10);
    //    p1->setStyleSheet("background:#00000000");

    QFont fontText;
    fontText.setPixelSize(13);

    QLabel* title = new QLabel;
    title->setFont(fontText);
    title->setStyleSheet("color:#3f3f3f");
    title->setText("Downloads");
    title->setAlignment(Qt::AlignCenter);
    //downloadLayout->addWidget(title);

    //    QPushButton* clearButton = new QPushButton(p1);
    //    clearButton->setFixedSize(10,10);
    //    clearButton->setEnabled(true);
    //    clearButton->setText("Clear");
    //    downloadLayout->addWidget(clearButton);

    mainLayout->addWidget(title);
    //end Label

    listWidget = new QListWidget;
    listWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //listWidget->setStyleSheet("QListWidget:item {  border-bottom: 1px solid gray; }");
    listWidget->setStyleSheet("QListWidget:item:selected { color: black; }");

    connect(listWidget, &QListWidget::itemDoubleClicked, this, &P_DownloadSelector::onListItemDoubleClicked);

    mainLayout->addWidget(listWidget);

    QTimer* pollingTimer = new QTimer();
    connect(pollingTimer, &QTimer::timeout, this, &P_DownloadSelector::listFileUpdate);
    pollingTimer->start(20);

    bottomLabel = new QLabel;
    bottomLabel->setFixedHeight(25);
    bottomLabel->setStyleSheet("color:#3f3f3f");
    bottomLabel->setAlignment(Qt::AlignCenter);
    bottomLabel->setText("0 tập tin");
    bottomLabel->setFont(fontText);
    mainLayout->addWidget(bottomLabel);

    this->zone->setMouseTracking(true);
    this->zone->setAttribute(Qt::WA_Hover);
    this->zone->installEventFilter(this);
}

void P_DownloadSelector::onListItemDoubleClicked(QListWidgetItem* pItem) {

    ClipperItem* pClipperItem = clipperItemsList[pItem->data(Qt::UserRole).toLongLong()];

    qDebug()<<"Item was double clicked!"<<pClipperItem<<" "<<pItem<<" "<<pItem->data(Qt::UserRole).toLongLong();

    //Change text to white
    pClipperItem->playClicked();

}

void P_DownloadSelector::listFileUpdate() {

    QMap<long, ClipperItem*>::iterator itClipper;
    QMap<long,ClipperCommand*> listFileWidget = P_PBWorkSpace::mCommandQueue->getItems();
    QSettings settings;


    QString pathVideoSaved = settings.value("path_save_videos").toString();
    QStorageInfo storage;
    if(!pathVideoSaved.isEmpty()){
        QFileInfo dirVideosSave(pathVideoSaved);
        if(dirVideosSave.isDir() && dirVideosSave.isWritable()){
           storage = QStorageInfo(pathVideoSaved);
        }
    }else{
        storage = QStorageInfo::root();
    }

    if(P_PBWorkSpace::mCommandQueue && P_PBWorkSpace::mCommandQueue->size() > 0){

        QMapIterator<long , ClipperCommand*> it(listFileWidget);
        while (it.hasNext()) {
            it.next();

            itClipper = clipperItemsList.find(it.key());

            //If current list has no member like in Comman queue list, let create new one
            if(itClipper == clipperItemsList.end()) {

                ClipperItem* clipperItem = new ClipperItem(it.value(), this->zone);
                clipperItemsList[it.key()] = clipperItem;

                QListWidgetItem* lwi = new QListWidgetItem;
                clipperItem->setListItem(lwi);
                lwi->setData(Qt::UserRole, (qint64)it.key());
                lwi->setSizeHint( clipperItem->sizeHint() );
                listWidget->addItem(lwi);
                listWidget->setItemWidget(lwi, clipperItem);

            }
            else {

                QListWidgetItem* lwi = itClipper.value()->getListItem();

                if(itClipper.value()->isNeedToRemove()) {

                    listWidget->takeItem(listWidget->row(lwi));

                    P_PBWorkSpace::mCommandQueue->remove(it.key());
                    listFileWidget.remove(it.key());
                    clipperItemsList.remove(it.key());

                    delete itClipper.value();

                } else {

                    //Or updating progress bar
                    //                    if(lwi->isSelected()){
                    //                        itClipper.value()->changeColorWhenCliked();
                    //                    } else {
                    //                        itClipper.value()->resetColor();
                    //                    }

                    itClipper.value()->updateProgressBar( it.value(), lwi->isSelected());
                }

            }
        }

        //Find different between command list and clipper item list
        QList<long> listFileKey = listFileWidget.keys();
        QList<long> clipperItemKey = clipperItemsList.keys();

        QSet<long> unusedKeys = clipperItemKey.toSet().subtract( listFileKey.toSet() );

        if( !unusedKeys.isEmpty() ) {

            QSet<long>::iterator itKey = unusedKeys.begin();
            while (itKey != unusedKeys.end()) {

                itClipper = clipperItemsList.find(*itKey);

                if(itClipper != clipperItemsList.end()) {

                    QListWidgetItem* lwi = itClipper.value()->getListItem();
                    listWidget->takeItem(listWidget->row(lwi));

                    clipperItemsList.remove(*itKey);

                    delete itClipper.value();

                    itKey = unusedKeys.erase(itKey);

                } else {
                    ++itKey;
                }
            }

        }

        bottomLabel->setText(QString::number(P_PBWorkSpace::mCommandQueue->size()) + " tập tin"
                             + ". Ổ cứng còn trống " + QString::number(storage.bytesAvailable()/1000/1000/1000) + " GB"
                             );
    } else {

        //Remove unused clipper item
        if(!clipperItemsList.isEmpty() ) {

            QMapIterator<long , ClipperItem*> iter(clipperItemsList);
            while (iter.hasNext()) {
                iter.next();

                QListWidgetItem* lwi = iter.value()->getListItem();
                listWidget->takeItem(listWidget->row(lwi));

                delete iter.value();
            }

            clipperItemsList.clear();
        }

        bottomLabel->setText(QString("0 tập tin. Ổ cứng còn trống ") + QString::number(storage.bytesAvailable()/1000/1000/1000) + " GB");
    }

}


void P_DownloadSelector::update() {}

void P_DownloadSelector::show() {
    if (control()->zone->isVisible()) {
        control()->zone->hide();
        control()->zone->lower();
    } else {
        control()->zone->show();
        control()->zone->raise();
    }
}

void P_DownloadSelector::hide() { control()->zone->hide(); }

QWidget *P_DownloadSelector::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}

bool P_DownloadSelector::eventFilter(QObject *watched, QEvent *event) {
    QWidget *zone = qobject_cast<QWidget *>(watched);
    if (zone == this->control()->zone) {
        switch (event->type()) {
        case QEvent::HoverEnter:
            hoverEnter(static_cast<QHoverEvent *>(event));
            return false;
            break;
        case QEvent::HoverLeave:
            hoverLeave(static_cast<QHoverEvent *>(event));
            return false;
            break;
        case QEvent::HoverMove:
            hoverMove(static_cast<QHoverEvent *>(event));
            return false;
            break;
        default:
            break;
        }
    }

    //  return QWidget::eventFilter(watched, event);
    return false;
}

void P_DownloadSelector::hoverEnter(QHoverEvent *) {}

void P_DownloadSelector::hoverLeave(QHoverEvent *) {
    control()->newUserAction(Message.DOWNLOAD_SELECTOR_HIDE_ALL, Q_NULLPTR);
    control()->zone->hide();
}

void P_DownloadSelector::hoverMove(QHoverEvent *) {}

void P_DownloadSelector::sizeTopControlBar(QVariant *dataStruct) {
    dataSizeTop = dataStruct->value<SizeTopControlBar>();
    int widthMove =
            dataSizeTop.widthTopBar - 5 - 40 - 300 - dataSizeTop.widthProfileSetting;
    // init gui object
    this->zone->move(widthMove, 40);

    this->zone->setFixedSize(300, 500);
}
