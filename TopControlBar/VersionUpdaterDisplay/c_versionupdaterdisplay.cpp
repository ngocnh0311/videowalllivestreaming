#include "c_versionupdaterdisplay.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_VersionUpdaterDisplay::C_VersionUpdaterDisplay(Control* ctrl, QWidget* zone) : Control(ctrl) {
    QVariant* dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext*>();

    this->abst = new A_VersionUpdaterDisplay(this);
    this->pres = new P_VersionUpdaterDisplay(this, zone);
    this->zone = zone;

    // create others controls
    // cXXX = new C_XXX();
}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VersionUpdaterDisplay::newUserAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.VERSION_UPDATER_SHOW:
        getParent()->newAction(message, attachment);
        break;
    case Message.VERSION_UPDATER_HIDE:
        getParent()->newAction(message, attachment);
        break;
    case Message.VERSION_UPDATER_HIDE_ALL:
        getParent()->newAction(message, attachment);
        break;
    default:
        qDebug() << "ERROR : General User action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VersionUpdaterDisplay::newSystemAction(int message, QVariant* attachment) {
    switch (message) {
    case 1: break;
    default:
        qDebug() << "ERROR : General System action in " + getClass() +
                    " non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_VersionUpdaterDisplay::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.VERSION_NEWEST_VERSION_UPDATER_HIDE_ALL:{
        presentation()->hide();
    }break;

    case Message.AUTO_CLICK_UPDATE_VERSION:{
        presentation()->updateVersionButtonClicked();
    }break;
    case Message.UPDATE_STATE_BUTTON_ALL_TOP_BAR:{
        presentation()->hideButtonUpdateVersion();
    }break;

    case Message.UPDATE_STATE_BUTTON_TOP_BAR:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.VERSION_UPDATER_DISPLAY_AUTO_SHOW:{
        presentation()->needShowContinue();
    }break;

    case Message.SHOW_BUTTON_UPDATE_VERSION_NEWEST:{
        presentation()->changeDescriptionVersionNewest();
        presentation()->showButtonUpdateVersion();
    }break;


    case Message.TOP_BAR_OVER_LAY_SHOW_WHEN_VERSION_UPDATE_SHOW:
    case Message.TOP_BAR_OVER_LAY_CAN_HIDE:
    case Message.TOP_BAR_OVER_LAY_CAN_NOT_HIDE:{
        getParent()->newAction(message, attachment);
    }break;

    case Message.VERSION_UPDATER_SHOW:
    {
        presentation()->show();
    }break;

    case Message.VERSION_UPDATER_HIDE:
    {
        presentation()->hide();
    }break;
    case Message.GET_SIZE_TOP_CONTROL_BAR:{

        presentation()->sizeTopControlBar(attachment);
    }break;

    default:
        qDebug() << "ERROR : General Internal pac action in" + getClass() +
                    "non-catched :" + Message.toString(message);
    }
}
