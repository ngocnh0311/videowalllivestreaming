#include "p_versionupdaterdisplay.h"

/**
     * Generic method to override for updating the presention.
     **/

P_VersionUpdaterDisplay::P_VersionUpdaterDisplay(Control *ctrl, QWidget *zone)
    : Presentation(ctrl) {

    // init gui object
    this->zone = zone;
    this->zone->hide();
    this->zoneLayout = new QVBoxLayout();
    this->zoneLayout->setContentsMargins(marginContain,marginContain,marginContain,marginContain);
    this->zoneLayout->setSpacing(10);
    this->zoneLayout->setAlignment(Qt::AlignTop);
    this->zone->setLayout(this->zoneLayout);
    this->zone->setStyleSheet("background-color: #404244; color: #ddd; border:0px solid #8a8a92;border-radius :0px;");

    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect(this->zone);
    effect->setBlurRadius(80);
    effect->setColor(QColor("#000000"));
    effect->setOffset(0,0);
    this->zone->setGraphicsEffect(effect);
    this->zone->setFixedSize(QSize(600,400));
    this->zone->setAttribute(Qt::WA_Hover, true);
    this->zone->installEventFilter(this);
    this->zone->setMouseTracking(true);
    this->zone->setAttribute(Qt::WA_Hover);
    //    //init title
    //    this->topWidget = new QWidget(this->zone);
    //    this->topWidget->setFixedSize(this->zone->width(), heightTopForm);

    //    this->topWidget->setStyleSheet(
    //                "background: QLinearGradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 "
    //                "#eff0f5, stop: 1 #dedede); border: 0px solid #8a8a92; "
    //                "border-top-left-radius: 5px; border-top-right-radius: 5px; "
    //                "border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;");
    //    this->topLayout = new QVBoxLayout();
    //    this->topLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    //    this->topLayout->setSpacing(5);
    //    this->topLayout->setContentsMargins(0, 0, 0, 0);

    //    this->topWidget->setLayout(this->topLayout);
    //    this->zoneLayout->addWidget(this->topWidget);
    //    initTitleZone();
    //    //containwidget chứa toàn bộ widget ở phần dưới
    //    containBottomWidget = new QWidget(this->zone);
    //    containBottomWidget->move(0, this->heightTopForm);
    //    containBottomWidget->setFixedSize(this->zone->width() , this->zone->height() - heightTopForm);
    //    containBottomWidget->setStyleSheet("background:#FFFFFF;color:black");

    //    QVBoxLayout *containBottomLayout = new QVBoxLayout();
    //    containBottomLayout->setAlignment(Qt::AlignCenter);
    //    containBottomWidget->setLayout(containBottomLayout);
    //    containBottomLayout->setSpacing(0);
    //    containBottomLayout->setMargin(0);
    //    containBottomLayout->setAlignment(Qt::AlignCenter);
    //    this->zoneLayout->addWidget(containBottomWidget); // add contain


    //top widget
    //    topTitleWidget = new QWidget(this->zone);
    //    topTitleWidget->move(0,0);
    //    topTitleWidget->setFixedSize(this->zone->width() , this->heightTop);
    //    topTitleLayout = new QVBoxLayout();
    //    topTitleLayout->setAlignment(Qt::AlignCenter);
    //    topTitleLayout->setSpacing(0);
    //    topTitleLayout->setMargin(0);
    //    topTitleWidget->setLayout(topTitleLayout);
    //top title

    //    QLabel *descriptionTitleLable = new QLabel(this->topTitleWidget);
    //    descriptionTitleLable->setText("Mô tả bản cập nhật");
    //    topTitleLayout->addWidget(descriptionTitleLable);

    //contain widget
    containWidget = new QWidget(this->zone);
    //    containWidget->setStyleSheet("background:green");
    containWidget->setFixedSize(this->zone->width() - (2*marginContain) ,this->zone->height() - this->heightBottom - (2*marginContain));
    containLayout = new QVBoxLayout();
    containLayout->setAlignment(Qt::AlignCenter);
    containLayout->setSpacing(0);
    containLayout->setMargin(0);
    containWidget->setLayout(containLayout);

    descriptionUpdateTextEdit = new QTextEdit(this->containWidget);
    descriptionUpdateTextEdit->setFixedSize(containWidget->size());
    descriptionUpdateTextEdit->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
//    descriptionUpdateTextEdit->setEnabled(false);
    QString descriptionUpdateNew = "";
    descriptionUpdateTextEdit->setHtml(descriptionUpdateNew);
    containLayout->addWidget(descriptionUpdateTextEdit);

    //bottom widget
    bottomWidget = new QWidget(this->zone);
    //    bottomWidget->setStyleSheet("background:red;color:white; border-radius:none");
    bottomWidget->setFixedSize(this->zone->width() - (2*marginContain) , this->heightBottom);
    bottomLayout = new QHBoxLayout();
    bottomLayout->setAlignment(Qt::AlignCenter);
    bottomLayout->setSpacing(25);
    bottomLayout->setMargin(0);
    bottomWidget->setLayout(bottomLayout);

    updateVersionButton =  new QPushButton(bottomWidget);
    updateVersionButton->setStyleSheet("QPushButton {background:#cf202e;border-radius:5px;color:#ddd}"
                                       "QPushButton:hover {background-color: #ef2636;"
                                       "color: #ddd}; border: none;");
    QSettings settings;
    int stateUpdateVersion = settings.value("state_update_version", -1).toInt();
    if(stateUpdateVersion == versionUpdateStates.APP_VERSION_UPDATING ||stateUpdateVersion == versionUpdateStates.APP_VERSION_UPDATED){
        updateVersionButton->setFixedSize(0, 0);
    }else{
        updateVersionButton->setFixedSize(150, 30);
    }

    updateVersionButton->setText(iconButtonTops.download +" Đồng Ý Cập Nhật");
    connect(updateVersionButton , &QPushButton::clicked , this, &P_VersionUpdaterDisplay::updateVersionButtonClicked);

    //    //update late
    //    QPushButton *updateVersionLateButton =  new QPushButton(bottomWidget);
    //    updateVersionLateButton->setStyleSheet("background-color:#DEDEDE; color:gray;border-radius :5px;");
    //    updateVersionLateButton->setFixedSize(150, 30);
    //    updateVersionLateButton->setText(iconButtonTops.close + " Đóng");
    //    connect(updateVersionLateButton , &QPushButton::clicked , this, &P_VersionUpdaterDisplay::onCloseButtonClicked);

    //    bottomLayout->addWidget(updateVersionLateButton);
    bottomLayout->addWidget(updateVersionButton);



    //bottom  left widget
    //    bottomSaveUpdateWidget = new QWidget(this->zone);
    //    bottomSaveUpdateWidget->setFixedSize(this->zone->width() , this->heightCheckSaveBottom);

    //    bottomSaveUpdateLayout = new QVBoxLayout();
    //    bottomSaveUpdateLayout->setAlignment(Qt::AlignCenter);
    //    bottomSaveUpdateLayout->setMargin(0);
    //    bottomSaveUpdateLayout->setSpacing(0);
    //    bottomSaveUpdateWidget->setLayout(bottomSaveUpdateLayout);

    //    gridLayoutAutoUpdateWidget = new QWidget(bottomSaveUpdateWidget);
    //    gridLayoutAutoUpdateWidget->setFixedSize(400 , this->heightCheckSaveBottom);
    //    bottomSaveUpdateLayout->addWidget(gridLayoutAutoUpdateWidget);


    //    gridLayoutAutoUpdate = new QGridLayout(this->gridLayoutAutoUpdateWidget);
    //    gridLayoutAutoUpdateWidget->setLayout(gridLayoutAutoUpdate);
    //    gridLayoutAutoUpdate->setMargin(0);
    //    gridLayoutAutoUpdate->setSpacing(0);
    //    gridLayoutAutoUpdate->setAlignment(Qt::AlignCenter);



    //add widget
    //    containBottomLayout->addWidget(topTitleWidget);
    this->zoneLayout->addWidget(containWidget);
    this->zoneLayout->addWidget(bottomWidget);
    //    this->zoneLayout->addWidget(bottomSaveUpdateWidget);

}

QString P_VersionUpdaterDisplay::createDescriptionVersionNewest(){
    QString descriptioneReturn;
    VersionInfo *versionInfo = control()->appContext->getVersionInfoNewest();
    if(versionInfo != Q_NULLPTR){
        VersionVideowall *versionVideowallNewest = versionInfo->getVersionVideowallNewest();
        if(versionVideowallNewest != Q_NULLPTR){
            descriptioneReturn.append("Những điểm mới trong phiên bản  - " + QString::number(versionVideowallNewest->getVersionX()) + "." + QString::number(versionVideowallNewest->getVersionY()) + "." + QString::number(versionVideowallNewest->getVersionZ()) + " : <br/> ") ;
            int sizeVersion = versionInfo->getListVersionVideowallNew().size();
            for (int index = (sizeVersion - 1); index >= 0; --index) {
                VersionVideowall *versionVideowall = versionInfo->getListVersionVideowallNew().at(index);
                QString desciption = versionVideowall->getNewMessage();
                descriptioneReturn.append(desciption);
                descriptioneReturn.append("<br/>");
            }
        }
    }
    return descriptioneReturn;
}

void P_VersionUpdaterDisplay::changeDescriptionVersionNewest(){
    QSettings settings;
    int stateUpdate = settings.value("state_update_version", -1).toInt();
    if (stateUpdate != versionUpdateStates.APP_VERSION_UPDATED) {
        QString descriptionUpdateNew = createDescriptionVersionNewest();
        settings.setValue("description_new_version", descriptionUpdateNew);
        descriptionUpdateTextEdit->setHtml(descriptionUpdateNew);
    }else{
        //if updated
        QString  descriptionUpdateSaved = settings.value("description_new_version").toString();
        descriptionUpdateTextEdit->setHtml(descriptionUpdateSaved);
    }

}



void P_VersionUpdaterDisplay::hideButtonUpdateVersion(){
    updateVersionButton->setFixedSize(0, 0); //hide button update version
}

void P_VersionUpdaterDisplay::showButtonUpdateVersion(){
    QSettings settings;
    int stateInt = settings.value("state_update_version", -1).toInt();
    if(stateInt == versionUpdateStates.APP_HAVE_VERSION_NEWEST){
        updateVersionButton->setFixedSize(150, 30);
    }
}


void P_VersionUpdaterDisplay::updateVersionButtonClicked(){
    qDebug() << Q_FUNC_INFO;
    //    "device_id": "00:1f:c6:9c:6c:01",
    //    "module_id": 216,
    //    "version":"2.15.10"
    QString uri = "/cms_api/servers/updateModuleVersion";
    QString userToken = this->control()->appContext->getWorkingUser()->getToken();
    if(!userToken.isEmpty()){
        QJsonObject json;
        QSettings settings;
        QString device = NetworkUtils::instance().getMacAddress();
        QString moduleId = "216";
        QString moduleIdConfig = AppProfile::getAppProfile()->getAppConfig()->getVideowallModuleId();
        if(!moduleIdConfig.isEmpty()){
            moduleId = moduleIdConfig;
        }
        QString version = settings.value("app_version").toString();
        json.insert("device_id", device);
        json.insert("module_id", moduleId);
        json.insert("version", version);
        QMap<QString, QString> params;
        params["token"] = userToken;

        std::function<void(QJsonObject)>  onFetchSuccess = [this](QJsonObject jsonObject){
            QSettings settings;

            if(!jsonObject.isEmpty()){
                int code = -1;
                QString message;
                if(jsonObject.contains("code")  == true){
                    code = jsonObject.take("code").toInt();
                    message = jsonObject.take("message").toString();
                    if(code == 0){
                        settings.setValue("state_update_version", versionUpdateStates.APP_VERSION_UPDATING);

                        double timestampCurrent = QDateTime::currentDateTime().toSecsSinceEpoch();
                        QString timestampCurrentToString = QString::number(timestampCurrent, 'g', 19);
                        settings.setValue("timestamp_start_update", timestampCurrentToString);
                        control()->newAction(Message.UPDATE_STATE_BUTTON_TOP_BAR, Q_NULLPTR);
                        qDebug() << "Code" << code << "Message" << message;
                    }else if(message == "This server has already had lastest module version"){
                        if(this->control()->appContext->getIsNeedUpdate()){
                            //if need update
                            settings.setValue("state_update_version", versionUpdateStates.APP_VERSION_UPDATING);
                        }else{
                            settings.setValue("state_update_version", versionUpdateStates.APP_VERSION_UPDATED);

                            double timestampCurrent = QDateTime::currentDateTime().toSecsSinceEpoch();
                            QString timestampCurrentToString = QString::number(timestampCurrent, 'g', 19);
                            settings.setValue("timestamp_start_update", timestampCurrentToString);
                        }
                        control()->newAction(Message.UPDATE_STATE_BUTTON_TOP_BAR, Q_NULLPTR);

                        qDebug() << Q_FUNC_INFO <<message;
                    }
                }else{
                    qDebug() << Q_FUNC_INFO << "ERROR" << QString::number(code);
                }
            }
        };
        std::function<void(QString)>  onFetchFailure = [this](QString message){
            qDebug() << Q_FUNC_INFO << "ERROR" << message;
        };

        NetworkUtils::instance().putRequest(10000, uri, json , params,onFetchSuccess,onFetchFailure);
    }
}

void P_VersionUpdaterDisplay::updateVersionLateButtonClicked(){
    qDebug() << Q_FUNC_INFO;

}
void P_VersionUpdaterDisplay::initTitleZone() {
    titleWidget = new QWidget(this->topWidget);
    titleWidget->setFixedSize(this->topWidget->size());
    titleWidget->setStyleSheet("background-color: #00000000");


    QHBoxLayout *titleLayout = new QHBoxLayout();
    titleLayout->setAlignment(Qt::AlignLeft);
    titleLayout->setSpacing(10);
    titleLayout->setContentsMargins(8, 10, 10, 8);
    titleWidget->setLayout(titleLayout);

    this->closeButton =
            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
                         "#aaff3b30", "#ff3b30", "#ff3b30", "");
    QPixmap pix(":/images/res/icon_tab_close.png");
    QIcon iconClose(pix);
    this->closeButton->setIcon(iconClose);
    this->closeButton->setIconSize(QSize(10, 10));
    connect(this->closeButton, &QPushButton::clicked, this,
            &P_VersionUpdaterDisplay::onCloseButtonClicked);

    //    this->minimizeButton =
    //            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
    //                         "#aaffcc00", "#ffcc00", "#ffcc00", "");
    //    this->minimizeButton->setIcon(QIcon(":/images/res/icon_tab_minimize.png"));
    //    this->minimizeButton->setIconSize(QSize(10, 10));
    //    connect(this->minimizeButton, &QPushButton::clicked, this,
    //            &P_VersionUpdaterDisplay::onMinimizeButtonClicked);

    //    this->maximizeButton =
    //            createButton(titleWidget, QSize(16, 16), QRect(8, 8, 8, 8), 0,
    //                         "#aa4cd964", "#4cd964", "#4cd964", "");
    //    this->maximizeButton->setIcon(QIcon(":/images/res/icon_tab_maximize.png"));
    //    this->maximizeButton->setIconSize(QSize(10, 10));
    //    connect(this->maximizeButton, &QPushButton::clicked, this,
    //            &P_VersionUpdaterDisplay::onMaximizeButtonClicked);

    this->titleLabel = new QLabel(titleWidget);
    this->titleLabel->setMinimumWidth(this->topWidget->width() - 110);
    this->titleLabel->setAlignment(Qt::AlignCenter);
    this->titleLabel->setFont(Resources::instance().getLargeBoldButtonFont());
    this->titleLabel->setStyleSheet(
                "background-color: #00000000; color: #1e1e1e");
    this->titleLabel->setText("VideowallPC - Phiên bản cập nhật");

    titleLayout->addWidget(this->closeButton);
    //    titleLayout->addWidget(this->minimizeButton);
    //    titleLayout->addWidget(this->maximizeButton);
    titleLayout->addWidget(this->titleLabel);

    this->topLayout->addWidget(titleWidget);
}
void P_VersionUpdaterDisplay::onCloseButtonClicked(){
    control()->zone->hide();
}

void P_VersionUpdaterDisplay::onMinimizeButtonClicked(){

}

void P_VersionUpdaterDisplay::onMaximizeButtonClicked(){

}
void P_VersionUpdaterDisplay::update() {}

void P_VersionUpdaterDisplay::show() {
    if (control()->zone->isVisible()) {
        control()->zone->hide();
        control()->newAction(Message.TOP_BAR_OVER_LAY_CAN_HIDE, Q_NULLPTR);
    } else {
        control()->zone->show();
        control()->zone->raise();
        control()->newAction(Message.TOP_BAR_OVER_LAY_CAN_NOT_HIDE, Q_NULLPTR);
    }
}

void P_VersionUpdaterDisplay::needShowContinue() {
    if (!control()->zone->isVisible()) {
        QSettings settings;
        QString isAutoUpdate = settings.value("auto_update_version").toString();
        int stateUpdate = settings.value("state_update_version", -1).toInt();
        int countShowNeedUpdateNewestVersion = settings.value("number_display_form_update_new_version", 0).toInt();
        if(isAutoUpdate == "OFF" && stateUpdate != versionUpdateStates.APP_VERSION_UPDATING && stateUpdate != versionUpdateStates.APP_VERSION_UPDATED && countShowNeedUpdateNewestVersion < 6){
            countShowNeedUpdateNewestVersion ++;
            settings.setValue("number_display_form_update_new_version", countShowNeedUpdateNewestVersion);
            control()->zone->show();
            control()->zone->raise();
            control()->newAction(Message.TOP_BAR_OVER_LAY_SHOW_WHEN_VERSION_UPDATE_SHOW, Q_NULLPTR);
            //after 15s hide
            QTimer::singleShot(15000, this , [this]{
                control()->zone->hide();
                control()->newAction(Message.TOP_BAR_OVER_LAY_CAN_HIDE, Q_NULLPTR);
            });
        }

        if(stateUpdate == versionUpdateStates.APP_VERSION_UPDATED && countShowUpdatedVersion < 3){
            QSettings settings;
            double timestampCurrent = QDateTime::currentDateTime().toSecsSinceEpoch();
            QString timestampCurrentToString = QString::number(timestampCurrent, 'g', 19);
            long timestampCurrentLong = timestampCurrentToString.toLong();
            long timestampCurrentSavedLong = 0;
            QString timestampCurrentSavedToString = settings.value("timestamp_start_update", "").toString();
            if(!timestampCurrentSavedToString.isEmpty()){
                timestampCurrentSavedLong = timestampCurrentSavedToString.toLong();
            }
            long timeMinus = timestampCurrentLong - timestampCurrentSavedLong;
            qDebug() << Q_FUNC_INFO << "time show form update saved" << timeMinus;
            if(timeMinus <= 3600){
                countShowUpdatedVersion ++;
                control()->zone->show();
                control()->zone->raise();
                control()->newAction(Message.TOP_BAR_OVER_LAY_SHOW_WHEN_VERSION_UPDATE_SHOW, Q_NULLPTR);
                //after 15s hide
                QTimer::singleShot(15000, this , [this]{
                    control()->zone->hide();
                    control()->newAction(Message.TOP_BAR_OVER_LAY_CAN_HIDE, Q_NULLPTR);
                });
            }
        }
    }
}

void P_VersionUpdaterDisplay::sizeTopControlBar(QVariant *dataStruct) {
    dataSizeTop = dataStruct->value<SizeTopControlBar>();
    int widthMove = dataSizeTop.widthTopBar - dataSizeTop.widthProfileSetting - dataSizeTop.widthFullScreen -  dataSizeTop.widthDownloadSelector - 600 - 85 ;
    this->zone->move(widthMove, appSize.topHeight);
}

void P_VersionUpdaterDisplay::hide() {
    control()->zone->hide();
}


QWidget *P_VersionUpdaterDisplay::getZone(int zoneId) {
    switch (zoneId) {
    case 1:
        return Q_NULLPTR;
    default:
        return Q_NULLPTR;
    }
}

bool P_VersionUpdaterDisplay::eventFilter(QObject *watched, QEvent *event) {
    QWidget *widget = qobject_cast<QWidget *>(watched);
    if (widget == this->zone) {
        switch (event->type()) {
        case QEvent::HoverEnter:
            hoverEnter(static_cast<QHoverEvent *>(event));
            return true;
            break;
        case QEvent::HoverLeave:
            hoverLeave(static_cast<QHoverEvent *>(event));
            return true;
            break;
        case QEvent::HoverMove:
            hoverMove(static_cast<QHoverEvent *>(event));
            return true;
            break;
        default:
            break;
        }
    }
    return false;
}

void P_VersionUpdaterDisplay::hoverEnter(QHoverEvent *) {}

void P_VersionUpdaterDisplay::hoverLeave(QHoverEvent *) {
    control()->newUserAction(Message.VERSION_UPDATER_HIDE_ALL, Q_NULLPTR);
    control()->zone->hide();
}

void P_VersionUpdaterDisplay::hoverMove(QHoverEvent *) {}

