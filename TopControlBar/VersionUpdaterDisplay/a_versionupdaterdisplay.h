#ifndef A_VersionUpdaterDisplay_H
#define A_VersionUpdaterDisplay_H

#include <QObject>
#include <QTimer>
#include "PacModel/control.h"
#include "c_versionupdaterdisplay.h"
class C_VersionUpdaterDisplay;
class A_VersionUpdaterDisplay : public Abstraction {
    Q_OBJECT
    // A ref on the control facet
private:

public:
    A_VersionUpdaterDisplay(Control *ctrl);
    C_VersionUpdaterDisplay *control() { return (C_VersionUpdaterDisplay *)this->ctrl; }
    void changeControl(Control *ctrl);

};

#endif  // A_VersionUpdaterDisplay_H
