#include "c_cam9rtcplayeroverlay.h"

/**
     * Contructor. Register the father in the pac hierarchy.
     * @param ctrl The reference on the father. Generally the pac agent which
     * create this agent.
     **/
C_Cam9RTCPlayerOverlay::C_Cam9RTCPlayerOverlay(Control *ctrl, QWidget *zone)
    : Control(ctrl) {
    QVariant *dataStruct = new QVariant();
    getParent()->newAction(Message.APP_CONTEXT_GET, dataStruct);
    this->appContext = dataStruct->value<AppContext *>();

    this->zone = zone;

    this->pres = new P_Cam9RTCPlayerOverlay(this, this->zone);

}

/**
     * Method to receive a message from the Presentation Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_Cam9RTCPlayerOverlay::newUserAction(int message, QVariant *attachment) {
    switch (message) {
    case Message.VIDEO_WALL_PLAYER_CHANGE_NETWORK_STREAM:{
        this->getParent()->newAction(message, attachment);
    }break;

    case Message.PLAYER_BEGIN_HIDE_FULLSCREEN_HAND_CLICK_OF_REMOTE_CONTROL:
    case Message.PLAYER_BEGIN_SHOW_FULLSCREEN_HAND_CLICK_OF_REMOTE_CONTROL:{
        this->getParent()->newAction(message, attachment);
    }break;

    case Message.PLAYER_RECORD_NORMAL: {
        this->getParent()->newAction(message, attachment);
    } break;

    case Message.PLAYER_RECORD_QUICK: {
        this->getParent()->newAction(message, attachment);
    } break;

    case Message.PLAYER_TAKE_SCREENSHOT: {
        this->getParent()->newAction(message, attachment);
    } break;

    case Message.PLAYER_PLAY_LIVE_HD:
    case Message.PLAYER_PLAY_LIVE_SD: {
        this->getParent()->newAction(message, attachment);
    } break;

    case Message.PLAYER_PLAY_VOD_SD: {
        this->getParent()->newAction(message, attachment);
    } break;

    case Message.PLAYER_PLAY_VOD_HD: {
        this->getParent()->newAction(message, attachment);
    } break;

    case Message.APP_PLAY_BACK_PlAYER_SHOW_FULL_SCREEN: {
        getParent()->newAction(message, attachment);
    } break;

    case Message.APP_PLAY_BACK_PlAYER_HIDE_FULL_SCREEN: {
        getParent()->newAction(message, attachment);
    } break;

    case Message.WHEEL_EVENT_ZOOM_VIDEO: {
        getParent()->newAction(message, attachment);
    } break;

    case Message.EVENT_MOVE_ZOOM_VIDEO: {
        getParent()->newAction(message, attachment);
    } break;

    default:
        qDebug() << "ERROR : General User action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from the Astraction Facet.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_Cam9RTCPlayerOverlay::newSystemAction(int message, QVariant *attachment) {
    Q_UNUSED(attachment)
    switch (message) {
    case Message.PLAYER_PLAY_HD_CLICK: {
        getParent()->newAction(message, attachment);
    } break;

    case Message.PLAYER_PLAY_SD_CLICK: {
        getParent()->newAction(message, attachment);
    } break;

    default:
        qDebug() << "ERROR : General System action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void C_Cam9RTCPlayerOverlay::newAction(int message, QVariant *attachment) {
    switch (message) {
    case Message.PLAYER_UPDATE_INFO_USE_FREE_SPACE:{
        CamItem *camItem = attachment->value<CamItem *>();
        presentation()->updateInfoUseFreeSpace(camItem);
    }break;

    case Message.APP_CONTEXT_GET:
        attachment->setValue(this->appContext);
        break;

    case Message.RESET_SHOW_FULL_SCREEN:{
        presentation()->resetShowFullScreen();
    }break;
    case Message.UPDATE_MODE_RENDER_DEBUG:{
        presentation()->onHideIndicatorlastFrame();
    }break;

    case Message.PLAYER_BEGIN_HIDE_FULLSCREEN_AUTO:
    case Message.PLAYER_BEGIN_SHOW_FULLSCREEN_AUTO:{
        getParent()->newAction(message, attachment);

    }break;

    case Message.DEVICE_ON_HIDE_FULL_SCREEN_CLICK_AUTO:{
        this->presentation()->onHideFullscreenClickedAuto();
    }break;
    case Message.DEVICE_ON_SHOW_FULL_SCREEN_CLICK_AUTO:{
        this->presentation()->onShowFullscreenClickedAuto();
    }break;

    case Message.PLAYER_START_LOADING: {
        presentation()->showLoader();
    } break;

    case Message.PLAYER_STOP_LOADING: {
        presentation()->hideLoader();
    } break;

    case Message.PLAYER_NO_SOURCE:
    case Message.PLAYER_LOADING: {
        presentation()->playerLoading();
    } break;

    case Message.PLAYER_PLAYING: {
        presentation()->playerPlaying();
    } break;

    case Message.PLAYER_STOPED: {
        presentation()->playerStopped();
    } break;

    case Message.PLAYER_SOURCE_CLEAR: {
        //        presentation()->hideAll();
        presentation()->clearStateLive();
    } break;

    case Message.PLAYER_NEW_LIVE_SOURCE_SET: {
        CamItem *camItem = attachment->value<CamItem *>();
        presentation()->updateInfo(camItem);
    } break;

    case Message.VIDEO_WALL_PLAYER_SWITCH_TO_SD_WHEN_EXIT_FULL_SCREEN:{
        CamItem *camItem = attachment->value<CamItem *>();
        presentation()->updateInfoSDWhenExitFullScreen(camItem);
    }break;

    case Message.PLAYER_UPDATE_INFO: {
        CamItem *camItem = attachment->value<CamItem *>();
        presentation()->updateInfo(camItem);
    } break;

    case Message.PLAYER_UPDATE_INFO_WHEN_SHOW_FULL_SCREEN:
    case Message.PLAYER_UPDATE_INFO_WHEN_HIDE_FULL_SCREEN: {
        CamItem *camItem = attachment->value<CamItem *>();
        presentation()->updateInfo(camItem);
    } break;


    case Message.PLAYER_END_SHOW_FULLSCREEN: {
        presentation()->playerEndShowFullScreen();
    } break;

    case Message.PLAYER_END_HIDE_FULLSCREEN: {
        presentation()->playerEndHideFullScreen();
    } break;

    case Message.PLAYER_PLAY_SD: {
        presentation()->displaySelectedSd();
    } break;

    case Message.PLAYER_PLAY_HD: {
        presentation()->displaySelectedHd();
    } break;

    case Message.PLAYER_BEGIN_SHOW_FULLSCREEN: {
        getParent()->newAction(message, attachment);
    } break;

    case Message.PLAYER_EXIT_POP_OUT_MODE: {
        presentation()->exitPopOutMode();
    } break;

    case Message.PLAYER_BEGIN_HIDE_FULLSCREEN: {
        getParent()->newAction(message, attachment);
    } break;

    case Message.APP_PLAY_BACK_START_ANIMAITON_RECORD_NORMAL: {
        presentation()->startAnimationNormalButton();
    } break;

    case Message.APP_PLAY_BACK_STOP_ANIMAITON_RECORD_NORMAL: {
        presentation()->stopAnimationNormalButton();

    } break;

    default:
        qDebug() << "ERROR : General Internal pac action in" << getClass()
                 << "non-catched :" + Message.toString(message);
    }
}


void C_Cam9RTCPlayerOverlay::onShowIndicatorLastFrame(){
    presentation()->onShowIndicatorlastFrame();
}

void C_Cam9RTCPlayerOverlay::onHideIndicatorLastFrame(){
    presentation()->onHideIndicatorlastFrame();
}


