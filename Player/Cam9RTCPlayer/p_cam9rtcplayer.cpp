#include "p_cam9rtcplayer.h"
#include "Common/LayoutSet.h"

P_Cam9RTCPlayer::P_Cam9RTCPlayer(Control *ctrl, QWidget *zone) : Presentation(ctrl) {
    this->zone = zone;
    this->zone->installEventFilter(this);

    //initUI
    mainLayout = new QStackedLayout();
    mainLayout->setMargin(0);
    mainLayout->setAlignment(Qt::AlignCenter);
    this->zone->setLayout(mainLayout);

    // 1. WALL LAYER
    this->wallZone = new QWidget(this->zone);

    QVBoxLayout *wallZoneLayout = new QVBoxLayout();
    wallZoneLayout->setAlignment(Qt::AlignCenter);
    this->wallZone->setLayout(wallZoneLayout);
    this->wallZone->setStyleSheet("background-color: #111");

    this->wallZone->move(0, 0);
    this->wallZone->resize(this->zone->size());
    //    wallZone->hide();

    QLabel *noCameraMessage = new QLabel(this->wallZone);
    noCameraMessage->setAlignment(Qt::AlignCenter);
    noCameraMessage->setStyleSheet("background-color: #00000000");
    noCameraMessage->setMinimumSize(50, 50);
    noCameraMessage->setText("NO CAMERA");
    noCameraMessage->setFont(Resources::instance().getMediumBoldButtonFont());
    wallZoneLayout->addWidget(noCameraMessage);


    // 2. SOURCE WARNING ZONE
    this->sourceWarnigZone = new QWidget(this->zone);
    QVBoxLayout *sourceWarnigLayout = new QVBoxLayout();
    sourceWarnigLayout->setAlignment(Qt::AlignCenter);
    this->sourceWarnigZone->setLayout(sourceWarnigLayout);
    this->sourceWarnigZone->setStyleSheet("background-color: #111");
    this->sourceWarnigZone->move(0,0);
    this->sourceWarnigZone->resize(this->zone->size());


    QLabel *noSourceMessage = new QLabel(this->sourceWarnigZone);
    noSourceMessage->setAlignment(Qt::AlignCenter);
    noSourceMessage->setStyleSheet("background-color: #00000000");
    noSourceMessage->setMinimumSize(50, 50);
    noSourceMessage->setText("NO SOURCE");
    noSourceMessage->setFont(Resources::instance().getMediumBoldButtonFont());
    sourceWarnigLayout->addWidget(noSourceMessage);

    // 3. PLAYER LAYER
    playerZone = new QWidget(this->zone);

    //    QVBoxLayout *playerZoneLayout = new QVBoxLayout();
    //    playerZoneLayout->setAlignment(Qt::AlignCenter);
    //this->playerZone->setLayout(playerZoneLayout);
    this->playerZone->setStyleSheet("background-color: #000000");
    this->playerZone->move(0, 0);
    this->playerZone->resize(this->zone->size().width(), this->zone->height());
    //    this->playerZone->show();

    // 4. RIDEAUX LAYER
    this->blackZone = new QWidget(this->zone);

    QVBoxLayout *blackZoneLayout = new QVBoxLayout();
    blackZoneLayout->setAlignment(Qt::AlignCenter);
    this->blackZone->setLayout(blackZoneLayout);
    this->blackZone->setStyleSheet("background-color: #111");

    //this->blackZone->setStyleSheet("background-color: #008000");
    //this->blackZone->setStyleSheet("background-color: black");
    this->blackZone->move(0, 0);
    this->blackZone->resize(this->zone->size());
    //    blackZone->hide();

    // 5. OVERLAY LAYER
    this->overlayZone = new QWidget(this->zone);
    this->overlayZone->move(0, 0);
    this->overlayZone->resize(this->zone->size());

    mainLayout->addWidget(wallZone);
    mainLayout->addWidget(blackZone);
    mainLayout->addWidget(playerZone);
    mainLayout->setCurrentWidget(playerZone);
}

void P_Cam9RTCPlayer::changeZone(QWidget *zoneNew){
    this->zone = zoneNew;
    this->zone->setLayout(mainLayout);
    this->sourceWarnigZone->setParent(this->zone);
    this->wallZone->setParent(this->zone);
    this->blackZone->setParent(this->zone);
    this->playerZone->setParent(this->zone);
    this->overlayZone->setParent(this->zone);
}

QWidget *P_Cam9RTCPlayer::getZone(int zoneId) {
    switch (zoneId) {

    case 0:
        return this->zone;

    case 1:
        return this->overlayZone;

    case 2:
        return this->blackZone;

    case 3: return this->playerZone;

    default:
        return Q_NULLPTR;
    }
}

void P_Cam9RTCPlayer::clearSource() {
    QString playerName = "Player " + this->control()->getDisplayName();
    qDebug() << playerName << ":::clearSource";
    mainLayout->setCurrentWidget(wallZone);
    overlayZone->hide();
}

void P_Cam9RTCPlayer::showLoadingZone(){
    //    qDebug() << "showLoadingZone";
    sourceWarnigZone->hide();
    mainLayout->setCurrentWidget(blackZone);
    overlayZone->show();
    overlayZone->raise();
}

void P_Cam9RTCPlayer::onShowLoadingSign() {
    sourceWarnigZone->hide();
    QString playerName = "Player " + this->control()->getDisplayName();
    control()->newUserAction(Message.PLAYER_LOADING, Q_NULLPTR);
    mainLayout->setCurrentWidget(blackZone);
    overlayZone->show();
    overlayZone->raise();
}

void P_Cam9RTCPlayer::onShowPlayerLayer() {
    sourceWarnigZone->hide();
    QString playerName = "Player " + this->control()->getDisplayName();
    control()->newAction(Message.PLAYER_PLAYING, Q_NULLPTR);
    mainLayout->setCurrentWidget(playerZone);
    overlayZone->show();
    overlayZone->raise();
}

void P_Cam9RTCPlayer::onShowWallLayer() {
    sourceWarnigZone->hide();
    QString playerName = "Player " + this->control()->getDisplayName();
    //   control()->newUserAction(Message.PLAYER_STOPED, Q_NULLPTR);
    mainLayout->setCurrentWidget(wallZone);
    overlayZone->hide();
}

void P_Cam9RTCPlayer::onShowSourceWarning(){
    control()->newUserAction(Message.PLAYER_NO_SOURCE, Q_NULLPTR);
    sourceWarnigZone->show();
    sourceWarnigZone->raise();
    overlayZone->show();
    overlayZone->raise();
}

void P_Cam9RTCPlayer::setSize(QSize size) {
    videoSize = size;
}

void P_Cam9RTCPlayer::adjustLayout(QSize size) {
    this->sourceWarnigZone->move(0,0);
    this->sourceWarnigZone->resize(size);

    this->wallZone->move(0, 0);
    this->wallZone->resize(size);

    this->blackZone->move(0, 0);
    this->blackZone->resize(size);

    this->playerZone->move(0, 0);
    this->playerZone->resize(size);

    this->overlayZone->move(0, 0);
    this->overlayZone->resize(size);
}

bool P_Cam9RTCPlayer::eventFilter(QObject *watched, QEvent *event) {
    QWidget *sender = qobject_cast<QWidget *>(watched);
    if (sender != Q_NULLPTR && sender == this->zone) {
        switch (event->type()) {
        case QEvent::Resize: {
            QResizeEvent *resizeEvent = (QResizeEvent *)(event);
            QSize newSize = resizeEvent->size();
            this->adjustLayout(newSize);
        } break;
        default:
            break;
        }
    }
    return true;
}


void P_Cam9RTCPlayer::updateSizeWhenExitFullScreenMode(){
    //    int topHeight = 40;
    //    int rightWidth = 420;
    qDebug()  << Q_FUNC_INFO << "updateSizeWhenExitFullScreenMode" << this->playerZone->size() <<this->zone->size();
    QSize sizeUpdate = this->zone->size();
    this->zone->resize(QSize(sizeUpdate.width() - 420 ,sizeUpdate.height() - 40));
    this->playerZone->move(0, 0);
    this->playerZone->updateGeometry();
}

void P_Cam9RTCPlayer::updateSizeWhenEnterFullScreenMode(){
    //    int topHeight = 40;
    //    int rightWidth = 420;
    qDebug()  << Q_FUNC_INFO << "updateSizeWhenExitFullScreenMode" << this->playerZone->size() <<this->zone->size();
    QSize sizeUpdate = this->zone->size();
    this->zone->resize(QSize(sizeUpdate.width() + 420 ,sizeUpdate.height() + 40));
    this->playerZone->move(0, 0);
    this->playerZone->updateGeometry();
}
