#include "objectsmanager.h"
VideoFrame ObjectsManager::getVideoFrame(long frame_id){
    videoFramesLock.lock();
    VideoFrame videoFrame;
    videoFrame = videoFrames.value(frame_id);
    videoFramesLock.unlock();
    return videoFrame;
}


void ObjectsManager::removeFrameImage(long frame_id){
    videoFramesLock.lock();
    videoFrames.remove(frame_id);
    videoFramesLock.unlock();
}

ObjectsManager::ObjectsManager() {
    MAX_QUEUE_SIZE = 500;
    FREE_RANGE = 50;
}

ObjectsManager::~ObjectsManager() {
    frameSerieLock.lock();

    QMapIterator<quint64, FrameObject*> it(frameSerie);
    while (it.hasNext()) {
        it.next();
        FrameObject* frameObj = it.value();
        if (frameObj != Q_NULLPTR)
            delete frameObj;
    }

    frameSerieLock.unlock();
}

bool ObjectsManager::contains(long frame_id) {
    bool ret;

    frameSerieLock.lock();
    ret = frameSerie.contains(frame_id);
    frameSerieLock.unlock();

    return ret;
}

long ObjectsManager::getSize() {
    long ret;

    frameSerieLock.lock();
    ret = frameSerie.size();
    frameSerieLock.unlock();

    return ret;
}

void ObjectsManager::removeAllFrameObject(){
    indexImage = 0;
    frameSerieLock.lock();
    QMapIterator<quint64, FrameObject *> it(frameSerie);
    while (it.hasNext()){
        it.next();
        FrameObject *frameObject = it.value();
        removeFrameImage(it.key());
        frameSerie.take(it.key());
        delete frameObject;
    }
    frameSerieLock.unlock();
    Q_EMIT finished();
}

void ObjectsManager::deleteAllFrameObject(){
    frameSerieLock.lock();
    QMapIterator<quint64, FrameObject *> it(frameSerie);
    while (it.hasNext()){
        it.next();
        FrameObject *frameObject = it.value();
        removeFrameImage(it.key());
        frameSerie.take(it.key());
        delete frameObject;
    }
    frameSerieLock.unlock();
}

void ObjectsManager::addFrameObject(FrameObject* frameObjects) {
    if(frameObjects == Q_NULLPTR) return;
    frameSerieLock.lock();
    long currentImageKey;
    currentImageKey = frameObjects->getFrameImageTimestamp();
    long delay = currentImageKey - lastImageKey;
    //    qDebug() << "delay" << delay;
    if (delay <= 0 || delay > 100) {
        this->timeWait = 40;
    } else {
        this->timeWait = delay;
    }
    lastImageKey = frameObjects->getFrameImageTimestamp();
    if (MAX_QUEUE_SIZE > 0) {
        int queueSize = frameSerie.size();
        if (queueSize < MAX_QUEUE_SIZE) {
            frameSerie.insert(frameObjects->getFrameImageId(), frameObjects);
        } else {
            delete frameObjects;
        }
    } else {
        frameSerie.insert(frameObjects->getFrameImageId(), frameObjects);
    }
    frameSerieLock.unlock();
}

void ObjectsManager::onAddFrameObject(bool isUseFrameObject, VideoFrame videoFrame, long timestamp , const QSet<QString> keysubscribers ){
    if (videoFrame.isValid()){
        if(!isUseFrameObject){
            //            imageQueue->enqueue(timestamp, image);
        } else {
            FrameObject* frameObject = new FrameObject(this, indexImage, timestamp, keysubscribers);
            videoFramesLock.lock();
            videoFrames.insert(indexImage, videoFrame);
            videoFramesLock.unlock();
            indexImage++;
            addFrameObject(frameObject);
        }
    }
}

void ObjectsManager::releaseFrameImage(quint64 frameImageId){
//    long timestampCurrent =  QDateTime::currentMSecsSinceEpoch();
//    qDebug() << "DIFF TIME RELEASE" << timestampCurrent - timestampBefore;
//    timestampBefore = QDateTime::currentMSecsSinceEpoch();
    videoFramesLock.lock();
    videoFrames.take(frameImageId);
    videoFramesLock.unlock();
    frameSerieLock.lock();
    FrameObject* frameObject = frameSerie.take(frameImageId);

    delete frameObject;
    frameSerieLock.unlock();
}


FrameObject* ObjectsManager::getFrameObject(long frame_id) {
    FrameObject* ret = Q_NULLPTR;

    frameSerieLock.lock();
    if (frameSerie.contains(frame_id))
        ret = frameSerie.value(frame_id);
    frameSerieLock.unlock();

    return ret;
}

FrameObject* ObjectsManager::getFirstFrame() {
    FrameObject* ret = Q_NULLPTR;
    frameSerieLock.lock();
    //    qDebug() << Q_FUNC_INFO << frameSerie.size();
    if (!frameSerie.isEmpty())
        ret = frameSerie.first();
    frameSerieLock.unlock();
    if(ret != Q_NULLPTR){
        indexImageCurrent = ret->getFrameImageId();
    }
    return ret;
}

long ObjectsManager::getTimeWait()
{
    double delay;
    frameSerieLock.lock();
    delay = timeWait;
    frameSerieLock.unlock();
    return delay;
}

FrameObject* ObjectsManager::getNextFrameOf(long frame_id) {
    FrameObject* ret = Q_NULLPTR;
    frameSerieLock.lock();
    QMap<quint64, FrameObject*>::iterator currentFrame = frameSerie.find(frame_id);
    if (currentFrame == frameSerie.end()) {// not found
        if (!frameSerie.isEmpty())
            ret = frameSerie.first();
    } else if (++currentFrame != frameSerie.end()) {
        ret = currentFrame.value();
    }
    if(ret != Q_NULLPTR){
        indexImageCurrent = ret->getFrameImageId();
    }
    frameSerieLock.unlock();
    return ret;
}

void  ObjectsManager::removeFrameObject(long obj_id){
    //    qDebug() << Q_FUNC_INFO << QThread::currentThread();
    frameSerieLock.lock();
    FrameObject* frameObject = frameSerie.take(obj_id);
    delete frameObject;
    frameSerieLock.unlock();
}

void ObjectsManager::addSemaphoreToAllFrameObject(QString semaphore){
    qDebug() << Q_FUNC_INFO << QThread::currentThread();
    frameSerieLock.lock();
    QMapIterator<quint64, FrameObject *> it(frameSerie);
    while (it.hasNext()){
        it.next();
        FrameObject *frameObject = it.value();
        //        qDebug() << Q_FUNC_INFO << "frame ID" << frameObject->getFrameImageId();
        frameObject->addSemaphore(semaphore);
    }
    frameSerieLock.unlock();
}

void ObjectsManager::removeSemaphoreOutAllFrameObject(QString semaphore){
    qDebug() << Q_FUNC_INFO << QThread::currentThread();
    frameSerieLock.lock();
    QMapIterator<quint64, FrameObject *> it(frameSerie);
    while (it.hasNext()){
        it.next();
        FrameObject *frameObject = it.value();
        frameObject->removeSemaphore(semaphore);
    }
    frameSerieLock.unlock();
}

QString ObjectsManager::getName() const {
    return name;
}

void ObjectsManager::setName(const QString &value) {
    name = value;
}
