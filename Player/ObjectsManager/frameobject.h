#ifndef FRAMEOBJECT_H
#define FRAMEOBJECT_H

#include <QObject>
#include <QDebug>
#include <QMutex>
#include <QImage>
#include <QMap>
#include <QThread>
#include <QTimer>
#include "dectectedobject.h"
#include "Common/generaldefine.h"
#include "objectsmanager.h"
#include "Player/RTCPlayer/RTCRenderWidget.h"
class ObjectsManager;
class FrameObject : public QObject {
    Q_OBJECT
    ObjectsManager *objectsManager = Q_NULLPTR;
    quint64 frameImageId = 0;
    long frameImageTimestamp = 0;

    QMutex semaphoresLock;
    QSet<QString> semaphores;
public:

    FrameObject(ObjectsManager *_objectsManager, quint64 _frameImageId, long _frameImageTimestamp, QSet<QString> semaphores);
    ~FrameObject();

    quint64 getFrameImageId();
    long getFrameImageTimestamp();
    VideoFrame getVideoFrame();
    void releaseFrameImage(QString workerId);

    bool contains(long obj_id);

    void addSemaphore(QString semaphore);
    void removeSemaphore(QString semaphore);
    bool isEmptySemaphores();
};

#endif // FRAMEOBJECT_H
