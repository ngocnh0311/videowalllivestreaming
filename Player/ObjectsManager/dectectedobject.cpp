#include "dectectedobject.h"

ObjectOutline::ObjectOutline() {

}

ObjectOutline::ObjectOutline(QList<QPoint> _points) {
    points = _points;
}

ObjectOutline::~ObjectOutline() {

}

DetectedObject::DetectedObject(long _objectIdByFrame, long _frameImageId, long _frameImageTimestamp,
                               long _objectOwnerId, ObjectOutline _detectedOutline) {
    id = _objectIdByFrame;

    objectIdByFrame = _objectIdByFrame;
    objectTrackingId = _objectIdByFrame;
    objectProfileId = _objectIdByFrame;

    frameImageId = _frameImageId;
    frameImageTimestamp = _frameImageTimestamp;

    ownerObjectId = _objectOwnerId;

    detectedOutline = _detectedOutline;
}

DetectedObject::~DetectedObject() {

}

long DetectedObject::getId() {
    return id;
}

void DetectedObject::setDetectedModel(QString _detectedModel) {
    detectedModel = _detectedModel;
}

QString DetectedObject::getDetectedModel() {
    return detectedModel;
}

void DetectedObject::setDetectedOutline(ObjectOutline _detectedOutline) {
    detectedOutline = _detectedOutline;
}

ObjectOutline DetectedObject::getDetectedOutline() {
    return detectedOutline;
}

void DetectedObject::setObjectIdByFrame(long _objectIdByFrame) {
    objectIdByFrame = _objectIdByFrame;
}

long DetectedObject::getObjectIdByFrame() {
    return objectIdByFrame;
}

void DetectedObject::setObjectTrackingId(long _objectTrackingId) {
    objectTrackingId = _objectTrackingId;
}

long DetectedObject::getObjectTrackingId() {
    return objectTrackingId;
}

void DetectedObject::setObjectProfileId(long _objectProfileId) {
    objectProfileId = _objectProfileId;
}

long DetectedObject::getObjectProfileId() {
    return objectProfileId;
}
