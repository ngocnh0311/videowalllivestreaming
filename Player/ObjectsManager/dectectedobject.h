#ifndef DECTECTEDOBJECT_H
#define DECTECTEDOBJECT_H

#include <QObject>
#include <QDebug>
#include <QMutex>
#include <QImage>

class ObjectOutline {
public:
    QList<QPoint> points;
    ObjectOutline();
    ObjectOutline(QList<QPoint> _points);
    ~ObjectOutline();
};

class DetectedObject {
    long id;

    long frameImageId;
    long frameImageTimestamp;

    ObjectOutline detectedOutline;
    QString detectedModel;

    long objectIdByFrame;
    long objectTrackingId;
    long objectProfileId;

    long ownerObjectId;

    QImage objectImage;

public:
    DetectedObject(long _objectIdByFrame, long _frameImageId, long _frameImageTimestamp,
                   long _objectOwnerId, ObjectOutline _detectedOutline);
    ~DetectedObject();

    long getId();

    void setDetectedModel(QString _detectedModel);
    QString getDetectedModel();

    void setDetectedOutline(ObjectOutline _detectedOutline);
    ObjectOutline getDetectedOutline();

    void setObjectIdByFrame(long _objectIdByFrame);
    long getObjectIdByFrame();

    void setObjectTrackingId(long _objectTrackingId);
    long getObjectTrackingId();

    void setObjectProfileId(long _objectProfileId);
    long getObjectProfileId();

    void setObjectImage(QImage image);
    QImage getObjectImage();

    QImage* getFrameImage();
};

#endif // DECTECTEDOBJECT_H
