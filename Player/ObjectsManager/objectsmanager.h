#ifndef OBJECTSMANAGER_H
#define OBJECTSMANAGER_H

#include <QMap>
#include <QImage>
#include <QMutex>
#include <QThread>
#include "frameobject.h"
#include <QObject>
#include "Player/RTCPlayer/RTCRenderWidget.h"
class FrameObject;
class ObjectsManager: public QObject {
    Q_OBJECT
    long timestampBefore;
    int MAX_QUEUE_SIZE;
    int FREE_RANGE;
    quint64 indexImageCurrent = 0;
    QMutex frameSerieLock;
    QMap<quint64, FrameObject*> frameSerie;
    QMutex videoFramesLock;
    QMap<quint64, VideoFrame> videoFrames;

    QString name;
    long timeWait = 0;
    quint64 indexImage = 0;
    long lastImageKey = 0;
public:
    ObjectsManager();
    ~ObjectsManager();

    bool contains(long frame_id);
    long getSize();

    void addFrameObject(FrameObject* frameObject);
    FrameObject* getFrameObject(long frame_id);
    FrameObject* getFirstFrame();
    FrameObject* getNextFrameOf(long frame_id);

    void removeFrameObject(long obj_id);
    QString getName() const;
    void setName(const QString &value);
    void removeAllFrameObject();
    void deleteAllFrameObject();
    long getTimeWait();
    void addSemaphoreToAllFrameObject(QString semaphore);
    void removeSemaphoreOutAllFrameObject(QString semaphore);
    void releaseFrameImage(quint64 frameImageId);
    VideoFrame getVideoFrame(long frame_id);

    void removeFrameImage(long frame_id);
public Q_SLOTS:
    void onAddFrameObject(bool isUseFrameObject, VideoFrame videoFrame, long timestamp , const QSet<QString> keysubscribers);
Q_SIGNALS:
    void finished();
};

#endif // OBJECTSMANAGER_H
