#include "frameobject.h"

FrameObject::FrameObject(ObjectsManager *_objectsManager, quint64 _frameImageId, long _frameImageTimestamp, QSet<QString> _semaphores) {
    objectsManager = _objectsManager;
    frameImageId = _frameImageId;
    frameImageTimestamp = _frameImageTimestamp;
    semaphores = _semaphores;
}

FrameObject::~FrameObject() {
}

quint64 FrameObject::getFrameImageId() {
    return frameImageId;
}


long FrameObject::getFrameImageTimestamp() {
    return frameImageTimestamp;
}

VideoFrame FrameObject::getVideoFrame(){
    VideoFrame videoFrame;
    if (objectsManager != Q_NULLPTR) {
        videoFrame = objectsManager->getVideoFrame(frameImageId);
    }
    return videoFrame;
}

void FrameObject::releaseFrameImage(QString workerId){
    if(objectsManager != Q_NULLPTR){
        semaphoresLock.lock();
        semaphores.remove(workerId);
        semaphoresLock.unlock();
        if(isEmptySemaphores()){
            objectsManager->releaseFrameImage(frameImageId);
        }
    }
}

void FrameObject::addSemaphore(QString semaphore) {
    semaphoresLock.lock();
    semaphores.insert(semaphore);
    semaphoresLock.unlock();
}

void FrameObject::removeSemaphore(QString semaphore) {
    semaphoresLock.lock();
    semaphores.remove(semaphore);
    semaphoresLock.unlock();
}

bool FrameObject::isEmptySemaphores(){
    semaphoresLock.lock();
    bool ret = semaphores.size() == 0 ? true : false;
    semaphoresLock.unlock();
    return ret;
}
