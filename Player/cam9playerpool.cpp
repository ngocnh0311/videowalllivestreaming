#include "cam9playerpool.h"

Cam9PlayerPool::Cam9PlayerPool() : QObject(Q_NULLPTR) {}
Cam9PlayerPool::~Cam9PlayerPool() {}

C_Cam9Player *Cam9PlayerPool::getPlayer(Control *control, QWidget *zone,
                                        QString appName, QString displayName) {
    bool playerFound = false;

    C_Cam9Player *foundPlayer = Q_NULLPTR;
    for (int index = 0; index < playerPoolPlayback.size(); ++index) {
        C_Cam9Player *player = playerPoolPlayback.at(index)->value<C_Cam9Player*>();
        if (player->getAppName() == appName &&
                player->getDisplayName() == displayName) {
            foundPlayer = player;
            playerFound = true;
            break;
        }
    }
    if (!playerFound) {
        //    for (int index = 0; index < playerPool.size(); ++index) {
        //      Cam9Player *player = playerPool.at(index);
        //      if (player->getAppName() != appName) {
        //        QString oldOwner = player->getAppName();
        //        foundPlayer = player;
        //        foundPlayer->setAppName(appName);
        //        playerFound = true;
        //        qDebug() << Q_FUNC_INFO << "Cuop cua" << oldOwner << "cho" <<
        //        appName
        //                 << displayName;
        //        break;
        //      }
        //    }
        foundPlayer = createNewPlayer(control, zone);
        foundPlayer->setAppName(appName);
        foundPlayer->setDisplayName(displayName);
    }
    return foundPlayer;
}

void Cam9PlayerPool::deleteAllPlayerOfApp(QString appName){
    int sizePlayer = playerPoolVideowall.size();
    QList<QVariant *> playerPoolVideowallTemp;
    QList<QVariant *> playerPoolVideowallOriginTemp;
    QList<QWidget *> playerZonePoolVideowallOriginTemp;

    for (int index = 0; index < playerPoolVideowall.size(); ++index) {
        C_Cam9RTCPlayer *cPlayer = playerPoolVideowall.at(index)->value<C_Cam9RTCPlayer*>();
        if(cPlayer->getAppName() != appName){
            playerPoolVideowallTemp.append(playerPoolVideowall.at(index));
            playerZonePoolVideowallOriginTemp.append(playerZonePoolVideowallOrigin.at(index));
            playerPoolVideowallOriginTemp.append(playerPoolVideowallOrigin.at(index));
        }
    }
    playerPoolVideowall =  playerPoolVideowallTemp;
    playerZonePoolVideowallOrigin = playerZonePoolVideowallOriginTemp;
    playerPoolVideowallOrigin = playerPoolVideowallOriginTemp;
}

C_Cam9RTCPlayer *Cam9PlayerPool::getCam9RTCPlayer(Control *control, QWidget *zone,
                                                  QString appName, QString displayName) {
    bool playerFound = false;
    C_Cam9RTCPlayer *foundPlayer = Q_NULLPTR;
    QString appCameras = "Cameras";
    if(appName == appCameras){
        for (int index = 0; index < playerPoolCameras.size(); ++index) {
            C_Cam9RTCPlayer *player = playerPoolCameras.at(index)->value<C_Cam9RTCPlayer*>();

            if (player->getAppName() == appName &&
                    player->getDisplayName() == displayName) {
                if(zone != Q_NULLPTR && player->getZone()){
                    player->changeZone(zone);
                }

                foundPlayer = player;
                playerFound = true;
                break;
            }
        }

        if (!playerFound) {
            foundPlayer = createNewCam9RTCPlayer(appName,control, zone);
            foundPlayer->setAppName(appName);
            foundPlayer->setDisplayName(displayName);
        }

    }else{
        for (int index = 0; index < playerPoolVideowall.size(); ++index) {
            C_Cam9RTCPlayer *player = playerPoolVideowall.at(index)->value<C_Cam9RTCPlayer*>();
            if (player->getAppName() == appName &&
                    player->getDisplayName() == displayName) {
                if(zone != Q_NULLPTR && player->getZone()){
                    player->changeZone(zone);
                }

                foundPlayer = player;
                playerFound = true;
                break;
            }
        }


        if (!playerFound) {
            //    for (int index = 0; index < playerPool.size(); ++index) {
            //      Cam9Player *player = playerPool.at(index);
            //      if (player->getAppName() != appName) {
            //        QString oldOwner = player->getAppName();
            //        foundPlayer = player;
            //        foundPlayer->setAppName(appName);
            //        playerFound = true;
            //        qDebug() << Q_FUNC_INFO << "Cuop cua" << oldOwner << "cho" <<
            //        appName
            //                 << displayName;
            //        break;
            //      }
            //    }


            foundPlayer = createNewCam9RTCPlayer(appName,control, zone);
            foundPlayer->setAppName(appName);
            foundPlayer->setDisplayName(displayName);
        }
    }
    return foundPlayer;
}



void Cam9PlayerPool::playAll(QString appName){
    Q_UNUSED(appName)
    //  for (int index = 0; index < playerPool.size(); ++index) {
    //    C_Cam9Player *player = playerPool.at(index);
    //    if (player->getAppName() == appName) {
    //      player->open();
    //      player->play();
    //    }
    //  }
}

C_Cam9Player *Cam9PlayerPool::createNewPlayer(Control *control, QWidget *zone) {
    C_Cam9Player *player = new C_Cam9Player(control, zone);
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<C_Cam9Player*>(player);
    playerPoolPlayback.append(dataStruct);
    return player;
}

C_Cam9RTCPlayer *Cam9PlayerPool::createNewCam9RTCPlayer(QString appName,Control *control, QWidget *zone) {
    QString appCameras = "Cameras";
    C_Cam9RTCPlayer *player = new C_Cam9RTCPlayer(control, zone);
    QVariant *dataStruct = new QVariant();
    player->setAppName(appName);
    dataStruct->setValue<C_Cam9RTCPlayer*>(player);
    if(appName == appCameras){
        playerPoolCameras.append(dataStruct);
        playerZonePoolCameralOrigin.append(zone);
        playerPoolCameraslOrigin.append(dataStruct);

    }else{
        playerPoolVideowall.append(dataStruct);
        playerZonePoolVideowallOrigin.append(zone);
        playerPoolVideowallOrigin.append(dataStruct);
    }
    return player;
}

//swap zone player
void Cam9PlayerPool::swapPlayer(QString appName,int indexSource, int indexTarget){
    QString appNameVideoWall = "Video Wall";
    QString appNameMagicShow = "MagicShow";

    QList<QVariant *> playerPoolVideowallOfAppNameTemp;
    QList<QWidget *> playerZonePoolVideowallOriginOfAppNameTemp;

    for (int index = 0; index < playerPoolVideowall.size(); ++index) {
        C_Cam9RTCPlayer *cPlayer = playerPoolVideowall.at(index)->value<C_Cam9RTCPlayer*>();
        if(cPlayer->getAppName() == appName){
            playerPoolVideowallOfAppNameTemp.append(playerPoolVideowall.at(index));
            playerZonePoolVideowallOriginOfAppNameTemp.append(playerZonePoolVideowallOrigin.at(index));
        }
    }

    //videowall
    if(indexSource > playerZonePoolVideowallOriginOfAppNameTemp.size() || indexTarget > playerZonePoolVideowallOriginOfAppNameTemp.size()) return;
    QWidget *playerZoneSourceVideowall = playerZonePoolVideowallOriginOfAppNameTemp.at(indexSource);
    QWidget *playerZoneTargetVideowall = playerZonePoolVideowallOriginOfAppNameTemp.at(indexTarget);

    C_Cam9RTCPlayer *playerSourceVideowall = playerPoolVideowallOfAppNameTemp.at(indexSource)->value<C_Cam9RTCPlayer*>();
    playerSourceVideowall->setDisplayName(QString::number(indexTarget));
    playerSourceVideowall->setZone(playerZoneTargetVideowall);

    C_Cam9RTCPlayer *playerTargetVideowall = playerPoolVideowallOfAppNameTemp.at(indexTarget)->value<C_Cam9RTCPlayer*>();
    playerTargetVideowall->setDisplayName(QString::number(indexSource));
    playerTargetVideowall->setZone(playerZoneSourceVideowall);
    playerPoolVideowallOfAppNameTemp.swap(indexSource, indexTarget);

    //delete player before append

    int sizePlayer = playerPoolVideowall.size();
    QList<QVariant *> playerPoolVideowallTemp;
    QList<QWidget *> playerZonePoolVideowallOriginTemp;

    for (int index = 0; index < playerPoolVideowall.size(); ++index) {
        C_Cam9RTCPlayer *cPlayer = playerPoolVideowall.at(index)->value<C_Cam9RTCPlayer*>();
        if(cPlayer->getAppName() != appName){
            playerPoolVideowallTemp.append(playerPoolVideowall.at(index));
            playerZonePoolVideowallOriginTemp.append(playerZonePoolVideowallOrigin.at(index));
        }
    }


    for (int index = 0; index < playerPoolVideowallOfAppNameTemp.size(); ++index) {
        playerPoolVideowallTemp.append(playerPoolVideowallOfAppNameTemp.at(index));
        playerZonePoolVideowallOriginTemp.append(playerZonePoolVideowallOriginOfAppNameTemp.at(index));
    }

    playerPoolVideowall =  playerPoolVideowallTemp;
    playerZonePoolVideowallOrigin = playerZonePoolVideowallOriginTemp;
}

void Cam9PlayerPool::resetIndexPlayers(QString appName, QList<QWidget *> listZone){
    QString appNameVideoWall = "Video Wall";
    QString appNameMagicShow = "MagicShow";

    QList<QVariant *> playerPoolVideowallAppNameTemp;
    QList<QVariant *> playerPoolVideowallAllTemp;
    QList<QWidget *> playerZonePoolVideowallAllOriginTemp;

    //change zone of player
    int sizeAllPlayer = playerPoolVideowall.size();
    //remove all player zone old of app
    for (int index = 0; index < sizeAllPlayer; ++index) {
        C_Cam9RTCPlayer *cPlayer = playerPoolVideowall.at(index)->value<C_Cam9RTCPlayer*>();
        if(cPlayer->appName != appName){
            playerZonePoolVideowallAllOriginTemp.append(playerZonePoolVideowallOrigin.at(index));
        }
    }

    //append zone of app new
    for (int index = 0; index < listZone.size(); ++index) {
        playerZonePoolVideowallAllOriginTemp.append(listZone.at(index));
    }

    playerZonePoolVideowallOrigin = playerZonePoolVideowallAllOriginTemp;


    //change player index and displayName
    for (int index = 0; index < sizeAllPlayer; ++index) {
        C_Cam9RTCPlayer *cPlayer = playerPoolVideowall.at(index)->value<C_Cam9RTCPlayer*>();
        if(cPlayer->getAppName() == appName){
            playerPoolVideowallAppNameTemp.append(playerPoolVideowall.at(index));
        }
    }

    //remove all player of app
    for (int index = 0; index < sizeAllPlayer; ++index) {
        C_Cam9RTCPlayer *cPlayer = playerPoolVideowall.at(index)->value<C_Cam9RTCPlayer*>();
        if(cPlayer->getAppName() != appName){
            playerPoolVideowallAllTemp.append(playerPoolVideowall.at(index));
        }
    }

    //change zone and index of player
    for (int index = 0; index < playerPoolVideowallAppNameTemp.size(); ++index) {
        C_Cam9RTCPlayer *cPlayer = playerPoolVideowallAppNameTemp.at(index)->value<C_Cam9RTCPlayer*>();
        cPlayer->setZone(listZone.at(index));
        cPlayer->setDisplayName(QString::number(index));
    }

    for (int index = 0; index < playerPoolVideowallAppNameTemp.size(); ++index) {
        playerPoolVideowallAllTemp.append(playerPoolVideowallAppNameTemp.at(index));
    }

    playerPoolVideowall = playerPoolVideowallAllTemp;

}

/**
     * Method to receive a message from an other agent.
     * @param message    : A string which describe the request
     * @param attachment : A ref on an eventual object necessary to treat the
  *request
     **/
void Cam9PlayerPool::newAction(int message, QVariant* attachment) {
    switch (message) {
    case Message.CREATE_NEW_CAM9_RTC_PLAYER:{


        //        PLAYER_DATA_STRUCT dataCreatPlayer = attachment->value<PLAYER_DATA_STRUCT>();
        //        Control * control = dataCreatPlayer.control;
        //        QWidget *zone = dataCreatPlayer.zone;
        //        C_Cam9RTCPlayer *newPlayer  = createNewCam9RTCPlayer(control, zone);
        //        QVariant *dataStruct = new QVariant();
        //        dataStruct->setValue<C_Cam9RTCPlayer*>(newPlayer);
        //        playerPoolVideowall.append(dataStruct);

    } break;

    case Message.CREATE_NEW_CAM9_PLAYER:{

        PLAYER_DATA_STRUCT dataCreatPlayer = attachment->value<PLAYER_DATA_STRUCT>();
        Control * control = dataCreatPlayer.control;
        QWidget *zone = dataCreatPlayer.zone;

        C_Cam9Player *newPlayer  = createNewPlayer(control, zone);
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<C_Cam9Player*>(newPlayer);
        playerPoolPlayback.append(dataStruct);

    } break;

    case Message.GET_CAM9_RTC_PLAYER:{
        //        Control *control, QWidget *zone,QString appName, QString displayName
        C_Cam9RTCPlayer *cPlayer = Q_NULLPTR;
        PLAYER_DATA_STRUCT playerInfo= attachment->value<PLAYER_DATA_STRUCT>();
        Control * control = playerInfo.control;
        QWidget *zone = playerInfo.zone;
        QString appName = playerInfo.appName;
        QString displayName = playerInfo.displayName;
        cPlayer = getCam9RTCPlayer(control, zone, appName, displayName);
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<C_Cam9RTCPlayer*>(cPlayer);
    }break;

    case Message.GET_CAM9_PLAYER:{
        PLAYER_DATA_STRUCT playerInfo= attachment->value<PLAYER_DATA_STRUCT>();
        Control * control = playerInfo.control;
        QWidget *zone = playerInfo.zone;
        QString appName = playerInfo.appName;
        QString displayName = playerInfo.displayName;
        C_Cam9Player *cPlayer = getPlayer(control, zone, appName, displayName);
        QVariant *dataStruct = new QVariant();
        dataStruct->setValue<C_Cam9Player*>(cPlayer);
    }break;
    }
}
