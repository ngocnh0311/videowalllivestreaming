﻿#ifndef RTCPLAYERWIDGET_H
#define RTCPLAYERWIDGET_H

#include <QBuffer>
#include <QByteArray>
#include <QElapsedTimer>
#include <QFile>
#include <QLabel>
#include <QMovie>
#include <QPushButton>
#include <QQueue>
#include <QRunnable>
#include <QThread>
#include <QThreadPool>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include <PacModel/control.h>
#include <QFileDialog>
#include <QMutex>

#include "RTCSocket.h"
#include "RTCSplitter.h"
#include "RTCRender.h"
#include "LinkManager.h"
#include "cam9mapqueue.h"
#include "message.h"
#include "Camera/camitem.h"
#include "NbtpSocket.h"
#include "Player/Cam9RTCPlayer/c_cam9rtcplayer.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/c_crawlercamera.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlerimagequeue.h"

class RTCSocket;
class RTCDownloader;
class RTCSplitter;
class RTCRender;
class RTCRenderWidget;

class LinkManager;
class RTCReceiver;
class cam9downloader;
class NbtpSocket;
class C_Cam9RTCPlayer;
class C_CrawlerCamera;
class CrawlerImageQueue;

enum RTCPlayerState {
    RTCPS_Loading,
    RTCPS_Playing,
    RTCPS_Paused,
    RTCPS_Stopped
};

class RTCPlayer : public QObject {
    Q_OBJECT

private:
    QString appName;
    QWidget *zone = Q_NULLPTR;
    QWidget *topbarSpaceHolder;
    QWidget *videoZone;
    QVBoxLayout *videoLayout;
    bool playerIsPlaying = false;

    AppMessage Message;
    QVBoxLayout *mainLayout;

    C_Cam9RTCPlayer *cCam9RTCPlayer = Q_NULLPTR;

    // playback
    NbtpSocket *nbtpSocket;
    LinkManager *linkManager; // playback stream manager

    QNetworkAccessManager *naManager;
    RTCSocket *pRTCSocket;
    RTCDownloader *rtpDownloader;
    RTCSplitter *pRTCSplitter;
    RTCRender *pRTCRenderer;
    RTCRenderWidget *pRTCRendererWidget;

    Cam9MapQueue rtpQueue;
    Cam9ImageQueue imageQueue;

    QQueue<cam9downloader *> downloaderPool;
    QMutex downloaderPoolMutex;

    QString sourceUrl = "";
    bool isAutoPlay = true;
    bool isAutoReconnect = true;
    QString playerName;

    CamItem *camItem = Q_NULLPTR;
    CamItemType networkType;

    LayoutStruct selectedLayoutCurrent;

public:
    AppContext* appContext;
    RTCPlayer(QWidget *zone, C_Cam9RTCPlayer *_cCam9RTCPlayer);
    ~RTCPlayer();
    void startWorking();
    void stopWorking();

    bool getIsAutoReconnect();
    void setIsAutoReconnect(bool value);

    bool getIsAutoPlay() const;
    void setIsAutoPlay(bool value);
    QString getPlayerName() const { return playerName; }
    void setPlayerName(QString value) {
        playerName = value;
    }
    void wheelEventZoomVideo(QVariant *dataStruct);
    void eventMoveVideo(QVariant *dataStruct);

    Cam9MapQueue& getRtpQueue() { return rtpQueue; }
    Cam9ImageQueue& getImageQueue() { return imageQueue; }

    void newAction(int message, QVariant *attachment);

    QNetworkAccessManager *getNaManager() const;

    NbtpSocket *getNbtpSocket() const;

    RTCSocket* getPRTCSocket()  { return pRTCSocket; }
    RTCDownloader *getRtpDownloader() const;
    RTCSplitter *getPRTCSplitter() const;

    bool isPlayerPlaying() const;
    void takeScreenshoot();
    QString getAppName() const;
    void setAppName(const QString &value);

    CamItemType getNetworkType() const;
    QString getChannelSaveOfCamera(int cameraId);
    void changeZone(QWidget *zoneNew);
public Q_SLOTS:
    void onSocketConnected();
    void onSocketDisconnected();

    void onShowLoadingSign();
    void onShowPlayerLayer();
    void onShowNoCameraLayer();

    void onSetDisplayNameForPlayer(QString displayName) {
        this->playerName = displayName;
    }

    void onSetAppNameForPlayer(QString appName) {
        this->appName= appName;
    }
    void onSetKeyPlayer(QString keyPlayer);
protected:
    bool eventFilter(QObject *watched, QEvent *event);
public Q_SLOTS:
Q_SIGNALS:
    void showLoadingSign();
    void showPlayerLayer();
    void showNoCameraLayer();
    void showSourceWarning();
    void showIndicatorLastFrame();
    void hideIndicatorLastFrame();
    void setKeyPlayer(QString key);
};

#endif  // RTCPLAYERWIDGET_H
