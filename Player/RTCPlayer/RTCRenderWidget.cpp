﻿#include "RTCRenderWidget.h"

RTCRenderWidget::RTCRenderWidget(QWidget *parent) : QOpenGLWidget(parent) {
    setWindowFlags(Qt::FramelessWindowHint);

    QtAV::setLogLevel(QtAV::LogOff);
    QtAV::setFFmpegLogLevel(0);
    videoRenderer = VideoRenderer::create(VideoRendererId_OpenGLWidget);

    videoRenderer->setOutAspectRatioMode(
                VideoRenderer::RendererAspectRatio);
    stackLayout = new QStackedLayout();
    stackLayout->setMargin(0);
    stackLayout->setSpacing(0);
    this->setLayout(stackLayout);
    stackLayout->addWidget(videoRenderer->widget());
    videoRenderer->widget()->setMouseTracking(true);
    Q_ASSERT_X(videoRenderer->widget(), "RTCRenderWidget()", "widget based renderer is not found");
    videoRenderer->widget()->setParent(this);
    videoRenderer->widget()->installEventFilter(this);

    blackZone = new QWidget(this);
    blackZone->setStyleSheet("background:green");
    stackLayout->addWidget(blackZone);
    stackLayout->setCurrentWidget(videoRenderer->widget());
}

void RTCRenderWidget::resizeEvent(QResizeEvent *e)
{
    videoRenderer->widget()->resize(e->size());
}

void RTCRenderWidget::onDrawImageFromRenderer(QImage image) {
    if(!image.isNull() && isWorking){
        videoRenderer->receive(VideoFrame(image));
    }
}

void RTCRenderWidget::onDisplayFrame(VideoFrame videoframe) {
    if(isWorking){
//        if(videoframe.displayAspectRatio() < float(4.0/3)){ //ratio : 4/3
//            videoRenderer->receive(videoframe.toImage());
//        }else{
            videoRenderer->receive(videoframe);
//        }
    }
}
void RTCRenderWidget::startRendering(){
    isWorking = true;
    videoRenderer = VideoRenderer::create(VideoRendererId_OpenGLWidget);
    videoRenderer->setOutAspectRatioMode(
                VideoRenderer::RendererAspectRatio);
    stackLayout->addWidget(videoRenderer->widget());
    stackLayout->setCurrentWidget(videoRenderer->widget());
}

void RTCRenderWidget::stopRendering(){
    isWorking = false;
    stackLayout->setCurrentWidget(blackZone);
    delete videoRenderer;
}

RTCRenderWidget::~RTCRenderWidget() {}

void RTCRenderWidget::wheelEventZoomVideo(QVariant *dataStruct) {
    DataZoomVideo data;
    data = dataStruct->value<DataZoomVideo>();

    QPoint dp = data.pixelDelta;
    qreal deg = data.deg;
    QPoint pos = data.pos;

    QPointF p = videoRenderer->widget()->mapFrom(this, pos);
    QPointF fp = videoRenderer->mapToFrame(p);

    if (fp.x() < 0) fp.setX(0);
    if (fp.y() < 0) fp.setY(0);
    if (fp.x() > videoRenderer->videoFrameSize().width())
        fp.setX(videoRenderer->videoFrameSize().width());
    if (fp.y() > videoRenderer->videoFrameSize().height())
        fp.setY(videoRenderer->videoFrameSize().height());

    QRectF viewport = QRectF(
                videoRenderer->mapToFrame(QPointF(0, 0)),
                videoRenderer->mapToFrame(QPointF(videoRenderer->rendererWidth(),
                                                  videoRenderer->rendererHeight())));
    qreal zoom = 1.0 + deg * 3.14 / 180.0;
    if (!dp.isNull()) {
        zoom = 1.0 + (qreal)dp.y() / 100.0;
        valueZoom = zoom;
    }
    static qreal z = 1.0;
    z *= zoom;
    if (z < 1.0) z = 1.0;
    if (z > 28.0) z = 1.0;
    valueZ = z;
    qreal x0 = fp.x() - fp.x() / z;
    qreal y0 = fp.y() - fp.y() / z;
    videoRenderer->setRegionOfInterest(
                QRectF(x0, y0, qreal(videoRenderer->videoFrameSize().width()) / z,
                       qreal(videoRenderer->videoFrameSize().height()) / z));
    return;
    QTransform m;
    m.translate(fp.x(), fp.y());
    m.scale(1.0 / zoom, 1.0 / zoom);
    m.translate(-fp.x(), -fp.y());
    QRectF r = m.mapRect(videoRenderer->realROI());
    videoRenderer->setRegionOfInterest(
                (r | m.mapRect(viewport)) &
                QRectF(QPointF(0, 0), videoRenderer->videoFrameSize()));
}

void RTCRenderWidget::eventMoveVideo(QVariant *dataStruct) {
    DataMoveVideo dataMove;
    dataMove = dataStruct->value<DataMoveVideo>();
    QPoint pos = dataMove.pos;
    if (pos.x() > 0 && pos.y() >= 40 &&
            pos.x() < videoRenderer->widget()->width() &&
            pos.y() < videoRenderer->widget()->height()) {
        QPointF p = videoRenderer->widget()->mapFrom(this, pos);
        QPointF fp = videoRenderer->mapToFrame(p);
        if (fp.x() < 0) fp.setX(0);
        if (fp.y() < 0) fp.setY(0);
        if (fp.x() > videoRenderer->videoFrameSize().width())
            fp.setX(videoRenderer->videoFrameSize().width());
        if (fp.y() > videoRenderer->videoFrameSize().height())
            fp.setY(videoRenderer->videoFrameSize().height());
        QRectF viewport = QRectF(
                    videoRenderer->mapToFrame(QPointF(0, 0)),
                    videoRenderer->mapToFrame(QPointF(videoRenderer->rendererWidth(),
                                                      videoRenderer->rendererHeight())));

        qreal x0 = fp.x() - fp.x() / valueZ;
        qreal y0 = fp.y() - fp.y() / valueZ;
        videoRenderer->setRegionOfInterest(
                    QRectF(x0, y0, qreal(videoRenderer->videoFrameSize().width()) / valueZ,
                           qreal(videoRenderer->videoFrameSize().height()) / valueZ));
        return;
        QTransform m;
        m.translate(fp.x(), fp.y());
        m.scale(1.0 / valueZoom, 1.0 / valueZoom);

        QRectF r = m.mapRect(videoRenderer->realROI());
        //setRegionOfInterest ::đặt khu vực quan tâm / videoFrameSize kích thước thật hiện tại
        videoRenderer->setRegionOfInterest(
                    (r | m.mapRect(viewport)) &
                    QRectF(QPointF(0, 0), videoRenderer->videoFrameSize()));
    }
}

void RTCRenderWidget::resetRenderWidget(){
    qreal zoom = 1.0;

    QTransform m;
    m.translate(0 , 0);
    m.scale(1.0, 1.0);

    QRectF r = m.mapRect(videoRenderer->realROI());

    QRectF viewport = QRectF(videoRenderer->mapToFrame(QPointF(0, 0)),videoRenderer->mapToFrame(QPointF(0, 0)));

    videoRenderer->setRegionOfInterest(
                (r | m.mapRect(viewport)) &
                QRectF(QPointF(0, 0), QPointF(0, 0)));
}
