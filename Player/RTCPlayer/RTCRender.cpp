﻿#include "RTCRender.h"
RTCRender::RTCRender(RTCPlayer *rtcPlayer) {
    pRTCPlayer = rtcPlayer;
    setObjectName("Render");
    QSettings settings;
    QString modeDebugSave = settings.value("mode_debug").toString();
    if(modeDebugSave == "ON"){
        isDebug = true;
    }else{
        isDebug = false;
    }
}

RTCRender::~RTCRender() {
}

void RTCRender::onStartWorking() {
    timestampBefore = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    while (!isWorking() && this->isRunning()) {
        QThread::msleep(1);
    }
    long timestampCurrent = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    qDebug()<< Q_FUNC_INFO << "Time Wait start render" << timestampCurrent - timestampBefore;
    renderMutex.lock();
    working = true;
    renderMutex.unlock();
    firstFrameObjectMutex.lock();
    isFirstFrameObject  = false;
    firstFrameObjectMutex.unlock();
    if(!this->isRunning()){
        QThread::start();
        if(!isModeDebug()){
            Q_EMIT hideIndicatorLastFrame();
        }
    }
    qDebug() << Q_FUNC_INFO << QThread::currentThread() << isWorking();
}

void RTCRender::onStopWorking() {
    renderMutex.lock();
    working = false;
    renderMutex.unlock();
}

bool RTCRender::isWorking() {
    bool w;
    renderMutex.lock();
    w = working;
    renderMutex.unlock();
    return w;
}

bool RTCRender::isModeDebug(){
    bool w;
    debugMutex.lock();
    w = isDebug;
    debugMutex.unlock();
    return w;
}
bool RTCRender::isFirstFrame(){
    bool w;
    firstFrameObjectMutex.lock();
    w = isFirstFrameObject;
    firstFrameObjectMutex.unlock();
    return w;
}
void RTCRender::setModeDebug(bool value){
    debugMutex.lock();
    isDebug = value;
    debugMutex.unlock();
}

void RTCRender::run() {
    qDebug() << Q_FUNC_INFO << this->keyPlayer << "STARTED";
    this->onSubscibeCrawlerCamera();
    while (isWorking()) {
        if (QThread::currentThread()->isInterruptionRequested()) {
            qDebug() << objectName() << QThread::currentThread() << "Interrupt2";
            return;
        }
        if(objectsManager != Q_NULLPTR && isWorking()){
            // reach the low water mark
            if (objectsManager->getSize() == 1) {
                if(isModeDebug()){
                    Q_EMIT showLoadingSign();
                }else{
                    Q_EMIT showIndicatorLastFrame();
                }

                while (objectsManager->getSize() < fps && isWorking()) {
                    QThread::msleep(1);
                }
                if(isWorking()){
                    if(!isModeDebug()){
                        Q_EMIT hideIndicatorLastFrame();
                    }else{
                        Q_EMIT showPlayerLayer();
                    }
                }else{
                    break;
                }
            }

            while(frameObject == Q_NULLPTR && !isFirstFrame() && isWorking()) {
                // qDebug() << "WHILE 2" << "frameObject FIRST" <<"Size" <<objectsManager->getSize() <<"objectsManager+++" <<objectsManager->getName();
                if (QThread::currentThread()->isInterruptionRequested()) {
                    qDebug() << objectName() << QThread::currentThread() << "Interrupt2";
                    return;
                }
                if(isWorking()){
                    frameObject = objectsManager->getFirstFrame();
                }
                if (frameObject == Q_NULLPTR) {
                    QThread::msleep(40);
                }else{
                    firstFrameObjectMutex.lock();
                    isFirstFrameObject = true;
                    firstFrameObjectMutex.unlock();
                    Q_EMIT showPlayerLayer();
                    break;
                }
            }

            if (frameObject != Q_NULLPTR) {
                lastFrameImageId = frameObject->getFrameImageId();
                if(isWorking()){
                    VideoFrame videoFrame = frameObject->getVideoFrame();
                    if(videoFrame.isValid()){
                        //                    qDebug() << "RENDER IMAGE"<< "KEY" << this->keyPlayer << "NAME OBJECT MANAGER" << objectsManager->getName() << "SIZE " << objectsManager->getSize() << "FRAMEID" << frameObject->getFrameImageId()<< this;
                        rendererWidget->onDisplayFrame(videoFrame);
                        frameObject->releaseFrameImage(this->keyPlayer);
                    }
                }else{
                    break;
                }
            }

            int size = objectsManager->getSize();
            QString modeCurrent = pRTCPlayer->getNetworkType().name;
            if(modeCurrent == "SD"){
                bufferScale = 3;
            } else if(modeCurrent == "HD"){
                bufferScale = 2;
            }

            if ((size > bufferScale*fps) || ((size >= fps ) && (refreshRate == 20))) {
                refreshRate = 20; // 5 times faster
            } else {
                refreshRate = objectsManager->getTimeWait();
            }

            QThread::msleep(refreshRate);
            //  qDebug() << "refreshRate" <<  refreshRate;
            if(isWorking()){
                frameObject = objectsManager->getNextFrameOf(lastFrameImageId);
            }
        }
        QThread::msleep(1);
    }
    this->onUnSubscibeCrawlerCamera();

}

void RTCRender::onSubscibeCrawlerCamera() {
    qDebug() << Q_FUNC_INFO <<"QThread::currentThread()"<< QThread::currentThread() << "KEY RENDER" << this->keyPlayer;
    DataGetCrawler *dataGetCrawler = new DataGetCrawler();
    dataGetCrawler->crawlerType = this->crawlerTypeCurrent;
    this->crawlerTypeLast = this->crawlerTypeCurrent;
    dataGetCrawler->keySubcriable = this->keyPlayer;
    dataGetCrawler->pCrawlerCamera = Q_NULLPTR;
    QVariant *dataStruct = new QVariant();
    dataStruct->setValue<DataGetCrawler *>(dataGetCrawler);
    pRTCPlayer->newAction(Message.VIDEO_WALL_RENDER_SUBSCRIBE_CRAWLER_CAMERA, dataStruct);
    this->crawlerCamera = dataGetCrawler->pCrawlerCamera;
    if (this->crawlerCamera != Q_NULLPTR) {
        this->setObjectsManager(this->crawlerCamera->getObjectsManager());
    }
}

void RTCRender::onUnSubscibeCrawlerCamera(){
    qDebug() << Q_FUNC_INFO <<"QThread::currentThread()"<< QThread::currentThread() << "KEY RENDER" << this->keyPlayer;
    if(this->crawlerCamera != Q_NULLPTR){
        QVariant *dataStruct = new QVariant();
        objectsManager = Q_NULLPTR;
        frameObject = Q_NULLPTR;
        crawlerCamera = Q_NULLPTR;
        DataStopCrawler *dataStopCrawler = new DataStopCrawler();
        dataStopCrawler->crawlerType = this->crawlerTypeLast;
        dataStopCrawler->keyUnSubcriable = this->keyPlayer ;
        dataStruct->setValue<DataStopCrawler *>(dataStopCrawler);
        pRTCPlayer->newAction(Message.VIDEO_WALL_RENDER_UNSUBSCRIBE_CRAWLER_CAMERA, dataStruct);
    }
}

void RTCRender::setRendererWidget(RTCRenderWidget *value) {
    rendererWidget = value;
}

RTCRenderWidget* RTCRender::getRendererWidget() const {
    return rendererWidget;
}

void RTCRender::setCrawlerType(const CrawlerType &value)
{
    crawlerTypeCurrent.cameraId = value.cameraId;
    crawlerTypeCurrent.protocol = value.protocol;
    crawlerTypeCurrent.network = value.network;
    crawlerTypeCurrent.channelName = value.channelName;
    crawlerTypeCurrent.dataSource = value.dataSource;
}
