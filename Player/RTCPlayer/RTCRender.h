﻿#ifndef RTCRENDER_H
#define RTCRENDER_H

#include <QDateTime>
#include <QDebug>
#include <QElapsedTimer>
#include <QImage>
#include <QPainter>
#include <QThread>
#include <QtCore>
#include "RTCPlayer.h"
#include "RTCRenderWidget.h"
#include "cam9imagequeue.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/CrawlerUtilities/crawlerimagequeue.h"
#include "Player/ObjectsManager/frameobject.h"
#include "Player/ObjectsManager/objectsmanager.h"
#include "SensorCameraManager/SensorCamera/CrawlerCamera/c_crawlercamera.h"
class RTCPlayer;
class RTCRenderWidget;
class FrameObject;
class RTCRender: public QThread{
    Q_OBJECT

private:
    long timestampBefore = 0;
    QString keyPlayer;
    AppMessage Message;
    RTCPlayer *pRTCPlayer  = Q_NULLPTR;
    RTCRenderWidget *rendererWidget  = Q_NULLPTR;

    //object manager
    C_CrawlerCamera *crawlerCamera = Q_NULLPTR;
    ObjectsManager *objectsManager = Q_NULLPTR;
    FrameObject* frameObject = Q_NULLPTR;
    long lastFrameImageId = 0;

    int fps = 25;
    int refreshRate = 40;

    QMutex renderMutex;
    bool working = false;
    QMutex stoppedLock;
    bool stopped ;


    QMutex firstFrameObjectMutex;
    bool isFirstFrameObject = false;

    QMutex debugMutex;
    bool isDebug = true;

    int bufferScale = 1;

    CrawlerType crawlerTypeCurrent;
    CrawlerType crawlerTypeLast;
public:
    explicit RTCRender(RTCPlayer *rtcPlayer);
    ~RTCRender();
    bool isFirstFrame();
    bool isWorking();


    RTCRenderWidget *getRendererWidget() const;
    void setRendererWidget(RTCRenderWidget *value);
    void setModeDebug(bool value);
    bool isModeDebug();

    void setCrawlerType(const CrawlerType &value);
    void setObjectsManager(ObjectsManager *_objectsManager){
        this->objectsManager = _objectsManager;
    }
    void onStartWorking();
    void onStopWorking();
    void onSubscibeCrawlerCamera();
    void onUnSubscibeCrawlerCamera();
public Q_SLOTS:


Q_SIGNALS:
    void startPlaying();
    void startLoading();
    void startReplaying();

    void showPlayerLayer();
    void showLoadingSign();
    void showNoCameraLayer();
    void showIndicatorLastFrame();
    void hideIndicatorLastFrame();
    void removeRenderingDone(QString keyRender, FrameObject *frameObject);
    void stoppedWorking();
    void startedWorking();
public Q_SLOTS:
    void onSetKeyPlayer(QString key){
        this->keyPlayer = key;
    }

protected:
    void run() override;

};

#endif  // RTCRENDER_H
