﻿#include "RTCPlayer.h"

#define USE_CRAWLER

RTCPlayer::RTCPlayer(QWidget *zone, C_Cam9RTCPlayer *_cCam9RTCPlayer) {
    cCam9RTCPlayer = _cCam9RTCPlayer;
    appContext = cCam9RTCPlayer->getAppContext();
    this->zone = zone;
    this->zone->installEventFilter(this);

    mainLayout = new QVBoxLayout(this->zone);
    mainLayout->setSpacing(0);
    mainLayout->setMargin(0);
    this->zone->setLayout(mainLayout);

    topbarSpaceHolder = new QWidget(this->zone);
    topbarSpaceHolder->setStyleSheet("background-color: #111;");
    topbarSpaceHolder->setFixedHeight(30);
    mainLayout->addWidget(topbarSpaceHolder);

    videoZone = new QWidget(this->zone);
    videoZone->setStyleSheet("background-color: black;");
    videoLayout = new QVBoxLayout();
    videoLayout->setSpacing(0);
    videoLayout->setMargin(0);
    videoZone->setLayout(videoLayout);
    mainLayout->addWidget(videoZone);

#ifndef USE_CRAWLER
    rtpQueue.setName("RTP Queue" + playerName);
    naManager = new QNetworkAccessManager(this);
    rtpDownloader = new RTCDownloader(this, naManager);
    pRTCSplitter = new RTCSplitter(this);

    pRTCSocket = new RTCSocket(this);
    connect(pRTCSocket, &RTCSocket::socketConnected, this, &RTCPlayer::onSocketConnected);
    connect(pRTCSocket, &RTCSocket::socketDisconnected, this, &RTCPlayer::onSocketDisconnected);
    connect(pRTCSplitter, &RTCSplitter::shouldReopenSouce, getPRTCSocket(), &RTCSocket::openSocket);
    connect(pRTCSocket , &RTCSocket::resetTimerReopenSouce , pRTCSplitter , &RTCSplitter::onResetTimerReopenSouce);
#endif
    pRTCRenderer = new RTCRender(this);
    pRTCRendererWidget = new RTCRenderWidget(videoZone);
    pRTCRenderer->setRendererWidget(pRTCRendererWidget);
    videoLayout->addWidget(pRTCRendererWidget);

    connect(this, &RTCPlayer::setKeyPlayer , this , &RTCPlayer::onSetKeyPlayer);
    connect(pRTCRenderer, &RTCRender::showLoadingSign, this, &RTCPlayer::onShowLoadingSign);
    connect(pRTCRenderer, &RTCRender::showPlayerLayer, this, &RTCPlayer::onShowPlayerLayer);
    connect(pRTCRenderer, &RTCRender::showNoCameraLayer, this, &RTCPlayer::onShowNoCameraLayer);
    connect(pRTCRenderer, &RTCRender::showIndicatorLastFrame, this, &RTCPlayer::showIndicatorLastFrame);
    connect(pRTCRenderer, &RTCRender::hideIndicatorLastFrame, this, &RTCPlayer::hideIndicatorLastFrame);
}

void RTCPlayer::changeZone(QWidget *zoneNew){
    this->zone = zoneNew;
    this->zone->setLayout(mainLayout);
}


void RTCPlayer::onSetKeyPlayer(QString keyPlayer){
    pRTCRenderer->onSetKeyPlayer(keyPlayer);
}
RTCPlayer::~RTCPlayer() {

    pRTCRenderer->onStopWorking();
    delete pRTCSocket;
#ifndef USE_CRAWLER
    pRTCSplitter->startWorking();
    delete naManager;
    delete rtpDownloader;
    delete pRTCSplitter;
#endif
    delete pRTCRenderer;
    delete pRTCRendererWidget;

    delete mainLayout;
}

void RTCPlayer::newAction(int message, QVariant *attachment) {
    switch (message) {

    case Message.PLAYER_RESET_RENDER_WIDGET:{
        pRTCRendererWidget->resetRenderWidget();
    }break;

    case Message.APP_VIDEOWALL_PLAYER_START_RENDERING:{
        pRTCRendererWidget->startRendering();
    }break;

    case Message.APP_VIDEOWALL_PLAYER_STOP_RENDERING:{
        pRTCRendererWidget->stopRendering();

    }break;

    case Message.VIDEO_WALL_RENDER_UNSUBSCRIBE_CRAWLER_CAMERA:
    case Message.VIDEO_WALL_RENDER_SUBSCRIBE_CRAWLER_CAMERA:{
        cCam9RTCPlayer->newAction(message, attachment);
    }break;

    case Message.PLAYER_NEW_LIVE_SOURCE_SET: {
        camItem = attachment->value<CamItem *>();
        selectedLayoutCurrent = camItem->getLayoutSelectedCurrent();
        startWorking();
    } break;
    case Message.VIDEO_WALL_PLAYER_CHANGE_NETWORK_STREAM: {
        newAction(Message.PLAYER_STOP_WORKING , Q_NULLPTR);
        startWorking();
    } break;

    case Message.VIDEO_WALL_PLAYER_SWITCH_TO_SD_WHEN_EXIT_FULL_SCREEN: {
        C_Cam9RTCPlayer *cPlayer = attachment->value<C_Cam9RTCPlayer *>();
        if(camItem->getIsMain() == true && selectedLayoutCurrent.numberOfCameras >= 9) {
            newAction(Message.PLAYER_STOP_WORKING , Q_NULLPTR);
            newAction(Message.PLAYER_PLAY_LIVE_SD, Q_NULLPTR);
            QVariant *dataStruct = new QVariant();
            CamItem *camItemUpdate = camItem;
            camItemUpdate->setIsMain(false);
            dataStruct->setValue(camItemUpdate);
            cPlayer->newAction(message, dataStruct);
        }
    } break;

    case Message.PLAYER_PLAY_LIVE_SD: {
        camItem->setIsMain(false);
        startWorking();
    } break;
    case Message.PLAYER_PLAY_LIVE_HD: {
        camItem->setIsMain(true);
        startWorking();
    } break;

    case Message.PLAYER_STOP_WORKING:
    case Message.PLAYER_SOURCE_CLEAR: {
        stopWorking();
    } break;

    case Message.UPDATE_MODE_RENDER_DEBUG: {
        QString modeDebug = attachment->value<QString>();
        if(modeDebug == "ON") {
            pRTCRenderer->setModeDebug(true);
        } else {
            pRTCRenderer->setModeDebug(false);
        }
    } break;

    case Message.PLAYER_TAKE_SCREENSHOT: {
        takeScreenshoot();
    } break;
    case Message.APP_NETWORK_IS_REACHABLE:
    case Message.APP_NETWORK_IS_UNREACHABLE: {
#ifndef USE_CRAWLER
        getPRTCSocket()->newAction(message, attachment);
#endif
    } break;
    case Message.HIDE: {
        zone->hide();
    } break;
    case Message.SHOW: {
        zone->show();
    } break;
    case Message.WHEEL_EVENT_ZOOM_VIDEO: {
        wheelEventZoomVideo(attachment);
    } break;
    case Message.EVENT_MOVE_ZOOM_VIDEO: {
        eventMoveVideo(attachment);
    } break;
    default:
        qDebug() << "ERROR : General Internal pac action in RTCPlayer "
                 << "non-catched :" + Message.toString(message);
    }
}

void RTCPlayer::startWorking() {
    qDebug() << Q_FUNC_INFO;
    if (camItem != Q_NULLPTR) {
        this->pRTCRendererWidget->resetRenderWidget();
        networkType = camItem->getNetworkType();

        int workspaceId = cCam9RTCPlayer->getVwWorkSpace()->windowsAppOfWorkspace->idWindows;
        //Lấy giá trị network của camera trước đấy xem nó là gì
        int userId = appContext->getWorkingUser()->getUserId();
        QString dataSourceCamItem = CameraNetworkSettings::getDataSourceSaveOfCamera(workspaceId, userId, camItem->getCameraId());
        if(!dataSourceCamItem.isEmpty()) {
            if(dataSourceCamItem == dataSourceCamera.CDN || dataSourceCamItem == dataSourceCamera.NVR){
                networkType.protocol = protocolCamera.WS;
            }

            if(dataSourceCamItem == dataSourceCamera.CAM){
                networkType.protocol = protocolCamera.RTSP;
            }

            networkType.dataSource = dataSourceCamItem;
            if(dataSourceCamItem == dataSourceCamera.CAM || dataSourceCamItem == dataSourceCamera.NVR) networkType.network = networkCamera.LAN;
            if(dataSourceCamItem == dataSourceCamera.CDN) networkType.network = networkCamera.CDN;
        }

        networkType.name = camItem->getIsMain() ? "HD" : "SD";
        if(selectedLayoutCurrent.numberOfCameras <= 4) {
            QString channelCamera = getChannelSaveOfCamera(camItem->getCameraId());
            if(!channelCamera.isEmpty()) {
                if(channelCamera == "SD" || channelCamera == "HD") {
                    networkType.name = channelCamera;
                }
            }
        }

        camItem->setNetworkType(networkType);
        networkType.name == "SD" ? camItem->setIsMain(false) : camItem->setIsMain(true);

        CrawlerType crawlerType;
        crawlerType.cameraId = camItem->getCameraId();
        crawlerType.network = camItem->getNetworkType().network;
        crawlerType.dataSource = camItem->getNetworkType().dataSource;
        crawlerType.protocol = camItem->getNetworkType().protocol;

        crawlerType.channelName = camItem->getIsMain() ? "HD" : "SD";
        pRTCRenderer->setCrawlerType(crawlerType);
        //start render working
        playerIsPlaying = true;
        CamStream *camStream = this->camItem->getCamStream(networkType);
        if(camStream){
            onShowLoadingSign();
            pRTCRenderer->onStartWorking();
        }else{
            showSourceWarning();
        }
    } else {
        Q_EMIT showNoCameraLayer();
    }
}

void RTCPlayer::stopWorking() {
    qDebug() << Q_FUNC_INFO << "Player" << playerName << "of" << appName;
    playerIsPlaying = false;
    pRTCRenderer->onStopWorking();
}

QString RTCPlayer::getChannelSaveOfCamera(int cameraId) {
    QString channelCamera = "";
    int indexCamera = -1;
    QString cameraIdString = QString::number(cameraId);
    QSettings settings;
    settings.beginGroup(QString::number(appContext->getWorkingUser()->getUserId()));
    //check all camera save if exits
    int sizeCameras = settings.beginReadArray("cameras_channel");
    for (int index = 0; index < sizeCameras; ++index) {
        settings.setArrayIndex(index);
        QString cameraIdSave = settings.value("cameraid").toString();
        if(cameraIdString == cameraIdSave) {
            indexCamera = index;
        }
    }
    settings.endArray();
    QString cameraIdSave;
    if(indexCamera != -1) {
        settings.beginReadArray("cameras_channel");
        settings.setArrayIndex(indexCamera);
        cameraIdSave = settings.value("cameraid").toString();
        if(cameraIdSave == cameraIdString) {
            channelCamera = settings.value("channel").toString();
        }
        settings.endArray();
    }
    return channelCamera;
}

void RTCPlayer::takeScreenshoot() {
    if (camItem != Q_NULLPTR) {
        QImage image;
        image = zone->grab().toImage();

        QString currentDateTime =
                QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm:ss");
        currentDateTime = currentDateTime.replace("/", "-");
        currentDateTime = currentDateTime.replace(":", "-");
        QString defaultFileName =
                QString("%1 %2").arg(camItem->getPostion()).arg(currentDateTime);
        QSettings settings;
        //        settings.beginGroup(QString::number(control()->appContext->getWorkingUser()->getUserId()));
        QString pathSaveMedia = settings.value("path_save_media").toString();

        QString fileName;
        if (pathSaveMedia == "") {
            QString homePath = QDir().homePath();
            //            QString filePathPictures = homePath + "/" + "MultimediaOfVideowall" + "/" + "Pictures";
            QString filePathPictures = "/mnt/hdd/videowallstores/Pictures";

            if(!QDir(filePathPictures).exists()) {
                QDir().mkpath(filePathPictures);
            }

            QFileDialog dialogSaveImage;
            dialogSaveImage.setOption(QFileDialog::DontUseNativeDialog, false);
            fileName = dialogSaveImage.getSaveFileName(
                        Q_NULLPTR, tr("Save Video Wall Screenshot"), (filePathPictures + "/" + defaultFileName),
                        tr("PNG (*.png);;All Files (*)"));

        } else {
            if(!QDir(pathSaveMedia).exists()) {
                QDir().mkpath(pathSaveMedia);
            }
            QFileDialog dialogSaveImage;
            dialogSaveImage.setOption(QFileDialog::DontUseNativeDialog, false);
            fileName = dialogSaveImage.getSaveFileName(
                        Q_NULLPTR, tr("Save Video Wall Screenshot"), (pathSaveMedia + "/" + defaultFileName),
                        tr("PNG (*.png);;All Files (*)"));
        }

        if (fileName.isEmpty()) {
            return;
        } else {
            if (!fileName.contains(".png")) {
                fileName.append(".png");
            }
            image.save(fileName);
        }
    }
}

bool RTCPlayer::eventFilter(QObject *watched, QEvent *event) {
    QWidget *sender = qobject_cast<QWidget *>(watched);
    if (sender != Q_NULLPTR && sender == zone) {
        switch (event->type()) {
        case QEvent::Resize: {
            QResizeEvent *resizeEvent = (QResizeEvent *)(event);
            QSize newSize = resizeEvent->size();
            videoZone->resize(newSize);
            topbarSpaceHolder->setFixedHeight(30);
            videoZone->move(0, 30);
            videoZone->resize(newSize.width(), newSize.height() - 30);
        }
            break;

        default:
            break;
        }
    }
    return true;
}

RTCDownloader *RTCPlayer::getRtpDownloader() const {
    return rtpDownloader;
}

QNetworkAccessManager *RTCPlayer::getNaManager() const {
    return naManager;
}

NbtpSocket *RTCPlayer::getNbtpSocket() const {
    return nbtpSocket;
}

bool RTCPlayer::isPlayerPlaying() const {
    return playerIsPlaying;
}

QString RTCPlayer::getAppName() const {
    return appName;
}

void RTCPlayer::setAppName(const QString &value) {
    appName = value;
}

CamItemType RTCPlayer::getNetworkType() const {
    return networkType;
}


void RTCPlayer::onSocketConnected() {
}

void RTCPlayer::onSocketDisconnected() {
}

void RTCPlayer::onShowLoadingSign() {
    Q_EMIT showLoadingSign();
}

void RTCPlayer::onShowPlayerLayer() {
    Q_EMIT showPlayerLayer();
}

void RTCPlayer::onShowNoCameraLayer() {
    Q_EMIT showNoCameraLayer();
}

void RTCPlayer::wheelEventZoomVideo(QVariant *dataStruct) {
    pRTCRendererWidget->wheelEventZoomVideo(dataStruct);
}

void RTCPlayer::eventMoveVideo(QVariant *dataStruct) {
    pRTCRendererWidget->eventMoveVideo(dataStruct);
}

RTCSplitter *RTCPlayer::getPRTCSplitter() const {
    return pRTCSplitter;
}
