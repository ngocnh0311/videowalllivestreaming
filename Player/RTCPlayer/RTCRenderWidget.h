﻿#ifndef RTCRENDERWIDGET_H
#define RTCRENDERWIDGET_H

#include <QApplication>
#include <QBuffer>
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QHBoxLayout>
#include <QImage>
#include <QLabel>
#include <QMouseEvent>
#include <QMutex>
#include <QPaintEvent>
#include <QPainter>
#include <QPoint>
#include <QResizeEvent>
#include <QSize>
#include <QTransform>
#include <QVBoxLayout>
#include <QWheelEvent>
#include <QWidget>
#include <QtAV>
#include <QtAVWidgets>
#include "Common/generaldefine.h"
#include <QThread>
#include <QStackedLayout>
#include <QOpenGLWidget>
#include <QDir>
#include <QTimer>
using namespace QtAV;

class RTCRenderWidget : public QOpenGLWidget {
    Q_OBJECT
    long timestampBefore;
    QStackedLayout *stackLayout = Q_NULLPTR;
    QWidget *blackZone = Q_NULLPTR;
    qreal valueZ;
    qreal valueZoom;
    bool isWorking = true;
public:
    RTCRenderWidget(QWidget *parent = 0);
    ~RTCRenderWidget();

    void onDrawImageFromRenderer(QImage image);
    void onDisplayFrame(VideoFrame videoframe);

    void wheelEventZoomVideo(QVariant *dataStruct);
    void eventMoveVideo(QVariant *dataStruct);
    void resetRenderWidget();
    void stopRendering();
    void startRendering();
protected:
    virtual void resizeEvent(QResizeEvent *);
private:
    void initUI();
    VideoRenderer *videoRenderer;
};

#endif  // RTCRENDERWIDGET_H
