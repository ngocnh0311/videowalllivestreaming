#include "cam9imagequeue.h"

double Cam9ImageQueue::getTimeWait()
{
    double delay;
    queueUpdate.lock();
    delay = timeWait;
    queueUpdate.unlock();
    return delay;
}

QImage Cam9ImageQueue::getDequeueImage()
{
    QImage image;
    queueUpdate.lock();
    //    dequeueImage = this->hashmap.take(this->hashmap.firstKey());
    image = dequeueImage;
    queueUpdate.unlock();
    //    qDebug()<< QDateTime::currentMSecsSinceEpoch() << this << Q_FUNC_INFO << image.size();
    return image;
}

bool Cam9ImageQueue::isWorking()
{
    return working;
}

void Cam9ImageQueue::setWorking(bool value)
{
    queueUpdate.lock();
    working = value;
    queueUpdate.unlock();
}

Cam9ImageQueue::Cam9ImageQueue()
{
}

void Cam9ImageQueue::enqueue(double timestamp, QImage newImage) {
    int size = getSize();
    int fps = 25;

    if (size > 6*fps) {
        qDebug() << getName() << "::Image Enqueue size=" << size << " .... EMPTY queue";
        freeQueue();
    }

    queueUpdate.lock();
    if(isWorking()){
        hashmap.insert(timestamp, newImage);
    }
    queueUpdate.unlock();
}

void Cam9ImageQueue::freeQueue(){
    int fps = 25;
    queueUpdate.lock();
    QMapIterator<double, QImage> it(hashmap);
    while (it.hasNext()) {
        it.next();
        if(hashmap.size() > 2*fps){
            hashmap.remove(it.key());
        }else{
            break;
        }
    }
    queueUpdate.unlock();
}

QString Cam9ImageQueue::getName() const
{
    return name;
}

void Cam9ImageQueue::setName(const QString &value)
{
    name = value;
}

void Cam9ImageQueue::dequeue() {
    QImage image;
    double currentImageKey, nextImageKey;
    queueUpdate.lock();
    if(hashmap.size() > 1 ) {
        currentImageKey   = hashmap.firstKey();
        image = hashmap.take(currentImageKey);
        nextImageKey = hashmap.firstKey();
        long delay = nextImageKey- currentImageKey;
        if (delay <= 0 || delay > 100) {
            this->timeWait = 40;
        } else {
            this->timeWait = delay;
        }
    }

    queueUpdate.unlock();
    this->dequeueImage = image;
}

void Cam9ImageQueue::empty() {
    queueUpdate.lock();
    hashmap.clear();
    queueUpdate.unlock();
}

// return video time length in second in the queue
long Cam9ImageQueue::getVideoTime() {
    long videoTime = 1;
    queueUpdate.lock();
    if (hashmap.size() > 0) {
        double firstTimestamp = hashmap.firstKey();
        double lastTimestamp = hashmap.lastKey();
        videoTime = lastTimestamp - firstTimestamp + 1;
    }
    queueUpdate.unlock();
    if (videoTime < 0) videoTime = 10000000;
    return videoTime;
}
