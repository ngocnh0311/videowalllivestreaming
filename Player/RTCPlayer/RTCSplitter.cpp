﻿#include "RTCSplitter.h"

RTCPlayer *RTCSplitter::getPRTCPlayer() const
{
    return pRTCPlayer;
}

RTCSplitter::RTCSplitter(RTCPlayer *rtcPlayer) {
    NALUStartCode.append((char)0x00);
    NALUStartCode.append((char)0x00);
    NALUStartCode.append((char)0x00);
    NALUStartCode.append((char)0x01);
    IDR_NAL_TYPE.append(101);

    decoderOpen();
    this->pRTCPlayer = rtcPlayer;
    this->setObjectName("Decoder");
}

RTCSplitter::~RTCSplitter() {
    decoderClose();
}

void RTCSplitter::startWorking() {
    QString playerName = "Player " + this->pRTCPlayer->getPlayerName();
    qDebug() << playerName << "::RTCSplitter::startWorking" ;
    Cam9MapQueue& rtpQueue = this->pRTCPlayer->getRtpQueue();
    rtpQueue.setWorking(true);
    splitterMutex.lock();
    working = true;
    splitterMutex.unlock();
    checkSPS = false;
    if (!isRunning()) {
        QThread::start();
    }
}

void RTCSplitter::stopWorking() {
    QString playerName = "Player " + this->pRTCPlayer->getPlayerName();
    qDebug() << playerName << "::RTCSplitter::splitterStop" ;
    Cam9MapQueue& rtpQueue = this->pRTCPlayer->getRtpQueue();
    rtpQueue.setWorking(false);
    rtpQueue.empty();
    splitterMutex.lock();
    working = false;
    splitterMutex.unlock();
    checkSPS = false;

}

bool RTCSplitter::isWorking() {
    bool w;
    splitterMutex.lock();
    w = working;
    splitterMutex.unlock();
    return w;
}

void RTCSplitter::run() {
    QString playerName = "Player " + this->pRTCPlayer->getPlayerName();
    Cam9MapQueue& rtpQueue = this->pRTCPlayer->getRtpQueue();
    rtpQueue.setName("RTP Queue of " + this->pRTCPlayer->getPlayerName());
    int HEADER_LENGTH = 12;
    bool GOPStartedWithIFrame = false;
    bool IDRPictureStarted = false;
    bool IDRPictureMiddle = false;
    bool IDRPictureEnd = false;
    QByteArray IDRPicture;
    qDebug() << playerName <<  "::RTCSplitter::run ";
    while (1) {
        while (isWorking()) {
            while (rtpQueue.getSize() < 2) {
                QThread::msleep(1);
            }
            QByteArray rawData = dequeueWithTimeout(downloadTimeWait);
            if (!rawData.isEmpty() && (rawData.size() > HEADER_LENGTH)) {
                long passedBytes = 0;
                bool rtpIsOk = true;
                while (isWorking() && passedBytes < rawData.size() && rtpIsOk) {
                    bool ret;
                    int payloadSize = rawData.mid(passedBytes + 0, 2).toHex().toInt(&ret,16);
                    long long timestring =  rawData.mid(passedBytes + 2, 8).toHex().toLongLong(0, 16);
                    double timestamp = reinterpret_cast<double&>(timestring);
                    QString timestampToString = QString::number(timestamp, 'g', 19);
                    long timestamplong = timestampToString.toLong();

                    int index = rawData.mid(passedBytes + 10, 2).toHex().toInt(&ret,16);
                    // Phần data h264 bị ngắn hơn chiều dài.
                    int lengthOfH264RawData = payloadSize - 10;

                    if (passedBytes + HEADER_LENGTH + lengthOfH264RawData > rawData.size()) {
                        rtpIsOk = false;
                        qDebug() << playerName << " something went wrong... in RTP passedBytes + HEADER_LENGTH + lengthOfH264RawData=" << (passedBytes + HEADER_LENGTH + lengthOfH264RawData) << "/" << rawData.size();
                        continue;
                        break;
                    }
                    QByteArray h264Raw = rawData.mid(passedBytes + HEADER_LENGTH, lengthOfH264RawData);
                    passedBytes = passedBytes + payloadSize + 2;

                    // h264 processing
                    int nalu_header_byte = h264Raw.mid(0, 1).toHex().toInt(&ret,16);
                    int nalType = nalu_header_byte & 0x1F;//0b11111
                    if((nalu_header_byte & 0b10000000)  == 1){
                        qDebug() << "The H.264 specification declares avalue of 1 as a syntax violation";
                    }

                    if (nalType != 28 && IDRPicture.size() > 0){
                        onReceiveRTPPackage(timestamp, NALUStartCode + IDRPicture);
                        IDRPicture.clear();
                    }
                    switch (nalType) {
                    case NALUnit.SEQUENCE_PARAMETER_SET: // SPS Type = 7
                    {
                        checkSPS = true;
                        GOPStartedWithIFrame = false;
                        decodeRTPPackage(NALUStartCode + h264Raw);
                    }
                        break;
                    case NALUnit.PICTURE_PARAMETER_SET: // PPS Type = 8
                    {
                        GOPStartedWithIFrame = false;
                        decodeRTPPackage(NALUStartCode + h264Raw);
                    }
                        break;

                    case NALUnit.ACCESS_UNIT_DELIMITER: // Access unit delimiter = 9
                    {
                        GOPStartedWithIFrame = false;
                        decodeRTPPackage(NALUStartCode + h264Raw);
                    }
                        break;

                    case NALUnit.CODED_SLICE_OF_AN_IDR_PICTURE: // IDR Type = 5
                    {
                        GOPStartedWithIFrame = true;
                        onReceiveRTPPackage(timestamp, NALUStartCode + h264Raw);
                    }
                        break;

                    case NALUnit.FRAGMENTATION_UNITS: { // IDR Picture Type MULTIPLE PART (Fragmentation Units)
                        int FU_A_byte = h264Raw.mid(1, 1).toHex().toInt(&ret,16);
                        if (FU_A_byte & 0x80) { //0b10000000
                            if(IDRPicture.size() > 0){
                                onReceiveRTPPackage(timestamp, NALUStartCode + IDRPicture);
                                IDRPicture.clear();
                            }
                            //nếu bằng 1 tức start IDR
                            IDRPictureStarted = true;
                            IDRPictureMiddle = false;
                            //                            qDebug() << "START MUTIPLE IDR " <<FU_A_byte << "(FU_A_byte & 0b11111)" <<(FU_A_byte & 0b11111);
                            //0xe0: 11100000 (3 bit dau)
                            //0x1f :    11111 (3 bit cuoi)
                            QByteArray IDR_NAL_TYPE_TEMP;
                            IDR_NAL_TYPE_TEMP.append((nalu_header_byte &0xe0) | (FU_A_byte & 0x1f));
                            IDRPicture  =  IDR_NAL_TYPE_TEMP + h264Raw.mid(2, payloadSize - 10);
                        } else
                        {
                            if (FU_A_byte& 0x40) { //0b01000000
                                // end of Fragment Unit
                                if (IDRPictureStarted == false) {
                                    rtpIsOk = false;
                                    break;
                                }
                                IDRPicture = IDRPicture + h264Raw.mid(2, payloadSize-10);
                                GOPStartedWithIFrame = true;
                                onReceiveRTPPackage(timestamp, NALUStartCode + IDRPicture);
                                IDRPicture.clear();
                                //                                qDebug() << "END MUTIPLE IDR " <<FU_A_byte << "SIZE PACKAGE SEND" << IDRPicture.size();
                                //                          qDebug() << index << "  -  IDR multipart ended";
                            } else {
                                //                                qDebug() << "MIDDEL MUTIPLE IDR" << FU_A_byte;
                                //nếu bằng 0
                                if (IDRPictureStarted == false) {
                                    rtpIsOk = false;
                                    break;
                                }
                                IDRPictureMiddle = true;
                                IDRPicture = IDRPicture + h264Raw.mid(2, payloadSize-HEADER_LENGTH);
                            }

                            if(FU_A_byte & 0x10){//0b00100000
                                //The Reserved bit MUST be equal to 0 and MUST be ignored by the receiver.
                                qDebug() << "The Reserved bit MUST be equal to 0 and MUST be ignored by the receiver";

                            }
                        }
                    }
                        break;

                    case NALUnit.CODED_SLICE_OF_A_NON_IDR_PICTURE: { // P-Frame  Picture Type
                        onReceiveRTPPackage(timestamp, NALUStartCode + h264Raw);
                    }
                        break;

                    case 0: { // End Of 10 minute type
                        qDebug() << playerName << " end of 10 minutes " << " nalu_header_byte" << nalu_header_byte;
                    }
                        break;

                    default:
                    {
                        qDebug() << "NALUnit type "<< "non-catched :" << NALUnit.toString(nalType);

                        if((nalu_header_byte & 0b01000000) == 0 ||(nalu_header_byte & 0b00100000) == 0 ){
                            qDebug()<< "nalType" << nalType<<"NRI" << (nalu_header_byte & 0b01000000) << (nalu_header_byte & 0b00100000) << "nalu_header_byte"<< nalu_header_byte << "nalType" << nalType;
                        }

                    }break;

                    }
                }
                this->sequenceOfTimeoutPackage = 0;
            }
        }
        QThread::msleep(1); // trong truong hop chay khong tai de cpu khong bi tang
    }

    qDebug() << playerName << "::RTCSplitter::STOPPED";
}

void RTCSplitter::liveWorker() {

}

void RTCSplitter::onResetTimerReopenSouce(){
    this->sequenceOfTimeoutPackage = 0;
}

QByteArray RTCSplitter::dequeueWithTimeout(long timeout) {
    Cam9MapQueue& rtpQueue = this->pRTCPlayer->getRtpQueue();
    QByteArray rtpData;

    while (isWorking() && (timeout > 0) ) {
        rtpData = rtpQueue.dequeue();
        if (!rtpData.isEmpty()) {
            break;
        }

        if (rtpData.isEmpty()) {
            QThread::msleep(1);
            timeout = timeout - 1;
            this->sequenceOfTimeoutPackage += 1;
        }

        if (rtpQueue.getSize() > 25) {
            qDebug() << "RTCSplitter::dequeueWithTimeout::bypass rtp pkt at " << downloadTimeWait - timeout;
            break;
        }
        if (this->sequenceOfTimeoutPackage >= 15000) { // if no data received after 10s then do reconnect
            this->sequenceOfTimeoutPackage = 0;
            Q_EMIT shouldReopenSouce();
        }
    }
    if (rtpData.isEmpty()) rtpQueue.next();
    return rtpData;
}

QImage RTCSplitter::decode2Image(QByteArray h264Data) {
    QString playerName = "Player " + this->pRTCPlayer->getPlayerName();

    QImage image;
    if (h264Data.size() > 0) {
        // h264 processing
        bool ret;
        int nalu_header_byte = h264Data.mid(4, 1).toHex().toInt(&ret,16);
        int nalType = nalu_header_byte & 0b11111;
        switch (nalType) {
        case NALUnit.SEQUENCE_PARAMETER_SET: { // SPS Type = 7
            decodeRTPPackage(h264Data);
        } break;

        case NALUnit.PICTURE_PARAMETER_SET: { // PPS Type = 8
            decodeRTPPackage(h264Data);
        }
            break;

        case NALUnit.ACCESS_UNIT_DELIMITER: { // ACCESS_UNIT_DELIMITER = 9
            decodeRTPPackage(h264Data);
        }
            break;

        case NALUnit.CODED_SLICE_OF_AN_IDR_PICTURE: { // IDR Type = 5
            image = decodeRTPPackage(h264Data);
        }
            break;

        case NALUnit.CODED_SLICE_OF_A_NON_IDR_PICTURE: { // P-Frame  Pictur Type = 1
            image = decodeRTPPackage(h264Data);
        }
            break;

        default:
            qDebug() << playerName << "RTCSplitter::decode2Image :: NALUnit type "<< "non-catched :" << NALUnit.toString(nalType) << nalType;
            break;
        }
    }
    return image;
}

void RTCSplitter::onReceiveRTPPackage(double timestamp, QByteArray h264Package) {
    QString playerName = "Player " + this->pRTCPlayer->getPlayerName();
    //        qDebug() << playerName << " Thread: " << this->thread()
    //                 << "RTCSplitter onReceiveRTPPackage - packageIndex: " << rtpPackageIndex;

    if (!checkSPS) return;
    Cam9ImageQueue & imageQueue = this->pRTCPlayer->getImageQueue();
    QImage image = decode2Image(h264Package);
    if (!image.isNull()){
        imageQueue.enqueue(timestamp, image);
    }else{
        //        qDebug() << "IMAGE NULL";
    }
}

QImage RTCSplitter::decodeRTPPackage(QByteArray rtpPackage) {
    QImage image;
    int rtpSize = rtpPackage.size();
    if (rtpSize > 0) {
        quint8 *data = new quint8[rtpSize];
        memcpy(data, rtpPackage.constData(), rtpSize);
        AVPacket avPacket;
        av_init_packet(&avPacket);

        avPacket.size = rtpSize;
        avPacket.data = data;

        AVFrame *avFrame = av_frame_alloc();
        if(avFrame == NULL){
            qDebug() << "Unable to allocate frames \n";
        }

        int decodedFrameSize = 0, decodedFrameCount = 0;

        decodedFrameSize = avcodec_decode_video2(avCodecContext, avFrame, &decodedFrameCount, &avPacket);
        if (decodedFrameSize > 0 && decodedFrameCount > 0) {
            int width = avCodecContext->width;
            int height = avCodecContext->height;

            if(width > 0 && height >0){
                struct SwsContext *swsContext = sws_getContext(width , height,
                                                               avCodecContext->pix_fmt, avCodecContext->width,
                                                               avCodecContext->height, AV_PIX_FMT_RGB24, SWS_BICUBIC,
                                                               NULL, NULL, NULL);

                uint8_t *outBuffer = (uint8_t *)av_malloc(avpicture_get_size(
                                                              AV_PIX_FMT_RGB24, avCodecContext->width, avCodecContext->height));
                AVPicture avPicture;
                avpicture_fill(&avPicture, outBuffer, AV_PIX_FMT_RGB24,
                               avCodecContext->width, avCodecContext->height);
                if(swsContext)    { //valid
                    sws_scale(swsContext, (const uint8_t *const *)avFrame->data,
                              avFrame->linesize, 0, avCodecContext->height,
                              avPicture.data, avPicture.linesize);

                    image = QImage(avPicture.data[0],
                            avFrame->width,
                            avFrame->height,
                            avPicture.linesize[0],
                            QImage::Format_RGB888).copy();

                    //                    QString rootPath =  QDir::homePath() + "/" + "imageVideoWall";
                    //                    if (!QDir(rootPath).exists()) {
                    //                        QDir().mkpath(rootPath);
                    //                    }

                    //                    QString filename =  rootPath + "/" + QString::number(QDateTime::currentMSecsSinceEpoch()) + ".png";
                    //                    qDebug() << "FILENAME" <<filename;
                    //                    image.save(filename);

                }

                if (image.isNull() || image.height() <= 0) {
                    qDebug() << "invalid package of H264 package size = " << rtpSize << "WIDTH " << avCodecContext->width << "HEIGHT" << avCodecContext->height ;
                }
                av_free(outBuffer);
                sws_freeContext(swsContext);
            }
        }
        av_frame_unref(avFrame);
        av_frame_free(&avFrame);
        av_packet_unref(&avPacket);
        av_free_packet(&avPacket);
        delete[] data;
    }

    return image;
}

QImage RTCSplitter::decodeRTPPackage2(QByteArray rtpPackage) {

}

void RTCSplitter::decoderOpen() {

    avCodec = avcodec_find_decoder(AV_CODEC_ID_H264);
    if (!avCodec) {
        // TODO
    }
    avCodecContext = avcodec_alloc_context3(avCodec);
    if (!avCodecContext) {
        // TODO
    } else {
        if (avcodec_open2(avCodecContext, avCodec, NULL) < 0) {
            // TODO
        }
        avCodecContext->thread_count = 4;
        avCodecContext->thread_type = 2;
    }
}

void RTCSplitter::decoderClose() {
    if(avCodecContext && avCodecContext->internal && avCodecContext->refcounted_frames != 0) {
        avcodec_flush_buffers(avCodecContext);
        avcodec_free_context(&avCodecContext);
        avCodecContext = avcodec_alloc_context3(avCodec);
        if(avCodecContext){
            avCodecContext->width = 0;
            avCodecContext->height = 0;
        }
    }
}
