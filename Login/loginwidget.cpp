﻿#include "Login/loginwidget.h"
#include <Authentication/user.h>
#include <Camera/camsite.h>
#include <Common/networkutils.h>
#include <qdebug.h>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QScreen>
#include <QStyle>
#include <QVBoxLayout>
#include <QWidget>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <mutex>
#include "Authentication/user.h"
#include "MainFrame/c_mainframe.h"
#include "QApplication"

QString LoginWidget::API_CHECK_TOKEN = "/cms_api/check";
LoginWidget::LoginWidget(QWidget *parent, QVariant *attachment)
    : QDialog(parent) {
    this->responseData = attachment;

    setModal(true);
    setFixedSize(400, 300);
    loginFaildCounter = 0;

    QVBoxLayout *mBoxLayout = new QVBoxLayout();
    mBoxLayout->setAlignment(Qt::AlignCenter);
    mBoxLayout->setSpacing(10);
    mBoxLayout->setMargin(20);
    setLayout(mBoxLayout);

    mUserLabel = new QLabel(this);
    mUserLabel->setText("Tài khoản:");
    mUserLabel->setFont(Resources::instance().getLargeBoldButtonFont());
    mBoxLayout->addWidget(mUserLabel);

    userLineEdit = new QLineEdit(this);
    userLineEdit->setFont(Resources::instance().getLargeRegularButtonFont());
    userLineEdit->setFixedSize(360, 30);
    userLineEdit->setText("");
    connect(userLineEdit, &QLineEdit::returnPressed, this, [this] {
        passWordLineEdit->setText("");
        passWordLineEdit->setFocus();
    });
    mBoxLayout->addWidget(userLineEdit);

    mPasswordLabel = new QLabel(this);
    mPasswordLabel->setFont(Resources::instance().getLargeBoldButtonFont());
    mPasswordLabel->setText("Mật khẩu:");
    mBoxLayout->addWidget(mPasswordLabel);

    passWordLineEdit = new QLineEdit(this);
    passWordLineEdit->setFont(Resources::instance().getLargeRegularButtonFont());
    passWordLineEdit->setFixedSize(360, 30);
    passWordLineEdit->setEchoMode(QLineEdit::Password);
    passWordLineEdit->setText("");
    connect(passWordLineEdit, &QLineEdit::returnPressed, this,
            &LoginWidget::onLoginClicked);
    mBoxLayout->addWidget(passWordLineEdit);

    mMessageLabel = new QLabel(this);
    mMessageLabel->setFont(Resources::instance().getLargeRegularButtonFont());
    mMessageLabel->setFixedSize(360, 30);
    mMessageLabel->setAlignment(Qt::AlignCenter);
    mMessageLabel->setText("");
    mBoxLayout->addWidget(mMessageLabel);

    mLoginButton = new QPushButton(this);
    mLoginButton->setFont(Resources::instance().getLargeBoldButtonFont());
    mLoginButton->setText("Đăng nhập");
    mLoginButton->setFixedSize(360, 30);
    connect(mLoginButton, &QPushButton::clicked, this,
            &LoginWidget::onLoginClicked);
    mBoxLayout->addWidget(mLoginButton);

    //Help user
    QWidget *helpWidget = new QWidget(this);
    helpWidget->setFixedSize(360,70);
    QVBoxLayout *helpLayout = new QVBoxLayout();
    helpLayout->setAlignment(Qt::AlignCenter);
    helpLayout->setSpacing(0);
    helpLayout->setMargin(0);
    helpWidget->setLayout(helpLayout);
    QLabel *helpLabel =  new QLabel(helpWidget);
    helpLabel->setFixedSize(360,30);
    helpLabel->setAlignment(Qt::AlignCenter);
    helpLabel->setText("Tổng đài hỗ trợ miễn phí 1800 80 00 - Nhánh 1");
    helpLabel->setFont(Resources::instance().getSmallBoldButtonFont());

    QLabel *copyrightLabel = new QLabel(this);
    copyrightLabel->setStyleSheet("color:#111");
    QString copyright = "";
    copyrightLabel->setText("2017 "+ copyright +" Tập đoàn Viettel");
    helpLayout->addWidget(copyrightLabel);
    copyrightLabel->setFont(Resources::instance().getMediumRegularButtonFont());
    mBoxLayout->addWidget(helpWidget);

    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(),
                                    qApp->desktop()->availableGeometry()));
    connect(&pingProcess, &QProcess::readyReadStandardOutput, this, [this]{
        this->loadLocalUserInfo();
        qDebug() << "NETWORK REACHABLE => START LOGIN";

    });
    connect(&pingProcess, &QProcess::readyReadStandardError, this,[this]{
        if(!timerCheckNetwork->isActive()) timerCheckNetwork->start(1000);
        mMessageLabel->setText("Vui lòng kiểm tra kết nối mạng!");
    });

    timerCheckNetwork = new QTimer(this);
    timerCheckNetwork->start(1000);
    connect(timerCheckNetwork, &QTimer::timeout, this, &LoginWidget::recheckNetWork);
    startCheckNetWork();
}
void LoginWidget::recheckNetWork(){
    pingProcess.close();
    startCheckNetWork();
}
void LoginWidget::onLoggedOut() {
    User::clearUserInfo();
    mMessageLabel->setText("");
}

void LoginWidget::onLoginClicked() {
    if(!timerCheckNetwork->isActive()){
        timerCheckNetwork->start(1000);
    }
    if (!mIsRequesting) {
        mLoginButton->setEnabled(false);
        mIsRequesting = true;
        mMessageLabel->setText("Đang đăng nhập...");

        this->username = userLineEdit->text().trimmed();
        this->password = NetworkUtils::instance().encodeSHA265(
                    passWordLineEdit->text().trimmed());

        userLineEdit->setText("");
        passWordLineEdit->setText("");
        userLineEdit->setDisabled(true);
        passWordLineEdit->setDisabled(true);

        function<void(QString)> onFailure = [this](QString message) {
            qDebug() << "Login failure!";
            userLineEdit->setEnabled(true);
            passWordLineEdit->setEnabled(true);
            mLoginButton->setEnabled(true);
            mIsRequesting = false;
            mMessageLabel->setText("Đăng nhập lỗi!");
            userLineEdit->setFocus();
            loginFaildCounter++;
            if (loginFaildCounter == 3) {
                this->responseData->setValue(Q_NULLPTR);
                this->close();
                this->accept();
            }
        };

        function<void(User *)> onSuccess = [this](User *user) {
            qDebug() << "Login successfull!";
            qDebug() << "Tên người dùng:" << user->getFullName();
            qDebug() << "Địa chỉ email :" << user->getEmail();
            userLineEdit->setEnabled(true);
            passWordLineEdit->setEnabled(true);
            mLoginButton->setEnabled(true);
            mIsRequesting = false;
            mMessageLabel->setText("Đăng nhập thành công!");
            this->responseData->setValue(user);
            this->close();
            this->accept();
        };

        std::function<void(QJsonObject)> onFetchSuccess =
                [this, onSuccess, onFailure](QJsonObject jsonObject) {
            // duongnt_25_09_2017
            QJsonValue jsonValue;
            jsonValue = jsonObject.take("code");
            if (!jsonValue.isUndefined()) {
                mMessageLabel->setText("Đăng nhập lỗi!");
                onFailure("Đăng nhập lỗi!");
                return;
            }
            // duongnt_25_09_2017 convert user json object to user json string
            QJsonDocument jsonDocument(jsonObject);
            QString jsonString(jsonDocument.toJson(QJsonDocument::Compact));
            userInfo.userData = jsonString;
            User::saveUserInfo(userInfo);
            User *user = User::parse(jsonObject);
            onSuccess(user);
        };

        std::function<void(QString)> onFetchFailure =
                [this, onFailure](QString message) { onFailure(message); };

        QMap<QString, QString> params;
        params["action"] = "login";
        params["password"] = this->password;
        params["device_id"] =  NetworkUtils::instance().getMacAddress();
        params["device_type"] = "videowallpc";

        if (isValidPhone(this->username)) {
            params["phone"] = this->username;
        } else {
            params["username"] = this->username;
        }

        NetworkUtils::instance().getRequest(
                    AppProfile::getAppProfile()->getAppConfig()->getLoginApiUri(), params,
                    onFetchSuccess, onFetchFailure);
    }
}

bool LoginWidget::isValidPhone(QString str) {
    QRegExp re("\\d*");
    return re.exactMatch(str);
}

void LoginWidget::startCheckNetWork(){
    QString hostPing = AppProfile::getAppProfile()->getAppConfig()->getSocketUrl();
    QUrl url = QUrl(hostPing);
    hostPing = url.host();
    // ping with interval 200 ms
    QString exec = "ping";
    QStringList params;
    params << "-c1" << hostPing ;
    // start pinging
    pingProcess.start(exec, params, QIODevice::ReadOnly);
    QCoreApplication::processEvents();
}


void LoginWidget::show() {}

void LoginWidget::loadLocalUserInfo() {
    qDebug() << "LoginWidget -> loadLocalUserInfo";
    userInfo = User::loadUserInfo();
    if (!userInfo.userData.isEmpty()) {
        User *user = User::getUserFrom(userInfo);
        if (user != Q_NULLPTR && !user->getToken().isEmpty() &&
                user->getUserId() > 0) {
            QString tokenSaved = user->getToken();
            std::function<void(QJsonObject)> onFetchSuccess = [this, user](QJsonObject jsonObject){
                mIsRequesting = false;
                if(jsonObject.contains("fullname") == false){
                    //token false
                    //                    success	false
                    //                    message	"Failed to authenticate token."
                    timerCheckNetwork->stop();
                    pingProcess.close();
                    mIsRequesting = false;
                    userLineEdit->setEnabled(true);
                    passWordLineEdit->setEnabled(true);
                    mLoginButton->setEnabled(true);
                    mMessageLabel->setText("Vui lòng nhập thông tin đăng nhập!");
                    userLineEdit->setFocus();
                }else{
                    QJsonValue jsonValue = jsonObject.take("fullname");
                    QString username;
                    int activeState;
                    if(!jsonValue.isNull()){
                        username =  jsonValue.toString();
                    }
                    jsonValue = jsonObject.take("active");
                    if(!jsonValue.isNull()){
                        QString activeString = jsonValue.toString();
                        activeState = activeString.toInt();
                    }
                    qDebug() << "username" << username << activeState << user->getFullName();
                    //token isvalid
                    if(username == user->getFullName() && activeState == 1){
                        mIsRequesting = false;
                        timerCheckNetwork->stop();
                        pingProcess.close();
                        userLineEdit->setEnabled(true);
                        passWordLineEdit->setEnabled(true);
                        mLoginButton->setEnabled(true);
                        mMessageLabel->setText("Đăng nhập thành công!");
                        this->responseData->setValue(user);
                        qDebug() << "đăng nhập thành công";
                        user->log();
                        this->close();
                        this->accept();
                        qDebug() << "FORM LOGIN CLOSE";
                    }else{
                        timerCheckNetwork->stop();
                        pingProcess.close();
                        mIsRequesting = false;
                        userLineEdit->setEnabled(true);
                        passWordLineEdit->setEnabled(true);
                        mLoginButton->setEnabled(true);
                        mMessageLabel->setText("Vui lòng nhập thông tin đăng nhập!");
                        userLineEdit->setFocus();
                    }
                }
            };

            std::function<void(QString)> onFetchFailure = [this](QString message){
                if(message == "Timeout Request"){
                    mMessageLabel->setText("Máy chủ không phản hồi!");
                }else{
                    mMessageLabel->setText("Đăng nhập lỗi!");
                }
                timerCheckNetwork->stop();
                pingProcess.close();
                userLineEdit->setEnabled(true);
                passWordLineEdit->setEnabled(true);
                mLoginButton->setEnabled(true);
                mIsRequesting = false;
                userLineEdit->setFocus();
                qDebug() << "Token login failure";
            };

            if (!mIsRequesting) {
                mLoginButton->setEnabled(false);
                mIsRequesting = true;
                mMessageLabel->setText("Đang đăng nhập...");

                userLineEdit->setText("");
                passWordLineEdit->setText("");
                userLineEdit->setDisabled(true);
                passWordLineEdit->setDisabled(true);

                QMap<QString, QString> params;
                params.insert("token", tokenSaved);
                NetworkUtils::instance().getRequest(10000, API_CHECK_TOKEN, params, onFetchSuccess, onFetchFailure);
            }
        }
    }else{
        //khong co thong tin
        timerCheckNetwork->stop();
        pingProcess.close();
        mIsRequesting = false;
        userLineEdit->setEnabled(true);
        passWordLineEdit->setEnabled(true);
        mLoginButton->setEnabled(true);
        mMessageLabel->setText("Vui lòng nhập thông tin đăng nhập!");
        userLineEdit->setFocus();
    }
}

