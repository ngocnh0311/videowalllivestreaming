#ifndef RESOURCES_H
#define RESOURCES_H

#include <QObject>
#include <QApplication>
#include <QDebug>
#include <QFont>
#include <QDesktopWidget>
#include <QtAwesome.h>
#include <QSettings>
class Resources
{
    public:
        
        static Resources &instance() {
            static Resources mInstance;
            return mInstance;
        }
        
        QtAwesome* getAwesome() const;
        QSize getScreenSize();
        QVariantMap getIconOptions();
        
        Resources(Resources const&) = delete;
        Resources(Resources &&) = delete;
        Resources& operator = (Resources const&) = delete;
        Resources& operator = (Resources &&) = delete;
        
        QFont getExtraSmallRegularButtonFont();
        QFont getExtraSmallBoldButtonFont();
        
        QFont getSmallRegularButtonFont();
        QFont getSmallBoldButtonFont();
        
        QFont getMediumRegularButtonFont();
        QFont getMediumBoldButtonFont();
        
        QFont getLargeRegularButtonFont();
        QFont getLargeBoldButtonFont();
        
        QFont getExtraLargeRegularButtonFont();
        QFont getExtraLargeBoldButtonFont();
        //Label
        QFont getExtraLargeUbuntuRegularLabelFont() const;

        QFont getLargeUbuntuRegularLabelFont() const;

        QFont getMediumUbuntuRegularLabelFont() const;

        QFont getSmallUbuntuRegularLabelFont() const;

        QFont getExtraBoldLargeUbuntuRegularLabelFont() const;

        QFont getLargeBoldUbuntuRegularLabelFont() const;

        QFont getMediumBoldUbuntuRegularLabelFont() const;

        QFont getSmallBoldUbuntuRegularLabelFont() const;

private:
        QtAwesome *mAwesome;
        QSize mScreenSize;
        QVariantMap mIconOptions;
        QFont mExtraLargeRegularButtonFont;
        QFont mExtraLargeBoldButtonFont;
        QFont mLargeRegularButtonFont;
        QFont mLargeBoldButtonFont;
        QFont mMediumRegularButtonFont;
        QFont mMediumBoldButtonFont;
        QFont mSmallRegularButtonFont;
        QFont mSmallBoldButtonFont;
        QFont mExtraSmallRegularButtonFont;
        QFont mExtraSmallBoldButtonFont;

        //label
        QFont mExtraLargeUbuntuRegularLabelFont;
        QFont mLargeUbuntuRegularLabelFont;
        QFont mMediumUbuntuRegularLabelFont;
        QFont mSmallUbuntuRegularLabelFont;


        QFont mExtraBoldLargeUbuntuRegularLabelFont;
        QFont mLargeBoldUbuntuRegularLabelFont;
        QFont mMediumBoldUbuntuRegularLabelFont;
        QFont mSmallBoldUbuntuRegularLabelFont;

    protected:
        Resources();
        
};

#endif // RESOURCES_H
