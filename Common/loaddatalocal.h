#ifndef LOADDATALOCAL_H
#define LOADDATALOCAL_H

#include <QObject>
#include <QJsonObject>
#include "appprofile.h"
class LoadDataLocal : public QObject
{
    Q_OBJECT
public:
public:
    static QString K_FOLDER;
    static QString K_FILE;

    static LoadDataLocal &instance() {
        static LoadDataLocal mInstance;
        return mInstance;
    }

    LoadDataLocal();
    void saveDataCamerasToDisk(QJsonObject jsonObject);
    QJsonObject loadDataCamerasFromDisk();


    //device
    QJsonObject loadDataCamerasDeviceFromDisk();
    void saveDataCamerasOfDeviceToDisk(QJsonObject jsonObject);

    //site
    void saveDataCamerasOfSiteToDisk(int siteId ,QJsonObject jsonObject);
    QJsonObject loadDataCamerasOfSiteToDisk(int siteId);
    //save list site to disk
    void saveListUserSitesToDisk(QJsonObject jsonObject);
    QJsonObject loadListUserSitesFromDisk();
    void clearDataLocal();
    bool checkFileData();

};

#endif // LOADDATALOCAL_H
