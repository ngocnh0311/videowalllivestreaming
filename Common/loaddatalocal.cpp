#include "loaddatalocal.h"

QString LoadDataLocal::K_FOLDER = "/.videowall/Data/";
LoadDataLocal::LoadDataLocal()
{

}

void LoadDataLocal::clearDataLocal(){
    QString path = QDir::homePath();
    path = path.append(K_FOLDER);
    if (!QDir(path).exists()) {
      QDir().mkpath(path);
    }

    QDir dir(path);
    dir.removeRecursively();
}

void LoadDataLocal::saveDataCamerasOfDeviceToDisk(QJsonObject jsonObject){
    Folders folders;
    Files files;
    QString rootPath = QDir::homePath();
    rootPath.append(folders.root);
    rootPath.append(folders.data);
    rootPath.append(folders.device);
    rootPath.append(folders.dataCameras);

    if (!QDir(rootPath).exists()) {
        QDir().mkpath(rootPath);
    }

    QString filePath = rootPath += files.dataCameras;
    qDebug() <<Q_FUNC_INFO<< "filePath" <<filePath;
    QFile file(filePath);
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    QJsonDocument document(jsonObject);
    QString jsonString = QString(document.toJson(QJsonDocument::Compact));
    QTextStream(&file) << jsonString;
    file.close();
}

QJsonObject LoadDataLocal::loadDataCamerasDeviceFromDisk(){
    QJsonObject jsonObjectResult;
    Folders folders;
    Files files;
    QString rootPath = QDir::homePath();
    rootPath.append(folders.root);
    rootPath.append(folders.data);
    rootPath.append(folders.device);
    rootPath.append(folders.dataCameras);

    if (!QDir(rootPath).exists()) {
        QDir().mkpath(rootPath);
    }
    QString filePath = rootPath += files.dataCameras;
    QFile file(filePath);
    if (file.exists()) {
        file.open(QIODevice::ReadOnly);
        QString data = QTextStream(&file).readAll();
        file.close();
        if (data.size() > 0) {
            QJsonDocument jsonDocument = QJsonDocument::fromJson(data.toUtf8());
            jsonObjectResult = jsonDocument.object();
            return jsonObjectResult;
        }
    }else{
        QString dataError = "Not Exists";
        QJsonDocument jsonDocument = QJsonDocument::fromJson(dataError.toUtf8());
        jsonObjectResult = jsonDocument.object();
        return jsonObjectResult;
    }
    return jsonObjectResult;
}

bool LoadDataLocal::checkFileData(){
    Folders folders;
    Files files;
    QString rootPath = QDir::homePath();
    rootPath.append(folders.root);
    rootPath.append(folders.data);
    rootPath.append(folders.device);
    rootPath.append(folders.dataCameras);

    if (!QDir(rootPath).exists()) {
        QDir().mkpath(rootPath);
    }
    QString filePath = rootPath += files.dataCameras;
    QFile file(filePath);
    if (file.exists()) {
        return true;
    }
    return false;
}

QJsonObject LoadDataLocal::loadListUserSitesFromDisk(){
    QJsonObject jsonObjectResult;
    Folders folders;
    Files files;
    QString rootPath = QDir::homePath();
    rootPath.append(folders.root);
    rootPath.append(folders.data);
    rootPath.append(folders.site);
    rootPath.append(folders.listSites);

    if (!QDir(rootPath).exists()) {
        QDir().mkpath(rootPath);
    }
    QString filePath = rootPath += files.dataSites;
    qDebug() <<"loadListUserSitesFromDisk"<< "filePath" <<filePath;

    QFile file(filePath);
    if (file.exists()) {
        file.open(QIODevice::ReadOnly);
        QString data = QTextStream(&file).readAll();
        file.close();
        if (data.size() > 0) {
            QJsonDocument jsonDocument = QJsonDocument::fromJson(data.toUtf8());
            jsonObjectResult = jsonDocument.object();
            return jsonObjectResult;
        }
    }else{
        QString dataError = "Not Exists";
        QJsonDocument jsonDocument = QJsonDocument::fromJson(dataError.toUtf8());
        jsonObjectResult = jsonDocument.object();
        return jsonObjectResult;
    }
    return jsonObjectResult;
}


void LoadDataLocal::saveListUserSitesToDisk(QJsonObject jsonObject){
    Folders folders;
    Files files;
    QString rootPath = QDir::homePath();
    rootPath.append(folders.root);
    rootPath.append(folders.data);
    rootPath.append(folders.site);
    rootPath.append(folders.listSites);

    if (!QDir(rootPath).exists()) {
        QDir().mkpath(rootPath);
    }

    QString filePath = rootPath += files.dataSites;
    qDebug() <<"saveListUserSitesToDisk"<< "filePath" <<filePath;
    QFile file(filePath);
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    QJsonDocument document(jsonObject);
    QString jsonString = QString(document.toJson(QJsonDocument::Compact));
    QTextStream(&file) << jsonString;
    file.close();
}



void LoadDataLocal::saveDataCamerasOfSiteToDisk(int siteId ,QJsonObject jsonObject){
    Folders folders;
    Files files;
    QString rootPath = QDir::homePath();
    rootPath.append(folders.root);
    rootPath.append(folders.data);
    rootPath.append(folders.site);

    rootPath.append(folders.dataCamerasOfSites);
    if (!QDir(rootPath).exists()) {
        QDir().mkpath(rootPath);
    }

    QString filePath = rootPath += "/camera_of_site_" + QString::number(siteId);
    qDebug() <<Q_FUNC_INFO<< "filePath" <<filePath;
    QFile file(filePath);
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    QJsonDocument document(jsonObject);
    QString jsonString = QString(document.toJson(QJsonDocument::Compact));
    QTextStream(&file) << jsonString;
    file.close();
}



QJsonObject LoadDataLocal::loadDataCamerasOfSiteToDisk(int siteId){
    QJsonObject jsonObjectResult;
    Folders folders;
    Files files;
    QString rootPath = QDir::homePath();
    rootPath.append(folders.root);
    rootPath.append(folders.data);

    rootPath.append(folders.site);
    rootPath.append(folders.dataCamerasOfSites);
    if (!QDir(rootPath).exists()) {
        QDir().mkpath(rootPath);
    }
    QString filePath = rootPath += "/camera_of_site_" + QString::number(siteId);
    QFile file(filePath);
    if (file.exists()) {
        file.open(QIODevice::ReadOnly);
        QString data = QTextStream(&file).readAll();
        file.close();
        if (data.size() > 0) {
            QJsonDocument jsonDocument = QJsonDocument::fromJson(data.toUtf8());
            jsonObjectResult = jsonDocument.object();
            return jsonObjectResult;
        }
    }else{
        QString dataError = "Not Exists";
        QJsonDocument jsonDocument = QJsonDocument::fromJson(dataError.toUtf8());
        jsonObjectResult = jsonDocument.object();
        return jsonObjectResult;
    }
    return jsonObjectResult;
}
