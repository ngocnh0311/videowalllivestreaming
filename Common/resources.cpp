
#include "resources.h"

Resources::Resources()
{
    mAwesome = new QtAwesome(qApp);
    mAwesome->initFontAwesome();


    QSettings settings;

    QString isShowWindowsList = settings.value("show_windows_list").toString();
    if(isShowWindowsList == "HIDE"){
        mScreenSize = QApplication::desktop()->screenGeometry().size();
    }else{
        //show
        mScreenSize = QApplication::desktop()->availableGeometry(0).size();
    }
    mIconOptions.insert("color", QColor(255, 255, 255));
    mIconOptions.insert("color-disabled", QColor(255, 255, 255));
    mIconOptions.insert("color-active", QColor(255, 255, 255));
    mIconOptions.insert("color-selected", QColor(255, 255, 255));
    
    mExtraLargeRegularButtonFont = mAwesome->font(17);
    mExtraLargeBoldButtonFont = mAwesome->font(17);
    mExtraLargeBoldButtonFont.setBold(true);
    
    mLargeRegularButtonFont = mAwesome->font(15);
    mLargeBoldButtonFont = mAwesome->font(15);
    mLargeBoldButtonFont.setBold(true);
    
    mMediumRegularButtonFont = mAwesome->font(13);
    mMediumBoldButtonFont = mAwesome->font(13);
    mMediumBoldButtonFont.setBold(true);
    
    mSmallRegularButtonFont = mAwesome->font(12);
    mSmallBoldButtonFont = mAwesome->font(12);
    mSmallBoldButtonFont.setBold(true);


    mExtraLargeUbuntuRegularLabelFont = QFont("Ubuntu Regular", 17, QFont::Normal);
    mLargeUbuntuRegularLabelFont = QFont("Ubuntu Regular", 15, QFont::Normal);
    mMediumUbuntuRegularLabelFont = QFont("Ubuntu Regular", 13, QFont::Normal);
    mSmallUbuntuRegularLabelFont = QFont("Ubuntu Regular", 12, QFont::Normal);


    mExtraBoldLargeUbuntuRegularLabelFont = QFont("Ubuntu Regular", 17, QFont::Bold);
    mLargeBoldUbuntuRegularLabelFont = QFont("Ubuntu Regular", 15, QFont::Bold);
    mMediumBoldUbuntuRegularLabelFont = QFont("Ubuntu Regular", 13, QFont::Bold);
    mSmallBoldUbuntuRegularLabelFont = QFont("Ubuntu Regular", 12, QFont::Bold);
    
}

QVariantMap Resources::getIconOptions() {
    return mIconOptions;
}


QtAwesome* Resources::getAwesome() const {
    return mAwesome;
}


QSize Resources::getScreenSize() {
    return mScreenSize;
}

QFont Resources::getExtraLargeBoldButtonFont() {
    return mExtraLargeBoldButtonFont;
}

//label
QFont Resources::getExtraLargeUbuntuRegularLabelFont() const
{
    return mExtraLargeUbuntuRegularLabelFont;
}

QFont Resources::getLargeUbuntuRegularLabelFont() const
{
    return mLargeUbuntuRegularLabelFont;
}

QFont Resources::getMediumUbuntuRegularLabelFont() const
{
    return mMediumUbuntuRegularLabelFont;
}

QFont Resources::getSmallUbuntuRegularLabelFont() const
{
    return mSmallUbuntuRegularLabelFont;
}

QFont Resources::getExtraBoldLargeUbuntuRegularLabelFont() const
{
    return mExtraBoldLargeUbuntuRegularLabelFont;
}

QFont Resources::getLargeBoldUbuntuRegularLabelFont() const
{
    return mLargeBoldUbuntuRegularLabelFont;
}

QFont Resources::getMediumBoldUbuntuRegularLabelFont() const
{
    return mMediumBoldUbuntuRegularLabelFont;
}

QFont Resources::getSmallBoldUbuntuRegularLabelFont() const
{
    return mSmallBoldUbuntuRegularLabelFont;
}

QFont Resources::getExtraLargeRegularButtonFont() {
    return mExtraLargeRegularButtonFont;
}

QFont Resources::getLargeBoldButtonFont() {
    return mLargeBoldButtonFont;
}

QFont Resources::getLargeRegularButtonFont() {
    return mLargeRegularButtonFont;
}

QFont Resources::getMediumBoldButtonFont() {
    return mMediumBoldButtonFont;
}

QFont Resources::getMediumRegularButtonFont() {
    return mMediumRegularButtonFont;
}

QFont Resources::getSmallBoldButtonFont() {
    return mSmallBoldButtonFont;
}

QFont Resources::getSmallRegularButtonFont() {
    return mSmallRegularButtonFont;
}

QFont Resources::getExtraSmallBoldButtonFont() {
    return mExtraSmallBoldButtonFont;
}

QFont Resources::getExtraSmallRegularButtonFont() {
    return mExtraSmallRegularButtonFont;
}


