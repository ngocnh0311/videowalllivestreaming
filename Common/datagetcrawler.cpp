#include "datagetcrawler.h"

DataGetCrawler::DataGetCrawler(CrawlerType _crawlerType, QString _keySubcriable, C_CrawlerCamera *_crawlerCamera){
    this->crawlerType = _crawlerType;
    crawlerType.cameraId = _crawlerType.cameraId;
    crawlerType.protocol = _crawlerType.protocol;
    crawlerType.network = _crawlerType.network;
    crawlerType.channelName = _crawlerType.channelName;
    crawlerType.dataSource = _crawlerType.dataSource;

    this->keySubcriable = _keySubcriable ;
    this->pCrawlerCamera = _crawlerCamera;
}

DataGetCrawler::DataGetCrawler(){

}

//DataStopCrawler
DataStopCrawler::DataStopCrawler(){

}

DataStopCrawler::DataStopCrawler(CrawlerType _crawlerType, QString _keyUnSubcriable)
{
    this->crawlerType = _crawlerType;
    crawlerType.cameraId = _crawlerType.cameraId;
    crawlerType.protocol = _crawlerType.protocol;
    crawlerType.network = _crawlerType.network;
    crawlerType.channelName = _crawlerType.channelName;
    crawlerType.dataSource = _crawlerType.dataSource;

    this->keyUnSubcriable = _keyUnSubcriable;
}
