#include "appconfig.h"

QString AppConfig::getSocketUrl() const { return mSocketUrl; }

void AppConfig::setSocketUrl(const QString &socketUrl) {
    mSocketUrl = socketUrl;
}

QString AppConfig::getVideowallModuleId() const
{
    return mVideowallModuleId;
}

void AppConfig::setVideowallModuleId(const QString &videowallModuleId)
{
    mVideowallModuleId = videowallModuleId;
}

QString AppConfig::getPathVideowallPid() const
{
    return mPathVideowallPid;
}

void AppConfig::setPathVideowallPid(const QString &pathVideowallPid)
{
    mPathVideowallPid = pathVideowallPid;
}

AppConfig::AppConfig(QObject *parent) : QObject(parent) {}

AppConfig *AppConfig::cloneAppConfig() {
    AppConfig *appConfig = new AppConfig(Q_NULLPTR);
    appConfig->mSocketUrl = mSocketUrl;
    appConfig->mBaseApiUrl = mBaseApiUrl;
    appConfig->mVideowallModuleId = mVideowallModuleId;
    appConfig->mPathVideowallPid = mPathVideowallPid;
    appConfig->mLoginApiUri = mLoginApiUri;
    appConfig->mTokenApiUri = mTokenApiUri;
    appConfig->mCamSiteByDeviceApiUri = mCamSiteByDeviceApiUri;
    appConfig->mCamSiteBySiteApiUri = mCamSiteBySiteApiUri;
    appConfig->mChangePasswordApiUri = mChangePasswordApiUri;
    appConfig->mBrandName = mBrandName;
    appConfig->mApiVodVersion = mApiVodVersion;
    return appConfig;
}

AppConfig *AppConfig::defaultAppConfig() {
    AppConfig *appConfig = new AppConfig(Q_NULLPTR);
    appConfig->mSocketUrl = "";
    appConfig->mBaseApiUrl = "";
    appConfig->mVideowallModuleId = "";
    appConfig->mPathVideowallPid = "";
    appConfig->mLoginApiUri = "/login";
    appConfig->mTokenApiUri = "/getToken";
    appConfig->mCamSiteByDeviceApiUri = "/cms_api/getAllCamOfSiteV2";
    appConfig->mCamSiteBySiteApiUri = "/cms_api/getCamera";
    appConfig->mChangePasswordApiUri = "/cms_api/updateUserPassword";
    appConfig->mBrandName = "VCAM";
    appConfig->mApiVodVersion = "3.0";
    return appConfig;
}

AppConfig *AppConfig::parseFrom(QJsonObject jsonObject) {
    AppConfig *appConfig = new AppConfig(NULL);
    appConfig->mSocketUrl = jsonObject.take("socket_url").toString("");
    appConfig->mBaseApiUrl = jsonObject.take("base_api_url").toString("");
    appConfig->mVideowallModuleId = jsonObject.take("videowall_module_id").toString("");
    appConfig->mPathVideowallPid = jsonObject.take("path_videowall_pid").toString();
    appConfig->mLoginApiUri = jsonObject.take("login_api_uri").toString("");
    appConfig->mTokenApiUri = jsonObject.take("token_api_uri").toString("");
    appConfig->mCamSiteByDeviceApiUri =
            jsonObject.take("cam_site_by_device_api_uri").toString("");
    appConfig->mChangePasswordApiUri = jsonObject.take("change_password_uri").toString("");
    appConfig->mCamSiteBySiteApiUri =
            jsonObject.take("cam_site_by_site_api_uri").toString("");
    appConfig->mBrandName = jsonObject.take("brand_name").toString("");
    appConfig->mApiVodVersion = jsonObject.take("api_vod_version").toString("");
    return appConfig;
}

QJsonObject AppConfig::toJsonObject() {
    QJsonObject jsonObject;
    jsonObject.insert("socket_url", mSocketUrl);
    jsonObject.insert("base_api_url", mBaseApiUrl);
    jsonObject.insert("videowall_module_id", mVideowallModuleId);
    jsonObject.insert("path_videowall_pid", mPathVideowallPid);
    jsonObject.insert("login_api_uri", mLoginApiUri);
    jsonObject.insert("token_api_uri", mTokenApiUri);
    jsonObject.insert("cam_site_by_device_api_uri", mCamSiteByDeviceApiUri);
    jsonObject.insert("cam_site_by_site_api_uri", mCamSiteBySiteApiUri);
    jsonObject.insert("change_password_uri", mChangePasswordApiUri);
    jsonObject.insert("brand_name", mBrandName);
    jsonObject.insert("api_vod_version", mApiVodVersion);
    return jsonObject;
}

QString AppConfig::getBaseApiUrl() const { return mBaseApiUrl; }

void AppConfig::setBaseApiUrl(QString baseApiUrl) { mBaseApiUrl = baseApiUrl; }

QString AppConfig::getCamSiteByDeviceApiUri() const {
    return mCamSiteByDeviceApiUri;
}

void AppConfig::setCamSiteByDeviceApiUri(QString camSiteByDeviceApiUri) {
    mCamSiteByDeviceApiUri = camSiteByDeviceApiUri;
}

QString AppConfig::getCamSiteBySiteApiUri() const {
    return mCamSiteBySiteApiUri;
}

void AppConfig::setCamSiteBySiteApiUri(QString camSiteBySiteApiUri) {
    mCamSiteBySiteApiUri = camSiteBySiteApiUri;
}

QString AppConfig::getLoginApiUri() const { return mLoginApiUri; }

void AppConfig::setLoginApiUri(QString loginApiUri) {
    mLoginApiUri = loginApiUri;
}

QString AppConfig::getTokenApiUri() const { return mTokenApiUri; }

void AppConfig::setTokenApiUri(QString tokenApiUri) {
    mTokenApiUri = tokenApiUri;
}

void AppConfig::setChangePasswordApiUri(QString uri) {
    mChangePasswordApiUri = uri;
}

QString AppConfig::getChangePasswordApiUri() const {
    return mChangePasswordApiUri;
}

void AppConfig::setBrandName(QString brandName) {
    this->mBrandName = brandName;
}
QString AppConfig::getBrandName() { return mBrandName; }

void AppConfig::setApiVodVersion(QString apiVersion) {
    mApiVodVersion = apiVersion;
}
QString AppConfig::getApiVodVersion() { return mApiVodVersion; }
