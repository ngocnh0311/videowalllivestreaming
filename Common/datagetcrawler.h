#ifndef DATAGETCRAWLER_H
#define DATAGETCRAWLER_H
#include "Camera/camitem.h"
#include "Player/Cam9RTCPlayer/c_cam9rtcplayer.h"
#include <QObject>
#include "SensorCameraManager/SensorCamera/CrawlerCamera/c_crawlercamera.h"
class C_Cam9RTCPlayer;
class DataGetCrawler : public QObject{
    Q_OBJECT
public:
  CrawlerType crawlerType;
  QString keySubcriable;
  C_CrawlerCamera *pCrawlerCamera = Q_NULLPTR;
  DataGetCrawler();
  DataGetCrawler (CrawlerType _crawlerType, QString _keySubcriable, C_CrawlerCamera *_crawlerCamera);

};

class DataStopCrawler: public QObject{
    Q_OBJECT
public:
    CrawlerType crawlerType;
    QString keyUnSubcriable;
    DataStopCrawler();
    DataStopCrawler(CrawlerType _crawlerType, QString _keyUnSubcriable);
};
#endif // DATAGETCRAWLER_H
