#include "camsite.h"

CamSite::CamSite(QObject *parent) : QObject(parent) {}

void CamSite::setLayout(int layout) { this->layout = layout; }

void CamSite::setPlayPage(int page) { this->playPage = page; }

int CamSite::getSumChannel() { return sumChannel; }

void CamSite::setSumChannel(int sumChannel) { this->sumChannel = sumChannel; }

int CamSite::getLayout() { return layout; }

int CamSite::getPlayPage() { return playPage; }

int CamSite::getPlayCamFullScreen() { return playCamFullscreen; }

QList<CamItem *> CamSite::getCamItems() { return camItems; }

void CamSite::setCamItems(QList<CamItem *> newCamItems) {
    camItems = newCamItems;
}

CamSite *CamSite::parseCamItems(QJsonObject jsonObject) {
    CamSite *camSite = new CamSite(NULL);
    QJsonValue jsonValue;

    jsonValue = jsonObject.take("sumchannel");
    if (!jsonValue.isNull()) {
        camSite->sumChannel = jsonValue.toInt();
    }

    jsonValue = jsonObject.take("layout");
    if (!jsonValue.isNull()) {
        camSite->layout = jsonValue.toInt();
    }

    jsonValue = jsonObject.take("play_page");
    if (!jsonValue.isNull()) {
        camSite->playPage = jsonValue.toInt();
    }

    jsonValue = jsonObject.take("play_cam_fullscreen");
    if (!jsonValue.isNull()) {
        camSite->playCamFullscreen = jsonValue.toInt();
    }

    jsonValue = jsonObject.take("TOTAL");
    if (!jsonValue.isNull()) {
        camSite->totalCamItem = jsonValue.toInt();
    }

    jsonValue = jsonObject.take("PER_PAGE");
    if (!jsonValue.isNull()) {
        camSite->perpageCamItem = jsonValue.toInt();
    }

    QJsonArray camJsonArray =
            jsonObject["CAMS_LIST"].toArray();

    camSite->camItems.clear();

    for (int index = 0; index < camJsonArray.size(); index++) {
        QJsonObject camJsonObject = camJsonArray[index].toObject();
        CamItem *camItem = CamItem::parse(camSite, camJsonObject);
        if(camItem != Q_NULLPTR) {
            camSite->camItems.append(camItem);
        }else{
            camSite->camItems.append(Q_NULLPTR);
        }
    }


    //    for (int index = 0; index < camSite->camItems.size(); ++index) {
    //        CamItem *camItem = camSite->camItems.at(index);
    //        if (camItem != Q_NULLPTR) {
    //            qDebug() << "Cam Item" << index << camItem->getCameraOrder()
    //                     << camItem->getCameraId() << camItem->getPostion();
    //        }
    //    }

    return camSite;
}

CamSite *CamSite::parseWithOrder(bool withOrder, QJsonObject jsonObject) {
    CamSite *camSite = new CamSite(NULL);
    QJsonValue jsonValue;

    if(jsonObject.contains("sumchannel") == false){
        //If json null
        camSite->sumChannel = -1;
        camSite->layout = -1;
        camSite->playPage = -1;
        camSite->playCamFullscreen = -1;
        camSite->totalCamItem = -1;
        camSite->perpageCamItem = -1;
    }


    jsonValue = jsonObject.take("maxcamera_display");
    if (!jsonValue.isNull()) {
        int value = jsonValue.toInt(0);
        camSite->maxCameraDisplay = value;
    }

    jsonValue = jsonObject.take("sumchannel");
    if (!jsonValue.isNull()) {
        camSite->sumChannel = jsonValue.toInt();
    }

    jsonValue = jsonObject.take("layout");
    if (!jsonValue.isNull()) {
        camSite->layout = jsonValue.toInt();
    }

    jsonValue = jsonObject.take("play_page");
    if (!jsonValue.isNull()) {
        camSite->playPage = jsonValue.toInt();
    }

    jsonValue = jsonObject.take("play_cam_fullscreen");
    if (!jsonValue.isNull()) {
        camSite->playCamFullscreen = jsonValue.toInt();
    }

    jsonValue = jsonObject.take("TOTAL");
    if (!jsonValue.isNull()) {
        camSite->totalCamItem = jsonValue.toInt();
    }


    jsonValue = jsonObject.take("PER_PAGE");
    if (!jsonValue.isNull()) {
        camSite->perpageCamItem = jsonValue.toInt();
    }


    QJsonArray camJsonArray =
            jsonObject[withOrder ? "cams_list" : "CAMS_LIST"].toArray();
    camSite->camItems.clear();

    if (!withOrder) {
        camSite->setSumChannel(camJsonArray.size());
        int maxLayout = 25;
        if (maxLayout < 16) {
            camSite->setLayout(maxLayout);
        } else {
            camSite->setLayout(16);
        }
        camSite->setPlayPage(1);
    }

    for (int index = 0; index < camJsonArray.size(); ++index) {
        camSite->camItems.append(NULL);
    }
    qDebug() << "camJsonArray.size()" << camJsonArray.size();
    for (int index = 0; index < camJsonArray.size(); index++) {
        QJsonObject camJsonObject = camJsonArray[index].toObject();
        CamItem *camItem = CamItem::parse(camSite, camJsonObject);
        if (!withOrder) {
            camItem->setOrder(index);
        }
        //        if(orderCam >= camSite->camItems.size()) continue;
        camSite->camItems.replace(index, camItem);
    }
    int totalCam = camSite->camItems.size();
    if (withOrder) {
        for (int i = 0; i < totalCam - 1; ++i) {
            for (int j = i + 1; j < totalCam; ++j) {
                CamItem *item0 = camSite->camItems.at(i);
                CamItem *item1 = camSite->camItems.at(j);
                if (QString::compare(item0->getPostion(), item1->getPostion(),
                                     Qt::CaseInsensitive) > 0 && (item0->getOrder() > item1->getOrder())) {
                    camSite->camItems.swap(i, j);
                }
            }
        }
    }

    for (int index = 0; index < totalCam; ++index) {
        CamItem *camItem = camSite->camItems.at(index);
        camItem->setOrder(index);
    }

    if (!withOrder) {
        for (int i = 0; i < totalCam - 1; ++i) {
            for (int j = i + 1; j < totalCam; ++j) {
                CamItem *item0 = camSite->camItems.at(i);
                CamItem *item1 = camSite->camItems.at(j);
                if (QString::compare(item0->getPostion(), item1->getPostion(),
                                     Qt::CaseInsensitive) > 0) {
                    camSite->camItems.swap(i, j);
                    item0->setOrder(j);
                    item1->setOrder(i);
                }
            }
        }
    }

    //    for (int index = 0; index < camSite->camItems.size(); ++index) {
    //        CamItem *camItem = camSite->camItems.at(index);
    //        if (camItem != Q_NULLPTR) {
    //            qDebug() << "Cam Item Sort" << index << camItem->getOrder()
    //                     << camItem->getCameraId() << camItem->getPostion();
    //        }
    //    }

    return camSite;
}


void CamSite::getCamerasWithDeviceId(QString token, std::function<void(CamSite *)> onSuccess,
                                     std::function<void(QString)> onFailure) {
    QSettings settings;
    QString offlineMode = settings.value("offline_mode").toString();
    std::function<void(QJsonObject)> onFetchSuccess =
            [onSuccess, onFailure](QJsonObject jsonObject) {
        //        qDebug() << Q_FUNC_INFO << jsonObject;
        //        code	403
        //        success	false
        //        message	"Permission denied."

        QJsonValue jsonValue;
        int code = 0;
        bool success = false;
        QString message = "";
        jsonValue = jsonObject.take("code");
        if(!jsonValue.isNull()){
            code  = jsonValue.toInt();
        }
        jsonValue = jsonObject.take("success");
        if(!jsonValue.isNull()){
            success = jsonValue.toBool();
        }

        jsonValue = jsonObject.take("message");
        if(!jsonValue.isNull()){
            message = jsonValue.toString();
        }

        QString permisionDenied = "Permission denied.";
        if(code == 403 && success == false && message == permisionDenied){
            onFailure(message);
        }else{
            CamSite *camSite = CamSite::parseWithOrder(true, jsonObject);
            //save data to local
            LoadDataLocal::instance().saveDataCamerasOfDeviceToDisk(jsonObject);
            onSuccess(camSite);
        }
    };

    std::function<void(QString)> onFetchFailure = [onFailure](QString message) {
        onFailure(message);
    };
    QMap<QString, QString> params;
    params["device_id"] = NetworkUtils::instance().getMacAddress();
    //    params["device_id"] = "4c:cc:6a:bc:b0:29";
    params["token"] = token;

    if(offlineMode != "ON"){
        NetworkUtils::instance().getRequest(5000, AppProfile::getAppProfile()->getAppConfig()->getCamSiteByDeviceApiUri(),
                                            params, onFetchSuccess, onFetchFailure);
    }

    if(offlineMode == "ON"){
        NetworkUtils::instance().getDataCamerasDeviceLocal(onFetchSuccess, onFetchFailure);
    }
}


void CamSite::updateCamerasWithDeviceId(QString token, std::function<void(CamSite *)> onSuccess,
                                        std::function<void(QString)> onFailure) {
    QSettings settings;
    QString offlineMode = settings.value("offline_mode").toString();
    std::function<void(QJsonObject)> onFetchSuccess =
            [onSuccess, onFailure](QJsonObject jsonObject) {
        //        qDebug() << Q_FUNC_INFO << jsonObject;
        //        code	403
        //        success	false
        //        message	"Permission denied."

        QJsonValue jsonValue;
        int code = 0;
        bool success = false;
        QString message = "";
        jsonValue = jsonObject.take("code");
        if(!jsonValue.isNull()){
            code  = jsonValue.toInt();
        }
        jsonValue = jsonObject.take("success");
        if(!jsonValue.isNull()){
            success = jsonValue.toBool();
        }

        jsonValue = jsonObject.take("message");
        if(!jsonValue.isNull()){
            message = jsonValue.toString();
        }

        QString permisionDenied = "Permission denied.";
        if(code == 403 && success == false && message == permisionDenied){
            onFailure(message);
        }else{
            //if jsonObject null is set default camsite
            CamSite *camSite = CamSite::parseWithOrder(true, jsonObject);
            //save data to local
            LoadDataLocal::instance().saveDataCamerasOfDeviceToDisk(jsonObject);
            onSuccess(camSite);
        }
    };

    std::function<void(QString)> onFetchFailure = [onFailure](QString message) {
        onFailure(message);
    };
    QMap<QString, QString> params;
    params["device_id"] = NetworkUtils::instance().getMacAddress();
    //    params["device_id"] = "4c:cc:6a:bc:b0:29";
    params["token"] = token;
    NetworkUtils::instance().getRequest(5000, AppProfile::getAppProfile()->getAppConfig()->getCamSiteByDeviceApiUri(),
                                        params, onFetchSuccess, onFetchFailure);

}


int CamSite::getTotalCamItem() const { return totalCamItem; }

void CamSite::setTotalCamItem(int value) { totalCamItem = value; }

int CamSite::getMaxCameraDisplay() const
{
    return maxCameraDisplay;
}

void CamSite::setMaxCameraDisplay(int value)
{
    maxCameraDisplay = value;
}

void CamSite::getCamerasOfSite(int siteId, QString token,
                               std::function<void(QJsonObject)> onSuccess,
                               std::function<void(QString)> onFailure) {
    QSettings settings;
    QString offlineMode = settings.value("offline_mode").toString();
    std::function<void(QJsonObject)> onFetchSuccess =
            [onSuccess](QJsonObject jsonObject) {
        if (!jsonObject.isEmpty()) {
            //save data to local
            QSettings settings;
            QString offlineMode = settings.value("offline_mode").toString();
            onSuccess(jsonObject);
        }else{

        }
    };
    std::function<void(QString)> onFetchFailure = [onFailure](QString message) {
        onFailure(message);
    };
    QMap<QString, QString> params;
    params["site_id"] = QString::number(siteId);
    params["version"] = "003";
    params["token"] = token;

    if(offlineMode != "ON"){
        NetworkUtils::instance().getRequest("/cms_api/getCamera", params,
                                            onFetchSuccess, onFetchFailure);
    }

    if(offlineMode == "ON"){
        NetworkUtils::instance().getDataCamerasSiteLocal(siteId, onFetchSuccess, onFetchFailure);
    }
}

void CamSite::getCamerasOfSiteSaveToLocal(int siteId, QString token,
                                          std::function<void(QJsonObject)> onSuccess,
                                          std::function<void(QString)> onFailure) {
    QSettings settings;
    QString offlineMode = settings.value("offline_mode").toString();
    std::function<void(QJsonObject)> onFetchSuccess =
            [onSuccess](QJsonObject jsonObject) {
        onSuccess(jsonObject);
    };
    std::function<void(QString)> onFetchFailure = [onFailure](QString message) {
        onFailure(message);
    };

    QMap<QString, QString> params;
    params["site_id"] = QString::number(siteId);
    params["version"] = "003";
    params["token"] = token;
    NetworkUtils::instance().getRequest("/cms_api/getCamera", params,
                                        onFetchSuccess, onFetchFailure);
}

void CamSite::getCamerasOfSiteWithLayoutPage(int siteId, QString token, int page,
                                             int layout,
                                             std::function<void(CamSite *)> onSuccess,
                                             std::function<void(QString)> onFailure) {

    std::function<void(QJsonObject)> onFetchSuccess =
            [onSuccess](QJsonObject jsonObject) {
        CamSite *camSite = CamSite::parseCamItems(jsonObject);
        onSuccess(camSite);
    };

    std::function<void(QString)> onFetchFailure = [onFailure](QString message) {
        onFailure(message);
    };

    QMap<QString, QString> params;
    params["site_id"] = QString::number(siteId);
    params["p"] = QString::number(page);
    params["c"] = QString::number(layout);
    params["version"] = "004";
    params["token"] = token;
    NetworkUtils::instance().getRequest("/cms_api/getCamera", params,
                                        onFetchSuccess, onFetchFailure);
}
//load data with layout page local
void CamSite::getCamerasOfSiteWithLayoutPageLocal(int siteId, int page,
                                                  int layout,
                                                  std::function<void(CamSite *)> onSuccess,
                                                  std::function<void(QString)> onFailure) {
    //    std::function<void(QJsonObject)> onFetchSuccess =
    //            [onSuccess](QJsonObject jsonObject) {
    //        CamSite *camSite = CamSite::parseCamItems(jsonObject);
    //        onSuccess(camSite);
    //    };

    //    std::function<void(QString)> onFetchFailure = [onFailure](QString message) {
    //        onFailure(message);
    //    };

    // NetworkUtils::instance().getDataCamerasSiteLocal();
}

QString CamSite::toString() {
    QString str = "";
    return str;
}

QList<CamItem *> CamSite::getPageCameras(LayoutStruct layout) {
    QList<CamItem *> cameras;
    int beginPage = (layout.selectedPage - 1) * layout.numberOfCameras;
    if (beginPage >= 0) {
        for (int index = beginPage; (index < beginPage + layout.numberOfCameras) &&
             (index < camItems.size());
             ++index) {
            cameras.append(camItems.at(index));
        }
    }
    return cameras;
}


