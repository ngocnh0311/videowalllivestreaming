#ifndef CONSTS_H
#define CONSTS_H


#include <QString>
#include <QDebug>

struct NALUnitType {

    const static int CODED_SLICE_OF_A_NON_IDR_PICTURE = 1;
    const static int PFRAME = 1;
    const static int CODED_SLICE_DATA_PARTITION_A = 2;
    const static int CODED_SLICE_DATA_PARTITION_B = 3;
    const static int CODED_SLICE_DATA_PARTITION_C = 4;
    const static int CODED_SLICE_OF_AN_IDR_PICTURE = 5;
    const static int IDR = 5;
    const static int SUPPLEMENTAL_ENHANCEMENT_INFORMATION = 6;
    const static int SEQUENCE_PARAMETER_SET = 7;
    const static int SPS = 7;
    const static int PICTURE_PARAMETER_SET = 8;
    const static int PPS = 8;
    const static int ACCESS_UNIT_DELIMITER = 9;
    const static int END_OF_SEQUENCE = 10;
    const static int END_OF_STREAM = 11;
    const static int FILLER_DATA = 12;
    const static int SEQUENCE_PARAMETER_SET_EXTENSION = 13;
    const static int PREFIX_NAL_UNIT = 14;
    const static int SUBSET_SEQUENCE_PARAMETER_SET = 15;
    const static int CODED_SLICE_OF_AN_AUXILIARY_CODED_PICTURE_WITHOUT_PARTITIONING = 19;
    const static int CODED_SLICE_EXTENSION = 20;
    const static int CODED_SLICE_EXTENSION_FOR_DEPTH_VIEW_COMPONENTS = 21;
    const static int FRAGMENTATION_UNITS = 28;
    const static int FU_H264 = 28;


    const static QString toString(int message) {
        switch (message) {
        case CODED_SLICE_OF_A_NON_IDR_PICTURE:{ // nal_unit_type 1
            return "P-Frame";
        }break;

        case CODED_SLICE_DATA_PARTITION_A:{ // nal_unit_type 2
            return "CODED_SLICE_DATA_PARTITION_A";
        }break;

        case CODED_SLICE_DATA_PARTITION_B:{ // nal_unit_type 3
            return "CODED_SLICE_DATA_PARTITION_B";
        }break;

        case CODED_SLICE_DATA_PARTITION_C:{ // nal_unit_type 4
            return "CODED_SLICE_DATA_PARTITION_C";
        }break;

        case CODED_SLICE_OF_AN_IDR_PICTURE:{ // nal_unit_type 5
            return "IDR";
        }break;

        case SUPPLEMENTAL_ENHANCEMENT_INFORMATION:{ // nal_unit_type 6
            return "SUPPLEMENTAL_ENHANCEMENT_INFORMATION";
        }break;

        case SEQUENCE_PARAMETER_SET:{ // nal_unit_type 7
            return "SPS";
        }break;

        case PICTURE_PARAMETER_SET:{ // nal_unit_type 8
            return "PPS";
        }break;

        case ACCESS_UNIT_DELIMITER:{ // nal_unit_type 9
            return "ACCESS_UNIT_DELIMITER";
        }break;

        case END_OF_SEQUENCE:{ // nal_unit_type 10
            return "END_OF_SEQUENCE";
        }break;

        case END_OF_STREAM:{// nal_unit_type 11
            return "END_OF_STREAM";
        }break;

        case FILLER_DATA:{// nal_unit_type 12
            return "FILLER_DATA";
        }break;

        case SEQUENCE_PARAMETER_SET_EXTENSION:{// nal_unit_type 13
            return "SEQUENCE_PARAMETER_SET_EXTENSION";
        }break;

        case PREFIX_NAL_UNIT:{// nal_unit_type 14
            return "PREFIX_NAL_UNIT";
        }break;

        case SUBSET_SEQUENCE_PARAMETER_SET:{// nal_unit_type 15
            return "SUBSET_SEQUENCE_PARAMETER_SET";
        }break;

        case CODED_SLICE_OF_AN_AUXILIARY_CODED_PICTURE_WITHOUT_PARTITIONING:{// nal_unit_type 19
            return "CODED_SLICE_OF_AN_AUXILIARY_CODED_PICTURE_WITHOUT_PARTITIONING";
        }break;

        case CODED_SLICE_EXTENSION:{// nal_unit_type 20
            return "CODED_SLICE_EXTENSION";
        }break;

        case CODED_SLICE_EXTENSION_FOR_DEPTH_VIEW_COMPONENTS:{// nal_unit_type 21
            return "CODED_SLICE_EXTENSION_FOR_DEPTH_VIEW_COMPONENTS";
        }break;

        default:
            return "Unspecified nal unit type " +
                    QString::number(message);
        }
    }
};


#endif // CONSTS_H

