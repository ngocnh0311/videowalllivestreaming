#ifndef MESSAGE_H
#define MESSAGE_H

#include <QList>
#include <QString>

struct AppSize {
    int topHeight = 40;
    int rightWidth = 420;
    int leftWidth = 420;
};

struct AppMessage {

    QString VIDEO_WALL_USING_PLAYER = "Cam9RTCPlayer"; // Cam9RTCPlayer or Cam9Player
    const static int APP_VIDEO_WALL_MAX_NUMBER_OF_PLAYERS = 25;
    const static int APP_PLAY_BACK_MAX_NUMBER_OF_PLAYERS = 25;

    const static int UPDATE_CDN_TYPE_SELECTED = 1101;
    const static int UPDATE_MODE_RENDER_DEBUG = 1102;
    const static int UPDATE_STATE_USE_FREE_SPACE = 1113;
    const static int PLAYER_UPDATE_INFO_USE_FREE_SPACE = 1114;
    const static int UPDATE_STATE_BUTTON_TOP_BAR = 1115;
    const static int UPDATE_STATE_BUTTON_ALL_TOP_BAR = 1116;
    const static int AUTO_CLICK_UPDATE_VERSION = 1117;
    const static int VERSION_NEWEST_VERSION_UPDATER_HIDE_ALL = 1118;
    const static int AUTO_UPDATE_VERSION = 1119;
    const static int AUTO_UPDATE_VERSION_ALL = 1120;
    const static int SHOW_BUTTON_UPDATE_VERSION_WHEN_APP_HAVE_NEW_VERSION = 1121;

    const static int APP_VIDEOWALL_NEW_WINDOWS_SELECTED = 1122;
    const static int APP_VIDEOWALL_ADD_NEW_WINDOWS = 1123;
    const static int APP_VIDEOWALL_UPDATE_LIST_WINDOWS = 1124;
    const static int APP_VIDEOWALL_POP_UP_WINDOWS_SELECTED = 1125;
    const static int APP_VIDEOWALL_CLOSED_WINDOWS = 1126;
    const static int APP_VIDEOWALL_STOP_ALL_WHEN_CREATE_NEW_WORKPSACE = 1127;
    const static int APP_VIDEOWALL_POP_UP_WINDOWS_SELECTED_REMOVE_SUCCESS = 1128;
    const static int APP_VIDEOWALL_SHOW_WINDOWS_POP_UP_SELECTED = 1129;
    const static int APP_VIDEOWALL_REFRESH_TAB_LAYOUT_PAGE = 1130;
    const static int APP_VIDEO_WALL_LOAD_LAYOUT_PAGE_DEFAULT_OF_WORKSPACE = 1131;
    const static int APP_VIDEOWALL_NEED_STOP_PLAYER_WHEN_POP_UP = 1132;
    const static int APP_VIDEO_WALL_LOAD_PAGE_OF_WORKSPACE = 1133;
    const static int APP_VIDEO_WALL_DELETE_WORKSPACE = 1135;
    const static int APP_VIDEO_WALL_FULL_SCREEN_WORKSPACE = 1136;
    const static int APP_VIDEO_WALL_MAX_WORKSPACE_INIT = 4;
    const static int APP_VIDEO_WALL_SHOW_FULLSCREEN_WORKSPACE_ORIGIN = 1137;
    const static int APP_VIDEO_WALL_WORKSPACES_INIT = 1138;
    const static int APP_VIDEO_WALL_CLOSE_POP_UP_WORKSPACE = 1139;
    const static int APP_VIDEO_WALL_CLOSE_FULL_SCREEN_WORKSPACE = 1140;
    const static int APP_VIDEO_WALL_HIDE_FULLSCREEN_WORKSPACE_ORIGIN = 1141;
    const static int PREPARE_FREE_WORKSPACE_NEED_PLAYER_STOP = 1142;
    const static int UPDATE_STATE_SHOW_WINDOWS_LIST = 1143;
    const static int APP_VIDEOWALL_WORKSPACE_STOP_PLAYER_WORKING = 1144;
    const static int APP_VIDEOWALL_PLAYER_STOP_RENDERING = 1145;
    const static int APP_VIDEOWALL_PLAYER_START_RENDERING = 1146;




    const static int LOGOUT = 2;
    const static int EXIT_APP = 3;
    const static int HIDE = 0;
    const static int SHOW = 1;
    const static int ENTER_FULLSCREEN_MODE = 4;
    const static int EXIT_FULLSCREEN_MODE = 5;

    const static int SHOW_INDICATOR = 6;
    const static int HIDE_INDICATOR = 7;
    const static int SHOW_ABOUT_APP = 8;
    const static int PLAYER_RESET_RENDER_WIDGET = 9;
    const static int SHOW_HELP_USER = 10;

    const static int ENTER_DOWNLOADSCREEN_MODE = 11;
    const static int EXIT_DOWNLOADSCREEN_MODE = 12;

    const static int STOP_TIMER_RECHECK_NETWORK = 1989;

    const static int PLAYER_PLAY = 100;
    const static int PLAYER_STOP = 101;
    const static int PLAYER_PAUSE = 102;
    const static int CREATE_NEW_CAM9_RTC_PLAYER = 521;
    const static int CREATE_NEW_CAM9_PLAYER = 522;
    const static int GET_CAM9_PLAYER = 523;
    const static int GET_CAM9_RTC_PLAYER = 524;

    const static int PLAYER_STOP_WORKING = 104;
    const static int PLAYER_NEW_LIVE_SOURCE_SET = 105;
    const static int PLAYER_NEW_VOD_SOURCE_SET = 106;
    const static int PLAYER_SOURCE_CLEAR = 107;
    const static int PLAYER_CHANGE_SOURCE = 108;
    const static int PLAYER_BEGIN_SHOW_FULLSCREEN = 109;
    const static int PLAYER_END_SHOW_FULLSCREEN = 110;
    const static int PLAYER_BEGIN_HIDE_FULLSCREEN = 111;
    const static int PLAYER_END_HIDE_FULLSCREEN = 112;
    const static int PLAYER_START_LOADING = 113;
    const static int PLAYER_STOP_LOADING = 114;
    const static int PLAYER_UPDATE_INFO = 115;
    const static int PLAYER_UPDATE_INFO_WHEN_HIDE_FULL_SCREEN = 1116;
    const static int PLAYER_UPDATE_INFO_WHEN_SHOW_FULL_SCREEN = 1117;


    const static int PLAYER_BEGIN_HIDE_FULLSCREEN_AUTO = 1111;
    const static int PLAYER_BEGIN_SHOW_FULLSCREEN_AUTO = 1109;

    const static int PLAYER_BEGIN_HIDE_FULLSCREEN_HAND_CLICK_OF_REMOTE_CONTROL = 1112;
    const static int PLAYER_BEGIN_SHOW_FULLSCREEN_HAND_CLICK_OF_REMOTE_CONTROL = 1103;


    const static int PLAYER_LOADING = 116;
    const static int PLAYER_PLAYING = 117;
    const static int PLAYER_STOPED = 118;
    const static int PLAYER_PAUSED = 119;
    const static int PLAYER_NO_SOURCE = 145;

    const static int PLAYER_RECORD_NORMAL = 120;
    const static int PLAYER_RECORD_QUICK = 121;
    const static int PLAYER_TAKE_SCREENSHOT = 122;
    const static int PLAYER_PLAY_LIVE_SD = 123;
    const static int PLAYER_PLAY_LIVE_HD = 124;


    const static int VIDEO_WALL_PLAYER_SWITCH_TO_SD_WHEN_EXIT_FULL_SCREEN = 1241;
    const static int PLAY_BACK_PLAYER_PLAY_SWITCH_TO_SD_WHEN_EXIT_FULL_SCREEN = 1242;

    const static int PLAYER_DROP_LINK_DOWNLOAD = 1243;
    const static int PLAYER_CONTINUE_LINK_DOWNLOAD = 1244;


    const static int PLAYER_PLAY_VOD_SD = 125;
    const static int PLAYER_PLAY_VOD_HD = 126;
    const static int PLAYER_PLAY_SD = 127;
    const static int PLAYER_PLAY_HD = 128;
    const static int PLAYER_ERROR = 139;
    const static int PLAYER_BLANK = 140;
    const static int PLAYER_PLAY_BACK_UPDATE_MODE = 141;
    const static int PLAYER_VIDEO_WALL_UPDATE_MODE = 142;
    const static int PLAYER_PLAYBACK_PAUSED = 143;
    const static int PLAYER_PLAYBACK_UNPAUSED = 144;


    const static int APP_PLAY_BACK_PlAYER_SHOW_FULL_SCREEN = 129;
    const static int APP_PLAY_BACK_PlAYER_HIDE_FULL_SCREEN = 130;
    const static int PLAYER_NEW_VOD_SOURCE_ONE_VIDEO_SET = 131;
    const static int APP_PLAY_BACK_RESET_PLAYER_SPEED = 132;
    const static int PLAYER_PLAY_SD_CLICK = 133;
    const static int PLAYER_PLAY_HD_CLICK = 134;
    const static int PLAYER_PLAY_EXIT_CLICK = 135;
    const static int PLAYER_PLAY_FULL_CLICK = 136;
    const static int PLAYER_EXIT_POP_OUT_MODE = 137;

    const static int PLAYER_PLAY_VOD_RTC = 138;

    const static int PAGE_TRANSITION_BEGIN = 200;
    const static int PAGE_TRANSITION_START_ON_CLICK = 201;
    const static int PAGE_TRANSITION_START_AUTOMATICALLY = 202;
    const static int PAGE_TRANSITION_DELAY = 204;
    const static int PLAYER_IMAGE_DOWNLOAD = 205;
    const static int WORKSPACE_PAGE_TRANSITION_BEGIN = 206;

    const static int PROJECT_RUN = 100001;
    const static int PROJECT_EXIT = 100002;

    const static int SITE_CHANGED = 100101;
    const static int SITE_NEW_SELECTED = 100102;

    const static int APP_SHOW = 200001;
    const static int APP_HIDE = 200002;
    const static int APP_SHOW_SETTINGS = 200003;
    const static int APP_HIDE_SETTINGS = 200004;
    const static int APP_UPDATE_INDEX_SETTINGS = 200005;

    const static int APP_CHANGED = 200104;
    const static int APP_NEW_SELECTED = 200105;
    const static int TOP_CONTROL_BAR_HIDE = 300001;
    const static int TOP_CONTROL_BAR_GET_SITE_LIST = 300002;
    const static int GET_SIZE_TOP_CONTROL_BAR = 300003;

    const static int APP_VIDEO_WALL_SHOW = 400000;
    const static int APP_VIDEO_WALL_RIGHT_BAR_SHOW = 400001;
    const static int APP_VIDEO_WALL_ZONE_LAYOUT_PAGE = 400200;
    const static int APP_VIDEO_WALL_ZONE_LAYOUT_SELECTOR = 400210;
    const static int APP_VIDEO_WALL_ZONE_PAGE_SELECTOR = 400220;
    const static int APP_VIDEO_WALL_ZONE_LAYOUT_SELECTED = 400211;
    const static int APP_VIDEO_WALL_ZONE_LAYOUT_SELECTED_BY_HAND = 411212;
    const static int APP_VIDEO_WALL_ZONE_WINDOWS_SELECTOR = 411213;

    const static int APP_VIDEO_WALL_UPDATE_CAMERA_ORDER = 410211;
    const static int APP_PLAY_BACK_UPDATE_CAMERA_ORDER = 410212;

    const static int APP_VIDEO_WALL_ZONE_PAGE_SELECTED = 400212;
    const static int APP_VIDEO_WALL_ZONE_DATA_MAP = 400300;
    const static int APP_VIDEO_WALL_ZONE_SETTING = 400400;
    const static int APP_VIDEO_WALL_LAYOUT_DEFAULT_SET = 400500;
    const static int APP_VIDEO_WALL_PAGE_DEFAULT_SET = 400501;
    const static int APP_VIDEO_WALL_UPDATE_PAGE_DEFAULT = 400502;
    const static int APP_VIDEO_WALL_GET_CAMERA_WITH_LAYOUT_PAGE = 400503;

    const static int APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE = 400504;
    const static int APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS = 400505;

    const static int APP_VIDEO_WALL_GET_CAMERAS_OF_SITE_WITH_SITE_ID = 500506;
    const static int APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_SITE_ID_SUCCESS = 400507;

    const static int APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED_HANDEL = 410216;

    const static int APP_VIDEO_WALL_ZONE_PAGE_REMOTE_CONTROL_SELECTED = 400216;
    const static int APP_VIDEO_WALL_LAYOUT_PAGE_UPDATE_REMOTE_CONTROL = 400508;

    const static int APP_VIDEO_WALL_LAYOUT_UPDATE_REMOTE_CONTROL = 400509;
    const static int APP_VIDEO_WALL_PAGE_UPDATE_REMOTE_CONTROL = 400510;
    const static int APP_VIDEO_WALL_UPDATE_CAMERA_ORDER_REMOTE_CONTROL = 410220;

    const static int APP_PLAY_BACK_LAYOUT_PAGE_UPDATE_REMOTE_CONTROL = 400511;
    const static int APP_PLAY_BACK_LAYOUT_UPDATE_REMOTE_CONTROL = 400512;
    const static int APP_PLAY_BACK_PAGE_UPDATE_REMOTE_CONTROL = 400513;

    const static int APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID = 500514;
    const static int APP_VIDEO_WALL_GET_CAMERAS_WITH_DEVICE_ID_SUCCESS = 500515;
    const static int APP_PLAY_BACK_ZONE_PAGE_REMOTE_CONTROL_SELECTED = 410217;

    const static int APP_PLAY_BACK_GET_CAMERAS_WITH_DEVICE_ID = 410218;
    const static int APP_PLAY_BACK_GET_CAMERAS_WITH_DEVICE_ID_SUCCESS = 410219;






    const static int APP_VIDEO_WALL_SHOW_SITE_TOP = 400;

    const static int APP_PLAY_BACK_SHOW = 500000;

    const static int APP_PLAY_BACK_RIGHT_BAR_SHOW = 500001;
    const static int APP_PLAY_BACK_ZONE_LAYOUT_PAGE = 500200;
    const static int APP_PLAY_BACK_ZONE_LAYOUT_SELECTOR = 500210;
    const static int APP_PLAY_BACK_ZONE_PAGE_SELECTOR = 500220;
    const static int APP_PLAY_BACK_ZONE_LAYOUT_SELECTED = 500211;
    const static int APP_PLAY_BACK_ZONE_PAGE_SELECTED = 500212;
    const static int APP_PLAY_BACK_ZONE_CALENDAR_SELECTED = 500213;
    const static int APP_PLAY_BACK_CALENDAR_NEW_DAY_SELECTED = 500214;
    const static int APP_PLAY_BACK_CHANGE_SPEED_ALL_VIDEO = 500215;
    const static int APP_PLAY_BACK_CHANGE_SPEED_VIDEO = 500216;
    const static int APP_PLAY_BACK_ZONE_DATA_MAP = 500300;
    const static int APP_PLAY_BACK_ZONE_DATA_MAP_CALENDAR = 500310;
    const static int APP_PLAY_BACK_ZONE_DATA_MAP_CHANGE_SELECT_DATE = 500311;
    const static int APP_PLAY_BACK_ZONE_DATA_MAP_PLAYLIST_OF_DAY = 500320;
    const static int APP_PLAY_BACK_ZONE_SETTING = 500400;
    const static int APP_PLAY_BACK_SHOW_SITE_TOP = 500401;
    const static int APP_PLAY_BACK_LAYOUT_DEFAULT_SET = 500402;
    const static int APP_PLAY_BACK_PAGE_DEFAULT_SET = 500412;
    const static int APP_PLAY_BACK_UPDATE_PAGE_DEFAULT = 500413;
    const static int APP_PLAY_BACK_UPDATE_DATAMAP_CALENDAR = 500414;
    const static int APP_PLAY_BACK_UPDATE_DATAMAP_HOURS = 500415;
    const static int APP_PLAY_BACK_UPDATE_DATAMAP_SEEK_BAR = 500416;

    const static int APP_PLAY_BACK_CALENDAR_UPDATE_DATE_MAX = 500417;
    const static int APP_PLAY_BACK_CALENDAR_MAX_HISTORYDAY = 500218;



    const static int APP_PLAY_BACK_SEEK_BAR_TO_NEW_POSITION = 500403;

    const static int APP_PLAY_BACK_GET_CAMERA_WITH_LAYOUT_PAGE = 500407;

    const static int APP_PLAY_BACK_GET_CAMERAS_OF_SITE_WITH_LAYOUT_PAGE = 410511;
    const static int APP_PLAY_BACK_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS = 400512;

    const static int APP_PLAY_BACK_GET_CAMERAS_OF_SITE_WITH_SITE_ID = 400513;
    const static int APP_PLAY_BACK_GET_CAMERAS_OF_SITE_WITH_SITE_ID_SUCCESS = 400514;


    const static int APP_PLAY_BACK_GET_CLIP_RECORD = 400515;
    const static int APP_PLAY_BACK_GET_CLIP_RECORD_SUCCESS = 400516;
    const static int APP_PLAY_BACK_GET_ALL_CLIP_RECORD = 400517;
    const static int APP_PLAY_BACK_WAIT_BUILD_CLIP_RECORD = 400518;
    const static int APP_PLAY_BACK_GET_ALL_CLIP_RECORD_SUCCESS = 400519;
    const static int APP_PLAY_BACK_CANCEL_RECORD = 400520;

    const static int APP_PLAY_BACK_GET_CLIP_RECORD_ERROR = 400521;
    const static int APP_PLAY_BACK_GET_ALL_CLIP_RECORD_ERROR = 400522;

    const static int APP_PLAY_BACK_SET_PATH_SAVE_VIDEOS = 500443;
    const static int APP_PLAY_BACK_SET_PATH_SAVE_PICTURES = 500404;
    const static int WHEEL_EVENT_ZOOM_VIDEO = 500405;
    const static int EVENT_MOVE_ZOOM_VIDEO = 500406;

    // seekbar
    const static int APP_PLAY_BACK_UPDATE_TIME_STAMP_CURRENT = 600003;
    const static int APP_PLAY_BACK_PLAY_LIST_VIDEO_VOD = 600004;
    const static int APP_PLAY_BACK_PLAY_ONE_VIDEO_VOD = 600005;

    // record
    const static int APP_PLAY_BACK_DOWNLOAD_VIDEO_SUCCESS = 600006;
    const static int APP_PLAY_BACK_LINK_RECORD_NOT_FOUND = 600007;
    const static int APP_PLAY_BACK_START_ANIMAITON_RECORD_NORMAL = 600008;
    const static int APP_PLAY_BACK_STOP_ANIMAITON_RECORD_NORMAL = 600009;
    const static int APP_PLAY_BACK_UPDATE_PROCESS_BAR = 600010;
    const static int APP_PLAY_BACK_CLOSE_MENU_RECORD_QUICK = 600011;
    const static int APP_PLAY_BACK_START_RECORD_QUICK = 600012;
    const static int APP_PLAY_BACK_CANCEL_SAVE_VIDEO = 600013;

    const static int APP_LOGIN_SHOW = 700001;

    const static int START_JOIN_SITE = 700002;
    const static int NVR_CHANGE_IP_NEED_REFRESH = 700003;
    const static int CMS_CHANGE_CAM_ORDER_NEED_REFRESH = 700004;
    const static int DEVICE_ON_SHOW_FULL_SCREEN_CLICK_AUTO = 700005;
    const static int DEVICE_ON_HIDE_FULL_SCREEN_CLICK_AUTO = 700006;





    const static int APP_CONTEXT_GET = 80000;
    const static int APP_CAM_SITE_GET = 80001;
    const static int APP_SOCKET_TO_CONNECT = 80002;
    const static int APP_SOCKET_CONNECTED = 80003;
    const static int APP_DATA_LOADED_SUCCESS = 80004;
    const static int APP_NETWORK_IS_REACHABLE = 80007;
    const static int APP_NETWORK_IS_UNREACHABLE = 80008;
    const static int APP_NETWORK_START_CHECKING = 80009;
    const static int APP_NETWORK_STOP_CHECKING = 80010;
    const static int APP_UPDATE_USER_SITES = 80011;
    const static int APP_DATA_LOADED_REMOTE_CONTROL_SUCCESS = 80012;
    const static int APP_LOAD_DATA_REMOTE_CONTROL_PERMISSION_DENIED = 80013;
    const static int USER_CHANGED_PASSWORD_NEED_LOGOUT = 81003;
    const static int START_JOIN_USER_ID = 81002;




    const static int SITE_SELECTOR = 900100;
    const static int SITE_SELECTOR_SHOW = 900101;
    const static int SITE_SELECTOR_HIDE = 900102;

    const static int DISPLAY_LIST_APPS = 900103;
    const static int APP_SELECTOR = 900200;
    const static int APP_SELECTOR_SHOW = 900201;
    const static int APP_SELECTOR_HIDE = 900202;
    const static int TOP_BAR_OVER_LAY_CAN_HIDE = 900203;
    const static int TOP_BAR_OVER_LAY_CAN_NOT_HIDE = 900204;

    const static int PROFILE_SETTING = 900300;
    const static int PROFILE_SETTING_SHOW = 900301;
    const static int PROFILE_SETTING_HIDE = 900302;

    const static int ZONE_VERSION_UPDATER = 900310;
    const static int VERSION_UPDATER_SHOW = 900311;
    const static int VERSION_UPDATER_HIDE = 900312;
    const static int VERSION_UPDATER_HIDE_ALL = 900313;
    const static int TOP_BAR_OVER_LAY_SHOW_WHEN_VERSION_UPDATE_SHOW = 900314;
    const static int SHOW_BUTTON_UPDATE_VERSION_NEWEST = 900315;
    const static int REQUEST_CHECK_VERSION_NEWEST = 900316;
    const static int VERSION_UPDATER_DISPLAY_AUTO_SHOW = 900317;


    const static int PROFILE_SETTING_HIDE_ALL = 900304;
    const static int APP_SELECTOR_HIDE_ALL = 900305;
    const static int SITE_SELECTOR_HIDE_ALL = 900306;

    const static int APP_PLAY_BACK_UPDATE_MAX_DATE_CALENDAR = 900307;
    const static int APP_PLAY_BACK_UPDATE_TIME_PLAYLISTOFDAY = 900308;
    const static int APP_PLAY_BACK_SEEK_TO_NEW_POSITION = 900309;
    const static int APP_PLAY_BACK_SELECTED_TIME_SLOT = 900400;
    const static int APP_PLAY_BACK_UPDATE_TIMESTAMP_FOR_PLAYER = 900401;

    const static int LOAD_DATA_CAMERAS_WITH_DEVICE_ID = 900402;

    const static int DOWNLOAD_SELECTOR = 900403;
    const static int DOWNLOAD_SELECTOR_SHOW = 900404;
    const static int DOWNLOAD_SELECTOR_HIDE = 900405;
    const static int DOWNLOAD_SELECTOR_HIDE_ALL = 900406;

    //record video

    const static int CANCEL_CREATE_VIDEO_RECORD = 11001;
    const static int CANCEL_CREATE_VIDEO_RECORD_SUCCESS = 11002;

    const static int CANCEL_DOWNLOAD_VIDEO_RECORD = 11003;
    const static int CANCEL_DOWNLOAD_VIDEO_RECORD_SUCCESS= 11004;
    const static int UPDATE_PERCENT_SOCKET_GERNERATE_LINK_MP4_RECORD = 11005;
    const static int TOTAL_SECOND_REQUEST_IS_TOO_SMALL = 11006;

    //load data save local
    const static int UPDATE_DATA_STAND_ALONE_MODE_AFTER_NETWORK_IS_UNREACHABLE = 11007;
    const static int TURN_OFF_OFFLINE_MODE = 11008;
    const static int TURN_ON_OFFLINE_MODE = 11009;
    const static int APP_VIDEO_WALL_LAYOUT_DEFAULT_SET_LOCAL = 11010;
    const static int APP_PLAYBACK_LAYOUT_DEFAULT_SET_LOCAL = 11011;

    const static int APP_VIDEO_WALL_LOAD_CAMERAS_OF_SITE_WITH_LAYOUT_SUCCESS_LOCAL = 11012;
    const static int RESET_SHOW_FULL_SCREEN = 11013;

    const static int VIDEO_WALL_PLAYER_CHANGE_NETWORK_STREAM = 11014;


    //playback save local

    const static int PLAY_BACK_PLAYER_CHANGE_NETWORK_STREAM = 11015;
    const static int APP_UPDATE_DATA_LOCAL_SUCCESS = 11016;

    //crawler videowall
    const static int  VIDEO_WALL_PLAYER_CLEAR_CRAWLER_BEFORE = 11017;
    const static int CREAT_LIST_CAMERA_SENSOR = 11018;
    const static int UPDATE_LIST_CAMERA_OF_DEVICE_FOR_SENSOR_CAMERA_MANAGER = 11019;
    const static int APP_UPDATE_WORKING_SITE = 11021;
    const static int START_CRAWLER_WORKING = 11022;
    const static int STOP_CRALER_WORKING = 11023;
    const static int ADD_SUBCRIBER_CRAWLER = 11024;
    const static int REMOVE_SUBCRIBER_CRAWLER = 11025;

    const static int  VIDEO_WALL_RENDER_SUBSCRIBE_CRAWLER_CAMERA = 11026;
    const static int  VIDEO_WALL_RENDER_UNSUBSCRIBE_CRAWLER_CAMERA = 11027;

    const static int CMS_CHANGE_CAM_ORDER_REFRESH_WORKSPACE =  11028;
    const static int UPDATE_DATA_NORMAL_MODE_AFTER_NETWORK_IS_UNREACHABLE = 11029;

    //Onvif client

    const static int APP_PLAY_ONVIF_CLIENT_SHOW =  12000;
    const static int SHOW_ZONE_DISCOVER_CAMERAS = 12001;
    const static int HIDE_ZONE_DISCOVER_CAMERAS = 12002;
    const static int SHOW_ZONE_LOADING_DISCOVER_CAMERAS = 12003;
    const static int HIDE_ZONE_LOADING_DISCOVER_CAMERAS = 12004;
    const static int SHOW_ZONE_FILTER_DISCOVER_CAMERAS = 12005;
    const static int HIDE_ZONE_FILTER_DISCOVER_CAMERAS = 12006;
    const static int DISCORVER_CAMERA_FINISHED = 12007;
    const static int SHOW_ZONE_DISCOVER_IP_DEVICES = 12008;
    const static int HIDE_ZONE_DISCOVER_IP_DEVICES = 12009;


    //Cameras manager
    const static int APP_CAMERAS_MANAGER_SHOW =  13000;
    const static int APP_CAMERAS_SHOW_TAB_LIVE =  13001;
    const static int APP_CAMERAS_SHOW_TAB_ONVIF =  13002;
    const static int START_SEARCH_CAMERA_IN_NETWORK =  13003;
    const static int APP_CAMERAS_CAMERA_SELECTED =  13004;


    const static QString toString(int message) {
        switch (message) {
        case APP_PLAY_BACK_CLOSE_MENU_RECORD_QUICK:
            return "APP_PLAY_BACK_CLOSE_MENU_RECORD_QUICK";
        case APP_PLAY_BACK_LINK_RECORD_NOT_FOUND:
            return "APP_PLAY_BACK_LINK_RECORD_NOT_FOUND";
        case APP_PLAY_BACK_DOWNLOAD_VIDEO_SUCCESS:
            return "APP_PLAY_BACK_DOWNLOAD_VIDEO_SUCCESS";
        case PLAYER_PLAY:
            return "PLAYER_PLAY";
        case PLAYER_STOP:
            return "PLAYER_STOP";
        case PLAYER_SOURCE_CLEAR:
            return "PLAYER_SOURCE_CLEAR";
        case PLAYER_PAUSE:
            return "PLAYER_PAUSE";
        case PLAYER_NEW_LIVE_SOURCE_SET:
            return "PLAYER_NEW_SOURCE_SET";

        case HIDE:
            return "HIDE";
        case SHOW:
            return "SHOW";
        case PROJECT_RUN:
            return "PROJECT_RUN";
        case PROJECT_EXIT:
            return "PROJECT_EXIT";
        case SITE_CHANGED:
            return "SITE_CHANGED";
        case APP_SHOW:
            return "APP_SHOW";
        case APP_HIDE:
            return "APP_HIDE";

        case ENTER_FULLSCREEN_MODE:
            return "ENTER_FULLSCREEN_MODE";
        case EXIT_FULLSCREEN_MODE:
            return "EXIT_FULLSCREEN_MODE";

        case TOP_CONTROL_BAR_HIDE:
            return "TOP_CONTROL_BAR_HIDE";
        case TOP_CONTROL_BAR_GET_SITE_LIST:
            return "TOP_CONTROL_BAR_GET_SITE_LIST";
        case APP_VIDEO_WALL_SHOW:
            return "APP_VIDEO_WALL_SHOW";
        case APP_VIDEO_WALL_RIGHT_BAR_SHOW:
            return "APP_VIDEO_WALL_RIGHT_BAR_SHOW";
        case APP_VIDEO_WALL_ZONE_LAYOUT_PAGE:
            return "APP_VIDEO_WALL_ZONE_LAYOUT_PAGE";
        case APP_VIDEO_WALL_ZONE_LAYOUT_SELECTOR:
            return "APP_VIDEO_WALL_ZONE_LAYOUT_SELECTOR";
        case APP_VIDEO_WALL_ZONE_PAGE_SELECTOR:
            return "APP_VIDEO_WALL_ZONE_PAGE_SELECTOR";
        case APP_VIDEO_WALL_ZONE_LAYOUT_SELECTED:
            return "APP_VIDEO_WALL_ZONE_LAYOUT_SELECTED";
        case APP_VIDEO_WALL_ZONE_PAGE_SELECTED:
            return "APP_VIDEO_WALL_ZONE_PAGE_SELECTED";
        case APP_VIDEO_WALL_ZONE_DATA_MAP:
            return "APP_VIDEO_WALL_ZONE_DATA_MAP";
        case APP_VIDEO_WALL_ZONE_SETTING:
            return "APP_VIDEO_WALL_ZONE_SETTING";
        case APP_VIDEO_WALL_LAYOUT_DEFAULT_SET:
            return "APP_VIDEO_WALL_LAYOUT_DEFAULT_SET";

        case APP_PLAY_BACK_SHOW:
            return "APP_PLAY_BACK_SHOW";
        case APP_PLAY_BACK_ZONE_LAYOUT_PAGE:
            return "APP_PLAY_BACK_ZONE_LAYOUT_PAGE";
        case APP_PLAY_BACK_ZONE_LAYOUT_SELECTOR:
            return "APP_PLAY_BACK_ZONE_LAYOUT_SELECTOR";
        case APP_PLAY_BACK_ZONE_PAGE_SELECTOR:
            return "APP_PLAY_BACK_ZONE_PAGE_SELECTOR";
        case APP_PLAY_BACK_ZONE_LAYOUT_SELECTED:
            return "APP_PLAY_BACK_ZONE_LAYOUT_SELECTED";
        case APP_PLAY_BACK_ZONE_PAGE_SELECTED:
            return "APP_PLAY_BACK_ZONE_PAGE_SELECTED";
        case APP_PLAY_BACK_ZONE_DATA_MAP:
            return "APP_PLAY_BACK_ZONE_DATA_MAP";
        case APP_PLAY_BACK_ZONE_DATA_MAP_CALENDAR:
            return "APP_PLAY_BACK_ZONE_DATA_MAP_CALENDAR";
        case APP_PLAY_BACK_ZONE_DATA_MAP_CHANGE_SELECT_DATE:
            return "APP_PLAY_BACK_ZONE_DATA_MAP_CHANGE_SELECT_DATE";
        case APP_PLAY_BACK_ZONE_DATA_MAP_PLAYLIST_OF_DAY:
            return "APP_PLAY_BACK_ZONE_DATA_MAP_PLAYLIST_OF_DAY";
        case APP_PLAY_BACK_ZONE_SETTING:
            return "APP_PLAY_BACK_ZONE_SETTING";
        case APP_LOGIN_SHOW:
            return "APP_LOGIN_SHOW";
        case APP_CONTEXT_GET:
            return "APP_CONTEXT_GET";
        case APP_CAM_SITE_GET:
            return "APP_CAM_SITE_GET";

        case SITE_SELECTOR:
            return "SITE_SELECTOR";
        case SITE_SELECTOR_SHOW:
            return "SITE_SELECTOR_SHOW";
        case SITE_SELECTOR_HIDE:
            return "SITE_SELECTOR_HIDE";

        case APP_SELECTOR:
            return "APP_SELECTOR";
        case APP_SELECTOR_SHOW:
            return "APP_SELECTOR_SHOW";
        case APP_SELECTOR_HIDE:
            return "APP_SELECTOR_HIDE";

        case PROFILE_SETTING:
            return "PROFILE_SETTING";
        case PROFILE_SETTING_SHOW:
            return "PROFILE_SETTING_SHOW";
        case PROFILE_SETTING_HIDE:
            return "PROFILE_SETTING_HIDE";

        case PROFILE_SETTING_HIDE_ALL:
            return "PROFILE_SETTING_HIDE_ALL";
        case APP_SELECTOR_HIDE_ALL:
            return "APP_SELECTOR_HIDE_ALL";
        case SITE_SELECTOR_HIDE_ALL:
            return "SITE_SELECTOR_HIDE_ALL";

        case APP_CHANGED:
            return "APP_CHANGED";
        case APP_NEW_SELECTED:
            return "APP_NEW_SELECTED";
        case APP_PLAY_BACK_UPDATE_MAX_DATE_CALENDAR:
            return "APP_PLAY_BACK_UPDATE_MAX_DATE_CALENDAR";
        case APP_PLAY_BACK_CALENDAR_NEW_DAY_SELECTED:
            return "APP_PLAY_BACK_CALENDAR_NEW_DAY_SELECTED";
        case APP_PLAY_BACK_SEEK_TO_NEW_POSITION:
            return "APP_PLAY_BACK_SEEK_TO_NEW_POSITION";
        case APP_PLAY_BACK_UPDATE_TIME_STAMP_CURRENT:
            return "APP_PLAY_BACK_UPDATE_TIME_STAMP_CURRENT";
        case APP_PLAY_BACK_PlAYER_SHOW_FULL_SCREEN:
            return "APP_PLAY_BACK_PlAYER_SHOW_FULL_SCREEN";
        case APP_PLAY_BACK_PlAYER_HIDE_FULL_SCREEN:
            return "APP_PLAY_BACK_PlAYER_HIDE_FULL_SCREEN";
        case APP_PLAY_BACK_PLAY_LIST_VIDEO_VOD:
            return "APP_PLAY_BACK_PLAY_LIST_VIDEO_VOD";
        case APP_PLAY_BACK_PLAY_ONE_VIDEO_VOD:
            return "APP_PLAY_BACK_PLAY_ONE_VIDEO_VOD";
        case APP_PLAY_BACK_CHANGE_SPEED_ALL_VIDEO:
            return "APP_PLAY_BACK_CHANGE_SPEED_ALL_VIDEO";
        case APP_PLAY_BACK_CHANGE_SPEED_VIDEO:
            return "APP_PLAY_BACK_CHANGE_SPEED_VIDEO";
        case APP_PLAY_BACK_START_ANIMAITON_RECORD_NORMAL: {
            return "APP_PLAY_BACK_START_ANIMAITON_RECORD_NORMAL";
        }
        case APP_PLAY_BACK_STOP_ANIMAITON_RECORD_NORMAL: {
            return "APP_PLAY_BACK_STOP_ANIMAITON_RECORD_NORMAL";
        }
        case APP_PLAY_BACK_RESET_PLAYER_SPEED: {
            return "APP_PLAY_BACK_RESET_PLAYER_SPEED";
        }
        case APP_PLAY_BACK_UPDATE_PROCESS_BAR:
            return "APP_PLAY_BACK_UPDATE_PROCESS_BAR";
        case PLAYER_PLAY_SD_CLICK:
            return "PLAYER_PLAY_SD_CLICK";
        case PLAYER_PLAY_HD_CLICK:
            return "PLAYER_PLAY_HD_CLICK";
        case PLAYER_PLAY_EXIT_CLICK:
            return "PLAYER_PLAY_EXIT_CLICK";
        case PLAYER_PLAY_FULL_CLICK:
            return "PLAYER_PLAY_FULL_CLICK";
        case PLAYER_BEGIN_HIDE_FULLSCREEN:
            return "PLAYER_BEGIN_HIDE_FULLSCREEN";
        case PLAYER_EXIT_POP_OUT_MODE:
            return "PLAYER_EXIT_POP_OUT_MODE";
        case TOP_BAR_OVER_LAY_CAN_HIDE: {
            return "TOP_BAR_OVER_LAY_CAN_HIDE";
        } break;
        case TOP_BAR_OVER_LAY_CAN_NOT_HIDE:
            return "TOP_BAR_OVER_LAY_CAN_NOT_HIDE";
        case APP_PLAY_BACK_UPDATE_TIME_PLAYLISTOFDAY:
            return "APP_PLAY_BACK_UPDATE_TIME_PLAYLISTOFDAY";
        case PAGE_TRANSITION_BEGIN:
            return "PAGE_TRANSITION_BEGIN";
        case PAGE_TRANSITION_START_ON_CLICK:
            return "PAGE_TRANSITION_START_ON_CLICK";
        case PAGE_TRANSITION_START_AUTOMATICALLY:
            return "PAGE_TRANSITION_START_AUTOMATICALLY";
        case APP_PLAY_BACK_SELECTED_TIME_SLOT:
            return "APP_PLAY_BACK_SELECTED_TIME_SLOT";
        case APP_PLAY_BACK_SEEK_BAR_TO_NEW_POSITION:
            return "APP_PLAY_BACK_SEEK_BAR_TO_NEW_POSITION";
        case APP_PLAY_BACK_SET_PATH_SAVE_PICTURES:
            return "APP_PLAY_BACK_SET_PATH_SAVE_PICTURES";
        case PAGE_TRANSITION_DELAY:
            return "PAGE_TRANSITION_DELAY";
        case DISPLAY_LIST_APPS:
            return "DISPLAY_LIST_APPS";
        case WHEEL_EVENT_ZOOM_VIDEO:
            return "WHEEL_EVENT_ZOOM_VIDEO";
        case EVENT_MOVE_ZOOM_VIDEO:
            return "EVENT_MOVE_ZOOM_VIDEO";
        case GET_SIZE_TOP_CONTROL_BAR:
            return "GET_SIZE_TOP_CONTROL_BAR";
        case APP_PLAY_BACK_LAYOUT_DEFAULT_SET:
            return "APP_PLAY_BACK_LAYOUT_DEFAULT_SET";
        case PLAYER_DROP_LINK_DOWNLOAD:
            return "PLAYER_DROP_LINK_DOWNLOAD";
        case PLAYER_CONTINUE_LINK_DOWNLOAD:
            return "PLAYER_CONTINUE_LINK_DOWNLOAD";

        default:
            return "System hide message or unknown message " +
                    QString::number(message);
        }
    }

    const static QString getAppBackgroundColor() { return "#222"; }
};

#endif  // MESSAGE_H
